import 'dart:convert';

MaxInvestModel maxInvestModelFromJson(String str) =>
    MaxInvestModel.fromJson(json.decode(str));

String maxInvestModelToJson(MaxInvestModel data) => json.encode(data.toJson());

class MaxInvestModel {
  MaxInvestModel({
    this.maxInvest,
  });

  double maxInvest;

  factory MaxInvestModel.fromJson(Map<String, dynamic> json) => MaxInvestModel(
        maxInvest: json["max_invest"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "max_invest": maxInvest,
      };
}
