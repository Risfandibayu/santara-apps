// To parse this JSON data, do
//
//     final pralistingMetaModel = pralistingMetaModelFromJson(jsonString);

import 'dart:convert';

PralistingMetaModel pralistingMetaModelFromJson(String str) =>
    PralistingMetaModel.fromJson(json.decode(str));

String pralistingMetaModelToJson(PralistingMetaModel data) =>
    json.encode(data.toJson());

class PralistingMetaModel {
  PralistingMetaModel({
    this.meta,
    this.data,
  });

  Meta meta;
  dynamic data;

  factory PralistingMetaModel.fromJson(Map<String, dynamic> json) =>
      PralistingMetaModel(
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "meta": meta == null ? null : meta.toJson(),
        "data": data,
      };
}

class Meta {
  Meta({
    this.message,
    this.status,
  });

  String message;
  int status;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
      };
}
