enum FileUploadState { uploading, success, error, uninitialized }

class FileDataModel {
  double progress;
  FileUploadState state;
  String fileName;
  String url;

  FileDataModel({
    this.progress,
    this.state,
    this.fileName,
    this.url,
  });

  FileUploadState stateFromString(String state) {
    switch (state) {
      case 'uploading':
        return FileUploadState.uploading;
        break;
      case 'success':
        return FileUploadState.success;
        break;
      case 'error':
        return FileUploadState.error;
        break;
      default:
        return FileUploadState.uninitialized;
        break;
    }
  }

  String stateToString(FileUploadState state) {
    switch (state) {
      case FileUploadState.uploading:
        return 'uploading';
        break;
      case FileUploadState.success:
        return 'success';
        break;
      case FileUploadState.error:
        return 'error';
        break;
      default:
        return 'uninitialized';
        break;
    }
  }

  FileDataModel.fromJson(Map<String, dynamic> json) {
    progress = json['progress'];
    state = stateFromString(json['state']);
    fileName = json['file_name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['progress'] = this.progress;
    data['state'] = stateToString(this.state);
    data['file_name'] = this.fileName;
    data['url'] = this.url;
    return data;
  }

  @override
  String toString() => '''FileDataModel {
  progress: $progress,
  state: $state,
  file_name: $fileName,
  url: $url
}''';
}
