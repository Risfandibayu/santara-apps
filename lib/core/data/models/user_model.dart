import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  int id;
  String uuid;
  String email;
  String createdAt;
  String updatedAt;
  dynamic deletedAt;
  int roleId;
  int isVerified;
  int twoFactorAuth;
  dynamic twoFactorSecret;
  int isLoggedIn;
  int isDeleted;
  dynamic createdBy;
  dynamic updatedBy;
  int isOtp;
  int attempt;
  int isDana;
  String phone;
  Trader trader;
  Job job;
  TraderBank traderBank;
  Priority priority;

  User(
      {this.id,
      this.uuid,
      this.email,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.roleId,
      this.isVerified,
      this.twoFactorAuth,
      this.twoFactorSecret,
      this.isLoggedIn,
      this.isDeleted,
      this.createdBy,
      this.updatedBy,
      this.isOtp,
      this.attempt,
      this.isDana,
      this.phone,
      this.trader,
      this.job,
      this.traderBank,
      this.priority});

  factory User.fromJson(Map<String, dynamic> json) => User(
      id: json["id"],
      uuid: json["uuid"],
      email: json["email"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      deletedAt: json["deleted_at"],
      roleId: json["role_id"],
      isVerified: json["is_verified"],
      twoFactorAuth: json["two_factor_auth"],
      twoFactorSecret: json["two_factor_secret"],
      isLoggedIn: json["is_logged_in"],
      isDeleted: json["is_deleted"],
      createdBy: json["created_by"],
      updatedBy: json["updated_by"],
      isOtp: json["is_otp"],
      attempt: json["attempt"],
      isDana: json["is_dana"],
      phone: json["phone"],
      trader: json["trader"] == null ? null : Trader.fromJson(json["trader"]),
      job: json["job"] == "" || json["job"] == null
          ? null
          : Job.fromJson(json["job"]),
      traderBank: json["trader_bank"] == null
          ? null
          : TraderBank.fromJson(json["trader_bank"]),
      priority: json["priority"] == null
          ? null
          : Priority.fromJson(json["priority"]));

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "email": email,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "deleted_at": deletedAt,
        "role_id": roleId,
        "is_verified": isVerified,
        "two_factor_auth": twoFactorAuth,
        "two_factor_secret": twoFactorSecret,
        "is_logged_in": isLoggedIn,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "is_otp": isOtp,
        "attempt": attempt,
        "is_dana": isDana,
        "phone": phone,
        "trader": trader != null ? trader.toJson() : null,
        "job": job != null ? job.toJson() : null,
        "trader_bank": traderBank != null ? traderBank.toJson() : null,
        "priority": priority != null ? priority.toJson() : null,
      };
}

class Job {
  int id;
  String uuid;
  String name;
  dynamic descriptionJob;
  String income;
  String typeAnnualIncome;
  String sourceOfInvestorFunds;
  String totalAssetsOfInvestors;
  String taxAccount;
  String taxAccountCode;
  String profit;
  String startOfInvestation;
  String dividendObjection;
  String emitenInterest;
  int traderId;
  String createdAt;
  String updatedAt;
  String reasonToJoin;
  String npwp;
  dynamic dateRegistrationOfNpwp;
  int hasSubmitJob;
  int isUnlimitedInvest;

  Job({
    this.id,
    this.uuid,
    this.name,
    this.descriptionJob,
    this.income,
    this.typeAnnualIncome,
    this.sourceOfInvestorFunds,
    this.totalAssetsOfInvestors,
    this.taxAccount,
    this.taxAccountCode,
    this.profit,
    this.startOfInvestation,
    this.dividendObjection,
    this.emitenInterest,
    this.traderId,
    this.createdAt,
    this.updatedAt,
    this.reasonToJoin,
    this.npwp,
    this.dateRegistrationOfNpwp,
    this.hasSubmitJob,
    this.isUnlimitedInvest,
  });

  factory Job.fromJson(Map<String, dynamic> json) => Job(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        descriptionJob: json["description_job"],
        income: json["income"],
        typeAnnualIncome: json["type_annual_income"],
        sourceOfInvestorFunds: json["source_of_investor_funds"],
        totalAssetsOfInvestors: json["total_assets_of_investors"],
        taxAccount: json["tax_account"],
        taxAccountCode: json["tax_account_code"],
        profit: json["profit"],
        startOfInvestation: json["start_of_investation"],
        dividendObjection: json["dividend_objection"],
        emitenInterest: json["emiten_interest"],
        traderId: json["trader_id"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        reasonToJoin: json["reason_to_join"],
        npwp: json["npwp"],
        dateRegistrationOfNpwp: json["date_registration_of_npwp"],
        hasSubmitJob: json["has_submit_job"],
        isUnlimitedInvest: json["is_unlimited_invest"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "description_job": descriptionJob,
        "income": income,
        "type_annual_income": typeAnnualIncome,
        "source_of_investor_funds": sourceOfInvestorFunds,
        "total_assets_of_investors": totalAssetsOfInvestors,
        "tax_account": taxAccount,
        "tax_account_code": taxAccountCode,
        "profit": profit,
        "start_of_investation": startOfInvestation,
        "dividend_objection": dividendObjection,
        "emiten_interest": emitenInterest,
        "trader_id": traderId,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "reason_to_join": reasonToJoin,
        "npwp": npwp,
        "date_registration_of_npwp": dateRegistrationOfNpwp,
        "has_submit_job": hasSubmitJob,
        "is_unlimited_invest": isUnlimitedInvest,
      };
}

class Trader {
  // all
  int id;
  String uuid;
  String name;
  String anotherEmail;
  int hasSubmitKyc2;
  int hasSubmitKyc3;
  dynamic createdBy;
  dynamic updatedBy;
  String traderType;
  int tnc;
  dynamic isVerified;
  int lastKycSubmissionId;
  int lastKycSubmissionIdRejected;
  int isEditKyc;
  String sidNumber;
  String securitiesAccountDateRegistration;
  // personal
  dynamic phoneHome;
  dynamic altPhone;
  String address;
  dynamic bio;
  dynamic regencyId;
  int userId;
  String postalCode;
  String photo;
  String idcardPhoto;
  String verificationPhoto;
  String createdAt;
  String updatedAt;
  dynamic lang;
  String birthDate;
  String idcardNumber;
  String idcardAddress;
  int idcardRegency;
  int idcardProvince;
  String idcardPostalCode;
  int idcardCountry;
  dynamic typeIdcard;
  String expiredDateIdcard;
  String gender;
  String maritalStatus;
  String spouseName;
  String motherMaidenName;
  String heir;
  String heirRelation;
  String heirPhone;
  dynamic job;
  dynamic fax;
  dynamic annualIncome;
  int hasSubmitKyc;
  int isDeleted;
  String birthPlace;
  String regency;
  String province;
  int countryId;
  int countryDomicile;
  int educationId;
  dynamic altPhoneHome;
  dynamic altFax;
  int isKyc;

  // company
  int traderId;
  String companyPhoto;
  int companyType;
  String companyType2Code;
  String companyType2;
  int companyCharacter;
  String companyCharName;
  String businessTypeName;
  String companySyariah;
  int companyCountryDomicile;
  String countryDomicileName;
  String companyEstablishmentPlace;
  String companyEstablishmentPlaceName;
  String companyDateEstablishment;
  String taxAccountCompany;
  String taxAccountCode;
  String npwp;
  String siup;
  String siupFile;
  String nib;
  String companyCertificateNumber;
  dynamic companyNibNumber;
  int companyCountryAddress;
  String countryAddressName;
  int companyProvinceAddress;
  String provinceName;
  int companyRegencyAddress;
  String regencyName;
  String companyAddress;
  String companyPhoneNumber;
  dynamic companyResponsibleName1;
  dynamic companyResponsiblePosition1;
  dynamic companyResponsibleIdcardNumber1;
  dynamic companyResponsibleNpwp1;
  dynamic companyResponsiblePassport1;
  dynamic companyResponsibleExpiredDateIdcard1;
  dynamic companyResponsibleExpiredDatePassport1;
  dynamic companyResponsibleName2;
  dynamic companyResponsiblePosition2;
  dynamic companyResponsibleIdcardNumber2;
  dynamic companyResponsibleNpwp2;
  dynamic companyResponsiblePassport2;
  dynamic companyResponsibleExpiredDateIdcard2;
  dynamic companyResponsibleExpiredDatePassport2;
  dynamic companyResponsibleName3;
  dynamic companyResponsiblePosition3;
  dynamic companyResponsibleIdcardNumber3;
  dynamic companyResponsibleNpwp3;
  dynamic companyResponsiblePassport3;
  dynamic companyResponsibleExpiredDateIdcard3;
  dynamic companyResponsibleExpiredDatePassport3;
  dynamic companyResponsibleName4;
  dynamic companyResponsiblePosition4;
  dynamic companyResponsibleIdcardNumber4;
  dynamic companyResponsibleNpwp4;
  dynamic companyResponsiblePassport4;
  dynamic companyResponsibleExpiredDateIdcard4;
  dynamic companyResponsibleExpiredDatePassport4;
  String sourceOfFunds;
  String fundsSourceText;
  String companyIncome1;
  dynamic companyIncome2;
  dynamic companyIncome3;
  String companyTotalProperty1;
  dynamic companyTotalProperty2;
  dynamic companyTotalProperty3;
  String reasonToJoin;
  String reasonToJoinCode;
  dynamic companyTypeDocument;
  dynamic companyTypeDocumentCode;
  dynamic companyDocument;
  dynamic companyDocumentNumber;
  dynamic companyDocumentExpiredDate;
  int hasSubmitKyc1;
  int hasSubmitKyc4;
  int hasSubmitKyc5;
  int hasSubmitKyc6;
  int hasSubmitKyc7;
  int hasSubmitKyc8;
  String statusKyc1;
  String statusKyc2;
  String statusKyc3;
  String statusKyc4;
  String statusKyc5;
  String statusKyc6;
  String statusKyc7;
  String statusKyc8;
  String description;
  String lkpub;
  String lkpubCode;
  String registrationDateNpwp;
  String companyDocumentChange;
  String documentSkKemenkumham;
  String documentNpwp;
  String idCardDirector;

  Trader({
    this.id,
    this.uuid,
    this.name,
    this.anotherEmail,
    this.hasSubmitKyc2,
    this.hasSubmitKyc3,
    this.createdBy,
    this.updatedBy,
    this.traderType,
    this.tnc,
    this.isVerified,
    this.lastKycSubmissionId,
    this.lastKycSubmissionIdRejected,
    this.isEditKyc,
    this.sidNumber,
    this.securitiesAccountDateRegistration,
    this.phoneHome,
    this.altPhone,
    this.address,
    this.bio,
    this.regencyId,
    this.userId,
    this.postalCode,
    this.photo,
    this.idcardPhoto,
    this.verificationPhoto,
    this.createdAt,
    this.updatedAt,
    this.lang,
    this.birthDate,
    this.idcardNumber,
    this.idcardAddress,
    this.idcardRegency,
    this.idcardProvince,
    this.idcardPostalCode,
    this.idcardCountry,
    this.typeIdcard,
    this.expiredDateIdcard,
    this.gender,
    this.maritalStatus,
    this.spouseName,
    this.motherMaidenName,
    this.heir,
    this.heirRelation,
    this.heirPhone,
    this.job,
    this.fax,
    this.annualIncome,
    this.hasSubmitKyc,
    this.isDeleted,
    this.birthPlace,
    this.regency,
    this.province,
    this.countryId,
    this.countryDomicile,
    this.educationId,
    this.altPhoneHome,
    this.altFax,
    this.isKyc,
    this.traderId,
    this.companyPhoto,
    this.companyType,
    this.companyType2Code,
    this.companyType2,
    this.companyCharacter,
    this.companyCharName,
    this.businessTypeName,
    this.companySyariah,
    this.companyCountryDomicile,
    this.countryDomicileName,
    this.companyEstablishmentPlace,
    this.companyEstablishmentPlaceName,
    this.companyDateEstablishment,
    this.taxAccountCompany,
    this.taxAccountCode,
    this.npwp,
    this.siup,
    this.siupFile,
    this.nib,
    this.companyCertificateNumber,
    this.companyNibNumber,
    this.companyCountryAddress,
    this.countryAddressName,
    this.companyProvinceAddress,
    this.provinceName,
    this.companyRegencyAddress,
    this.regencyName,
    this.companyAddress,
    this.companyPhoneNumber,
    this.companyResponsibleName1,
    this.companyResponsiblePosition1,
    this.companyResponsibleIdcardNumber1,
    this.companyResponsibleNpwp1,
    this.companyResponsiblePassport1,
    this.companyResponsibleExpiredDateIdcard1,
    this.companyResponsibleExpiredDatePassport1,
    this.companyResponsibleName2,
    this.companyResponsiblePosition2,
    this.companyResponsibleIdcardNumber2,
    this.companyResponsibleNpwp2,
    this.companyResponsiblePassport2,
    this.companyResponsibleExpiredDateIdcard2,
    this.companyResponsibleExpiredDatePassport2,
    this.companyResponsibleName3,
    this.companyResponsiblePosition3,
    this.companyResponsibleIdcardNumber3,
    this.companyResponsibleNpwp3,
    this.companyResponsiblePassport3,
    this.companyResponsibleExpiredDateIdcard3,
    this.companyResponsibleExpiredDatePassport3,
    this.companyResponsibleName4,
    this.companyResponsiblePosition4,
    this.companyResponsibleIdcardNumber4,
    this.companyResponsibleNpwp4,
    this.companyResponsiblePassport4,
    this.companyResponsibleExpiredDateIdcard4,
    this.companyResponsibleExpiredDatePassport4,
    this.sourceOfFunds,
    this.fundsSourceText,
    this.companyIncome1,
    this.companyIncome2,
    this.companyIncome3,
    this.companyTotalProperty1,
    this.companyTotalProperty2,
    this.companyTotalProperty3,
    this.reasonToJoin,
    this.reasonToJoinCode,
    this.companyTypeDocument,
    this.companyTypeDocumentCode,
    this.companyDocument,
    this.companyDocumentNumber,
    this.companyDocumentExpiredDate,
    this.hasSubmitKyc1,
    this.hasSubmitKyc4,
    this.hasSubmitKyc5,
    this.hasSubmitKyc6,
    this.hasSubmitKyc7,
    this.hasSubmitKyc8,
    this.statusKyc1,
    this.statusKyc2,
    this.statusKyc3,
    this.statusKyc4,
    this.statusKyc5,
    this.statusKyc6,
    this.statusKyc7,
    this.statusKyc8,
    this.description,
    this.lkpub,
    this.lkpubCode,
    this.registrationDateNpwp,
    this.companyDocumentChange,
    this.documentNpwp,
    this.documentSkKemenkumham,
    this.idCardDirector,
  });

  factory Trader.fromJson(Map<String, dynamic> json) => Trader(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        anotherEmail: json["another_email"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        hasSubmitKyc2: json["has_submit_kyc2"],
        hasSubmitKyc3: json["has_submit_kyc3"],
        isVerified: json["is_verified"],
        tnc: json["tnc"],
        traderType: json["trader_type"],
        lastKycSubmissionId: json["last_kyc_submission_id"],
        lastKycSubmissionIdRejected: json["last_kyc_submission_id_rejected"],
        isEditKyc: json["is_edit_kyc"],
        sidNumber: json["sid_number"],
        securitiesAccountDateRegistration:
            json["securities_account_date_registration"],
        phoneHome: json["phone_home"],
        altPhone: json["alt_phone"],
        address: json["address"],
        bio: json["bio"],
        regencyId: json["regency_id"],
        userId: json["user_id"],
        postalCode: json["postal_code"],
        photo: json["photo"],
        idcardPhoto: json["idcard_photo"],
        verificationPhoto: json["verification_photo"],
        lang: json["lang"],
        birthDate: json["birth_date"],
        idcardNumber: json["idcard_number"],
        idcardAddress: json["idcard_address"],
        idcardRegency: json["idcard_regency"],
        idcardProvince: json["idcard_province"],
        idcardPostalCode: json["idcard_postal_code"],
        idcardCountry: json["idcard_country"],
        typeIdcard: json["type_idcard"],
        expiredDateIdcard: json["expired_date_idcard"],
        gender: json["gender"],
        maritalStatus: json["marital_status"],
        spouseName: json["spouse_name"],
        motherMaidenName: json["mother_maiden_name"],
        heir: json["heir"],
        heirRelation: json["heir_relation"],
        heirPhone: json["heir_phone"],
        job: json["job"],
        fax: json["fax"],
        annualIncome: json["annual_income"],
        hasSubmitKyc: json["has_submit_kyc"],
        isDeleted: json["is_deleted"],
        birthPlace: json["birth_place"],
        regency: json["regency"],
        province: json["province"],
        countryId: json["country_id"],
        countryDomicile: json["country_domicile"],
        educationId: json["education_id"],
        altPhoneHome: json["alt_phone_home"],
        altFax: json["alt_fax"],
        isKyc: json["is_kyc"],
        traderId: json["trader_id"],
        companyPhoto: json["company_photo"],
        companyType: json["company_type"],
        companyType2Code: json["company_type2_code"],
        companyType2: json["company_type2"],
        companyCharacter: json["company_character"],
        companyCharName: json["company_char_name"],
        businessTypeName: json["business_type_name"],
        companySyariah: json["company_syariah"],
        companyCountryDomicile: json["company_country_domicile"],
        countryDomicileName: json['country_domicile_name'],
        companyEstablishmentPlace: json["company_establishment_place"],
        companyEstablishmentPlaceName: json["company_establishment_place_name"],
        companyDateEstablishment: json["company_date_establishment"],
        taxAccountCompany: json["tax_account_company"],
        taxAccountCode: json["tax_account_code"],
        npwp: json["npwp"],
        siup: json["siup"],
        siupFile: json["document_siup"],
        nib: json["nib"],
        companyCertificateNumber: json["company_certificate_number"],
        companyNibNumber: json["company_nib_number"],
        companyCountryAddress: json["company_country_address"],
        countryAddressName: json['country_address_name'],
        companyProvinceAddress: json["company_province_address"],
        provinceName: json['province_name'],
        companyRegencyAddress: json["company_regency_address"],
        regencyName: json['regency_name'],
        companyAddress: json["company_address"],
        companyPhoneNumber: json["company_phone_number"],
        //pj1
        companyResponsibleName1: json["company_responsible_name1"],
        companyResponsiblePosition1: json["company_responsible_position1"],
        companyResponsibleIdcardNumber1:
            json["company_responsible_idcard_number1"],
        companyResponsibleNpwp1: json["company_responsible_npwp1"],
        companyResponsiblePassport1: json["company_responsible_passport1"],
        companyResponsibleExpiredDateIdcard1:
            json["company_responsible_expired_date_idcard1"],
        companyResponsibleExpiredDatePassport1:
            json["company_responsible_expired_date_passport1"],
        //pj2
        companyResponsibleName2: json["company_responsible_name2"],
        companyResponsiblePosition2: json["company_responsible_position2"],
        companyResponsibleIdcardNumber2:
            json["company_responsible_idcard_number2"],
        companyResponsibleNpwp2: json["company_responsible_npwp2"],
        companyResponsiblePassport2: json["company_responsible_passport2"],
        companyResponsibleExpiredDateIdcard2:
            json["company_responsible_expired_date_idcard2"],
        companyResponsibleExpiredDatePassport2:
            json["company_responsible_expired_date_passport2"],
        //pj3
        companyResponsibleName3: json["company_responsible_name3"],
        companyResponsiblePosition3: json["company_responsible_position3"],
        companyResponsibleIdcardNumber3:
            json["company_responsible_idcard_number3"],
        companyResponsibleNpwp3: json["company_responsible_npwp3"],
        companyResponsiblePassport3: json["company_responsible_passport3"],
        companyResponsibleExpiredDateIdcard3:
            json["company_responsible_expired_date_idcard3"],
        companyResponsibleExpiredDatePassport3:
            json["company_responsible_expired_date_passport3"],
        //pj4
        companyResponsibleName4: json["company_responsible_name4"],
        companyResponsiblePosition4: json["company_responsible_position4"],
        companyResponsibleIdcardNumber4:
            json["company_responsible_idcard_number4"],
        companyResponsibleNpwp4: json["company_responsible_npwp4"],
        companyResponsiblePassport4: json["company_responsible_passport4"],
        companyResponsibleExpiredDateIdcard4:
            json["company_responsible_expired_date_idcard4"],
        companyResponsibleExpiredDatePassport4:
            json["company_responsible_expired_date_passport4"],
        //end pj
        sourceOfFunds: json["source_of_funds"],
        fundsSourceText: json["funds_source_text"],
        companyIncome1: json["company_income1"],
        companyIncome2: json["company_income2"],
        companyIncome3: json["company_income3"],
        companyTotalProperty1: json["company_total_property1"],
        companyTotalProperty2: json["company_total_property2"],
        companyTotalProperty3: json["company_total_property3"],
        reasonToJoin: json["reason_to_join"],
        reasonToJoinCode: json["reason_to_join_code"],
        companyTypeDocument: json["company_type_document"],
        companyTypeDocumentCode: json["company_type_document_code"],
        companyDocument: json["company_document"],
        companyDocumentNumber: json["company_document_number"],
        companyDocumentExpiredDate: json["company_document_expired_date"],
        hasSubmitKyc1: json["has_submit_kyc1"],
        hasSubmitKyc4: json["has_submit_kyc4"],
        hasSubmitKyc5: json["has_submit_kyc5"],
        hasSubmitKyc6: json["has_submit_kyc6"],
        hasSubmitKyc7: json["has_submit_kyc7"],
        hasSubmitKyc8: json["has_submit_kyc8"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        description: json["description"],
        lkpub: json["lkpub"],
        lkpubCode: json["lkpub_code"],
        registrationDateNpwp: json["registration_date_npwp"],
        companyDocumentChange: json["company_document_change"],
        documentSkKemenkumham: json["document_sk_kemenkumham"],
        documentNpwp: json["document_npwp"],
        idCardDirector: json["idcard_director"],
      );

  Map<String, dynamic> toJsonStatusKyc() => {
        "status_kyc1": statusKyc1,
        "status_kyc2": statusKyc2,
        "status_kyc3": statusKyc3,
        "status_kyc4": statusKyc4,
        "status_kyc5": statusKyc5,
        "status_kyc6": statusKyc6,
        "status_kyc7": statusKyc7,
        "status_kyc8": statusKyc8,
      };

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "phone_home": phoneHome,
        "alt_phone": altPhone,
        "address": address,
        "bio": bio,
        "regency_id": regencyId,
        "user_id": userId,
        "postal_code": postalCode,
        "photo": photo,
        "idcard_photo": idcardPhoto,
        "verification_photo": verificationPhoto,
        "is_verified": isVerified,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "lang": lang,
        "birth_date": birthDate,
        "idcard_number": idcardNumber,
        "idcard_address": idcardAddress,
        "idcard_regency": idcardRegency,
        "idcard_province": idcardProvince,
        "idcard_postal_code": idcardPostalCode,
        "idcard_country": idcardCountry,
        "type_idcard": typeIdcard,
        "expired_date_idcard": expiredDateIdcard,
        "another_email": anotherEmail,
        "gender": gender,
        "marital_status": maritalStatus,
        "spouse_name": spouseName,
        "mother_maiden_name": motherMaidenName,
        "heir": heir,
        "heir_relation": heirRelation,
        "heir_phone": heirPhone,
        "job": job,
        "fax": fax,
        "annual_income": annualIncome,
        "has_submit_kyc": hasSubmitKyc,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "birth_place": birthPlace,
        "regency": regency,
        "province": province,
        "country_id": countryId,
        "country_domicile": countryDomicile,
        "education_id": educationId,
        "tnc": tnc,
        "alt_phone_home": altPhoneHome,
        "alt_fax": altFax,
        "trader_type": traderType,
        "last_kyc_submission_id": lastKycSubmissionId,
        "last_kyc_submission_id_rejected": lastKycSubmissionIdRejected,
        "is_edit_kyc": isEditKyc,
        "sid_number": sidNumber,
        "securities_account_date_registration":
            securitiesAccountDateRegistration,
        "has_submit_kyc2": hasSubmitKyc2,
        "has_submit_kyc3": hasSubmitKyc3,
        "is_kyc": isKyc,
        "description": description,
        "lkpub": lkpub,
        "lkpub_code": lkpubCode,
        "registration_date_npwp": registrationDateNpwp,
        "company_document_change": companyDocumentChange,
        "document_sk_kemenkumham": documentSkKemenkumham,
        "document_npwp": documentNpwp,
        "idcard_director": idCardDirector,
      };
}

class TraderBank {
  int id;
  String uuid;
  int traderId;
  String bank1;
  dynamic bankInvestor1;
  String accountName1;
  String accountCurrency1;
  String accountNumber1;
  String bank2;
  dynamic bankInvestor2;
  dynamic accountName2;
  dynamic accountCurrency2;
  dynamic accountNumber2;
  String bank3;
  dynamic bankInvestor3;
  dynamic accountName3;
  dynamic accountCurrency3;
  dynamic accountNumber3;
  dynamic securitiesAccount;
  String securitiesAccountDateRegistration;
  int hasSubmitBank;
  int isVerifiedBank;
  int isDeleted;
  String createdAt;
  String updatedAt;

  TraderBank({
    this.id,
    this.uuid,
    this.traderId,
    this.bank1,
    this.bankInvestor1,
    this.accountName1,
    this.accountCurrency1,
    this.accountNumber1,
    this.bank2,
    this.bankInvestor2,
    this.accountName2,
    this.accountCurrency2,
    this.accountNumber2,
    this.bank3,
    this.bankInvestor3,
    this.accountName3,
    this.accountCurrency3,
    this.accountNumber3,
    this.securitiesAccount,
    this.securitiesAccountDateRegistration,
    this.hasSubmitBank,
    this.isVerifiedBank,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
  });

  factory TraderBank.fromJson(Map<String, dynamic> json) => TraderBank(
        id: json["id"],
        uuid: json["uuid"],
        traderId: json["trader_id"],
        bankInvestor1: json["bank_investor1"],
        bank1: json['bank1'],
        accountName1: json["account_name1"],
        accountCurrency1: json["account_currency1"],
        accountNumber1: json["account_number1"],
        bank2: json['bank2'],
        bankInvestor2: json["bank_investor2"],
        accountName2: json["account_name2"],
        accountCurrency2: json["account_currency2"],
        accountNumber2: json["account_number2"],
        bank3: json['bank3'],
        bankInvestor3: json["bank_investor3"],
        accountName3: json["account_name3"],
        accountCurrency3: json["account_currency3"],
        accountNumber3: json["account_number3"],
        securitiesAccount: json["securities_account"],
        securitiesAccountDateRegistration:
            json["securities_account_date_registration"],
        hasSubmitBank: json["has_submit_bank"],
        isVerifiedBank: json["is_verified_bank"],
        isDeleted: json["is_deleted"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "trader_id": traderId,
        "bank1": bank1,
        "bank_investor1": bankInvestor1,
        "account_name1": accountName1,
        "account_currency1": accountCurrency1,
        "account_number1": accountNumber1,
        "bank2": bank2,
        "bank_investor2": bankInvestor2,
        "account_name2": accountName2,
        "account_currency2": accountCurrency2,
        "account_number2": accountNumber2,
        "bank3": bank3,
        "bank_investor3": bankInvestor3,
        "account_name3": accountName3,
        "account_currency3": accountCurrency3,
        "account_number3": accountNumber3,
        "securities_account": securitiesAccount,
        "securities_account_date_registration":
            securitiesAccountDateRegistration,
        "has_submit_bank": hasSubmitBank,
        "is_verified_bank": isVerifiedBank,
        "is_deleted": isDeleted,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}

class Priority {
  int id;
  String uuid;
  String priority1;
  String priority2;
  String priority3;
  int traderId;
  String createdAt;
  String updatedAt;
  int hasSubmitPriorities;

  Priority({
    this.id,
    this.uuid,
    this.priority1,
    this.priority2,
    this.priority3,
    this.traderId,
    this.createdAt,
    this.updatedAt,
    this.hasSubmitPriorities,
  });

  factory Priority.fromJson(Map<String, dynamic> json) => Priority(
        id: json["id"],
        uuid: json["uuid"],
        priority1: json["priority1"],
        priority2: json["priority2"],
        priority3: json["priority3"],
        traderId: json["trader_id"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        hasSubmitPriorities: json["has_submit_priorities"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "priority1": priority1,
        "priority2": priority2,
        "priority3": priority3,
        "trader_id": traderId,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "has_submit_priorities": hasSubmitPriorities,
      };
}
