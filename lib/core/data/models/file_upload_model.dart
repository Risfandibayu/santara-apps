// To parse this JSON data, do
//
//     final fileUploadModel = fileUploadModelFromJson(jsonString);

import 'dart:convert';

FileUploadModel fileUploadModelFromJson(String str) =>
    FileUploadModel.fromJson(json.decode(str));

String fileUploadModelToJson(FileUploadModel data) =>
    json.encode(data.toJson());

class FileUploadModel {
  FileUploadModel({
    this.url,
    this.filename,
  });

  String url;
  String filename;

  factory FileUploadModel.fromJson(Map<String, dynamic> json) =>
      FileUploadModel(
        url: json["url"] == null ? null : json["url"],
        filename: json["filename"] == null ? null : json["filename"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "filename": filename == null ? null : filename,
      };
}
