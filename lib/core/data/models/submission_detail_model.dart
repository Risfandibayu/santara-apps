class Submission {
  Submission({
    this.id,
    this.emitenId,
    this.status,
    this.submissionDetail,
  });

  int id;
  int emitenId;
  String status;
  List<SubmissionDetail> submissionDetail;

  factory Submission.fromJson(Map<String, dynamic> json) => Submission(
        id: json["id"] == null ? null : json["id"],
        emitenId: json["emiten_id"] == null ? null : json["emiten_id"],
        status: json["status"] == null ? null : json["status"],
        submissionDetail: json["submission_detail"] == null
            ? null
            : List<SubmissionDetail>.from(json["submission_detail"]
                .map((x) => SubmissionDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "emiten_id": emitenId == null ? null : emitenId,
        "status": status == null ? null : status,
        "submission_detail": submissionDetail == null
            ? null
            : List<dynamic>.from(submissionDetail.map((x) => x.toJson())),
      };
}

class SubmissionDetail {
  SubmissionDetail({
    this.id,
    this.fieldId,
    this.stepId,
    this.status,
    this.error,
    this.pralistingSubmissionId,
  });

  int id;
  String fieldId;
  String stepId;
  int status;
  String error;
  int pralistingSubmissionId;

  factory SubmissionDetail.fromJson(Map<String, dynamic> json) =>
      SubmissionDetail(
        id: json["id"] == null ? null : json["id"],
        fieldId: json["field_id"] == null ? null : json["field_id"],
        stepId: json["step_id"] == null ? null : json["step_id"],
        status: json["status"] == null ? null : json["status"],
        error: json["error"] == null ? null : json["error"],
        pralistingSubmissionId: json["pralisting_submission_id"] == null
            ? null
            : json["pralisting_submission_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "field_id": fieldId == null ? null : fieldId,
        "step_id": stepId == null ? null : stepId,
        "status": status == null ? null : status,
        "error": error == null ? null : error,
        "pralisting_submission_id":
            pralistingSubmissionId == null ? null : pralistingSubmissionId,
      };
}
