import 'package:meta/meta.dart';

enum FormStatus {
  submit_success,
  submit_success_close_page,
  delete_success,
  delete_error,
  get_success,
  submit_error,
  validation_error,
  get_error,
  get_field_data_error,
  get_field_data_success
}

class FormResult {
  final FormStatus status;
  final String message;
  final dynamic data;

  FormResult({
    @required this.status,
    @required this.message,
    this.data,
  });
}
