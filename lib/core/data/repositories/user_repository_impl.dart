import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../domain/repositories/user_repository.dart';
import '../../error/failure.dart';
import '../../http/network_info.dart';
// import '../datasources/user_local_data_source.dart';
import '../datasources/user_remote_data_source.dart';
import '../models/user_model.dart';

class UserRepositoryImpl implements UserRepository {
  final NetworkInfo networkInfo;
  final UserRemoteDataSource remoteDataSource;
  // final UserLocalDataSource localDataSource;

  UserRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
    // @required this.localDataSource,
  });

  @override
  Future<Either<Failure, User>> getUserByUuid({@required String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getUserByUuid(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  // @override
  // Future<Either<Failure, void>> clearStorageData() async {
  //   try {
  //     final result = await localDataSource.clearStorageData();
  //     return Right(result);
  //   } catch (e, stack) {
  //     santaraLog(e, stack);
  //     return Left(
  //       Failure(
  //         type: FailureType.localError,
  //         message: "Tidak dapat menghapus data!",
  //       ),
  //     );
  //   }
  // }

  // @override
  // Future<Either<Failure, void>> saveLoginData({LoginResultModel data}) async {
  //   try {
  //     final result = await localDataSource.saveLoginData(data);
  //     return Right(result);
  //   } catch (e, stack) {
  //     santaraLog(e, stack);
  //     return Left(
  //       Failure(
  //         type: FailureType.localError,
  //         message: "Tidak dapat menyimpan data login!",
  //       ),
  //     );
  //   }
  // }

  // @override
  // Future<Either<Failure, Token>> getToken() async {
  //   try {
  //     final result = await localDataSource.getToken();
  //     return Right(result);
  //   } catch (e, stack) {
  //     santaraLog(e, stack);
  //     return Left(
  //       Failure(
  //         type: FailureType.localError,
  //         message: "Tidak dapat menerima data token!",
  //       ),
  //     );
  //   }
  // }

  // @override
  // Future<Either<Failure, void>> logoutAccount({
  //   @required String refreshToken,
  //   @required String id,
  // }) async {
  //   if (await networkInfo.isConnected) {
  //     try {
  //       final data = await remoteDataSource.logoutAccount(
  //         refreshToken: refreshToken,
  //         id: id,
  //       );
  //       return Right(data);
  //     } on Failure catch (failure) {
  //       return Left(failure);
  //     }
  //   } else {
  //     return Left(Failure(type: FailureType.noConnection));
  //   }
  // }

  // @override
  // Future<Either<Failure, LoginResultModel>> getUserLocal() async {
  //   try {
  //     final result = await localDataSource.getLoginData();
  //     return Right(result);
  //   } catch (e, stack) {
  //     santaraLog(e, stack);
  //     return Left(
  //       Failure(
  //         type: FailureType.localError,
  //         message: "Tidak dapat menerima data login!",
  //       ),
  //     );
  //   }
  // }
}
