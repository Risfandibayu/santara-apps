import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../domain/repositories/max_invest_repository.dart';
import '../../error/failure.dart';
import '../../http/network_info.dart';
import '../datasources/max_invest_data_source.dart';
import '../models/max_invest_model.dart';

class MaxInvestRepositoryImpl implements MaxInvestRepository {
  final NetworkInfo networkInfo;
  final MaxInvestRemoteDataSource dataSource;

  MaxInvestRepositoryImpl({
    @required this.networkInfo,
    @required this.dataSource,
  });

  @override
  Future<Either<Failure, MaxInvestModel>> getMaxInvest() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await dataSource.getMaxInvest();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
