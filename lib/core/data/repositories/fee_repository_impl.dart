import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../domain/repositories/fee_repository.dart';
import '../../error/failure.dart';
import '../../http/network_info.dart';
import '../datasources/fee_remote_data_source.dart';
import '../models/fee_model.dart';

class FeeRepositoryImpl implements FeeRepository {
  final NetworkInfo networkInfo;
  final FeeRemoteDataSource remoteDataSource;

  FeeRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, FeeModel>> getAllFee() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getAllFee();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
