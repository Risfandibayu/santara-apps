import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../error/failure.dart';
import '../../http/rest_client.dart';
import '../../utils/helpers/rest_helper.dart';
import '../models/max_invest_model.dart';

abstract class MaxInvestRemoteDataSource {
  Future<MaxInvestModel> getMaxInvest();
}

class MaxInvestRemoteDataSourceImpl implements MaxInvestRemoteDataSource {
  final RestClient client;
  MaxInvestRemoteDataSourceImpl({@required this.client});

  @override
  Future<MaxInvestModel> getMaxInvest() async {
    try {
      final result = await client.get(path: "/traders/max-invest");
      if (result.statusCode == 200) {
        return MaxInvestModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
