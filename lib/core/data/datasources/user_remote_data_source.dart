import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../error/failure.dart';
import '../../http/rest_client.dart';
import '../../utils/helpers/rest_helper.dart';
import '../models/user_model.dart';

abstract class UserRemoteDataSource {
  Future<User> getUserByUuid({@required String uuid});
  // Future<void> logoutAccount({
  //   @required String refreshToken,
  //   @required String id,
  // });
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final RestClient client;
  UserRemoteDataSourceImpl({@required this.client});

  @override
  Future<User> getUserByUuid({@required String uuid}) async {
    try {
      final result = await client.get(path: "/users/by-uuid/$uuid");
      if (result.statusCode == 200) {
        return User.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  // @override
  // Future<void> logoutAccount({
  //   @required String refreshToken,
  //   @required String id,
  // }) async {
  //   try {
  //     final body = {
  //       "refreshToken": "$refreshToken",
  //       "user_id": "$id",
  //     };
  //     final result = await client.post(path: "/auth/logout", body: body);
  //     if (result.statusCode == 200) {
  //       return true;
  //     } else {
  //       throw RestHelper.throwServerError(result);
  //     }
  //   } on DioError catch (error) {
  //     throw RestHelper.catchFailure(error: error);
  //   } on Failure catch (e, stack) {
  //     throw RestHelper.throwFailure(e, stack: stack);
  //   } catch (e, stack) {
  //     throw RestHelper.throwLocalError(error: e, stack: stack);
  //   }
  // }
}
