// import 'dart:convert';

// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:meta/meta.dart';

// import '../../../features/auth/login/data/models/login_result_model.dart';
// import '../../utils/constants/app_constants.dart';
// import '../../utils/tools/logger.dart';
// import '../models/user_model.dart';

// abstract class UserLocalDataSource {
//   Future<void> saveLoginData(LoginResultModel result);
//   Future<void> clearStorageData();
//   Future<LoginResultModel> getLoginData();
//   Future<Token> getToken();
// }

// class UserLocalDataSourceImpl implements UserLocalDataSource {
//   final FlutterSecureStorage storage;
//   UserLocalDataSourceImpl({@required this.storage});

//   @override
//   Future<LoginResultModel> getLoginData() async {
//     try {
//       final userLoginData = await storage.read(key: USER_LOGIN_DATA);
//       final userRefreshToken = await storage.read(key: USER_REFRESH_TOKEN);
//       final userBearerToken = await storage.read(key: USER_BEARER_TOKEN);
//       if (userLoginData != null &&
//           userRefreshToken != null &&
//           userBearerToken != null) {
//         final user = User.fromJson(json.decode(userLoginData));
//         final rawJsonToken = {
//           "refreshToken": userRefreshToken,
//           "token": userBearerToken,
//           "type": "token",
//         };
//         final token = Token.fromJson(rawJsonToken);
//         return LoginResultModel(token: token, user: user);
//       } else {
//         return null;
//       }
//     } catch (e, stack) {
//       santaraLog(e, stack);
//       return null;
//     }
//   }

//   @override
//   Future<void> saveLoginData(LoginResultModel result) async {
//     storage.write(key: USER_BEARER_TOKEN, value: result.token.token);
//     storage.write(key: USER_REFRESH_TOKEN, value: result.token.refreshToken);
//     storage.write(key: USER_UUID, value: result.user.uuid);
//     storage.write(key: USER_ID, value: "${result.user.id}");
//     storage.write(key: USER_LOGIN_DATA, value: userToJson(result.user));
//   }

//   @override
//   Future<void> clearStorageData() async {
//     await storage.deleteAll();
//   }

//   @override
//   Future<Token> getToken() async {
//     final bearerToken = await storage.read(key: USER_BEARER_TOKEN);
//     final refreshToken = await storage.read(key: USER_REFRESH_TOKEN);

//     if (bearerToken != null && refreshToken != null) {
//       return Token(
//         refreshToken: refreshToken,
//         token: bearerToken,
//         type: "token",
//       );
//     } else {
//       return null;
//     }
//   }
// }
