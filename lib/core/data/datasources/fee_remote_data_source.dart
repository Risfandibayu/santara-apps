import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';

import '../../error/failure.dart';
import '../../http/rest_client.dart';
import '../../utils/helpers/rest_helper.dart';
import '../models/fee_model.dart';

abstract class FeeRemoteDataSource {
  Future<FeeModel> getAllFee();
}

class FeeRemoteDataSourceImpl implements FeeRemoteDataSource {
  final RestClient client;
  FeeRemoteDataSourceImpl({@required this.client});

  @override
  Future<FeeModel> getAllFee() async {
    try {
      final result = await client.getExternalUrl(url: "$apiLocalImage/fee");
      if (result.statusCode == 200) {
        return FeeModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
