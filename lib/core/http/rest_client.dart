import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

abstract class RestClient {
  // Rest client for santara api services
  Future<Response> get({@required String path, dynamic headers, int timeout});
  Future<Response> post(
      {@required String path, dynamic body, dynamic headers, int timeout});
  Future<Response> put(
      {@required String path, dynamic body, dynamic headers, int timeout});
  Future<Response> delete(
      {@required String path, dynamic body, dynamic headers, int timeout});

  // If the app need external request, use this method instead
  Future<Response> getExternalUrl({
    @required String url,
    dynamic headers,
    int timeout,
  });

  Future<Response> postExternalUrl({
    @required String url,
    dynamic body,
    dynamic headers,
    int timeout,
  });

  Future<Response> putExternalUrl({
    @required String url,
    dynamic body,
    dynamic headers,
    int timeout,
  });

  Future<Response> deleteExternalUrl({
    @required String url,
    dynamic body,
    dynamic headers,
    int timeout,
  });
}
