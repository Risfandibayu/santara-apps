import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../helpers/UserAgent.dart';
import '../../utils/api.dart';
import '../error/failure.dart';
import 'rest_client.dart';

class RestClientImpl implements RestClient {
  final Dio dioClient;
  RestClientImpl({@required this.dioClient});

  @override
  Future<Response> get({
    String path,
    dynamic headers,
    int timeout,
  }) async {
    final generatedHeaders = await UserAgent.headers();
    dioClient.options.headers = headers == null ? generatedHeaders : headers;
    // logger.i(">> Headers : $generatedHeaders");
    final result = await dioClient.get("$apiLocal$path").timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> post({
    String path,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.post("$apiLocal$path", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> put({
    String path,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.put("$apiLocal$path", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> delete({
    String path,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.delete("$apiLocal$path", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> deleteExternalUrl({
    String url,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.baseUrl = "";
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.delete("$url", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> getExternalUrl({
    String url,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.baseUrl = "";
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.get("$url").timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> postExternalUrl({
    String url,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.baseUrl = "";
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.post("$url", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }

  @override
  Future<Response> putExternalUrl({
    String url,
    body,
    dynamic headers,
    int timeout,
  }) async {
    dioClient.options.baseUrl = "";
    dioClient.options.headers =
        headers == null ? await UserAgent.headers() : headers;
    final result = await dioClient.put("$url", data: body).timeout(
          Duration(seconds: timeout != null ? timeout : 30),
          onTimeout: () => throw Failure(
            type: FailureType.timeoutConnection,
            message: "Request Failed, Connection Timeout!",
          ),
        );
    return result;
  }
}
