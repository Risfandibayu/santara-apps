import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:meta/meta.dart';

import '../tools/logger.dart';

abstract class BiometricApi {
  Future<bool> isBiometricAvailable();
  Future<List<BiometricType>> getListOfBiometricTypes();
  Future<bool> authenticateUser();
}

class BiometricApiImpl implements BiometricApi {
  final LocalAuthentication localAuthentication;
  BiometricApiImpl({@required this.localAuthentication});

  @override
  Future<List<BiometricType>> getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e, stack) {
      santaraLog(e, stack);
      listOfBiometrics = null;
    }
    return listOfBiometrics;
  }

  @override
  Future<bool> isBiometricAvailable() async {
    bool isAvailable = false;

    try {
      isAvailable = await localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e, stack) {
      santaraLog(e, stack);
      return false;
    }

    return isAvailable;
  }

  @override
  Future<bool> authenticateUser() async {
    bool isAuthenticated = false;

    try {
      isAuthenticated = await localAuthentication.authenticateWithBiometrics(
        localizedReason:
            "Letakkan sidik jari di perangkat Anda untuk melanjutkan",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e, stack) {
      santaraLog(e, stack);
      return false;
    }

    return isAuthenticated;
  }
}
