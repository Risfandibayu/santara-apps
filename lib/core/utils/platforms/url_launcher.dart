import 'package:meta/meta.dart';
import 'package:url_launcher/url_launcher.dart';

import '../tools/logger.dart';

class UrlLauncher {
  static Future open({@required String url}) async {
    try {
      if (await canLaunch(url)) {
        await launch(
          url,
          forceSafariVC: false,
          forceWebView: false,
        );
      } else {
        throw 'Could not launch $url';
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }
}
