import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:flutter_user_agent/flutter_user_agent.dart';
import 'package:santaraapp/core/utils/constants/app_constants.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';

class UserAgent {
  static Future<Map<String, dynamic>> headers() async {
    final storage = secureStorage.FlutterSecureStorage();
    try {
      await FlutterUserAgent.init();
      final userAgent = await FlutterUserAgent.getPropertyAsync('userAgent');
      final appVersion =
          await FlutterUserAgent.getProperty('applicationVersion');
      // check if user has token or not
      final token = await storage.read(key: USER_BEARER_TOKEN);
      if (token != null) {
        final headers = {
          HttpHeaders.authorizationHeader: "Bearer $token",
          HttpHeaders.userAgentHeader: "$userAgent",
          "App-Version": "$appVersion",
        };
        return headers;
      } else {
        final headers = {
          HttpHeaders.userAgentHeader: "$userAgent",
          "App-Version": "$appVersion",
        };
        return headers;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      final token = await storage.read(key: USER_BEARER_TOKEN);
      final userAgent = await FlutterUserAgent.getPropertyAsync('userAgent');
      final appVersion =
          await FlutterUserAgent.getProperty('applicationVersion');
      final headers = {
        HttpHeaders.authorizationHeader: "Bearer $token",
        HttpHeaders.userAgentHeader: "$userAgent",
        "App-Version": "$appVersion",
      };
      return headers;
    }
  }
}
