import 'package:intl/intl.dart';

class CurrencyFormatter {
  static String format(num value) {
    final rupiah = new NumberFormat("#,###");
    return "Rp ${rupiah.format(value)}";
  }
}
