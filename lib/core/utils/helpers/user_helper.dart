class UserHelper {
  static bool check = false; // is authenticated
  static String token = ''; // bearer token
  static String username = "Guest"; // username
  static String email = "guest@gmail.com"; // email
  static int userId;
  static String refreshToken;
  static int notif = 0;
  static int reqResetPassword = 0;
  static final bool useBottomMenu = false;
  static int stackViewDividen = 0;
}
