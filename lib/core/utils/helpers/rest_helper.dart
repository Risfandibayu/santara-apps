import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../error/failure.dart';
import '../constants/error_message_constants.dart';
import '../tools/logger.dart';

class RestHelper {
  static Failure catchFailure({@required DioError error}) {
    var response = error.response;
    logger.i(">> Catch Failure Result << \n${error.response.data}");
    switch (response.statusCode) {
      case 401:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : UNAUTHORIZED,
          data: response.data,
        );
        break;
      case 403:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : FORBIDDEN,
          data: response.data,
        );
        break;
      case 404:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : NOT_FOUND,
          data: response.data,
        );
        break;
      case 405:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : METHOD_NOT_ALLOWED,
          data: response.data,
        );
        break;
      case 500:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : SERVER_ERROR,
          data: response.data,
        );
        break;
      case 501:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : NOT_IMPLEMENTED,
          data: response.data,
        );
        break;
      case 502:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : BAD_GATEWAY,
          data: response.data,
        );
        break;
      case 503:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : SERVER_UNAVAILABLE,
          data: response.data,
        );
        break;
      case 504:
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] != null
              ? response.data["message"]
              : GATEWAY_TIMEOUT,
          data: response.data,
        );
        break;
      default:
        logger.e("${response.data}");
        return Failure(
          type: FailureType.serverError,
          apiStatus: response.statusCode,
          message: response.data["message"] ?? response.statusMessage,
          data: response.data,
        );
        break;
    }
  }

  static Failure throwServerError(Response response) {
    logger
        .e(">> Throw Server Error << \nStatus Code : ${response.statusCode}\n");
    return Failure(
      type: FailureType.serverError,
      apiStatus: response.statusCode,
      message: ERROR_OCCURED,
    );
  }

  static Failure throwLocalError({dynamic error, StackTrace stack}) {
    santaraLog(error, stack);
    return Failure(
      type: FailureType.localError,
      message: LOCAL_ERROR,
    );
  }

  static Failure throwFailure(Failure error, {StackTrace stack}) {
    santaraLog(error, stack);
    return Failure(
      type: error.type,
      message: "${error.message}",
    );
  }
}
