import 'package:flutter/material.dart';

import '../ui/screen_sizes.dart';
import 'colors.dart';

class SantaraTextStyle {
  static TextStyle titleStyle =
      TextStyle(color: SantaraColor.black, fontSize: Sizes.s18);
  static TextStyle linkStyle =
      TextStyle(color: SantaraColor.blueLink, fontSize: Sizes.s15);
}
