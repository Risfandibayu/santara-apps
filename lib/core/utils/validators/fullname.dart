import 'package:formz/formz.dart';

enum FullnameValidationError { empty, invalidFormat }

class Fullname extends FormzInput<String, FullnameValidationError> {
  const Fullname.pure() : super.pure('');
  const Fullname.dirty([String value = '']) : super.dirty(value);

  @override
  FullnameValidationError validator(String value) {
    if (value?.isNotEmpty == true) {
      if (RegExp(r"^[a-zA-Z\ ]*$").hasMatch(value)) {
        return null;
      } else {
        return FullnameValidationError.invalidFormat;
      }
    } else {
      return FullnameValidationError.empty;
    }
  }

  static String mapErrorFieldToString(FullnameValidationError error) {
    switch (error) {
      case FullnameValidationError.empty:
        return "Nama tidak boleh kosong!";
        break;
      case FullnameValidationError.invalidFormat:
        return "Mohon masukan nama yang valid!";
      default:
        return null;
    }
  }
}
