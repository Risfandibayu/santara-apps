import 'package:formz/formz.dart';

enum EmailValidationError { empty, invalidFormat }

class Email extends FormzInput<String, EmailValidationError> {
  const Email.pure() : super.pure('');
  const Email.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError validator(String value) {
    if (value?.isNotEmpty == true) {
      if (RegExp(
              r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
          .hasMatch(value)) {
        return null;
      } else {
        return EmailValidationError.invalidFormat;
      }
    } else {
      return EmailValidationError.empty;
    }
  }

  static String mapErrorFieldToString(EmailValidationError error) {
    switch (error) {
      case EmailValidationError.empty:
        return "Email tidak boleh kosong!";
        break;
      case EmailValidationError.invalidFormat:
        return "Format email yang anda masukan salah";
      default:
        return null;
    }
  }
}
