import 'package:formz/formz.dart';

enum PhoneNumberValidationError {
  empty,
  invalidFormat,
  invalidLength,
  invalidStartWith,
}

class PhoneNumber extends FormzInput<String, PhoneNumberValidationError> {
  const PhoneNumber.pure() : super.pure('');
  const PhoneNumber.dirty([String value = '']) : super.dirty(value);

  @override
  PhoneNumberValidationError validator(String value) {
    if (value?.isNotEmpty == true) {
      if (!RegExp(r"^[0-9]*$").hasMatch(value)) {
        return PhoneNumberValidationError.invalidFormat;
      } else if (value[0] == "0") {
        return PhoneNumberValidationError.invalidStartWith;
      } else if (value.length < 7) {
        return PhoneNumberValidationError.invalidLength;
      } else {
        return null;
      }
    } else {
      return PhoneNumberValidationError.empty;
    }
  }

  static String mapErrorFieldToString(PhoneNumberValidationError error) {
    switch (error) {
      case PhoneNumberValidationError.empty:
        return "Nomor Telepon tidak boleh kosong!";
        break;
      case PhoneNumberValidationError.invalidFormat:
        return "Mohon masukan nama tanpa simbol!";
        break;
      case PhoneNumberValidationError.invalidLength:
        return "Nomor telepon harus lebih dari 7 digit!";
        break;
      case PhoneNumberValidationError.invalidStartWith:
        return "Nomor telepon tidak boleh diawali dengan 0!";
        break;
      default:
        return null;
        break;
    }
  }
}
