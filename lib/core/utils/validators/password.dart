import 'package:formz/formz.dart';

enum PasswordValidationError {
  empty,
  invalidLength,
  invalidLowerCase,
  invalidUpperCase,
  invalidNumber,
  invalidSpecialChars,
  passwordNotMatch,
}

class Password extends FormzInput<String, PasswordValidationError> {
  const Password.pure(this.passVal) : super.pure('');
  const Password.dirty(this.passVal, [String value = '']) : super.dirty(value);

  final String passVal;

  @override
  PasswordValidationError validator(String value) {
    if (value?.isNotEmpty == true) {
      if (value.isEmpty) {
        return PasswordValidationError.empty;
      } else if (value.length < 8) {
        return PasswordValidationError.invalidLength;
      } else if (!RegExp(r"^(?=.*[a-z])").hasMatch(value)) {
        return PasswordValidationError.invalidLowerCase;
      } else if (!RegExp(r"^(?=.*[A-Z])").hasMatch(value)) {
        return PasswordValidationError.invalidUpperCase;
      } else if (!RegExp(r"^(?=.*[0-9])").hasMatch(value)) {
        return PasswordValidationError.invalidNumber;
      } else if (!RegExp(r"^(?=.*[^\w\s])").hasMatch(value)) {
        return PasswordValidationError.invalidSpecialChars;
      } else {
        if (passVal != null) {
          if (value != passVal) {
            return PasswordValidationError.passwordNotMatch;
          } else {
            return null;
          }
        } else {
          return null;
        }
      }
    } else {
      return PasswordValidationError.empty;
    }
  }

  static String mapErrorFieldToString(PasswordValidationError error) {
    switch (error) {
      case PasswordValidationError.empty:
        return "Password tidak boleh kosong!";
        break;
      case PasswordValidationError.invalidLength:
        return "Password minimal 8 karakter!";
      case PasswordValidationError.invalidLowerCase:
        return "Password harus mengandung huruf kecil!";
      case PasswordValidationError.invalidNumber:
        return "Password harus mengandung angka!";
      case PasswordValidationError.invalidSpecialChars:
        return "Password harus mengandung simbol!";
      case PasswordValidationError.invalidUpperCase:
        return "Password harus mengandung huruf besar!";
      case PasswordValidationError.passwordNotMatch:
        return "Password tidak cocok!";
      default:
        return null;
    }
  }
}
