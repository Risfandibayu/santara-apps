import 'package:formz/formz.dart';

enum NotEmptyStringValidator { empty, invalidFormat }

class NotEmptyString extends FormzInput<String, NotEmptyStringValidator> {
  const NotEmptyString.pure() : super.pure('');
  const NotEmptyString.dirty([String value = '']) : super.dirty(value);

  @override
  NotEmptyStringValidator validator(String value) {
    if (value?.isNotEmpty == true) {
      return null;
    } else {
      return NotEmptyStringValidator.empty;
    }
  }

  static String mapErrorFieldToString(NotEmptyStringValidator error) {
    switch (error) {
      case NotEmptyStringValidator.empty:
        return "Password tidak boleh kosong!";
        break;
      default:
        return null;
    }
  }
}
