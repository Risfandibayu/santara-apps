import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/core/http/rest_client_impl.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';

class UnauthorizeUsecase {
  static Future check(BuildContext context) async {
    final dio = Dio();
    final client = RestClientImpl(dioClient: dio);
    try {
      final result = await client.get(path: "/auth/check-auth");
      if (result.statusCode == 401) {
        NavigatorHelper.pushToExpiredSession(context);
      }
    } on DioError catch (e) {
      if (e.response.statusCode == 401) {
        NavigatorHelper.pushToExpiredSession(context);
      }
    } catch (_) {
      logger.i(_);
    }
  }
}
