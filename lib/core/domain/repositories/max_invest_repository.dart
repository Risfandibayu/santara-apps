import 'package:dartz/dartz.dart';

import '../../data/models/max_invest_model.dart';
import '../../error/failure.dart';

abstract class MaxInvestRepository {
  Future<Either<Failure, MaxInvestModel>> getMaxInvest();
}
