import 'package:dartz/dartz.dart';

import '../../data/models/fee_model.dart';
import '../../error/failure.dart';

abstract class FeeRepository {
  Future<Either<Failure, FeeModel>> getAllFee();
}
