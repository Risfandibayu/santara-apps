import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import '../../data/models/user_model.dart';
import '../../error/failure.dart';

abstract class UserRepository {
  Future<Either<Failure, User>> getUserByUuid({@required String uuid});
  // Future<Either<Failure, LoginResultModel>> getUserLocal();
  // Future<Either<Failure, void>> saveLoginData({
  //   @required LoginResultModel data,
  // });
  // Future<Either<Failure, void>> clearStorageData();
  // Future<Either<Failure, Token>> getToken();
  // Future<Either<Failure, void>> logoutAccount({
  //   @required String refreshToken,
  //   @required String id,
  // });
}
