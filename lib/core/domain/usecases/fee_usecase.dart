import 'package:dartz/dartz.dart';

import '../../data/models/fee_model.dart';
import '../../error/failure.dart';
import '../../usecases/usecase.dart';
import '../repositories/fee_repository.dart';

class GetAllFee implements UseCase<FeeModel, NoParams> {
  final FeeRepository repository;
  GetAllFee(this.repository);

  @override
  Future<Either<Failure, FeeModel>> call(NoParams params) async {
    return await repository.getAllFee();
  }
}
