import 'package:dartz/dartz.dart';

import '../../data/models/user_model.dart';
import '../../error/failure.dart';
import '../../usecases/usecase.dart';
import '../repositories/user_repository.dart';

class GetUserByUuid implements UseCase<User, String> {
  final UserRepository repository;
  GetUserByUuid(this.repository);

  @override
  Future<Either<Failure, User>> call(String uuid) async {
    return await repository.getUserByUuid(uuid: uuid);
  }
}

// class GetUserLocal implements UseCase<LoginResultModel, NoParams> {
//   final UserRepository repository;
//   GetUserLocal(this.repository);

//   @override
//   Future<Either<Failure, LoginResultModel>> call(NoParams params) async {
//     return await repository.getUserLocal();
//   }
// }

// class ClearStorageData implements UseCase<void, NoParams> {
//   final UserRepository repository;
//   ClearStorageData(this.repository);

//   @override
//   Future<Either<Failure, void>> call(NoParams uuid) async {
//     return await repository.clearStorageData();
//   }
// }

// class SaveLoginData implements UseCase<void, LoginResultModel> {
//   final UserRepository repository;
//   SaveLoginData(this.repository);

//   @override
//   Future<Either<Failure, void>> call(LoginResultModel data) async {
//     return await repository.saveLoginData(data: data);
//   }
// }

// class GetToken implements UseCase<Token, NoParams> {
//   final UserRepository repository;
//   GetToken(this.repository);

//   @override
//   Future<Either<Failure, Token>> call(NoParams data) async {
//     return await repository.getToken();
//   }
// }

// class LogoutAccountUser implements UseCase<void, Map<String, String>> {
//   final UserRepository repository;
//   LogoutAccountUser(this.repository);

//   @override
//   Future<Either<Failure, void>> call(Map<String, String> params) async {
//     return await repository.logoutAccount(
//       refreshToken: params["refresh_token"],
//       id: params["id"],
//     );
//   }
// }
