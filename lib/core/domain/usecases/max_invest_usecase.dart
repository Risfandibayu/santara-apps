import 'package:dartz/dartz.dart';

import '../../data/models/max_invest_model.dart';
import '../../error/failure.dart';
import '../../usecases/usecase.dart';
import '../repositories/max_invest_repository.dart';

class GetMaxInvest implements UseCase<MaxInvestModel, NoParams> {
  final MaxInvestRepository repository;
  GetMaxInvest(this.repository);

  @override
  Future<Either<Failure, MaxInvestModel>> call(NoParams params) async {
    return await repository.getMaxInvest();
  }
}
