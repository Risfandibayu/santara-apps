import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

enum FailureType {
  noConnection,
  timeoutConnection,
  serverError,
  localError,
  unknownError
}

class Failure extends Equatable {
  Failure({
    this.type,
    this.message,
    this.apiStatus,
    this.data,
  });

  final FailureType type;
  final String message;
  final int apiStatus;
  final dynamic data; // if an error should pass any data, put it here

  @override
  List<Object> get props => [type, message, apiStatus, data];
}

// General Failures
class ServerFailure extends Failure {
  final String message;
  ServerFailure({@required this.message});

  @override
  List<Object> get props => [message];
}

// No Connection
class NoConnection extends Failure {
  @override
  List<Object> get props => [];
}

// Local Failures
class LocalFailure extends Failure {
  final String message;
  LocalFailure({@required this.message});

  @override
  List<Object> get props => [message];
}

// Auth Failure
class AuthFailure extends Failure {
  final String message;
  AuthFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
