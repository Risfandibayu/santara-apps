import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class SearchBottomSheet extends StatefulWidget {
  final List<dynamic> items;
  final dynamic selectedItem;
  final String title;
  final String hintText;
  final bool showSearch;

  SearchBottomSheet({
    @required this.items,
    this.selectedItem,
    this.title,
    this.hintText,
    this.showSearch = true,
  });

  @override
  _SearchBottomSheetState createState() =>
      _SearchBottomSheetState(items, selectedItem, title, hintText, showSearch);
}

class _SearchBottomSheetState extends State<SearchBottomSheet> {
  List<dynamic> items;
  List<dynamic> tempItems;
  dynamic selectedItem;
  String title;
  String hintText;
  bool showSearch;
  final TextEditingController textController = TextEditingController();
  final ScrollController _scrollController = ScrollController();

  bool showShade = true;
  double shadeHeight = 150.0;

  _SearchBottomSheetState(
    this.items,
    this.selectedItem,
    this.title,
    this.hintText,
    this.showSearch,
  );

  Widget _showBottomSheetWithSearch(int index, List<dynamic> items) {
    return Text(
      items[index].name,
      style: TextStyle(
        color: Colors.black,
        fontSize: FontSize.s14,
        fontWeight: FontWeight.w600,
      ),
      textAlign: TextAlign.left,
    );
  }

  List<dynamic> _buildSearchList(String query) {
    var item = items
        .where((e) => e.name.toLowerCase().contains(query.toLowerCase()))
        .toList();
    return item;
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent) {
        if (showShade) {
          setState(() {
            showShade = false;
            shadeHeight = 0.0;
          });
        }
      } else {
        if (!showShade) {
          setState(() {
            showShade = true;
            shadeHeight = 150.0;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              title != null && title.isNotEmpty
                  ? Padding(
                      padding: EdgeInsets.only(top: Sizes.s40),
                      child: Center(
                        child: Text(
                          "$title",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: FontSize.s18,
                          ),
                        ),
                      ),
                    )
                  : Container(),
              Padding(
                padding: EdgeInsets.all(Sizes.s20),
                child: showSearch ?? true
                    ? TextField(
                        controller: textController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                              left: Sizes.s20,
                              top: Sizes.s10,
                              bottom: Sizes.s10),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(Sizes.s25),
                            borderSide:
                                BorderSide(color: Colors.grey[300], width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(Sizes.s25),
                            borderSide:
                                BorderSide(color: Colors.grey[300], width: 1.0),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(Sizes.s25),
                            borderSide:
                                BorderSide(color: Colors.grey[300], width: 1.0),
                          ),
                          hintText: '$hintText',
                          hintStyle: TextStyle(
                              fontSize: FontSize.s14, color: Color(0xffdadada)),
                          filled: true,
                          suffixIcon: Container(
                            margin: EdgeInsets.only(right: Sizes.s20),
                            child: Icon(
                              Icons.search,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        onChanged: (value) => setState(
                          () => tempItems = _buildSearchList(value),
                        ),
                      )
                    : Container(),
              ),
              Container(
                height: Sizes.s350,
                child: Stack(
                  children: [
                    tempItems != null && tempItems.length < 1
                        ? Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(height: Sizes.s10),
                                  Image.asset(
                                    "assets/icon/no-result.png",
                                    width: Sizes.s150,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical: Sizes.s15,
                                    ),
                                    child: Text(
                                      "Data Tidak Ditemukan",
                                      style: TextStyle(
                                        fontSize: FontSize.s18,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : ListView.builder(
                            controller: _scrollController,
                            physics: BouncingScrollPhysics(),
                            itemCount: (tempItems != null)
                                ? tempItems.length
                                : items.length,
                            itemBuilder: (context, index) {
                              var datax =
                                  tempItems != null && tempItems.length > 0
                                      ? tempItems[index]
                                      : items[index];
                              return ListTile(
                                selected: selectedItem != null &&
                                        selectedItem == datax
                                    ? true
                                    : false,
                                selectedTileColor: Colors.grey.withOpacity(.3),
                                contentPadding: EdgeInsets.only(
                                  left: Sizes.s20,
                                  right: Sizes.s20,
                                ),
                                //6
                                title: (tempItems != null &&
                                        tempItems.length > 0)
                                    ? _showBottomSheetWithSearch(
                                        index, tempItems)
                                    : _showBottomSheetWithSearch(index, items),
                                onTap: () {
                                  setState(() => selectedItem = datax);
                                  FocusScope.of(context).unfocus();
                                },
                              );
                            },
                          ),
                    items != null && items.length > 6
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: IgnorePointer(
                              child: AnimatedContainer(
                                curve: Curves.bounceInOut,
                                duration: Duration(milliseconds: 200),
                                height: shadeHeight,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Colors.white.withOpacity(.2),
                                      Colors.white.withOpacity(.8)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              Container(
                height: Sizes.s100,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.white.withOpacity(.8),
                      Colors.white.withOpacity(.2),
                    ],
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(height: Sizes.s30),
                    Container(
                      height: Sizes.s45,
                      padding:
                          EdgeInsets.only(left: Sizes.s25, right: Sizes.s25),
                      width: double.maxFinite,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(Sizes.s10),
                        child: FlatButton(
                          color: Color(0xffBF2D30),
                          disabledColor: Colors.grey,
                          onPressed: selectedItem != null
                              ? () => Navigator.pop(context, selectedItem)
                              : null,
                          child: Text(
                            "Pilih",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
