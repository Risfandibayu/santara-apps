import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class MessageBottomSheet extends StatelessWidget {
  final String title;
  final String message;
  MessageBottomSheet({
    this.title,
    this.message,
  });

  static void show(
    BuildContext context, {
    Key key,
    String title,
    String message,
  }) =>
      showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(5.0)),
        ),
        context: context,
        builder: (context) {
          return MessageBottomSheet(
            title: title,
            message: message,
          );
        },
      ).then((_) => FocusScope.of(context).requestFocus(FocusNode()));

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Sizes.s30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: Sizes.s20),
          Text(
            "$title",
            style: TextStyle(
              fontSize: FontSize.s18,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "$message",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: Sizes.s40),
          SantaraMainButton(
            title: "Tutup",
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
    );
  }
}
