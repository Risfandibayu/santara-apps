import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/message_bottom_sheet/message_bottom_sheet.dart';
import 'package:santaraapp/utils/sizes.dart';

class PralistingInfoButton extends StatelessWidget {
  final String title;
  final String message;
  PralistingInfoButton({@required this.title, @required this.message});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.info,
        color: Colors.grey[300],
        size: Sizes.s25,
      ),
      onPressed: () {
        MessageBottomSheet.show(
          context,
          title: "$title",
          message: "$message",
        );
      },
    );
  }
}
