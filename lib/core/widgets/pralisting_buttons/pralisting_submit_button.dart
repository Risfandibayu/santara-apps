import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class PralistingSubmitButton extends StatelessWidget {
  final String nextKey;
  final String saveKey;
  final Function onTapNext;
  final Function onTapSave;

  PralistingSubmitButton({
    @required this.nextKey,
    @required this.saveKey,
    @required this.onTapNext,
    @required this.onTapSave,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: Sizes.s60),
        ClipRRect(
          // borderRadius: BorderRadius.circular(10),
          child: SantaraMainButton(
            key: Key('$nextKey'),
            onPressed: onTapNext,
            title: "Selanjutnya",
          ),
        ),
        SizedBox(height: Sizes.s10),
        ClipRRect(
          // borderRadius: BorderRadius.circular(10),
          child: SantaraOutlineButton(
            key: Key('$saveKey'),
            onPressed: onTapSave,
            title: "Simpan & Lanjutkan Nanti",
          ),
        ),
      ],
    );
  }
}
