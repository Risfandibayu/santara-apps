import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/core/data/models/file_data_model.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/image_preview_page.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'bloc.dart';

class ImageUploadSubmission extends StatelessWidget {
  final String label;
  final String description;
  final String path;
  final String name;
  final String type;
  final String location;
  final Widget annotation;
  final List<FileUploadModel> files; // uploaded files
  final int maxFiles; // maximum files to upload
  final int minFiles; // minimum files to upload
  final int maxFileSize;
  final SingleFieldBloc fieldBloc;
  final Function onTapInfo;

  ImageUploadSubmission({
    this.label = '',
    this.description = '',
    this.annotation,
    this.files,
    this.maxFileSize,
    @required this.path,
    @required this.name,
    @required this.type,
    @required this.location,
    @required this.maxFiles,
    @required this.minFiles,
    @required this.fieldBloc,
    @required this.onTapInfo,
  });

  Widget _imagePicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              "$label",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w600,
              ),
            ),
            onTapInfo != null
                ? IconButton(
                    icon: Icon(
                      Icons.info,
                      color: Color(0xffDDDDDD),
                    ),
                    onPressed: onTapInfo,
                  )
                : Container()
          ],
        ),
        SizedBox(height: Sizes.s10),
        DottedBorder(
          color: Color(0xFFB8B8B8),
          strokeWidth: 2,
          radius: Radius.circular(Sizes.s25),
          dashPattern: [5, 5],
          child: Container(
            padding: EdgeInsets.only(top: Sizes.s10, bottom: Sizes.s10),
            width: double.maxFinite,
            decoration: BoxDecoration(
              color: Color(0xffF4F4F4),
            ),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(width: Sizes.s10),
                //
                DottedBorder(
                  color: Color(0xFFB8B8B8),
                  strokeWidth: 2,
                  radius: Radius.circular(Sizes.s25),
                  dashPattern: [5, 5],
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(height: Sizes.s10),
                        Icon(
                          Icons.camera_alt_outlined,
                          color: Color(0xffB8B8B8),
                          size: Sizes.s30,
                        ),
                        SizedBox(height: Sizes.s5),
                        Container(
                          padding: EdgeInsets.only(
                            left: Sizes.s15,
                            right: Sizes.s15,
                          ),
                          margin: EdgeInsets.only(
                            left: Sizes.s10,
                            right: Sizes.s10,
                          ),
                          height: Sizes.s30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Sizes.s5),
                            color: Color(0xff6870E1),
                          ),
                          child: Center(
                            child: Text(
                              "Unggah Foto",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: Sizes.s10),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: Sizes.s20),
                Flexible(
                  child: Text(
                    "$description",
                    style: TextStyle(
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
                SizedBox(
                  width: Sizes.s10,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _progress({
    @required InputFieldBloc bloc,
    @required VoidCallback onCancel,
    @required VoidCallback onDelete,
    @required VoidCallback onRetry,
    @required double width,
    BuildContext context,
  }) {
    FileDataModel result =
        bloc.state.extraData != null ? bloc.state.extraData : null;
    if (result != null && result.state != FileUploadState.uninitialized) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(Sizes.s5),
        child: Stack(
          children: [
            result?.url != null && result.url.isNotEmpty
                ? SantaraCachedImage(
                    image: "${result.url}",
                    fit: BoxFit.cover,
                    width: double.maxFinite,
                    height: double.maxFinite,
                  )
                : result?.fileName != null
                    ? Container(
                        alignment: Alignment.center,
                        child: Image.file(
                          File(result?.fileName),
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                          height: double.maxFinite,
                        ),
                      )
                    : Container(),
            result?.state != FileUploadState.success
                ? Container(
                    color: Colors.black.withOpacity(.5),
                    width: double.maxFinite,
                    height: double.maxFinite,
                  )
                : Container(),
            result?.state == FileUploadState.error
                ? InkWell(
                    onTap: onRetry,
                    onLongPress: onDelete,
                    child: Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      padding: EdgeInsets.all(Sizes.s8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(),
                          Text(
                            "Gagal Mengunggah",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              fontWeight: FontWeight.w400,
                              color: Colors.red,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Icon(
                            Icons.replay_outlined,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
            result?.state == FileUploadState.uploading
                ? Container(
                    height: double.maxFinite,
                    width: double.maxFinite,
                    padding: EdgeInsets.all(Sizes.s8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(),
                        Text(
                          "Mengunggah..",
                          style: TextStyle(
                              fontSize: FontSize.s12,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                        LinearPercentIndicator(
                          width: width < 600 ? Sizes.s100 : width / 3.5,
                          lineHeight: Sizes.s10,
                          percent: result.state == FileUploadState.uploading
                              ? result.progress
                              : result.state == FileUploadState.success
                                  ? 1.0
                                  : 0.0,
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          backgroundColor: Colors.grey,
                          progressColor: Color(0xff6870E1),
                        )
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  void showSnackbar(BuildContext context, String message) {
    ToastHelper.showBasicToast(context, message);
    // Scaffold.of(context).showSnackBar(
    //   SnackBar(
    //     content: SingleChildScrollView(
    //       child: Text(
    //         "$message",
    //       ),
    //     ),
    //     duration: Duration(milliseconds: 1500),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ImageUploadBloc(
        name: name,
        path: path,
        type: type,
        location: location,
        uploadedFiles: files,
        maximumFiles: maxFiles,
        minimumFiles: minFiles,
        fieldBloc: fieldBloc,
      ),
      child: Builder(
        builder: (context) {
          // ignore: ImageUploadBloc
          final bloc = context.bloc<ImageUploadBloc>();
          return FormBlocListener<ImageUploadBloc, String, String>(
            onSubmitting: (context, state) {},
            onSuccess: (context, state) async {
              if (state.successResponse.isNotEmpty) {
                showSnackbar(context, "${state.successResponse}");
              }
              // await Future.delayed(Duration(milliseconds: 500));
              // onFinished(bloc.isDoneAll());
            },
            onFailure: (context, state) {
              showSnackbar(context, "${state.failureResponse}");
            },
            child: BlocBuilder<ImageUploadBloc, FormBlocState>(
                builder: (context, state) {
              bool canUpload = state is FormBlocSubmitting ? false : true;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 5),
                  InkWell(
                    onTap: canUpload
                        ? () async {
                            List<String> allowedExtensions = [
                              'jpg',
                              'jpeg',
                              'png'
                            ];
                            try {
                              bool show = false; // show loading dialog
                              int countFilesOnState = 0;
                              // Cek apakah pickedFiles sudah ada filenya
                              if (bloc
                                  .pickedFiles.state.fieldBlocs.isNotEmpty) {
                                countFilesOnState =
                                    bloc.pickedFiles.state.fieldBlocs.length;
                              }
                              // end
                              FilePickerResult result =
                                  await FilePicker.platform.pickFiles(
                                allowMultiple: true,
                                type: FileType.custom,
                                allowedExtensions: allowedExtensions,
                                onFileLoading: (status) {
                                  // file picker caching file to storage
                                  if (status == FilePickerStatus.picking) {
                                    show = true;
                                    PopupHelper.showLoading(context);
                                  }

                                  if (status == FilePickerStatus.done) {
                                    if (show) {
                                      Navigator.pop(context);
                                    }
                                  }
                                },
                              );

                              if (result != null) {
                                List<File> files = result.paths
                                    .map((path) => File(path))
                                    .toList();

                                if (maxFileSize != null &&
                                    files != null &&
                                    files.length > 0) {
                                  files.forEach((file) {
                                    int sizeInBytes = file.lengthSync();
                                    double sizeInMb =
                                        sizeInBytes / (1024 * 1024);
                                    if (sizeInMb > maxFileSize) {
                                      showSnackbar(
                                        context,
                                        "Maksimal ukuran file yang dapat dipilih adalah $maxFileSize MB",
                                      );
                                      files.remove(file);
                                    }
                                  });
                                }

                                if (countFilesOnState != null &&
                                    files.length >
                                        (maxFiles - countFilesOnState)) {
                                  showSnackbar(
                                    context,
                                    "Jumlah maksimal file yang dapat dipilih adalah ${(maxFiles - countFilesOnState)} file",
                                  );
                                  await FilePicker.platform
                                      .clearTemporaryFiles();
                                } else {
                                  files.forEach((element) {
                                    String _fileExtension =
                                        KycHelpers.getfiletype(element.path);
                                    // Check File Extensions
                                    if (allowedExtensions
                                        .any((ext) => ext == _fileExtension)) {
                                      bloc.addFile(element);
                                    } else {
                                      showSnackbar(
                                        context,
                                        "Format file yang anda pilih tidak sesuai!",
                                      );
                                    }
                                  });
                                  bloc.submit();
                                }
                              }
                            } catch (e, stack) {
                              print(e);
                              print(stack);
                            }
                          }
                        : null,
                    child: _imagePicker(),
                  ),
                  SizedBox(height: 15),
                  BlocBuilder<ListFieldBloc, ListFieldBlocState>(
                    bloc: bloc.pickedFiles,
                    builder: (context, state) {
                      if (state.fieldBlocs.isNotEmpty) {
                        return GridView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: state.fieldBlocs.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 8,
                            mainAxisSpacing: 8,
                            childAspectRatio: 1.4,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            var width = MediaQuery.of(context).size.width;
                            // ignore: close_sinks
                            var currentBloc =
                                state.fieldBlocs[index] as InputFieldBloc;
                            return InkWell(
                              onTap: () async {
                                FileDataModel image =
                                    currentBloc.state.extraData != null
                                        ? currentBloc.state.extraData
                                        : null;
                                final nav = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ImagePreviewPage(
                                      image: image.url,
                                    ),
                                  ),
                                );
                                if (nav != null && nav) {
                                  bloc.deleteFile(index);
                                }
                              },
                              child: _progress(
                                bloc: currentBloc,
                                width: width,
                                onCancel: () => bloc.cancelUpload(index),
                                onDelete: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      FileDataModel data =
                                          currentBloc?.state?.extraData;
                                      return AlertDialog(
                                        title: Text(
                                            "Hapus ${data?.fileName ?? ''}?"),
                                        content: Text(
                                            "File ini akan dihapus dari server, anda mungkin perlu mengunggahnya kembali."),
                                        actions: [
                                          FlatButton(
                                            child: Text(
                                              "Hapus",
                                              style: TextStyle(
                                                color: Colors.red,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              bloc.deleteFile(index);
                                            },
                                          ),
                                          FlatButton(
                                            child: Text(
                                              "Batalkan",
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                                onRetry: () async =>
                                    bloc.retryUploadFile(index),
                                context: context,
                              ),
                            );
                          },
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  !fieldBloc.state.isInitial
                      ? fieldBloc.state.hasError
                          ? Container(
                              margin: EdgeInsets.only(left: Sizes.s15),
                              child: Text(
                                "${fieldBloc.state.extraData != null && fieldBloc.state.extraData.length > 0 && fieldBloc.state.extraData.length < minFiles ? 'Data tidak lengkap' : fieldBloc.state.error}",
                                style: TextStyle(
                                  color: Colors.red[700],
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                            )
                          : Container()
                      : Container(),
                  SizedBox(height: Sizes.s10),
                  bloc.isDoneAll()
                      ? InkWell(
                          onTap: () {
                            print(fieldBloc.state.extraData);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: Sizes.s10),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.check_circle,
                                  color: Color(0xff6870E1),
                                  size: Sizes.s20,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  "Foto Lengkap",
                                  style: TextStyle(
                                    color: Color(0xff6870E1),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      : Container()
                ],
              );
            }),
          );
        },
      ),
    );
  }
}
