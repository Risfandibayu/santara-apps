import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/pages/pralisting_journey_page.dart';

class PralistingAppbar extends StatelessWidget {
  final String title;
  final Widget stepTitle;
  final bool showSteps;
  final bool centerTitle;
  final String uuid;

  PralistingAppbar({
    @required this.title,
    this.stepTitle,
    this.showSteps = true,
    this.centerTitle = false,
    @required this.uuid,
  });

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(50),
      child: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            stepTitle != null ? stepTitle : Container(),
            Text(
              "$title",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s18,
              ),
            ),
          ],
        ),
        centerTitle: centerTitle,
        actions: [
          showSteps
              ? IconButton(
                  icon: Icon(Icons.more_horiz),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PralistingJourneyPage(
                        uuid: "$uuid",
                      ),
                    ),
                  ),
                )
              : Container()
        ],
        elevation: 1.0,
        backgroundColor: Colors.black,
        iconTheme: IconThemeData(color: Colors.white),
      ),
    );
  }
}
