import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class BoxShimmer extends StatelessWidget {
  final double width;
  final double height;
  final double borderRadius;

  BoxShimmer({
    @required this.width,
    @required this.height,
    @required this.borderRadius,
  });

  @override
  Widget build(BuildContext context) {
    // "",
    return Container(
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.white,
        child: ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(borderRadius),
          ),
          child: Container(
            height: height,
            width: width,
            child: Image.asset(
              'assets/slide/responsive1.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
