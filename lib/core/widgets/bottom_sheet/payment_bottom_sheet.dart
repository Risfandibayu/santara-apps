import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import '../../utils/formatter/currency_formatter.dart';
import '../../utils/ui/screen_sizes.dart';
import '../../../widget/widget/components/main/SantaraButtons.dart';

class PaymentBottomSheet {
  static Widget _headerBottomSheet(BuildContext context, String title) {
    return Row(children: [
      GestureDetector(
        child: Icon(
          Icons.close,
          color: Colors.black,
        ),
        onTap: () => Navigator.pop(context),
      ),
      Container(width: 10),
      Text(
        title,
        style: TextStyle(
            fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    ]);
  }

  static get _divider {
    return Container(height: 2, color: Color(0xFFF0F0F0));
  }

  static Widget paymentItem(String title, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: Sizes.s8),
      child: Row(
        children: [
          Expanded(child: Text(title)),
          Text(":"),
          Expanded(child: Text(value, textAlign: TextAlign.end)),
        ],
      ),
    );
  }

  static String _buttonText(int channel) {
    switch (channel) {
      case 0:
        return "Gunakan DANA";
        break;
      case 1:
        return "Gunakan Dompet Santara";
        break;
      case 2:
        return "Gunakan Virtual Account";
        break;
      case 5:
        return "Gunakan OTHER PAYMENT";
      default:
        return "";
    }
  }

  static void confirm(BuildContext context,
      {String icon,
      String title,
      num saldo,
      num total,
      int channel, // 0: Dana, 1: Dompet, 2: Va
      Function onTap}) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // title
              _headerBottomSheet(context, "Konfirmasi Pembayaran"),
              SizedBox(height: Sizes.s16),

              // saldo
              ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: channel == 3
                      ? null
                      : Image.asset(icon,
                          height: channel == 2 ? 20 : 25,
                          fit: BoxFit.fitHeight),
                  title: Text(title,
                      style: TextStyle(
                          fontSize: channel == 3 ? FontSize.s15 : FontSize.s13,
                          fontWeight: FontWeight.bold)),
                  trailing: Text(
                      channel == 2 || channel == 3
                          ? ""
                          : CurrencyFormatter.format(saldo),
                      style: TextStyle(
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.bold))),
              _divider,

              // total
              ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text("Total Pembayaran",
                      style: TextStyle(
                          fontSize: FontSize.s13, fontWeight: FontWeight.bold)),
                  trailing: Text(CurrencyFormatter.format(total),
                      style: TextStyle(
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.bold))),

              _divider,

              SizedBox(height: Sizes.s16),
              Text(
                  "Apakah Anda yakin ingin menggunakan metode pembayaran ini ?",
                  style: TextStyle(fontSize: FontSize.s14)),
              SizedBox(height: Sizes.s16),

              // button
              SantaraMainButton(
                key: Key("submit"),
                color: channel == 0 ? Color(0xFF008CEB) : Color(0xFFBF2D30),
                title: _buttonText(channel),
                onPressed: () {
                  Navigator.pop(context);
                  onTap();
                },
              )
            ],
          ),
        );
      },
    );
  }

  static void paymentDetail(BuildContext context,
      {String name, String codeEmiten, num price, num amount, String total}) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // title
              _headerBottomSheet(context, "Detail Pembayaran"),
              SizedBox(height: Sizes.s16),

              paymentItem("Nama Saham", name),
              paymentItem("Kode Saham", codeEmiten),
              paymentItem("Harga Saham", CurrencyFormatter.format(price)),
              paymentItem("Jumlah Saham",
                  NumberFormatter.convertNumber(amount) + "Lembar"),
              SizedBox(height: Sizes.s10),
              _divider,
              ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text("Total Pembayaran",
                      style: TextStyle(
                          fontSize: FontSize.s13, fontWeight: FontWeight.bold)),
                  trailing: Text(CurrencyFormatter.format(num.parse(total)),
                      style: TextStyle(
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.bold))),
              _divider,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total Bayar",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s16,
                    ),
                  ),
                  Container(width: 10),
                  Row(
                    children: [
                      Text(
                        RupiahFormatter.initialValueFormat(total),
                        style: TextStyle(
                          color: Color(0xFF0E7E4A),
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s16,
                        ),
                      ),
                      IconButton(
                          icon: Icon(Icons.keyboard_arrow_down),
                          onPressed: () => Navigator.pop(context))
                    ],
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
