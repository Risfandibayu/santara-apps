import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';

class SantaraDropDownField extends StatelessWidget {
  final Function onTap;
  final Function onReload;
  final String labelText;
  final String hintText;
  final String value;
  final String errorText;
  final FieldState state;

  SantaraDropDownField({
    @required this.onTap,
    @required this.onReload,
    @required this.labelText,
    @required this.hintText,
    this.value,
    this.errorText,
    @required this.state,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      readOnly: true,
      onTap: onTap,
      decoration: InputDecoration(
        border: PralistingInputDecoration.outlineInputBorder,
        contentPadding: EdgeInsets.all(Sizes.s15),
        isDense: true,
        errorMaxLines: 5,
        labelText: "$labelText",
        labelStyle: PralistingInputDecoration.labelTextStyle,
        hintText: value != null ? value : hintText,
        hintStyle: PralistingInputDecoration.hintTextStyle(
          value != null && value.isNotEmpty,
        ),
        errorText: errorText,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: state == FieldState.loading
            ? CupertinoActivityIndicator()
            : state == FieldState.error
                ? IconButton(
                    icon: Icon(Icons.replay),
                    onPressed: onReload,
                  )
                : Icon(
                    Icons.arrow_drop_down,
                    color: Colors.black,
                  ),
      ),
    );
  }
}
