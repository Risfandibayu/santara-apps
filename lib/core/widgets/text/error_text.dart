import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class ErrorText extends StatelessWidget {
  final String message;
  ErrorText({@required this.message});

  @override
  Widget build(BuildContext context) {
    return message != null && message.isNotEmpty
        ? Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "$message",
              style: TextStyle(
                color: Colors.red[700],
                fontSize: FontSize.s12,
              ),
              maxLines: 5,
            ),
          )
        : Container();
  }
}
