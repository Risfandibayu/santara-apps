import 'package:flutter/material.dart';
import 'package:santaraapp/core/utils/ui/screen_sizes.dart';

class SantaraBulletList extends StatelessWidget {
  final Widget child;

  const SantaraBulletList({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(right: Sizes.s10, top: Sizes.s6),
          child: Icon(
            Icons.fiber_manual_record,
            size: Sizes.s8,
            color: Color(0xFF747474),
          ),
        ),
        Flexible(child: child),
      ],
    );
  }
}
