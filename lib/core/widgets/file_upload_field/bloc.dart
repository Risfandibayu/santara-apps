import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/file_data_model.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:path/path.dart' as filePath;
import 'package:http_parser/http_parser.dart';

class FileUploadBloc extends FormBloc<String, String> {
  final String name; // key name
  final String path; // path or url
  final String type; // images / documents
  final String location; // path upload location
  // final List<FileUploadModel> uploadedFiles; // uploaded files
  final int maximumFiles; // maximum selected files
  final int minimumFiles; // minimum uploaded files
  final SingleFieldBloc fieldBloc;
  final pickedFiles = ListFieldBloc<InputFieldBloc<File, Object>>();
  List<UploadFileProgress> _uploadFile = [];
  List<StreamSubscription<double>> _uploadProgressSubscription = [];

  FileUploadBloc({
    @required this.name,
    @required this.path,
    @required this.type,
    @required this.location,
    // this.uploadedFiles,
    @required this.maximumFiles,
    @required this.minimumFiles,
    @required this.fieldBloc,
  }) {
    addFieldBlocs(fieldBlocs: [pickedFiles]);
    initUploadedFiles();
  }

  bool isDoneAll() {
    bool isDone = false;
    if (pickedFiles.state.fieldBlocs.isNotEmpty &&
        pickedFiles.state.fieldBlocs.length >= minimumFiles) {
      pickedFiles.state.fieldBlocs.forEach((element) {
        FileDataModel data = element.state?.extraData;
        if (data != null && data.state == FileUploadState.success) {
          isDone = true;
        } else {
          isDone = false;
        }
      });
    } else {
      isDone = false;
    }
    return isDone;
  }

  // if uploaded files is not null
  void initUploadedFiles() {
    String errorMessage = fieldBloc.state.error;
    // print(">> ERROR MESSAGE : $errorMessage");
    if (fieldBloc.state.extraData != null &&
        fieldBloc.state.extraData.length > 0) {
      // if (fieldBloc.state.name == "business_certificate") {
      // print(">> ${fieldBloc.state.name}");
      // print(">> ${fieldBloc.state.extraData}");
      // }
      fieldBloc.state.extraData.asMap().forEach((index, element) {
        final data = FileDataModel(
          progress: 0.0,
          state: FileUploadState.success,
          fileName: element.filename,
          url: element.url,
        );
        _handleDoneUpload(data.url, data.fileName);
        // fieldBloc.updateExtraData(uploadedFiles);
        pickedFiles.addFieldBloc(InputFieldBloc(name: name, extraData: data));
        _uploadFile.add(
          UploadFileProgress(
            uploadUrl: '$path',
            userFile: null,
            name: name,
            type: type,
            location: location,
          ),
        );
        _uploadProgressSubscription.add(
          _uploadFile[index].uploadProgress.listen((event) {}),
        );
      });
      fieldBloc.updateValue("OK");
      if (errorMessage != null &&
          errorMessage.isNotEmpty &&
          errorMessage != "Data tidak boleh kosong") {
        fieldBloc.addFieldError(errorMessage);
      }
    }
  }

  // add file to bloc
  void addFile(File file) {
    try {
      pickedFiles.addFieldBloc(
        InputFieldBloc(
          name: name,
          initialValue: file,
        ),
      );
    } catch (e) {
      print(">> ERR $e");
    }
  }

  // delete file & Bloc
  void deleteFile(int index) {
    // remove from fieldBloc
    FileDataModel data = pickedFiles.value[index].state?.extraData;
    if (data != null) {
      if (fieldBloc.state.extraData != null) {
        List<FileUploadModel> _listFiles = fieldBloc.state.extraData;
        _listFiles.removeWhere((element) => element.filename == data.fileName);
        // print(data.url);
        // logger.i(">> Length : ${pickedFiles.}\n>> File : ${data.fileName}");
        // pickedFiles.removeFieldBlocAt(index);
        // _listFiles.removeAt(index);
        print(_listFiles.length);
        fieldBloc.updateExtraData(_listFiles);
        // cek
      }
    }
    // end
    _uploadProgressSubscription.removeAt(index);
    _uploadFile.removeAt(index);
    pickedFiles.removeFieldBlocAt(index);

    // cek apakah jumlah file yg diunggah >= minimumFiles
    var extras = fieldBloc.state.extraData;
    if (extras != null && extras.length >= minimumFiles) {
      fieldBloc.updateValue("OK"); // nandain kalo berhasil upload semua
    } else {
      fieldBloc.updateValue(""); // nandain kalo belum berhasil diupload semua
    }

    emitSuccess(canSubmitAgain: true);
    // pickedFiles.state.fieldBlocs.length
    // isDoneAll();
  }

  // when fails to upload
  void retryUploadFile(int index) async {
    File _file = pickedFiles.state.fieldBlocs[index].state.value;
    await _reuploadFile(_file, index);
  }

  // cancel upload
  void cancelUpload(int index) {
    try {
      pickedFiles.removeFieldBlocAt(index);
      _uploadFile[index].cancel();
      _uploadFile.removeAt(index);
      _uploadProgressSubscription.removeAt(index);
      // _uploadProgressSubscription[index].cancel();
      // pickedFiles.state.fieldBlocs[index].state.formBloc.state
      //     .toSubmissionCancelled();
      emitSuccess(
        successResponse: "Proses upload berhasil dibatalkan",
        canSubmitAgain: true,
      );
    } catch (e) {
      emitSuccess(canSubmitAgain: true);
    }
  }

  void _handleDoneUpload(String url, String filename) {
    final List<FileUploadModel> extras = [];
    extras.add(FileUploadModel(filename: filename, url: url));
    if (fieldBloc.state.extraData != null) {
      extras.addAll(fieldBloc.state.extraData);
    }
    if (extras != null && extras.length >= minimumFiles) {
      fieldBloc.updateValue("OK"); // nandain kalo berhasil upload semua
    } else {
      fieldBloc.updateValue(""); // nandain kalo belum berhasil diupload semua
    }
    fieldBloc.updateExtraData(extras);
  }

  Future<void> _reuploadFile(File file, int index) async {
    // Current Extra Data
    FileDataModel currentExtraData = pickedFiles.value[index].state.extraData;
    if (currentExtraData != null) {
      currentExtraData.progress = 0.0;
    }
    pickedFiles.value[index].updateExtraData(currentExtraData);
    // reupload logic starts here
    _uploadFile[index].reupload();
    _uploadFile[index].uploadProgress.listen(
      (progress) {
        if (_uploadFile.length > index) {
          if (!_uploadFile[index].isCancelled) {
            final data = FileDataModel(
              progress: progress,
              state: FileUploadState.uploading,
              fileName: file.path,
            );
            pickedFiles.value[index].updateExtraData(data);
            emitSubmitting(progress: progress);
            isDoneAll();
          }
        }
      },
      onDone: () async {
        if (_uploadFile.length > index) {
          if (!_uploadFile[index]._isCancelled) {
            logger.i(_uploadFile[index].success);
            final data = FileDataModel(
              progress: 1,
              state: FileUploadState.success,
              fileName: _uploadFile[index].success["data"]["filename"],
              url: _uploadFile[index].success["data"]["url"],
            );
            pickedFiles.value[index].updateExtraData(data);
            _handleDoneUpload(data.url, data.fileName);
            isDoneAll();
            emitSuccess(
              successResponse:
                  "Berhasil mengunggah ${filePath.basename(data.fileName)}",
              canSubmitAgain: true,
            );
          }
        }
      },
      onError: (err) {
        String errorMessage = err != null ? err["message"] : "";
        if (_uploadFile.length > index) {
          final data = FileDataModel(
            progress: 0.0,
            state: FileUploadState.error,
            fileName: file.path,
          );
          pickedFiles.value[index].updateExtraData(data);
          emitFailure(failureResponse: errorMessage);
          isDoneAll();
        }
      },
    );
  }

  Future<void> _uploadImage(File file, int index) async {
    // print(">> INDEX : $index");
    _uploadFile.add(
      UploadFileProgress(
        uploadUrl: '$path',
        userFile: file,
        name: name,
        type: type,
        location: location,
      ),
    );

    _uploadProgressSubscription.add(
      _uploadFile[index].uploadProgress.listen(
        (progress) {
          if (_uploadFile.length > index) {
            if (!_uploadFile[index].isCancelled) {
              final data = FileDataModel(
                progress: progress,
                state: FileUploadState.uploading,
                fileName: file.path,
              );
              pickedFiles.value[index].updateExtraData(data);
              emitSubmitting(progress: progress);
              isDoneAll();
            }
          }
        },
        onDone: () async {
          if (_uploadFile.length > index) {
            if (!_uploadFile[index]._isCancelled) {
              logger.i(_uploadFile[index].success);
              final data = FileDataModel(
                progress: 1,
                state: FileUploadState.success,
                fileName: _uploadFile[index].success["data"]["filename"],
                url: _uploadFile[index].success["data"]["url"],
              );
              pickedFiles.value[index].updateExtraData(data);
              _handleDoneUpload(data.url, data.fileName);
              isDoneAll();
              emitSuccess(
                successResponse:
                    "Berhasil mengunggah ${filePath.basename(data.fileName)}",
                canSubmitAgain: true,
              );
            }
          }
        },
        onError: (err) {
          String errorMessage = err != null ? err["message"] : "";
          if (_uploadFile.length > index) {
            final data = FileDataModel(
              progress: 0.0,
              state: FileUploadState.error,
              fileName: file.path,
            );
            pickedFiles.value[index].updateExtraData(data);
            emitFailure(failureResponse: errorMessage);
            isDoneAll();
          }
        },
      ),
    );
  }

  @override
  void onSubmitting() async {
    try {
      if (pickedFiles.value != null) {
        pickedFiles.value.asMap().forEach((index, elem) async {
          if (elem.state.extraData == null) {
            await _uploadImage(elem.value, index);
          } else {
            FileDataModel extraData = elem.state.extraData;
          }
        });
      } else {
        print(">> Value is empty!");
      }
    } catch (e, stack) {
      print(e);
      print(stack);
      emitFailure();
    }
  }

  @override
  void onCancelingSubmission() async {
    _uploadFile.last.cancel();
    emitSubmitting(progress: 0.0);
    emitSubmissionCancelled();
  }
}

class UploadFileProgress {
  final Dio dio = Dio();
  final BehaviorSubject<double> _subject = BehaviorSubject.seeded(0.0);
  bool _isCancelled = false;
  Map<String, dynamic> _error = {};
  Map<String, dynamic> _success = {};
  bool get isCancelled => _isCancelled;
  Map<String, dynamic> get error => _error;
  Map<String, dynamic> get success => _success;
  final String uploadUrl;
  final String name; // key name
  final String location;
  final String type;
  final File userFile;
  CancelToken cancelToken = CancelToken();

  Stream<double> get uploadProgress => _subject.stream;

  UploadFileProgress({
    @required this.uploadUrl,
    @required this.name,
    @required this.location,
    @required this.type,
    @required this.userFile,
  }) {
    _subject.doOnDone(() {
      _subject.close();
    });
    _initUpload();
  }

  void _initUpload() async {
    try {
      _subject.value = 0.0;
      if (userFile != null && uploadUrl != null) {
        String filePath = userFile.path;
        String mediaType = "image";
        String fileType = "";
        //
        if (filePath != null) {
          fileType = KycHelpers.getfiletype(filePath);
          mediaType =
              fileType == "pdf" || fileType == "xlsx" || fileType == "xls"
                  ? "document"
                  : "image";
        }

        MultipartFile file = await MultipartFile.fromFile(
          userFile.path,
          contentType: MediaType("$mediaType", "$fileType"),
        );

        // cek type, jika pdf = pdf, xlsx = excel, jpg/png/jpeg = image
        String uploadType = "";
        if (fileType == "pdf") {
          uploadType = "pdf";
        } else if (fileType == "xlsx" || fileType == "xls") {
          uploadType = "excel";
        } else if (fileType == "jpg" ||
            fileType == "png" ||
            fileType == "jpeg") {
          uploadType = "images";
        }

        FormData formData = FormData.fromMap({
          "$name": file,
          "type": "$uploadType",
          "location": "$location",
        });

        final headers = await UserAgent.headers();
        dio.options.headers = headers;

        final response = await dio.post(
          "$uploadUrl",
          data: formData,
          onSendProgress: (int sent, int total) async {
            if (!_isCancelled) {
              final val = (sent / total);
              _subject.add(val);
            }
          },
          cancelToken: cancelToken,
        );

        // print(">> Result : ${response}");

        if (response.statusCode == 200) {
          _success = response.data;
          _subject.close();
        } else {
          if (!_subject.isClosed) {
            _error = response.data;
            _subject.addError(_error);
          }
        }
        //

      }
    } on DioError catch (error) {
      print(">> ${error.response.statusCode}");
      print(">> erro : ${error.response.data}");
      if (error.response.statusCode == 500) {
        _error = {"message": "Tidak dapat mengunggah, Server Error!"};
      }
      if (!_subject.isClosed) {
        _subject.value = 0.0;
        _subject.addError(error?.response?.data ?? "");
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      if (!_subject.isClosed) {
        print(">> ERROR : $e");
        print(">> STACK : $stack");
        _error = {"error": "ERROR"};
        _subject.value = 0.0;
        _subject.addError(_error);
      }
    }
  }

  void cancel() {
    _isCancelled = true;
    _subject.value = 0.0;
    cancelToken.cancel('request canceled by user');
    _subject.close();
  }

  void remove() {
    _subject.close();
  }

  void reupload() {
    _isCancelled = false;
    _initUpload();
  }
}
