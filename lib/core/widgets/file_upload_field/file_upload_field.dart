import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/core/data/models/file_data_model.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/widgets/image_upload_field.dart/bloc.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'bloc.dart';
import 'package:path/path.dart' as filePath;

class FileUploadSubmission extends StatelessWidget {
  final String label;
  final String hintText;
  final String path;
  final String name;
  final String type;
  final String location;
  final Widget annotation;
  final List<FileUploadModel> files; // uploaded files
  final int maxFiles; // maximum files to upload
  final int minFiles; // minimum files to upload
  final List<String> allowedTypes; // allowed file types to upload
  final TextFieldBloc fieldBloc;
  final double maxFileSize;

  FileUploadSubmission({
    this.label = '',
    this.hintText = '',
    this.annotation,
    this.files,
    @required this.path,
    @required this.name,
    @required this.type,
    @required this.location,
    @required this.maxFiles,
    @required this.minFiles,
    @required this.allowedTypes,
    @required this.fieldBloc,
    this.maxFileSize = 10, // mb
  });

  Widget _progress({
    @required InputFieldBloc bloc,
    @required VoidCallback onCancel,
    @required VoidCallback onDelete,
    @required VoidCallback onRetry,
    @required VoidCallback onChangeFile,
    BuildContext context,
  }) {
    FileDataModel result =
        bloc.state.extraData != null ? bloc.state.extraData : null;
    if (result != null && result.state != FileUploadState.uninitialized) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: Sizes.s5),
                child: LinearPercentIndicator(
                  width: MediaQuery.of(context).size.width -
                      (result.state == FileUploadState.error
                          ? Sizes.s140
                          : Sizes.s100),
                  lineHeight: Sizes.s30,
                  percent: result.state == FileUploadState.uploading
                      ? result.progress
                      : result.state == FileUploadState.success
                          ? 1.0
                          : 0.0,
                  center: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          "${filePath.basename(result?.fileName ?? '')}",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: FontSize.s10,
                          ),
                          maxLines: 2,
                        ),
                        flex: 1,
                      ),
                      result.state == FileUploadState.uploading
                          ? Text(
                              "Mengunggah...",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s10,
                              ),
                            )
                          : result.state == FileUploadState.error
                              ? Text(
                                  "Gagal...",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontSize: FontSize.s10,
                                  ),
                                )
                              : result.state == FileUploadState.success
                                  ? Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: FontSize.s14,
                                    )
                                  : Container(),
                    ],
                  ),
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  backgroundColor: Colors.grey,
                  progressColor: Color(0xff6870E1),
                ),
              ),
              Container(width: Sizes.s10),
              result.state == FileUploadState.uploading
                  ? InkWell(
                      onTap: onCancel,
                      child: Container(
                        height: Sizes.s40,
                        width: Sizes.s40,
                        child: Center(
                          child: Icon(
                            Icons.cancel,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    )
                  : result.state == FileUploadState.error
                      ? Row(
                          children: [
                            InkWell(
                              onTap: onChangeFile,
                              child: Container(
                                height: Sizes.s40,
                                width: Sizes.s40,
                                child: Center(
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: onRetry,
                              child: Container(
                                height: Sizes.s40,
                                width: Sizes.s40,
                                child: Center(
                                  child: Icon(
                                    Icons.replay_outlined,
                                    color: Colors.orange,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      : result.state == FileUploadState.success
                          ? InkWell(
                              onTap: onDelete,
                              child: Container(
                                height: Sizes.s40,
                                width: Sizes.s40,
                                child: Center(
                                  child: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            )
                          : Container()
            ],
          ),
          // SizedBox(height: 10),
        ],
      );
    } else {
      return Container();
    }
  }

  void showSnackbar(BuildContext context, String message) {
    ToastHelper.showBasicToast(context, message);
  }

  pickFile({@required dynamic bloc, @required BuildContext context}) async {
    try {
      bool show = false; // show loading dialog
      int countFilesOnState = 0;
      // Cek apakah pickedFiles sudah ada filenya
      if (bloc.pickedFiles.state.fieldBlocs.isNotEmpty) {
        countFilesOnState = bloc.pickedFiles.state.fieldBlocs.length;
      }
      // end
      FilePickerResult result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.custom,
        allowedExtensions: allowedTypes,
        onFileLoading: (status) {
          // file picker caching file to storage
          if (status == FilePickerStatus.picking) {
            show = true;
            PopupHelper.showLoading(context);
          }

          if (status == FilePickerStatus.done) {
            if (show) {
              Navigator.pop(context);
            }
          }
        },
      );

      if (result != null) {
        List<File> files = result.paths.map((path) => File(path)).toList();
        if (countFilesOnState != null &&
            files.length > (maxFiles - countFilesOnState)) {
          showSnackbar(
            context,
            "Jumlah maksimal file yang dapat dipilih adalah $maxFiles file",
          );
          await FilePicker.platform.clearTemporaryFiles();
        } else {
          files.forEach((element) async {
            String _fileExtension = KycHelpers.getfiletype(element.path);
            // Check File Extensions
            if (allowedTypes.any((ext) => ext == _fileExtension)) {
              // get file size
              // file from path
              File _file = File(element.path);
              if (_file.lengthSync() > (maxFileSize * 1000000)) {
                showSnackbar(
                  context,
                  "Ukuran file maksimal adalah $maxFileSize MB",
                );
              } else {
                bloc.addFile(element);
                bloc.submit();
              }
            } else {
              showSnackbar(
                context,
                "Format file yang anda pilih tidak sesuai!",
              );
            }
          });
        }
      }
    } catch (e, stack) {
      print(e);
      print(stack);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FileUploadBloc(
        name: name,
        path: path,
        // uploadedFiles: files,
        maximumFiles: maxFiles,
        minimumFiles: minFiles,
        fieldBloc: fieldBloc,
        type: type,
        location: location,
      ),
      child: Builder(
        builder: (context) {
          // ignore: close_sinks
          final bloc = context.bloc<FileUploadBloc>();
          return FormBlocListener<FileUploadBloc, String, String>(
            onSubmitting: (context, state) {},
            onSuccess: (context, state) async {
              if (state.successResponse != null &&
                  state.successResponse.isNotEmpty) {
                showSnackbar(context, "${state.successResponse}");
              }
              // await Future.delayed(Duration(milliseconds: 500));
              // onFinished(bloc.isDoneAll());
            },
            onFailure: (context, state) {
              showSnackbar(context, "${state.failureResponse}");
            },
            child: BlocBuilder<FileUploadBloc, FormBlocState>(
                builder: (context, state) {
              bool canUpload = state is FormBlocSubmitting ? false : true;
              // GET ERROR FIELD
              // logger.i(
              //     ">> Error Field : \n Is Initial : ${fieldBloc.state.isInitial} \n Has Error : ${fieldBloc.state.hasError} \n Error : ${fieldBloc.state.error} ");
              // END
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    onChanged: (String val) {},
                    readOnly: true,
                    decoration: InputDecoration(
                      labelText: '$label',
                      labelStyle: PralistingInputDecoration.labelTextStyle,
                      hintText: '$hintText',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintStyle: PralistingInputDecoration.hintTextStyle(false),
                      errorMaxLines: 5,
                      contentPadding: EdgeInsets.only(
                        top: 0,
                        bottom: 0,
                        left: 15,
                      ),
                      errorText: !fieldBloc.state.isInitial
                          ? fieldBloc.state.hasError
                              ? fieldBloc.state.extraData != null &&
                                      fieldBloc.state.extraData.length > 0 &&
                                      fieldBloc.state.extraData.length <
                                          minFiles
                                  ? "Data tidak lengkap"
                                  : fieldBloc.state.error
                              : null
                          : null,
                      // errorText: fieldBloc.state.error,

                      // floatingLabelBehavior: FloatingLabelBehavior.always,
                      border: OutlineInputBorder(),
                      suffixIcon: InkWell(
                        onTap: canUpload
                            ? () async =>
                                await pickFile(bloc: bloc, context: context)
                            : null,
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: BoxDecoration(
                            color: canUpload ? Color(0xff6870E1) : Colors.grey,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Center(
                            child: Text(
                              "Unggah File",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  annotation != null ? annotation : Container(),
                  bloc.pickedFiles.state.fieldBlocs.isNotEmpty
                      ? SizedBox(height: Sizes.s15)
                      : Container(),
                  BlocBuilder<ListFieldBloc, ListFieldBlocState>(
                    bloc: bloc.pickedFiles,
                    builder: (context, state) {
                      if (state.fieldBlocs.isNotEmpty) {
                        return ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: state.fieldBlocs.length,
                          itemBuilder: (context, index) {
                            // ignore: close_sinks
                            var currentBloc =
                                state.fieldBlocs[index] as InputFieldBloc;
                            return _progress(
                              bloc: currentBloc,
                              onCancel: () => bloc.cancelUpload(index),
                              onDelete: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    FileDataModel data =
                                        currentBloc?.state?.extraData;
                                    return AlertDialog(
                                      title: Text(
                                          "Hapus ${filePath.basename(data?.fileName) ?? ''}?"),
                                      content: Text(
                                          "File ini akan dihapus dari server, anda mungkin perlu mengunggahnya kembali."),
                                      actions: [
                                        FlatButton(
                                          child: Text(
                                            "Hapus",
                                            style: TextStyle(
                                              color: Colors.red,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.pop(context);
                                            bloc.deleteFile(index);
                                          },
                                        ),
                                        FlatButton(
                                          child: Text(
                                            "Batalkan",
                                          ),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              onRetry: () async => bloc.retryUploadFile(index),
                              onChangeFile: () => bloc.deleteFile(index),
                              context: context,
                            );
                          },
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  bloc.isDoneAll()
                      ? Container(
                          margin: EdgeInsets.only(bottom: Sizes.s10),
                          child: Row(
                            children: [
                              Icon(
                                Icons.check_circle,
                                color: Color(0xff6870E1),
                              ),
                              SizedBox(width: 5),
                              Text(
                                "Dokumen Lengkap",
                                style: TextStyle(
                                  color: Color(0xff6870E1),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              )
                            ],
                          ),
                        )
                      : Container()
                ],
              );
            }),
          );
        },
      ),
    );
  }
}
