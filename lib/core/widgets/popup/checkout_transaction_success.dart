import 'package:flutter/material.dart';
import '../../../pages/Home.dart';

class CheckoutTransactionSuccess {
  static void show(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text('Checkout Saham Telah Berhasil'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => Home(index: 1)),
                      (Route<dynamic> route) => false);
                  // Navigator.of(context).pushNamed('/token');
                },
              ),
            ],
          );
        });
  }
}
