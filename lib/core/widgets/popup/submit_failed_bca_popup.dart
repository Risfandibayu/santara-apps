import 'package:flutter/material.dart';

import '../../../features/deposit/main/presentation/pages/main_deposit_page.dart';
import '../../../pages/Home.dart';

class SubmitFailedBcaPopup {
  static show(BuildContext context, int isDeposit, Function onTapLanjutkan) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      child: Icon(Icons.close),
                      onTap: () => Navigator.pop(context)),
                ),
                Container(height: 20),
                Text(
                  "Anda masih memiliki pembayaran yang harus diselesaikan",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    "Anda harus menyelesaikan pembayaran melalui virtual account BCA di transaksi sebelumnya terlebih dahulu atau sistem virtual account BCA akan menghapus pembayaran sebelumnya.",
                    style: TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (isDeposit == 0) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(index: 4)),
                                (Route<dynamic> route) => false);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) =>
                                        MainDepositPage(initialTab: 1)));
                          } else if (isDeposit == 1) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(index: 1)),
                                (Route<dynamic> route) => false);
                          }
                        },
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(color: Color(0xFFC52D2F))),
                          child: Center(
                              child: Text("Lakukan Pembayaran",
                                  style: TextStyle(
                                      color: Color(0xFFC52D2F), fontSize: 13),
                                  textAlign: TextAlign.center)),
                        ),
                      ),
                    ),
                    Container(width: 10),
                    Expanded(
                      child: GestureDetector(
                        onTap: onTapLanjutkan,
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              color: Color(0xFFC52D2F),
                              borderRadius: BorderRadius.circular(4)),
                          child: Center(
                              child: Text("Lanjutkan",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 13),
                                  textAlign: TextAlign.center)),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ));
        });
  }
}
