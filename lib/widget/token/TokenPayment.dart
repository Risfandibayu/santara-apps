import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/TokenHistory.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/token/Payment.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class TokenPayment extends StatefulWidget {
  @override
  _TokenPaymentState createState() => _TokenPaymentState();
}

class _TokenPaymentState extends State<TokenPayment> {
  final rupiah = new NumberFormat("#,##0");
  final angka = new NumberFormat("#,##0");
  final storage = new FlutterSecureStorage();
  List<TokenHistory> tokenHistory;
  var isLoading = true;
  var token;
  int isVerified;
  int tryLooping = 0;
  int tryLooping2 = 0;

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    final datas = userFromJson(await storage.read(key: 'user'));
    final uuid = datas.uuid;
    final http.Response response = await http.get(
        '$apiLocal/users/by-uuid/$uuid',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      final data = userFromJson(response.body);
      setState(() {
        isVerified = data.trader.isVerified;
        tryLooping = 0;
      });
    } else {
      if (tryLooping < 3) {
        setState(() => tryLooping = tryLooping + 1);
        getLocalStorage();
      }
    }
  }

  Future getTransactionsHistory() async {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/transactions/', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        tokenHistory = tokenHistoryFromJson(response.body);
        isLoading = false;
        tryLooping2 = 0;
      });
    } else {
      if (tryLooping2 < 5) {
        setState(() {
          tryLooping2 = tryLooping2 + 1;
          getTransactionsHistory();
        });
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((_) {
      getTransactionsHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        heightFactor: 20,
        child: CircularProgressIndicator(),
      );
    } else if (tokenHistory == null) {
      return Center(
        child: Text('Anda Tidak Memiliki Transaksi Pembelian Saham'),
      );
    } else {
      return Container(
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          itemCount: tokenHistory.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0XFFE5E5E5)),
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0XFFF5F5F5)),
              child: Column(
                children: <Widget>[
                  //container 1
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Row(
                      children: <Widget>[
                        SantaraCachedImage(
                          height: MediaQuery.of(context).size.width / 7 + 10,
                          width: MediaQuery.of(context).size.width / 7 + 10,
                          // placeholder: 'assets/Preload.jpeg',
                          image:
                              '$urlAsset/token/${tokenHistory[index].picture[0].picture}',
                          fit: BoxFit.cover,
                        ),
                        //text 1
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('${tokenHistory[index].companyName}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                Text(
                                    DateFormat('dd MMM yyy HH:mm:ss')
                                        .format(DateTime.parse(
                                            tokenHistory[index].createdAt))
                                        .toString(),
                                    style: TextStyle(fontSize: 14)),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  //container 2
                  Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      color: Color(0XFFECECEC),
                      child: Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Quantity'),
                                Text('Harga Per Satuan (Rp)'),
                                Text('Total')
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(':'),
                                Text(':'),
                                Text(':')
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                    '${angka.format(tokenHistory[index].amount / tokenHistory[index].price)} lembar'),
                                Text(
                                    'Rp ${rupiah.format(tokenHistory[index].price)}'),
                                Text(
                                    'Rp ${rupiah.format(tokenHistory[index].amount)}')
                              ],
                            )
                          ],
                        ),
                      )),
                  //Container 3
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        //button 1
                        Container(
                            child: tokenHistory[index].lastStatus == 'CREATED'
                                ? Chip(
                                    backgroundColor: Colors.grey,
                                    label: Text('Belum Konfirmasi',
                                        style: TextStyle(color: Colors.white)))
                                : tokenHistory[index].lastStatus ==
                                        'WAITING FOR VERIFICATION'
                                    ? Chip(
                                        backgroundColor: Color(0XFFFF9149),
                                        label: Text('Menunggu Konfirmasi',
                                            style:
                                                TextStyle(color: Colors.white)))
                                    : tokenHistory[index].lastStatus ==
                                            'EXPIRED'
                                        ? Chip(
                                            backgroundColor: Color(0XFFBF2D30),
                                            label: Text('Pesanan Kadaluarsa',
                                                style: TextStyle(
                                                    color: Colors.white)))
                                        : Chip(
                                            backgroundColor: Color(0XFF4E5DE3),
                                            label: Text('Sudah Konfirmasi',
                                                style: TextStyle(
                                                    color: Colors.white)))),
                        //button 2
                        Container(
                            width: MediaQuery.of(context).size.width / 3,
                            child: tokenHistory[index].lastStatus == 'CREATED'
                                ? FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                    onPressed: () {
                                      if (isVerified == 0) {
                                        showModalUnverified(
                                            "Akun anda belum diverifikasi oleh admin");
                                      } else if (isVerified == 1) {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Payment(
                                                      uuid: tokenHistory[index]
                                                          .uuid,
                                                      chanel:
                                                          tokenHistory[index]
                                                              .chanel,
                                                      name: tokenHistory[index]
                                                          .companyName,
                                                      image: tokenHistory[index]
                                                          .picture[0]
                                                          .picture,
                                                      emitenId:
                                                          tokenHistory[index]
                                                              .id
                                                              .toString(),
                                                      amount:
                                                          tokenHistory[index]
                                                              .amount,
                                                      price: tokenHistory[index]
                                                          .price,
                                                      bank: tokenHistory[index]
                                                          .bank,
                                                      expiredDate:
                                                          tokenHistory[index]
                                                              .expiredDate,
                                                      merchantCode:
                                                          tokenHistory[index]
                                                              .merchantCode,
                                                      accountNumber:
                                                          tokenHistory[index]
                                                              .accountNumber,
                                                      nameVa:
                                                          tokenHistory[index]
                                                              .name,
                                                      count: tokenHistory[index]
                                                              .amount /
                                                          tokenHistory[index]
                                                              .price,
                                                    )));
                                      } else {
                                        showModalUnverified(
                                            "Terjadi kesalahan, harap hubungi CS Santara");
                                      }
                                    },
                                    color: Color(0XFF0E7E4A),
                                    child: Text(
                                      'Konfirmasi',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                                : FlatButton(
                                    onPressed: null,
                                    disabledColor: Colors.grey[300],
                                    child: Text('Konfirmasi'),
                                  ))
                      ],
                    ),
                  )
                ],
              ),
            );
          },
        ),
      );
    }
  }

  void showModalUnverified(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Pembayaran gagal dilakukan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 50,
                      width: double.maxFinite,
                      color: Color(0xFFBF2D30),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }
}
