import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/TokenHistory.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/token.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/token/Payment.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class ListTransaction extends StatefulWidget {
  final int index;
  ListTransaction({this.index});
  @override
  _ListTransactionState createState() => _ListTransactionState();
}

class _ListTransactionState extends State<ListTransaction> {
  final rupiah = new NumberFormat("#,##0");
  DateFormat dateFormat = new DateFormat("EEEE, dd MMM yyyy HH:mm:ss", "id");
  DateFormat dateFormat2 = new DateFormat("dd MMM yyyy HH:mm", "id");
  DateFormat dateFormat3 = new DateFormat("dd MMM yyyy", "id");
  final storage = new FlutterSecureStorage();
  DateTime now;
  var token;
  int isVerified;
  int tryLooping = 0;
  int tryLooping1 = 0;

  bool isLoading = true;
  List<TokenHistory> tokenHistory = [];
  final apiService = ApiService();

  Future getTransactionsHistory() async {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/transactions/', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        tokenHistory = tokenHistoryFromJson(response.body);
        tryLooping1 = 0;
        isLoading = false;
      });
    } else {
      if (tryLooping1 < 5) {
        setState(() {
          tryLooping1 = tryLooping1 + 1;
          getTransactionsHistory();
        });
      } else {
        setState(() {
          tokenHistory = [];
          isLoading = false;
        });
      }
    }
  }

  Future getAvalableTransaction() async {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/transactions/checkout', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        tokenHistory = tokenHistoryFromJson(response.body);
        tryLooping1 = 0;
        isLoading = false;
      });
    } else {
      if (tryLooping1 < 5) {
        setState(() {
          tryLooping1 = tryLooping1 + 1;
          getAvalableTransaction();
        });
      } else {
        setState(() {
          tokenHistory = [];
          isLoading = false;
        });
      }
    }
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
      });
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    final datas = userFromJson(await storage.read(key: 'user'));
    final uuid = datas.uuid;
    final http.Response response = await http.get(
        '$apiLocal/users/by-uuid/$uuid',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      final data = userFromJson(response.body);
      setState(() {
        isVerified = data.trader.isVerified;
        tryLooping = 0;
      });
    } else {
      if (tryLooping < 3) {
        setState(() => tryLooping = tryLooping + 1);
        getLocalStorage();
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((_) {
      getNowTime().then((_) {
        if (widget.index == 0) {
          getAvalableTransaction();
        } else {
          getTransactionsHistory();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? _stillLoading()
          : tokenHistory.length == 0
              ? _listIsEmpty(widget.index)
              : ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: tokenHistory.length,
                  padding: EdgeInsets.all(10),
                  itemBuilder: (_, i) {
                    tokenHistory[i].fee =
                        tokenHistory[i].fee == null ? 0 : tokenHistory[i].fee;
                    return _itemList(tokenHistory[i]);
                  },
                ),
    );
  }

  Widget _buildItemTransaction(TokenHistory token) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Theme(
          data: theme,
          child: ExpansionTile(
            tilePadding: EdgeInsets.zero,
            backgroundColor: Colors.transparent,
            initiallyExpanded: false,
            title: Container(
              margin: EdgeInsets.only(left: Sizes.s15),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Color(0xFFBF2D30),
                ),
                borderRadius: BorderRadius.circular(Sizes.s5),
              ),
              width: double.maxFinite,
              padding: EdgeInsets.all(Sizes.s10),
              child: Text(
                "Batas : " +
                    dateFormat.format(token.expiredDate == null
                        ? DateTime(1970)
                        : DateTime.parse(token.expiredDate)),
                style: TextStyle(
                  color: Color(0xFFBF2D30),
                  fontSize: FontSize.s14,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            trailing: Container(
              width: Sizes.s60 + Sizes.s5,
              child: Row(
                children: [
                  Text(
                    "Detail",
                    style: TextStyle(
                      color: Color(0xff218196),
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff218196),
                  )
                ],
              ),
            ),
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: Text("No. Transaksi",
                            style: TextStyle(fontSize: 15))),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "SAN-${token.id}-${token.codeEmiten}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        InkWell(
                          onTap: () {
                            Clipboard.setData(ClipboardData(
                              text: "SAN-${token.id}-${token.codeEmiten}",
                            ));
                            Toast.show(
                              "Copied to Clipboard",
                              context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM,
                            );
                          },
                          child: Container(
                            width: Sizes.s35,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Icon(
                                Icons.link,
                                size: FontSize.s25,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Harga Saham",
                            style: TextStyle(fontSize: 15))),
                    Text("Rp ${rupiah.format(token.price)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Jumlah Investasi",
                            style: TextStyle(fontSize: 15))),
                    Text(
                        "${(token.amount / token.price).toStringAsFixed(0)} Lembar",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15))
                  ],
                ),
              ),
              token.chanel == "VA"
                  ? Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Text("Biaya Admin",
                                  style: TextStyle(fontSize: 15))),
                          Text("Rp ${rupiah.format(token.fee)}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15))
                        ],
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: Sizes.s20),
          height: 1,
          color: Color(0xFFC4C4C4),
        ),
        Container(
          // color: Color(0xFFF2F2F2),
          padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Total",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      "Rp ${rupiah.format(token.amount + token.fee)}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    )
                  ],
                ),
              ),
              token.lastStatus == 'CREATED'
                  ? FlatButton(
                      color: Color(0xffFF572E),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(Sizes.s5),
                      ),
                      onPressed: () {
                        if (token.lastStatus == 'CREATED') {
                          if (isVerified == 1) {
                            final result = Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Payment(
                                        uuid: token.uuid,
                                        chanel: token.chanel,
                                        name: token.companyName,
                                        emitenId: token.codeEmiten,
                                        image: token.picture[0].picture,
                                        count: token.amount / token.price,
                                        bank: token.bank,
                                        bankTo: token.bankTo,
                                        price: token.price,
                                        fee: token.fee,
                                        amount: token.amount,
                                        expiredDate: token.expiredDate,
                                        merchantCode: token.merchantCode,
                                        accountNumber: token.accountNumber,
                                        nameVa: token.name)));
                            result.then((_) {
                              getAvalableTransaction();
                            });
                          } else if (isVerified == 0) {
                            showModalUnverified(
                                'Akun anda belum diverifikasi oleh admin');
                          } else {
                            showModalUnverified(
                                'Terjadi kesalahan, harap hubungi CS Santara');
                          }
                        }
                      },
                      child: Text(
                        "Bayar",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        )
      ],
    );
  }

  Widget _buildItemTransactionHistory(TokenHistory token) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 16),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Text(
                      dateFormat2.format(DateTime.parse(token.createdAt)))),
              token.lastStatus == 'WAITING FOR VERIFICATION'
                  ? Text('Menunggu Konfirmasi',
                      style: TextStyle(
                          color: Color(0xFFE5A037),
                          fontWeight: FontWeight.bold))
                  : token.lastStatus == 'VERIFIED'
                      ? Text('Pembelian Berhasil',
                          style: TextStyle(
                              color: Color(0XFF0E7E4A),
                              fontWeight: FontWeight.bold))
                      : Text('Pembelian Gagal',
                          style: TextStyle(
                              color: Color(0XFFBF2D30),
                              fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Nomor Transaksi",
                  style: TextStyle(color: Color(0xFF858585), fontSize: 13)),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(8),
                    margin: EdgeInsets.fromLTRB(0, 4, 10, 4),
                    decoration: BoxDecoration(
                        color: Color(0xf7e0bb).withOpacity(1.0),
                        borderRadius: BorderRadius.circular(8)),
                    child: Text("SAN-${token.id}-${token.codeEmiten}",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  InkWell(
                      onTap: () {
                        Clipboard.setData(new ClipboardData(
                            text: "SAN-${token.id}-${token.codeEmiten}"));
                        Toast.show("Copied to Clipboard", context,
                            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                      },
                      child: Text("Salin",
                          style: TextStyle(
                              color: Color(0xFF218196),
                              fontWeight: FontWeight.bold)))
                ],
              )
            ],
          ),
        ),
        Container(
          height: 1,
          color: Color(0xFFC4C4C4),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Text("Harga Saham", style: TextStyle(fontSize: 15))),
              Text("Rp ${rupiah.format(token.price)}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
          child: Row(
            children: <Widget>[
              Expanded(
                  child:
                      Text("Jumlah Investasi", style: TextStyle(fontSize: 15))),
              Text("${(token.amount / token.price).toStringAsFixed(0)} Lembar",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
            ],
          ),
        ),
        token.chanel == "VA"
            ? Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Biaya Admin",
                            style: TextStyle(fontSize: 15))),
                    Text("Rp ${rupiah.format(token.fee)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15))
                  ],
                ),
              )
            : Container(),
        Container(
          height: 1,
          margin: EdgeInsets.only(left: 10, right: 10),
          color: Color(0xFFC4C4C4),
        ),
        Container(
          // color: Color(0xFFF2F2F2),
          padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "Total",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text("Rp ${rupiah.format(token.amount + token.fee)}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
            ],
          ),
        ),
      ],
    );
  }

  Widget _itemList(TokenHistory token) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Sizes.s10),
              border: Border.all(
                color: Color(
                  0xFFC4C4C4,
                ),
              ),
            ), // border: Border(top: BorderSide(color:Color(0xFFC4C4C4)), left: BorderSide(color:Color(0xFFC4C4C4)), right: BorderSide(color:Color(0xFFC4C4C4)))),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(Sizes.s15),
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(Sizes.s50),
                      child: SantaraCachedImage(
                        height: Sizes.s60,
                        width: Sizes.s60,
                        image: '$urlAsset/token/${token.picture[0].picture}',
                        fit: BoxFit.cover,
                      ),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: token.category == null
                                  ? Container()
                                  : Text(
                                      token.category,
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontSize: FontSize.s12,
                                        color: Color(0xFF292F8D),
                                      ),
                                    ),
                            ),
                            widget.index == 0
                                ? Container(
                                    width: Sizes.s30,
                                    height: Sizes.s30,
                                    child: PopupMenuButton<int>(
                                      icon: Icon(
                                        Icons.more_horiz,
                                        size: Sizes.s20,
                                      ),
                                      // padding: EdgeInsets.only(right: 10),
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(3.0),
                                        side: BorderSide(
                                          color: Color(0xffD6D6D6),
                                          width: 1,
                                        ),
                                      ),
                                      // margin: EdgeInsets.all(0),
                                      onSelected: (int result) async {
                                        if (result == 1) {
                                          PopupHelper.popConfirmation(context,
                                              () async {
                                            Navigator.pop(context);
                                            await cancelPayment(
                                                context, token.id, this.token);
                                            getAvalableTransaction();
                                          }, "Hapus Transaksi",
                                              "Apakah anda yakin ingin menghapus dan membatalkan transaksi ini?");
                                        }
                                      },
                                      itemBuilder: (BuildContext context) =>
                                          <PopupMenuEntry<int>>[
                                        PopupMenuItem<int>(
                                          height: Sizes.s25,
                                          value: 1,
                                          child: Text("Hapus Transaksi"),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                        Container(
                          child: token.trademark == null
                              ? Container()
                              : Text(
                                  token.trademark,
                                  overflow: TextOverflow.visible,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: FontSize.s16,
                                  ),
                                ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                "${token?.companyName ?? ''}",
                                overflow: TextOverflow.visible,
                                style: TextStyle(fontSize: FontSize.s12),
                              ),
                            ),
                            Container(
                              child: Text(
                                dateFormat3.format(
                                  DateTime.parse(
                                    token?.createdAt ?? DateTime(1970),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                token.lastStatus == "CREATED"
                    ? _buildItemTransaction(token)
                    : _buildItemTransactionHistory(token),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void showModalUnverified(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Pembayaran gagal dilakukan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 50,
                      width: double.maxFinite,
                      color: Color(0xFFBF2D30),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _stillLoading() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _listIsEmpty(int index) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/icon/empty_dividen.png"),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Text(
                index == 0
                    ? "Anda tidak memiliki transaksi pembelian"
                    : "Anda tidak memiliki riwayat transaksi pembelian",
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ));
  }
}
