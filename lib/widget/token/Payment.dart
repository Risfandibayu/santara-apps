import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:camera/camera.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:shimmer/shimmer.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../helpers/UserAgent.dart';
import '../../models/Bank.dart';
import '../../pages/Home.dart';
import '../../services/api_service.dart';
import '../../utils/api.dart';
import '../../utils/logger.dart';
import '../../utils/sizes.dart';
import '../widget/Camera.dart';

class Payment extends StatefulWidget {
  final String uuid;
  final String chanel;
  final String name;
  final String emitenId;
  final String image;
  final double count;
  final String bank;
  final String bankTo;
  final int price;
  final int amount;
  final String expiredDate;
  final String merchantCode;
  final String accountNumber;
  final String nameVa;
  final int fee;
  Payment({
    this.uuid,
    this.chanel,
    this.name,
    this.emitenId,
    this.image,
    this.count,
    this.price,
    this.fee = 0,
    this.bank,
    this.bankTo,
    this.amount,
    this.expiredDate,
    this.merchantCode,
    this.accountNumber,
    this.nameVa,
  });
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  DateTime now;
  DateFormat dateFormat = new DateFormat("EEEE, dd MMMM yyyy HH:mm:ss", "id");
  final rupiah = new NumberFormat("#,##0");
  final angka = new NumberFormat("#,##0");
  double saldoRupiah;
  var isExpanded = false;
  final formKeyOVO = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  TextEditingController ovoPhone = TextEditingController();
  TextEditingController nominalOvo = MoneyMaskedTextController(
      decimalSeparator: '', thousandSeparator: ',', precision: 0);
  File bukti;
  String token;
  var isLoading = false;
  var isFinish = false;
  var loading = true;
  var loadingIdr = true;
  double idr = 0.0;
  final apiService = ApiService();

  List<CameraDescription> cameras;
  bool isBack = true;

  Future getAvailableCamera() async {
    cameras = await availableCameras();
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
        loading = false;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
        loading = false;
      });
    }
  }

  List<Bank> bankList = [];
  String bank;

  Future getBank() async {
    final http.Response response = await http.get('$apiLocal/banks');
    if (response.statusCode == 200) {
      setState(() {
        bankList = bankFromJson(response.body);
        for (var i = 0; i < bankList.length; i++) {
          if (bankList[i].provider == widget.bankTo) {
            setState(() => bank = bankList[i].id.toString());
          }
        }
        // dropDownItem = getDropDownMenuItems(bankList);
      });
    } else {
      bankList = null;
    }
  }

  // List<DropdownMenuItem<String>> dropDownItem;
  // List<DropdownMenuItem<String>> getDropDownMenuItems(List<Bank> bankList) {
  //   List<DropdownMenuItem<String>> items = new List();
  //   bankList.map((value) {
  //     items.add(new DropdownMenuItem(
  //         value: value.id.toString(), child: Text(value.provider)));
  //   }).toList();
  //   return items;
  // }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    saldoRupiah = await getIdrDeposit();
  }

  Future manualPayment(
      BuildContext context, String uuid, File evidence, String token) async {
    setState(() => isLoading = true);
    final headers = await UserAgent.headers();
    final uri = Uri.parse('$apiLocal/transactions/confirmation');
    final request = http.MultipartRequest("PUT", uri);
    var bukti = http.ByteStream(DelegatingStream.typed(evidence.openRead()));
    var buktiLength = await evidence.length();

    var gambar = new http.MultipartFile(
        'verification_photo', bukti, buktiLength,
        filename: path.basename(evidence.path),
        contentType: MediaType('image', 'png'));

    request.fields['transaction_uuid'] = uuid;
    request.fields['bank_to'] = bank;
    request.files.add(gambar);
    request.headers['Authorization'] = 'Bearer ' + token;
    try {
      var response = await request.send();
      if (response.statusCode == 200) {
        setState(() => isLoading = false);
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Bukti telah berhasil dikirim'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home(index: 1)),
                          (Route<dynamic> route) => false);
                      // Navigator.pop(context);
                      // Navigator.pop(context);
                      // Navigator.of(context).pushAndRemoveUntil(
                      //   MaterialPageRoute(builder: (context) => Home(index: 1)),
                      //   (Route<dynamic> route) => false);
                      // Navigator.of(context).pushNamedAndRemoveUntil(
                      //     '/index', (Route<dynamic> route) => false);
                    },
                  ),
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        final respStr = await response.stream.bytesToString();
        final data = jsonDecode(respStr);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text(respStr.contains("message")
                    ? data["message"]
                    : 'Maaf ada beberapa kesalahan, silahkan periksa kembali data anda'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future paymentWithWallet(BuildContext context, String uuid) async {
    setState(() => isLoading = true);
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http.post(
        '$apiLocal/transactions/confirmation-wallet-payment/',
        headers: headers,
        body: {"transaction_uuid": "$uuid"},
      );
      if (response.statusCode == 200) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Pembayaran dengan wallet telah berhasil'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/index', (Route<dynamic> route) => false);
                    },
                  ),
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        final data = jsonDecode(response.body);
        showModalBottomSheet(
            isScrollControlled: true,
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
            ),
            builder: (builder) {
              return Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(4),
                      height: 4,
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.grey),
                    ),
                    Container(
                      height: 60,
                      child: Center(
                        child: Text('Pembayaran gagal dilakukan',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Colors.black)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        data['message'],
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Container(
                          height: 50,
                          width: double.maxFinite,
                          color: Color(0xFFBF2D30),
                          child: Center(
                              child: Text("Mengerti",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future paymentWithOvo() async {
    setState(() => isLoading = true);
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http.post(
        '$apiLocal/transactions/confirmation-ewallet-payment/',
        headers: headers,
        body: {
          "uuid": widget.uuid,
          "phone": ovoPhone.text,
          "amount": nominalOvo.text
        },
      );
      if (response.statusCode == 200) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Pembayaran dengan ovo telah berhasil'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/index', (Route<dynamic> route) => false);
                    },
                  ),
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        final data = jsonDecode(response.body);
        showModalBottomSheet(
            context: context,
            builder: (builder) {
              return Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16))),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(4),
                      height: 4,
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.grey),
                    ),
                    Container(
                      height: 60,
                      child: Center(
                        child: Text('Pembayaran gagal dilakukan',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Colors.black)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        data['message'],
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Container(
                          height: 50,
                          width: double.maxFinite,
                          color: Color(0xFFBF2D30),
                          child: Center(
                              child: Text("Mengerti",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future getIdrDeposit() async {
    final http.Response response = await http.get('$apiLocal/traders/idr',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      setState(() {
        final data = jsonDecode(response.body);
        idr = data['idr'] / 1;
        loadingIdr = false;
      });
    } else {
      setState(() => loadingIdr = false);
    }
  }

  @override
  void initState() {
    super.initState();
    getAvailableCamera();
    getNowTime();
    getLocalStorage().then((_) {
      getIdrDeposit();
    });
    getBank();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
            widget.chanel == "OVO"
                ? "OVO"
                : widget.chanel == "WALLET"
                    ? "Saldo Wallet"
                    : widget.chanel == "VA"
                        ? "Virtual Account"
                        : "Bank Transfer",
            style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.w600)),
      ),
      body: loading
          ? Container(child: Center(child: CircularProgressIndicator()))
          : ListView(
              children: <Widget>[
                DateTime.parse(widget.expiredDate).difference(now).inSeconds <=
                        0
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(0, 20, 0, 8),
                        child: Text("Batas Waktu Pembayaran",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center),
                      ),
                DateTime.parse(widget.expiredDate).difference(now).inSeconds <=
                        0
                    ? Container()
                    : _countdown(),
                SizedBox(height: Sizes.s10),
                Text(
                  "${dateFormat.format(DateTime.parse(widget.expiredDate))}",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s14,
                  ),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(
                      Sizes.s40, Sizes.s20, Sizes.s40, Sizes.s20),
                  decoration: BoxDecoration(
                    color: Color(0xffBF2D30),
                    borderRadius: BorderRadius.circular(Sizes.s10),
                  ),
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Text(
                    "PEMBELIAN OTOMATIS DIBATALKAN JIKA ANDA BELUM MENYELESAIKAN PEMBAYARAN DAN SAHAM SUDAH TERJUAL HABIS ",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                // DateTime.parse(widget.expiredDate).difference(now).inSeconds <=
                //         0
                //     ? Container()
                //     : _batasPembayaran(),
                _petunjukPembayaran(),
                _detailPembayaran(),
                widget.chanel == "OVO"
                    ? _ovo()
                    : widget.chanel == "WALLET"
                        ? _wallet()
                        : widget.chanel == "VA"
                            ? _va()
                            : _bankTransfer()
              ],
            ),
    );
  }

  Widget _countdown() {
    return Countdown(
      duration: Duration(
          seconds:
              DateTime.parse(widget.expiredDate).difference(now).inSeconds),
      onFinish: () {
        setState(() => isFinish = true);
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (_) {
              return AlertDialog(
                content: Text("Waktu pembayaran anda telah berakhir"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Oke"),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
      },
      builder: (BuildContext context, Duration remaining) {
        if (isFinish ||
            DateTime.parse(widget.expiredDate).difference(now).inSeconds <= 0) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text('00',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Jam",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(":",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center),
              ),
              Column(
                children: <Widget>[
                  Text('00',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Menit",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(":",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center),
              ),
              Column(
                children: <Widget>[
                  Text('00',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Detik",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
            ],
          );
        } else {
          String hour = (remaining.inHours).toString();
          String second = (remaining.inSeconds % 60).toString();
          String minute = (remaining.inMinutes % 60).toString();
          if (second.length == 1) {
            second = "0" + second;
          }
          if (minute.length == 1) {
            minute = "0" + minute;
          }
          if (hour.length == 1) {
            hour = "0" + hour;
          }
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text('$hour',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Jam",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(":",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center),
              ),
              Column(
                children: <Widget>[
                  Text('$minute',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Menit",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(":",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center),
              ),
              Column(
                children: <Widget>[
                  Text('$second',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center),
                  Text("Detik",
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textAlign: TextAlign.center),
                ],
              ),
            ],
          );
        }
      },
    );
  }

  Widget _batasPembayaran() {
    return Container(
      padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
      margin: EdgeInsets.all(16),
      color: Color(0xFFBF2D30),
      child: Text(
        "Batas : ${dateFormat.format(DateTime.parse(widget.expiredDate))}",
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _petunjukPembayaran() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Text("Petunjuk Pembayaran",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center),
          widget.chanel == "VA"
              ? Column(
                  children: <Widget>[
                    _itemCaraBayar(
                        "Pembayaran harus menggunakan virtual account pemilik akun Santara."),
                    _itemCaraBayar(
                        "Pembayaran akan otomatis dibatalkan jika melewati batas waktu pembayaran. "),
                  ],
                )
              : widget.chanel == "WALLET"
                  ? Column(
                      children: <Widget>[
                        _itemCaraBayar(
                            "Pembayaran akan diambil dari saldo di wallet akun Santara Anda."),
                        _itemCaraBayar(
                            "Pembayaran akan otomatis dibatalkan jika melewati batas waktu pembayaran. "),
                      ],
                    )
                  : widget.chanel == "OVO"
                      ? Column(
                          children: <Widget>[
                            _itemCaraBayar(
                                "Pembayaran akan diambil dari saldo di akun OVO Anda."),
                            _itemCaraBayar(
                                "Pembayaran akan otomatis dibatalkan jika melewati batas waktu pembayaran."),
                          ],
                        )
                      : Column(
                          children: <Widget>[
                            _itemCaraBayar(
                                "Pembayaran harus menggunakan rekening atas nama Anda."),
                            _itemCaraBayar(
                                "Pembayaran akan otomatis dibatalkan jika melewati batas waktu pembayaran. "),
                          ],
                        )
        ],
      ),
    );
  }

  Widget _itemCaraBayar(String item) {
    return Container(
      padding: const EdgeInsets.only(top: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 6, 16, 0),
            child:
                Icon(Icons.fiber_manual_record, color: Colors.black, size: 8),
          ),
          Expanded(
              child: Text(
            item,
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }

  Widget _itemDetail() {
    return Container(
      child: widget.chanel == "VA"
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 20),
                  child: Row(
                    children: <Widget>[
                      Text("Transfer ke : "),
                      widget.merchantCode == null
                          ? Container()
                          : Container(
                              width: 75,
                              child: Image.asset(widget.bank == "BNI"
                                  ? 'assets/bank/bni.png'
                                  : widget.bank == "PERMATA"
                                      ? 'assets/bank/permatabank.png'
                                      : widget.bank == "BRI"
                                          ? 'assets/bank/bri.png'
                                          : widget.bank == "BCA"
                                              ? 'assets/bank/bca.png'
                                              : 'assets/bank/mandiri.png')),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Nomor Virtual Account"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "${widget.accountNumber}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 24),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                        child: Text("a/n ${widget.nameVa}",
                            style: TextStyle(color: Colors.grey)),
                      )),
                      InkWell(
                        onTap: () {
                          Clipboard.setData(
                              new ClipboardData(text: widget.accountNumber));
                          Toast.show("Copied to Clipboard", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                        },
                        child: Text("Salin",
                            style: TextStyle(
                                color: Color(0xFF218196),
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.right),
                      ),
                    ],
                  ),
                ),
                widget.chanel == "VA"
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text("Jumlah Pembelian"),
                      )
                    : Container(),
                widget.chanel == "VA"
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        margin:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                        child: Text(
                          "Rp ${rupiah.format(widget.amount)}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                      )
                    : Container(),
                widget.chanel == "VA"
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text("Biaya Admin"),
                      )
                    : Container(),
                widget.chanel == "VA"
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        margin:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                        child: Text(
                          "Rp ${rupiah.format(widget.fee)}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                      )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Jumlah yang harus ditransfer"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "Rp ${rupiah.format(widget.amount + widget.fee)}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    children: <Widget>[
                      Expanded(child: Container()),
                      InkWell(
                        onTap: () {
                          Clipboard.setData(ClipboardData(
                              text: ((widget.amount + widget.fee)).toString()));
                          Toast.show("Copied to Clipboard", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                        },
                        child: Text("Salin",
                            style: TextStyle(
                                color: Color(0xFF218196),
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.right),
                      ),
                    ],
                  ),
                )
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Jumlah yang harus dibayarkan"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "Rp ${rupiah.format(widget.amount)}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _detailPembayaran() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
      padding: EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _itemDetail(),
          GestureDetector(
            onTap: () {
              setState(() {
                isExpanded ? isExpanded = false : isExpanded = true;
              });
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 20, 8, 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: Icon(
                        isExpanded
                            ? Icons.keyboard_arrow_up
                            : Icons.keyboard_arrow_down,
                        color: Color(0xFF218196)),
                  ),
                  Text(
                    "Detail Pembelian",
                    style: TextStyle(
                        color: Color(0xFF218196), fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
          isExpanded
              ? Container(
                  padding: EdgeInsets.only(top: 12),
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(color: Color(0xFFE6EAFA)),
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 70,
                              width: 70,
                              margin: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(40),
                                  image: DecorationImage(
                                      image: NetworkImage(widget.image),
                                      fit: BoxFit.cover)),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(widget.name,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600)),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(height: 12),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 6),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text(
                              "Harga Saham",
                              style: TextStyle(color: Colors.grey),
                            )),
                            Text("Rp ${rupiah.format(widget.price)}",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 6),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Jumlah Investasi",
                                    style: TextStyle(color: Colors.grey))),
                            Text("${angka.format(widget.count)} Lembar",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      widget.chanel == "VA"
                          ? Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16, vertical: 6),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Text(
                                    "Biaya Admin",
                                    style: TextStyle(color: Colors.grey),
                                  )),
                                  Text("Rp ${rupiah.format(widget.fee)}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            )
                          : Container(),
                      Container(
                        height: 1,
                        color: Colors.grey[200],
                        margin: EdgeInsets.all(8),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 6),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Total",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold))),
                            Text(
                                "Rp ${rupiah.format(widget.amount + widget.fee)}",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget _wallet() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: Container(
        color: Color(0xFFE6EAFA),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 6),
              child: Text("Saldo wallet anda"),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                margin: const EdgeInsets.fromLTRB(16, 6, 16, 16),
                decoration: BoxDecoration(color: Colors.white),
                child: loadingIdr
                    ? Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey,
                        child: Container(
                          color: Colors.grey,
                          height: 22,
                          width: MediaQuery.of(context).size.width,
                        ))
                    : Text('Rp ${rupiah.format(idr)}',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16))),
            Container(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 20),
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: InkWell(
                    onTap: () => Navigator.pushNamed(context, "/deposit"),
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Color(0xFF1BC47D)),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Center(child: Text("Isi Saldo")),
                    ),
                  )),
                  Container(width: 8),
                  Expanded(
                      child: Container(
                    // margin: EdgeInsets.only(top: 20),
                    width: MediaQuery.of(context).size.width / 1,
                    height: 40,
                    child: FlatButton(
                      disabledColor: Colors.grey,
                      onPressed: isLoading
                          ? null
                          : () {
                              paymentWithWallet(context, widget.uuid);
                            },
                      child: Text(
                          isLoading ? 'Sedang memuat...' : 'Bayar Saham',
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center),
                      color: Color(0xFF1BC47D),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: BorderSide(color: Color(0xFF2CAD71))),
                    ),
                  ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ovo() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: Form(
        key: formKeyOVO,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text("Transfer ke : "),
                Image.asset("assets/icon/ovo.png"),
              ],
            ),
            Text("Nomor telepon OVO anda"),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: Color(0xFFDDDDDD)),
                  borderRadius: BorderRadius.circular(5)),
              margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 20.0),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                controller: ovoPhone,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Masukkan Nomor HP';
                  } else if (value.length <= 9) {
                    return 'No Hp Kurang dari 8 digit';
                  } else if (!RegExp(r"^(00|\+)[1-9]{1}([0-9][\s]*){9,16}$")
                      .hasMatch(value)) {
                    return 'Masukkan nomor HP dengan kode negara';
                  } else {
                    return null;
                  }
                },
                decoration: InputDecoration(
                    border: InputBorder.none,
                    errorMaxLines: 5,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10)),
              ),
            ),
            Text("Masukkan nominal pembayaran"),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: Color(0xFFDDDDDD)),
                  borderRadius: BorderRadius.circular(5)),
              margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 10.0),
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: nominalOvo,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Masukkan nominal pembayaran';
                  } else if (!RegExp(r"^[0-9\,]*$").hasMatch(value) ||
                      value == ',' ||
                      value.contains(',,')) {
                    return 'Masukkan input tanpa spesial karakter';
                  } else {
                    return null;
                  }
                },
                decoration: InputDecoration(
                    border: InputBorder.none,
                    prefix: Text("Rp "),
                    errorMaxLines: 5,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10)),
              ),
            ),
            GestureDetector(
              onTap: isLoading
                  ? null
                  : () {
                      if (formKeyOVO.currentState.validate()) {
                        paymentWithOvo();
                      }
                    },
              child: Container(
                height: 40,
                margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                decoration: BoxDecoration(
                  color: isLoading ? Colors.grey[600] : Color(0xFF59439B),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Center(
                    child: Text(
                        isLoading ? "Sedang memuat..." : "Bayar dengan OVO",
                        style: TextStyle(color: Colors.white))),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _bankTransfer() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
              "Transfer sejumlah nominal diatas ke salah satu rekening berikut untuk memproses pembayaran Anda.",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.w600)),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(20, 8, 20, 20),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.grey[300])),
          child: bankList.length == 0
              ? Container(
                  height: 140,
                  child: Center(child: CircularProgressIndicator()))
              : ListView.builder(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: bankList.length,
                  itemBuilder: (context, i) {
                    return _listBank(bankList[i]);
                  },
                ),
        ),
        // Container(
        //   margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.start,
        //     children: <Widget>[
        //       Padding(
        //         padding: const EdgeInsets.only(left: 2, right: 4),
        //         child: Text("Bank Tujuan : "),
        //       ),
        //       Expanded(
        //         child: DropdownButton(
        //           isDense: true,
        //           isExpanded: true,
        //           value: bank,
        //           items: dropDownItem,
        //           hint: Text("Pilih Bank"),
        //           onChanged: (selected) {
        //             setState(() {
        //               bank = selected;
        //             });
        //           },
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        Container(
          margin: EdgeInsets.fromLTRB(20, 8, 20, 20),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.grey[300])),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: Text('Unggah Bukti Pembayaran',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600)),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  // margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(vertical: 12),
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xFFE6EAFA),
                  child: Column(
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                    title: Text('Pilih ambil dari :'),
                                    content: Container(
                                      height: 100,
                                      width: 200,
                                      child: Column(
                                        children: <Widget>[
                                          FlatButton(
                                              color: Colors.red[900],
                                              child: Text(
                                                'Galeri',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              onPressed: () async {
                                                await ImagePicker.pickImage(
                                                        source:
                                                            ImageSource.gallery)
                                                    .then((image) {
                                                  setState(() {
                                                    bukti = image;
                                                  });
                                                });
                                                Navigator.pop(context);
                                              }),
                                          FlatButton(
                                            color: Colors.red[900],
                                            child: Text('Kamera',
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            onPressed: () async {
                                              Navigator.pop(context);
                                              if (cameras.length == 0) {
                                                await ImagePicker.pickImage(
                                                        source:
                                                            ImageSource.camera)
                                                    .then((image) {
                                                  setState(() {
                                                    bukti = image;
                                                  });
                                                });
                                                Navigator.pop(context);
                                              } else {
                                                final result = Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            Camera()));
                                                result.then((image) {
                                                  if (image != null) {
                                                    setState(() {
                                                      bukti = image;
                                                    });
                                                  }
                                                });
                                              }
                                            },
                                          )
                                        ],
                                      ),
                                    ));
                              });
                        },
                        child: bukti == null
                            ? Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 100),
                                child: Column(
                                  children: <Widget>[
                                    Icon(
                                      Icons.folder,
                                      color: Color(0xFF218196),
                                      size: 50,
                                    ),
                                    Container(height: 8),
                                    Container(
                                      padding: const EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2,
                                              color: Color(0xFF218196)),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                      child: Center(
                                        child: Text("Upload Bukti Pembayaran",
                                            style: TextStyle(
                                              color: Color(0xFF218196),
                                              fontSize: 15,
                                            )),
                                      ),
                                    )
                                  ],
                                ))
                            : Image.file(bukti, fit: BoxFit.fill),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: Text(
                  "Max. ukuran file 10 mb. Gunakan format file .jpg, png atau .bmp",
                  style: TextStyle(fontSize: 12),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 16),
                width: MediaQuery.of(context).size.width / 1,
                child: FlatButton(
                  disabledColor: Colors.grey,
                  color: Color(0XFF4E5DE3),
                  onPressed: isLoading
                      ? null
                      : bukti == null || bank == null
                          ? null
                          : () {
                              manualPayment(
                                  context, widget.uuid, bukti, this.token);
                            },
                  child: Text(isLoading ? 'Mengupload...' : 'Upload',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _va() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Cara Transfer",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 1,
            color: Colors.grey[300],
          ),
          widget.bank == "MANDIRI"
              ? _mandiri()
              : widget.bank == "BRI"
                  ? _bri()
                  : widget.bank == "BNI"
                      ? _bni()
                      : widget.bank == "BCA"
                          ? _bca()
                          : _permata(),
          Container(height: 16),
        ],
      ),
    );
  }

  Widget caraTransfer(String title, Widget content) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      scrollOnExpand: false,
      scrollOnCollapse: true,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                header: Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      title,
                      style: TextStyle(fontSize: 16),
                    )),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: content,
                    ),
                    Container(
                      height: 1,
                      color: Colors.grey[200],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _listBank(Bank bank) {
    if (bank.provider == widget.bankTo) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
                width: 75,
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                child: Image.asset(
                    bank.provider == "BCA"
                        ? 'assets/bank/bca.png'
                        : bank.provider == "BRI"
                            ? 'assets/bank/bri.png'
                            : 'assets/bank/mandiri.png',
                    scale: MediaQuery.of(context).size.width / 24)),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      bank.accountName,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                      overflow: TextOverflow.visible,
                    ),
                    Text(
                      bank.accountNumber,
                      style: TextStyle(fontSize: 12),
                      overflow: TextOverflow.visible,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _mandiri() {
    String codeMandiri = "";
    if (widget.accountNumber.substring(0, 5) == "88608") {
      codeMandiri = "88608";
    } else if (widget.accountNumber.substring(0, 5) == "88908") {
      codeMandiri = "88908";
    }
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Kunjungi website Mandiri Internet Banking: "),
                      new TextSpan(
                        text: "https://ibank.bankmandiri.co.id/retail3/",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://ibank.bankmandiri.co.id/retail3/'),
                      ),
                      new TextSpan(
                          text:
                              "\n2. Login dengan memasukkan USER ID dan PIN\n3. Pilih 'Pembayaran’\n4. Pilih 'Multi Payment’\n5. Pilih 'No Rekening Anda’\n6. Pilih Penyedia Jasa 'Xendit $codeMandiri\n7. Pilih 'No Virtual Account’ \n8. Masukkan nomor Virtual Account anda\n9. Masuk ke halaman konfirmasi 1\n10. Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik 'Lanjutkan’\n11. Masuk ke halaman konfirmasi 2\n12. Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik 'Kirim’\n13. Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai"),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Log in Mobile Banking Mandiri Online (Install Mandiri Online by PT. Bank Mandiri (Persero Tbk.) dari App Store: \n"),
                      new TextSpan(
                        text:
                            "https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8'),
                      ),
                      new TextSpan(text: " atau Play Store: \n"),
                      new TextSpan(
                        text:
                            "https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&hl=en",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&hl=en'),
                      ),
                      new TextSpan(
                          text:
                              " \n2. Klik 'Icon Menu' di sebelah kiri atas\n3. Pilih menu 'Pembayaran’\n4. Pilih Buat Pembayaran Baru’\n5. Pilih 'Multi Payment’\n6. Pilih Penyedia Jasa 'Xendit $codeMandiri’\n7. Pilih 'No. Virtual’\n8. Masukkan no virtual account dengan kode perusahaan '$codeMandiri' lalu pilih 'Tambah Sebagai Nomor Baru’\n9. Masukkan 'Nominal' lalu pilih 'Konfirmasi’\n10. Pilih 'Lanjut’\n11. Muncul tampilan konfirmasi pembayaran\n12. Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih 'Konfirmasi’\n13. Masukkan 'PIN' dan transaksi telah selesai"),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Masukkan kartu ATM dan pilih 'Bahasa Indonesia'\n2. Ketik nomor PIN dan tekan BENAR\n3. Pilih menu 'BAYAR/BELI’\n4. Pilih menu 'MULTI PAYMENT’\n5. Ketik kode perusahaan, yaitu '$codeMandiri' (Xendit $codeMandiri), tekan 'BENAR’\n6. Masukkan nomor Virtual Account\n7. Isi NOMINAL, kemudian tekan 'BENAR’\n8. Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan 'YA'\n9. Muncul konfirmasi pembayaran. Tekan 'YA' untuk melakukan pembayaran\n10. Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri"),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _bri() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Masukan User ID dan Password \n2. Pilih menu Pembayaran \n3. Pilih menu BRIVA \n4. Pilih rekening Pembayar \n5. Masukkan Nomor Virtual Account BRI Anda (Contoh: 26215-xxxxxxxxxxxx) \n6. Masukkan nominal yang akan dibayar \n7. Masukkan password dan Mtoken anda"),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Log in ke Mobile Banking \n2. Pilih menu Pembayaran \n3. Pilih menu BRIVA \n4. Masukkan nomor BRI Virtual Account dan jumlah pembayaran \n5. Masukkan nomor PIN anda \n6. Tekan “OK” untuk melanjutkan transaksi \n7. Transaksi berhasil \n8. SMS konfirmasi akan masuk ke nomor telepon anda"),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Pilih menu Transaksi Lain \n2. Pilih menu Lainnya \n3. Pilih menu Pembayaran \n4. Pilih menu Lainnya \n5. Pilih menu BRIVA \n6. Masukkan Nomor BRI Virtual Account (Contoh: 26215-xxxxxxxxxxxx), lalu tekan “Benar” \n7. Konfirmasi pembayaran, tekan “Ya” bila sesuai"),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _bni() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pilih “ Transaksi” \n2. Pilih “INFO & ADMINISTRASI TRANSFER” \n3. Pilih “ATUR REKENING TUJUAN” \n4. Tambah rekening tujuan lalu klik “OK” \n5. Isi data rekening lalu tekan “LANJUTKAN” \n6. Akan muncul detail konfirmasi, apabila sudah benar dan sesuai, masukkan 8 digit angka yang dihasilkan dari APPLI 2 pada token BNI anda lalu klik ”PROSES” \n7. Rekening tujuan berhasil ditambahkan \n8. Pilih “TRANSFER” \n9. Pilih “Transfer antar rek BNI” \n10. Lengkapi semua data akun penerima, lalu klik “LANJUTKAN” \n11. Transaksi anda telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Log in Mobile Banking \n2. Pilih menu “TRANSFER” \n3. Pilih “WITHIN BANK” \n4. Isi kolom “DEBIT ACCOUNT” kemudian klik menu “ TO ACCOUNT” \n5. Pilih menu ”AD HOC BENEFICIARY” \n6. Lengkapi datanya dengan mengisi: NICKNAME, NOMOR VIRTUAL ACCOUNT, DAN BENEFICIARY EMAIL ADDRESS \n7. Konfirmasi akan muncul lalu klik “CONTINUE” \n8. Isi semua form yang ada setelah itu klik “CONTINUE” \n9. Detail konfirmasi muncul dengan meminta password transaksi \n10. Setelah dilengkapi, klik “CONTINUE” \n11. Transaksi anda telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pilih "Menu Lainnya" \n2. Pilih "Transfer" \n3. Pilih “TRANSAKSI LAINNYA” \n4. Pilih ke “ REKENING BNI” \n5. Masukkan nomor Virtual Account , lalu tekan “BENAR” \n6. Isi NOMINAL, kemudian tekan ”YA" \n7. Konfirmasi transaksi telah selesai, tekan “TIDAK” untuk menyelesaikan transaksi'),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _permata() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: '1. Buka situs\n'),
                      new TextSpan(
                        text: 'https://new.permatanet.com',
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch('https://new.permatanet.com'),
                      ),
                      new TextSpan(
                          text:
                              '\ndan login \n2. Pilih menu “pembayaran”. \n3. Pilih menu “Pembayaran Tagihan”. \n4. Pilih “Virtual Account” \n5. Pilih sumber pembayaran \n6. Pilih menu "Masukkan Daftar Tagihan Baru" \n7. Masukan nomor Virtual Account Anda \n8. Konfirmasi transaksi anda \n9. Masukkan SMS token response \n10. Pembayaran Anda berhasil \n11. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Buka Permata Mobile dan Login \n2. Pilih Pay "Pay Bills" / "Pembayaran Tagihan" \n3. Pilih menu “Transfer” \n4. Pilih sumber pembayaran \n5. Pilih “daftar tagihan baru” \n6. Masukan nomor Virtual Account Anda \n7. Periksa ulang mengenai transaksi yang anda ingin lakukan \n8. Konfirmasi transaksi anda \n9. Masukkan SMS token respons \n10. Pembayaran Anda berhasil \n11. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking PERMATA X",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Buka Permata Mobile X dan Login \n2. Pilih Pay "Pay Bills"/ "Pembayaran Tagihan" \n3. Pilih “Virtual Account” \n4. Masukkan Nomor Virtual Account anda \n5. Detail pembayaran anda akan muncul di layar \n6. Nominal yang ditagihkan akan muncul di layar. Pilih sumber pembayaran \n7. Konfirmasi transaksi anda \n8. Masukkan kode response token anda \n9. Transaksi anda telah selesai \n10. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pada menu utama, pilih transaksi lain \n2. Pilih Pembayaran Transfer \n3. Pilih pembayaran lainnya \n4. Pilih pembayaran Virtual Account \n5. Masukkan nomor virtual account anda \n6. Pada halaman konfirmasi, akan muncul nominal yang dibayarkan, nomor, dan nama merchant, lanjutkan jika sudah sesuai \n7. Pilih sumber pembayaran anda dan lanjutkan \n8. Transaksi anda selesai \n9. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _bca() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "ATM BCA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Masukkan kartu ke mesin ATM\n2. Masukkan 6 digit PIN Anda\n3. Pilih “Transaksi Lainnya”\n4. Pilih “Transfer”\n5. Pilih ke “ke Rekening BCA Virtual Account”\n6. Masukkan nomor BCA Virtual Account Anda, kemudian tekan “Benar”\n7. Masukkan jumlah yang akan dibayarkan, selanjutnya tekan “Benar”\n8. Validasi pembayaran Anda. Pastikan semua detail transaksi yang ditampilkan sudah benar, kemudian pilih “Ya”\n9. Pembayaran Anda telah selesai. Tekan “Tidak” untuk menyelesaikan transaksi, atau tekan “Ya” untuk melakukan transaksi lainnya'),
                    ],
                  )),
            )),
        caraTransfer(
            "M-BCA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan login pada aplikasi BCA Mobile\n2. Pilih “m-BCA”, lalu masukkan kode akses m-BCA\n3. Pilih “m-Transfer”\n4. Pilih “BCA Virtual Account”\n5. Masukkan nomor BCA Virtual Account Anda, atau pilih dari Daftar Transfer\n6. Masukkan jumlah yang akan dibayarkan\n7. Masukkan PIN m-BCA Anda\n8. Transaksi telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "Klik BCA Pribadi",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan login pada aplikasi KlikBCA Individual\n2. Masukkan User ID dan PIN Anda\n3. Pilih “Transfer Dana”\n4. Pilih “Transfer ke BCA Virtual Account”\n5. Masukkan nomor BCA Virtual Account Anda atau pilih dari Daftar Transfer\n6. Masukkan jumlah yang akan dibayarkan\n7. Validasi pembayaran. Pastikan semua datanya sudah benar, lalu masukkan kode yang diperoleh dari KEYBCA APPLI 1, kemudian klik “Kirim”\n8. Pembayaran telah selesai dilakukan'),
                    ],
                  )),
            )),
        caraTransfer(
            "Klik BCA Bisnis",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan melakukan login di KlikBCA Bisnis\n2. Pilih “Transfer Dana” > “Daftar Transfer” > “Tambah”\n3. Masukkan nomor BCA Virtual Account, lalu “Kirim”\n4. Pilih “Transfer Dana”\n5. Pilih “Ke BCA Virtual Account”\n6. Pilih rekening sumber dana dan BCA Virtual Account tujuan\n7. Masukkan jumlah yang akan dibayarkan, lalu pilih “Kirim”\n8. Validasi Pembayaran. Sampai tahap ini berarti data berhasil di input. Kemudian pilih “simpan”\n9. Pilih “Transfer Dana” > “Otorisasi Transaksi”, lalu pilih transaksi yang akan diotorisasi\n10. Pembayaran telah selesai dilakukan'),
                    ],
                  )),
            )),
      ],
    );
  }
}
