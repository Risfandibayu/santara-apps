import 'package:flutter/material.dart';
import 'package:santaraapp/widget/token/ListTransaction.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';

class MainHistoryTransaction extends StatefulWidget {
  @override
  _MainHistoryTransactionState createState() => _MainHistoryTransactionState();
}

class _MainHistoryTransactionState extends State<MainHistoryTransaction> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        appBar: CustomAppbar(drawerKey: _drawerKey),
        drawer: CustomDrawer(),
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 50,
              child: DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(50),
                    child: Container(
                      child: SafeArea(
                        child: TabBar(
                            indicatorColor: Color((0xFFBF2D30)),
                            labelColor: Color((0xFFBF2D30)),
                            unselectedLabelColor: Colors.black,
                            tabs: [
                              Container(
                                  key: Key('mainTransactionTab'),
                                  height: 50,
                                  child: Center(child: Text("Transaksi"))),
                              Container(
                                  key: Key('historyTransactionTab'),
                                  height: 50,
                                  child:
                                      Center(child: Text("Riwayat Transaksi"))),
                            ]),
                      ),
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      ListTransaction(index: 0),
                      ListTransaction(index: 1),
                    ],
                  ),
                ),
              ),
            ),
            KycNotificationStatus(
              showCloseButton: false,
            )
          ],
        ));
  }
}
