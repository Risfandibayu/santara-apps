import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CCPayment extends StatefulWidget {
  final String url;
  CCPayment({this.url});
  @override
  _CCPaymentState createState() => _CCPaymentState();
}

class _CCPaymentState extends State<CCPayment> {
  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Image.asset('assets/santara/1.png', width: 110.0),
        ),
        body: IndexedStack(
          index: _stackToView,
          children: <Widget>[
            WebView(
              initialUrl: widget.url,
              javascriptMode: JavascriptMode.unrestricted,
              onPageFinished: _handleLoad,
            ),
            Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ],
        ));
  }
}
