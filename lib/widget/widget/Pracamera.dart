import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/widget/widget/Camera.dart';

class Pracamera extends StatefulWidget {
  final int isKTP;
  Pracamera({this.isKTP});

  @override
  _PracameraState createState() => _PracameraState();
}

class _PracameraState extends State<Pracamera> {
  File file;

  List<CameraDescription> cameras;

  bool isBack = true;

  Future getAvailableCamera() async {
    cameras = await availableCameras();
  }

  @override
  void initState() {
    super.initState();
    getAvailableCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Image.asset('assets/santara/1.png', width: 110.0),
        ),
        body: widget.isKTP == 1 ? _bodyKTP(context) : _bodySelfie(context));
  }

  Widget _item(BuildContext context, String image, String text) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
          height: MediaQuery.of(context).size.width - 60 > 300
              ? 300
              : MediaQuery.of(context).size.width - 60,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(image), fit: BoxFit.fitHeight)),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(60, 0, 60, 30),
          child: Text(
            text,
            style: TextStyle(fontSize: 16),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget _bodyKTP(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 30, 30, 10),
          child: Text(
            "Pastikan foto yang Anda upload sudah memenuhi syarat seperti pada contoh-contoh dibawah:",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
        ),
        _item(context, "assets/icon/ktp_salah1.png",
            "Foto KTP tidak boleh blur (kabur)"),
        _item(context, "assets/icon/ktp_salah2.png",
            "Foto KTP tidak  boleh terpotong"),
        _item(context, "assets/icon/ktp_benar.png",
            "Foto KTP terlihat dengan jelas dan tidak terpotong"),
        InkWell(
          onTap: () async {
            if (cameras.length == 0) {
              await ImagePicker.pickImage(source: ImageSource.camera).then((image) {
                setState(() {
                  file = image;
                });
                Navigator.pop(context, file);
              });
            } else {
              Navigator.push(context, MaterialPageRoute(builder: (_) => Camera(isKyc: 1)));
            }
          },
          child: Container(
            height: 46,
            margin: EdgeInsets.fromLTRB(30, 0, 30, 30),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Color(0xFF1BC47D)),
            child: Center(
              child: Text(
                "Mengerti",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _bodySelfie(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 30, 30, 10),
          child: Text(
            "Pastikan foto yang Anda upload sudah memenuhi syarat seperti pada contoh-contoh dibawah:",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
        ),
        _item(context, "assets/icon/selfie_salah1.png",
            "Foto selfie dengan KTP tidak boleh blur (kabur)"),
        _item(context, "assets/icon/selfie_salah2.png",
            "Muka tidak boleh tertutup dengan KTP"),
        _item(context, "assets/icon/selfie_benar.png",
            "Muka dan KTP terlihat dengan jelas"),
        InkWell(
          onTap: () async {
             if (cameras.length == 0) {
              await ImagePicker.pickImage(source: ImageSource.camera).then((image) {
                setState(() {
                  file = image;
                });
                Navigator.pop(context, file);
              });
            } else {
              Navigator.push(context, MaterialPageRoute(builder: (_) => Camera(isKyc: 1)));
            }
          },
          child: Container(
            height: 46,
            margin: EdgeInsets.fromLTRB(30, 0, 30, 30),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Color(0xFF1BC47D)),
            child: Center(
              child: Text(
                "Mengerti",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        )
      ],
    );
  }
}
