import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class WebviewEvent {}

class WebviewState {}

class WebviewUninitialized extends WebviewState {}

class WebviewError extends WebviewState {
  String error;
  WebviewError({this.error});

  WebviewError copyWith({String error}) {
    return WebviewError(error: error ?? this.error);
  }
}

class WebviewLoaded extends WebviewState {}

class WebviewBloc extends Bloc<WebviewState, WebviewEvent> {
  WebviewBloc(WebviewEvent initialState) : super(initialState);

  Future launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  handleLoaded() {
    
  }

  @override
  Stream<WebviewEvent> mapEventToState(WebviewState event) async* {}
}
