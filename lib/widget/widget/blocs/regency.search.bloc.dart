import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/services/listing/listing_service.dart';

class RegencySearchBlocEvent {
  String selected;
  RegencySearchBlocEvent({this.selected});
}

class RegencySearchBlocState {}

class RegencySearchBlocUninitialized extends RegencySearchBlocState {}

class RegencySearchError extends RegencySearchBlocState {
  String error;
  RegencySearchError({this.error});

  RegencySearchError copyWith({String error}) {
    return RegencySearchError(error: error ?? this.error);
  }
}

class RegencySearchBlocLoaded extends RegencySearchBlocState {
  List<Regency> regencies;
  Regency selected;

  RegencySearchBlocLoaded({this.regencies, this.selected});

  RegencySearchBlocLoaded copyWith(
      {List<Regency> regencies, Regency selected}) {
    return RegencySearchBlocLoaded(
      regencies: regencies ?? this.regencies,
      selected: selected ?? this.selected,
    );
  }
}

class RegencySearchBloc
    extends Bloc<RegencySearchBlocEvent, RegencySearchBlocState> {
  ListingApiService apiService = ListingApiService();
  RegencySearchBloc(RegencySearchBlocState initialState) : super(initialState);

  int convertInt(String value) {
    try {
      return int.parse(value);
    } catch (e) {
      return null;
    }
  }

  @override
  Stream<RegencySearchBlocState> mapEventToState(
      RegencySearchBlocEvent event) async* {
    yield RegencySearchBlocUninitialized();
    var result = await apiService.fetchRegencies();
    if (result != null) {
      Regency selected;
      if (convertInt(event.selected) != null) {
        selected = result
            .firstWhere((element) => element.id == convertInt(event.selected));
      }
      yield RegencySearchBlocLoaded(regencies: result, selected: selected);
    } else {
      yield RegencySearchError(error: "Tidak dapat memuat data!");
    }
  }
}
