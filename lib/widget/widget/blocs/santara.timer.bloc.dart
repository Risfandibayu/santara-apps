import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

class TimerEvent {
  final int seconds;

  TimerEvent({this.seconds});
}

class TimerState {}

class TimerUninitialized extends TimerState {}

class TimerCountdown extends TimerState {
  final String timerText;

  TimerCountdown({this.timerText});

  TimerCountdown copyWith({String timerText}) {
    return TimerCountdown(timerText: timerText ?? this.timerText);
  }
}

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  TimerBloc(TimerState initialState) : super(initialState);
  // initialize variables
  final interval = const Duration(seconds: 1);
  int currentSeconds = 0;
  // String get timerText =>
  //     '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}: ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  @override
  Stream<TimerState> mapEventToState(TimerEvent event) async* {
    // final String seconds;
    var duration = interval;
    Timer.periodic(duration, (timer) {
      currentSeconds = timer.tick;
      
      if (timer.tick >= event.seconds) timer.cancel();
    });
  }
}
