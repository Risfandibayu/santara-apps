import 'dart:async';

import 'package:flutter/material.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewWidget extends StatefulWidget {
  final String appBarName;
  final String url;
  final String method;
  WebViewWidget({this.appBarName, this.url, this.method});
  @override
  _WebViewWidgetState createState() => _WebViewWidgetState(url: url);
}

class _WebViewWidgetState extends State<WebViewWidget> {
  final url;
  _WebViewWidgetState({this.url});

  num _stackToView = 1;
  Completer<WebViewController> _controller = Completer<WebViewController>();

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
    Navigator.pop(context);
  }

  Future<bool> _onWillPop() async {
    return null ?? false;
  }

  @override
  void initState() {
    super.initState();
    // _launchURL(widget.url);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
                widget.appBarName == 'ONEPAY'
                    ? 'OTHER PAYMENT'
                    : widget.appBarName,
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            leading: GestureDetector(
                child: Icon(Icons.arrow_back_ios_outlined, color: Colors.black),
                onTap: () {
                  if (widget.method == "TRANSACTION") {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (_) => Home(
                                  index: 1,
                                )),
                        (route) => false);
                  } else if (widget.method == "PROFILE" ||
                      widget.method == "DEPOSIT") {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (_) => Home(
                                  index: 4,
                                )),
                        (route) => false);
                  } else {
                    Navigator.pop(context);
                  }
                }),
          ),
          body: IndexedStack(
            index: _stackToView,
            children: <Widget>[
              WebView(
                onWebViewCreated: (webViewCreate) {
                  _controller.complete(webViewCreate);
                },
                onWebResourceError: (val) {
                  // print(">> Webview Error Code : ${val.errorCode}");
                  // print(">> Webview Error Desc : ${val.description}");
                  // print(">> Webview Error Type : ${val.errorType}");
                  // print(">> Webview Error Type : ${val.domain}");
                },
                initialUrl: widget.url,
                javascriptMode: JavascriptMode.unrestricted,
                onPageFinished: _handleLoad,
                gestureNavigationEnabled: true,
              ),
              Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ],
          )),
    );
  }
}
