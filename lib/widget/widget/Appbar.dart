import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Notification.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/rev_color.dart';

class CustomAppbar extends StatefulWidget implements PreferredSizeWidget {
  final bottom;
  final GlobalKey<ScaffoldState> drawerKey;
  CustomAppbar({this.bottom, this.drawerKey});
  @override
  _CustomAppbarState createState() => _CustomAppbarState();

  @override
  Size get preferredSize =>
      Size.fromHeight(kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0));
}

class _CustomAppbarState extends State<CustomAppbar> {
  final storage = new FlutterSecureStorage();
  String newNotifString = "";
  List<Notifications> newNotif;
  List<Notifications> oldNotif = [];

  Future getNotification() async {
    final headers = await UserAgent.headers();
    // final token = await storage.read(key: "token");
    final http.Response response =
        await http.get('$apiLocal/notification/', headers: headers);
    if (response.statusCode == 200) {
      storage.write(key: "newNotif", value: response.body);
      final data = userFromJson(await storage.read(key: 'user'));
      await storage.write(key: 'phone', value: data.phone);
      setState(() {
        newNotifString = response.body;
        newNotif = notificationsFromJson(response.body);
        storage.read(key: "oldNotif").then((cache) {
          if (cache != null) {
            oldNotif = notificationsFromJson(cache);
          }
          Helper.notif = newNotif.length - oldNotif.length;
        });
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getNotification();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      shape: Border(
          bottom: BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
      leadingWidth: 200,
      leading: Center(
        child: Image.asset(
          'assets/santara/1.png',
          width: 110.0,
        ),
      ),
      // title: Image.asset('assets/santara/1.png', width: 110.0),
      centerTitle: true,
      backgroundColor: Color(ColorRev.mainBlack),
      actions: <Widget>[
        Row(
          children: [
            Stack(children: <Widget>[
              IconButton(
                  icon: Helper.notif == 0
                      ? Icon(
                          Icons.notifications_none,
                          color: Color(ColorRev.mainwhite),
                          size: 25,
                        )
                      : Icon(
                          Icons.notifications,
                          color: Color(0xFFBF2D30),
                          size: 25,
                        ),
                  onPressed: () {
                    storage.write(key: "oldNotif", value: newNotifString);
                    Navigator.of(context).pushNamed('/notification');
                  }),
              // Helper.notif == 0
              //     ? Container()
              //     : Positioned(
              //         right: 11,
              //         top: 11,
              //         child: Container(
              //           padding: EdgeInsets.all(2),
              //           decoration: BoxDecoration(
              //             color: Colors.red,
              //             borderRadius: BorderRadius.circular(6),
              //           ),
              //           constraints: BoxConstraints(
              //             minWidth: 14,
              //             minHeight: 14,
              //           ),
              //           child: Text('${Helper.notif}',
              //               style: TextStyle(color: Colors.white, fontSize: 8),
              //               textAlign: TextAlign.center),
              //         ),
              //       )
            ]),
            IconButton(
              icon: Icon(Icons.menu),
              color: Color(ColorRev.mainwhite),
              onPressed: () => widget.drawerKey.currentState.openDrawer(),
            ),
          ],
        ),
      ],
      bottom: widget.bottom,
    );
  }
}
