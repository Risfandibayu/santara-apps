import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';

import '../../App.dart';

class ParseSlug extends StatefulWidget {
  final String slug;
  ParseSlug({@required this.slug});

  @override
  _ParseSlugState createState() => _ParseSlugState();
}

class _ParseSlugState extends State<ParseSlug> {
  int retrial = 0;
  getUuid(String slug) async {
    final http.Response response =
        await http.get('$apiLocal/emitens/slug/$slug');
    if (response.statusCode == 200) {
      final result = Emiten.fromJson(json.decode(response.body));
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => EmitenUI(
            uuid: result.uuid,
          ),
        ),
      );
    } else {
      if (retrial < 3) {
        setState(() {
          retrial += 1;
        });
        await getUuid(slug);
      } else {
        ToastHelper.showFailureToast(context, "Data tidak ditemukan!");
        await Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => Main(spesificPage: 0)),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getUuid(widget.slug);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Center(
          child: CupertinoActivityIndicator(),
        ),
      ),
    );
  }
}
