import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  String username = 'Guest';
  String email = 'Guest@gmail.com';
  String token;
  var photo;
  var datas;
  var refreshToken;
  var tokenFirebase;
  final storage = new FlutterSecureStorage();

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    tokenFirebase = await storage.read(key: "tokenFirebase");
    if (token != null) {
      datas = userFromJson(await storage.read(key: 'user'));
      refreshToken = await storage.read(key: "refreshToken");
      final uuid = datas.uuid;
      final http.Response response = await http.get(
          '$apiLocal/users/by-uuid/$uuid',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      setState(() {
        datas = userFromJson(response.body);
        photo = datas.trader.traderType == "company"
            ? datas.trader.companyPhoto
            : datas.trader.photo;
      });
    }
  }

  Future logout(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(child: Center(child: CircularProgressIndicator()));
        });
    final id = datas.id;
    await http.post('$apiLocal/auth/logout', body: {
      "refreshToken": "$refreshToken",
      "user_id": "$id",
    });
    storage.delete(key: 'token');
    storage.delete(key: 'user');
    storage.delete(key: 'uuid');
    storage.delete(key: 'refreshToken');
    storage.delete(key: 'userId');
    storage.delete(key: 'newNotif');
    storage.delete(key: 'oldNotif');
    Helper.check = false;
    Helper.username = "Guest";
    Helper.email = "guest@gmail.com";
    Helper.userId = null;
    Helper.refreshToken = null;
    Helper.notif = 0;
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Home()),
        (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(padding: EdgeInsets.all(0.0), children: <Widget>[
      Container(
        color: Color(ColorRev.mainBlack),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // UserAccountsDrawerHeader(
            //   accountName:
            //       Text(Helper.username == null ? '' : '${Helper.username}'),
            //   accountEmail: Text('${Helper.email}'),
            //   currentAccountPicture: photo == null
            //       ? CircleAvatar(
            //           backgroundColor: Colors.white,
            //           child: Icon(Icons.person),
            //         )
            //       : ClipRRect(
            //           borderRadius: BorderRadius.circular(50),
            //           child: SantaraCachedImage(
            //               height: 100,
            //               width: 100,
            //               image: GetAuthenticatedFile.convertUrl(
            //                 image: photo,
            //                 type: PathType.traderPhoto,
            //               ),
            //               // placeholder: 'assets/icon/user.png',
            //               fit: BoxFit.cover),
            //         ),
            //   decoration: BoxDecoration(color: Color(ColorRev.mainBlack)),
            // ),
            SizedBox(
              height: 50,
            ),
            Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.white,
                      size: 25,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Text('Menu Utama',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  photo == null
                      ? CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(Icons.person),
                        )
                      : ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: SantaraCachedImage(
                              height: 100,
                              width: 100,
                              image: GetAuthenticatedFile.convertUrl(
                                image: photo,
                                type: PathType.traderPhoto,
                              ),
                              // placeholder: 'assets/icon/user.png',
                              fit: BoxFit.cover),
                        ),
                  SizedBox(width: 20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(Helper.username == null ? '' : '${Helper.username}',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                      Text('${Helper.email}',
                          style: TextStyle(color: Colors.white))
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Container(
                height: 0.3,
                color: Color(0xFFF0F0F0),
                width: MediaQuery.of(context).size.width),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                'Seputar Santara',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            SizedBox(height: 10),
            ListTile(
              title: Text(
                'Tentang Santara',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/info-circle.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/tentang');
              },
            ),
            ListTile(
              title: Text(
                'Cara Investasi',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/lamp-on.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/petunjuk');
              },
            ),
            ListTile(
              title: Text(
                'Berita Santara',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/firstline.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/berita');
              },
            ),
            ListTile(
              title: Text(
                'Pertanyaan',
                style: TextStyle(color: Colors.white),
              ),
              leading:
                  Image.asset('assets/icon/message-question.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/faq');
              },
            ),
            ListTile(
              title: Text(
                'Syarat & Ketentuan Pemodal',
                style: TextStyle(color: Colors.white),
              ),
              leading:
                  Image.asset('assets/icon/clipboard-text.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/snkPemodal');
              },
            ),
            ListTile(
              title: Text(
                'Syarat & Ketentuan Penerbit',
                style: TextStyle(color: Colors.white),
              ),
              leading:
                  Image.asset('assets/icon/clipboard-text.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/snkPenerbit');
              },
            ),
            ListTile(
              title: Text(
                'Kebijakan & Privasi',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/security-user.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/kebijakan');
              },
            ),
            ListTile(
              title: Text(
                'Disclaimer',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/danger.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/disclaimer');
              },
            ),
            ListTile(
              title: Text(
                'Info App',
                style: TextStyle(color: Colors.white),
              ),
              leading: Image.asset('assets/icon/info-circle.png', scale: 3.3),
              onTap: () {
                Navigator.pushNamed(context, '/app_info');
              },
            ),

            Helper.check == true
                ? ListTile(
                    title: Text(
                      'Keluar',
                      style: TextStyle(color: Colors.white),
                    ),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                                backgroundColor: Color(ColorRev.mainBlack),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Image.asset(
                                            'assets/icon/updateupdate.png'),
                                      ],
                                    ),
                                    Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Text('Logout dari Santara',
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Color(
                                                    ColorRev.mainwhite)))),
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Text(
                                        'Apakah Anda ingin logout?',
                                        style: TextStyle(
                                            color: Color(ColorRev.mainwhite)),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.05,
                                          decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              border: Border.all(
                                                  color: Colors.white),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          margin: EdgeInsets.only(top: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: FlatButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              'Batal',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.05,
                                          decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          margin: EdgeInsets.only(top: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: FlatButton(
                                            onPressed: () {
                                              logout(context);
                                            },
                                            child: Text(
                                              'Keluar',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ));

                            // return AlertDialog(
                            //   title: Text('Logout dari Santara'),
                            //   content: Text('Apakah Anda ingin logout?'),
                            //   actions: <Widget>[
                            //     FlatButton(
                            //       child: Text('No'),
                            //       onPressed: () {
                            //         Navigator.of(context).pop();
                            //       },
                            //     ),
                            //     FlatButton(
                            //       child: Text('Yes'),
                            //       onPressed: () {
                            //         logout(context);
                            //       },
                            //     )
                            //   ],
                            // );
                          });
                    },
                    leading: Image.asset('assets/icon/logout.png', scale: 3.3),
                  )
                : Column(
                    children: [
                      SizedBox(height: 30),
                      Container(
                          height: 0.3,
                          color: Color(0xFFF0F0F0),
                          width: MediaQuery.of(context).size.width),
                      SizedBox(height: 30),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, '/login');
                        },
                        child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Colors.red[500],
                                border: Border.all(),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.05,
                            alignment: Alignment.center,
                            child: Text(
                              "Masuk",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )),
                      ),
                      SizedBox(height: 10),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, '/register');
                        },
                        child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                border: Border.all(color: Colors.white),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.05,
                            alignment: Alignment.center,
                            child: Text(
                              "Daftar",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )),
                      ),
                    ],
                  ),
          ],
        ),
      ),
    ]));
  }
}
