import 'dart:async';
import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';

class Camera extends StatefulWidget {
  final int isKyc;
  final int isSelfie;

  Camera({this.isKyc, this.isSelfie});

  @override
  _CameraState createState() => _CameraState();
}

class _CameraState extends State<Camera> {
  CameraController controller;
  String imagePath;
  String videoPath;
  VoidCallback videoPlayerListener;
  List<CameraDescription> cameras;
  bool isBack = true;

  Future getAvailableCamera() async {
    cameras = await availableCameras();
    Future.delayed(Duration(seconds: 1), () {
      if (widget.isSelfie == 1) {
        controller = CameraController(cameras[1], ResolutionPreset.max);
        onNewCameraSelected(cameras[1]);
      } else {
        controller = CameraController(cameras[0], ResolutionPreset.max);
        onNewCameraSelected(cameras[0]);
      }
    });
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.max);

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });
      }
    });
  }

  String timestamp() =>
      DateTime
          .now()
          .millisecondsSinceEpoch
          .toString();

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        '',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: Stack(
          children: [
            CameraPreview(controller),
            SizedBox(
              child: Container(
                  child: widget.isSelfie == 1
                      ? Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      child: Image.asset('assets/frameselfie.png'))
                      : Center(
                    child: Container(
                      width: 300,
                      height: 200,
                      child: DottedBorder(
                          color: Color(0xFFB8B8B8),
                          strokeWidth: 2,
                          radius: Radius.circular(15),
                          dashPattern: [5, 5],
                          child: Container()),
                    ),
                  )),
            ),
            widget.isSelfie == 0 ? SizedBox(
              child: Center(
                child: Container(
                  color: Colors.transparent,
                  width: 300,
                  height: 200,
                  child: DottedBorder(
                      color: Color(0xFFB8B8B8),
                      strokeWidth: 2,
                      radius: Radius.circular(15),
                      dashPattern: [5, 5],
                      child: Container()),
                ),
              ),
            ) : Container(),
          ],
        ),
      );
    }
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
  }

  void logError(String code, String message) =>
      debugPrint('Error: $code\nError Message: $message');

  Future<bool> cameraPermissionRequest() async {
    try {
      // Cek permission camera
      PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.camera);
      PermissionStatus microPhone = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.microphone);

      // jika belum request camera
      if (permission != PermissionStatus.granted) {
        // do request
        await PermissionHandler().requestPermissions([PermissionGroup.camera]);
      }

      // jika belum request microphone
      if (microPhone != PermissionStatus.granted) {
        // do request
        await PermissionHandler()
            .requestPermissions([PermissionGroup.microphone]);
      }

      if (permission == PermissionStatus.denied ||
          permission == PermissionStatus.neverAskAgain ||
          microPhone == PermissionStatus.denied ||
          microPhone == PermissionStatus.neverAskAgain) {
        return false;
      }
      return true;
    } catch (e) {
      ToastHelper.showFailureToast(
        context,
        "Tidak dapat mengakses kamera!",
      );
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    imagePath = null;
    FirebaseAnalytics()
        .logEvent(name: 'camera_view', parameters: {'status': 'camera_opened'});
    cameraPermissionRequest().then((value) {
      getAvailableCamera();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Center(
                        child: imagePath == null
                            ? _cameraPreviewWidget()
                            : Image.file(File(imagePath),
                            fit: BoxFit.fitWidth,
                            width: MediaQuery
                                .of(context)
                                .size
                                .width),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        border: Border.all(
                          color: controller != null &&
                              controller.value.isRecordingVideo
                              ? Colors.redAccent
                              : Colors.grey,
                          width: 3.0,
                        ),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16, top: 16),
                  child: imagePath == null
                      ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      widget.isSelfie == 1
                          ? Container()
                          : Container(width: 50),
                      Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(width: 2, color: Colors.grey),
                        ),
                        child: InkWell(
                          highlightColor: null,
                          splashColor: null,
                          onTap: controller != null &&
                              controller.value.isInitialized &&
                              !controller.value.isRecordingVideo
                              ? onTakePictureButtonPressed
                              : null,
                          child: Icon(MdiIcons.camera, size: 30),
                        ),
                      ),
                      widget.isSelfie == 1
                          ? Container()
                          : Container(
                        width: 50,
                        child: IconButton(
                          icon: Icon(MdiIcons.rotate3DVariant),
                          onPressed: () {
                            if (isBack) {
                              setState(() {
                                isBack = false;
                                onNewCameraSelected(cameras[1]);
                              });
                            } else {
                              setState(() {
                                isBack = true;
                                onNewCameraSelected(cameras[0]);
                              });
                            }
                          },
                        ),
                      ),
                    ],
                  )
                      : Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 2, color: Colors.grey),
                          ),
                          child: InkWell(
                              onTap: () {
                                setState(() => imagePath = null);
                              },
                              child: Icon(Icons.close)),
                        ),
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(width: 2, color: Colors.grey),
                          ),
                          child: InkWell(
                              onTap: () {
                                if (widget.isKyc == 1) {
                                  Navigator.pop(context, File(imagePath));
                                  Navigator.pop(context, File(imagePath));
                                } else if (widget.isKyc == 2) {
                                  Navigator.pop(context, File(imagePath));
                                } else {
                                  Navigator.pop(context, File(imagePath));
                                }
                              },
                              child: Icon(Icons.check)),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Platform.isIOS
                ? Padding(
                padding: const EdgeInsets.only(top: 24),
                child: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.pop(context, null),
                ))
                : Container(),
          ],
        ));
  }
}
