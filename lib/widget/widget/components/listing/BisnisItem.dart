import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/BisnisItemHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/DiajukanListModel.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';

import 'ListingListContent.dart';
import 'SantaraNotification.dart';

enum BisnisPopAction { delete, edit, repost }

class BisnisItem extends StatelessWidget {
  final String tanggal;
  final Function onDelete;
  final Function onRepost;
  final Function onEdit;
  final VoidCallback onCheckMail;
  final VoidCallback onTap;
  final DiajukanListModel data;
  // final int status;
  final String statusString; // penyesuaian dengan api mas irfan

  // constructor
  BisnisItem(
      {@required this.tanggal,
      @required this.onDelete,
      @required this.onEdit,
      @required this.onRepost,
      @required this.onTap,
      @required this.onCheckMail,
      @required this.data,
      // @required this.status,
      @required this.statusString});

  // status :
  // 0 = Sedang diproses
  // 1 = Tidak lolos ditayangkan
  // 2 = Tidak lolos screening
  // 3 = Pengajuan didanai!
  TextStyle _msgStyle() {
    var _size = SizeConfig.safeBlockHorizontal;
    return TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontSize: _size * 2.875);
  }

  Widget _notifBuilder(BuildContext context, String status) {
    var _size = SizeConfig.safeBlockHorizontal;
    var _message = "";
    var _message2 = "";
    var _height;

    Color _color;
    switch (status) {
      case "waiting for verification":
        _message =
            "Pengajuan bisnis ini sedang diproses dan akan segera ditayangkan";
        _color = Color(0xffEEF1FF);
        _height = _size * 13;
        break;
      case "rejected":
        _message =
            "Pengajuan bisnis tidak lolos untuk ditayangkan. Silakan cek E-mail anda untuk info selengkapnya. ";
        _message2 = " Cek E-mail";
        _color = Color(0xffFEE0E0);
        _height = _size * 15;
        break;
      // case "rejected airing":
      //   _message =
      //       "Pengajuan bisnis tidak lolos untuk ditayangkan. Silakan cek E-mail anda untuk info selengkapnya. ";
      //   _message2 = " Cek E-mail";
      //   _color = Color(0xffFEE0E0);
      //   _height = _size * 15;
      //   break;
      // case "rejected screeening":
      //   _message =
      //       "Pengajuan bisnis tidak lolos tahap screening.. Silakan cek E-mail anda untuk info selengkapnya. ";
      //   _message2 = " Cek E-mail";
      //   _color = Color(0xffFEE0E0);
      //   _height = _size * 10.5;
      //   break;
      case "verified":
        _message = "Pengajuan Didanai";
        _color = Color(0xffF1F1F1);
        _height = _size * 10.5;
        break;
      default:
        _message = "";
        _color = Colors.transparent;
        _height = _size * 0;
        break;
    }
    return SantaraNotification(
      message: RichText(
        text: TextSpan(
          text: _message,
          style: _msgStyle(),
          children: _message2.isNotEmpty
              ? <TextSpan>[
                  TextSpan(
                      text: _message2,
                      style: TextStyle(
                          color: Colors.black,
                          height: 1.5,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid,
                          fontWeight: FontWeight.bold,
                          fontSize: _size * 2.875),
                      recognizer: null)
                ]
              : null,
        ),
      ),
      backgroundColor: _color,
      height: _height,
      margin: EdgeInsets.fromLTRB(_size * 2, _size, _size * 2, _size * 5),
    );
  }

  Widget _engagmentBuilder(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      width: SizeConfig.screenWidth * .4,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          statusString == "verified"
              ? TextIconView(
                  iconData: Icons.person_outline, text: "${data.votes}" ?? "")
              : TextIconView(iconData: Icons.person_outline, text: "-"),
          Container(
            width: _size,
          ),
          statusString == "verified"
              ? TextIconView(
                  iconData: Icons.favorite_border,
                  text: "${data.likes}" ?? "",
                )
              : TextIconView(
                  iconData: Icons.favorite_border,
                  text: "-",
                ),
          Container(
            width: _size,
          ),
          statusString == "verified"
              ? TextIconView(
                  iconData: Icons.chat_bubble_outline,
                  text: "${data.comments}",
                )
              : TextIconView(
                  iconData: Icons.chat_bubble_outline,
                  text: "-",
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // print(
    //     apiLocalImage + "/uploads/emiten_picture/" + data.pictures[0].picture);
    var _size = SizeConfig.safeBlockHorizontal;
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(bottom: _size * 5),
        width: double.infinity,
        // height: status < 4
        //     ? SizeConfig.screenHeight / 3.3
        //     : SizeConfig.screenHeight / 4.5,
        // height: SizeConfig.isTablet(context)
        //     ? SizeConfig.screenHeight / 3
        //     : SizeConfig.screenHeight / 3,
        height: SizeConfig.screenHeight / 3,
        // padding: EdgeInsets.all(_size * 3),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xffDADADA)),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          // mainAxisAlignment: status < 4
          //     ? MainAxisAlignment.spaceBetween
          //     : MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: _size * 8,
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(_size * 5, _size, _size, _size),
              decoration: BoxDecoration(
                color: Color(0xffF0F0F0),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Dari: $tanggal",
                    style: TextStyle(fontSize: _size * 3),
                  ),
                  Spacer(),
                  statusString == "verified"
                      ? Container()
                      : Container(
                          child: Container(
                            child: PopupMenuButton<BisnisPopAction>(
                              padding: EdgeInsets.all(0),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3.0),
                                  side: BorderSide(
                                      color: Color(0xffD6D6D6), width: 1)),
                              // margin: EdgeInsets.all(0),
                              onSelected: (BisnisPopAction result) {
                                switch (result) {
                                  case BisnisPopAction.delete:
                                    onDelete();
                                    break;
                                  case BisnisPopAction.edit:
                                    onEdit();
                                    break;
                                  case BisnisPopAction.repost:
                                    onRepost();
                                    break;
                                  default:
                                    // print("Unknown Action");
                                    break;
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<BisnisPopAction>>[
                                BisnisItemHelper.showRepost(statusString)
                                    ? PopupMenuItem<BisnisPopAction>(
                                        height: _size * 10,
                                        value: BisnisPopAction.repost,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/icon/repost.png",
                                              width: _size * 5,
                                              height: _size * 5,
                                            ),
                                            Container(
                                              width: _size * 3,
                                            ),
                                            Text("Ajukan Kembali")
                                          ],
                                        ))
                                    : null,
                                BisnisItemHelper.showEdit(statusString)
                                    ? PopupMenuItem<BisnisPopAction>(
                                        height: _size * 10,
                                        value: BisnisPopAction.edit,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/icon/edit.png",
                                              width: _size * 5,
                                              height: _size * 5,
                                            ),
                                            Container(
                                              width: _size * 3,
                                            ),
                                            Text("Edit")
                                          ],
                                        ))
                                    : null,
                                BisnisItemHelper.showDelete(statusString)
                                    ? PopupMenuItem<BisnisPopAction>(
                                        height: _size * 10,
                                        value: BisnisPopAction.delete,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.asset(
                                              "assets/icon/delete.png",
                                              width: _size * 5,
                                              height: _size * 5,
                                            ),
                                            Container(
                                              width: _size * 3,
                                            ),
                                            Text("Hapus")
                                          ],
                                        ))
                                    : null,
                              ],
                            ),
                          ),
                        )
                ],
              ),
            ),
            Container(
              height: _size,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(_size * 5, _size, _size * 5, _size),
              width: double.infinity,
              height: SizeConfig.screenHeight / 7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    child: SantaraCachedImage(
                      height: SizeConfig.screenHeight / 7.5,
                      width: SizeConfig.screenHeight / 7.5,
                      // placeholder: 'assets/Preload.jpeg',
                      image: apiLocalImage +
                          "/uploads/emiten_picture/" +
                          data.pictures[0].picture,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    width: _size * 3,
                  ),
                  Container(
                    width: SizeConfig.screenWidth * .45,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            "${data.category}",
                            maxLines: 2,
                            style: TextStyle(
                                color: Color(0xff292F8D),
                                fontSize: SizeConfig.safeBlockHorizontal * 3),
                          ),
                        ),
                        Text(
                          "${data.trademark}",
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "${data.companyName}",
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 2.75),
                        ),
                        Container(
                          height: _size * 3,
                        ),
                        _engagmentBuilder(context)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Spacer(),
            InkWell(
                onTap: onCheckMail,
                child: Container(child: _notifBuilder(context, statusString))),
            // Container()
            // status < 4
            //     ? Container()
            //     : Container(
            //         width: _size * 6,
            //       ),
          ],
        ),
      ),
    );
  }
}

class BisnisItemLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(bottom: _size * 5),
      width: double.infinity,
      height: SizeConfig.screenHeight / 4,
      // padding: EdgeInsets.all(_size * 3),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Color(0xffDADADA)),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: _size * 8,
            width: double.infinity,
            padding: EdgeInsets.fromLTRB(_size * 5, _size, _size, _size),
            decoration: BoxDecoration(
              color: Color(0xffF0F0F0),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ListingLoaderSegment(
                  height: _size * 3,
                  width: SizeConfig.screenWidth * .3,
                ),
                Spacer(),
                ListingLoaderSegment(
                  width: _size * 5,
                  height: _size * 5,
                )
              ],
            ),
          ),
          Container(
            height: _size,
          ),
          Container(
            margin: EdgeInsets.fromLTRB(_size * 5, _size, _size * 5, _size),
            width: double.infinity,
            height: SizeConfig.screenHeight / 8,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  child: ListingLoaderSegment(
                    height: SizeConfig.screenHeight / 7.5,
                    width: SizeConfig.screenHeight / 7.5,
                  ),
                ),
                Container(
                  width: _size * 3,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    ListingLoaderSegment(
                      height: _size * 3,
                      width: SizeConfig.screenWidth * .4,
                    ),
                    ListingLoaderSegment(
                      height: _size * 3,
                      width: SizeConfig.screenWidth * .3,
                    ),
                    ListingLoaderSegment(
                      height: _size * 3,
                      width: SizeConfig.screenWidth * .25,
                    ),
                    Container(
                      height: _size * 3,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        ListingLoaderSegment(
                          height: _size * 3,
                          width: SizeConfig.screenWidth * .08,
                        ),
                        Container(
                          width: _size * 3,
                        ),
                        ListingLoaderSegment(
                          height: _size * 3,
                          width: SizeConfig.screenWidth * .08,
                        ),
                        Container(
                          width: _size * 3,
                        ),
                        ListingLoaderSegment(
                          height: _size * 3,
                          width: SizeConfig.screenWidth * .08,
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(_size * 2, _size, _size * 2, _size * 5),
            child: ListingLoaderSegment(
              height: _size * 5,
              width: SizeConfig.screenWidth,
            ),
          ),
        ],
      ),
    );
  }
}

class KategoriLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: SizeConfig.safeBlockHorizontal * 9,
        decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Color(0xffdadada),
            ),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        width: SizeConfig.safeBlockHorizontal * 42,
        margin: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 5),
        padding: EdgeInsets.only(
            left: SizeConfig.safeBlockHorizontal * 2,
            right: SizeConfig.safeBlockHorizontal * 2),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CupertinoActivityIndicator(),
            Container(
              width: SizeConfig.safeBlockHorizontal * 4,
            ),
            ListingLoaderSegment(
              height: SizeConfig.safeBlockHorizontal * 5,
              width: SizeConfig.safeBlockHorizontal * 27,
            )
          ],
        ));
  }
}
