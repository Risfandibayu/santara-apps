import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';

class ListingGridContent extends StatelessWidget {
  final String image;
  final String tagName;
  final String businessName;
  final String companyName;
  final String submissions;
  final String likes;
  final String comments;
  ListingGridContent(
      {this.image,
      this.tagName,
      this.businessName,
      this.companyName,
      this.submissions,
      this.likes,
      this.comments});
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5), topRight: Radius.circular(5)),
              child: SantaraCachedImage(
                height: SizeConfig.screenHeight / 4,
                // placeholder: 'assets/Preload.jpeg',
                image: image,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    tagName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Color(0xff292F8D)),
                  ),
                  Text(businessName),
                  Text(
                    companyName,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: SizeConfig.safeBlockHorizontal * 2.6),
                  ),
                  Container(
                    height: 25,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      TextIconView(
                        iconData: Icons.check,
                        text: "$submissions",
                      ),
                      TextIconView(
                        iconData: Icons.favorite_border,
                        text: "$likes",
                      ),
                      TextIconView(
                        iconData: Icons.chat_bubble_outline,
                        text: "$comments",
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
