import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/Pralisting.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';
import 'package:shimmer/shimmer.dart';
import 'ListingListContent.dart';

class BusinessTile extends StatelessWidget {
  final int index;
  final bool isPengajuan;
  final PralistingEmiten data;
  final int
      status; // 0 = tidak ada pengajuan, 1 = menunggu verifikasi, 2 = tidak lolos verifikasi, 3 = lolos verifikasi
  final VoidCallback onTap;
  final VoidCallback onDelete;
  final bool showEngagement;
  final bool showPosition;
  BusinessTile(
      {this.index,
      this.data,
      this.isPengajuan = false,
      this.status = 0,
      @required this.onTap,
      @required this.onDelete,
      this.showEngagement = false,
      this.showPosition = false});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
        margin:
            EdgeInsets.fromLTRB(_size * 5, _size * 1.5, _size * 5, _size * 1.5),
        height: isPengajuan
            ? SizeConfig.screenHeight / 4
            : SizeConfig.screenHeight / 4.5,
        decoration: BoxDecoration(
            color: status == 3
                ? Color.fromRGBO(236, 236, 236, 0.95)
                : Colors.transparent,
            border: Border.all(
                color: Color(0xffDADADA), width: 1.0, style: BorderStyle.solid),
            borderRadius: BorderRadius.all(Radius.circular(4))),
        child: Stack(
          children: <Widget>[
            InkWell(
              onTap: onTap,
              child: Container(
                padding: EdgeInsets.all(_size * 3.75),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      child: SantaraCachedImage(
                        height: SizeConfig.screenHeight / 6,
                        width: SizeConfig.screenHeight / 8,
                        image: data.pictures,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: _size * 3.75,
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: SizeConfig.screenHeight / 6,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    data.category ?? "",
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal * 3),
                                  ),
                                  Text(
                                    data.trademark ?? "",
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                3.5,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    data.companyName ?? "",
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                2.7),
                                  ),
                                ]),
                            Container(),
                            Text(
                              "${data.businessDescription}",
                              style: TextStyle(
                                  fontSize:
                                      SizeConfig.safeBlockHorizontal * 2.5,
                                  color: Colors.grey),
                              maxLines: SizeConfig.isTablet(context) ? 1 : 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            isPengajuan
                                ? status == 1
                                    ? Text(
                                        "Menunggu Verifikasi",
                                        style: TextStyle(
                                            color: Color(0xffE5A037),
                                            fontSize:
                                                SizeConfig.safeBlockHorizontal *
                                                    3,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : status == 2
                                        ? Text(
                                            "Tidak Lolos Verifikasi",
                                            style: TextStyle(
                                                color: Color(0xffBF2D30),
                                                fontSize: SizeConfig
                                                        .safeBlockHorizontal *
                                                    3,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : status == 3
                                            ? Text(
                                                "Lolos Verifikasi",
                                                style: TextStyle(
                                                    color: Color(0xff0E7E4A),
                                                    fontSize: SizeConfig
                                                            .safeBlockHorizontal *
                                                        3,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )
                                            : Container()
                                : showEngagement
                                    ? Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          TextIconView(
                                            isActive:
                                                data.isVote == 1 ? true : false,
                                            iconData: data.isVote == 1
                                                ? Icons.person
                                                : Icons.person_outline,
                                            text: "${data.totalVotes}",
                                          ),
                                          TextIconView(
                                            isActive: data.isLikes == 1
                                                ? true
                                                : false,
                                            iconData: data.isLikes == 1
                                                ? Icons.favorite
                                                : Icons.favorite_border,
                                            text: "${data.totalLikes}",
                                          ),
                                          TextIconView(
                                            isActive: data.isComment == 1
                                                ? true
                                                : false,
                                            iconData: data.isComment == 1
                                                ? Icons.chat_bubble
                                                : Icons.chat_bubble_outline,
                                            text: "${data.totalComments}",
                                          )
                                        ],
                                      )
                                    : Container()
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            isPengajuan
                ? Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: SizeConfig.safeBlockHorizontal * 8,
                      height: SizeConfig.safeBlockHorizontal * 8,
                      child: InkWell(
                        onTap: onDelete,
                        child: Align(
                          alignment: Alignment.topRight,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            child: Container(
                              decoration: BoxDecoration(shape: BoxShape.circle),
                              child: Icon(Icons.close,
                                  color: Colors.grey,
                                  size: SizeConfig.safeBlockHorizontal * 4),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
            status == 3
                ? Container(
                    color: Color.fromRGBO(236, 236, 236, 0.58),
                    width: double.infinity,
                    height: SizeConfig.screenHeight / 4.8,
                  )
                : Container(),
            showPosition
                ? Positioned.fill(
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: _size * 20,
                          height: _size * 20,
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.only(topLeft: Radius.circular(3)),
                            child: Triangle(text: (index + 1).toString()),
                          ),
                        )),
                  )
                : Container(),
          ],
        ));
  }
}

// Punya mas ryan pakai yg ini ya
class BusinessTile2 extends StatelessWidget {
  final int index;
  final bool isPengajuan;
  final DataPralisting data;
  final int
      status; // 0 = tidak ada pengajuan, 1 = menunggu verifikasi, 2 = tidak lolos verifikasi, 3 = lolos verifikasi
  final VoidCallback onTap;
  final VoidCallback onDelete;
  final bool showEngagement;
  final bool showPosition;
  BusinessTile2(
      {this.index,
      this.data,
      this.isPengajuan = false,
      this.status = 0,
      @required this.onTap,
      @required this.onDelete,
      this.showEngagement = false,
      this.showPosition = false});

  @override
  Widget build(BuildContext context) {
    var foto = data.pictures;
    var splitag = foto.split(",");
    // SizeConfig().init(context);
    // var _size = SizeConfig.safeBlockHorizontal;

    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      padding: EdgeInsets.only(bottom: 8),
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      decoration: BoxDecoration(
        color: Color(ColorRev.maingrey),
        borderRadius: BorderRadius.circular(10),
        // border: Border.all(width: 1, color: Color(0xFFE6EAFA)),
        // boxShadow: [
        //   new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
        // ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Stack(
            children: <Widget>[
              SantaraCachedImage(
                height: MediaQuery.of(context).size.height * 0.33,
                width: MediaQuery.of(context).size.width * 0.75,
                image:
                    'https://storage.googleapis.com/santara-bucket-prod/pralisting/emitens_pictures/' +
                        '${splitag[0]}',
                fit: BoxFit.cover,
              ),
            ],
          ),

          //content
          SizedBox(height: 10),
          Container(
            decoration: BoxDecoration(
                color: Color(0xff7F1D1D),
                border: Border.all(
                  color: Color(0xff7F1D1D),
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                data.category,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Inter',
                  fontSize: 10,
                ),
                overflow: TextOverflow.visible,
              ),
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      data.companyName,
                      style: TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Color(ColorRev.mainwhite)),
                      maxLines: 2,
                      overflow: TextOverflow.visible,
                    ),
                    SizedBox(width: 5),
                    Image.asset('assets/santara/check-verified.png', scale: 4.4)
                  ],
                ),
                Text(
                  "${data.companyName}",
                  style: TextStyle(
                      fontFamily: 'Inter',
                      fontSize: 12,
                      color: Color(ColorRev.mainwhite)),
                  maxLines: 2,
                  overflow: TextOverflow.visible,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      children: [
                        Icon(Icons.person_outline, color: Colors.white),
                        Text(
                          " ${data.votes}",
                          style: TextStyle(
                              fontFamily: 'Inter',
                              fontSize: 12,
                              color: Color(ColorRev.mainwhite)),
                          maxLines: 2,
                          overflow: TextOverflow.visible,
                        ),
                      ],
                    ),
                    SizedBox(width: 20),
                    Row(
                      children: [
                        Icon(Icons.favorite_border, color: Colors.white),
                        Text(
                          " ${data.likes}",
                          style: TextStyle(
                              fontFamily: 'Inter',
                              fontSize: 12,
                              color: Color(ColorRev.mainwhite)),
                          maxLines: 2,
                          overflow: TextOverflow.visible,
                        ),
                      ],
                    ),
                    SizedBox(width: 20),
                    Row(
                      children: [
                        Icon(Icons.message, color: Colors.white),
                        Text(
                          " ${data.comments}",
                          style: TextStyle(
                              fontFamily: 'Inter',
                              fontSize: 12,
                              color: Color(ColorRev.mainwhite)),
                          maxLines: 2,
                          overflow: TextOverflow.visible,
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          ),

          Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            height: 1,
            color: Color(0xFFF0F0F0),
            width: MediaQuery.of(context).size.width * 0.7,
          ),
          SizedBox(height: 10),
          InkWell(
            onTap: onTap,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              width: MediaQuery.of(context).size.width * 0.7,
              height: MediaQuery.of(context).size.height * 0.05,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(
                    color: Color(0xFFF0F0F0),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Center(
                  child: Text(
                    "Dukung Bisnis",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Inter',
                      fontSize: 14,
                    ),
                    overflow: TextOverflow.visible,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
        ],
      ),
    );

    // Container(
    //     margin:
    //         EdgeInsets.fromLTRB(_size * 5, _size * 1.5, _size * 5, _size * 1.5),
    //     height: isPengajuan
    //         ? SizeConfig.screenHeight / 4
    //         : SizeConfig.screenHeight / 4.5,
    //     decoration: BoxDecoration(
    //         color: status == 3
    //             ? Color.fromRGBO(236, 236, 236, 0.95)
    //             : Colors.transparent,
    //         border: Border.all(
    //             color: Color(0xffDADADA), width: 1.0, style: BorderStyle.solid),
    //         borderRadius: BorderRadius.all(Radius.circular(4))),
    //     child: Stack(
    //       children: <Widget>[
    //         InkWell(
    //           onTap: onTap,
    //           child: Container(
    //             padding: EdgeInsets.all(_size * 3.75),
    //             child: Row(
    //               mainAxisSize: MainAxisSize.max,
    //               mainAxisAlignment: MainAxisAlignment.start,
    //               crossAxisAlignment: CrossAxisAlignment.start,
    //               children: <Widget>[
    //                 ClipRRect(
    //                   borderRadius: BorderRadius.all(Radius.circular(5)),
    //                   child: SantaraCachedImage(
    //                     height: SizeConfig.screenHeight / 6,
    //                     width: SizeConfig.screenHeight / 8,
    //                     image:
    //                         '${data.pictures != null && data.pictures.length > 0 ? data.pictures[0].picture : ''}',
    //                     fit: BoxFit.cover,
    //                   ),
    //                 ),
    //                 Container(
    //                   width: _size * 3.75,
    //                 ),
    //                 Expanded(
    //                   flex: 1,
    //                   child: Container(
    //                     height: SizeConfig.screenHeight / 6,
    //                     child: Column(
    //                       mainAxisSize: MainAxisSize.min,
    //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                       crossAxisAlignment: CrossAxisAlignment.stretch,
    //                       children: <Widget>[
    //                         Column(
    //                             crossAxisAlignment: CrossAxisAlignment.start,
    //                             mainAxisSize: MainAxisSize.min,
    //                             children: <Widget>[
    //                               Text(
    //                                 data.category ?? "",
    //                                 style: TextStyle(
    //                                     color: Colors.red,
    //                                     fontSize:
    //                                         SizeConfig.safeBlockHorizontal * 3),
    //                               ),
    //                               Text(
    //                                 data.trademark ?? "",
    //                                 style: TextStyle(
    //                                     fontSize:
    //                                         SizeConfig.safeBlockHorizontal *
    //                                             3.5,
    //                                     fontWeight: FontWeight.bold,
    //                                     color: Colors.white),
    //                               ),
    //                               Text(
    //                                 data.companyName ?? "",
    //                                 style: TextStyle(
    //                                     color: Colors.white,
    //                                     fontSize:
    //                                         SizeConfig.safeBlockHorizontal *
    //                                             2.7),
    //                               ),
    //                             ]),
    //                         Container(),
    //                         Text(
    //                           "${data.businessDescription}",
    //                           maxLines: SizeConfig.isTablet(context) ? 1 : 2,
    //                           overflow: TextOverflow.ellipsis,
    //                           style: TextStyle(
    //                               fontSize:
    //                                   SizeConfig.safeBlockHorizontal * 2.5,
    //                               color: Colors.white),
    //                         ),
    //                         isPengajuan
    //                             ? status == 1
    //                                 ? Text(
    //                                     "Menunggu Verifikasi",
    //                                     style: TextStyle(
    //                                         color: Color(0xffE5A037),
    //                                         fontSize:
    //                                             SizeConfig.safeBlockHorizontal *
    //                                                 3,
    //                                         fontWeight: FontWeight.bold),
    //                                   )
    //                                 : status == 2
    //                                     ? Text(
    //                                         "Tidak Lolos Verifikasi",
    //                                         style: TextStyle(
    //                                             color: Color(0xffBF2D30),
    //                                             fontSize: SizeConfig
    //                                                     .safeBlockHorizontal *
    //                                                 3,
    //                                             fontWeight: FontWeight.bold),
    //                                       )
    //                                     : status == 3
    //                                         ? Text(
    //                                             "Lolos Verifikasi",
    //                                             style: TextStyle(
    //                                                 color: Color(0xff0E7E4A),
    //                                                 fontSize: SizeConfig
    //                                                         .safeBlockHorizontal *
    //                                                     3,
    //                                                 fontWeight:
    //                                                     FontWeight.bold),
    //                                           )
    //                                         : Container()
    //                             : showEngagement
    //                                 ? Row(
    //                                     mainAxisAlignment:
    //                                         MainAxisAlignment.spaceBetween,
    //                                     mainAxisSize: MainAxisSize.max,
    //                                     children: <Widget>[
    //                                       TextIconView(
    //                                         isActive:
    //                                             data.isVote == 1 ? true : false,
    //                                         iconData: data.isVote == 1
    //                                             ? Icons.person
    //                                             : Icons.person_outline,
    //                                         text: "${data.votes}",
    //                                       ),
    //                                       TextIconView(
    //                                         isActive: data.isLikes == 1
    //                                             ? true
    //                                             : false,
    //                                         iconData: data.isLikes == 1
    //                                             ? Icons.favorite
    //                                             : Icons.favorite_border,
    //                                         text: "${data.likes}",
    //                                       ),
    //                                       TextIconView(
    //                                         isActive: data.isComment == 1
    //                                             ? true
    //                                             : false,
    //                                         iconData: data.isComment == 1
    //                                             ? Icons.chat_bubble
    //                                             : Icons.chat_bubble_outline,
    //                                         text: "${data.comments}",
    //                                       )
    //                                     ],
    //                                   )
    //                                 : Container()
    //                       ],
    //                     ),
    //                   ),
    //                 )
    //               ],
    //             ),
    //           ),
    //         ),
    //         isPengajuan
    //             ? Align(
    //                 alignment: Alignment.topRight,
    //                 child: Container(
    //                   width: SizeConfig.safeBlockHorizontal * 8,
    //                   height: SizeConfig.safeBlockHorizontal * 8,
    //                   child: InkWell(
    //                     onTap: onDelete,
    //                     child: Align(
    //                       alignment: Alignment.topRight,
    //                       child: ClipRRect(
    //                         borderRadius: BorderRadius.all(Radius.circular(5)),
    //                         child: Container(
    //                           decoration: BoxDecoration(shape: BoxShape.circle),
    //                           child: Icon(Icons.close,
    //                               color: Colors.grey,
    //                               size: SizeConfig.safeBlockHorizontal * 4),
    //                         ),
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //               )
    //             : Container(),
    //         status == 3
    //             ? Container(
    //                 color: Color.fromRGBO(236, 236, 236, 0.58),
    //                 width: double.infinity,
    //                 height: SizeConfig.screenHeight / 4.8,
    //               )
    //             : Container(),
    //         showPosition
    //             ? Positioned.fill(
    //                 child: Align(
    //                     alignment: Alignment.topLeft,
    //                     child: Container(
    //                       width: _size * 20,
    //                       height: _size * 20,
    //                       child: ClipRRect(
    //                         borderRadius:
    //                             BorderRadius.only(topLeft: Radius.circular(3)),
    //                         child: Triangle(text: (index + 1).toString()),
    //                       ),
    //                     )),
    //               )
    //             : Container(),
    //       ],
    //     ));
  }
}

class BusinessTileLoader extends StatelessWidget {
  final bool isPengajuan;

  BusinessTileLoader({this.isPengajuan = false});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _height = SizeConfig.screenHeight;
    var _width = SizeConfig.screenWidth;
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
        height: 400,
        padding: EdgeInsets.all(4),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        width: 150,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));

    // Container(
    //     margin:
    //         EdgeInsets.fromLTRB(_size * 5, _size * 1.5, _size * 5, _size * 1.5),
    //     decoration: BoxDecoration(
    //         border: Border.all(
    //             color: Color(0xffDADADA), width: 1.0, style: BorderStyle.solid),
    //         borderRadius: BorderRadius.all(Radius.circular(4))),
    //     // color: Colors.pink,
    //     height: isPengajuan ? _height / 4.8 : _height / 5,
    //     width: _width,
    //     child: Container(
    //       padding: EdgeInsets.fromLTRB(_size * 4.5, _size * 4.5, _size * 3, 0),
    //       child: Row(
    //         mainAxisSize: MainAxisSize.max,
    //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: <Widget>[
    //           ListingLoaderSegment(
    //             height: _height / 7,
    //             width: _height / 7,
    //           ),
    //           Container(
    //             width: _size * 2.5,
    //           ),
    //           Expanded(
    //             flex: 1,
    //             child: Column(
    //               mainAxisSize: MainAxisSize.max,
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               crossAxisAlignment: CrossAxisAlignment.start,
    //               children: <Widget>[
    //                 Column(
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     mainAxisSize: MainAxisSize.min,
    //                     children: <Widget>[
    //                       ListingLoaderSegment(
    //                         height: _size * 2.5,
    //                         width: _size * 22.5,
    //                       ),
    //                       Container(height: 8),
    //                       ListingLoaderSegment(
    //                         height: _size * 2.5,
    //                         width: _size * 27.5,
    //                       ),
    //                       Container(height: 5),
    //                       ListingLoaderSegment(
    //                         height: _size * 2.5,
    //                         width: _size * 32.5,
    //                       ),
    //                     ]),
    //                 ListingLoaderSegment(
    //                   height: _size * 2.5,
    //                   width: _size * 25,
    //                 ),
    //                 isPengajuan
    //                     ? Column(
    //                         mainAxisSize: MainAxisSize.min,
    //                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: <Widget>[
    //                           ListingLoaderSegment(
    //                             height: _size * 2.5,
    //                             width: _size * 25,
    //                           ),
    //                           Container(),
    //                           Container(
    //                             height: _size * 3.75,
    //                           )
    //                         ],
    //                       )
    //                     : Container(),
    //               ],
    //             ),
    //           )
    //         ],
    //       ),
    //     ));
  }
}

// ini untuk view paling banyak diajukan ( PraListingUI.dart )
class GridContentV2 extends StatelessWidget {
  final Data data; // data perusahaan yg mau ditampilin
  final bool
      showPosition; // jika mau nampilin banner set to true ( default false )

  GridContentV2({@required this.data, this.showPosition = false});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      // width: SizeConfig.screenWidth * .5,
      margin: EdgeInsets.only(right: Sizes.s10),
      width: Sizes.s200,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Stack(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  child: SantaraCachedImage(
                    height: Sizes.s200,
                    width: double.infinity,
                    // placeholder: 'assets/Preload.jpeg',
                    image: data.image,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: Sizes.s130,
                  padding: EdgeInsets.all(Sizes.s10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${data.tagName}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff292F8D),
                          fontSize: FontSize.s12,
                        ),
                      ),
                      Text(
                        "${data.businessName}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: FontSize.s14,
                        ),
                      ),
                      Text(
                        "${data.companyName}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s12,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.fade,
                      ),
                      // Container(
                      //   height: SizeConfig.safeBlockHorizontal * 3.75,
                      // ),
                      Spacer(),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        // crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          TextIconView(
                            isActive: data.isVoted,
                            iconData: data.isVoted
                                ? Icons.person
                                : Icons.person_outline,
                            text: "${data.submissions}",
                          ),
                          TextIconView(
                            isActive: data.isLiked,
                            iconData: data.isLiked
                                ? Icons.favorite
                                : Icons.favorite_border,
                            text: "${data.likes}",
                          ),
                          TextIconView(
                            isActive: data.isCommented,
                            iconData: data.isCommented
                                ? Icons.chat_bubble
                                : Icons.chat_bubble_outline,
                            text: "${data.comments}",
                          )
                          // TextIconView(
                          //   iconData: Icons.person_outline,
                          //   text: "${data.submissions}",
                          // ),
                          // TextIconView(
                          //   iconData: Icons.favorite_border,
                          //   text: "${data.likes}",
                          // ),
                          // TextIconView(
                          //   iconData: Icons.chat_bubble_outline,
                          //   text: "${data.comments}",
                          // ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            showPosition
                ? Positioned.fill(
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          width: Sizes.s80,
                          height: Sizes.s80,
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.only(topLeft: Radius.circular(3)),
                            child: Triangle(
                              text: data.position != null
                                  ? data.position.toString()
                                  : "X",
                            ),
                          ),
                        )),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}

class GridContentV2Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400,
        padding: EdgeInsets.all(4),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        width: 150,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

// pra listing lainnay di detail emitten
class GridContentV3 extends StatelessWidget {
  final Data data;

  GridContentV3({@required this.data});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.screenWidth * .45,
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5), topRight: Radius.circular(5)),
              child: SantaraCachedImage(
                height: SizeConfig.screenHeight / 4,
                // placeholder: 'assets/Preload.jpeg',
                image: data.image,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data.tagName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff292F8D),
                        fontSize: SizeConfig.safeBlockHorizontal * 2.75),
                  ),
                  Text(
                    data.businessName,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                  ),
                  Text(
                    data.companyName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockHorizontal * 2.75),
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                  ),
                  Container(
                    height: 15,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Icon(
                        Icons.favorite_border,
                        size: SizeConfig.safeBlockHorizontal * 3.75,
                        color: Color(0xffdadada),
                      ),
                      Container(
                        width: 20,
                      ),
                      Icon(
                        Icons.chat_bubble_outline,
                        size: SizeConfig.safeBlockHorizontal * 3.75,
                        color: Color(0xffdadada),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
