import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/api.dart';

class RepliesSection extends StatelessWidget {
  final CommentHistories data;
  final VoidCallback onReply;
  final VoidCallback onDelete;
  final VoidCallback onReport;
  final Widget replies;
  final double maxRadius;
  final DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");
  final bool showAction;
  final bool
      isFirstReply; // jika balasan adalah balasan pertama, maka padding top set jadi 0, default = false

  RepliesSection(
      {@required this.data,
      @required this.onReply,
      @required this.onReport,
      @required this.onDelete,
      @required this.maxRadius,
      this.showAction = true,
      this.isFirstReply = false,
      this.replies});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    var _paddingSize = _size * 4;
    return Container(
      padding: EdgeInsets.only(
          right: _paddingSize, top: isFirstReply ? 0 : _paddingSize),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: "$apiLocalImage${data.photo}",
            placeholder: (context, url) => const CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 15,
              child: CupertinoActivityIndicator(),
            ),
            errorWidget: (context, data, _) => CircleAvatar(
              child: Image.asset(
                'assets/icon/user.png',
                fit: BoxFit.cover,
              ),
              radius: maxRadius,
            ),
            imageBuilder: (context, image) => CircleAvatar(
              backgroundImage: image,
              radius: maxRadius,
            ),
          ),
          Container(
            width: _paddingSize,
          ),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("${data.name}",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: _size * 4,
                        color: Colors.white)),
                Container(
                  height: _size * 1.25,
                ),
                Text(
                  "${data.comment}",
                  style: TextStyle(color: Colors.white, fontSize: _size * 3),
                  // maxLines: 2,
                  // overflow: TextOverflow.ellipsis,
                ),
                Container(
                  height: _size * 2.5,
                ),
                showAction
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            dateFormat
                                .format(DateTime.parse("${data.createdAt}")),
                            style: TextStyle(
                                fontSize: _size * 2.5, color: Colors.grey),
                          ),
                          Container(
                            width: 5,
                          ),
                          Text(
                            "●",
                            style: TextStyle(
                                fontSize: _size * 2, color: Colors.grey),
                          ),
                          Container(
                            width: _size * 1.25,
                          ),
                          InkWell(
                            onTap: onReply,
                            child: Text(
                              "Balas",
                              style: TextStyle(
                                  fontSize: _size * 2.5, color: Colors.grey),
                            ),
                          ),
                          Container(
                            width: _size * 1.25,
                          ),
                          Text(
                            "●",
                            style: TextStyle(
                                fontSize: _size * 2, color: Colors.grey),
                          ),
                          Container(
                            width: _size * 1.25,
                          ),
                          data.isMine == 1
                              ? InkWell(
                                  onTap: onDelete,
                                  child: Text(
                                    "Hapus",
                                    style: TextStyle(
                                        fontSize: _size * 2.5,
                                        color: Colors.grey),
                                  ),
                                )
                              : InkWell(
                                  onTap: onReport,
                                  child: Text(
                                    "Laporkan",
                                    style: TextStyle(
                                        fontSize: _size * 2.5,
                                        color: Colors.grey),
                                  ),
                                )
                        ],
                      )
                    : Text(
                        dateFormat.format(DateTime.parse("${data.createdAt}")),
                        style: TextStyle(
                            fontSize: _size * 2.5, color: Colors.grey),
                      ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
