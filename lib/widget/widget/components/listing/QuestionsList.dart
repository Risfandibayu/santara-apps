import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/QuestionModel.dart';

class QuestionsList extends StatelessWidget {
  final QuestionModel questionModel;
  final ValueChanged<int> onChanged;

  QuestionsList({@required this.questionModel, @required this.onChanged});

  double calculateHeight(int textLength) {
    var _size = SizeConfig.safeBlockHorizontal;
    if (textLength > 50 && textLength <= 75) {
      return _size * 10;
    } else if (textLength >= 75) {
      return _size * 13.5;
    } else if (textLength < 50) {
      return _size * 5;
    } else {
      return _size * 5;
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(_size * 2.5),
          child: Text(
            questionModel.question,
            style:
                TextStyle(fontWeight: FontWeight.w600, fontSize: _size * 3.5),
          ),
        ),
        Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: questionModel.answers
                .map((item) => Container(
                      margin: EdgeInsets.only(bottom: _size * 3.75),
                      height: calculateHeight(item.answer.length),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                              value: item.value,
                              groupValue: questionModel.groupValue,
                              onChanged: onChanged),
                          Container(
                            width: _size,
                          ),
                          Flexible(
                            child: Text(
                              item.answer,
                              style: TextStyle(fontSize: _size * 3),
                              maxLines: 3,
                            ),
                          )
                        ],
                      ),
                    ))
                .toList()),
      ],
    );
  }
}
