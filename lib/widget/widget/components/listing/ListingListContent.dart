import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingBisnis.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';
import 'package:shimmer/shimmer.dart';

class ListingListContent extends StatelessWidget {
  final PralistingBisnis data;
  final bool showPosition;

  ListingListContent({this.data, this.showPosition = false});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _height = SizeConfig.screenHeight;
    var _width = SizeConfig.screenWidth;
    return Container(
      margin: EdgeInsets.all(10),
      height: _height / 4.5,
      decoration: BoxDecoration(
          border: Border.all(
              color: Color(0xffDADADA), width: 1.0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(4))),
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(18, 18, 12, 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  child: SantaraCachedImage(
                    height: _height / 7,
                    width: _height / 7,
                    // placeholder: 'assets/Preload.jpeg',
                    image: data.foto,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      data.kategori,
                      style: TextStyle(
                          color: Color(0xff292F8D),
                          fontSize: SizeConfig.safeBlockHorizontal * 3),
                    ),
                    Text(
                      data.namaBisnis,
                      style: TextStyle(
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      data.namaPerusahaan,
                      style: TextStyle(
                          fontSize: SizeConfig.safeBlockHorizontal * 2.75),
                    ),
                    Container(
                      height: 15,
                    ),
                    Container(
                      width: _width * 0.55,
                      child: Text(
                        data.deskripsi,
                        style: TextStyle(
                            fontSize: SizeConfig.safeBlockHorizontal * 2.75),
                        maxLines: 2,
                      ),
                    ),
                    Container(
                      height: 15,
                    ),
                    Container(
                      width: _width * .5,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        // crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          TextIconView(
                              iconData: Icons.check, text: data.diajukan),
                          // Spacer(),
                          TextIconView(
                            isActive: false, //data.isLiked,
                            iconData: false // data.isLiked
                                ? Icons.favorite
                                : Icons.favorite_border,
                            text: data.likes.toString(),
                          ),
                          // Spacer(),
                          TextIconView(
                            isActive: false, //data.isCommented,
                            iconData: false //data.isCommented
                                ? Icons.chat_bubble
                                : Icons.chat_bubble_outline,
                            text: data.comments.toString(),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          showPosition
              ? Positioned.fill(
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: _height / 10,
                        height: _height / 10,
                        child: ClipRRect(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(3)),
                          child: Triangle(
                            text: data.idBisnis.toString(),
                          ),
                        ),
                      )),
                )
              : Container(),
        ],
      ),
    );
  }
}

class Triangle extends StatelessWidget {
  const Triangle({
    Key key,
    this.color,
    this.text = "",
  }) : super(key: key);
  final Color color;
  final String text;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return CustomPaint(
      painter: ShapesPainter(),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5),
          ),
        ),
        height: Sizes.s50,
        width: Sizes.s50,
        child: Center(
          child: Padding(
            padding: EdgeInsets.only(right: Sizes.s30, bottom: Sizes.s30),
            child: Text(
              text,
              style: TextStyle(
                color: Colors.white,
                fontSize: FontSize.s20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ListingListLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(
                color: Color(0xffDADADA), width: 1.0, style: BorderStyle.solid),
            borderRadius: BorderRadius.all(Radius.circular(4))),
        // color: Colors.pink,
        height: _height / 4.5,
        width: _width,
        child: Container(
          padding: EdgeInsets.fromLTRB(18, 18, 12, 0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListingLoaderSegment(
                height: _height / 7,
                width: _height / 7,
              ),
              Container(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  ListingLoaderSegment(
                    height: 10,
                    width: 90,
                  ),
                  Container(height: 8),
                  ListingLoaderSegment(
                    height: 10,
                    width: 110,
                  ),
                  Container(height: 5),
                  ListingLoaderSegment(
                    height: 10,
                    width: 130,
                  ),
                  Container(
                    height: 20,
                  ),
                  ListingLoaderSegment(
                    height: 10,
                    width: 150,
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(
                    width: _width * .5,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      // crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        ListingLoaderSegment(
                          height: 10,
                          width: 30,
                        ),
                        ListingLoaderSegment(
                          height: 10,
                          width: 30,
                        ),
                        ListingLoaderSegment(
                          height: 10,
                          width: 30,
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    // set the paint color to be white
    paint.color = Colors.transparent;
    // Create a rectangle with size and width same as the canvas
    var rect = Rect.fromLTWH(0, 0, size.width, size.height);
    // draw the rectangle using the paint
    canvas.drawRect(rect, paint);
    paint.color = Color(0xffBF2D30);
    // create a path
    var path = Path();
    path.lineTo(0, size.height);
    // path.quadraticBezierTo(0, 10, 0, 10);
    path.lineTo(size.width, 0);
    // close the path to form a bounded shape
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class ListingLoaderSegment extends StatelessWidget {
  final double width;
  final double height;
  ListingLoaderSegment({this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.white,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Container(
          height: height,
          width: width,
          child: Image.asset(
            'arfa.png',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
