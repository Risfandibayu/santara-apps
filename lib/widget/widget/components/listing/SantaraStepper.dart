import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/EmitenJourney.dart';
import 'package:santaraapp/utils/sizes.dart';

class VerticalSeparator extends StatelessWidget {
  final Color separatorColor;
  VerticalSeparator({@required this.separatorColor});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.zero,
      height: Sizes.s45,
      width: 2,
      color: separatorColor,
    );
  }
}

class SantaraStepper extends StatelessWidget {
  final String title;
  final bool isActive;
  final bool isEnd;
  final bool enableSeparator;
  SantaraStepper({
    this.title,
    this.isActive,
    this.isEnd = false,
    this.enableSeparator = false,
  });
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: Sizes.s30,
              child: Center(
                child: Stack(
                  children: <Widget>[
                    isEnd
                        ? Padding(
                            padding: EdgeInsets.only(left: Sizes.s10),
                            child: Container(width: 1),
                          )
                        : Padding(
                            padding: EdgeInsets.only(left: Sizes.s10),
                            child: VerticalSeparator(
                              separatorColor: enableSeparator
                                  ? Color(0xffBF2D30)
                                  : Color(0xffDADADA),
                            ),
                          ),
                    Container(
                      height: Sizes.s20,
                      width: Sizes.s20,
                      decoration: BoxDecoration(
                        color: isActive ? Color(0xffBF2D30) : Color(0xffDADADA),
                        shape: BoxShape.circle,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: Sizes.s10),
                child: Text(
                  '$title',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: isActive ? Colors.black : Color(0xffDADADA),
                    fontSize: FontSize.s14,
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}

// Emiten
class VerticalSeparatorV2 extends StatelessWidget {
  final bool isActive;
  VerticalSeparatorV2({this.isActive = true});
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: Sizes.s5),
      height: Sizes.s50,
      width: Sizes.s3,
      decoration: BoxDecoration(
        color: isActive ? Color(0xffBF2D30) : Color(0xffDADADA),
        borderRadius: BorderRadius.circular(Sizes.s5),
      ),
    );
  }
}

class SantaraStepperV2 extends StatelessWidget {
  final String number;
  final String title;
  final String subtitle;
  final bool isActive;
  final bool isEnd;
  SantaraStepperV2({
    this.number = "",
    this.title = "",
    this.subtitle = "",
    this.isActive,
    this.isEnd = false,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: Sizes.s30,
              child: Center(
                child: Stack(
                  children: <Widget>[
                    isEnd
                        ? Padding(
                            padding: EdgeInsets.only(left: Sizes.s9),
                            child: Container(
                              width: 1,
                            ),
                          )
                        : Container(
                            margin: EdgeInsets.only(left: Sizes.s9),
                            child: VerticalSeparatorV2(
                              isActive: isActive,
                            ),
                          ),
                    Container(
                      height: Sizes.s20,
                      width: Sizes.s20,
                      decoration: BoxDecoration(
                        color: isActive ? Color(0xffBF2D30) : Color(0xffDADADA),
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Text(
                          "$number",
                          style: TextStyle(
                            fontSize: FontSize.s10,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: Sizes.s10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '$title',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: isActive ? Colors.black : Color(0xffDADADA),
                        fontSize: FontSize.s14,
                      ),
                    ),
                    Text(
                      '$subtitle',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: isActive ? Colors.black : Color(0xffDADADA),
                        fontSize: FontSize.s11,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}

// Stepper
class SantaraEmitenStepper extends StatelessWidget {
  final String stepNumber;
  final String title;
  final String subtitle;
  final bool isActive;
  final bool isNodeActive;
  final bool isEnd;
  final List<EmitenJourney> emitenJourney;

  SantaraEmitenStepper({
    this.stepNumber = "",
    this.title = "",
    this.subtitle = "",
    this.isActive = true,
    this.isNodeActive = true,
    this.isEnd = false,
    this.emitenJourney,
  });

  @override
  Widget build(BuildContext context) {
    bool isComplete = false;
    String date = " ";
    for (var item in emitenJourney) {
      if (item.title == title) {
        isComplete = true;
        date = item.date;
      }
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: Sizes.s20,
              height: Sizes.s20,
              decoration: BoxDecoration(
                color: isComplete ? Color(0xff7F1D1D) : Color(0xffA3A3A3),
                shape: BoxShape.circle,
              ),
              child: Center(
                child: Text(
                  "$stepNumber",
                  style: TextStyle(
                    fontSize: FontSize.s10,
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
            isEnd
                ? Container()
                : Container(
                    decoration: BoxDecoration(
                      color: isComplete ? Color(0xff7F1D1D) : Color(0xffA3A3A3),
                      borderRadius: BorderRadius.circular(Sizes.s10),
                    ),
                    margin: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                    width: 3,
                    height: Sizes.s20,
                  )
          ],
        ),
        SizedBox(width: Sizes.s10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "$title",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s14,
                color: isComplete
                    ? isEnd
                        ? Colors.blue
                        : Colors.white
                    : Color(0xffA3A3A3),
                decoration:
                    isEnd ? TextDecoration.underline : TextDecoration.none,
                decorationStyle: TextDecorationStyle.solid,
              ),
            ),
            Text(
              "$date",
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: FontSize.s11,
                color: isComplete ? Colors.red : Colors.white,
              ),
            ),
          ],
        )
      ],
    );
  }
}
