import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class ListingAlertDialog extends StatelessWidget {
  final bool showCloseButton;
  final String title;
  final String subtitle;
  final String buttonTitle;
  final VoidCallback onClose;
  final VoidCallback onTap;
  final double height;

  ListingAlertDialog(
      {this.showCloseButton = false,
      @required this.title,
      @required this.subtitle,
      @required this.buttonTitle,
      @required this.onClose,
      @required this.onTap,
      this.height = 200});

  @override
  Widget build(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5))),
        elevation: 0,
        contentPadding: EdgeInsets.all(0),
        content: Container(
          height: height,
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(_size * 4),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: title.isNotEmpty ? _size * 10 : 0,
                    ),
                    Center(
                      child: Text(
                        "$title",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: _size * 4.5),
                      ),
                    ),
                    Container(
                      height: title.isNotEmpty ? _size * 4 : 0,
                    ),
                    Center(
                      child: Text(
                        "$subtitle",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xff676767), fontSize: _size * 3),
                      ),
                    ),
                    Container(
                      height: _size * 4,
                    ),
                    Container(
                      height: _size * 8.5,
                      child: SantaraMainButton(
                        title: "$buttonTitle",
                        onPressed: onTap,
                      ),
                    )
                  ],
                ),
              ),
              showCloseButton
                  ? Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                          icon: Icon(Icons.close), onPressed: onClose))
                  : Container()
            ],
          ),
        ));
  }
}

// class RejectedBusinessDialog extends StatelessWidget {
//   final bool showCloseButton;
//   final String title;
//   final String reason;
//   final VoidCallback onClose;
//   final VoidCallback onTap;
//   final double height;

//   RejectedBusinessDialog(
//       {this.showCloseButton = false,
//       @required this.title,
//       @required this.reason,
//       @required this.onClose,
//       @required this.onTap,
//       this.height = 200});

//   TextStyle _style() {
//     var _size = SizeConfig.safeBlockHorizontal;
//     return TextStyle(color: Color(0xff676767), fontSize: _size * 3);
//   }

//   @override
//   Widget build(BuildContext context) {
//     var _size = SizeConfig.safeBlockHorizontal;
//     return AlertDialog(
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.all(Radius.circular(5))),
//         elevation: 0,
//         contentPadding: EdgeInsets.all(0),
//         content: Container(
//           height: height,
//           width: double.infinity,
//           child: Stack(
//             children: <Widget>[
//               Container(
//                 padding: EdgeInsets.all(_size * 4),
//                 child: Column(
//                   mainAxisSize: MainAxisSize.max,
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       height: title.isNotEmpty ? _size * 5 : 0,
//                     ),
//                     Center(
//                       child: Text(
//                         "$title",
//                         textAlign: TextAlign.center,
//                         style: TextStyle(
//                             fontWeight: FontWeight.bold, fontSize: _size * 4.5),
//                       ),
//                     ),
//                     Container(
//                       height: title.isNotEmpty ? _size * 4 : 0,
//                     ),
//                     Container(
//                       height: _size * 40,
//                       child: Scrollbar(
//                         child: SingleChildScrollView(
//                           child: Center(
//                             child: Text(
//                               "$reason",
//                               textAlign: TextAlign.center,
//                               style: _style(),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                     Container(
//                       height: _size * 4,
//                     ),
//                     Center(
//                       child: Text(
//                         "Cara Mengajukan Kembali",
//                         textAlign: TextAlign.center,
//                         style: TextStyle(
//                             fontWeight: FontWeight.bold, fontSize: _size * 4.5),
//                       ),
//                     ),
//                     Container(
//                       height: _size * 2,
//                     ),
//                     Text(
//                       "1. Tap “Perbaiki Pengajuan”",
//                       style: _style(),
//                     ),
//                     Text(
//                       "2. Edit Kriteria Diatas",
//                       style: _style(),
//                     ),
//                     Text(
//                       "3. Kembali ke halaman Bisnis Diajukan",
//                       style: _style(),
//                     ),
//                     Row(
//                       children: <Widget>[
//                         Text(
//                           "4. Tap pada ",
//                           style: _style(),
//                         ),
//                         Icon(
//                           Icons.more_vert,
//                           size: _size * 4,
//                           color: Color(0xff676767),
//                         ),
//                         Text(
//                           " kemudian pilih ajukan kembali",
//                           style: _style(),
//                         )
//                       ],
//                     ),
//                     Container(
//                       height: _size * 2,
//                     ),
//                     Container(
//                       height: _size * 8.5,
//                       child: SantaraMainButton(
//                         title: "Perbaiki Pengajuan",
//                         onPressed: onTap,
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//               showCloseButton
//                   ? Align(
//                       alignment: Alignment.topRight,
//                       child: IconButton(
//                           icon: Icon(Icons.close), onPressed: onClose))
//                   : Container()
//             ],
//           ),
//         ));
//   }
// }
