// untuk view notification

import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';

class SantaraNotification extends StatelessWidget {
  final Widget message;
  final Color backgroundColor;
  final double height;
  final EdgeInsets margin;

  SantaraNotification(
      {@required this.message,
      @required this.backgroundColor,
      this.height,
      this.margin});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _height = SizeConfig.screenHeight;
    var _size = SizeConfig.safeBlockHorizontal;

    return Container(
      margin: margin == null ? EdgeInsets.only(right: _size * 5) : margin,
      width: double.infinity,
      // height: height == null ? _height * .10 : height,
      decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(6))),
      child: Padding(padding: EdgeInsets.all(_size * 3.25), child: message),
    );
  }
}
