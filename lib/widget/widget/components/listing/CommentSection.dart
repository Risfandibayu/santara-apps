import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';

class CommentSection extends StatelessWidget {
  final Comments data;
  final VoidCallback onReply;
  final VoidCallback onDelete;
  final VoidCallback onReport;
  final Widget replies;
  final double maxRadius;
  final bool showAction; // nampilin button balas, hapus, laporkan
  final DateFormat dateFormat = DateFormat("dd LLLL yyyy hh:mm", "id");

  CommentSection(
      {@required this.data,
      @required this.onReply,
      @required this.onReport,
      @required this.onDelete,
      @required this.maxRadius,
      this.showAction = true,
      @required this.replies});

  // batas / spasi antara tanggal, bullet dan button balas/hapus/laporkan
  get buttonSeparator {
    return Container(width: Sizes.s5);
  }

  // style untuk tanggal, bullet dan button balas/hapus/laporkan
  get buttonStyle {
    return TextStyle(fontSize: FontSize.s9, color: Colors.grey);
  }

  @override
  Widget build(BuildContext context) {
    String imageUrl = "$apiLocalImage${data.photo}";
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(right: Sizes.s10, top: Sizes.s10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: imageUrl.replaceAll("trader", "trader-photo"),
            placeholder: (context, url) => CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: Sizes.s20,
              child: CupertinoActivityIndicator(),
            ),
            errorWidget: (context, data, _) => CircleAvatar(
              radius: maxRadius,
              child: Image.asset(
                'assets/icon/user.png',
                fit: BoxFit.cover,
              ),
              backgroundColor: Colors.transparent,
            ),
            imageBuilder: (context, image) => CircleAvatar(
              backgroundImage: image,
              radius: maxRadius,
            ),
          ),
          Container(
            width: Sizes.s10,
          ),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("${data.name}",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s13,
                        color: Colors.white)),
                Container(
                  height: Sizes.s5,
                ),
                Text(
                  "${data.comment}",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: FontSize.s11,
                  ),
                ),
                Container(
                  height: Sizes.s8,
                ),
                showAction
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            dateFormat
                                .format(DateTime.parse("${data.createdAt}")),
                            style: buttonStyle,
                          ),
                          buttonSeparator,
                          Text("●", style: buttonStyle),
                          buttonSeparator,
                          InkWell(
                            onTap: onReply,
                            child: Text("Balas", style: buttonStyle),
                          ),
                          buttonSeparator,
                          Text("●", style: buttonStyle),
                          buttonSeparator,
                          data.isMine == 1
                              ? InkWell(
                                  onTap: onDelete,
                                  child: Text("Hapus", style: buttonStyle),
                                )
                              : InkWell(
                                  onTap: onReport,
                                  child: Text("Laporkan", style: buttonStyle),
                                )
                        ],
                      )
                    : Text(
                        dateFormat.format(DateTime.parse("${data.createdAt}")),
                        style: buttonStyle,
                      ),
                replies
              ],
            ),
          )
        ],
      ),
    );
  }
}

class UserComment extends StatelessWidget {
  final Comments data;
  final VoidCallback onReply;
  final VoidCallback onDelete;
  final VoidCallback onReport;
  final Widget replies;
  final double maxRadius;
  final bool showAction;

  UserComment(
      {@required this.data,
      @required this.onReply,
      @required this.onReport,
      @required this.onDelete,
      @required this.maxRadius,
      this.showAction = true,
      this.replies});

  @override
  Widget build(BuildContext context) {
    return CommentSection(
      maxRadius: maxRadius,
      data: data,
      showAction: showAction,
      onReply: onReply,
      replies: replies,
      onDelete: onDelete,
      onReport: onReport,
    );
  }
}
