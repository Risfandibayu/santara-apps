import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class SantaraSaldo extends StatelessWidget {
  final bool isLoading; // loading balance
  final bool batasLoaded; // loading batas kepemilikan saham, default = true
  final String title;
  final dynamic idr;
  final String info;
  final VoidCallback onTapInfo;
  final rupiah = NumberFormat("#,##0");

  SantaraSaldo({
    @required this.isLoading,
    this.batasLoaded = true,
    @required this.title,
    @required this.idr,
    @required this.info,
    @required this.onTapInfo,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color(0xffF4F4F4),
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
          color: Color(0xffB8B8B8),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "$title",
                style: TextStyle(fontSize: 10),
              ),
              isLoading
                  ? ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.white,
                        child: Container(
                          width: MediaQuery.of(context).size.height / 5,
                          height: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  : Text(
                      idr.runtimeType is String
                          ? idr
                          : "Rp ${rupiah.format(idr ?? 0)}",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    )
            ],
          ),
          batasLoaded
              ? InkWell(
                  onTap: onTapInfo,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.info,
                        size: 12,
                        color: Color(0xff218196),
                      ),
                      Container(
                        width: 3,
                      ),
                      Text(
                        "$info",
                        style: TextStyle(
                          color: Color(0xff218196),
                          fontSize: 10,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid,
                        ),
                      )
                    ],
                  ),
                )
              : ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.white,
                    child: Container(
                      width: 100,
                      height: 16,
                      color: Colors.grey,
                    ),
                  ),
                )
        ],
      ),
    );
  }
}

class SantaraSaldoWithdraw extends StatelessWidget {
  final bool isLoading; // loading balance
  final bool batasLoaded; // loading batas kepemilikan saham, default = true
  final String title1;
  final String title2;
  final dynamic idr;
  final dynamic pendingSaldo;
  final String info;
  final VoidCallback onTapTitle1;
  final VoidCallback onTapTitle2;
  final VoidCallback onTapInfo;
  final rupiah = NumberFormat("#,##0");

  SantaraSaldoWithdraw({
    @required this.isLoading,
    this.batasLoaded = true,
    @required this.title1,
    @required this.title2,
    @required this.idr,
    @required this.pendingSaldo,
    @required this.info,
    @required this.onTapTitle1,
    @required this.onTapTitle2,
    @required this.onTapInfo,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color(0xffF4F4F4),
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
          color: Color(0xffB8B8B8),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  Text(
                    "$title1",
                    style: TextStyle(fontSize: 10),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: GestureDetector(
                      child: Icon(
                        Icons.info,
                        size: 12,
                        color: Color(0xFFB8B8B8),
                      ),
                      onTap: onTapTitle1,
                    ),
                  )
                ],
              ),
              isLoading
                  ? ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.white,
                        child: Container(
                          width: MediaQuery.of(context).size.height / 5,
                          height: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  : Text(
                      idr.runtimeType is String
                          ? idr
                          : "Rp ${rupiah.format(idr ?? '0')}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Color(0xFF0E7E4A)),
                    ),
              Container(height: 12),
              Row(
                children: [
                  Text(
                    "$title2",
                    style: TextStyle(fontSize: 10),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: GestureDetector(
                      child: Icon(
                        Icons.info,
                        size: 12,
                        color: Color(0xFFB8B8B8),
                      ),
                      onTap: onTapTitle2,
                    ),
                  )
                ],
              ),
              isLoading
                  ? ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.white,
                        child: Container(
                          width: MediaQuery.of(context).size.height / 5,
                          height: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  : Text(
                      idr.runtimeType is String
                          ? pendingSaldo
                          : "Rp ${rupiah.format(pendingSaldo ?? '0')}",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    )
            ],
          ),
          batasLoaded
              ? InkWell(
                  onTap: onTapInfo,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.info,
                        size: 12,
                        color: Color(0xff218196),
                      ),
                      Container(
                        width: 3,
                      ),
                      Text(
                        "$info",
                        style: TextStyle(
                          color: Color(0xff218196),
                          fontSize: 10,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid,
                        ),
                      )
                    ],
                  ),
                )
              : ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.white,
                    child: Container(
                      width: 100,
                      height: 16,
                      color: Colors.grey,
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
