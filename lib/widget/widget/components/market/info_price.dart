import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../../helpers/RupiahFormatter.dart';

class InfoPrice extends StatefulWidget {
  final String codeEmiten;
  final int prev;
  final int price;
  InfoPrice({this.codeEmiten, this.prev, this.price});
  @override
  _InfoPriceState createState() => _InfoPriceState();
}

class _InfoPriceState extends State<InfoPrice> {
  @override
  Widget build(BuildContext context) {
    Query match = FirebaseFirestore.instance
        .collection("match_histories_${widget.codeEmiten}")
        .orderBy("price");
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Column(
            children: <Widget>[
              _itemHeaderOrderBook(
                  "Prev (Rp)", NumberFormatter.numberWithSymbol(widget.prev)),
              Container(height: 8),
              _itemHeaderOrderBook(
                  "Chg (Rp)",
                  NumberFormatter.numberWithSymbol(
                      (widget.price - widget.prev).abs())),
              Container(height: 8),
              _itemHeaderOrderBook(
                  "Chg%",
                  ((widget.price - widget.prev) /
                              (widget.prev == 0 ? 1 : widget.prev) *
                              100)
                          .toStringAsFixed(2) +
                      "%")
            ],
          ),
        ),
        Container(width: 10),
        Flexible(
            flex: 1,
            child: StreamBuilder<QuerySnapshot>(
                stream: match.snapshots(),
                builder: (context, stream) {
                  QuerySnapshot querySnapshot = stream.data;
                  List matchData = [];
                  if (stream.connectionState == ConnectionState.active) {
                    for (var i = 0; i < querySnapshot.docs.length; i++) {
                      matchData.add(querySnapshot.docs[i].data());
                    }
                  }
                  return Column(
                    children: <Widget>[
                      _itemHeaderOrderBook(
                          "High (Rp)",
                          matchData.length == 0
                              ? "0"
                              : NumberFormatter.numberWithSymbol(
                                  matchData[matchData.length - 1]["price"])),
                      Container(height: 8),
                      _itemHeaderOrderBook(
                          "Low (Rp)",
                          matchData.length == 0
                              ? "0"
                              : NumberFormatter.numberWithSymbol(
                                  matchData[0]["price"])),
                    ],
                  );
                })),
        Container(width: 10),
        Flexible(
            flex: 1,
            child: StreamBuilder<QuerySnapshot>(
                stream: match.snapshots(),
                builder: (context, stream) {
                  QuerySnapshot querySnapshot = stream.data;
                  var saham = 0;
                  var total = 0;
                  if (stream.connectionState == ConnectionState.active) {
                    for (var i = 0; i < querySnapshot.docs.length; i++) {
                      saham += querySnapshot.docs[i].data()["amount"];
                      total += querySnapshot.docs[i].data()["amount"] *
                          querySnapshot.docs[i].data()["price"].toInt();
                    }
                  }
                  return Column(
                    children: <Widget>[
                      _itemHeaderOrderBook(
                          "Saham", NumberFormatter.numberWithSymbol(saham)),
                      Container(height: 8),
                      _itemHeaderOrderBook("Total (Rp)",
                          NumberFormatter.numberWithSymbol(total)),
                      Container(height: 8),
                      _itemHeaderOrderBook(
                          "Avg (Rp)",
                          NumberFormatter.numberWithSymbol(
                              total / (saham == 0 ? 1 : saham)))
                    ],
                  );
                }))
      ],
    );
  }

  Widget _itemHeaderOrderBook(String name, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Text(
            name,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width / 37.5 > 15
                    ? 15
                    : MediaQuery.of(context).size.width / 37.5),
            overflow: TextOverflow.visible,
          ),
        ),
        Expanded(
          child: Text(
            value,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width / 37.5 > 15
                    ? 15
                    : MediaQuery.of(context).size.width / 37.5,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.end,
            overflow: TextOverflow.visible,
          ),
        )
      ],
    );
  }
}
