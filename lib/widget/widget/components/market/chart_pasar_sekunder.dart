import 'dart:math';

import 'package:charts_flutter/flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_candlesticks/flutter_candlesticks.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:santaraapp/blocs/market/data_chart_bloc.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/data_chart.dart';
import 'package:charts_flutter/src/text_element.dart' as ChartText;
import 'package:charts_flutter/src/text_style.dart' as ChartStyle;

class BarOHLC extends StatelessWidget {
  final String codeEmiten;

  const BarOHLC({@required this.codeEmiten});

  List _parseDataStreamChart(List<QueryDocumentSnapshot> events) {
    List<DataChartItem> datas = [];
    datas.clear();
    var data;
    Timestamp today = Timestamp.now();
    if (events.length > 0) {
      data = events[0].data();
      today = data["created_at"];
      // create data per jam dengan value = 0
      for (var j = 0; j <= today.toDate().hour; j++) {
        datas.add(DataChartItem(
            createdAt: DateTime(today.toDate().year, today.toDate().month,
                today.toDate().day, j),
            close: 0.0,
            low: 0.0,
            high: 0.0,
            open: 0.0,
            volume: 0.0,
            openedAt: null,
            closedAt: null));
      }
      // update data yang tadi sudah created per jam dengan data stream firestore
      for (var i = 0; i < events.length; i++) {
        var data = events[i].data();
        Timestamp c = data["created_at"];
        for (var j = 0; j < datas.length; j++) {
          if (j == c.toDate().hour) {
            DateTime time = c.toDate();
            if (data["price"] > datas[j].high) {
              datas[j].high = data["price"] / 1;
            }
            if (data["price"] < datas[j].low || datas[j].low == 0) {
              datas[j].low = data["price"] / 1;
            }
            if (datas[j].openedAt == null || time.isBefore(datas[j].openedAt)) {
              datas[j].openedAt = time;
              datas[j].open = data["price"] / 1;
            }
            if (datas[j].closedAt == null || time.isAfter(datas[j].closedAt)) {
              datas[j].closedAt = time;
              datas[j].close = data["price"] / 1;
            }
            datas[j].volume += data["amount"] / 1;
          }
        }
      }
      return List<dynamic>.from(datas.map((x) => x.toJson()));
    } else {
      return [
        {"open": 0.0, "high": 0.0, "low": 0.0, "close": 0.0, "volumeto": 0.0}
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataChartBloc, DataChartState>(
      builder: (context, state) {
        if (state is DataChartLoaded) {
          List<Map<dynamic, dynamic>> sampleData = [
            {
              "open": 0.0,
              "high": 0.0,
              "low": 0.0,
              "close": 0.0,
              "volumeto": 0.0
            }
          ];
          if (state.data != []) {
            sampleData = state.data;
          }
          return OHLCVGraph(
              fallbackHeight: MediaQuery.of(context).size.width / 2,
              labelPrefix: "",
              lineWidth: 0.5,
              data: sampleData,
              enableGridLines: true,
              volumeProp: 0.2);
        } else if (state is DataChartDailyLoaded) {
          Query query = FirebaseFirestore.instance
              .collection("match_histories_$codeEmiten")
              .orderBy("created_at", descending: false);
          return StreamBuilder<QuerySnapshot>(
              stream: query.snapshots(),
              builder: (context, stream) {
                List sampleData = [
                  {
                    "open": 0.0,
                    "high": 0.0,
                    "low": 0.0,
                    "close": 0.0,
                    "volumeto": 0.0
                  }
                ];
                if (stream.connectionState == ConnectionState.active) {
                  QuerySnapshot querySnapshot = stream.data;
                  if (querySnapshot.docs.length > 0) {
                    sampleData = _parseDataStreamChart(querySnapshot.docs);
                  }
                }
                return OHLCVGraph(
                    fallbackHeight: MediaQuery.of(context).size.width / 2,
                    labelPrefix: "",
                    data: sampleData,
                    enableGridLines: true,
                    volumeProp: 0.2);
              });
        } else {
          return Container();
        }
      },
    );
  }
}

class LineChart extends StatelessWidget {
  final List<charts.Series<Sales, int>> seriesLineData;

  LineChart({this.seriesLineData});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: charts.NumericComboChart(
        seriesLineData,
        defaultRenderer:
            new charts.LineRendererConfig(includeArea: false, stacked: false),
        animate: true,
        animationDuration: Duration(seconds: 1),
        defaultInteractions: true,
        behaviors: [],
      ),
    );
  }
}

class EndPointsAxisTimeSeriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  EndPointsAxisTimeSeriesChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(seriesList,
        animate: true,
        defaultInteractions: true,
        domainAxis: new charts.EndPointsTimeAxisSpec(),
        behaviors: [
          new charts.LinePointHighlighter(
              symbolRenderer: CustomCircleSymbolRenderer()),
          new charts.SelectNearest(
              eventTrigger: charts.SelectionTrigger.tapAndDrag)
        ],
        selectionModels: [
          charts.SelectionModelConfig(
              changedListener: (charts.SelectionModel model) {
            if (model.hasDatumSelection) {
              ToolTipMgr.setTitle({
                'title':
                    '\n${NumberFormatter.convertNumber(model.selectedDatum[0].datum.value)}\n${DateFormat('dd/MM/yy').format(model.selectedDatum[0].datum.time)}',
              });
            }
          })
        ]);
  }
}

/// Sample time series data type.
class TimeSeriesData {
  final DateTime time;
  final num value;

  TimeSeriesData(this.time, this.value);
}

class Sales {
  int yearval;
  num salesval;

  Sales(this.yearval, this.salesval);
}

class FinancialData {
  DateTime dateTime;
  num value;

  FinancialData(this.dateTime, this.value);
}

String _title;

class ToolTipMgr {
  static String get title => _title;

  static setTitle(Map<String, dynamic> data) {
    if (data['title'] != null && data['title'].length > 0) {
      _title = data['title'];
    }
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  // double height = Adaptor.px(450.0);

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
      Color fillColor,
      FillPatternType fillPattern,
      Color strokeColor,
      double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left - 5, bounds.top, bounds.width + 100,
            bounds.height + 10),
        fill: Color.transparent);

    ChartStyle.TextStyle textStyle = ChartStyle.TextStyle();

    textStyle.color = Color(r: 14, g: 126, b: 74);
    textStyle.fontSize = 10;
    textStyle.fontFamily = "Nunito";

    canvas.drawText(ChartText.TextElement(ToolTipMgr.title, style: textStyle),
        (bounds.left).round(), (bounds.top + 2).round());
  }
}
