import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class SantaraMainButton extends StatelessWidget {
  final Key key;
  final Widget child;
  final double height;
  final double width;
  final Function onTap;
  final BoxDecoration decoration;
  SantaraMainButton(
      {@required this.key,
      this.height = 50,
      this.width,
      this.child,
      this.onTap,
      this.decoration});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: decoration == null
            ? BoxDecoration(
                color: Color(0xFFBF2D30),
                borderRadius: BorderRadius.circular(4))
            : decoration,
        child: Center(child: child),
      ),
    );
  }
}

class SantaraMainButtonWithIcon extends StatelessWidget {
  final Key key;
  final Widget icon;
  final String name;
  final double height;
  final double width;
  final Function onTap;
  final Color textColor;
  final BoxDecoration decoration;
  final EdgeInsetsGeometry padding;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  SantaraMainButtonWithIcon(
      {@required this.key,
      this.icon,
      this.height = 50,
      this.width = double.infinity,
      this.name,
      this.onTap,
      this.textColor,
      this.decoration,
      this.padding,
      this.mainAxisAlignment: MainAxisAlignment.center,
      this.crossAxisAlignment: CrossAxisAlignment.center});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: padding == null ? EdgeInsets.all(0) : padding,
        height: height,
        decoration: decoration == null
            ? BoxDecoration(
                color: Color(0xFFBF2D30),
                borderRadius: BorderRadius.circular(4))
            : decoration,
        child: Center(
          child: Row(
            mainAxisAlignment: mainAxisAlignment,
            crossAxisAlignment: crossAxisAlignment,
            children: <Widget>[
              icon,
              Container(width: Sizes.s8),
              Text(
                name,
                style: TextStyle(
                    color: textColor == null ? Colors.white : textColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SantaraSecondaryButton extends StatelessWidget {
  final Key key;
  final Widget child;
  final double height;
  final double width;
  final Function onTap;
  final BoxDecoration decoration;
  SantaraSecondaryButton(
      {@required this.key,
      this.height = 50,
      this.width,
      this.child,
      this.onTap,
      this.decoration});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: decoration == null
            ? BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 1, color: Color(0xFFBF2D30)),
                borderRadius: BorderRadius.circular(4))
            : decoration,
        child: Center(child: child),
      ),
    );
  }
}

class TutorialButton extends StatelessWidget {
  final Key key;
  final Widget icon;
  final String name;
  final Function onTap;
  TutorialButton({@required this.key, this.icon, this.name, this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: MediaQuery.of(context).size.width / 4,
        width: MediaQuery.of(context).size.width / 4,
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xFFB8B8B8)),
            borderRadius: BorderRadius.circular(4)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            icon,
            Container(height: 10),
            Text(
              name,
              style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
              overflow: TextOverflow.visible,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
