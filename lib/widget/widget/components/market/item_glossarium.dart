import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class ItemGlossarium extends StatelessWidget {
  final Key key;
  final String title;
  final String content;
  ItemGlossarium({@required this.key, this.title, this.content});
  @override
  Widget build(BuildContext context) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      scrollOnExpand: false,
      scrollOnCollapse: true,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Color(0xFFDADADA))),
        child: Column(
          children: <Widget>[
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                header: Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      title,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    )),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: Text(content),
                    ),
                    Container(
                      height: 10,
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
