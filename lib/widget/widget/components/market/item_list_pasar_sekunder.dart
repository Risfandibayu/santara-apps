import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart';
import 'package:santaraapp/models/market/watchlist_model.dart' as wl;
import 'package:santaraapp/widget/market/detail_saham_view.dart';

class ListSahamPasarSekunder extends StatefulWidget {
  final Key key;
  final Emiten emiten;
  ListSahamPasarSekunder({@required this.key, this.emiten})
      : assert(emiten != null);

  @override
  _ListSahamPasarSekunderState createState() => _ListSahamPasarSekunderState();
}

class _ListSahamPasarSekunderState extends State<ListSahamPasarSekunder> {
  @override
  Widget build(BuildContext context) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${widget.emiten.codeEmiten}");
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => DetailSahamView(
                    id: widget.emiten.id,
                    codeEmiten: widget.emiten.codeEmiten,
                  ))),
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Color(0xFFDADADA))),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(widget.emiten.codeEmiten,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      )),
                  Text(
                    widget.emiten.trademark,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    widget.emiten.companyName,
                    style: TextStyle(color: Color(0xFF676767)),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Container(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text("Harga Terakhir"),
                  StreamBuilder<QuerySnapshot>(
                      stream: query.snapshots(),
                      builder: (context, stream) {
                        if (stream.connectionState == ConnectionState.waiting) {
                          return CupertinoActivityIndicator();
                        }

                        if (stream.hasError) {
                          return Text("0", style: TextStyle(fontSize: 16));
                        }

                        QuerySnapshot querySnapshot = stream.data;
                        var data;
                        if (querySnapshot.docs.length > 0) {
                          data = querySnapshot.docs[0].data();
                          widget.emiten.currentPriceData = CurrentPriceData(
                            currentPrice: data["price"],
                            prev: data["prev"],
                            changePrice: (data["price"] - data["prev"]).abs(),
                            changeInPercent: data["price"] -
                                data["prev"] /
                                    (data["prev"] == 0 ? 1 : data["prev"]) *
                                    100,
                            status: data["price"] > data["prev"]
                                ? 1
                                : data["price"] < data["prev"]
                                    ? 2
                                    : 0,
                          );
                        } else {
                          widget.emiten.currentPriceData = CurrentPriceData(
                            currentPrice: widget.emiten.currentPrice,
                            prev: widget.emiten.prevPrice,
                            changePrice: (widget.emiten.currentPrice -
                                    widget.emiten.prevPrice)
                                .abs(),
                            changeInPercent: widget.emiten.currentPrice -
                                widget.emiten.prevPrice /
                                    (widget.emiten.prevPrice == 0
                                        ? 1
                                        : widget.emiten.prevPrice) *
                                    100,
                            status: widget.emiten.currentPrice >
                                    widget.emiten.prevPrice
                                ? 1
                                : widget.emiten.currentPrice <
                                        widget.emiten.prevPrice
                                    ? 2
                                    : 0,
                          );
                        }
                        return Text(
                            "Rp " +
                                NumberFormatter.convertNumber(data == null
                                    ? widget.emiten.currentPrice
                                    : widget
                                        .emiten.currentPriceData.currentPrice),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold));
                      }),
                  StreamBuilder<QuerySnapshot>(
                      stream: query.snapshots(),
                      builder: (context, stream) {
                        QuerySnapshot querySnapshot = stream.data;
                        var data;
                        if (stream.connectionState != ConnectionState.waiting) {
                          if (querySnapshot.docs.length > 0) {
                            data = querySnapshot.docs[0].data();
                          }
                        }
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            stream.connectionState == ConnectionState.waiting
                                ? CupertinoActivityIndicator()
                                : stream.hasError
                                    ? Container()
                                    : data == null
                                        ? Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4, right: 4),
                                            child: Icon(MdiIcons.circleDouble,
                                                color: Colors.orange, size: 16),
                                          )
                                        : data["price"] > data["prev"]
                                            ? Icon(Icons.arrow_drop_up,
                                                color: Colors.green)
                                            : data["price"] < data["prev"]
                                                ? Icon(
                                                    Icons.arrow_drop_down,
                                                    color: Colors.red,
                                                  )
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 4, right: 4),
                                                    child: Icon(
                                                        MdiIcons.circleDouble,
                                                        color: Colors.orange,
                                                        size: 16),
                                                  ),
                            Text(
                                stream.connectionState !=
                                        ConnectionState.waiting
                                    ? data == null
                                        ? "${((widget.emiten.currentPrice - widget.emiten.prevPrice) / (widget.emiten.prevPrice == 0 ? 1 : widget.emiten.prevPrice) * 100).toStringAsFixed(1)} %"
                                        : "${((data["price"] - data["prev"]) / data["prev"] * 100).toStringAsFixed(1)} %"
                                    : "",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: data == null
                                        ? widget.emiten.currentPrice >
                                                widget.emiten.prevPrice
                                            ? Colors.green
                                            : widget.emiten.currentPrice <
                                                    widget.emiten.prevPrice
                                                ? Colors.red
                                                : Colors.orange
                                        : data["price"] > data["prev"]
                                            ? Colors.green
                                            : data["price"] < data["prev"]
                                                ? Colors.red
                                                : Colors.orange)),
                          ],
                        );
                      })
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SahamWatchlist extends StatefulWidget {
  final Key key;
  final wl.Watchlist watchlist;
  SahamWatchlist({@required this.key, this.watchlist})
      : assert(watchlist != null);

  @override
  _SahamWatchlistState createState() => _SahamWatchlistState();
}

class _SahamWatchlistState extends State<SahamWatchlist> {
  @override
  Widget build(BuildContext context) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${widget.watchlist.emiten.codeEmiten}");
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => DetailSahamView(
                    id: widget.watchlist.emiten.id,
                    codeEmiten: widget.watchlist.emiten.codeEmiten,
                  ))),
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Color(0xFFDADADA))),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(widget.watchlist.emiten.codeEmiten,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  Text(
                    widget.watchlist.emiten.trademark,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    widget.watchlist.emiten.companyName,
                    style: TextStyle(color: Color(0xFF676767)),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Container(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text("Harga Terakhir"),
                  StreamBuilder<QuerySnapshot>(
                      stream: query.snapshots(),
                      builder: (context, stream) {
                        if (stream.connectionState == ConnectionState.waiting) {
                          return CupertinoActivityIndicator();
                        }

                        if (stream.hasError) {
                          return Text("0", style: TextStyle(fontSize: 16));
                        }

                        QuerySnapshot querySnapshot = stream.data;
                        var data;
                        if (querySnapshot.docs.length > 0) {
                          data = querySnapshot.docs[0].data();
                        }

                        return Text(
                            "Rp " +
                                NumberFormatter.convertNumber(data == null
                                    ? widget.watchlist.emiten.currentPrice
                                    : data["price"]),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold));
                      }),
                  StreamBuilder<QuerySnapshot>(
                      stream: query.snapshots(),
                      builder: (context, stream) {
                        QuerySnapshot querySnapshot = stream.data;
                        var data;
                        if (stream.connectionState != ConnectionState.waiting) {
                          if (querySnapshot.docs.length > 0) {
                            data = querySnapshot.docs[0].data();
                          }
                        }
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            stream.connectionState == ConnectionState.waiting
                                ? CupertinoActivityIndicator()
                                : stream.hasError
                                    ? Container()
                                    : data == null
                                        ? Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4, right: 4),
                                            child: Icon(MdiIcons.circleDouble,
                                                color: Colors.orange, size: 16),
                                          )
                                        : data["price"] > data["prev"]
                                            ? Icon(Icons.arrow_drop_up,
                                                color: Colors.green)
                                            : data["price"] < data["prev"]
                                                ? Icon(
                                                    Icons.arrow_drop_down,
                                                    color: Colors.red,
                                                  )
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 4, right: 4),
                                                    child: Icon(
                                                        MdiIcons.circleDouble,
                                                        color: Colors.orange,
                                                        size: 16),
                                                  ),
                            Text(
                                stream.connectionState !=
                                        ConnectionState.waiting
                                    ? data == null
                                        ? "0.0 %"
                                        : "${((data["price"] - data["prev"]) / data["prev"] * 100).toStringAsFixed(1)} %"
                                    : "",
                                style: TextStyle(
                                    color: data == null
                                        ? Colors.orange
                                        : data["price"] > data["prev"]
                                            ? Colors.green
                                            : data["price"] < data["prev"]
                                                ? Colors.red
                                                : Colors.orange)),
                          ],
                        );
                      })
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
