import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class ToastHelper {
  static void showSuccessToast(BuildContext context, String message) {
    Toast.show("$message", context,
        backgroundColor: Colors.green[800].withOpacity(0.8), duration: 3);
  }

  static void showFailureToast(BuildContext context, String message) {
    Toast.show("$message", context,
        backgroundColor: Colors.red[800].withOpacity(0.8), duration: 3);
  }

  static void showBasicToast(BuildContext context, String message) {
    Toast.show("$message", context,
        backgroundColor: Colors.grey[800].withOpacity(0.8), duration: 3);
  }

  static void showSnackBar(
      GlobalKey<ScaffoldState> key, String message, Color color) {
    key.currentState.showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: color,
      duration: Duration(milliseconds: 500),
    ));
  }

  static void showSnackBarWithAction(
      {BuildContext context,
      GlobalKey<ScaffoldState> key,
      Widget icon,
      String message,
      Color backgroundColor,
      Duration duration}) {
    key.currentState.showSnackBar(SnackBar(
      content: ListTile(
        leading: icon,
        title: Text(message),
      ),
      action: SnackBarAction(
          textColor: Colors.white,
          label: "OK",
          onPressed: () => key.currentState.hideCurrentSnackBar()),
      backgroundColor: backgroundColor,
      duration: duration,
    ));
  }
}
