import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';

// funtion ini untuk parsing data streaming dari firestore
//
List newDataOrderbook(List<QueryDocumentSnapshot> data, bool isDesc) {
  // ubah List<QueryDocumentSnapshot> menjadi List<Map>
  List<Map> tempData = [];
  for (var i = 0; i < data.length; i++) {
    tempData.add(data[i].data());
  }

  // data group by price
  // return nya
  // [
  //   2000: [{price: 2000, amount: 100, type: sell, status: false}, {price: 2000, amount: 80, type: sell, status: false}],
  //   2020: [{price: 2020, amount: 100, type: sell, status: false}, {price: 2020, amount: 80, type: sell, status: false}],
  // ]
  var newMap = groupBy(tempData, (obj) => obj['price']).map((k, v) {
    return MapEntry(
        k,
        v
            .map((item) => {
                  'price': item['price'],
                  'amount': item['amount'],
                  "type": item["type"],
                  "status": item["status"]
                })
            .toList());
  });

  // custom json setelah di group by
  // return nya
  // [
  //   {
  //      price: 2000,
  //      value: [
  //        {price: 2000, amount: 100, type: sell, status: false},
  //        {price: 2000, amount: 80, type: sell, status: false}
  //      ]
  //    },
  //   {
  //      price: 2020,
  //      value: [
  //        {price: 2020, amount: 100, type: sell, status: false},
  //        {price: 2020, amount: 80, type: sell, status: false}
  //      ]
  //    },
  // ]
  List orderbook = [];
  newMap.forEach((k, v) => orderbook.add({'price': k, 'value': v}));

  // sum value amount setelah di rubah json nya
  List dataOrderbook = [];
  for (var i = 0; i < orderbook.length; i++) {
    var data = {
      'price': orderbook[i]["price"],
      'amount': 0,
      "type": orderbook[i]["value"][0]["type"],
      "status": orderbook[i]["value"][0]["status"]
    };
    for (var j = 0; j < orderbook[i]["value"].length; j++) {
      data["amount"] += orderbook[i]["value"][j]["amount"];
    }
    dataOrderbook.add(data);
  }

  // sort data
  if (isDesc) {
    dataOrderbook.sort((a, b) => a["price"].compareTo(b["price"]));
  } else {
    dataOrderbook.sort((a, b) => b["price"].compareTo(a["price"]));
  }

  return dataOrderbook;
}

class TableJualBeli extends StatefulWidget {
  final int prev;
  final bool isJual;
  final String codeEmiten;

  TableJualBeli({
    this.prev,
    this.isJual,
    this.codeEmiten,
  });

  @override
  _TableJualBeliState createState() => _TableJualBeliState();
}

class _TableJualBeliState extends State<TableJualBeli> {
  @override
  Widget build(BuildContext context) {
    Query queryJual = FirebaseFirestore.instance
        .collection("orderbooks_${widget.codeEmiten}")
        .where("type", isEqualTo: "sell")
        .where("status", isEqualTo: false);

    Query queryBeli = FirebaseFirestore.instance
        .collection("orderbooks_${widget.codeEmiten}")
        .where("type", isEqualTo: "buy")
        .where("status", isEqualTo: false);
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
          crossAxisAlignment:
              widget.isJual ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color:
                        widget.isJual ? Color(0xFFFFDDDD) : Color(0xFFDDFFEF),
                    border: Border.all(color: Color(0xFFF4F4F4), width: 3)),
                child: widget.isJual
                    ? Row(
                        children: [
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jual"),
                                )),
                          ),
                          Container(
                            color: Color(0xFFF4F4F4),
                            width: 3,
                            height: 40,
                          ),
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jumlah"),
                                )),
                          )
                        ],
                      )
                    : Row(
                        children: [
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jumlah"),
                                )),
                          ),
                          Container(
                            color: Color(0xFFF4F4F4),
                            width: 3,
                            height: 40,
                          ),
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Beli"),
                                )),
                          )
                        ],
                      )),
            StreamBuilder<QuerySnapshot>(
                stream: queryJual.snapshots(),
                builder: (context, streamJual) {
                  return StreamBuilder<QuerySnapshot>(
                      stream: queryBeli.snapshots(),
                      builder: (context, streamBeli) {
                        QuerySnapshot snapshotJual = streamJual.data;
                        QuerySnapshot snapshotBeli = streamBeli.data;
                        List dataJual = [];
                        List dataBeli = [];

                        if (streamJual.connectionState ==
                            ConnectionState.active) {
                          if (snapshotJual.docs.length > 0) {
                            dataJual =
                                newDataOrderbook(snapshotJual.docs, true);
                          }
                        }

                        if (streamBeli.connectionState ==
                            ConnectionState.active) {
                          if (snapshotBeli.docs.length > 0) {
                            dataBeli =
                                newDataOrderbook(snapshotBeli.docs, false);
                          }
                        }

                        return Container(
                          decoration: BoxDecoration(
                              border: Border(
                            right:
                                BorderSide(color: Color(0xFFF4F4F4), width: 3),
                            bottom: BorderSide(
                                color: Color(0xFFF4F4F4),
                                width:
                                    dataJual.length == 0 && dataBeli.length == 0
                                        ? 0
                                        : 3),
                            left:
                                BorderSide(color: Color(0xFFF4F4F4), width: 3),
                          )),
                          child: ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: dataBeli.length > dataJual.length
                                  ? dataBeli.length > 10
                                      ? 10
                                      : dataBeli.length
                                  : dataJual.length > 10
                                      ? 10
                                      : dataJual.length,
                              itemBuilder: (_, i) {
                                if (widget.isJual) {
                                  if (i >= dataJual.length) {
                                    return Row(
                                      children: [
                                        Flexible(child: Container(height: 40)),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(child: Container(height: 40))
                                      ],
                                    );
                                  } else {
                                    return Row(
                                      children: [
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(
                                                  NumberFormatter.convertNumber(
                                                      dataJual[i]["price"]),
                                                  style: TextStyle(
                                                      color: dataJual[i]
                                                                  ["price"] >
                                                              widget.prev
                                                          ? Color(0xFF0E7E4A)
                                                          : dataJual[i][
                                                                      "price"] <
                                                                  widget.prev
                                                              ? Color(
                                                                  0xFFBF2D30)
                                                              : Color(
                                                                  0xFFE99D2D)),
                                                ),
                                              )),
                                        ),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(
                                                  NumberFormatter.convertNumber(
                                                      dataJual[i]["amount"]),
                                                ),
                                              )),
                                        ),
                                      ],
                                    );
                                  }
                                } else {
                                  if (i >= dataBeli.length) {
                                    return Row(
                                      children: [
                                        Flexible(child: Container(height: 40)),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(child: Container(height: 40)),
                                      ],
                                    );
                                  } else {
                                    return Row(
                                      children: [
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(NumberFormatter
                                                    .convertNumber(
                                                        dataBeli[i]["amount"])),
                                              )),
                                        ),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(
                                                  NumberFormatter.convertNumber(
                                                      dataBeli[i]["price"]),
                                                  style: TextStyle(
                                                      color: dataBeli[i]
                                                                  ["price"] >
                                                              widget.prev
                                                          ? Color(0xFF0E7E4A)
                                                          : dataBeli[i][
                                                                      "price"] <
                                                                  widget.prev
                                                              ? Color(
                                                                  0xFFBF2D30)
                                                              : Color(
                                                                  0xFFE99D2D)),
                                                ),
                                              )),
                                        ),
                                      ],
                                    );
                                  }
                                }
                                // return Text('$i');
                              }),
                        );
                      });
                }),
            // total amount
            StreamBuilder<QuerySnapshot>(
                stream: queryJual.snapshots(),
                builder: (context, streamJual) {
                  return StreamBuilder<QuerySnapshot>(
                      stream: queryBeli.snapshots(),
                      builder: (context, streamBeli) {
                        QuerySnapshot snapshotJual = streamJual.data;
                        QuerySnapshot snapshotBeli = streamBeli.data;
                        List dataJual = [];
                        List dataBeli = [];
                        int grandJual = 0;
                        int grandBeli = 0;

                        if (streamJual.connectionState ==
                            ConnectionState.active) {
                          if (snapshotJual.docs.length > 0) {
                            dataJual =
                                newDataOrderbook(snapshotJual.docs, true);
                            for (var i = 0; i < dataJual.length; i++) {
                              grandJual += dataJual[i]["amount"];
                            }
                          }
                        }

                        if (streamBeli.connectionState ==
                            ConnectionState.active) {
                          if (snapshotBeli.docs.length > 0) {
                            dataBeli =
                                newDataOrderbook(snapshotBeli.docs, false);
                            for (var i = 0; i < dataBeli.length; i++) {
                              grandBeli += dataBeli[i]["amount"];
                            }
                          }
                        }

                        if (dataJual.length == 0 && dataBeli.length == 0) {
                          return Container(
                            width: MediaQuery.of(context).size.width / 2 - 10,
                            decoration: BoxDecoration(
                                border: Border(
                              left: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                              bottom: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                              right: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                            )),
                            child: Image.asset(
                                "assets/icon_market/orderbook_empty.png"),
                          );
                        } else {
                          return Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width / 4 - 10,
                              child: Center(
                                  child: Text(
                                      NumberFormatter.convertNumber(
                                          widget.isJual
                                              ? grandJual
                                              : grandBeli),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))));
                        }
                      });
                }),
          ]),
    );
  }
}

class TableJualBeliCollapse extends StatefulWidget {
  final int prev;
  final bool isJual;
  final String codeEmiten;

  TableJualBeliCollapse({this.prev, this.isJual, this.codeEmiten});

  @override
  _TableJualBeliCollapseState createState() => _TableJualBeliCollapseState();
}

class _TableJualBeliCollapseState extends State<TableJualBeliCollapse> {
  @override
  Widget build(BuildContext context) {
    Query queryJual = FirebaseFirestore.instance
        .collection("orderbooks_${widget.codeEmiten}")
        .where("type", isEqualTo: "sell")
        .where("status", isEqualTo: false);

    Query queryBeli = FirebaseFirestore.instance
        .collection("orderbooks_${widget.codeEmiten}")
        .where("type", isEqualTo: "buy")
        .where("status", isEqualTo: false);
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
          crossAxisAlignment:
              widget.isJual ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color:
                        widget.isJual ? Color(0xFFFFDDDD) : Color(0xFFDDFFEF),
                    border: Border.all(color: Color(0xFFF4F4F4), width: 3)),
                child: widget.isJual
                    ? Row(
                        children: [
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jual"),
                                )),
                          ),
                          Container(
                            color: Color(0xFFF4F4F4),
                            width: 3,
                            height: 40,
                          ),
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jumlah"),
                                )),
                          )
                        ],
                      )
                    : Row(
                        children: [
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Jumlah"),
                                )),
                          ),
                          Container(
                            color: Color(0xFFF4F4F4),
                            width: 3,
                            height: 40,
                          ),
                          Flexible(
                            child: Container(
                                height: 40,
                                child: Center(
                                  child: Text("Beli"),
                                )),
                          )
                        ],
                      )),
            StreamBuilder<QuerySnapshot>(
                stream: queryJual.snapshots(),
                builder: (context, streamJual) {
                  return StreamBuilder<QuerySnapshot>(
                      stream: queryBeli.snapshots(),
                      builder: (context, streamBeli) {
                        QuerySnapshot snapshotJual = streamJual.data;
                        QuerySnapshot snapshotBeli = streamBeli.data;
                        List dataJual = [];
                        List dataBeli = [];

                        if (streamJual.connectionState ==
                            ConnectionState.active) {
                          if (snapshotJual.docs.length > 0) {
                            dataJual =
                                newDataOrderbook(snapshotJual.docs, true);
                          }
                        }

                        if (streamBeli.connectionState ==
                            ConnectionState.active) {
                          if (snapshotBeli.docs.length > 0) {
                            dataBeli =
                                newDataOrderbook(snapshotBeli.docs, false);
                          }
                        }

                        return Container(
                          decoration: BoxDecoration(
                              border: Border(
                            right:
                                BorderSide(color: Color(0xFFF4F4F4), width: 3),
                            bottom: BorderSide(
                                color: Color(0xFFF4F4F4),
                                width:
                                    dataJual.length == 0 && dataBeli.length == 0
                                        ? 0
                                        : 3),
                            left:
                                BorderSide(color: Color(0xFFF4F4F4), width: 3),
                          )),
                          child: ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: dataBeli.length > dataJual.length
                                  ? dataBeli.length > 3
                                      ? 3
                                      : dataBeli.length
                                  : dataJual.length > 3
                                      ? 3
                                      : dataJual.length,
                              itemBuilder: (_, i) {
                                if (widget.isJual) {
                                  if (i >= dataJual.length) {
                                    return Row(
                                      children: [
                                        Flexible(child: Container(height: 40)),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(child: Container(height: 40))
                                      ],
                                    );
                                  } else {
                                    return Row(
                                      children: [
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(
                                                  NumberFormatter.convertNumber(
                                                      dataJual[i]["price"]),
                                                  style: TextStyle(
                                                      color: dataJual[i]
                                                                  ["price"] >
                                                              widget.prev
                                                          ? Color(0xFF0E7E4A)
                                                          : dataJual[i][
                                                                      "price"] <
                                                                  widget.prev
                                                              ? Color(
                                                                  0xFFBF2D30)
                                                              : Color(
                                                                  0xFFE99D2D)),
                                                ),
                                              )),
                                        ),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(NumberFormatter
                                                    .convertNumber(
                                                        dataJual[i]["amount"])),
                                              )),
                                        ),
                                      ],
                                    );
                                  }
                                } else {
                                  if (i >= dataBeli.length) {
                                    return Row(
                                      children: [
                                        Flexible(child: Container(height: 40)),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(child: Container(height: 40)),
                                      ],
                                    );
                                  } else {
                                    return Row(
                                      children: [
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(NumberFormatter
                                                    .convertNumber(
                                                        dataBeli[i]["amount"])),
                                              )),
                                        ),
                                        Container(
                                          color: Color(0xFFF4F4F4),
                                          width: 3,
                                          height: 40,
                                        ),
                                        Flexible(
                                          child: Container(
                                              height: 40,
                                              child: Center(
                                                child: Text(
                                                  NumberFormatter.convertNumber(
                                                      dataBeli[i]["price"]),
                                                  style: TextStyle(
                                                      color: dataBeli[i]
                                                                  ["price"] >
                                                              widget.prev
                                                          ? Color(0xFF0E7E4A)
                                                          : dataBeli[i][
                                                                      "price"] <
                                                                  widget.prev
                                                              ? Color(
                                                                  0xFFBF2D30)
                                                              : Color(
                                                                  0xFFE99D2D)),
                                                ),
                                              )),
                                        ),
                                      ],
                                    );
                                  }
                                }
                                // return Text('$i');
                              }),
                        );
                      });
                }),
            // total amount
            StreamBuilder<QuerySnapshot>(
                stream: queryJual.snapshots(),
                builder: (context, streamJual) {
                  return StreamBuilder<QuerySnapshot>(
                      stream: queryBeli.snapshots(),
                      builder: (context, streamBeli) {
                        QuerySnapshot snapshotJual = streamJual.data;
                        QuerySnapshot snapshotBeli = streamBeli.data;
                        List dataJual = [];
                        List dataBeli = [];

                        if (streamJual.connectionState ==
                            ConnectionState.active) {
                          if (snapshotJual.docs.length > 0) {
                            dataJual =
                                newDataOrderbook(snapshotJual.docs, true);
                          }
                        }

                        if (streamBeli.connectionState ==
                            ConnectionState.active) {
                          if (snapshotBeli.docs.length > 0) {
                            dataBeli =
                                newDataOrderbook(snapshotBeli.docs, false);
                          }
                        }

                        if (dataJual.length == 0 && dataBeli.length == 0) {
                          return Container(
                            width: MediaQuery.of(context).size.width / 2 - 10,
                            decoration: BoxDecoration(
                                border: Border(
                              left: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                              bottom: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                              right: BorderSide(
                                  color: Color(0xFFF4F4F4), width: 3),
                            )),
                            child: Image.asset(
                                "assets/icon_market/orderbook_empty.png"),
                          );
                        } else {
                          return Container();
                        }
                      });
                }),
          ]),
    );
  }
}
