import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';

class CheckTodayWidget extends StatefulWidget {
  final CheckTodayBloc checkTodayBloc;
  final Widget Function(BuildContext, CheckTodayState) builder;
  const CheckTodayWidget({
    Key key,
    @required this.checkTodayBloc,
    @required this.builder,
  });

  @override
  _CheckTodayWidgetState createState() => _CheckTodayWidgetState();
}

class _CheckTodayWidgetState extends State<CheckTodayWidget> {
  MarketTrailsServices _trailsServices = MarketTrailsServices();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CheckTodayBloc, CheckTodayState>(
      listener: (context, state) async {
        if (state is CheckTodayLoaded) {
          if (state.isOpen) {
            if (state.type == TransactionType.beli) {
              await _trailsServices.createTrails(
                  event: marketTrailsData["buy_stocks_form_view"]["event"],
                  note: marketTrailsData["buy_stocks_form_view"]["note"]);
            } else {
              await _trailsServices.createTrails(
                  event: marketTrailsData["sell_stock_form"]["event"],
                  note: marketTrailsData["sell_stock_form"]["note"]);
            }
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => JualBeliView(
                          type: state.type,
                          uuid: state.uuid,
                          amount: state.amount,
                          codeEmiten: state.codeEmiten,
                          price: state.price,
                          totalSaham: state.totalSaham,
                          trxUuid: state.trxUuid,
                          pending: state.pending,
                        )));
          } else {
            await _trailsServices.createTrails(
                event: marketTrailsData["buy_stocks_form_view_failed"]["event"],
                note: marketTrailsData["buy_stocks_form_view_failed"]["note"]);
            PopupHelperMarket.showActivePeriod(context, state.activePeriod);
          }
        } else if (state is CheckTodayError) {
          await _trailsServices.createTrails(
              event: marketTrailsData["buy_stocks_form_view_failed"]["event"],
              note: marketTrailsData["buy_stocks_form_view_failed"]["note"]);
          PopupHelper.showMessage(context, state.message);
        }
      },
      builder: widget.builder,
    );
  }
}
