import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/blocs/market/data_chart_bloc.dart';
import 'package:santaraapp/models/market/data_chart.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

enum ChartTypeSyncfusion { candle, ohlcv, line }

class SyncfusionChart extends StatefulWidget {
  final String codeEmiten;
  final ChartTypeSyncfusion type;

  const SyncfusionChart({this.codeEmiten, this.type});

  @override
  _SyncfusionChartState createState() => _SyncfusionChartState();
}

class _SyncfusionChartState extends State<SyncfusionChart> {
  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    switch (widget.type) {
      case ChartTypeSyncfusion.line:
        await _trailsServices.createTrails(
            event: marketTrailsData["line_chart_view_success"]["event"],
            note: marketTrailsData["line_chart_view_success"]["note"]);
        break;
      case ChartTypeSyncfusion.candle:
        await _trailsServices.createTrails(
            event: marketTrailsData["candlestick_view_success"]["event"],
            note: marketTrailsData["candlestick_view_success"]["note"]);
        break;
      case ChartTypeSyncfusion.ohlcv:
        await _trailsServices.createTrails(
            event: marketTrailsData["ohlc_bar_view_success"]["event"],
            note: marketTrailsData["ohlc_bar_view_success"]["note"]);
        break;
      default:
    }
  }

  DateFormat dateFormat(String rangeType) {
    switch (rangeType) {
      case "daily":
        return DateFormat("hh:mm");
        break;
      case "weekly":
        return DateFormat("dd MMM\nhh:mm");
        break;
      case "monthly":
        return DateFormat("dd MMM");
        break;
      default:
        return DateFormat("MMM yyyy");
    }
  }

  /// Parse data stream to list of DataChartItem
  List<DataChartItem> _parseDataStreamChart(
      List<QueryDocumentSnapshot> events, double prev) {
    List<DataChartItem> datas = [];
    datas.clear();
    var dataPertama, dataTerakhir;
    Timestamp firstTime = Timestamp.now();
    Timestamp lastTime = Timestamp.now();
    if (events.length > 0) {
      dataPertama = events[0].data();
      dataTerakhir = events.last.data();
      firstTime = dataPertama["created_at"];
      lastTime = dataTerakhir["created_at"];
      // create data per jam dengan value = 0
      for (var i = firstTime.toDate().hour - 1;
          i <= lastTime.toDate().hour;
          i++) {
        datas.add(DataChartItem(
            createdAt: DateTime(firstTime.toDate().year,
                firstTime.toDate().month, firstTime.toDate().day, i),
            close: prev,
            low: prev,
            high: prev,
            open: prev,
            volume: 0,
            openedAt: null,
            closedAt: null));
      }

      // update data yang tadi sudah created per jam dengan data stream firestore
      for (var i = 0; i < events.length; i++) {
        var data = events[i].data();
        Timestamp c = data["created_at"];
        for (var j = 0; j < datas.length; j++) {
          DateTime time = c.toDate();
          if (time.hour == datas[j].createdAt.hour) {
            if (data["price"] > datas[j].high) {
              datas[j].high = data["price"] / 1;
            }
            if (data["price"] < datas[j].low || datas[j].low == 0) {
              datas[j].low = data["price"] / 1;
            }
            if (datas[j].openedAt == null || time.isBefore(datas[j].openedAt)) {
              datas[j].openedAt = time;
              datas[j].open = data["price"] / 1;
            }
            if (datas[j].closedAt == null || time.isAfter(datas[j].closedAt)) {
              datas[j].closedAt = time;
              datas[j].close = data["price"] / 1;
            }
            datas[j].volume += data["amount"] / 1;
          }
        }
      }
      // for (var i = 0; i < datas.length; i++) {
      //   print(
      //       "time: ${datas[i].createdAt}, open: ${datas[i].open}, close: ${datas[i].close},  high: ${datas[i].high}, low: ${datas[i].low}");
      // }
      return datas;
    } else {
      return [
        DataChartItem(
            createdAt: DateTime.now(),
            close: 0.0,
            low: 0.0,
            high: 0.0,
            open: 0.0,
            volume: 0.0,
            openedAt: null,
            closedAt: null),
      ];
    }
  }

  /// Get candle stick series
  List<CandleSeries<DataChartItem, DateTime>> _getCandleSeries(
      List<DataChartItem> dataChart) {
    return <CandleSeries<DataChartItem, DateTime>>[
      CandleSeries<DataChartItem, DateTime>(
          enableSolidCandles: true,
          dataSource: dataChart,
          xValueMapper: (DataChartItem sales, _) => sales.createdAt,
          lowValueMapper: (DataChartItem sales, _) => sales.low,
          highValueMapper: (DataChartItem sales, _) => sales.high,
          openValueMapper: (DataChartItem sales, _) => sales.open,
          closeValueMapper: (DataChartItem sales, _) => sales.close)
    ];
  }

  /// Get the hilo open close series
  List<HiloOpenCloseSeries<DataChartItem, DateTime>> _getHiloOpenCloseSeries(
      List<DataChartItem> dataChart) {
    return <HiloOpenCloseSeries<DataChartItem, DateTime>>[
      HiloOpenCloseSeries<DataChartItem, DateTime>(
          dataSource: dataChart,
          xValueMapper: (DataChartItem sales, _) => sales.createdAt,
          lowValueMapper: (DataChartItem sales, _) => sales.low,
          highValueMapper: (DataChartItem sales, _) => sales.high,
          openValueMapper: (DataChartItem sales, _) => sales.open,
          closeValueMapper: (DataChartItem sales, _) => sales.close),
    ];
  }

  /// Get default column series
  List<ColumnSeries<DataChartItem, DateTime>> _getDefaultColumnSeries(
      List<DataChartItem> dataChart) {
    return <ColumnSeries<DataChartItem, DateTime>>[
      ColumnSeries<DataChartItem, DateTime>(
          dataSource: dataChart,
          xValueMapper: (DataChartItem sales, _) => sales.createdAt,
          yValueMapper: (DataChartItem sales, _) => sales.volume,
          pointColorMapper: (DataChartItem sales, _) =>
              sales.open > sales.close ? Colors.red : Colors.green)
    ];
  }

  List<LineSeries<DataChartItem, DateTime>> _getDefaultLineSeries(
      List<DataChartItem> dataChart) {
    return <LineSeries<DataChartItem, DateTime>>[
      LineSeries<DataChartItem, DateTime>(
          dataSource: dataChart,
          xValueMapper: (DataChartItem sales, _) => sales.createdAt,
          yValueMapper: (DataChartItem sales, _) => sales.close,
          width: 2,
          color: Colors.green)
    ];
  }

  Widget chartOHLCV(List<DataChartItem> data, String rangeType) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // OHLC
        Flexible(
          flex: 2,
          child: SfCartesianChart(
            plotAreaBorderWidth: 0,
            trackballBehavior: TrackballBehavior(
                enable: true,
                activationMode: ActivationMode.singleTap,
                tooltipSettings: InteractiveTooltip(
                    arrowLength: 0, arrowWidth: 0, enable: true)),
            primaryXAxis: DateTimeAxis(isVisible: false),
            primaryYAxis: NumericAxis(
              labelFormat: '{value}',
              minimum: data
                          .reduce(
                              (curr, next) => curr.low < next.low ? curr : next)
                          .low <=
                      0
                  ? 0
                  : data
                          .reduce(
                              (curr, next) => curr.low < next.low ? curr : next)
                          .low -
                      20,
              maximum: data
                      .reduce((curr, next) => curr.low > next.low ? curr : next)
                      .low +
                  20,
              opposedPosition: true,
              numberFormat:
                  NumberFormat.compactCurrency(decimalDigits: 0, symbol: 'Rp '),
              labelStyle: TextStyle(fontSize: 8),
              majorGridLines: MajorGridLines(width: 0.5),
              axisLine: AxisLine(width: 0.5),
              minorGridLines: MinorGridLines(width: 0.5),
            ),
            series: _getHiloOpenCloseSeries(data),
          ),
        ),
        // volume
        Flexible(
          flex: 1,
          child: SfCartesianChart(
            trackballBehavior: TrackballBehavior(
                enable: true,
                activationMode: ActivationMode.singleTap,
                tooltipSettings: InteractiveTooltip(
                    arrowLength: 0,
                    arrowWidth: 0,
                    enable: true,
                    format: 'point.x\nVolume: point.y')),
            primaryXAxis: DateTimeAxis(dateFormat: dateFormat(rangeType)),
            primaryYAxis: NumericAxis(
              numberFormat: NumberFormat.compact(),
              labelFormat: '{value}',
              opposedPosition: true,
              maximumLabels: 6,
              labelStyle: TextStyle(fontSize: 8),
              majorGridLines: MajorGridLines(width: 0.5),
              axisLine: AxisLine(width: 0.5),
              minorGridLines: MinorGridLines(width: 0.5),
            ),
            series: _getDefaultColumnSeries(data),
          ),
        ),
      ],
    );
  }

  Widget chartCandle(List<DataChartItem> data, String rangeType) {
    return Container(
        height: 280,
        child: SfCartesianChart(
          primaryXAxis: DateTimeAxis(dateFormat: dateFormat(rangeType)),
          primaryYAxis: NumericAxis(
              minimum: data
                          .reduce(
                              (curr, next) => curr.low < next.low ? curr : next)
                          .low <=
                      0
                  ? 0
                  : data
                          .reduce(
                              (curr, next) => curr.low < next.low ? curr : next)
                          .low -
                      20,
              maximum: data
                      .reduce((curr, next) => curr.low > next.low ? curr : next)
                      .low +
                  20,
              opposedPosition: true,
              labelFormat: '{value}',
              numberFormat:
                  NumberFormat.compactCurrency(decimalDigits: 0, symbol: 'Rp '),
              labelStyle: TextStyle(fontSize: 8),
              majorGridLines: MajorGridLines(width: 0.5),
              axisLine: AxisLine(width: 0.5),
              minorGridLines: MinorGridLines(width: 0.5)),
          series: _getCandleSeries(data),
          trackballBehavior: TrackballBehavior(
              enable: true, activationMode: ActivationMode.singleTap),
        ));
  }

  Widget chartLine(List<DataChartItem> data, String rangeType) {
    return SfCartesianChart(
      primaryXAxis: DateTimeAxis(dateFormat: dateFormat(rangeType)),
      primaryYAxis: NumericAxis(
          opposedPosition: true,
          labelFormat: '{value}',
          numberFormat:
              NumberFormat.compactCurrency(decimalDigits: 0, symbol: 'Rp '),
          labelStyle: TextStyle(fontSize: 8),
          majorGridLines: MajorGridLines(width: 0.5),
          axisLine: AxisLine(width: 0.5),
          minorGridLines: MinorGridLines(width: 0.5)),
      series: _getDefaultLineSeries(data),
      trackballBehavior: TrackballBehavior(
          enable: true, activationMode: ActivationMode.singleTap),
    );
  }

  Widget chart(
      ChartTypeSyncfusion type, List<DataChartItem> data, String rangeType) {
    switch (type) {
      case ChartTypeSyncfusion.candle:
        return chartCandle(data, rangeType);
        break;
      case ChartTypeSyncfusion.ohlcv:
        return chartOHLCV(data, rangeType);
        break;
      case ChartTypeSyncfusion.line:
        return chartLine(data, rangeType);
        break;
      default:
        return Container();
    }
  }

  @override
  void initState() {
    super.initState();
    initMarketTrails();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataChartBloc, DataChartState>(
      builder: (context, state) {
        if (state is DataChartLoaded) {
          List<DataChartItem> data;
          String rangeType = state.time;
          if (state.data != []) {
            data = state.data2;
            rangeType = state.time;
          }
          if (data == null || data.length <= 0) {
            return Center(child: Text("Data tidak tersedia"));
          } else {
            return chart(widget.type, data, rangeType);
          }
        } else if (state is DataChartDailyLoaded) {
          String rangeType = state.time;
          Query queryPrice = FirebaseFirestore.instance
              .collection("current_price_${widget.codeEmiten}");
          Query query = FirebaseFirestore.instance
              .collection("match_histories_${widget.codeEmiten}")
              .orderBy("created_at", descending: false);
          return StreamBuilder<QuerySnapshot>(
              stream: queryPrice.snapshots(),
              builder: (context, stream) {
                QuerySnapshot querySnapshot = stream.data;
                double prev;
                if (stream.connectionState == ConnectionState.active) {
                  if (querySnapshot.docs.length > 0) {
                    var data = querySnapshot.docs[0].data();
                    prev = data["prev"] / 1;
                  }
                }
                return StreamBuilder<QuerySnapshot>(
                    stream: query.snapshots(),
                    builder: (context, stream) {
                      List<DataChartItem> data;
                      if (stream.connectionState == ConnectionState.active) {
                        QuerySnapshot querySnapshot = stream.data;
                        if (querySnapshot.docs.length > 0) {
                          data =
                              _parseDataStreamChart(querySnapshot.docs, prev);
                        }
                      }
                      if (data == null || data.length <= 0) {
                        return Center(child: Text("Data tidak tersedia"));
                      } else {
                        return chart(widget.type, data, rangeType);
                      }
                    });
              });
        } else {
          return Container();
        }
      },
    );
  }
}
