import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_common/src/chart/scatter_plot/point_renderer.dart';
import 'CustomCircleSymbolRenderer.dart';

/*
 * This class override PointRendererConfig to make possible paint label over dot in time series chart.
 * Rodrigo Dantas @rdantasnunes
*/
class CustomPointRendererConfig<num> extends charts.PointRendererConfig<num>
    implements charts.SeriesRendererConfig<num> {
  final List<String> yAxisValues;
  @override
  charts.PointRenderer<num> build() {
    return CustomPointRenderer<num>(yAxisValues,
        config: this, rendererId: customRendererId);
  }

  CustomPointRendererConfig(
    this.yAxisValues, {
    String customRendererId,
    int layoutPaintOrder,
    var pointRendererDecorators,
    double radiusPx,
    double boundsLineRadiusPx,
    double strokeWidthPx,
    CustomCircleSymbolRenderer symbolRenderer,
    var customSymbolRenderers,
  }) : super(
          customRendererId: customRendererId,
          layoutPaintOrder: (layoutPaintOrder == null
              ? charts.LayoutViewPaintOrder.point
              : layoutPaintOrder),
          pointRendererDecorators: (pointRendererDecorators == null
              ? const []
              : pointRendererDecorators),
          radiusPx: (radiusPx == null ? 3.5 : radiusPx),
          boundsLineRadiusPx: boundsLineRadiusPx,
          strokeWidthPx: strokeWidthPx == null ? 0.0 : strokeWidthPx,
          symbolRenderer: symbolRenderer,
          customSymbolRenderers: customSymbolRenderers,
        );
}

class CustomPointRenderer<D> extends charts.PointRenderer<D> {
  final List<String> yAxisValues;

  CustomPointRenderer(
    this.yAxisValues, {
    String rendererId,
    charts.PointRendererConfig config,
  }) : super(rendererId: rendererId, config: config);

  void paint(charts.ChartCanvas canvas, double animationPercent) {
    // Clean up the points that no longer exist.
    if (animationPercent == 1.0) {
      final keysToRemove = <String>[];

      seriesPointMap.forEach((String key, List<AnimatedPoint<D>> points) {
        points.removeWhere((AnimatedPoint<D> point) => point.animatingOut);

        if (points.isEmpty) {
          keysToRemove.add(key);
        }
      });

      keysToRemove.forEach((String key) => seriesPointMap.remove(key));
    }

    seriesPointMap.forEach((String key, List<AnimatedPoint<D>> points) {
      points
          .map<PointRendererElement<D>>((AnimatedPoint<D> animatingPoint) =>
              animatingPoint.getCurrentPoint(animationPercent))
          .forEach((PointRendererElement point) {
        // Decorate the points with decorators that should appear below the main
        // series data.
        pointRendererDecorators
            .where((charts.PointRendererDecorator decorator) =>
                !decorator.renderAbove)
            .forEach((charts.PointRendererDecorator decorator) {
          decorator.decorate(point, canvas, graphicsFactory,
              drawBounds: componentBounds,
              animationPercent: animationPercent,
              rtl: isRtl);
        });

        // Skip points whose center lies outside the draw bounds. Those that lie
        // near the edge will be allowed to render partially outside. This
        // prevents harshly clipping off half of the shape.
        if (point.point.y != null &&
            componentBounds.containsPoint(point.point)) {
          final bounds = Rectangle<double>(
              point.point.x - point.radiusPx,
              point.point.y - point.radiusPx,
              point.radiusPx * 2,
              point.radiusPx * 2);

          if (point.symbolRendererId == defaultSymbolRendererId) {
            CustomCircleSymbolRenderer c = symbolRenderer;
            c.paint(canvas, bounds,
                fillColor: point.fillColor,
                strokeColor: point.color,
                strokeWidthPx: point.strokeWidthPx,
                text: yAxisValues.elementAt(point.index));
          } else {
            final id = point.symbolRendererId;
            if (!config.customSymbolRenderers.containsKey(id)) {
              throw ArgumentError('Invalid custom symbol renderer id "${id}"');
            }

            final customRenderer = config.customSymbolRenderers[id];
            customRenderer.paint(canvas, bounds,
                fillColor: point.fillColor,
                strokeColor: point.color,
                strokeWidthPx: point.strokeWidthPx);
          }
        }

        // Decorate the points with decorators that should appear above the main
        // series data. This is the typical place for labels.
        pointRendererDecorators
            .where((charts.PointRendererDecorator decorator) =>
                decorator.renderAbove)
            .forEach((charts.PointRendererDecorator decorator) {
          decorator.decorate(point, canvas, graphicsFactory,
              drawBounds: componentBounds,
              animationPercent: animationPercent,
              rtl: isRtl);
        });
      });
    });
  }
}
