/// Bar chart example
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:flutter_color/flutter_color.dart' as hex;
import 'package:santaraapp/utils/sizes.dart';

class StackedChart extends StatefulWidget {
  final List<ListDatum> seriesList;
  final bool animate;

  StackedChart(this.seriesList, {this.animate});
  @override
  _StackedChartState createState() => _StackedChartState();
}

class _StackedChartState extends State<StackedChart>
    with AutomaticKeepAliveClientMixin {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  List<charts.Series<Sale, String>> series = [];
  // ngedapetin nama cabang pertama

  @override
  void initState() {
    super.initState();
    ListDatum firstValue =
        widget?.seriesList?.length != null && widget.seriesList.length > 0
            ? widget.seriesList[0]
            : null;
    widget.seriesList.forEach((element) {
      var dumm = charts.Series<Sale, String>(
        id: element.desc,
        colorFn: (_, __) =>
            charts.ColorUtil.fromDartColor(hex.HexColor("#${element.color}")),
        domainFn: (Sale sales, _) => " ${sales.month}\n(${sales.year})",
        measureFn: (Sale sales, _) => sales.value,
        data: element.sales,
        // Set a label accessor to control the text of the bar label.
        // labelAccessorFn: (Sale sales, _) => firstValue != null
        //     ? element.desc == firstValue.desc ? '${sales.value}' : ''
        //     : '',
        labelAccessorFn: (Sale sales, _) =>
            sales.value != 0.0 ? "${sales.value}" : "",
      );
      series.add(dumm);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return charts.BarChart(
      series,
      animate: widget.animate,
      // Configure the default renderer as a bar renderer.
      defaultRenderer: charts.BarRendererConfig(
        groupingType: charts.BarGroupingType.stacked,
        stackHorizontalSeparator: 50,
        barRendererDecorator: charts.BarLabelDecorator(
          // labelAnchor: charts.BarLabelAnchor.middle,
          labelPosition: charts.BarLabelPosition.outside,
          insideLabelStyleSpec: charts.TextStyleSpec(
            fontSize: FontSize.s8.toInt(),
            color: charts.ColorUtil.fromDartColor(Colors.white),
            // lineHeight: 0.14,
            fontWeight: '500',
          ),
          outsideLabelStyleSpec: charts.TextStyleSpec(
            fontSize: FontSize.s8.toInt(),
            color: charts.ColorUtil.fromDartColor(Color(0xff202020)),
            lineHeight: 0.14,
            // fontWeight: '700',
          ),
        ),
      ),
      behaviors: [
        charts.SeriesLegend(
          position: charts.BehaviorPosition.bottom,
          outsideJustification: charts.OutsideJustification.middleDrawArea,
          desiredMaxColumns: 1,
          desiredMaxRows: 1,
        )
      ],
      primaryMeasureAxis: charts.NumericAxisSpec(
        renderSpec: charts.NoneRenderSpec(),
        showAxisLine: false, // show y line
      ),
      // domainAxis: charts.OrdinalAxisSpec(
      //   // Make sure that we draw the domain axis line.
      //   showAxisLine: true,
      //   // But don't draw anything else.
      //   renderSpec: charts.NoneRenderSpec(),
      // ),
      // Custom renderer configuration for the line series. This will be used for
      // any series that does not define a rendererIdKey.
      customSeriesRenderers: [
        charts.LineRendererConfig(
          // ID used to link series to this renderer.
          customRendererId: 'Desktop',
          dashPattern: [5],
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
