/// Bar chart example
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

/// Example of an ordinal combo chart with two series rendered as stacked bars, and a
/// third rendered as a line.

class OrdinalComboBarLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  OrdinalComboBarLineChart(this.seriesList, {this.animate});

  factory OrdinalComboBarLineChart.withSampleData() {
    return OrdinalComboBarLineChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      seriesList,
      animate: animate,
      // Configure the default renderer as a bar renderer.
      defaultRenderer: charts.BarRendererConfig(
        groupingType: charts.BarGroupingType.stacked,
        stackHorizontalSeparator: 50,
        barRendererDecorator: charts.BarLabelDecorator(
          // labelAnchor: charts.BarLabelAnchor.middle,
          labelPosition: charts.BarLabelPosition.outside,
          insideLabelStyleSpec: charts.TextStyleSpec(
            fontSize: 12,
            color: charts.ColorUtil.fromDartColor(Colors.white),
            // lineHeight: 0.14,
            fontWeight: '500',
          ),
          outsideLabelStyleSpec: charts.TextStyleSpec(
            fontSize: 14,
            color: charts.ColorUtil.fromDartColor(Color(0xff202020)),
            lineHeight: 0.14,
            // fontWeight: '700',
          ),
        ),
      ),
      behaviors: [
        charts.SeriesLegend(
          position: charts.BehaviorPosition.bottom,
          outsideJustification: charts.OutsideJustification.middleDrawArea,
        )
      ],
      primaryMeasureAxis: charts.NumericAxisSpec(
        renderSpec: charts.NoneRenderSpec(),
        showAxisLine: false, // show y line
      ),
      // domainAxis: charts.OrdinalAxisSpec(
      //   // Make sure that we draw the domain axis line.
      //   showAxisLine: true,
      //   // But don't draw anything else.
      //   renderSpec: charts.NoneRenderSpec(),
      // ),
      // Custom renderer configuration for the line series. This will be used for
      // any series that does not define a rendererIdKey.
      customSeriesRenderers: [
        charts.LineRendererConfig(
          // ID used to link series to this renderer.
          customRendererId: 'Desktop',
          dashPattern: [5],
        )
      ],
    );
  }

  /// Create series list with multiple series
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final desktopSalesData = [
      OrdinalSales('Feb', 360),
      OrdinalSales('Mar', 360),
      OrdinalSales('Apr', 360),
      OrdinalSales('Mei', 360),
    ];

    final tableSalesData = [
      OrdinalSales('Feb', 250),
      OrdinalSales('Mar', 220),
      OrdinalSales('Apr', 280),
      OrdinalSales('Mei', 200),
    ];

    final mobileSalesData = [
      OrdinalSales('Feb', 400),
      OrdinalSales('Mar', 250),
      OrdinalSales('Apr', 150),
      OrdinalSales('Mei', 100),
    ];

    return [
      charts.Series<OrdinalSales, String>(
        id: 'Cabang 1',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xffAAD58B)),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: desktopSalesData,
        // Set a label accessor to control the text of the bar label.
        labelAccessorFn: (OrdinalSales sales, _) =>
            '${sales.sales.toString()},0',
      ),
      charts.Series<OrdinalSales, String>(
        id: 'Cabang 2',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xffFFA643)),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: tableSalesData,
        labelAccessorFn: (OrdinalSales sales, _) => '',
      ),
      charts.Series<OrdinalSales, String>(
        id: 'Cabang 3',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xffF56190)),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: mobileSalesData,
        labelAccessorFn: (OrdinalSales sales, _) => '',
      )
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
