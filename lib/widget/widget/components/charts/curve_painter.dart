import 'package:flutter/material.dart';

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.grey;
    paint.strokeWidth = 1;
    canvas.drawLine(
      Offset(0, size.height / 2),
      Offset(size.width, size.height / 2),
      paint,
    );

    final rect = Rect.fromCenter(
      center: Offset(size.width / 2, size.height / 2),
      width: 50,
      height: 30,
    );

    paint.color = Colors.white;
    paint.style = PaintingStyle.fill;
    paintBorder(canvas, rect);
    canvas.drawOval(
      rect,
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
