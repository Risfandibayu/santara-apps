import 'package:charts_common/src/common/color.dart' as chart_color;
import 'package:charts_flutter/src/text_style.dart' as style;
import 'package:charts_flutter/src/text_element.dart' as te;
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math';

import 'package:flutter/material.dart';

/*
 * This class paint a dot in chart with a label over it.
 * This class was made based on Stanislaw Góra's (@stasgora) 
 * code on https://github.com/google/charts/issues/58#issuecomment-488695645
* Thank you @stasgora
 * Rodrigo Dantas @rdantasnunes
 */
class CustomCircleSymbolRenderer extends charts.CircleSymbolRenderer {
  final double fontSize;
  CustomCircleSymbolRenderer({this.fontSize = 12});

  @override
  void paint(charts.ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
      chart_color.Color fillColor,
      charts.FillPatternType fillPattern,
      chart_color.Color strokeColor,
      double strokeWidthPx,
      String text}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);

    // if (text == null || text.isEmpty || text == "0") return;
    // Tirei este desenho para não ficar o fundo branco.
    // canvas.drawRect(
    //     Rectangle(bounds.left - 5, bounds.top - 30, bounds.width + 10,
    //         bounds.height + 10),
    //     fill: chart_color.Color.white);

    var textStyle = style.TextStyle();
    textStyle.color = charts.ColorUtil.fromDartColor(Colors.blue);
    textStyle.fontSize = fontSize.toInt();
    canvas.drawText(te.TextElement(text, style: textStyle),
        (bounds.left - 15).round(), (bounds.top - 18).round());
  }
}
