/// Example of a numeric combo chart with two series rendered as lines, and a
/// third rendered as points along the top line with a different color.
///
/// This example demonstrates a method for drawing points along a line using a
/// different color from the main series color. The line renderer supports
/// drawing points with the "includePoints" option, but those points will share
/// the same color as the line.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'CustomCircleSymbolRenderer.dart';
import 'CustomPointRendererConfig.dart';

class NumericComboLinePointChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  NumericComboLinePointChart(this.seriesList, {this.animate});

  /// Creates a [LineChart] with sample data and no transition.
  factory NumericComboLinePointChart.withSampleData() {
    return NumericComboLinePointChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.NumericComboChart(
      seriesList,
      animate: animate,
      primaryMeasureAxis: charts.NumericAxisSpec(
        renderSpec: charts.NoneRenderSpec(),
        showAxisLine: false,
      ),
      domainAxis: charts.NumericAxisSpec(
        // Make sure that we draw the domain axis line.
        showAxisLine: false,
        // But don't draw anything else.
        renderSpec: charts.NoneRenderSpec(),
      ),
      defaultRenderer: charts.LineRendererConfig(
        includePoints: true,
        dashPattern: [3],
        strokeWidthPx: 1,
      ),
      // Configure the default renderer as a line renderer. This will be used
      // for any series that does not define a rendererIdKey.
      // Custom renderer configuration for the point series.

      customSeriesRenderers: [
        CustomPointRendererConfig(
          ["29,38", "17,11", "-7,70", "-13,32"],
          symbolRenderer: CustomCircleSymbolRenderer(),
          customRendererId: 'customPoint',
        ),
      ],
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData() {
    final tableSalesData = [
      LinearSales(0, 100),
      LinearSales(1, 90),
      LinearSales(2, 110),
      LinearSales(3, 50),
    ];

    final mobileSalesData = [
      LinearSales(0, 100),
      LinearSales(1, 90),
      LinearSales(2, 110),
      LinearSales(3, 50),
    ];

    return [
      charts.Series<LinearSales, int>(
        id: 'Tablet',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: tableSalesData,
      ),
      charts.Series<LinearSales, int>(
        id: 'Mobile',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: mobileSalesData,
      )
        // Configure our custom point renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customPoint'),
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
