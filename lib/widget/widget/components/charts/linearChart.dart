/// Example of a numeric combo chart with two series rendered as lines, and a
/// third rendered as points along the top line with a different color.
///
/// This example demonstrates a method for drawing points along a line using a
/// different color from the main series color. The line renderer supports
/// drawing points with the "includePoints" option, but those points will share
/// the same color as the line.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'CustomCircleSymbolRenderer.dart';
import 'CustomPointRendererConfig.dart';

class LinearChart extends StatefulWidget {
  final bool animate;
  final List<ListNetIncome> listData;

  LinearChart({this.animate, this.listData});
  @override
  _LinearChartState createState() => _LinearChartState();
}

class _LinearChartState extends State<LinearChart>
    with AutomaticKeepAliveClientMixin {
  List<charts.Series<LinearSalesV2, int>> series = [];
  List<String> points = [];

  // List<LinearSalesV2> datax = [
  //   LinearSalesV2("25", 25.0),
  //   LinearSalesV2("25.0", 25.0 + 0.01),
  //   LinearSalesV2("25.0", 25.0),
  // ];

  @override
  void initState() {
    super.initState();
    var i = 0;

    List<LinearSalesV2> dummies = [];
    // widget.listData.forEach((element) {
    //   if (element.sales.length > 0) {
    //     dummies.add(LinearSalesV2("${i++}", element.netIncome));
    //     points.add("${element.netIncome}");
    //   }
    // });

    widget.listData.asMap().forEach((index, element) {
      points.add("${element.netIncome}");
      if (index > 0) {
        // get current value
        if (element.netIncome == widget.listData[index - 1].netIncome) {
          element.netIncome += 0.01;
        }
      }
      dummies.add(LinearSalesV2("${i++}", element.netIncome));
    });

    // for (var i = 0; i < widget.listData.length - 1; i++) {
    //   print(">> Net Income [2] : ${widget.listData[i].netIncome}");
    //   if (i > 0) {
    //     if (widget.listData[i].netIncome == widget.listData[i - 1].netIncome) {
    //       widget.listData[i].netIncome += 0.01;
    //     }
    //   }
    //   dummies.add(LinearSalesV2("${i++}", widget.listData[i].netIncome));
    //   points.add("${widget.listData[i].netIncome}");
    // }

    // dummies.add(LinearSalesV2("${i++}", 1.0));
    //   points.add("");

    // datax.forEach((element) {
    //   print(">> Net Income [2] : ${element.sales}");
    //   dummies.add(LinearSalesV2("${i++}", element.sales));
    //   points.add("${element.year}");
    // });

    // chart 1
    var chart = charts.Series<LinearSalesV2, int>(
      id: 'Mobile',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (LinearSalesV2 sales, _) => int.parse(sales.year),
      measureFn: (LinearSalesV2 sales, _) => sales.sales,
      data: dummies,
    );
    series.add(chart);
    // // chart 2
    var chart2 = charts.Series<LinearSalesV2, int>(
      id: 'Mobile2',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (LinearSalesV2 sales, _) => int.parse(sales.year),
      measureFn: (LinearSalesV2 sales, _) => sales.sales,
      data: dummies,
    )
      // Configure our custom point renderer for this series.
      ..setAttribute(charts.rendererIdKey, 'customPoint');
    series.add(chart2);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return charts.NumericComboChart(
      series,
      animate: widget.animate,
      primaryMeasureAxis: charts.NumericAxisSpec(
        renderSpec: charts.NoneRenderSpec(),
        showAxisLine: false,
      ),
      domainAxis: charts.NumericAxisSpec(
        // Make sure that we draw the domain axis line.
        showAxisLine: false,
        // But don't draw anything else.
        renderSpec: charts.NoneRenderSpec(),
      ),
      defaultRenderer: charts.LineRendererConfig(
        includePoints: true,
        dashPattern: [3],
        strokeWidthPx: 1,
      ),
      // Configure the default renderer as a line renderer. This will be used
      // for any series that does not define a rendererIdKey.
      // Custom renderer configuration for the point series.

      customSeriesRenderers: [
        CustomPointRendererConfig(
          points,
          symbolRenderer: CustomCircleSymbolRenderer(
            fontSize: FontSize.s10,
          ),
          customRendererId: 'customPoint',
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

/// Sample linear data type.
class LinearSalesV2 {
  final String year;
  final double sales;

  LinearSalesV2(this.year, this.sales);
}
