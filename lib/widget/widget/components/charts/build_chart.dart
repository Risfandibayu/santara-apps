import 'package:flutter/material.dart';
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'curve_painter.dart';
import 'stacked-v2.dart';
import 'linearChart.dart';
import 'dart:math' as math;

// ignore: must_be_immutable
class BuildFinancialChart extends StatelessWidget {
  final SahamData data;
  BuildFinancialChart({this.data});
  double angleValue = 0.0;
  Color color = Colors.white;

  angle() {
    if (data?.listData != null) {
      if (data?.performance != null) {
        if (data.performance < 0) {
          angleValue = math.pi / 20;
          color = Colors.red;
        } else if (data.performance > 0) {
          angleValue = -math.pi / 20;
          color = Colors.green;
        } else {
          angleValue = 0.0;
          color = Colors.black;
        }
      } else {
        angleValue = 0.0;
        color = Colors.black;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    angle();
    return data?.listData != null && data.listData.length > 0
        ? SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              // dinamis width, menyesuaikan jumlah data chart
              width: data.listNetIncome.length < 7
                  ? MediaQuery.of(context).size.width
                  : (70 * data.listNetIncome.length / 1),
              child: InteractiveViewer(
                panEnabled: false, // Set it to false to prevent panning.
                boundaryMargin: EdgeInsets.all(80),
                minScale: 1,
                maxScale: 3,
                child: Stack(
                  children: [
                    // Container(
                    //   alignment: Alignment.bottomLeft,
                    //   margin: EdgeInsets.only(left: Sizes.s20, top: Sizes.s35),
                    //   height: Sizes.s250,
                    //   width: Sizes.s2,
                    //   color: Colors.grey,
                    // ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: Sizes.s20),
                              child: Text(
                                "Sales Revenue\n(Rp juta)",
                                style: TextStyle(
                                  fontSize: FontSize.s10,
                                  color: Color(ColorRev.mainwhite),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: Sizes.s20),
                              child: Text(
                                "Net Income\n(Rp juta)",
                                style: TextStyle(
                                  fontSize: FontSize.s10,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: Sizes.s10),
                        Container(
                          margin: EdgeInsets.only(
                            left: Sizes.s20,
                            right: Sizes.s20,
                          ),
                          height: Sizes.s100,
                          // width: MediaQuery.of(context).size.width - 50,
                          child: LinearChart(
                            animate: false,
                            listData: data?.listNetIncome ?? [],
                          ),
                        ),
                        SizedBox(height: Sizes.s20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          child: Transform.rotate(
                            angle: angleValue,
                            child: CustomPaint(
                              painter: CurvePainter(),
                              child: Center(
                                child: Text(
                                  "${data?.performance ?? ''}%",
                                  style: TextStyle(
                                    color: color,
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: Sizes.s20),
                        Container(
                          height:
                              Sizes.s300 + (30 * (data?.listData?.length ?? 0)),
                          child: StackedChart(data?.listData ?? []),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        : Center(
            child: Container(
              padding: EdgeInsets.all(Sizes.s10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(height: Sizes.s20),
                  Image.asset(
                    "assets/icon/no_financial_statements.png",
                    height: Sizes.s180,
                  ),
                  SizedBox(height: Sizes.s20),
                  Text(
                    "Belum Ada Laporan Keuangan",
                    style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  SizedBox(height: Sizes.s10),
                  Text(
                    "Penerbit ini belum memiliki laporan keuangan",
                    style: TextStyle(
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w300,
                        color: Colors.white),
                  ),
                  // _buildButtons(emiten.prospektus),
                ],
              ),
            ),
          );
  }
}
