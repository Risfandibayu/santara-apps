// /// Bar chart example
// import 'package:flutter/material.dart';
// import 'package:charts_flutter/flutter.dart' as charts;
// import 'package:santaraapp/models/ListPortfolio.dart';

// /// Example of an ordinal combo chart with two series rendered as stacked bars, and a
// /// third rendered as a line.

// class StackedChart extends StatelessWidget {
//   final List<ListPortfolioData> seriesList;
//   final bool animate;

//   StackedChart(this.seriesList, {this.animate});

//   @override
//   Widget build(BuildContext context) {
//     List<charts.Series<BarPercentage, String>> series = [];
//     seriesList.forEach((element) {
//       // print(element.month);
//       var dumm = charts.Series<BarPercentage, String>(
//         id: element.month,
//         colorFn: (_, __) => charts.ColorUtil.fromDartColor(Color(0xffAAD58B)),
//         domainFn: (BarPercentage sales, _) => "${sales.desc}",
//         measureFn: (BarPercentage sales, _) => sales.percentage,
//         data: element.barPercentage,
//         // Set a label accessor to control the text of the bar label.
//         labelAccessorFn: (BarPercentage sales, _) => '${sales.percentage},0',
//       );
//       series.add(dumm);
//     });

//     return charts.BarChart(
//       series,
//       animate: animate,
//       // Configure the default renderer as a bar renderer.
//       defaultRenderer: charts.BarRendererConfig(
//         groupingType: charts.BarGroupingType.stacked,
//         stackHorizontalSeparator: 50,
//         barRendererDecorator: charts.BarLabelDecorator(
//           // labelAnchor: charts.BarLabelAnchor.middle,
//           labelPosition: charts.BarLabelPosition.outside,
//           insideLabelStyleSpec: charts.TextStyleSpec(
//             fontSize: 12,
//             color: charts.ColorUtil.fromDartColor(Colors.white),
//             // lineHeight: 0.14,
//             fontWeight: '500',
//           ),
//           outsideLabelStyleSpec: charts.TextStyleSpec(
//             fontSize: 14,
//             color: charts.ColorUtil.fromDartColor(Color(0xff202020)),
//             lineHeight: 0.14,
//             // fontWeight: '700',
//           ),
//         ),
//       ),
//       behaviors: [
//         charts.SeriesLegend(
//           position: charts.BehaviorPosition.bottom,
//           outsideJustification: charts.OutsideJustification.middleDrawArea,
//         )
//       ],
//       primaryMeasureAxis: charts.NumericAxisSpec(
//         renderSpec: charts.NoneRenderSpec(),
//         showAxisLine: false, // show y line
//       ),
//       // domainAxis: charts.OrdinalAxisSpec(
//       //   // Make sure that we draw the domain axis line.
//       //   showAxisLine: true,
//       //   // But don't draw anything else.
//       //   renderSpec: charts.NoneRenderSpec(),
//       // ),
//       // Custom renderer configuration for the line series. This will be used for
//       // any series that does not define a rendererIdKey.
//       customSeriesRenderers: [
//         charts.LineRendererConfig(
//           // ID used to link series to this renderer.
//           customRendererId: 'Desktop',
//           dashPattern: [5],
//         )
//       ],
//     );
//   }
// }
