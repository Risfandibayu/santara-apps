import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class KycWrapper extends StatelessWidget {
  final String title;
  final Widget subtitle;
  final Widget description;
  final String image;
  final bool isCenter;

  KycWrapper({
    @required this.title,
    @required this.subtitle,
    this.description,
    @required this.image,
    this.isCenter = false,
  });

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 2;
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Color(0xffF4F5FA),
      child: Column(
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            child: Image.asset(
              "$image",
              // width: Sizes.s200,
              // height: height / 1.4,
            ),
            height: height,
            width: double.maxFinite,
          ),
          Container(
            width: double.maxFinite,
            child: Padding(
              padding: EdgeInsets.only(left: Sizes.s50, right: Sizes.s50),
              child: Column(
                children: [
                  SizedBox(
                    height: Sizes.s60,
                  ),
                  Text(
                    "$title",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: FontSize.s20,
                    ),
                  ),
                  SizedBox(height: Sizes.s15),
                  isCenter
                      ? Center(
                          child: subtitle,
                        )
                      : subtitle,
                  SizedBox(height: Sizes.s15),
                  description != null ? description : Container()
                ],
              ),
            ),
            height: height,
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}
