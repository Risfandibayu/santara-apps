import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class KycTitleWrapper extends StatelessWidget {
  final String title;
  KycTitleWrapper({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "$title",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: FontSize.s16,
          ),
        ),
        // divider line
        Container(
          margin: EdgeInsets.only(top: Sizes.s10),
          height: 4,
          width: double.maxFinite,
          color: Color(0xffF4F4F4),
        ),
      ],
    );
  }
}

class KycViewAppbar extends StatelessWidget implements PreferredSizeWidget {
  final bottom;
  final String title;
  final String steps;
  final Function onEdit;
  KycViewAppbar({
    this.bottom,
    @required this.title,
    @required this.steps,
    @required this.onEdit,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black,
      centerTitle: true,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "$title",
            style: TextStyle(
              color: Colors.white,
              fontSize: FontSize.s18,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            "$steps",
            style: TextStyle(
              color: Color(0xff676767),
              fontSize: FontSize.s8,
            ),
          ),
        ],
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      // actions: [IconButton(icon: Icon(Icons.edit), onPressed: onEdit)],
    );
  }

  @override
  Size get preferredSize =>
      Size.fromHeight(kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0));
}

class KycItemTile extends StatelessWidget {
  final String title;
  final String subtitle;
  KycItemTile({@required this.title, @required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      title: Text(
        "$title",
        style: TextStyle(
          color: Colors.black,
          fontSize: FontSize.s10,
        ),
      ),
      subtitle: Text(
        subtitle == "null" || subtitle == null ? "-" : "$subtitle",
        style: TextStyle(
          fontWeight: FontWeight.w600,
          color: Colors.black,
          fontSize: FontSize.s12,
        ),
      ),
    );
  }
}

class PreviewNotification extends StatelessWidget {
  final VoidCallback onClose;
  PreviewNotification({this.onClose});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: Sizes.s60,
      padding: EdgeInsets.only(left: Sizes.s20, right: Sizes.s10),
      decoration: BoxDecoration(
        color: Color(0xffFDE9CD),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        border: Border.all(width: 1, color: Color(0xffF6911A)),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(92, 92, 92, 0.25),
            blurRadius: 5,
            offset: Offset(0, 5),
          )
        ],
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.info,
                  color: Color(0xffF6911A),
                  size: FontSize.s25,
                ),
                SizedBox(
                  width: Sizes.s10,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Anda sedang dalam mode preview",
                        style: TextStyle(
                          fontSize: FontSize.s13,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          InkWell(
            onTap: onClose,
            child: Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: EdgeInsets.only(top: Sizes.s5),
                child: Icon(
                  Icons.close,
                  size: FontSize.s15,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
