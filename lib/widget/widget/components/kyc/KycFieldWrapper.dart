// tag identifier
// untuk nandain setiap widget
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class KycFieldWrapper {
  // constructor class
  final AutoScrollController controller;
  KycFieldWrapper({@required this.controller});

  // divier ( kotak pembatas )
  get divider {
    return SizedBox(
      height: Sizes.s30,
    );
  }

  // nandain widget dengan index
  Widget viewWidget(int index, double height, Widget widget) {
    return _wrapScrollTag(
      index: index,
      child: Container(
        padding: EdgeInsets.only(bottom: Sizes.s10),
        alignment: Alignment.topCenter,
        // height: height,
        child: widget,
      ),
    );
  }

  Widget wrap({@required int index, @required Widget child}) {
    return _wrapScrollTag(
      index: index,
      child: child,
    );
  }

// tag identifier
// untuk highlight jika ada error
  Widget _wrapScrollTag({int index, Widget child}) => AutoScrollTag(
        key: ValueKey(index),
        controller: controller,
        index: index,
        child: child,
        highlightColor: Colors.black.withOpacity(0.1),
      );

  // scroll ke offset (pixel listview) yg sudah ditentukan
  // digunakan ketika method scrollTo mengalami error
  void _scrollToOffset(int index) async {
    double offset = index * Sizes.s100;
    await controller.animateTo(
      offset,
      duration: Duration(milliseconds: 300),
      curve: Curves.linear,
    );
    controller.highlight(index);
  }

// auto scroll ke spesifik widget
  void scrollTo(int index) async {
    try {
      await controller.scrollToIndex(
        index,
        preferPosition: AutoScrollPosition.begin,
      );
      controller.highlight(index);
      // jika fungsi scroll ke field tidak berjalan, maka gunakan method scrollToOffset
      if (controller.offset >= (controller.position.maxScrollExtent - 100))
        _scrollToOffset(index);
    } catch (e) {
      // jika terjadi error ketika melakukan scroll maka gunakan method scrollToOffset
      _scrollToOffset(index);
    }
  }
}

class KycDatePicker extends StatelessWidget {
  final bool enabled;
  final TextFieldBloc day;
  final SelectFieldBloc month;
  final TextFieldBloc year;

  KycDatePicker({
    this.enabled = true,
    @required this.day,
    @required this.month,
    @required this.year,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Container(
            height: Sizes.s100,
            child: SantaraBlocTextField(
              enabled: enabled,
              textFieldBloc: day,
              hintText: "01",
              labelText: "Tanggal",
              inputType: TextInputType.datetime,
              inputFormatters: [
                LengthLimitingTextInputFormatter(2),
                FilteringTextInputFormatter.allow(
                  RegExp(r"[0-9]*$"),
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            margin: EdgeInsets.only(
              left: Sizes.s10,
              right: Sizes.s10,
            ),
            height: Sizes.s100,
            child: SantaraBlocDropDown(
              enabled: enabled,
              selectFieldBloc: month,
              label: "Bulan",
              hintText: "Januari",
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            height: Sizes.s100,
            child: SantaraBlocTextField(
              enabled: enabled,
              textFieldBloc: year,
              hintText: "2020",
              labelText: "Tahun",
              inputType: TextInputType.datetime,
              inputFormatters: [
                LengthLimitingTextInputFormatter(4),
                FilteringTextInputFormatter.allow(
                  RegExp(r"[0-9]*$"),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
