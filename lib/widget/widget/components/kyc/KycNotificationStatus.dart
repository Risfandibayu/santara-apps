import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../models/StatusKycTrader.dart';
import '../../../../utils/sizes.dart';
import '../../../kyc/kyc_pribadi_new/syarat_ketentuan/SyaratKetentuanUI.dart';
import '../../../kyc/widgets/pra_kyc.dart';
import '../../../setting_account/kyc_perusahaan.dart';
import '../../../setting_account/kyc_pribadi.dart';
import 'blocs/kyc.notification.bloc.dart';

class KycNotificationStatus extends StatelessWidget {
  final bool showCloseButton;
  KycNotificationStatus({this.showCloseButton = true});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => KycNotifBloc(
        KycNotifUninitialized(),
      )..add(KycNotifEvent()),
      child: KycNotificationBody(
        showCloseButton: showCloseButton,
      ),
    );
  }
}

class KycNotificationBody extends StatefulWidget {
  final bool showCloseButton;
  KycNotificationBody({this.showCloseButton});
  @override
  _KycNotificationBodyState createState() => _KycNotificationBodyState();
}

class _KycNotificationBodyState extends State<KycNotificationBody> {
  KycNotifBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<KycNotifBloc>(context);
  }

  Widget _notifKyc(String title, String message, bool isVerify, Widget next) {
    return InkWell(
      onTap: isVerify
          ? null
          : () async {
              await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => next),
              );
              bloc..add(KycNotifEvent());
            },
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(Sizes.s15),
            decoration: BoxDecoration(
              border: Border.all(
                  width: 1,
                  color: isVerify ? Color(0xffF6911A) : Color(0xffFC6A61)),
              borderRadius: BorderRadius.circular(Sizes.s10),
              color: isVerify ? Color(0xffFDE9CD) : Color(0xffFFE2DF),
            ),
            height: Sizes.s80,
            width: double.maxFinite,
            child: Container(
              margin: EdgeInsets.only(left: Sizes.s25, top: Sizes.s15),
              // color: Colors.blue,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.info,
                    color: isVerify ? Color(0xffF6911A) : Color(0xffFC6A61),
                    size: FontSize.s25,
                  ),
                  SizedBox(
                    width: Sizes.s10,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "$title",
                          style: TextStyle(
                            fontSize: FontSize.s13,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "$message",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              decorationStyle:
                                  isVerify ? null : TextDecorationStyle.solid,
                              decoration:
                                  isVerify ? null : TextDecoration.underline,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showNotifCompleteKyc(bool isCompleteKyc, bool tnc) {
    if (isCompleteKyc) {
      return tnc
          ? Container()
          : _notifKyc(
              "Pembaharuan Syarat dan Ketentuan",
              "Tap disini untuk membaca syarat dan ketentuan",
              false,
              KycSyaratKetentuanUI(),
            );
    } else {
      return _notifKyc(
        "Mohon lengkapi data Anda untuk mulai mendanai.",
        "Klik disini untuk melengkapi data Anda sekarang",
        false,
        PraKyc(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<KycNotifBloc, KycNotifState>(
      builder: (context, state) {
        if (state is KycNotifUninitialized) {
          return Container();
        } else if (state is KycNotifError) {
          return Container();
        } else if (state is KycNotifLoaded) {
          KycNotifLoaded data = state;
          return data.showNotification
              ? Stack(
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        data.status != null
                            ? data.status.status != EndStatus.verified
                                ? data.status.isVerifying && !data.tncStatus
                                    ? Container()
                                    : _notifKyc(
                                        data.status.title,
                                        data.status.subtitle,
                                        data.status.isVerifying,
                                        data.traderType == "personal"
                                            ? KycPribadiSetting(
                                                preview: false,
                                              )
                                            : KycPerusahaanSetting(
                                                preview: false,
                                              ),
                                      )
                                : Container()
                            : Container(),
                        data.status == null ||
                                data.status.status == EndStatus.verified ||
                                data.status.isVerifying
                            ? _showNotifCompleteKyc(
                                data.isCompleteKyc,
                                data.tncStatus,
                              )
                            : Container()
                      ],
                    ),
                    widget.showCloseButton
                        ? data.status != null
                            ? Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      size: FontSize.s12,
                                    ),
                                    onPressed: () {
                                      bloc.add(KycHideNotification());
                                    }),
                              )
                            : Container()
                        : Container()
                  ],
                )
              : Container();
        } else {
          return Container();
        }
      },
    );
  }
}
