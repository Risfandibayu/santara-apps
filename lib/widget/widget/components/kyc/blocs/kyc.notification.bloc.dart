import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/kyc/StatusTraderKyc.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';

class KycNotifEvent {}

class KycHideNotification extends KycNotifEvent {}

class KycNotifState {}

class KycNotifUninitialized extends KycNotifState {}

class KycNotifUnauthorized extends KycNotifState {}

class KycNotifLoaded extends KycNotifState {
  StatusResult status;
  bool isCompleteKyc;
  String traderType;
  bool tncStatus;
  bool showNotification;

  KycNotifLoaded({
    this.status,
    this.isCompleteKyc,
    this.traderType,
    this.tncStatus,
    this.showNotification = true,
  });

  KycNotifLoaded copyWith({
    StatusResult status,
    bool isCompleteKyc,
    String traderType,
    bool tncStatus,
    bool showNotification,
  }) {
    return KycNotifLoaded(
      status: status ?? this.status,
      isCompleteKyc: isCompleteKyc ?? this.isCompleteKyc,
      traderType: traderType ?? this.traderType,
      tncStatus: tncStatus ?? this.tncStatus,
      showNotification: showNotification ?? this.showNotification,
    );
  }
}

class KycNotifError extends KycNotifState {
  String error;
  KycNotifError({this.error});

  KycNotifError copyWith({String error}) {
    return KycNotifError(error: error ?? this.error);
  }
}

class KycNotifBloc extends Bloc<KycNotifEvent, KycNotifState> {
  KycNotifBloc(KycNotifState initialState) : super(initialState);
  final storage = FlutterSecureStorage();

  Future<http.Response> checking() async {
    var token = await storage.read(key: 'token');
    var uuid = await storage.read(key: 'uuid');
    if (Helper.check) {
      final http.Response response = await http
          .get('$apiLocal/users/by-uuid/$uuid', headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token'
      }).timeout(Duration(seconds: 15));
      return response;
    } else {
      return null;
    }
  }

  @override
  Stream<KycNotifState> mapEventToState(KycNotifEvent event) async* {
    if (Helper.check) {
      if (event is KycHideNotification) {
        if (state is KycNotifLoaded) {
          var current = state as KycNotifLoaded;
          yield current.copyWith(showNotification: false);
        }
      } else {
        if (state is KycNotifUninitialized) {
          var result = await checking();
          if (result.statusCode == 200) {
            final data = userFromJson(result.body);
            if (data.trader.traderType == "personal") {
              KycPersonalService _api = KycPersonalService();
              var _response = await _api.statusIndividualKyc();
              // logger.i(">> Executing..");
              if (_response != null) {
                var _res = StatusTraderKycHelper.checkingIndividual(
                    StatusKycTrader.fromJson(_response.data));
                yield KycNotifLoaded(
                  isCompleteKyc: true,
                  traderType: data.trader.traderType,
                  status: _res,
                  tncStatus: data.trader.tnc == 1 ? true : false,
                );
              }
            } else if (data.trader.traderType == "company") {
              KycPerusahaanService _api = KycPerusahaanService();
              var _response = await _api.statusKycTrader();
              if (_response != null) {
                var _res = StatusTraderKycHelper.checkingCompany(
                    StatusKycTrader.fromJson(_response.data));
                yield KycNotifLoaded(
                  isCompleteKyc: true,
                  traderType: data.trader.traderType,
                  status: _res,
                  tncStatus: data.trader.tnc == 1 ? true : false,
                );
              }
            } else if (data.trader == null ||
                data.trader.traderType == null ||
                data.trader.traderType == "") {
              yield KycNotifLoaded(
                isCompleteKyc: false,
                traderType: null,
                status: null,
                tncStatus: false,
              );
            } else {
              yield KycNotifLoaded(
                isCompleteKyc: false,
                traderType: null,
                status: null,
                tncStatus: false,
              );
            }
          } else if (result.statusCode == 401) {
            yield KycNotifUnauthorized();
          } else {
            yield KycNotifError(error: "Tidak dapat memuat");
          }
        }
      }
    }
  }
}
