import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';

class KycErrorWrapper extends StatelessWidget {
  final Bloc bloc;
  KycErrorWrapper({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: bloc,
      builder: (context, state) {
        if (state.hasError) {
          return Container(
            margin: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s10),
            child: Text(
              state.error,
              style: TextStyle(color: Colors.red, fontSize: FontSize.s12),
              overflow: TextOverflow.visible,
              textAlign: TextAlign.start,
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
