import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';

class SantaraDropDown<T> extends StatelessWidget {
  final Key key;
  final List<DropdownMenuItem<T>> items;
  final T value;
  final Widget hint;
  final ValueChanged<T> onChanged;
  final bool isExpanded;
  final Widget icon;

  SantaraDropDown(
      {this.key,
      this.icon,
      @required this.items,
      @required this.value,
      this.hint,
      @required this.onChanged,
      this.isExpanded});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;

    return Container(
      height: _size * 10,
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          border: Border.all(width: 2, color: Color(0xffDDDDDD))),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          icon != null
              ? Container(
                  width: _size * 6.25,
                  height: _size * 6.25,
                  margin: EdgeInsets.only(left: _size * 2, right: _size * 2),
                  child: icon)
              : Container(
                  width: _size * 2.5,
                ),
          Expanded(
            flex: 1,
            child: DropdownButton(
              key: key,
              isExpanded: isExpanded,
              underline: Container(height: 0, color: Colors.grey),
              value: value,
              hint: hint,
              items: items,
              onChanged: onChanged,
            ),
          ),
        ],
      ),
    );
  }
}
