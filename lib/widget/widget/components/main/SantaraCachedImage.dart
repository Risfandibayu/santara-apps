import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/sizes.dart';

class SantaraCachedImage extends StatefulWidget {
  final String image;
  final double width;
  final double height;
  final BoxFit fit;

  SantaraCachedImage({
    @required this.image,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
  });
  @override
  _SantaraCachedImageState createState() => _SantaraCachedImageState();
}

class _SantaraCachedImageState extends State<SantaraCachedImage>
    with AutomaticKeepAliveClientMixin {
  dynamic headers;
  @override
  void initState() {
    super.initState();
    UserAgent.headers().then((value) => setState(() => headers = value));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      width: widget.width != null ? widget.width : double.maxFinite,
      height: widget.height != null ? widget.height : double.maxFinite,
      child: widget.image != null && widget.image.isNotEmpty
          ? CachedNetworkImage(
              httpHeaders: headers,
              imageUrl: "${widget.image}",
              placeholder: (context, url) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: ExactAssetImage("assets/Preload.jpeg"),
                    fit: widget.fit,
                  ),
                ),
              ),
              errorWidget: (context, data, _) => Container(
                color: Colors.grey[300],
                child: Center(
                  child: Image.asset(
                    "assets/icon_notif/warning-white.png",
                    width: Sizes.s100,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              imageBuilder: (context, image) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: image,
                    fit: widget.fit,
                  ),
                ),
              ),
            )
          : Container(
              color: Colors.grey[300],
              child: Center(
                child: Image.asset(
                  "assets/icon_notif/warning-white.png",
                  width: Sizes.s100,
                  fit: BoxFit.contain,
                ),
              ),
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
