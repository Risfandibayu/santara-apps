import 'dart:async';
import 'package:flutter/material.dart';

class SantaraCountdownTimer extends StatefulWidget {
  final int timerMaxSeconds;
  final Function onFinish;
  SantaraCountdownTimer({@required this.timerMaxSeconds, this.onFinish});
  @override
  _SantaraCountdownTimerState createState() => _SantaraCountdownTimerState();
}

class _SantaraCountdownTimerState extends State<SantaraCountdownTimer> {
  final interval = const Duration(seconds: 1);
  int currentSeconds = 0;

  String get timerText =>
      '${((widget.timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')} : ${((widget.timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  startTimeout([int milliseconds]) {
    var duration = interval;
    Timer.periodic(duration, (timer) {
      setState(() {
        // print(timer.tick);
        currentSeconds = timer.tick;
        if (timer.tick >= widget.timerMaxSeconds) {
          timer.cancel();
          // run function if ended
          widget.onFinish();
        }
      });
    });
  }

  @override
  void initState() {
    startTimeout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Text(timerText);
  }
}
