import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';

class SantaraBottomSheet {
  static void show(BuildContext context, String title) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            color: Color(ColorRev.mainBlack),
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width / 3 + 20,
                  width: MediaQuery.of(context).size.width / 3 + 20,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          MediaQuery.of(context).size.width / 3),
                      image: DecorationImage(
                          image: AssetImage("assets/icon/forbidden.png"),
                          fit: BoxFit.fitWidth)),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                  child: Center(
                    child: Text(title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Colors.white)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 16),
                  child: FlatButton(
                    key: Key('hideDaftarkanBisnisSheet'),
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }
}
