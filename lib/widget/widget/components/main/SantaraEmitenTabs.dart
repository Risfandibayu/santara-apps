import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'SantaraFinancialStatements.dart';

class SantaraEmitenTabs extends StatefulWidget {
  final Widget stockInfo;
  final Widget stockDetail;
  final Widget about;
  final Widget financial;
  final Widget location;

  SantaraEmitenTabs({
    @required this.stockInfo,
    @required this.stockDetail,
    @required this.about,
    @required this.financial,
    @required this.location,
  });

  @override
  _SantaraEmitenTabsState createState() => _SantaraEmitenTabsState();
}

class _SantaraEmitenTabsState extends State<SantaraEmitenTabs>
    with SingleTickerProviderStateMixin {
  // inisialisasi nama tabs
  final List<Widget> tabs = [
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Informasi Saham",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Detail Saham",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Tentang Penerbit",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Finansial",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Lokasi",
      ),
    ),
  ];
  TabController _tabController; // controller tabs
  int _tabIndex = 0; // current index

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_handleTabChanges);
    super.initState();
  }

  _logEvent(String event) {
    FirebaseAnalytics().logEvent(
      name: event,
      parameters: null,
    );
  }

  _handleTabChanges() {
    if (_tabController.indexIsChanging) {
      switch (_tabController.index) {
        case 1:
          _logEvent('app_select_tab_detail_saham');
          break;
        case 2:
          _logEvent('app_select_profile_perusahaan');
          break;
        case 3:
          _logEvent('app_select_tab_finansial');
          break;
        case 4:
          _logEvent('app_select_tab_lokasi');
          break;
        default:
          break;
      }
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Detail Informasi Saham
        TabBar(
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          controller: _tabController,
          isScrollable: true,
          tabs: tabs,
        ),
        Divider(height: 5),
        Center(
          child: [
            widget.stockInfo,
            widget.stockDetail,
            widget.about,
            widget.financial,
            widget.location,
          ][_tabIndex],
        ),
      ],
    );
  }
}
