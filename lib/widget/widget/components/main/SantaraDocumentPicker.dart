import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'SantaraButtons.dart';

class SantaraDocPicker extends StatelessWidget {
  final String title;
  final String fileName;
  final String fileType;
  final bool isEmpty;
  final VoidCallback onPressed;

  SantaraDocPicker(
      {@required this.title,
      this.fileName,
      this.fileType,
      @required this.isEmpty,
      @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _height = SizeConfig.screenHeight;
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 15),
            child: Text(
              "$title",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.safeBlockHorizontal * 4),
            ),
          ),
          Container(
            height: _height / 4,
            width: double.infinity,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xffB8B8B8)),
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 20,
                ),
                isEmpty
                    ? Icon(
                        Icons.description,
                        size: SizeConfig.safeBlockHorizontal * 10,
                        color: Colors.grey,
                      )
                    : Icon(
                        Icons.picture_as_pdf,
                        size: SizeConfig.safeBlockHorizontal * 10,
                        color: Colors.grey,
                      ),
                Container(height: 10),
                isEmpty
                    ? Container()
                    : Text(
                        "$fileName",
                        style: TextStyle(color: Color(0xff676767)),
                      ),
                Container(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: SantaraPickerButton(
                    title: isEmpty ? "Pilih File" : "Ganti File",
                    onPressed: onPressed,
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Icon(
                Icons.info,
                color: Colors.red,
                size: SizeConfig.safeBlockHorizontal * 3,
              ),
              Container(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: Text(
                    "Tulisan harus berbahasa Indonesia dan terbaca dengan jelas.",
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockHorizontal * 3)),
              )
            ],
          ),
          Container(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Icon(
                Icons.info,
                color: Colors.red,
                size: SizeConfig.safeBlockHorizontal * 3,
              ),
              Container(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: Text("Pastikan file dalam bentuk PDF Maks. 5 MB.",
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockHorizontal * 3)),
              )
            ],
          )
        ],
      ),
    );
  }
}
