import 'package:flutter/material.dart';

import 'SantaraFinancialStatements.dart';

class SantaraFinanceTabs extends StatefulWidget {
  final Widget stockInfo;
  final Widget stockDetail;
  final Widget about;
  final Widget financialStatement;
  final Widget location;

  SantaraFinanceTabs({
    @required this.stockInfo,
    @required this.stockDetail,
    @required this.about,
    @required this.financialStatement,
    @required this.location,
  });

  @override
  _SantaraFinanceTabsState createState() => _SantaraFinanceTabsState();
}

class _SantaraFinanceTabsState extends State<SantaraFinanceTabs>
    with SingleTickerProviderStateMixin {
  // inisialisasi nama tabs
  final List<Widget> tabs = [
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Informasi Saham",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Detail Saham",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Tentang Penerbit",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Finansial",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Lokasi",
      ),
    ),
  ];
  TabController _tabController; // controller tabs
  int _tabIndex = 0; // current index

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_handleTabChanges);
    super.initState();
  }

  _handleTabChanges() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Detail Informasi Penerbit
        TabBar(
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          controller: _tabController,
          isScrollable: true,
          tabs: tabs,
        ),
        Divider(height: 5),
        Center(
          child: [
            widget.stockInfo,
            widget.stockDetail,
            widget.about,
            widget.financialStatement,
            widget.location,
          ][_tabIndex],
        ),
      ],
    );
  }
}
