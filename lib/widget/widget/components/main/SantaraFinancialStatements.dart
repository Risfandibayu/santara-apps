import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';

import 'SantaraCachedImage.dart';

// Tabbar dengan icon info disamping kirine
class IconTabBar extends StatelessWidget {
  final bool isNotified;
  final String title;
  IconTabBar({this.isNotified = false, @required this.title});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        isNotified
            ? Icon(
                Icons.info,
                color: Colors.orange,
                size: FontSize.s20,
              )
            : Container(),
        isNotified ? SizedBox(width: Sizes.s5) : Container(),
        Text(
          "$title",
          style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
              color: Color(ColorRev.mainwhite)),
        )
      ],
    );
  }
}

// Text row
class FinanceTextRow extends StatelessWidget {
  final String title;
  final String value;

  FinanceTextRow({@required this.title, @required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w300,
            ),
          ),
          Spacer(),
          Text(
            "$value",
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}

// Widget jika belum ada laporan keuangan
class SantaraNoFinancialData extends StatelessWidget {
  final String title;
  final String subtitle;
  SantaraNoFinancialData({@required this.title, @required this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(Sizes.s20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/icon/no_financial_statements.png",
            width: Sizes.s200,
          ),
          SizedBox(height: Sizes.s30),
          Text(
            "$title",
            style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
                color: Color(ColorRev.mainwhite)),
          ),
          SizedBox(height: Sizes.s5),
          Text(
            "$subtitle",
            style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w300,
                color: Color(ColorRev.mainwhite)),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

// Widget untuk unduh laporan keuangan
class SantaraFinanceDownloadButton extends StatelessWidget {
  final Function onTap;
  final String title;
  SantaraFinanceDownloadButton({@required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: Sizes.s20,
            height: Sizes.s20,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 2,
                color: Color(0xffBF2D30),
              ),
            ),
            child: Icon(
              Icons.arrow_downward,
              size: Sizes.s14,
              color: Color(0xffBF2D30),
            ),
          ),
          SizedBox(width: Sizes.s5),
          Text(
            "$title",
            style: TextStyle(
              color: Color(0xffBF2D30),
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w700,
            ),
          )
        ],
      ),
    );
  }
}

// Profil Bisnis
// Widget untuk cover image
class SantaraFinanceImage extends StatelessWidget {
  final String image;
  final bool isVideo;
  final Function onTap;

  SantaraFinanceImage({@required this.image, this.isVideo = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: Sizes.s10),
      child: InkWell(
        onTap: onTap,
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(Sizes.s5),
              child: SantaraCachedImage(
                image: "$image",
                width: Sizes.s300,
                height: Sizes.s200,
              ),
            ),
            isVideo
                ? Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[850].withOpacity(0.7),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0))),
                        height: Sizes.s60,
                        width: Sizes.s60,
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                          size: Sizes.s40,
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}

// Business Type (Jenis bisnis di profil bisnis / emiten)
Text businessType(String text) {
  return Text(
    "$text",
    style: TextStyle(
      color: Color(ColorRev.mainwhite),
      fontSize: FontSize.s10,
      fontWeight: FontWeight.w700,
    ),
  );
}

// Business Name (Nama bisnis di profil bisnis / emiten)
Text businessName(String text) {
  return Text(
    "$text",
    style: TextStyle(
      fontSize: FontSize.s20,
      color: Color(ColorRev.mainwhite),
      fontWeight: FontWeight.w700,
    ),
  );
}

Text businessCompanyName(String text) {
  return Text(
    "$text",
    style: TextStyle(
      fontSize: FontSize.s14,
      color: Color(ColorRev.mainwhite),
      fontWeight: FontWeight.w400,
    ),
  );
}
