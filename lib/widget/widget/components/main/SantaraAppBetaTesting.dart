import 'package:flutter/material.dart';
import 'package:santaraapp/utils/api.dart';

class SantaraAppBetaTesting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (isShowOBTInfo) {
      return Container(
        padding: EdgeInsets.fromLTRB(10, 14, 10, 14),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            color: Color(0xFF333333), borderRadius: BorderRadius.circular(6)),
        child: Row(
          children: [
            Icon(
              Icons.warning_rounded,
              color: Color(0xFFFBFF24),
            ),
            Container(width: 10),
            Expanded(
              child: Text(
                "Anda sedang menggunakan aplikasi Beta Testing Santara. Segala transaksi dalam aplikasi ini bersifat simulasi.",
                style: TextStyle(
                    color: Color(0xFFFFCC2F),
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
