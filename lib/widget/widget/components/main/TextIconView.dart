import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/utils/sizes.dart';

class TextIconView extends StatelessWidget {
  final IconData iconData;
  final bool isActive;
  final String text;
  final double iconSize;
  final double textSize;
  final double margin;

  TextIconView(
      {this.iconData,
      this.isActive = false,
      this.text,
      this.iconSize = 0.0,
      this.textSize = 0.0,
      this.margin = 0.0});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Icon(
          iconData,
          color: isActive ? Color(0xffBF2D30) : Colors.white,
          size: FontSize.s15,
        ),
        Container(width: margin != 0.0 ? margin : 5),
        Text(
          text,
          style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        )
      ],
    );
  }
}
