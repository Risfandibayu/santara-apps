import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:shimmer/shimmer.dart';

class BuildScaffoldNoState extends StatelessWidget {
  final Widget content;
  final String title;
  BuildScaffoldNoState({this.content, this.title = ""});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: title.isEmpty
            ? Image.asset(
                'assets/santara/1.png',
                width: 110,
              )
            : Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
        centerTitle: true,
        backgroundColor: Color(ColorRev.mainBlack),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        color: Color(ColorRev.mainBlack),
        width: double.maxFinite,
        height: double.maxFinite,
        child: content,
      ),
    );
  }
}

// Build loading shimmer
Widget buildShimmer(double height, double width) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(Sizes.s10),
    child: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.white,
      child: Container(
        height: height,
        width: width,
        color: Colors.pinkAccent,
        child: Image.asset(
          'arfa.png',
          fit: BoxFit.cover,
        ),
      ),
    ),
  );
}
