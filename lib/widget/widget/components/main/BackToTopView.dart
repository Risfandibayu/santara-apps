import 'package:flutter/material.dart';

class BackToTopView extends StatelessWidget {
  final VoidCallback onTap; // event ketika ditap

  BackToTopView({this.onTap});

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    return InkWell(
      onTap: onTap,
      child: Container(
        height: _height / 10,
        color: Color(0xff184DAF),
        child: Center(
            child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Icon(
              Icons.restore,
              color: Colors.white,
            ),
            Container(
              width: 8,
            ),
            Text(
              "Kembali ke Halaman Paling Atas",
              style: TextStyle(color: Colors.white),
            ),
          ],
        )),
      ),
    );
  }
}
