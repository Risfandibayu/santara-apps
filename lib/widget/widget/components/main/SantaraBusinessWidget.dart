import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/models/UserBusinessModel.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/registrasi/PreRegistrasiUI.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/pages/pre_screening_page.dart';

import 'SantaraButtons.dart';
import 'SantaraCachedImage.dart';

// Status bisnis, Pendanaan gagal terpenuhi, dalam proses pendanaan, belum membuat laporan keuangan
enum BusinessStatus { inProgress, noFunding, noReport, haveReport }

// warna untuk card status bisnis
final Color statusBlue = Color(0xffEEF1FF);
final Color statusRed = Color(0xffFFE2DF);
final Color statusOrange = Color(0xffFDE9CD);

// Progress indikator pendanaan
class SantaraBusinessProgressIndicator extends StatelessWidget {
  final EdgeInsetsGeometry margin;
  final double percent;
  final double lineHeight;
  final Widget text;
  final Widget caption;

  SantaraBusinessProgressIndicator(
      {this.margin,
      this.percent = 0.0,
      this.lineHeight = 15,
      @required this.text,
      this.caption});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: margin,
          child: LinearPercentIndicator(
            alignment: MainAxisAlignment.center,
            padding: EdgeInsets.zero,
            animation: true,
            animationDuration: 2000,
            lineHeight: lineHeight,
            percent: percent,
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: Color(ColorRev.mainRed),
            center: text,
          ),
        ),
        // Business progress message
        caption != null ? caption : Container()
        // Text(
        //   "$caption",
        //   style: TextStyle(
        //     fontSize: FontSize.s10,
        //     fontWeight: FontWeight.w600,
        //   ),
        // ),
      ],
    );
  }
}

// status bisnis (ditolak atau diterima)
class SantaraBusinessStatus extends StatelessWidget {
  final BusinessStatus status;
  final String businessName;
  final String businessUrl;
  final String title;
  final String subtitle;
  final Function onTap;

  SantaraBusinessStatus({
    @required this.status,
    this.title = "",
    this.subtitle = "",
    this.businessName = "",
    this.businessUrl = "",
    this.onTap,
  });

  // Message status berdasarkan status bisnisnya
  // Jika status diterima
  Widget _buildOnProgress() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title.isNotEmpty
              ? title
              : "Bisnis Anda sedang dalam proses pendanaan",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: Sizes.s5),
        RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Nunito',
              fontSize: 11,
            ),
            children: <TextSpan>[
              TextSpan(
                text: subtitle.isNotEmpty
                    ? subtitle
                    : "Bagikan bisnis ini ke media sosial agar pendanaan dapat cepat terpenuhi. ",
              ),
              // TextSpan(
              //   text: "Bagikan",
              //   style: TextStyle(
              //     fontWeight: FontWeight.bold,
              //     decoration: TextDecoration.underline,
              //     decorationStyle: TextDecorationStyle.solid,
              //   ),
              //   recognizer: TapGestureRecognizer()
              //     ..onTap = () => Share.share(
              //           'Lihat $businessName di $businessUrl',
              //         ),
              // ),
            ],
          ),
        ),
      ],
    );
  }

  // Jika Status ditolak
  Widget _buildRejected() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title.isNotEmpty ? title : "Pendanaan bisnis Anda gagal terpenuhi",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: Sizes.s5),
        Text(
          subtitle.isNotEmpty
              ? subtitle
              : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisi dictum nibh lorem sit vitae.",
          style: TextStyle(
            fontSize: FontSize.s11,
            fontWeight: FontWeight.w400,
          ),
        )
      ],
    );
  }

  // Jika belum membuat laporan keuangan
  Widget _buildNoReport() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title.isNotEmpty
              ? title
              : "Anda belum membuat laporan keuangan untuk bulan ini",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: Sizes.s5),
        Text(
          subtitle.isNotEmpty
              ? subtitle
              : "Segera buat laporan keuangan Anda sebelum tanggal 30 Agustus 2020.",
          style: TextStyle(
            fontSize: FontSize.s11,
            fontWeight: FontWeight.w400,
          ),
        )
      ],
    );
  }

  // Warna berdasarkan status
  Color _buildColor() {
    switch (status) {
      case BusinessStatus.inProgress:
        return statusBlue;
        break;
      case BusinessStatus.noFunding:
        return statusRed;
        break;
      case BusinessStatus.noReport:
        return statusOrange;
        break;
      default:
        return Colors.grey[200];
        break;
    }
  }

  Widget _buildMessage() {
    switch (status) {
      case BusinessStatus.inProgress:
        return _buildOnProgress();
        break;
      case BusinessStatus.noFunding:
        return _buildRejected();
        break;
      case BusinessStatus.noReport:
        return _buildNoReport();
        break;
      default:
        return Container();
        break;
    }
  }

  // build widget starts here
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: Sizes.s80,
      width: double.maxFinite,
      margin: EdgeInsets.only(top: Sizes.s15),
      padding: EdgeInsets.fromLTRB(
        Sizes.s15,
        Sizes.s10,
        Sizes.s15,
        Sizes.s10,
      ),
      decoration: BoxDecoration(
        color: _buildColor(),
        borderRadius: BorderRadius.circular(Sizes.s5),
      ),
      child: _buildMessage(),
    );
  }
}

// financial statements highlight
class SantaraFinancialStatements extends StatelessWidget {
  final LastReport report;
  SantaraFinancialStatements({@required this.report});
  final rupiah = NumberFormat("#,##0");
  final DateFormat dateFormat = DateFormat("dd/MM/yyyy", "id");

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: Sizes.s5),
        Divider(),
        Text(
          "Laporan Keuangan Terakhir",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: Sizes.s10),
        TextValueEmiten(
          title: "Tanggal",
          value: report?.date != null
              ? dateFormat.format(DateTime.parse(report?.date))
              : "-",
        ),
        TextValueEmiten(
          title: "Omset Terakhir",
          value: "Rp. ${rupiah.format(report?.salesRevenue ?? 0)}",
        ),
        TextValueEmiten(
          title: "Profit Terakhir",
          value: "Rp. ${rupiah.format(report?.netProfit ?? 0)}",
        ),
      ],
    );
  }
}

// Main Card Bisnis
class SantaraBusinessCard extends StatelessWidget {
  final UserBusinessModel data; // data bisnis
  final Function onTap; // on tap
  SantaraBusinessCard({@required this.data, this.onTap});
  final DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  Widget buildProgress() {
    return SantaraBusinessProgressIndicator(
      margin: EdgeInsets.fromLTRB(
        Sizes.s8,
        Sizes.s15,
        Sizes.s8,
        Sizes.s5,
      ),
      percent: data.percent,
      lineHeight: Sizes.s15,
      caption: Text(
        "Sisa Waktu : ${data?.dayRemaining ?? '-'} Hari - ${data?.totalInvestor ?? '-'} Investor",
        style: TextStyle(
          fontSize: FontSize.s12,
          fontWeight: FontWeight.w600,
        ),
      ),
      text: Text(
        "${data.percent * 100} %",
        style: TextStyle(
          color: Colors.white,
          fontSize: FontSize.s10,
        ),
      ),
    );
  }

  // Untuk mendapatkan status emiten verified
  // Jika statusnya PENYERAHAN DANA, PENDANAAN TERPENUHI, PEMBAGIAN DIVIDEN
  String statusEmiten(String status) {
    if (status == "PENYERAHAN DANA" ||
        status == "PENDANAAN TERPENUHI" ||
        status == "PEMBAGIAN DIVIDEN") {
      return "verified";
    } else {
      return status;
    }
  }

  Widget buildStatus(String status) {
    switch (status) {
      case "verified":
        return SantaraFinancialStatements(
          report: data.lastReport,
        );
        break;
      case "empty":
        return SantaraBusinessStatus(
          title: "Anda belum membuat laporan keuangan untuk bulan ini",
          subtitle:
              "Segera buat laporan keuangan Anda sebelum tanggal ${dateFormat.format(DateFormat("DD-MM-yyyy").parse(data?.reportDeadline ?? '0000-00-00'))}.",
          status: BusinessStatus.noReport,
        );
        break;
      case "PENAWARAN SAHAM":
        return Column(
          children: [
            buildProgress(),
            SantaraBusinessStatus(
              status: BusinessStatus.inProgress,
              businessName: data.trademark,
              businessUrl: data.url,
            ),
          ],
        );
        break;
      case "PENDANAAN TIDAK TERPENUHI":
        return Column(
          children: [
            buildProgress(),
            SantaraBusinessStatus(status: BusinessStatus.noFunding),
          ],
        );
        break;
      default:
        return Container();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    // device screen width
    var width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(Sizes.s15),
        margin: EdgeInsets.all(Sizes.s20),
        // height: Sizes.s250,
        decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Color(0xffDADADA),
            ),
            borderRadius: BorderRadius.circular(Sizes.s5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Business detail section
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Sizes.s5),
                    topRight: Radius.circular(Sizes.s5),
                  ),
                  child: SantaraCachedImage(
                    image: "$urlAsset/token/${data.picture}",
                    width: Sizes.s80,
                    height: Sizes.s70,
                  ),
                ),
                SizedBox(width: Sizes.s10),
                Container(
                  // set width untuk keterangan bisnis
                  // handling ketika nama bisnis memiliki panjang > 1 line
                  width: width - Sizes.s165,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${data.category}",
                        style: TextStyle(
                          color: Color(0xff292F8D),
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                      ),
                      Text(
                        "${data.trademark}",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 2,
                      ),
                      Text(
                        "${data.companyName}",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s11,
                        ),
                        maxLines: 1,
                      ),
                    ],
                  ),
                )
              ],
            ),
            // Business Status
            // Jika memiliki report deadline, otomatis statusnya menjadi empty
            buildStatus((data?.reportDeadline ?? "").isNotEmpty
                ? "empty"
                : statusEmiten(data.status))
          ],
        ),
      ),
    );
  }
}

// Text Value
class TextValue extends StatelessWidget {
  final String title;
  final String value;
  final bool isBold;

  TextValue({
    @required this.title,
    @required this.value,
    this.isBold = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s5),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              "$title",
              style: TextStyle(
                fontWeight: isBold ? FontWeight.w700 : FontWeight.w400,
                fontSize: FontSize.s11,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              ":",
              style: TextStyle(
                fontWeight: isBold ? FontWeight.w700 : FontWeight.w400,
                fontSize: FontSize.s11,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              "$value",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: FontSize.s11,
              ),
            ),
          )
        ],
      ),
    );
  }
}

// Text Value Emiten
class TextValueEmiten extends StatelessWidget {
  final String title;
  final String value;
  final bool isBold;

  TextValueEmiten({
    @required this.title,
    @required this.value,
    this.isBold = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s5),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              "$title",
              style: TextStyle(
                fontWeight: isBold ? FontWeight.w700 : FontWeight.w400,
                fontSize: FontSize.s11,
                color: Color(ColorRev.mainwhite),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              ":  $value",
              style: TextStyle(
                fontWeight: isBold ? FontWeight.w700 : FontWeight.w400,
                fontSize: FontSize.s11,
                color: Color(ColorRev.mainwhite),
              ),
            ),
          )
        ],
      ),
    );
  }
}

// Notifikasi jika ada bisnis yang sedang diajukan
class SubmittedBusinessNotification extends StatelessWidget {
  final String message;
  final Function onTap;
  SubmittedBusinessNotification({@required this.message, @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          leading: Container(
            width: Sizes.s35,
            height: Sizes.s35,
            child: Image.asset(
              "assets/icon_bottom/pralisting_unselected.png",
            ),
          ),
          title: Text(
            "$message",
            style: TextStyle(
              fontSize: FontSize.s14,
              color: Color(ColorRev.mainwhite),
            ),
          ),
          subtitle: InkWell(
            onTap: onTap,
            child: Text(
              "Lihat Bisnis",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
                color: Color(0xff198197),
                decoration: TextDecoration.underline,
                decorationStyle: TextDecorationStyle.solid,
              ),
            ),
          ),
        ),
        Container(
          height: 2,
          color: Color(0xffDDDDDD),
          width: double.maxFinite,
        )
      ],
    );
  }
}

// Widget jika belum ada bisnis yang didaftarkan
class SantaraNoBusinessAvailable extends StatelessWidget {
  final int pralisting;
  final VoidCallback callback;
  SantaraNoBusinessAvailable({
    this.pralisting = 0,
    this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(Sizes.s20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/icon/no_business.png",
            width: Sizes.s250,
          ),
          SizedBox(height: Sizes.s30),
          Text(
            "Belum Ada Bisnis yang Didanai",
            style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
                color: Color(ColorRev.mainwhite)),
          ),
          SizedBox(height: Sizes.s5),
          Text(
            "Anda belum memiliki bisnis yang siap untuk didanai",
            style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w300,
                color: Color(ColorRev.mainwhite)),
          ),
          pralisting < 1
              ? Container(
                  margin: EdgeInsets.only(top: Sizes.s20),
                  child: SantaraMainButton(
                    title: "Daftarkan Bisnis Anda",
                    onPressed: () async {
                      final result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PreScreeningPage(),
                        ),
                      );
                      callback();
                    },
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}

// Button beli saham
class SantaraBuyStockButton extends StatelessWidget {
  final String title;
  final Function onTap;
  final Key key;
  SantaraBuyStockButton({@required this.title, @required this.onTap, this.key});
  @override
  Widget build(BuildContext context) {
    return Container(
      key: key,
      padding: EdgeInsets.fromLTRB(
        Sizes.s20,
        Sizes.s10,
        Sizes.s20,
        Sizes.s10,
      ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 1,
            color: Color(ColorRev.mainwhite),
          ),
        ),
      ),
      height: Sizes.s70,
      child: SantaraMainButton(
        title: "$title",
        onPressed: onTap,
      ),
    );
  }
}

// Status Untuk Tab Laporan Keuangan
// Jika blm buat laporan keuangan
class SantaraFinancialStatementsStatus extends StatelessWidget {
  final Color color;
  final String title;
  final String subtitle;
  SantaraFinancialStatementsStatus(
      {@required this.color, this.title, this.subtitle = ""});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: Sizes.s80,
      width: double.maxFinite,
      margin: EdgeInsets.only(top: Sizes.s15),
      padding: EdgeInsets.fromLTRB(
        Sizes.s15,
        Sizes.s10,
        Sizes.s15,
        Sizes.s10,
      ),
      decoration: BoxDecoration(
        color: Color(ColorRev.mainRed),
        borderRadius: BorderRadius.circular(Sizes.s5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.bold,
                color: Colors.white ?? Colors.white),
          ),
          subtitle.length > 0 ? SizedBox(height: Sizes.s5) : Container(),
          subtitle.length > 0
              ? Text(
                  "$subtitle",
                  style: TextStyle(
                      fontSize: FontSize.s11,
                      fontWeight: FontWeight.w400,
                      color: Colors.white ?? Colors.white),
                )
              : Container()
        ],
      ),
    );
  }
}
