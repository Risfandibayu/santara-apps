import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/utils/sizes.dart';

class SantaraMainButton extends StatelessWidget {
  final Key key;
  final bool isActive;
  final VoidCallback onPressed;
  final Color color;
  final Color textColor;
  final Color disableColor;
  final String title;

  const SantaraMainButton({
    this.key,
    this.isActive = true,
    this.title,
    this.color,
    this.textColor,
    this.disableColor,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: Sizes.s45,
      // margin: EdgeInsets.all(20),
      width: double.infinity,
      child: FlatButton(
        key: key,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Text(
          title,
          style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        color: color ?? Color(0xffBF2D30),
        textColor: textColor ?? Colors.white,
        disabledColor: disableColor ?? Colors.transparent,
        onPressed: isActive ? onPressed : null,
      ),
    );
  }
}

class SantaraPickerButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;

  SantaraPickerButton({this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      // margin: EdgeInsets.all(20),
      width: double.infinity,
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Text(
          title,
          style: TextStyle(
              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
              fontWeight: FontWeight.bold),
        ),
        color: Color(0xff666EE8),
        textColor: Colors.white,
        onPressed: onPressed,
      ),
    );
  }
}

class SantaraOutlineButton extends StatelessWidget {
  final Key key;
  final VoidCallback onPressed;
  final String title;
  final Color color;

  SantaraOutlineButton({this.key, this.title, this.onPressed, this.color});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      key: key,
      // margin: EdgeInsets.all(20),
      width: double.infinity,
      height: Sizes.s45,
      child: OutlineButton(
        borderSide: BorderSide(color: color ?? Color(0xffBF2D30), width: 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(
            color: color ?? Color(0xffBF2D30),
            width: 1,
          ),
        ),
        child: Text(
          title,
          style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.bold,
              color: color ?? Colors.white),
        ),
        color: Color(0xff666EE8),
        textColor: Colors.white,
        onPressed: onPressed,
      ),
    );
  }
}

class SantaraDisabledButton extends StatelessWidget {
  final String title;
  SantaraDisabledButton({this.title = "Sedang Dalam Proses Verifikasi"});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: Sizes.s5,
        right: Sizes.s5,
        bottom: Sizes.s15,
      ),
      height: Sizes.s40,
      child: OutlineButton(
        onPressed: null,
        child: Text(title),
      ),
    );
  }
}
