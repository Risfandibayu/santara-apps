import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/widget/listing/dashboard/PengaturanBisnisUI.dart';

class SantaraAppBar extends StatelessWidget {
  final bool isEdit;
  final Widget leading;
  final List<Widget> actions;
  SantaraAppBar({this.leading, this.isEdit = false, this.actions});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AppBar(
      leading: leading != null
          ? leading
          : IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
      title: Image.asset('assets/santara/1.png', width: 110.0),
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.white),
      backgroundColor: Colors.black,
      actions: actions != null
          ? actions
          : <Widget>[
              isEdit
                  ? InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PengaturanBisnisUI(
                                      emitenDetail: null,
                                    )));
                      },
                      child: Container(
                        width: 80,
                        height: 40,
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Edit",
                            style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                color: Color(0xff218196)),
                          ),
                        ),
                      ),
                    )
                  : Container()
            ],
    );
  }
}
