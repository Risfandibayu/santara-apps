import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';

import 'SantaraButtons.dart';

class SantaraAlertDialog extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onYakin;
  final VoidCallback onBatal;
  final double height;
  final String textCancel;
  final String textConfirm;

  SantaraAlertDialog({
    @required this.title,
    @required this.description,
    @required this.onYakin,
    @required this.onBatal,
    this.height = 150,
    this.textCancel = "Batal",
    this.textConfirm = "Yakin",
  });
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // set up the AlertDialog
    return AlertDialog(
      backgroundColor: Color(ColorRev.maingrey),
      // contentPadding: EdgeInsets.all(10),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      content: Container(
        // height: height,
        width: double.infinity,
        child: Column(
          // shrinkWrap: true,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "$title",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s14,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              height: SizeConfig.safeBlockHorizontal * 2.5,
            ),
            Text(
              "$description",
              style: TextStyle(
                color: Color(ColorRev.mainwhite),
                fontSize: FontSize.s12,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              height: SizeConfig.safeBlockHorizontal * 5.5,
            ),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: Sizes.s35,
                      child: SantaraOutlineButton(
                        title: "${textCancel ?? 'Batal'}",
                        onPressed: onBatal,
                      ),
                    ),
                  ),
                  Container(
                    width: Sizes.s15,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: Sizes.s35,
                      child: SantaraMainButton(
                        title: "${textConfirm ?? 'Yakin'}",
                        onPressed: onYakin,
                      ),
                    ),
                  )
                ])
          ],
        ),
      ),
    );
  }
}
