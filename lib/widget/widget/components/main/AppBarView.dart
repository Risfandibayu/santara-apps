import 'package:flutter/material.dart';

class OnSearchAppBar extends StatelessWidget with PreferredSizeWidget {
  final TextEditingController
      searchController; // controller textfield di appbar
  final String hint; // hint text textfield di appbar
  final FocusNode focusNode; // focus node field
  @required
  final ValueChanged<String> onSubmitted; // event onsubmit textfield
  @required
  final Widget bottom; // untuk bottom widget
  @required
  final VoidCallback onCloseTextfield; // event ketika button close di appbar
  final VoidCallback onTouchSearch; // event ketika button search ditekan
  OnSearchAppBar(
      {this.searchController,
      this.focusNode,
      this.hint = "Contoh Martabak",
      this.onSubmitted,
      this.onCloseTextfield,
      this.onTouchSearch,
      this.bottom});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: Padding(
          padding: const EdgeInsets.only(right: 8),
          child: TextField(
            focusNode: focusNode,
            controller: searchController,
            onSubmitted: onSubmitted,
            decoration: InputDecoration(
                hintText: hint,
                hintStyle: TextStyle(color: Color(0xff676767)),
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                fillColor: Color(0xffF2F2F2),
                border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                    borderSide: BorderSide(color: Color(0xffDDDDDD))),
                suffixIcon: IconButton(
                    padding: EdgeInsets.only(right: 16),
                    color: Colors.black,
                    icon: Icon(Icons.search),
                    onPressed: onTouchSearch)),
          ),
        ),
        backgroundColor: Color(0xffF2F2F2),
        leading: IconButton(
          icon: Icon(Icons.close),
          color: Colors.black,
          onPressed: onCloseTextfield,
        ),
        bottom: bottom);
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class ListingAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title; // judul appbar
  final VoidCallback onTapSearch; // event ketika button search ditekan
  final PreferredSizeWidget bottom; // widget dibawah appbar

  ListingAppBar({this.title, this.onTapSearch, this.bottom});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "$title",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        bottom: bottom,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
            ),
            onPressed: onTapSearch,
          )
        ]);
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
