import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/blocs/regency.search.bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycErrorWrapper.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class SantaraTextField extends StatelessWidget {
  final Key key;
  final double height;
  final int lines;
  final String hintText;
  final String labelText;
  final String errorText;
  final String initialValue;
  final String prefixText;
  final Widget icon;
  final bool enabled;
  final bool obscureText;
  final ValueChanged<String> onChanged;
  final TextInputType inputType;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final TextEditingController controller;

  SantaraTextField({
    this.key,
    this.height = 50,
    this.lines = 1,
    @required this.hintText,
    @required this.labelText,
    this.enabled = true,
    this.obscureText = false,
    this.prefixText,
    this.icon,
    this.initialValue,
    this.errorText,
    this.onChanged,
    this.validator,
    this.onSaved,
    this.inputType,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return TextFormField(
        style: TextStyle(color: Colors.white),
        key: key,
        controller: controller,
        obscureText: obscureText,
        initialValue: initialValue,
        validator: validator,
        enabled: enabled,
        decoration: InputDecoration(
          icon: icon,
          errorMaxLines: 5,
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 2)),
          hintText: hintText,
          labelText: labelText,
          prefixText: prefixText,
          filled: true,
          fillColor: enabled ? Colors.transparent : Colors.white,
          hintStyle: TextStyle(fontSize: _size * 3.5, color: Colors.white),
          labelStyle: TextStyle(fontSize: _size * 3.5, color: Colors.white),
        ),
        keyboardType: inputType ?? TextInputType.text,
        maxLines: lines,
        onChanged: onChanged,
        onSaved: onSaved);
  }
}

class SantaraFileField extends StatelessWidget {
  final Key key;
  final String title;
  final String filename;
  final VoidCallback onTap;

  SantaraFileField({
    this.key,
    this.title = "",
    @required this.filename,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        title.isNotEmpty
            ? Text(
                "$title",
                style: TextStyle(
                    fontSize: SizeConfig.safeBlockHorizontal * 3,
                    fontWeight: FontWeight.w600),
              )
            : Container(),
        title.isNotEmpty
            ? Container(
                height: SizeConfig.safeBlockHorizontal * 3.75,
              )
            : Container(),
        Container(
          height: SizeConfig.safeBlockHorizontal * 12,
          padding: EdgeInsets.only(left: 10),
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border.all(color: Color(0xffB8B8B8)),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Text(
                  "$filename",
                  overflow: TextOverflow.ellipsis,
                  style:
                      TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 3),
                ),
              ),
              Container(
                width: 10,
              ),
              Expanded(
                  flex: 1,
                  child: InkWell(
                    key: key,
                    onTap: onTap,
                    child: Container(
                      height: SizeConfig.safeBlockHorizontal * 12,
                      width: SizeConfig.safeBlockHorizontal * 20,
                      decoration: BoxDecoration(
                          color: Color(0xff6870E1),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Center(
                        child: Text(
                          "Pilih File",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: SizeConfig.safeBlockHorizontal * 3.25),
                        ),
                      ),
                    ),
                  ))
            ],
          ),
        ),
      ],
    );
  }
}

class SantaraCashInput extends StatelessWidget {
  final Key key;
  final int maxDigits;
  final String hintText;
  final String labelText;
  final String errorText;
  final Widget suffixIcon;
  final VoidCallback onEditingComplete;
  final ValueChanged<String> onChanged;
  final String initialValue;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final bool enabled;

  SantaraCashInput({
    this.key,
    @required this.maxDigits,
    @required this.hintText,
    @required this.labelText,
    this.errorText,
    this.suffixIcon,
    this.onEditingComplete,
    this.onChanged,
    this.initialValue,
    this.onSaved,
    this.validator,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;

    return TextFormField(
      key: key,
      enabled: enabled,
      initialValue: initialValue,
      validator: validator,
      maxLines: 1,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          contentPadding:
              EdgeInsets.symmetric(horizontal: _size * 4, vertical: _size * 4),
          hintText: "$hintText",
          labelText: "$labelText",
          labelStyle: TextStyle(fontSize: _size * 3.5),
          hintStyle: TextStyle(fontSize: _size * 3.5),
          errorText: errorText,
          errorMaxLines: 5,
          border: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: Color(0xffDDDDDD)),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          suffixIcon: suffixIcon != null ? suffixIcon : null),
      inputFormatters: [
        WhitelistingTextInputFormatter.digitsOnly,
        RupiahFormatter(maxDigits: maxDigits),
      ],
      onEditingComplete: onEditingComplete,
      onChanged: onChanged,
      onSaved: onSaved,
    );
  }
}

class SantaraBasicField extends StatelessWidget {
  final String hintText;
  final String labelText;
  final String errorText;
  final bool enabled;
  final bool obscureText;
  final ValueChanged<String> onChanged;
  final TextInputType inputType;
  final TextEditingController controller;
  final Widget suffixIcon;

  SantaraBasicField({
    @required this.hintText,
    @required this.labelText,
    this.enabled = true,
    this.obscureText = false,
    this.errorText,
    this.onChanged,
    this.inputType,
    this.controller,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.grey),
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        hintText: "$hintText",
        errorText: errorText,
        labelText: "$labelText",
        enabled: enabled,
        errorMaxLines: 5,
        suffixIcon: suffixIcon ?? Container(),
      ),
      obscureText: obscureText,
      keyboardType: inputType,
      onChanged: onChanged,
    );
  }
}

class SantaraBlocTextField extends StatelessWidget {
  final Key key;
  final TextFieldBloc<Object> textFieldBloc;
  final String hintText;
  final String labelText;
  final String errorText;
  final bool enabled;
  final bool obscureText;
  final bool isPhone;
  final TextInputType inputType;
  final List<TextInputFormatter> inputFormatters;
  final Widget suffix;
  final Widget suffixIcon;
  final Function onEditingComplete;
  final Function(String) onChanged;
  final Function onTapIfDisabled;
  final FloatingLabelBehavior floatingLabelBehavior;

  SantaraBlocTextField({
    this.key,
    @required this.textFieldBloc,
    @required this.hintText,
    @required this.labelText,
    this.errorText,
    this.enabled = true,
    this.obscureText = false,
    this.isPhone = false,
    this.inputType = TextInputType.text,
    this.inputFormatters,
    this.suffix,
    this.suffixIcon,
    this.onEditingComplete,
    this.onChanged,
    this.onTapIfDisabled,
    this.floatingLabelBehavior = FloatingLabelBehavior.always,
  });

  Widget textField() {
    return isPhone == true
        ? Container(
            child: Row(
              children: [
                Container(
                  height: 62,
                  decoration: BoxDecoration(
                      color: Color(0xffF4F4F4),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                      ),
                      border: Border.all(width: 0.5, color: Colors.grey)),
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Text('+ 62', style: TextStyle(color: Colors.grey)),
                  )),
                ),
                Flexible(
                  child: textInput(),
                ),
              ],
            ),
          )
        : textInput();
  }

  Widget textInput() {
    return TextFieldBlocBuilder(
      key: key,
      textFieldBloc: textFieldBloc,
      enableSuggestions: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.grey),
          borderRadius: isPhone == true
              ? BorderRadius.only(
                  topRight: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                )
              : BorderRadius.all(
                  Radius.circular(5),
                ),
        ),
        errorMaxLines: 5,
        hintText: "$hintText",
        hintStyle: TextStyle(color: Colors.grey),
        errorText: errorText,
        labelText: "$labelText",
        floatingLabelBehavior: floatingLabelBehavior,
        suffix: suffix,
        suffixIcon: suffixIcon,
        filled: !enabled,
        fillColor: Color(0xffF4F4F4),
        alignLabelWithHint: true,
      ),
      obscureText: obscureText,
      keyboardType: inputType,
      inputFormatters: inputFormatters,
      onEditingComplete: onEditingComplete,
      onChanged: onChanged,
      isEnabled: enabled,
    );
  }

  @override
  Widget build(BuildContext context) {
    return enabled
        ? textField()
        : InkWell(
            onTap: onTapIfDisabled,
            child: textField(),
          );
  }
}

class SantaraBlocDropDown extends StatelessWidget {
  final SelectFieldBloc<SubDataBiodata, dynamic> selectFieldBloc;
  final String label;
  final String hintText;
  final bool enabled;
  final Widget suffixIcon;
  final Function onTapIfDisabled;
  final FloatingLabelBehavior floatingLabelBehavior;

  SantaraBlocDropDown({
    @required this.selectFieldBloc,
    @required this.label,
    this.enabled = true,
    this.hintText,
    this.suffixIcon,
    this.onTapIfDisabled,
    this.floatingLabelBehavior = FloatingLabelBehavior.always,
  });

  Widget dropDown() {
    return DropdownFieldBlocBuilder<SubDataBiodata>(
      selectFieldBloc: selectFieldBloc,
      isEnabled: enabled,
      decoration: InputDecoration(
        errorMaxLines: 5,
        border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        labelText: "$label",
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.grey),
        suffixIcon: suffixIcon,
        filled: !enabled,
        fillColor: Color(0xffF4F4F4),
        floatingLabelBehavior: floatingLabelBehavior,
      ),
      itemBuilder: (context, value) => value.name,
    );
  }

  @override
  Widget build(BuildContext context) {
    return enabled
        ? dropDown()
        : InkWell(
            onTap: onTapIfDisabled,
            child: dropDown(),
          );
  }
}

class SantaraBlocRadioGroup extends StatelessWidget {
  final SelectFieldBloc<SubDataBiodata, dynamic> selectFieldBloc;
  final bool enabled;

  SantaraBlocRadioGroup({
    @required this.selectFieldBloc,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: RadioButtonGroupFieldBlocBuilder<SubDataBiodata>(
        decoration: InputDecoration(border: InputBorder.none, errorMaxLines: 5),
        selectFieldBloc: selectFieldBloc,
        itemBuilder: (context, item) => item.name,
        padding: EdgeInsets.all(0),
        isEnabled: enabled,
      ),
    );
  }
}

class AddressBlocDropdown extends StatelessWidget {
  final Bloc bloc;

  AddressBlocDropdown({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: bloc,
      builder: (context, state) {
        if (state.hasError) {
          return Text(
            state.error,
            style: TextStyle(color: Colors.red),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class SantaraPdfBlocField extends StatelessWidget {
  final String status;
  final String title;
  final InputFieldBloc<File, dynamic> selectFieldBloc;
  final VoidCallback onTap;

  SantaraPdfBlocField({
    this.title = "",
    @required this.status,
    @required this.selectFieldBloc,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return BlocBuilder<InputFieldBloc, InputFieldBlocState>(
      bloc: selectFieldBloc,
      builder: (context, state) {
        File file = state.value;
        final String fileName = file != null ? path.basename(file.path) : "";
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            title.isNotEmpty
                ? Text(
                    "$title",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.safeBlockHorizontal * 3,
                        fontWeight: FontWeight.w600),
                  )
                : Container(),
            title.isNotEmpty
                ? Container(
                    height: SizeConfig.safeBlockHorizontal * 3.75,
                  )
                : Container(),
            Container(
              height: SizeConfig.safeBlockHorizontal * 12,
              padding: EdgeInsets.only(left: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xffB8B8B8)),
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              child: state.extraData == null
                  ? Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            file == null ? "" : "$fileName",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3),
                          ),
                        ),
                        Container(
                          width: 10,
                        ),
                        Expanded(
                            flex: 1,
                            child: InkWell(
                              onTap: onTap,
                              child: Container(
                                height: SizeConfig.safeBlockHorizontal * 12,
                                width: SizeConfig.safeBlockHorizontal * 20,
                                decoration: BoxDecoration(
                                    color: Color(0xff6870E1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Center(
                                  child: Text(
                                    file == null ? "Unggah PDF" : "Ubah PDF",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                3.25),
                                  ),
                                ),
                              ),
                            ))
                      ],
                    )
                  : Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            state.extraData["hasDocument"]
                                ? state.extraData["url"]
                                : "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3),
                          ),
                        ),
                        Container(
                          width: 10,
                        ),
                        Expanded(
                            flex: 1,
                            child: InkWell(
                              onTap: onTap,
                              child: Container(
                                height: SizeConfig.safeBlockHorizontal * 12,
                                width: SizeConfig.safeBlockHorizontal * 20,
                                decoration: BoxDecoration(
                                    color: Color(0xff6870E1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Center(
                                  child: Text(
                                    state.extraData["hasDocument"]
                                        ? "Ubah PDF"
                                        : "Unggah PDF",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal *
                                                3.25),
                                  ),
                                ),
                              ),
                            ))
                      ],
                    ),
            ),
            KycErrorWrapper(bloc: selectFieldBloc)
          ],
        );
      },
    );
  }
}

class SantaraRevenueField extends StatelessWidget {
  final SingleFieldBloc bloc;
  final String hintText;
  final String labelText;
  final bool enabled;
  final Widget suffix;
  final Widget suffixIcon;
  final Function onEditingComplete;
  final Function(String) onChanged;
  final Function onTapIfDisabled;
  final FloatingLabelBehavior floatingLabelBehavior;

  SantaraRevenueField({
    @required this.bloc,
    this.hintText = '',
    this.labelText = '',
    this.enabled = true,
    this.suffix,
    this.suffixIcon,
    this.onEditingComplete,
    this.onChanged,
    this.onTapIfDisabled,
    this.floatingLabelBehavior = FloatingLabelBehavior.always,
  });

  Widget textField() {
    return BlocBuilder<SingleFieldBloc, FieldBlocState>(
      bloc: bloc,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocBuilder<SingleFieldBloc, FieldBlocState>(
              bloc: bloc,
              builder: (context, state) {
                return TextFormField(
                  initialValue: state.value,
                  style: TextStyle(color: enabled ? Colors.black : Colors.grey),
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                    RupiahFormatter(maxDigits: 12),
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    hintText: "$hintText",
                    hintStyle: TextStyle(color: Colors.grey),
                    labelText: "$labelText",
                    suffix: suffix,
                    suffixIcon: suffixIcon,
                    filled: !enabled,
                    fillColor: Color(0xffF4F4F4),
                  ),
                  onEditingComplete: onEditingComplete,
                  onChanged: onChanged,
                  enabled: enabled,
                );
              },
            ),
            SizedBox(height: Sizes.s10),
            state.hasError
                ? Text(
                    "${state.error}",
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: FontSize.s12,
                    ),
                  )
                : Container()
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return enabled
        ? textField()
        : InkWell(
            onTap: onTapIfDisabled,
            child: textField(),
          );
  }
}

class SearchRegencyField extends StatefulWidget {
  final SingleFieldBloc regencyBloc;
  final String selectedRegency;
  final String label;
  final String hintText;
  final Function onTapIfDisabled;
  final bool enabled;

  SearchRegencyField({
    @required this.regencyBloc,
    this.selectedRegency,
    @required this.label,
    @required this.hintText,
    this.enabled = true,
    this.onTapIfDisabled,
  });

  @override
  _SearchRegencyFieldState createState() => _SearchRegencyFieldState();
}

class _SearchRegencyFieldState extends State<SearchRegencyField>
    with AutomaticKeepAliveClientMixin {
  RegencySearchBloc bloc;

  // @override
  // void initState() {
  //   super.initState();
  // }

  Widget dropDown() {
    return BlocProvider(
      create: (context) => RegencySearchBloc(RegencySearchBlocUninitialized())
        ..add(RegencySearchBlocEvent(selected: widget.selectedRegency)),
      child: BlocBuilder<RegencySearchBloc, RegencySearchBlocState>(
        builder: (context, state) {
          // print(">> State : ${state.toString()}");
          // ignore: close_sinks
          // final bloc = BlocProvider.of<RegencySearchBloc>(context);
          if (state is RegencySearchBlocUninitialized) {
            return Container(
              constraints: BoxConstraints(
                maxHeight: Sizes.s70,
                maxWidth: MediaQuery.of(context).size.width,
              ),
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(Sizes.s5)),
                    ),
                    child: SearchableDropdown(
                      icon: CupertinoActivityIndicator(),
                      items: [],
                      underline: Container(),
                      value: "",
                      hint: Text("${widget.hintText}"),
                      searchHint: Text("Masukan Nama Kota"),
                      onChanged: () {},
                      isExpanded: true,
                      searchFn: () {},
                      readOnly: true,
                      menuBackgroundColor: widget.enabled
                          ? Colors.transparent
                          : Colors.grey[300],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(left: Sizes.s10),
                      color: Colors.white,
                      padding: EdgeInsets.only(left: Sizes.s5, right: Sizes.s5),
                      child: Text(
                        "${widget.label}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.grey[600],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else if (state is RegencySearchBlocLoaded) {
            RegencySearchBlocLoaded data = state;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(
                    maxHeight: Sizes.s70,
                    maxWidth: MediaQuery.of(context).size.width,
                  ),
                  child: Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius:
                              BorderRadius.all(Radius.circular(Sizes.s5)),
                          // color: Colors.pinkAccent,
                        ),
                        child: SearchableDropdown(
                          icon: Icon(Icons.arrow_drop_down),
                          items: data.regencies.map((Regency e) {
                            return DropdownMenuItem(
                                value: e, child: Text(e.name));
                          }).toList(),
                          underline: Container(),
                          multipleSelection: false,
                          value: widget.regencyBloc.value,
                          hint: Text(data.selected != null
                              ? "${data.selected.name}"
                              : "${widget.hintText}"),
                          searchHint: Text("Masukan Nama Kota"),
                          onChanged: (Regency selected) {
                            widget.regencyBloc.updateValue(selected);
                          },
                          isExpanded: true,
                          searchFn: (String keyword, items) {
                            List<int> ret = List<int>();
                            if (keyword != null &&
                                items != null &&
                                keyword.isNotEmpty) {
                              keyword.split(" ").forEach((k) {
                                int i = 0;
                                items.forEach((item) {
                                  if (k.isNotEmpty &&
                                      (item.value.name
                                          .toString()
                                          .toLowerCase()
                                          .contains(k.toLowerCase()))) {
                                    ret.add(i);
                                  }
                                  i++;
                                });
                              });
                            }

                            if (keyword.isEmpty) {
                              ret =
                                  Iterable<int>.generate(items.length).toList();
                            }
                            return (ret);
                          },
                          readOnly: !widget.enabled,
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          margin: EdgeInsets.only(left: Sizes.s10),
                          color: Colors.white,
                          padding:
                              EdgeInsets.only(left: Sizes.s5, right: Sizes.s5),
                          child: Text(
                            "${widget.label}",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                KycErrorWrapper(bloc: widget.regencyBloc)
              ],
            );
          } else {
            bloc = BlocProvider.of<RegencySearchBloc>(context);
            return Column(
              children: [
                OutlineButton(
                  onPressed: () => bloc
                    ..add(
                      RegencySearchBlocEvent(selected: widget.selectedRegency),
                    ),
                  child: Text("Muat ulang"),
                ),
                Text(
                  "Tidak dapat memuat tempat lahir",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.enabled
        ? dropDown()
        : InkWell(
            onTap: widget.onTapIfDisabled,
            child: dropDown(),
          );
  }

  @override
  bool get wantKeepAlive => true;
}
