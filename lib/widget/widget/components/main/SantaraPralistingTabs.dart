import 'package:flutter/material.dart';
import 'SantaraFinancialStatements.dart';

class SantaraPralistingTabs extends StatefulWidget {
  final Widget shortDesc;
  final Widget about;
  final Widget financial;
  final Widget nonFinancial;
  final Widget location;

  SantaraPralistingTabs({
    @required this.shortDesc,
    @required this.about,
    @required this.financial,
    @required this.nonFinancial,
    @required this.location,
  });

  @override
  _SantaraPralistingTabsState createState() => _SantaraPralistingTabsState();
}

class _SantaraPralistingTabsState extends State<SantaraPralistingTabs>
    with SingleTickerProviderStateMixin {
  // inisialisasi nama tabs
  final List<Widget> tabs = [
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Informasi Bisnis",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Deskripsi  Bisnis",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Detail Bisnis",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Informasi Non Finansial",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Kontak",
      ),
    ),
  ];
  TabController _tabController; // controller tabs
  int _tabIndex = 0; // current index

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_handleTabChanges);
    super.initState();
  }

  _handleTabChanges() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Detail Informasi Saham
        TabBar(
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          controller: _tabController,
          isScrollable: true,
          tabs: tabs,
        ),
        Divider(height: 5),
        Center(
          child: [
            widget.financial,
            widget.shortDesc,
            widget.about,
            widget.nonFinancial,
            widget.location,
          ][_tabIndex],
        ),
      ],
    );
  }
}
