import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/utils/ui/screen_sizes.dart';

class MessagePopup {
  static void show(
    context, {
    @required String message,
    @required Function onClose,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(Sizes.s20),
          actionsPadding: EdgeInsets.zero,
          content: Text(
            "$message",
            style: TextStyle(
              fontSize: FontSize.s14,
            ),
            textAlign: TextAlign.center,
          ),
          actions: [
            FlatButton(
              onPressed: onClose,
              child: Text("OK"),
            )
          ],
        );
      },
    );
  }
}
