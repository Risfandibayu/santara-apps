import 'package:flutter/material.dart';
import 'package:santaraapp/features/deposit/main/presentation/pages/main_deposit_page.dart';
import 'package:santaraapp/features/dividen/presentation/pages/main_dividen_page.dart';
import 'package:santaraapp/features/withdraw/presenstation/pages/main_withdraw_page.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/DefaultText.dart';
import 'package:santaraapp/widget/widget/WebView.dart';

class DialogHelper {
  static Future<bool> _onWillPop() async {
    return null ?? false;
  }

  static void show(BuildContext context,
      {String url,
      String channel,
      bool isWithdrawDeviden = false,
      int method,
      String title,
      String message,
      Function function}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: _onWillPop,
            child: AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 50,
                      height: 50,
                      decoration: new BoxDecoration(
                        border: Border.all(color: Colors.green),
                        shape: BoxShape.circle,
                      ),
                      child: Icon(Icons.check, color: Colors.green)),
                  DefaultText(
                    alignment: Alignment.center,
                    textAlign: TextAlign.center,
                    textLabel: title != null ? title : 'Berhasil',
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    fontWeight: FontWeight.bold,
                    sizeText: 20,
                  ),
                  DefaultText(
                    alignment: Alignment.center,
                    textAlign: TextAlign.center,
                    textLabel: message != null ? message : 'Transaksi Berhasil',
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: GestureDetector(
                      onTap: function != null
                          ? function
                          : () {
                              if (channel == "ONEPAY") {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => WebViewWidget(
                                              url: url,
                                              appBarName: 'OTHER PAYMENT',
                                              method: isWithdrawDeviden
                                                  ? "PROFILE"
                                                  : "TRANSACTION",
                                            )));
                              } else {
                                if (method == 1) {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              MainDepositPage(initialTab: 1)));
                                } else if (method == 2) {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => MainWithdrawPage(
                                              initialTab: message ==
                                                      'Berhasil memasukkan bank baru'
                                                  ? 0
                                                  : 1)));
                                } else {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => MainDividenPage(
                                              initialTab: message ==
                                                      'Berhasil memasukkan bank baru'
                                                  ? 0
                                                  : 1)));
                                }
                              }
                            },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        height: 40,
                        width: 200,
                        child: DefaultText(
                          colorsText: Colors.white,
                          alignment: Alignment.center,
                          textLabel: 'OK',
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  static void showGagal(BuildContext context,
      {String title, int method, String message, Function function}) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: _onWillPop,
            child: AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 50,
                      height: 50,
                      decoration: new BoxDecoration(
                        border: Border.all(color: Colors.red),
                        shape: BoxShape.circle,
                      ),
                      child: Icon(Icons.close, color: Colors.red)),
                  DefaultText(
                    alignment: Alignment.center,
                    textAlign: TextAlign.center,
                    textLabel: 'Gagal!',
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    fontWeight: FontWeight.bold,
                    sizeText: 20,
                  ),
                  DefaultText(
                      alignment: Alignment.center,
                      textAlign: TextAlign.center,
                      textLabel: message),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: GestureDetector(
                      onTap: function != null
                          ? function
                          : () {
                              if (method != null) {
                                if (method == 1) {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              MainDepositPage(initialTab: 0)));
                                } else if (method == 2) {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              MainWithdrawPage(initialTab: 0)));
                                } else {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => Home(index: 4)),
                                      (Route<dynamic> route) => false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              MainDividenPage(initialTab: 1)));
                                }
                              } else {
                                Navigator.pop(context);
                                Navigator.pop(context);
                              }
                            },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        height: 40,
                        width: 200,
                        child: DefaultText(
                          colorsText: Colors.white,
                          alignment: Alignment.center,
                          textLabel: 'OK',
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
