import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/BiodataPribadiModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class BiodataState {}

class BiodataEvent {}

class HideNotification extends BiodataEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class BiodataUninitialized extends BiodataState {}

class BiodataError extends BiodataState {
  final String error;

  BiodataError({this.error});

  BiodataError copyWith({String error}) {
    return BiodataError(error: error ?? this.error);
  }
}

class BiodataUnauthorized extends BiodataState {}

class BiodataLoaded extends BiodataState {
  BiodataPribadiModel data;
  bool showNotification;
  String token;
  BiodataLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  BiodataLoaded copyWith({
    BiodataPribadiModel data,
    bool showNotification,
    String token,
  }) {
    return BiodataLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class BiodataViewBloc extends Bloc<BiodataEvent, BiodataState> {
  BiodataViewBloc(BiodataState initialState) : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(BiodataLoaded state) {
    // print(">> Wadidaw");
    // emit();
    state.copyWith(showNotification: false);
  }

  @override
  Stream<BiodataState> mapEventToState(BiodataEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is BiodataLoaded) {
        BiodataLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is BiodataUninitialized) {
      var result = await _service.getBiodataPribadiData();
      if (result.statusCode == 200) {
        var data = BiodataPribadiModel.fromJson(result.data);
        yield BiodataLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield BiodataUnauthorized();
      } else {
        yield BiodataError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
