import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/BioPribadiUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

import 'biodata.view.bloc.dart';

class ViewBiodataUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewBiodataUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Biodata Pribadi",
          steps: "1/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BiodataPribadiUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Biodata anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => BiodataViewBloc(
          BiodataUninitialized(),
        )..add(BiodataEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Biodata Pribadi"),
              )
            : BiodataBody(),
      ),
    );
  }
}

class BiodataBody extends StatefulWidget {
  @override
  _BiodataBodyState createState() => _BiodataBodyState();
}

class _BiodataBodyState extends State<BiodataBody> {
  BiodataViewBloc bloc;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<BiodataViewBloc>(context);
  }

  String convertDate(String date) {
    try {
      return dateFormat.format(DateTime.parse(date));
    } catch (e) {
      return "-";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<BiodataViewBloc, BiodataState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is BiodataUnauthorized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<BiodataViewBloc, BiodataState>(
          builder: (context, state) {
            if (state is BiodataUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is BiodataLoaded) {
              BiodataLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycTitleWrapper(
                        title: "Data Diri",
                      ),
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      Center(
                        child: Container(
                          width: Sizes.s100,
                          height: Sizes.s100,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(Sizes.s60),
                            ),
                            child: data?.data?.photo != null
                                ? SantaraCachedImage(
                                    image: GetAuthenticatedFile.convertUrl(
                                      image: "${data.data.photo}",
                                      type: PathType.traderPhoto,
                                    ),
                                  )
                                : CircleAvatar(
                                    backgroundColor: Colors.grey[100],
                                    child: Icon(
                                      Icons.person,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      KycItemTile(
                        title: "Nama Lengkap",
                        subtitle: "${data?.data?.name ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Tanggal Lahir",
                        subtitle: convertDate(data.data.birthDate),
                      ),
                      KycItemTile(
                        title: "Tempat Lahir",
                        subtitle: "${data?.data?.birthPlaceName ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Jenis Kelamin",
                        subtitle:
                            "${data?.data?.gender != null && data?.data?.gender == 'm' ? 'Laki-laki' : 'Perempuan'}",
                      ),
                      KycItemTile(
                        title: "Pendidikan Terakhir",
                        subtitle: "${data?.data?.education ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Kewarganegaraan",
                        subtitle: "${data?.data?.countryName ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Email",
                        subtitle: "${data?.data?.email ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Email Lain",
                        subtitle: "${data?.data?.anotherEmail ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Nomor Telepon",
                        subtitle: "${data?.data?.phone ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Nomor Telepon Lain",
                        subtitle: "${data?.data?.altPhone ?? '-'}",
                      ),
                      KycTitleWrapper(
                        title: "Data Diri",
                      ),
                      KycItemTile(
                        title: "Nomor Induk Kependudukan (NIK)",
                        subtitle: "${data?.data?.idcardNumber ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Tanggal Registrasi KTP",
                        subtitle: convertDate(data.data.regisDateIdcard),
                      ),
                      KycItemTile(
                        title: "Tanggal Kadaluarsa KTP",
                        subtitle:
                            "${data.data.typeIdcard == 'KTP' ? convertDate(data.data.expiredDateIdcard) : 'Seumur Hidup'}",
                      ),
                      KycItemTile(
                        title: "Nomor Passport",
                        subtitle: "${data?.data?.passportNumber ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Tanggal Kadaluarsa Passport",
                        subtitle: data?.data?.expiredDatePassport != null
                            ? convertDate(data.data.expiredDatePassport)
                            : "-",
                      ),
                      Text(
                        "Foto Kartu Data Diri",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s10,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      Center(
                        child: SantaraCachedImage(
                          width: Sizes.s250,
                          height: Sizes.s150,
                          image: GetAuthenticatedFile.convertUrl(
                            image: "${data.data.photoIdcard}",
                            type: PathType.traderIdCardPhoto,
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Foto Selfie dengan Kartu Data Diri",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s10,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      Center(
                        child: SantaraCachedImage(
                          width: Sizes.s250,
                          height: Sizes.s150,
                          image: GetAuthenticatedFile.convertUrl(
                            image: "${data.data.photoVerification}",
                            type: PathType.traderVerificationPhoto,
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                      // KycItemTile(
                      //   title: "",
                      //   subtitle: "",
                      // ),
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
