import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/alamat/AlamatUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';
import 'alamat.view.bloc.dart';

class ViewAlamatUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewAlamatUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Alamat",
          steps: "3/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AlamatUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Alamat anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => AlamatViewBloc(
          AlamatUninitialized(),
        )..add(AlamatEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Alamat"),
              )
            : AlamatBody(),
      ),
    );
  }
}

class AlamatBody extends StatefulWidget {
  @override
  _AlamatBodyState createState() => _AlamatBodyState();
}

class _AlamatBodyState extends State<AlamatBody> {
  AlamatViewBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<AlamatViewBloc>(context);
  }

  Widget _alamatTinggal(AlamatLoaded data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        KycTitleWrapper(title: "Alamat Tinggal Sekarang"),
        KycItemTile(
          title: "Negara",
          subtitle: "${data.data.countryDomicileName}",
        ),
        KycItemTile(
          title: "Provinsi",
          subtitle: "${data.data.province}",
        ),
        KycItemTile(
          title: "Kota",
          subtitle: "${data.data.regency}",
        ),
        KycItemTile(
          title: "Alamat Tinggal Saat Ini",
          subtitle: "${data.data.address}",
        ),
        KycItemTile(
          title: "Kode POS",
          subtitle: "${data.data.postalCode}",
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<AlamatViewBloc, AlamatState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is AlamatUnauthorized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<AlamatViewBloc, AlamatState>(
          builder: (context, state) {
            if (state is AlamatUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is AlamatLoaded) {
              AlamatLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycTitleWrapper(title: "Alamat Sesuai Dokumen Data Diri"),
                      KycItemTile(
                        title: "Negara",
                        subtitle: "${data.data.countryName}",
                      ),
                      KycItemTile(
                        title: "Provinsi",
                        subtitle: "${data.data.provinceName}",
                      ),
                      KycItemTile(
                        title: "Kota",
                        subtitle: "${data.data.regencyName}",
                      ),
                      KycItemTile(
                        title: "Alamat Sesuai KTP",
                        subtitle: "${data.data.idcardAddress}",
                      ),
                      KycItemTile(
                        title: "Kode POS",
                        subtitle: "${data.data.idcardPostalCode}",
                      ),
                      SizedBox(
                        height: Sizes.s50,
                      ),
                      data.data.addressSameWithIdcard == 2
                          ? _alamatTinggal(data)
                          : Container()
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
