import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/AlamatModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class AlamatState {}

class AlamatEvent {}

class HideNotification extends AlamatEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class AlamatUninitialized extends AlamatState {}

class AlamatUnauthorized extends AlamatState {}

class AlamatError extends AlamatState {
  final String error;

  AlamatError({this.error});

  AlamatError copyWith({String error}) {
    return AlamatError(error: error ?? this.error);
  }
}

class AlamatLoaded extends AlamatState {
  AlamatModel data;
  bool showNotification;
  String token;
  AlamatLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  AlamatLoaded copyWith({
    AlamatModel data,
    bool showNotification,
    String token,
  }) {
    return AlamatLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class AlamatViewBloc extends Bloc<AlamatEvent, AlamatState> {
  AlamatViewBloc(AlamatState initialState) : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(AlamatLoaded state) {
    // print(">> Wadidaw");
    // emit();
    state.copyWith(showNotification: false);
  }

  @override
  Stream<AlamatState> mapEventToState(AlamatEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is AlamatLoaded) {
        AlamatLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is AlamatUninitialized) {
      var result = await _service.getAlamatData();
      if (result.statusCode == 200) {
        var data = AlamatModel.fromJson(result.data);
        yield AlamatLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield AlamatUnauthorized();
      } else {
        yield AlamatError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
