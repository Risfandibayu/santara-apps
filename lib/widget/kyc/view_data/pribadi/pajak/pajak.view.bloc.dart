import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/InformasiPajakModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class PajakState {}

class PajakEvent {}

class HideNotification extends PajakEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class PajakUninitialized extends PajakState {}

class PajakUnauthorized extends PajakState {}

class PajakError extends PajakState {
  final String error;

  PajakError({this.error});

  PajakError copyWith({String error}) {
    return PajakError(error: error ?? this.error);
  }
}

class PajakLoaded extends PajakState {
  InformasiPajakModel data;
  bool showNotification;
  String token;
  PajakLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  PajakLoaded copyWith({
    InformasiPajakModel data,
    bool showNotification,
    String token,
  }) {
    return PajakLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class PajakViewBloc extends Bloc<PajakEvent, PajakState> {
  PajakViewBloc(PajakState initialState) : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(PajakLoaded state) {
    state.copyWith(showNotification: false);
  }

  @override
  Stream<PajakState> mapEventToState(PajakEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is PajakLoaded) {
        PajakLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is PajakUninitialized) {
      var result = await _service.getInformasiPajakData();
      if (result.statusCode == 200) {
        var data = InformasiPajakModel.fromJson(result.data);
        yield PajakLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield PajakUnauthorized();
      } else {
        yield PajakError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
