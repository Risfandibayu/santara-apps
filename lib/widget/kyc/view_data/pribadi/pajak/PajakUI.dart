import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/informasi_pajak/InformasiPajakUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/pajak/pajak.view.bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewPajakUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewPajakUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Informasi Pajak",
          steps: "5/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InformasiPajakUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Informasi Pajak anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => PajakViewBloc(
          PajakUninitialized(),
        )..add(PajakEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Informasi Pajak"),
              )
            : PajakBody(),
      ),
    );
  }
}

class PajakBody extends StatefulWidget {
  @override
  _PajakBodyState createState() => _PajakBodyState();
}

class _PajakBodyState extends State<PajakBody> {
  PajakViewBloc bloc;
  final rupiah = NumberFormat("#,##0");
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<PajakViewBloc>(context);
  }

  openPdf(String url) async {
    try {
      await launch(url);
    } catch (e) {
      print(">> ERROR : $e");
    }
  }

  Widget pdfCard(String title, String file) {
    return file == null
        ? Container()
        : Container(
            margin: EdgeInsets.only(bottom: Sizes.s25),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "$title",
                  style: TextStyle(
                      fontSize: FontSize.s12, fontWeight: FontWeight.w600),
                ),
                Container(
                  margin: EdgeInsets.only(top: Sizes.s10),
                  height: Sizes.s50,
                  padding: EdgeInsets.only(left: 10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xffB8B8B8)),
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "${file.split('/').last}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: FontSize.s12,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () => openPdf(file),
                        child: Container(
                          height: Sizes.s50,
                          width: Sizes.s80,
                          decoration: BoxDecoration(
                              color: Color(0xff6870E1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Center(
                            child: Text(
                              "Lihat",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s12,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<PajakViewBloc, PajakState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is PajakUnauthorized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<PajakViewBloc, PajakState>(
          builder: (context, state) {
            if (state is PajakUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is PajakLoaded) {
              PajakLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycTitleWrapper(title: "Informasi Pajak"),
                      KycItemTile(
                          title: "Pendapatan Pertahun Sebelum Pajak",
                          subtitle:
                              "Rp ${rupiah.format(int.parse(data.data.income ?? 0))}"),
                      KycItemTile(
                          title: "Kode Akun Pajak",
                          subtitle: "${data.data.taxAccountCode}"),
                      KycItemTile(
                          title: "No NPWP", subtitle: "${data.data.npwp}"),
                      KycItemTile(
                        title: "Tanggal Registrasi NPWP",
                        subtitle:
                            "${data.data.dateRegistrationOfNpwp != null ? dateFormat.format(DateTime.parse(data.data.dateRegistrationOfNpwp)) : '-'}",
                      ),
                      KycTitleWrapper(title: "Rekening Efek"),
                      KycItemTile(
                        title: "No SID",
                        subtitle: "${data?.data?.sidNumber ?? '-'}",
                      ),
                      KycItemTile(
                        title: "Tanggal Registrasi Rekening Efek",
                        subtitle:
                            "${data.data.securitiesAccountDateRegistration != null ? dateFormat.format(DateTime.parse(data.data.securitiesAccountDateRegistration)) : '-'}",
                      ),
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      data.data.haveSecuritiesAccount
                          ? pdfCard(
                              "Foto Rekening Efek",
                              data.data.securitiesAccount,
                            )
                          : Container()
                      //     ? Text(
                      //         "Foto Kartu Data Diri",
                      //         style: TextStyle(
                      //           color: Colors.black,
                      //           fontSize: FontSize.s10,
                      //         ),
                      //       )
                      //     : Container(),
                      // SizedBox(height: Sizes.s10),
                      // data.data.haveSecuritiesAccount
                      //     ? Center(
                      //         child: FadeInImage.assetNetwork(
                      //           width: Sizes.s250,
                      //           height: Sizes.s150,
                      //           placeholder: 'assets/Preload.jpeg',
                      //           image: apiLocalImage +
                      //               data.data.securitiesAccount +
                      //               "?token=${data.token}",
                      //           fit: BoxFit.cover,
                      //         ),
                      //       )
                      //     : Container(),
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
