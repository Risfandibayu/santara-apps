import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/BiodataKeluargaModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class BiodataKeluargaState {}

class BiodataKeluargaEvent {}

class HideNotification extends BiodataKeluargaEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class BiodataKeluargaUninitialized extends BiodataKeluargaState {}

class BiodataKeluargaUnauthorized extends BiodataKeluargaState {}

class BiodataKeluargaError extends BiodataKeluargaState {
  final String error;

  BiodataKeluargaError({this.error});

  BiodataKeluargaError copyWith({String error}) {
    return BiodataKeluargaError(error: error ?? this.error);
  }
}

class BiodataKeluargaLoaded extends BiodataKeluargaState {
  BiodataKeluargaModel data;
  bool showNotification;
  String token;
  BiodataKeluargaLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  BiodataKeluargaLoaded copyWith({
    BiodataKeluargaModel data,
    bool showNotification,
    String token,
  }) {
    return BiodataKeluargaLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class BiodataKeluargaViewBloc
    extends Bloc<BiodataKeluargaEvent, BiodataKeluargaState> {
  BiodataKeluargaViewBloc(BiodataKeluargaState initialState)
      : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(BiodataKeluargaLoaded state) {
    state.copyWith(showNotification: false);
  }

  @override
  Stream<BiodataKeluargaState> mapEventToState(
      BiodataKeluargaEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is BiodataKeluargaLoaded) {
        BiodataKeluargaLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is BiodataKeluargaUninitialized) {
      var result = await _service.getBiodataKeluargaData();
      if (result.statusCode == 200) {
        var data = BiodataKeluargaModel.fromJson(result.data);
        yield BiodataKeluargaLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield BiodataKeluargaUnauthorized();
      } else {
        yield BiodataKeluargaError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
