import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/bio_keluarga/BioKeluargaUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/biodata_keluarga/biokeluarga.view.bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewBiodataKeluargaUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewBiodataKeluargaUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Biodata Keluarga",
          steps: "2/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BiodataKeluargaUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Biodata keluarga anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => BiodataKeluargaViewBloc(
          BiodataKeluargaUninitialized(),
        )..add(BiodataKeluargaEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Biodata Keluarga"),
              )
            : BiodataBody(),
      ),
    );
  }
}

class BiodataBody extends StatefulWidget {
  @override
  _BiodataBodyState createState() => _BiodataBodyState();
}

class _BiodataBodyState extends State<BiodataBody> {
  BiodataKeluargaViewBloc bloc;

  String parseNikah(String id) {
    switch (id) {
      case "1":
        return "Single";
        break;
      case "2":
        return "Menikah";
        break;
      case "3":
        return "Duda";
        break;
      case "4":
        return "Janda";
        break;
      default:
        return "-";
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<BiodataKeluargaViewBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<BiodataKeluargaViewBloc, BiodataKeluargaState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is BiodataKeluargaUnauthorized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<BiodataKeluargaViewBloc, BiodataKeluargaState>(
          builder: (context, state) {
            if (state is BiodataKeluargaUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is BiodataKeluargaLoaded) {
              BiodataKeluargaLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycItemTile(
                        title: "Status Pernikahan",
                        subtitle: "${parseNikah(data.data.maritalStatus)}",
                      ),
                      KycItemTile(
                        title: "Nama Lengkap Pasangan",
                        subtitle: "${data.data.spouseName}",
                      ),
                      KycItemTile(
                        title: "Nama Gadis Ibu Kandung",
                        subtitle: "${data.data.motherMaidenName}",
                      ),
                      KycItemTile(
                        title: "Nama Lengkap Ahli Waris",
                        subtitle: "${data.data.heir}",
                      ),
                      KycItemTile(
                        title: "Hubungan Dengan Ahli Waris",
                        subtitle: "${data.data.heirRelation}",
                      ),
                      KycItemTile(
                        title: "No Handphone Ahli Waris",
                        subtitle: "${data.data.heirPhone}",
                      ),
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
