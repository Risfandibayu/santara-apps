import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/PekerjaanModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class PekerjaanState {}

class PekerjaanEvent {}

class HideNotification extends PekerjaanEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class PekerjaanUninitialized extends PekerjaanState {}

class PekerjaanUnauthorized extends PekerjaanState {}

class PekerjaanError extends PekerjaanState {
  final String error;

  PekerjaanError({this.error});

  PekerjaanError copyWith({String error}) {
    return PekerjaanError(error: error ?? this.error);
  }
}

class PekerjaanLoaded extends PekerjaanState {
  PekerjaanModel data;
  bool showNotification;
  String token;
  PekerjaanLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  PekerjaanLoaded copyWith({
    PekerjaanModel data,
    bool showNotification,
    String token,
  }) {
    return PekerjaanLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class PekerjaanViewBloc extends Bloc<PekerjaanEvent, PekerjaanState> {
  PekerjaanViewBloc(PekerjaanState initialState) : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(PekerjaanLoaded state) {
    state.copyWith(showNotification: false);
  }

  @override
  Stream<PekerjaanState> mapEventToState(PekerjaanEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is PekerjaanLoaded) {
        PekerjaanLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is PekerjaanUninitialized) {
      var result = await _service.getPekerjaanData();
      if (result.statusCode == 200) {
        var data = PekerjaanModel.fromJson(result.data);
        yield PekerjaanLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield PekerjaanUnauthorized();
      } else {
        yield PekerjaanError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
