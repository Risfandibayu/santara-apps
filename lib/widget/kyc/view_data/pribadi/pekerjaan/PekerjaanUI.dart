import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/pekerjaan/PekerjaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/pekerjaan/pekerjaan.view.bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewPekerjaanUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewPekerjaanUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Pekerjaan",
          steps: "4/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => KycPekerjaanUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Pekerjaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => PekerjaanViewBloc(
          PekerjaanUninitialized(),
        )..add(PekerjaanEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Pekerjaan"),
              )
            : PekerjaanBody(),
      ),
    );
  }
}

class PekerjaanBody extends StatefulWidget {
  @override
  _PekerjaanBodyState createState() => _PekerjaanBodyState();
}

class _PekerjaanBodyState extends State<PekerjaanBody> {
  PekerjaanViewBloc bloc;
  final rupiah = NumberFormat("#,##0");

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<PekerjaanViewBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<PekerjaanViewBloc, PekerjaanState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is PekerjaanUnauthorized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<PekerjaanViewBloc, PekerjaanState>(
          builder: (context, state) {
            if (state is PekerjaanUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is PekerjaanLoaded) {
              PekerjaanLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycItemTile(
                        title: "Pekerjaan",
                        subtitle: "${data.data.name}",
                      ),
                      KycItemTile(
                        title: "Bidang Usaha",
                        subtitle: "${data.data.descriptionJob}",
                      ),
                      KycItemTile(
                        title: "Total Aset",
                        subtitle:
                            "Rp ${rupiah.format(int.parse(data.data.totalAssetsOfInvestors ?? 0))}",
                      ),
                      KycItemTile(
                        title: "Sumber Dana",
                        subtitle: "${data.data.sourceOfInvestorFunds}",
                      ),
                      KycItemTile(
                        title: "Motivasi Investasi",
                        subtitle: "${data.data.reasonToJoin}",
                      ),
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
