import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../../models/kyc/pribadi/BankModel.dart';
import '../../../../../services/kyc/KycPersonalService.dart';

class BankState {}

class BankEvent {}

class HideNotification extends BankEvent {
  bool showNotification;

  HideNotification({this.showNotification});
}

class BankUninitialized extends BankState {}

class BankUnauthroized extends BankState {}

class BankError extends BankState {
  final String error;

  BankError({this.error});

  BankError copyWith({String error}) {
    return BankError(error: error ?? this.error);
  }
}

class BankLoaded extends BankState {
  BankModel data;
  bool showNotification;
  String token;
  BankLoaded({
    this.data,
    this.showNotification = true,
    this.token,
  });

  BankLoaded copyWith({
    BankModel data,
    bool showNotification,
    String token,
  }) {
    return BankLoaded(
      data: data ?? this.data,
      showNotification: showNotification ?? this.showNotification,
      token: token ?? this.token,
    );
  }
}

class BankViewBloc extends Bloc<BankEvent, BankState> {
  BankViewBloc(BankState initialState) : super(initialState);
  final _service = KycPersonalService();
  final storage = new FlutterSecureStorage(); // storage

  hideNotification(BankLoaded state) {
    state.copyWith(showNotification: false);
  }

  @override
  Stream<BankState> mapEventToState(BankEvent event) async* {
    final token = await storage.read(key: 'token');
    if (event is HideNotification) {
      if (state is BankLoaded) {
        BankLoaded data = state;
        yield data.copyWith(showNotification: false);
      }
    }

    if (state is BankUninitialized) {
      var result = await _service.getBankData();
      if (result.statusCode == 200) {
        var data = BankModel.fromJson(result.data);
        yield BankLoaded(
          showNotification: true,
          data: data,
          token: token,
        );
      } else if (result.statusCode == 401) {
        yield BankUnauthroized();
      } else {
        yield BankError(error: "Tidak dapat memuat data!");
      }
    }
  }
}
