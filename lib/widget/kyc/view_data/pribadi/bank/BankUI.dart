import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/bank/KycBankUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

import 'bank.view.bloc.dart';

class ViewBankUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;

  ViewBankUI({
    @required this.submissionId,
    @required this.isEdit,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Akun Bank",
          steps: "6/6",
          onEdit: () {
            if (status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => KycBankUI(
                    submissionId: submissionId,
                    isEdit: isEdit,
                    status: status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Akun Bank anda sedang dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: BlocProvider(
        create: (context) => BankViewBloc(
          BankUninitialized(),
        )..add(BankEvent()),
        child: status == "empty"
            ? Center(
                child: Text("Anda Belum Mengisi Bank"),
              )
            : BankBody(),
      ),
    );
  }
}

class BankBody extends StatefulWidget {
  @override
  _BankBodyState createState() => _BankBodyState();
}

class _BankBodyState extends State<BankBody> {
  BankViewBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<BankViewBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      padding: EdgeInsets.all(Sizes.s18),
      child: BlocListener<BankViewBloc, BankState>(
        bloc: bloc,
        listener: (context, state) {
          if (state is BankUnauthroized) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        },
        child: BlocBuilder<BankViewBloc, BankState>(
          builder: (context, state) {
            if (state is BankUninitialized) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is BankLoaded) {
              BankLoaded data = state;
              return Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      data.showNotification
                          ? PreviewNotification(
                              onClose: () => bloc.add(
                                HideNotification(),
                              ),
                            )
                          : Container(),
                      data.showNotification
                          ? SizedBox(
                              height: Sizes.s20,
                            )
                          : Container(),
                      KycTitleWrapper(title: "Akun Bank"),
                      KycItemTile(
                        title: "Nama Pemilik Rekening",
                        subtitle: "${data.data.accountName1}",
                      ),
                      KycItemTile(
                        title: "Nama Bank",
                        subtitle: "${data.data.bank1}",
                      ),
                      KycItemTile(
                        title: "Nomor Rekening",
                        subtitle: "${data.data.accountNumber1}",
                      ),
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      KycTitleWrapper(title: "Akun Bank 2"),
                      KycItemTile(
                        title: "Nama Pemilik Rekening",
                        subtitle: "${data.data.accountName2}",
                      ),
                      KycItemTile(
                        title: "Nama Bank",
                        subtitle: "${data.data.bank2}",
                      ),
                      KycItemTile(
                        title: "Nomor Rekening",
                        subtitle: "${data.data.accountNumber2}",
                      ),
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      KycTitleWrapper(title: "Akun Bank 3"),
                      KycItemTile(
                        title: "Nama Pemilik Rekening",
                        subtitle: "${data.data.accountName3}",
                      ),
                      KycItemTile(
                        title: "Nama Bank",
                        subtitle: "${data.data.bank3}",
                      ),
                      KycItemTile(
                        title: "Nomor Rekening",
                        subtitle: "${data.data.accountNumber3}",
                      ),
                      SizedBox(
                        height: Sizes.s20,
                      ),
                    ],
                  ),
                ],
              );
            } else {
              return Center(
                child: Text(
                  "Unknown state",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
