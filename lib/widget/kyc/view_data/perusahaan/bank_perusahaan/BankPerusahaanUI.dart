import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/bank/BankPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewBankPerusahaanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewBankPerusahaanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewBankPerusahaanUIState createState() => _ViewBankPerusahaanUIState();
}

class _ViewBankPerusahaanUIState extends State<ViewBankPerusahaanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    TraderBank company = widget.data?.traderBank;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Bank Perusahaan",
          steps: "8/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BankPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Bank Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Akun Bank"),
              KycItemTile(
                title: "Nama Bank Perusahaan",
                subtitle: "${company?.bank1 ?? '-'}",
              ),
              KycItemTile(
                title: "Nama Pemilik Rekening Perusahaan",
                subtitle: "${company?.accountName1 ?? '-'}",
              ),
              KycItemTile(
                title: "Nomor Rekening Perusahaan",
                subtitle: "${company?.accountNumber1 ?? '-'}",
              ),
              KycItemTile(
                title: "Mata Uang Bank",
                subtitle: "${company?.accountCurrency1 ?? '-'}",
              ),
              SizedBox(
                height: 20,
              ),
              KycTitleWrapper(title: "Akun Bank 2"),
              KycItemTile(
                title: "Nama Bank Perusahaan",
                subtitle: "${company?.bank2 ?? '-'}",
              ),
              KycItemTile(
                title: "Nama Pemilik Rekening Perusahaan",
                subtitle: "${company?.accountName2 ?? '-'}",
              ),
              KycItemTile(
                title: "Nomor Rekening Perusahaan",
                subtitle: "${company?.accountNumber2 ?? '-'}",
              ),
              KycItemTile(
                title: "Mata Uang Bank",
                subtitle: "${company?.accountCurrency2 ?? '-'}",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Akun Bank 3"),
              KycItemTile(
                title: "Nama Bank Perusahaan",
                subtitle: "${company?.bank3 ?? '-'}",
              ),
              KycItemTile(
                title: "Nama Pemilik Rekening Perusahaan",
                subtitle: "${company?.accountName3 ?? '-'}",
              ),
              KycItemTile(
                title: "Nomor Rekening Perusahaan",
                subtitle: "${company?.accountNumber3 ?? '-'}",
              ),
              KycItemTile(
                title: "Mata Uang Bank",
                subtitle: "${company?.accountCurrency3 ?? '-'}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
