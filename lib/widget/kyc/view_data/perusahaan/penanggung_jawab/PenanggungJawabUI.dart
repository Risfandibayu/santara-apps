import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/pj/PjPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewPenanggungJawabUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewPenanggungJawabUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewPenanggungJawabUIState createState() => _ViewPenanggungJawabUIState();
}

class _ViewPenanggungJawabUIState extends State<ViewPenanggungJawabUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Penanggung Jawab Perusahaan",
          steps: "4/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PjPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Penanggung Jawab Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Penanggung Jawab Perusahaan"),
              KycItemTile(
                title: "Nama Lengkap",
                subtitle: "${company.companyResponsibleName1}",
              ),
              KycItemTile(
                title: "Jabatan",
                subtitle: "${company.companyResponsiblePosition1}",
              ),
              KycItemTile(
                title: "Nomor KTP",
                subtitle: "${company.companyResponsibleIdcardNumber1}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa KTP",
                subtitle: company?.companyResponsibleExpiredDateIdcard1 != null
                    ? "${dateFormat.format(DateTime.parse(company.companyResponsibleExpiredDateIdcard1 ?? DateTime.now()))}"
                    : "-",
              ),
              KycItemTile(
                title: "Nomor NPWP Penanggung Jawab",
                subtitle: "${company.companyResponsibleNpwp1}",
              ),
              KycItemTile(
                title: "Nomor Passport",
                subtitle: "${company.companyResponsiblePassport1}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa Passport",
                subtitle: company?.companyResponsibleExpiredDatePassport1 !=
                        null
                    ? "${dateFormat.format(DateTime.parse(company.companyResponsibleExpiredDatePassport1 ?? DateTime.now()))}"
                    : "-",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Penanggung Jawab Perusahaan 2"),
              KycItemTile(
                title: "Nama Lengkap",
                subtitle: "${company.companyResponsibleName2}",
              ),
              KycItemTile(
                title: "Jabatan",
                subtitle: "${company.companyResponsiblePosition2}",
              ),
              KycItemTile(
                title: "Nomor KTP",
                subtitle: "${company.companyResponsibleIdcardNumber2}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa KTP",
                subtitle: company?.companyResponsibleExpiredDateIdcard2 != null
                    ? "${dateFormat.format(DateTime.parse(company.companyResponsibleExpiredDateIdcard2 ?? DateTime.now()))}"
                    : "-",
              ),
              KycItemTile(
                title: "Nomor NPWP Penanggung Jawab",
                subtitle: "${company.companyResponsibleNpwp2}",
              ),
              KycItemTile(
                title: "Nomor Passport",
                subtitle: "${company.companyResponsiblePassport2}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa Passport",
                subtitle: company?.companyResponsibleExpiredDatePassport2 !=
                        null
                    ? "${dateFormat.format(DateTime.parse(company?.companyResponsibleExpiredDatePassport2 ?? DateTime.now()))}"
                    : "-",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Penanggung Jawab Perusahaan 3"),
              KycItemTile(
                title: "Nama Lengkap",
                subtitle: "${company.companyResponsibleName3}",
              ),
              KycItemTile(
                title: "Jabatan",
                subtitle: "${company.companyResponsiblePosition3}",
              ),
              KycItemTile(
                title: "Nomor KTP",
                subtitle: "${company.companyResponsibleIdcardNumber3}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa KTP",
                subtitle: company?.companyResponsibleExpiredDateIdcard3 != null
                    ? "${dateFormat.format(DateTime.parse(company.companyResponsibleExpiredDateIdcard3 ?? DateTime.now()))}"
                    : "-",
              ),
              KycItemTile(
                title: "Nomor NPWP Penanggung Jawab",
                subtitle: "${company.companyResponsibleNpwp3}",
              ),
              KycItemTile(
                title: "Nomor Passport",
                subtitle: "${company.companyResponsiblePassport3}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa Passport",
                subtitle: company?.companyResponsibleExpiredDatePassport3 !=
                        null
                    ? "${dateFormat.format(DateTime.parse(company?.companyResponsibleExpiredDatePassport3 ?? DateTime.now()))}"
                    : "-",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Penanggung Jawab Perusahaan 4"),
              KycItemTile(
                title: "Nama Lengkap",
                subtitle: "${company.companyResponsibleName4}",
              ),
              KycItemTile(
                title: "Jabatan",
                subtitle: "${company.companyResponsiblePosition4}",
              ),
              KycItemTile(
                title: "Nomor KTP",
                subtitle: "${company.companyResponsibleIdcardNumber4}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa KTP",
                subtitle: company?.companyResponsibleExpiredDateIdcard4 != null
                    ? "${dateFormat.format(DateTime.parse(company.companyResponsibleExpiredDateIdcard4 ?? DateTime.now()))}"
                    : "-",
              ),
              KycItemTile(
                title: "Nomor NPWP Penanggung Jawab",
                subtitle: "${company.companyResponsibleNpwp4}",
              ),
              KycItemTile(
                title: "Nomor Passport",
                subtitle: "${company.companyResponsiblePassport4}",
              ),
              KycItemTile(
                title: "Tanggal Kadaluarsa Passport",
                subtitle: company?.companyResponsibleExpiredDatePassport4 !=
                        null
                    ? "${dateFormat.format(DateTime.parse(company?.companyResponsibleExpiredDatePassport4 ?? DateTime.now()))}"
                    : "-",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
