import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/aset/AsetPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewAsetPerusahaanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewAsetPerusahaanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewAsetPerusahaanUIState createState() => _ViewAsetPerusahaanUIState();
}

class _ViewAsetPerusahaanUIState extends State<ViewAsetPerusahaanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  String parseTotalAset(String id) {
    switch (id) {
      case "1":
        return "< Rp 100 Milyar";
        break;
      case "2":
        return "Rp 100 Milyar - Rp 500 Milyar";
        break;
      case "3":
        return "Rp 500 Milyar - Rp 1 Triliun";
        break;
      case "4":
        return "Rp 1 Triliun - Rp 5 Triliun";
        break;
      case "5":
        return "> Rp 5 Triliun";
        break;
      default:
        return "-";
        break;
    }
  }

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Aset Perusahaan",
          steps: "5/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AsetPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Aset Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Keterangan Aset"),
              KycItemTile(
                title: "Sumber Dana",
                subtitle: "${company?.sourceOfFunds ?? '-'}",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Total Aset"),
              KycItemTile(
                title: "Total Aset 1 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyTotalProperty1)}",
              ),
              KycItemTile(
                title: "Total Aset 2 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyTotalProperty2)}",
              ),
              KycItemTile(
                title: "Total Aset 3 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyTotalProperty3)}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
