import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/biodata_perusahaan/BiodataPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class ViewBiodataPerusahaanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewBiodataPerusahaanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewBiodataPerusahaanUIState createState() =>
      _ViewBiodataPerusahaanUIState();
}

class _ViewBiodataPerusahaanUIState extends State<ViewBiodataPerusahaanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Biodata Perusahaan",
          steps: "1/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BiodataPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Biodata Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Profil Perusahaan"),
              SizedBox(
                height: Sizes.s20,
              ),
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(Sizes.s60),
                  ),
                  child: SantaraCachedImage(
                    image: GetAuthenticatedFile.convertUrl(
                      image: company?.companyPhoto ?? '',
                      type: PathType.traderPhoto,
                    ),
                    width: Sizes.s60,
                    height: Sizes.s60,
                  ),
                ),
              ),
              KycItemTile(
                title: "Nama Perusahaan",
                subtitle: "${company?.name ?? '-'}",
              ),
              KycItemTile(
                title: "Jenis Perusahaan",
                subtitle: "${company?.businessTypeName ?? '-'}",
              ),
              KycItemTile(
                title: "Tipe Perusahaan",
                subtitle: "${company?.companyCharName ?? '-'}",
              ),
              KycItemTile(
                title: "Berbasis Syariah",
                subtitle:
                    "${company?.companySyariah != null && company?.companySyariah == 'Y' ? 'Ya' : 'Tidak'}",
              ),
              KycItemTile(
                title: "Domisili Perusahaan",
                subtitle: "Indonesia",
              ),
              KycItemTile(
                title: "Tempat Pendirian Usaha",
                subtitle: "${company?.companyEstablishmentPlaceName ?? '-'}",
              ),
              KycItemTile(
                title: "Tanggal Pendirian Usaha",
                subtitle: company?.companyDateEstablishment != null
                    ? "${dateFormat.format(DateTime.parse(company?.companyDateEstablishment ?? DateTime.now()))}"
                    : "-",
              ),
              KycItemTile(
                title: "Email",
                subtitle: "${widget?.data?.email ?? '-'}",
              ),
              KycItemTile(
                title: "Email Lain",
                subtitle: "${company?.anotherEmail ?? '-'}",
              ),
              KycItemTile(
                title: "Deskripsi Usaha",
                subtitle: "${company?.description ?? '-'}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
