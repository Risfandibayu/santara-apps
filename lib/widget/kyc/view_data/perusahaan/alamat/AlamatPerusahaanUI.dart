import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/alamat/AlamatPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewAlamatUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewAlamatUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewAlamatUIState createState() => _ViewAlamatUIState();
}

class _ViewAlamatUIState extends State<ViewAlamatUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Alamat Perusahaan",
          steps: "3/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AlamatPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Alamat anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Alamat Perusahaan"),
              KycItemTile(
                title: "Negara",
                subtitle: "${company?.countryDomicileName ?? '-'}",
              ),
              // KycItemTile(
              //   title: "Jenis Perusahaan",
              //   subtitle: "${company}",
              // ),
              // KycItemTile(
              //   title: "",
              //   subtitle: "${company}",
              // ),
              KycItemTile(
                title: "Provinsi",
                subtitle: "${company?.provinceName ?? '-'}",
              ),
              KycItemTile(
                title: "Kota",
                subtitle: "${company?.regencyName ?? '-'}",
              ),
              KycItemTile(
                title: "Alamat Perusahaan",
                subtitle: "${company?.companyAddress ?? '-'}",
              ),
              KycItemTile(
                title: "Kode POS",
                subtitle: "${company?.postalCode ?? '-'}",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Nomor Fax & Telepon"),
              KycItemTile(
                title: "Nomor Handphone Perusahaan",
                subtitle: "${widget.data?.phone ?? '-'}",
              ),
              KycItemTile(
                title: "Nomor Telepon",
                subtitle: "${company?.companyPhoneNumber ?? '-'}",
              ),
              KycItemTile(
                title: "No. Fax",
                subtitle: "${company?.fax ?? '-'}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
