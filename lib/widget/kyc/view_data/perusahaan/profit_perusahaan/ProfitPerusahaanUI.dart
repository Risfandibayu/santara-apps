import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/profit/ProfitPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewProfitPerusahaanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewProfitPerusahaanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewProfitPerusahaanUIState createState() => _ViewProfitPerusahaanUIState();
}

class _ViewProfitPerusahaanUIState extends State<ViewProfitPerusahaanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  String parseTotalAset(String id) {
    switch (id) {
      case "1":
        return "< Rp 100 Milyar";
        break;
      case "2":
        return "Rp 100 Milyar - Rp 500 Milyar";
        break;
      case "3":
        return "Rp 500 Milyar - Rp 1 Triliun";
        break;
      case "4":
        return "Rp 1 Triliun - Rp 5 Triliun";
        break;
      case "5":
        return "> Rp 5 Triliun";
        break;
      default:
        return "-";
        break;
    }
  }

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Profit Perusahaan",
          steps: "6/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ProfitPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Profit Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Informasi Profit"),
              KycItemTile(
                title: "Profit Operasional 1 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyIncome1)}",
              ),
              KycItemTile(
                title: "Profit Operasional 2 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyIncome2)}",
              ),
              KycItemTile(
                title: "Profit Operasional 3 Tahun Terakhir",
                subtitle: "${parseTotalAset(company?.companyIncome3)}",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Tujuan Investasi"),
              KycItemTile(
                title: "Profit Operasional 1 Tahun Terakhir",
                subtitle: "${company?.reasonToJoin}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
