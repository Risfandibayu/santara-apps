import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/pajak/PajakPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';

class ViewPajakPerizinanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewPajakPerizinanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewPajakPerizinanUIState createState() => _ViewPajakPerizinanUIState();
}

class _ViewPajakPerizinanUIState extends State<ViewPajakPerizinanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Pajak & Perizinan Perusahaan",
          steps: "2/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PajakPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Pajak & Perizinan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              KycTitleWrapper(title: "Informasi Pajak"),
              KycItemTile(
                title: "Kode Akun Pajak",
                subtitle: "${company.taxAccountCompany}",
              ),
              // KycItemTile(
              //   title: "Jenis Perusahaan",
              //   subtitle: "${company}",
              // ),
              // KycItemTile(
              //   title: "",
              //   subtitle: "${company}",
              // ),
              KycItemTile(
                title: "Kode LKPBU",
                subtitle: "${company.lkpub}",
              ),
              KycItemTile(
                title: "NPWP Perusahaan",
                subtitle: "${company.npwp}",
              ),
              KycItemTile(
                title: "Tanggal Registrasi NPWP Perusahaan",
                subtitle: company?.registrationDateNpwp != null
                    ? "${dateFormat.format(DateTime.parse(company.registrationDateNpwp))}"
                    : "-",
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              KycTitleWrapper(title: "Nomor Perizinan Usaha"),
              KycItemTile(
                title: "Nomor SIUP",
                subtitle: "${company.siup}",
              ),
              KycItemTile(
                title: "Nomor Akta Pendirian Usaha",
                subtitle: "${company.companyCertificateNumber}",
              ),
              KycItemTile(
                title: "Nomor NIB Perusahaan",
                subtitle: "${company.nib}",
              ),
              KycItemTile(
                title: "Nomor SID",
                subtitle: "${company?.sidNumber ?? '-'}",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
