import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/dokumen/DokPerusahaanUI.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycCommonWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewDokumenPerusahaanUI extends StatefulWidget {
  final String submissionId;
  final String status;
  final User data;
  ViewDokumenPerusahaanUI({
    @required this.submissionId,
    @required this.status,
    @required this.data,
  });

  @override
  _ViewDokumenPerusahaanUIState createState() =>
      _ViewDokumenPerusahaanUIState();
}

class _ViewDokumenPerusahaanUIState extends State<ViewDokumenPerusahaanUI> {
  final storage = FlutterSecureStorage(); // storage
  bool _showNotification = true;
  String token;
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  getToken() async {
    var userToken = await storage.read(key: 'token');
    setState(() => token = userToken);
  }

  openPdf(String pdf) async {
    try {
      await launch(pdf);
    } catch (e) {
      // print(">> ERROR : $e");
    }
  }

  Widget pdfCard(String title, String file) {
    return file == null
        ? Container()
        : Container(
            margin: EdgeInsets.only(bottom: Sizes.s25),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "$title",
                  style: TextStyle(
                      fontSize: FontSize.s12, fontWeight: FontWeight.w600),
                ),
                Container(
                  margin: EdgeInsets.only(top: Sizes.s10),
                  height: Sizes.s50,
                  padding: EdgeInsets.only(left: 10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xffB8B8B8)),
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "${file.split('/').last}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: FontSize.s12,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () => openPdf(file),
                        child: Container(
                          height: Sizes.s50,
                          width: Sizes.s80,
                          decoration: BoxDecoration(
                              color: Color(0xff6870E1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: Center(
                            child: Text(
                              "Lihat",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s12,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    Trader company = widget.data?.trader;
    return Scaffold(
      appBar: KycViewAppbar(
          title: "Dokumen Perusahaan",
          steps: "7/8",
          onEdit: () {
            if (widget.status != "verifying") {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DokPerusahaanUI(
                    submissionId: widget.submissionId,
                    status: widget.status,
                  ),
                ),
              );
            } else {
              ToastHelper.showBasicToast(
                context,
                "Dokumen Perusahaan anda masih dalam proses verifikasi, anda tidak dapat mengubahnya saat ini!",
              );
            }
          }),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(Sizes.s15),
          child: ListView(
            children: <Widget>[
              _showNotification
                  ? PreviewNotification(
                      onClose: () {
                        setState(() => _showNotification = false);
                      },
                    )
                  : Container(),
              _showNotification
                  ? SizedBox(
                      height: Sizes.s20,
                    )
                  : Container(),
              pdfCard(
                "Dok. Akta Pendirian Usaha",
                company.companyDocument,
              ),
              pdfCard(
                "Dok. Akta Perubahan Terakhir",
                company.companyDocumentChange,
              ),
              pdfCard(
                "Dok. SK Kemenkumham",
                company.documentSkKemenkumham,
              ),
              pdfCard(
                "Dok. NPWP Perusahaan",
                company.documentNpwp,
              ),
              pdfCard(
                "Dok. SIUP/NIB Perusahaan",
                company.siupFile,
              ),
              KycItemTile(
                title: "KTP Direktur Utama/Direksi",
                subtitle: company.idCardDirector != null ? "" : "-",
              ),
              token != null
                  ? Container(
                      height: Sizes.s200,
                      width: double.maxFinite,
                      child: SantaraCachedImage(
                        image: GetAuthenticatedFile.convertUrl(
                          image: company.idCardDirector,
                          type: PathType.traderIdCardPhoto,
                        ),
                        fit: BoxFit.contain,
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
