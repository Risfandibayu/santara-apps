import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/AsetPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/logger.dart';

class AsetPerusahaanBloc extends FormBloc<String, String> {
  final KycPerusahaanService _service = KycPerusahaanService();
  User company = User();
  final String submissionId;
  final String status;

  static var rawFundsSource = [
    SubDataBiodata(id: "1", name: "Business Profit"),
    SubDataBiodata(id: "2", name: "Bunga"),
    SubDataBiodata(id: "3", name: "Dana Pensiun"),
    SubDataBiodata(id: "4", name: "Undian"),
    SubDataBiodata(id: "5", name: "Proceed from investment"),
    SubDataBiodata(id: "6", name: "Deposit"),
    SubDataBiodata(id: "7", name: "Capital"),
    SubDataBiodata(id: "8", name: "Pinjaman"),
    SubDataBiodata(id: "9", name: "Lain-lain"),
  ];

  static var rawAssets = [
    SubDataBiodata(id: "1", name: "< Rp 100 Milyar"),
    SubDataBiodata(id: "2", name: "Rp 100 Milyar - Rp 500 Milyar"),
    SubDataBiodata(id: "3", name: "Rp 500 Milyar - Rp 1 Triliun"),
    SubDataBiodata(id: "4", name: "Rp 1 Triliun - Rp 5 Triliun"),
    SubDataBiodata(id: "5", name: "> Rp 5 Triliun"),
  ];

  final fundsSource =
      MultiSelectFieldBloc<SubDataBiodata, dynamic>(items: rawFundsSource);
  final fundsSourceText = TextFieldBloc();
  final lastYearAsset =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawAssets);
  final twoYearAsset =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawAssets);
  final threeYearAsset =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawAssets);

  AsetPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      fundsSource,
      lastYearAsset,
      twoYearAsset,
      threeYearAsset,
      fundsSourceText,
    ]);
    updateCompanyData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "5").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // Update value setiap fields
      // sumber dana
      if (company.trader.sourceOfFunds != null) {
        List<SubDataBiodata> datas = List<SubDataBiodata>();
        var companyFundsSource = company.trader.sourceOfFunds.split(',');
        companyFundsSource.forEach((element) {
          fundsSource.state.items.forEach((val) {
            if (element.toLowerCase() == val.name.toLowerCase()) {
              datas.add(val);
            }
          });
        });
        fundsSource.updateInitialValue(datas);

        if (company.trader.sourceOfFunds.toLowerCase().contains("lain")) {
          addFieldBloc(fieldBloc: fundsSourceText);
          // if (company.trader.fundsSourceText != null) {
          List<String> sourceOfFundsValue =
              company.trader.sourceOfFunds.split(",");
          fundsSourceText.updateInitialValue("${sourceOfFundsValue.last}");
          // print(">> HELLO WORLD!");
          // }
        }
      }

      // total assets
      // 1 tahun terakhir
      if (company.trader.companyTotalProperty1 != null) {
        var selected = lastYearAsset.state.items.where(
            (element) => element.id == company.trader.companyTotalProperty1);
        if (selected != null) lastYearAsset.updateInitialValue(selected.first);
      }
      // 2 tahun terakhir
      if (company.trader.companyTotalProperty2 != null) {
        var selected = twoYearAsset.state.items.where(
            (element) => element.id == company.trader.companyTotalProperty2);
        if (selected != null) twoYearAsset.updateInitialValue(selected.first);
      }
      // 3 tahun terakhir
      if (company.trader.companyTotalProperty3 != null) {
        var selected = threeYearAsset.state.items.where(
            (element) => element.id == company.trader.companyTotalProperty2);
        if (selected != null) threeYearAsset.updateInitialValue(selected.first);
      }

      if (status == "rejected") {
        await getFieldErrorStatus();
      }
      emitLoaded();
    } catch (e) {
      emitLoaded();
    }
  }

  onFieldsChange() {
    detectChanges(fundsSource);
    detectChanges(lastYearAsset);
    detectChanges(twoYearAsset);
    detectChanges(threeYearAsset);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      AsetPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // validators
  bool isFundSourceError() {
    if (fundsSource.value == null) {
      fundsSource.addFieldError("Sumber dana wajib diisi!");
      return true;
    } else if (fundsSource.value.length < 1) {
      fundsSource.addFieldError("Sumber dana wajib diisi!");
      return true;
    } else {
      // print(fundsSource.value.length);
      return false;
    }
  }

  bool isFundSourceTextError() {
    try {
      bool check = false;
      fundsSource.value.forEach((element) {
        if (element.id == "9") {
          check = true;
        }
      });
      if (check) {
        if (fundsSourceText.value.length < 1) {
          fundsSourceText.addFieldError("Keterangan Wajib Diisi!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      fundsSourceText.addFieldError("Keterangan Wajib Diisi!");
      return true;
    }
  }

  bool isLastYearAssetsError() {
    if (lastYearAsset.value == null) {
      lastYearAsset
          .addFieldError("Total aset 1 tahun terakhir tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }
  //

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      // jika yg error field name
      case "company_source_of_funds":
        addErrorMessage(fundsSource, _errMsg, "company_source_of_funds");
        break;
      case "source_of_funds":
        addErrorMessage(fundsSource, _errMsg, "company_source_of_funds");
        break;
      case "company_total_property1":
        addErrorMessage(lastYearAsset, _errMsg, "company_total_property1");
        break;
      case "company_total_property2":
        addErrorMessage(twoYearAsset, _errMsg, "company_total_property2");
        break;
      case "company_total_property3":
        addErrorMessage(threeYearAsset, _errMsg, "company_total_property3");
        break;
      default:
        emitFailure(
            failureResponse:
                "ERRORSUBMIT Tidak dapat mengirim data, cek kembali form anda!");
        break;
    }
  }

  @override
  void onSubmitting() async {
    try {
      var dana = StringBuffer();
      fundsSource.value.forEach((val) {
        // sumberDana += "${val.name},";
        dana.write(val.name + ",");
      });

      if (isFundSourceError()) {
        emitFailure(failureResponse: "company_source_of_funds");
      } else if (isLastYearAssetsError()) {
        emitFailure(failureResponse: "company_total_property1");
      } else if (isFundSourceTextError()) {
        // print("HOHOHOH");
        emitFailure(failureResponse: "funds_source_text");
      } else {
        AsetPerusahaanData data = AsetPerusahaanData(
          fundsSource: dana.toString().substring(0, dana.toString().length - 1),
          fundsSourceText: "${fundsSourceText?.value ?? ''}",
          lastYearAsset: "${lastYearAsset?.value?.id ?? ''}",
          twoYearAsset:
              twoYearAsset?.value == null ? null : twoYearAsset.value.id,
          threeYearAsset:
              threeYearAsset?.value == null ? null : threeYearAsset.value.id,
        );
        await _service.postAsetPerusahaan(data, status).then((value) {
          if (value.statusCode == 200) {
            emitSuccess(
              canSubmitAgain: false,
              successResponse: "Berhasil menyimpan Aset Perusahaan",
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else if (value.statusCode == 400) {
            try {
              value.data.forEach((res) {
                var dummy = ValidationResultModel.fromJson(res);
                handleApiValidation(dummy.field, dummy.message);
              });
            } catch (e) {
              emitFailure(
                failureResponse: "ERRORSUBMIT ${value.data['message']}",
              );
            }
          } else {
            // jika status code bukan 200 / 400
            emitFailure(
              failureResponse:
                  "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
            );
          }
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      emitFailure(
        failureResponse: "ERRORSUBMIT Error saat mencoba mengirimkan data!",
      );
    }
  }
}
