import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/SubData.dart';
import '../../../../models/kyc/company/AsetPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/aset.perusahaan.bloc.dart';

class AsetPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  AsetPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (AsetPerusahaanHelper.dokumen.isEdited != null) {
            if (AsetPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                AsetPerusahaanHelper.dokumen = AsetPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              AsetPerusahaanHelper.dokumen = AsetPerusahaanData();
              return true;
            }
          } else {
            AsetPerusahaanHelper.dokumen = AsetPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Aset Perusahaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "5/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => AsetPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: AsetPerusahaanForm(),
          )),
    );
  }
}

class AsetPerusahaanForm extends StatefulWidget {
  @override
  _AsetPerusahaanFormState createState() => _AsetPerusahaanFormState();
}

class _AsetPerusahaanFormState extends State<AsetPerusahaanForm> {
  AsetPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    AsetPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<AsetPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<AsetPerusahaanBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, "${state.successResponse}");
            Navigator.pop(context);
            AsetPerusahaanHelper.dokumen = AsetPerusahaanData();
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            if (AsetPerusahaanHelper.dokumen.isSubmitted != null) {
              Navigator.pop(context);
            }

            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
            }

            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              switch (state.failureResponse.toLowerCase()) {
                case "company_source_of_funds":
                  fieldWrapper.scrollTo(1);
                  break;
                case "funds_source_text":
                  fieldWrapper.scrollTo(2);
                  break;
                case "company_total_property1":
                  fieldWrapper.scrollTo(3);
                  break;
                case "company_total_property2":
                  fieldWrapper.scrollTo(4);
                  break;
                case "company_total_property3":
                  fieldWrapper.scrollTo(5);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<AsetPerusahaanBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                if (state is FormBlocLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is FormBlocLoadFailed) {
                  return Center(
                    child: Text("Failed to load!"),
                  );
                } else {
                  return Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Colors.white,
                      child: ListView(
                          scrollDirection: scrollDirection,
                          padding: EdgeInsets.all(Sizes.s20),
                          controller: controller,
                          children: [
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Keterangan Aset",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            Text(
                              "Sumber Dana",
                              style: TextStyle(
                                fontSize: FontSize.s15,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "(Dapat memilih lebih dari satu)",
                              style: TextStyle(
                                fontSize: FontSize.s12,
                                color: Color(0xff858585),
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              1,
                              Sizes.s450,
                              SingleChildScrollView(
                                physics: NeverScrollableScrollPhysics(),
                                child: CheckboxGroupFieldBlocBuilder<
                                    SubDataBiodata>(
                                  isEnabled: KycHelpers.fieldCheck(bloc.status),
                                  padding: EdgeInsets.all(0),
                                  multiSelectFieldBloc: bloc.fundsSource,
                                  itemBuilder: (context, item) => item.name,
                                  decoration: InputDecoration(
                                    errorMaxLines: 5,
                                    labelText: '',
                                    prefixIcon: SizedBox(),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                            // KycErrorWrapper(bloc: bloc.fundsSource),
                            BlocBuilder<MultiSelectFieldBloc,
                                    MultiSelectFieldBlocState>(
                                bloc: bloc.fundsSource,
                                builder: (context, state) {
                                  if (state.hasValue) {
                                    Widget widget = Container();
                                    for (var i = 0;
                                        i < state.value.length;
                                        i++) {
                                      if (state.value[i].id == "9") {
                                        widget = fieldWrapper.viewWidget(
                                          2,
                                          Sizes.s100,
                                          SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.fundsSourceText,
                                            hintText: "Keterangan Lain-lain",
                                            labelText: "Keterangan Lain-lain",
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.always,
                                          ),
                                        );
                                      } else {
                                        var res = state.value.where(
                                            (element) => element.id == "9");
                                        if (res != null && res.length > 0) {
                                        } else {
                                          widget = Container();
                                        }
                                      }
                                    }
                                    return widget;
                                  } else {
                                    return Container();
                                  }
                                }),
                            // divider,
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Total Aset",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s30,
                            ),
                            fieldWrapper.viewWidget(
                              3,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                selectFieldBloc: bloc.lastYearAsset,
                                label: "Total Aset 1 Tahun terakhir",
                                hintText: "< Rp 100 Milyar",
                                enabled: KycHelpers.fieldCheck(bloc.status),
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              4,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                selectFieldBloc: bloc.twoYearAsset,
                                label: "Total Aset 2 Tahun terakhir",
                                hintText:
                                    "*Kosongkan jika operasional <2 Tahun ",
                                enabled: KycHelpers.fieldCheck(bloc.status),
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              5,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                selectFieldBloc: bloc.threeYearAsset,
                                label: "Total Aset 3 Tahun terakhir",
                                hintText:
                                    "*Kosongkan jika operasional <3 Tahun ",
                                enabled: KycHelpers.fieldCheck(bloc.status),
                              ),
                            ),
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            !KycHelpers.fieldCheck(bloc.status)
                                ? SantaraDisabledButton()
                                : SantaraMainButton(
                                    title: "Simpan",
                                    onPressed: () {
                                      AsetPerusahaanHelper.dokumen.isSubmitted =
                                          true;
                                      FocusScope.of(context).unfocus();
                                      bloc.submit();
                                    },
                                  )
                          ]));
                }
              })),
    );
  }
}
