import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/PajakPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';

class PajakPerusahaanBloc extends FormBloc<String, String> {
  User company = User();
  KycPerusahaanService _service = KycPerusahaanService();
  final String submissionId;
  final String status;

  // value Kode Akun Pajak untuk dropdown
  static var rawTaxIdCorp = [
    SubDataBiodata(id: "1016", name: "1016 - Asabri"),
    SubDataBiodata(id: "1018", name: "1018 - Badan Usaha Tetap"),
    SubDataBiodata(id: "1077", name: "1077 - Badan Usaha Tetap Khusus Non Tax"),
    SubDataBiodata(id: "1008", name: "1008 - Bank - Domestik"),
    SubDataBiodata(id: "1009", name: "1009 - Bank -  Foreign (Joint Venture)"),
    SubDataBiodata(id: "1036", name: "1036 - Bank - Pure Foreign"),
    SubDataBiodata(id: "1001", name: "1001 - Broker (Local Broker)"),
    SubDataBiodata(id: "1002", name: "1002 - Custodian Bank"),
    SubDataBiodata(id: "1067", name: "1067 - Financial Institution"),
    SubDataBiodata(id: "1078", name: "1078 - Government of Indonesia"),
    SubDataBiodata(id: "1035", name: "1035 - Institution - Domestic (Non FI)"),
    SubDataBiodata(id: "1017", name: "1017 - Institution - Foreign (Non Fi)"),
    SubDataBiodata(id: "1004", name: "1004 - Institution Foreign No Tax"),
    SubDataBiodata(id: "1100", name: "1100 - Insurance Non NPWP"),
    SubDataBiodata(id: "1019", name: "1019 - Insurance NPWP"),
    SubDataBiodata(id: "1005", name: "1005 - Issuer"),
    SubDataBiodata(id: "1015", name: "1015 - Jamsostek JHT"),
    SubDataBiodata(id: "1082", name: "1082 - Jamsostek Non JHT"),
    SubDataBiodata(id: "1013", name: "1013 - Koperasi"),
    SubDataBiodata(id: "1006", name: "1006 - Mutual Fund"),
    SubDataBiodata(id: "1080", name: "1080 - Mutual Fund - More then 5 years"),
    SubDataBiodata(id: "1007", name: "1007 - Pension Fund"),
    SubDataBiodata(id: "1098", name: "1098 - Perusahaan Terbatas Non NPWP"),
    SubDataBiodata(id: "1037", name: "1037 - Perusahaan Terbatas NPWP"),
    SubDataBiodata(id: "1014", name: "1014 - Taspen"),
    SubDataBiodata(id: "1099", name: "1099 - Yayasan Non NPWP"),
    SubDataBiodata(id: "1012", name: "1012 - Yayasan NPWP"),
  ];

  // value LKPBU Code untuk dropdown
  static var rawLkpbuCorp = [
    SubDataBiodata(id: "001", name: "BANK INDONESIA"),
    SubDataBiodata(id: "100", name: "PEMERINTAH PUSAT"),
    SubDataBiodata(id: "200", name: "BANK SENTRAL DI LN"),
    SubDataBiodata(id: "290", name: "BANK UMUM DI INDONESIA"),
    SubDataBiodata(id: "299", name: "BANK UMUM DI LUAR NEGERI"),
    SubDataBiodata(id: "310", name: "PERUSAHAAN PEMBIAYAAN"),
    SubDataBiodata(id: "320", name: "MODAL VENTURA"),
    SubDataBiodata(id: "330", name: "PERUSAHAAN SEKURITAS"),
    SubDataBiodata(id: "340", name: "PERUSAHAAN ASURANSI"),
    SubDataBiodata(id: "350", name: "DANA PENSIUN"),
    SubDataBiodata(id: "360", name: "REKSADANA"),
    SubDataBiodata(id: "410", name: "BUMN NON LEMBAGA KEUANGAN"),
    SubDataBiodata(id: "430", name: "PERUSAHAAN LAINNYA"),
    SubDataBiodata(id: "XXX", name: "PERUSAHAAN ASING"),
    SubDataBiodata(id: "999", name: "LAINNYA"),
  ];

  // value bulan untuk dropdown
  static var rawMonths = [
    SubDataBiodata(id: "01", name: "Januari"),
    SubDataBiodata(id: "02", name: "Februari"),
    SubDataBiodata(id: "03", name: "Maret"),
    SubDataBiodata(id: "04", name: "April"),
    SubDataBiodata(id: "05", name: "Mei"),
    SubDataBiodata(id: "06", name: "Juni"),
    SubDataBiodata(id: "07", name: "Juli"),
    SubDataBiodata(id: "08", name: "Agustus"),
    SubDataBiodata(id: "09", name: "September"),
    SubDataBiodata(id: "10", name: "Oktober"),
    SubDataBiodata(id: "11", name: "November"),
    SubDataBiodata(id: "12", name: "Desember"),
  ];

  final taxIdCorp = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawTaxIdCorp,
  );
  final lkpbuCode = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawLkpbuCorp,
  );
  final npwp = TextFieldBloc();
  final dayNpwpReg = TextFieldBloc();
  final monthNpwpReg = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawMonths,
  );
  final yearNpwpReg = TextFieldBloc();
  final npwpRegistrationDate = TextFieldBloc();
  final bussinessRegCert = TextFieldBloc();
  final articleOfAssc = TextFieldBloc();
  final investorCompanyBICode = TextFieldBloc();

  PajakPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      taxIdCorp,
      lkpbuCode,
      npwp,
      dayNpwpReg,
      monthNpwpReg,
      yearNpwpReg,
      bussinessRegCert,
      articleOfAssc,
      investorCompanyBICode,
    ]);
    // no return value
    updateCompanyData().then((x) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "2").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // kode akun pajak
      if (company.trader.taxAccountCode != null) {
        var selectedChars = taxIdCorp.state.items
            .where(
                (element) => element.id == "${company.trader.taxAccountCode}")
            .first;
        taxIdCorp.updateInitialValue(selectedChars);
      }

      // kode lkpbu
      if (company.trader.lkpubCode != null) {
        var selectedLkpbu = lkpbuCode.state.items
            .where((element) => element.id == "${company.trader.lkpubCode}")
            .first;
        lkpbuCode.updateInitialValue(selectedLkpbu);
      }

      // npwp perusahaan
      npwp.updateInitialValue(company.trader.npwp);
      // tanggal registrasi npwp
      if (company.trader.registrationDateNpwp != null) {
        var tglReg = DateTime.parse(company.trader.registrationDateNpwp);
        dayNpwpReg.updateInitialValue(
            "${tglReg.day < 10 ? '0' + tglReg.day.toString() : tglReg.day}");
        monthNpwpReg
            .updateInitialValue(monthNpwpReg.state.items[tglReg.month - 1]);
        yearNpwpReg.updateInitialValue(tglReg.year.toString());
      }
      // nomor siup
      bussinessRegCert.updateInitialValue(company.trader.siup);
      // nomor akta pendirian usaha
      articleOfAssc.updateInitialValue(company.trader.companyCertificateNumber);
      // nomor nib perusahaan
      investorCompanyBICode.updateInitialValue(company.trader.nib);
      //
      if (status == "rejected") {
        await getFieldErrorStatus();
      }
      emitLoaded();
    } catch (e, stack) {
      emitLoaded();
    }
  }

  onFieldsChange() {
    detectChanges(taxIdCorp);
    detectChanges(lkpbuCode);
    detectChanges(npwp);
    detectChanges(dayNpwpReg);
    detectChanges(monthNpwpReg);
    detectChanges(yearNpwpReg);
    detectChanges(bussinessRegCert);
    detectChanges(articleOfAssc);
    detectChanges(investorCompanyBICode);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      // print(">> Changed");
      PajakPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  bool isTaxIdError() {
    if (taxIdCorp.value == null) {
      taxIdCorp.addFieldError("Kode akun pajak tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isLkpbuError() {
    if (lkpbuCode.value == null) {
      lkpbuCode.addFieldError("Kode LKPBU tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isNpwpError() {
    if (company.trader.companyCountryDomicile != null &&
        company.trader.companyCountryDomicile == 78) {
      return KycHelpers.npwpChecker(npwp, true);
    } else {
      return false;
    }
  }

  bool isBussinessRegCertError() {
    if (company.trader.companyCountryDomicile != null &&
        company.trader.companyCountryDomicile == 78) {
      if (bussinessRegCert.value != null && bussinessRegCert.value.length > 0) {
        if (bussinessRegCert.value.length > 20) {
          bussinessRegCert.addFieldError("Nomor SIUP maksimal 20 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        bussinessRegCert.addFieldError("Nomor SIUP tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isArticleOfAsscError() {
    if (articleOfAssc.value != null && articleOfAssc.value.length > 0) {
      if (articleOfAssc.value.length > 20) {
        articleOfAssc
            .addFieldError("Nomor akta pendirian usaha maksimal 20 karakter!");
        return true;
      } else {
        return false;
      }
    } else {
      articleOfAssc
          .addFieldError("Nomor akta pendirian usaha tidak boleh kosong!");
      return true;
    }
  }

  bool isDateNpwpNotEmpty() {
    /// jika user mengisi tanggal / bulan / tahun
    /// lanjutkan validasi tanggal, jika user tidak mengisi, tidak usah cek tanggalnya
    try {
      if (dayNpwpReg.value == null ||
          monthNpwpReg.value == null ||
          yearNpwpReg.value == null) {
        return false;
      } else {
        return KycHelpers.dateChecker(
            dayNpwpReg, monthNpwpReg, yearNpwpReg, npwpRegistrationDate, true);
      }
    } catch (e) {
      return KycHelpers.dateChecker(
          dayNpwpReg, monthNpwpReg, yearNpwpReg, npwpRegistrationDate, true);
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      case "company_tax_account_code":
        addErrorMessage(taxIdCorp, _errMsg, "tax_account_code");
        break;
      case "tax_account_code":
        addErrorMessage(taxIdCorp, _errMsg, "tax_account_code");
        break;
      case "lkpub_code":
        addErrorMessage(lkpbuCode, _errMsg, "lkpub_code");
        break;
      case "company_lkpub_code":
        addErrorMessage(lkpbuCode, _errMsg, "lkpub_code");
        break;
      case "npwp":
        addErrorMessage(npwp, _errMsg, "npwp");
        break;
      case "company_npwp":
        addErrorMessage(npwp, _errMsg, "npwp");
        break;
      case "registration_date_npwp":
        addErrorMessage(
            npwpRegistrationDate, _errMsg, "registration_date_npwp");
        break;
      case "company_registration_date_npwp":
        addErrorMessage(
            npwpRegistrationDate, _errMsg, "registration_date_npwp");
        break;
      case "siup":
        addErrorMessage(bussinessRegCert, _errMsg, "siup");
        break;
      case "company_siup":
        addErrorMessage(bussinessRegCert, _errMsg, "siup");
        break;
      case "nib":
        addErrorMessage(investorCompanyBICode, _errMsg, "nib");
        break;
      case "company_nib":
        addErrorMessage(investorCompanyBICode, _errMsg, "nib");
        break;
      case "company_certificate_number":
        addErrorMessage(articleOfAssc, _errMsg, "article_of_assc");
        break;
      case "company_certificare_number":
        addErrorMessage(articleOfAssc, _errMsg, "article_of_assc");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT $_errMsg");
        break;
    }
  }

  @override
  void onLoading() {
    // emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      if (isTaxIdError()) {
        emitFailure(failureResponse: "tax_account_code");
      } else if (isLkpbuError()) {
        emitFailure(failureResponse: "lkpub_code");
      } else if (isNpwpError()) {
        emitFailure(failureResponse: "npwp");
      } else if (isDateNpwpNotEmpty()) {
        emitFailure(failureResponse: "registration_date_npwp");
      } else if (isBussinessRegCertError()) {
        emitFailure(failureResponse: "siup");
      } else if (isArticleOfAsscError()) {
        emitFailure(failureResponse: "article_of_assc");
      } else {
        PajakPerusahaanData data = PajakPerusahaanData(
          taxIdCorp: taxIdCorp?.value?.id ?? '',
          lkpbuCode: lkpbuCode?.value?.id ?? '',
          npwp: npwp?.value ?? '',
          npwpRegDate: yearNpwpReg.value != null &&
                  monthNpwpReg.value != null &&
                  dayNpwpReg.value != null
              ? "${yearNpwpReg.value}-${monthNpwpReg.value.id}-${dayNpwpReg.value}"
              : "",
          bussinessRegCert: bussinessRegCert?.value ?? '',
          articleOfAssc: articleOfAssc?.value ?? '',
          investorCompanyBICode: investorCompanyBICode?.value ?? '',
        );
        await _service.postPajakPerizinan(data, status).then((value) {
          if (value.statusCode == 200) {
            emitSuccess(
              canSubmitAgain: false,
              successResponse: "Berhasil menyimpan pajak perizinan",
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else if (value.statusCode == 400) {
            try {
              value.data.forEach((res) {
                var dummy = ValidationResultModel.fromJson(res);
                handleApiValidation(dummy.field, dummy.message);
              });
            } catch (e) {
              // print(e);
              emitFailure(
                failureResponse: "ERRORSUBMIT ${value.data['message']}",
              );
            }
          } else {
            // jika status code bukan 200 / 400
            emitFailure(
              failureResponse:
                  "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
            );
          }
        });
      }
    } catch (e, stack) {
      // print(e);
      // print(stack);
      emitFailure(
        failureResponse:
            "ERRORSUBMIT Terjadi kesalahan saat mencoba mengirim data!",
      );
    }
  }
}
