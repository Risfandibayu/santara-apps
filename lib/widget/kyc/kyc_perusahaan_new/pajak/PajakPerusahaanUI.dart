import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/company/PajakPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/pajak.perusahaan.bloc.dart';

class PajakPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;

  PajakPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (PajakPerusahaanHelper.dokumen.isEdited != null) {
            if (PajakPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                PajakPerusahaanHelper.dokumen = PajakPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              PajakPerusahaanHelper.dokumen = PajakPerusahaanData();
              return true;
            }
          } else {
            PajakPerusahaanHelper.dokumen = PajakPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Pajak dan Perizinan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "2/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => PajakPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: PajakPerusahaanForm(),
          )),
    );
  }
}

class PajakPerusahaanForm extends StatefulWidget {
  @override
  _PajakPerusahaanFormState createState() => _PajakPerusahaanFormState();
}

class _PajakPerusahaanFormState extends State<PajakPerusahaanForm> {
  PajakPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    PajakPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<PajakPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<PajakPerusahaanBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(context, "${state.successResponse}");
          Navigator.pop(context);
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          // print(">> Is Submitted : ${PajakPerusahaanHelper.dokumen.isSubmitted}");
          if (PajakPerusahaanHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }

          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
            return;
          }

          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse.toLowerCase()) {
              case "tax_account_code":
                fieldWrapper.scrollTo(1);
                break;
              case "lkpub_code":
                fieldWrapper.scrollTo(2);
                break;
              case "npwp":
                fieldWrapper.scrollTo(3);
                break;
              case "registration_date_npwp":
                fieldWrapper.scrollTo(4);
                break;
              case "siup":
                fieldWrapper.scrollTo(5);
                break;
              case "article_of_assc":
                fieldWrapper.scrollTo(6);
                break;
              case "nib":
                fieldWrapper.scrollTo(7);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<PajakPerusahaanBloc, FormBlocState>(
            buildWhen: (previous, current) =>
                previous.runtimeType != current.runtimeType ||
                previous is FormBlocLoading && current is FormBlocLoading,
            builder: (context, state) {
              if (state is FormBlocLoading) {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (state is FormBlocLoadFailed) {
                return Center(
                  child: Text("Failed to load!"),
                );
              } else {
                return Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    color: Colors.white,
                    child: ListView(
                        scrollDirection: scrollDirection,
                        padding: EdgeInsets.all(Sizes.s20),
                        controller: controller,
                        children: [
                          // divider
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          // data diri title
                          Text(
                            "Informasi Pajak",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.s16,
                            ),
                          ),
                          // divider line
                          Container(
                            margin: EdgeInsets.only(top: Sizes.s10),
                            height: 4,
                            width: double.maxFinite,
                            color: Color(0xffF4F4F4),
                          ),
                          // divider
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          fieldWrapper.viewWidget(
                            1,
                            Sizes.s100,
                            SantaraBlocDropDown(
                              selectFieldBloc: bloc.taxIdCorp,
                              label: "Kode Akun Pajak",
                              hintText: "Pilih",
                              enabled: KycHelpers.fieldCheck(bloc.status),
                            ),
                          ),
                          fieldWrapper.viewWidget(
                            2,
                            Sizes.s100,
                            SantaraBlocDropDown(
                              selectFieldBloc: bloc.lkpbuCode,
                              label: "Kode LKPBU",
                              hintText: "Pilih",
                              enabled: KycHelpers.fieldCheck(bloc.status),
                            ),
                          ),
                          fieldWrapper.viewWidget(
                            3,
                            Sizes.s100,
                            SantaraBlocTextField(
                              enabled: KycHelpers.isVerified(bloc.status)
                                  ? KycHelpers.fieldCheck(bloc.status)
                                  : false,
                              textFieldBloc: bloc.npwp,
                              hintText: "",
                              labelText: "NPWP Perusahaan",
                              inputType: TextInputType.number,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(20),
                              ],
                            ),
                          ),
                          Text(
                            "Tanggal Registrasi NPWP Perusahaan",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          fieldWrapper.viewWidget(
                            4,
                            Sizes.s140,
                            Padding(
                              padding: EdgeInsets.only(top: Sizes.s5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          width: Sizes.s100,
                                          height: Sizes.s100,
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.isVerified(
                                                    bloc.status)
                                                ? KycHelpers.fieldCheck(
                                                    bloc.status)
                                                : false,
                                            textFieldBloc: bloc.dayNpwpReg,
                                            hintText: "01",
                                            labelText: "Tanggal",
                                            inputType: TextInputType.datetime,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  2),
                                              FilteringTextInputFormatter.allow(
                                                RegExp(r"[0-9]*$"),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          height: Sizes.s100,
                                          width: Sizes.s150,
                                          margin: EdgeInsets.only(
                                            left: Sizes.s10,
                                            right: Sizes.s10,
                                          ),
                                          child: SantaraBlocDropDown(
                                            enabled: KycHelpers.isVerified(
                                                    bloc.status)
                                                ? KycHelpers.fieldCheck(
                                                    bloc.status)
                                                : false,
                                            selectFieldBloc: bloc.monthNpwpReg,
                                            label: "Bulan",
                                            hintText: "Januari",
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          height: Sizes.s100,
                                          width: Sizes.s100,
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.isVerified(
                                                    bloc.status)
                                                ? KycHelpers.fieldCheck(
                                                    bloc.status)
                                                : false,
                                            textFieldBloc: bloc.yearNpwpReg,
                                            hintText: "2020",
                                            labelText: "Tahun",
                                            inputType: TextInputType.datetime,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  4),
                                              FilteringTextInputFormatter.allow(
                                                RegExp(r"[0-9]*$"),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  KycErrorWrapper(
                                    bloc: bloc.npwpRegistrationDate,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // divider
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          // data diri title
                          Text(
                            "Nomor Perizinan Usaha",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.s16,
                            ),
                          ),
                          // divider line
                          Container(
                            margin: EdgeInsets.only(top: Sizes.s10),
                            height: 4,
                            width: double.maxFinite,
                            color: Color(0xffF4F4F4),
                          ),
                          // divider
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          fieldWrapper.viewWidget(
                            5,
                            Sizes.s100,
                            SantaraBlocTextField(
                              enabled: KycHelpers.isVerified(bloc.status)
                                  ? KycHelpers.fieldCheck(bloc.status)
                                  : false,
                              textFieldBloc: bloc.bussinessRegCert,
                              hintText: "",
                              labelText: "Nomor SIUP",
                            ),
                          ),
                          fieldWrapper.viewWidget(
                            6,
                            Sizes.s100,
                            SantaraBlocTextField(
                              enabled: KycHelpers.isVerified(bloc.status)
                                  ? KycHelpers.fieldCheck(bloc.status)
                                  : false,
                              textFieldBloc: bloc.articleOfAssc,
                              hintText: "",
                              labelText: "Nomor Akta Pendirian Usaha",
                            ),
                          ),
                          fieldWrapper.viewWidget(
                            7,
                            Sizes.s100,
                            SantaraBlocTextField(
                              enabled: KycHelpers.fieldCheck(bloc.status),
                              textFieldBloc: bloc.investorCompanyBICode,
                              hintText: "*Kosongkan jika tidak ada (Optional)",
                              labelText: "Nomor NIB Perusahaan",
                            ),
                          ),
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          !KycHelpers.fieldCheck(bloc.status)
                              ? SantaraDisabledButton()
                              : SantaraMainButton(
                                  title: "Simpan",
                                  onPressed: () {
                                    PajakPerusahaanHelper.dokumen.isSubmitted =
                                        true;
                                    FocusScope.of(context).unfocus();
                                    bloc.submit();
                                  },
                                )
                        ]));
              }
            }),
      ),
    );
  }
}
