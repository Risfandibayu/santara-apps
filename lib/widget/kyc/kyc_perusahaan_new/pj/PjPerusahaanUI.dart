import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/company/PjPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/pj.perusahaan.bloc.dart';

class PjPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  PjPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (PjPerusahaanHelper.dokumen.isEdited != null) {
            if (PjPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                PjPerusahaanHelper.dokumen = PjPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              PjPerusahaanHelper.dokumen = PjPerusahaanData();
              return true;
            }
          } else {
            PjPerusahaanHelper.dokumen = PjPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Penanggung Jawab",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "4/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => PjPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: PjPerusahaanForm(),
          )),
    );
  }
}

class PjPerusahaanForm extends StatefulWidget {
  @override
  _PjPerusahaanFormState createState() => _PjPerusahaanFormState();
}

class _PjPerusahaanFormState extends State<PjPerusahaanForm> {
  PjPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    PjPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<PjPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return KycHideKeyboard(
      child: FormBlocListener<PjPerusahaanBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, "${state.successResponse}");
            Navigator.pop(context);
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            // print("${PjPerusahaanHelper.dokumen.isSubmitted}");
            if (PjPerusahaanHelper.dokumen.isSubmitted != null) {
              Navigator.pop(context);
            }

            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
            }

            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              switch (state.failureResponse.toLowerCase()) {
                case "company_responsible_name1":
                case "company_responsible_position1":
                case "company_responsible_idcard_number1":
                case "company_responsible_npwp1":
                case "company_responsible_passport1":
                case "company_responsible_expired_date_idcard1":
                case "company_responsible_expired_date_passport1":
                  fieldWrapper.scrollTo(0);
                  break;
                case "company_responsible_expired_date_idcard2":
                case "company_responsible_expired_date_passport2":
                  fieldWrapper.scrollTo(1);
                  break;
                case "company_responsible_expired_date_idcard3":
                case "company_responsible_expired_date_passport3":
                  fieldWrapper.scrollTo(2);
                  break;
                case "company_responsible_expired_date_idcard4":
                case "company_responsible_expired_date_passport4":
                  fieldWrapper.scrollTo(3);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<PjPerusahaanBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                if (state is FormBlocLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is FormBlocLoadFailed) {
                  return Center(
                    child: Text("Failed to load!"),
                  );
                } else {
                  return Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Colors.white,
                      child: ListView(
                          // shrinkWrap: true,
                          scrollDirection: scrollDirection,
                          padding: EdgeInsets.symmetric(vertical: Sizes.s20),
                          controller: controller,
                          children: [
                            // pj 1
                            fieldWrapper.viewWidget(
                              0,
                              null,
                              Theme(
                                data: theme,
                                child: ExpansionTile(
                                  backgroundColor: Colors.transparent,
                                  initiallyExpanded: true,
                                  title: Text(
                                    "Penanggung Jawab Perusahaan",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: FontSize.s16,
                                    ),
                                  ),
                                  subtitle: Container(
                                    margin: EdgeInsets.only(top: Sizes.s5),
                                    color: Color(0xffF4F4F4),
                                    height: 3,
                                    width: double.maxFinite,
                                  ),
                                  children: [
                                    // nama
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.nama1,
                                            inputType: TextInputType.text,
                                            hintText: "Contoh : Putri Aisyah",
                                            labelText: "Nama Lengkap"),
                                      ),
                                    ),
                                    // jabatan
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.jabatan1,
                                            inputType: TextInputType.text,
                                            hintText:
                                                "*Jabatan sesuai dengan Akta Pendirian Usaha",
                                            labelText: "Jabatan"),
                                      ),
                                    ),
                                    // nomor ktp
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                          enabled: KycHelpers.fieldCheck(
                                              bloc.status),
                                          textFieldBloc: bloc.noKtp1,
                                          inputType: TextInputType.number,
                                          hintText:
                                              "*Kosongkan jika tidak ada (Optional)",
                                          labelText: "Nomor KTP",
                                          inputFormatters: [
                                            LengthLimitingTextInputFormatter(
                                                16),
                                          ],
                                        ),
                                      ),
                                    ),
                                    // title kadaluarsa ktp
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: Text(
                                        "Tanggal Kadaluarsa KTP",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    // radio ktp valid until 1
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SantaraBlocRadioGroup(
                                          enabled: KycHelpers.fieldCheck(
                                              bloc.status),
                                          selectFieldBloc: bloc.ktpValidUntil1),
                                    ),
                                    // tgl kadaluarsa ktp
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child:
                                          BlocBuilder<SelectFieldBloc, dynamic>(
                                        bloc: bloc.ktpValidUntil1,
                                        builder: (context, state) {
                                          if (state.value != null) {
                                            if (state.value.id == "2") {
                                              return Padding(
                                                padding: EdgeInsets.only(
                                                    top: Sizes.s10),
                                                child: KycDatePicker(
                                                  enabled:
                                                      KycHelpers.fieldCheck(
                                                    bloc.status,
                                                  ),
                                                  day: bloc.dayKtp1,
                                                  month: bloc.monthKtp1,
                                                  year: bloc.yearKtp1,
                                                ),
                                              );
                                            } else {
                                              return Container();
                                            }
                                          } else {
                                            return Container();
                                          }
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: KycErrorWrapper(
                                          bloc: bloc.ktpExpDate1),
                                    ),
                                    // no npwp
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                          enabled: KycHelpers.fieldCheck(
                                              bloc.status),
                                          textFieldBloc: bloc.npwp1,
                                          inputType: TextInputType.number,
                                          inputFormatters: [
                                            LengthLimitingTextInputFormatter(
                                                20),
                                          ],
                                          hintText:
                                              "*Kosongkan jika tidak ada (Optional)",
                                          labelText:
                                              "Nomor NPWP Penanggung Jawab",
                                        ),
                                      ),
                                    ),
                                    // nomor passport
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.noPassport1,
                                            inputType: TextInputType.text,
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText: "Nomor Passport"),
                                      ),
                                    ),
                                    // title tgl kadaluarsa passport
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: Text(
                                        "Tanggal Kadaluarsa Passport ",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                    // subtitle tgl kadaluarsa passport
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: Text(
                                        "*Kosongkan jika field Passport kosong",
                                        style: TextStyle(
                                          fontSize: FontSize.s10,
                                          color: Color(0xffB8B8B8),
                                        ),
                                      ),
                                    ),
                                    // tgl kadaluarsa passport
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: KycDatePicker(
                                        enabled: KycHelpers.fieldCheck(
                                          bloc.status,
                                        ),
                                        day: bloc.dayPassport1,
                                        month: bloc.monthPassport1,
                                        year: bloc.yearPassport1,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: KycErrorWrapper(
                                          bloc: bloc.passportExpDate1),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            fieldWrapper.divider,
                            // pj 2
                            fieldWrapper.viewWidget(
                              1,
                              null,
                              Theme(
                                  data: theme,
                                  child: ExpansionTile(
                                    backgroundColor: Colors.transparent,
                                    initiallyExpanded: false,
                                    title: Text(
                                      "Penanggung Jawab Perusahaan 2",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Kosongkan jika tidak ada Penanggung Jawab lain (Optional)",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.only(top: Sizes.s5),
                                          color: Color(0xffF4F4F4),
                                          height: 3,
                                          width: double.maxFinite,
                                        ),
                                        SizedBox(
                                          height: Sizes.s20,
                                        ),
                                      ],
                                    ),
                                    children: [
                                      // nama 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.nama2,
                                              inputType: TextInputType.text,
                                              hintText: "Contoh : Putri Aisyah",
                                              labelText: "Nama Lengkap"),
                                        ),
                                      ),
                                      // jabatan 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.jabatan2,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Jabatan sesuai dengan Akta Pendirian Usaha",
                                              labelText: "Jabatan"),
                                        ),
                                      ),
                                      // nomor ktp 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.noKtp2,
                                            inputType: TextInputType.number,
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText: "Nomor KTP",
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  16),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // title kadaluarsa ktp 2
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa KTP",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // radio ktp valid until 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SantaraBlocRadioGroup(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            selectFieldBloc:
                                                bloc.ktpValidUntil2),
                                      ),
                                      // tgl kadaluarsa ktp 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: BlocBuilder<SelectFieldBloc,
                                            dynamic>(
                                          bloc: bloc.ktpValidUntil2,
                                          builder: (context, state) {
                                            if (state.value != null) {
                                              if (state.value.id == "2") {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: Sizes.s10),
                                                  child: KycDatePicker(
                                                    enabled:
                                                        KycHelpers.fieldCheck(
                                                      bloc.status,
                                                    ),
                                                    day: bloc.dayKtp2,
                                                    month: bloc.monthKtp2,
                                                    year: bloc.yearKtp2,
                                                  ),
                                                );
                                              } else {
                                                return Container();
                                              }
                                            } else {
                                              return Container();
                                            }
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.ktpExpDate2),
                                      ),
                                      // no npwp 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.npwp2,
                                            inputType: TextInputType.number,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  20),
                                            ],
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText:
                                                "Nomor NPWP Penanggung Jawab",
                                          ),
                                        ),
                                      ),
                                      // nomor passport 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.noPassport2,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Kosongkan jika tidak ada (Optional)",
                                              labelText: "Nomor Passport"),
                                        ),
                                      ),
                                      // title tgl kadaluarsa passport 2
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa Passport ",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // subtitle tgl kadaluarsa passport 2
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "*Kosongkan jika field Passport kosong",
                                          style: TextStyle(
                                            fontSize: FontSize.s10,
                                            color: Color(0xffB8B8B8),
                                          ),
                                        ),
                                      ),
                                      // tgl kadaluarsa passport 2
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycDatePicker(
                                          enabled: KycHelpers.fieldCheck(
                                            bloc.status,
                                          ),
                                          day: bloc.dayPassport2,
                                          month: bloc.monthPassport2,
                                          year: bloc.yearPassport2,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.passportExpDate2),
                                      )
                                    ],
                                  )),
                            ),
                            fieldWrapper.divider,
                            // pj 3
                            fieldWrapper.viewWidget(
                              2,
                              null,
                              Theme(
                                  data: theme,
                                  child: ExpansionTile(
                                    backgroundColor: Colors.transparent,
                                    initiallyExpanded: false,
                                    title: Text(
                                      "Penanggung Jawab Perusahaan 3",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Kosongkan jika tidak ada Penanggung Jawab lain (Optional)",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.only(top: Sizes.s5),
                                          color: Color(0xffF4F4F4),
                                          height: 3,
                                          width: double.maxFinite,
                                        ),
                                        SizedBox(
                                          height: Sizes.s20,
                                        ),
                                      ],
                                    ),
                                    children: [
                                      // nama 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.nama3,
                                              inputType: TextInputType.text,
                                              hintText: "Contoh : Putri Aisyah",
                                              labelText: "Nama Lengkap"),
                                        ),
                                      ),
                                      // jabatan 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.jabatan3,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Jabatan sesuai dengan Akta Pendirian Usaha",
                                              labelText: "Jabatan"),
                                        ),
                                      ),
                                      // nomor ktp 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.noKtp3,
                                            inputType: TextInputType.number,
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText: "Nomor KTP",
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  16),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // title kadaluarsa ktp 3
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa KTP",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // radio ktp valid until 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SantaraBlocRadioGroup(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            selectFieldBloc:
                                                bloc.ktpValidUntil3),
                                      ),
                                      // tgl kadaluarsa ktp 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: BlocBuilder<SelectFieldBloc,
                                            dynamic>(
                                          bloc: bloc.ktpValidUntil3,
                                          builder: (context, state) {
                                            if (state.value != null) {
                                              if (state.value.id == "2") {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: Sizes.s10),
                                                  child: KycDatePicker(
                                                    enabled:
                                                        KycHelpers.fieldCheck(
                                                      bloc.status,
                                                    ),
                                                    day: bloc.dayKtp3,
                                                    month: bloc.monthKtp3,
                                                    year: bloc.yearKtp3,
                                                  ),
                                                );
                                              } else {
                                                return Container();
                                              }
                                            } else {
                                              return Container();
                                            }
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.ktpExpDate3),
                                      ),
                                      // no npwp 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.npwp3,
                                            inputType: TextInputType.number,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  20),
                                            ],
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText:
                                                "Nomor NPWP Penanggung Jawab",
                                          ),
                                        ),
                                      ),
                                      // nomor passport 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.noPassport3,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Kosongkan jika tidak ada (Optional)",
                                              labelText: "Nomor Passport"),
                                        ),
                                      ),
                                      // title tgl kadaluarsa passport 3
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa Passport ",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // subtitle tgl kadaluarsa passport 3
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "*Kosongkan jika field Passport kosong",
                                          style: TextStyle(
                                            fontSize: FontSize.s10,
                                            color: Color(0xffB8B8B8),
                                          ),
                                        ),
                                      ),
                                      // tgl kadaluarsa passport 3
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycDatePicker(
                                          enabled: KycHelpers.fieldCheck(
                                            bloc.status,
                                          ),
                                          day: bloc.dayPassport3,
                                          month: bloc.monthPassport3,
                                          year: bloc.yearPassport3,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.passportExpDate3),
                                      )
                                    ],
                                  )),
                            ),
                            fieldWrapper.divider,
                            // pj 4
                            fieldWrapper.viewWidget(
                              3,
                              null,
                              Theme(
                                  data: theme,
                                  child: ExpansionTile(
                                    backgroundColor: Colors.transparent,
                                    initiallyExpanded: false,
                                    title: Text(
                                      "Penanggung Jawab Perusahaan 4",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Kosongkan jika tidak ada Penanggung Jawab lain (Optional)",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.only(top: Sizes.s5),
                                          color: Color(0xffF4F4F4),
                                          height: 3,
                                          width: double.maxFinite,
                                        ),
                                        SizedBox(
                                          height: Sizes.s20,
                                        ),
                                      ],
                                    ),
                                    children: [
                                      // nama 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.nama4,
                                              inputType: TextInputType.text,
                                              hintText: "Contoh : Putri Aisyah",
                                              labelText: "Nama Lengkap"),
                                        ),
                                      ),
                                      // jabatan 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.jabatan4,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Jabatan sesuai dengan Akta Pendirian Usaha",
                                              labelText: "Jabatan"),
                                        ),
                                      ),
                                      // nomor ktp 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.noKtp4,
                                            inputType: TextInputType.number,
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText: "Nomor KTP",
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  16),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // title kadaluarsa ktp 4
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa KTP",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // radio ktp valid until 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SantaraBlocRadioGroup(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            selectFieldBloc:
                                                bloc.ktpValidUntil4),
                                      ),
                                      // tgl kadaluarsa ktp 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: BlocBuilder<SelectFieldBloc,
                                            dynamic>(
                                          bloc: bloc.ktpValidUntil4,
                                          builder: (context, state) {
                                            if (state.value != null) {
                                              if (state.value.id == "2") {
                                                return Padding(
                                                  padding: EdgeInsets.only(
                                                      top: Sizes.s10),
                                                  child: KycDatePicker(
                                                    enabled:
                                                        KycHelpers.fieldCheck(
                                                      bloc.status,
                                                    ),
                                                    day: bloc.dayKtp4,
                                                    month: bloc.monthKtp4,
                                                    year: bloc.yearKtp4,
                                                  ),
                                                );
                                              } else {
                                                return Container();
                                              }
                                            } else {
                                              return Container();
                                            }
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.ktpExpDate4),
                                      ),
                                      // no npwp 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.npwp4,
                                            inputType: TextInputType.number,
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(
                                                  20),
                                            ],
                                            hintText:
                                                "*Kosongkan jika tidak ada (Optional)",
                                            labelText:
                                                "Nomor NPWP Penanggung Jawab",
                                          ),
                                        ),
                                      ),
                                      // nomor passport 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.noPassport4,
                                              inputType: TextInputType.text,
                                              hintText:
                                                  "*Kosongkan jika tidak ada (Optional)",
                                              labelText: "Nomor Passport"),
                                        ),
                                      ),
                                      // title tgl kadaluarsa passport 4
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "Tanggal Kadaluarsa Passport ",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      // subtitle tgl kadaluarsa passport 4
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: Text(
                                          "*Kosongkan jika field Passport kosong",
                                          style: TextStyle(
                                            fontSize: FontSize.s10,
                                            color: Color(0xffB8B8B8),
                                          ),
                                        ),
                                      ),
                                      // tgl kadaluarsa passport 4
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycDatePicker(
                                          enabled: KycHelpers.fieldCheck(
                                            bloc.status,
                                          ),
                                          day: bloc.dayPassport4,
                                          month: bloc.monthPassport4,
                                          year: bloc.yearPassport4,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: KycErrorWrapper(
                                            bloc: bloc.passportExpDate4),
                                      )
                                    ],
                                  )),
                            ),
                            fieldWrapper.divider,
                            Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: Sizes.s20),
                              child: !KycHelpers.fieldCheck(bloc.status)
                                  ? SantaraDisabledButton()
                                  : SantaraMainButton(
                                      onPressed: () {
                                        PjPerusahaanHelper.dokumen.isSubmitted =
                                            true;
                                        FocusScope.of(context).unfocus();
                                        bloc.submit();
                                      },
                                      title: "Simpan",
                                    ),
                            ),
                          ]));
                }
              })),
    );
  }
}
