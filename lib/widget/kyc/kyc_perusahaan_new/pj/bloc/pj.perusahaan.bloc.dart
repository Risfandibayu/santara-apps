import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/PjPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/logger.dart';

class PjPerusahaanBloc extends FormBloc<String, String> {
  final KycPerusahaanService _service = KycPerusahaanService();
  User company = User();
  final String submissionId;
  final String status;

  // value bulan untuk dropdown
  static var rawMonths = [
    SubDataBiodata(id: "01", name: "Januari"),
    SubDataBiodata(id: "02", name: "Februari"),
    SubDataBiodata(id: "03", name: "Maret"),
    SubDataBiodata(id: "04", name: "April"),
    SubDataBiodata(id: "05", name: "Mei"),
    SubDataBiodata(id: "06", name: "Juni"),
    SubDataBiodata(id: "07", name: "Juli"),
    SubDataBiodata(id: "08", name: "Agustus"),
    SubDataBiodata(id: "09", name: "September"),
    SubDataBiodata(id: "10", name: "Oktober"),
    SubDataBiodata(id: "11", name: "November"),
    SubDataBiodata(id: "12", name: "Desember"),
  ];

  // subdatabiodata = model data berisi id & name
  static var rawValidUntil = [
    SubDataBiodata(id: "1", name: "Seumur Hidup"),
    SubDataBiodata(id: "2", name: "Berlaku Hingga"),
  ];

  //data pj 1
  final nama1 = TextFieldBloc();
  final jabatan1 = TextFieldBloc();
  final noKtp1 = TextFieldBloc();
  final npwp1 = TextFieldBloc();
  final noPassport1 = TextFieldBloc();
  final dayKtp1 = TextFieldBloc();
  final monthKtp1 = SelectFieldBloc(items: rawMonths);
  final yearKtp1 = TextFieldBloc();
  final dayPassport1 = TextFieldBloc();
  final monthPassport1 = SelectFieldBloc(items: rawMonths);
  final yearPassport1 = TextFieldBloc();
  final ktpValidUntil1 = SelectFieldBloc(items: rawValidUntil);
  final ktpExpDate1 = TextFieldBloc();
  final passportExpDate1 = TextFieldBloc();

  // data pj 2
  final nama2 = TextFieldBloc();
  final jabatan2 = TextFieldBloc();
  final noKtp2 = TextFieldBloc();
  final npwp2 = TextFieldBloc();
  final noPassport2 = TextFieldBloc();
  final dayKtp2 = TextFieldBloc();
  final monthKtp2 = SelectFieldBloc(items: rawMonths);
  final yearKtp2 = TextFieldBloc();
  final dayPassport2 = TextFieldBloc();
  final monthPassport2 = SelectFieldBloc(items: rawMonths);
  final yearPassport2 = TextFieldBloc();
  final ktpValidUntil2 = SelectFieldBloc(items: rawValidUntil);
  final ktpExpDate2 = TextFieldBloc();
  final passportExpDate2 = TextFieldBloc();

  // data pj 3
  final nama3 = TextFieldBloc();
  final jabatan3 = TextFieldBloc();
  final noKtp3 = TextFieldBloc();
  final npwp3 = TextFieldBloc();
  final noPassport3 = TextFieldBloc();
  final dayKtp3 = TextFieldBloc();
  final monthKtp3 = SelectFieldBloc(items: rawMonths);
  final yearKtp3 = TextFieldBloc();
  final dayPassport3 = TextFieldBloc();
  final monthPassport3 = SelectFieldBloc(items: rawMonths);
  final yearPassport3 = TextFieldBloc();
  final ktpValidUntil3 = SelectFieldBloc(items: rawValidUntil);
  final ktpExpDate3 = TextFieldBloc();
  final passportExpDate3 = TextFieldBloc();

  // data pj 4
  final nama4 = TextFieldBloc();
  final jabatan4 = TextFieldBloc();
  final noKtp4 = TextFieldBloc();
  final npwp4 = TextFieldBloc();
  final noPassport4 = TextFieldBloc();
  final dayKtp4 = TextFieldBloc();
  final monthKtp4 = SelectFieldBloc(items: rawMonths);
  final yearKtp4 = TextFieldBloc();
  final dayPassport4 = TextFieldBloc();
  final monthPassport4 = SelectFieldBloc(items: rawMonths);
  final yearPassport4 = TextFieldBloc();
  final ktpValidUntil4 = SelectFieldBloc(items: rawValidUntil);
  final ktpExpDate4 = TextFieldBloc();
  final passportExpDate4 = TextFieldBloc();

  PjPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      //pj1
      nama1,
      jabatan1,
      noKtp1,
      npwp1,
      noPassport1,
      dayPassport1,
      monthPassport1,
      yearPassport1,
      ktpValidUntil1,
      passportExpDate1,
      //pj2
      nama2,
      jabatan2,
      noKtp2,
      npwp2,
      noPassport2,
      dayPassport2,
      monthPassport2,
      yearPassport2,
      ktpValidUntil2,
      passportExpDate2,
      //pj3
      nama3,
      jabatan3,
      noKtp3,
      npwp3,
      noPassport3,
      dayPassport3,
      monthPassport3,
      yearPassport3,
      ktpValidUntil3,
      passportExpDate3,
      //pj4
      nama4,
      jabatan4,
      noKtp4,
      npwp4,
      noPassport4,
      dayPassport4,
      monthPassport4,
      yearPassport4,
      ktpValidUntil4,
      passportExpDate4,
    ]);
    onChangeValidKtp(1);
    onChangeValidKtp(2);
    onChangeValidKtp(3);
    onChangeValidKtp(4);
    updateCompanyData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
      // onChangeValidKtp(1);
      // onChangeValidKtp(2);
      // onChangeValidKtp(3);
      // onChangeValidKtp(4);
    });
  }

  onChangeValidKtp(int index) {
    switch (index) {
      case 1:
        ktpValidUntil1.onValueChanges(onData: (data, v) async* {
          if (v.value.id == "2") {
            showKtpExpiredDate(index);
          } else {
            // jika tidak remove field yg telah ditambahkan
            removeKtpExpiredDate(index);
          }
        });
        break;
      case 2:
        ktpValidUntil2.onValueChanges(onData: (data, v) async* {
          if (v.value.id == "2") {
            showKtpExpiredDate(index);
          } else {
            // jika tidak remove field yg telah ditambahkan
            removeKtpExpiredDate(index);
          }
        });
        break;
      case 3:
        ktpValidUntil3.onValueChanges(onData: (data, v) async* {
          if (v.value.id == "2") {
            showKtpExpiredDate(index);
          } else {
            // jika tidak remove field yg telah ditambahkan
            removeKtpExpiredDate(index);
          }
        });
        break;
      case 4:
        ktpValidUntil4.onValueChanges(onData: (data, v) async* {
          if (v.value.id == "2") {
            showKtpExpiredDate(index);
          } else {
            // jika tidak remove field yg telah ditambahkan
            removeKtpExpiredDate(index);
          }
        });
        break;
      default:
        break;
    }
  }

  void showKtpExpiredDate(int index) {
    switch (index) {
      case 1:
        addFieldBloc(fieldBloc: dayKtp1);
        addFieldBloc(fieldBloc: monthKtp1);
        addFieldBloc(fieldBloc: yearKtp1);
        addFieldBloc(fieldBloc: ktpExpDate1);
        break;
      case 2:
        addFieldBloc(fieldBloc: dayKtp2);
        addFieldBloc(fieldBloc: monthKtp2);
        addFieldBloc(fieldBloc: yearKtp2);
        addFieldBloc(fieldBloc: ktpExpDate2);
        break;
      case 3:
        addFieldBloc(fieldBloc: dayKtp3);
        addFieldBloc(fieldBloc: monthKtp3);
        addFieldBloc(fieldBloc: yearKtp3);
        addFieldBloc(fieldBloc: ktpExpDate3);
        break;
      case 4:
        addFieldBloc(fieldBloc: dayKtp4);
        addFieldBloc(fieldBloc: monthKtp4);
        addFieldBloc(fieldBloc: yearKtp4);
        addFieldBloc(fieldBloc: ktpExpDate4);
        break;
      default:
        break;
    }
  }

  void removeKtpExpiredDate(int index) {
    switch (index) {
      case 1:
        removeFieldBloc(fieldBloc: dayKtp1);
        removeFieldBloc(fieldBloc: monthKtp1);
        removeFieldBloc(fieldBloc: yearKtp1);
        removeFieldBloc(fieldBloc: ktpExpDate1);
        break;
      case 2:
        removeFieldBloc(fieldBloc: dayKtp2);
        removeFieldBloc(fieldBloc: monthKtp2);
        removeFieldBloc(fieldBloc: yearKtp2);
        removeFieldBloc(fieldBloc: ktpExpDate2);
        break;
      case 3:
        removeFieldBloc(fieldBloc: dayKtp3);
        removeFieldBloc(fieldBloc: monthKtp3);
        removeFieldBloc(fieldBloc: yearKtp3);
        removeFieldBloc(fieldBloc: ktpExpDate3);
        break;
      case 4:
        removeFieldBloc(fieldBloc: dayKtp4);
        removeFieldBloc(fieldBloc: monthKtp4);
        removeFieldBloc(fieldBloc: yearKtp4);
        removeFieldBloc(fieldBloc: ktpExpDate4);
        break;
      default:
        break;
    }
  }

  onFieldsChange() {
    detectChanges(nama1);
    detectChanges(jabatan1);
    detectChanges(noKtp1);
    detectChanges(npwp1);
    detectChanges(noPassport1);
    detectChanges(dayKtp1);
    detectChanges(monthKtp1);
    detectChanges(yearKtp1);
    detectChanges(dayPassport1);
    detectChanges(monthPassport1);
    detectChanges(yearPassport1);
    detectChanges(ktpValidUntil1);
    detectChanges(ktpExpDate1);
    detectChanges(passportExpDate1);
    detectChanges(nama2);
    detectChanges(jabatan2);
    detectChanges(noKtp2);
    detectChanges(npwp2);
    detectChanges(noPassport2);
    detectChanges(dayKtp2);
    detectChanges(monthKtp2);
    detectChanges(yearKtp2);
    detectChanges(dayPassport2);
    detectChanges(monthPassport2);
    detectChanges(yearPassport2);
    detectChanges(ktpValidUntil2);
    detectChanges(nama3);
    detectChanges(jabatan3);
    detectChanges(noKtp3);
    detectChanges(npwp3);
    detectChanges(noPassport3);
    detectChanges(dayKtp3);
    detectChanges(monthKtp3);
    detectChanges(yearKtp3);
    detectChanges(dayPassport3);
    detectChanges(monthPassport3);
    detectChanges(yearPassport3);
    detectChanges(ktpValidUntil3);
    detectChanges(nama4);
    detectChanges(jabatan4);
    detectChanges(noKtp4);
    detectChanges(npwp4);
    detectChanges(noPassport4);
    detectChanges(dayKtp4);
    detectChanges(monthKtp4);
    detectChanges(yearKtp4);
    detectChanges(dayPassport4);
    detectChanges(monthPassport4);
    detectChanges(yearPassport4);
    detectChanges(ktpValidUntil4);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      PjPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "4").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // pj 1
      nama1.updateInitialValue(company.trader.companyResponsibleName1);
      jabatan1.updateInitialValue(company.trader.companyResponsiblePosition1);
      noKtp1.updateInitialValue(company.trader.companyResponsibleIdcardNumber1);
      npwp1.updateInitialValue(company.trader.companyResponsibleNpwp1);
      noPassport1
          .updateInitialValue(company.trader.companyResponsiblePassport1);

      if (company.trader.companyResponsibleExpiredDateIdcard1 != null) {
        ktpValidUntil1.updateInitialValue(ktpValidUntil1.state.items.last);
        try {
          // explode tanggal kadaluarsa ktp
          var tglExpKtp = DateTime.parse(
              company.trader.companyResponsibleExpiredDateIdcard1);
          await Future.delayed(Duration(milliseconds: 100));
          dayKtp1.updateInitialValue(
              "${tglExpKtp.day < 10 ? '0' + tglExpKtp.day.toString() : tglExpKtp.day}");
          monthKtp1
              .updateInitialValue(monthKtp1.state.items[tglExpKtp.month - 1]);
          yearKtp1.updateInitialValue(tglExpKtp.year.toString());
        } catch (e, stack) {
          // print(e);
        }
      } else {
        if (status != "empty") {
          if (company.trader.companyResponsibleIdcardNumber1 != null) {
            ktpValidUntil1.updateInitialValue(ktpValidUntil1.state.items.first);
          }
        }
      }
      if (company.trader.companyResponsibleExpiredDatePassport1 != null) {
        // explode tanggal kadaluarsa ktp
        var tglExpPaspor = DateTime.parse(
            company.trader.companyResponsibleExpiredDatePassport1);
        dayPassport1.updateInitialValue(
          "${tglExpPaspor.day < 10 ? '0' + tglExpPaspor.day.toString() : tglExpPaspor.day}",
        );
        monthPassport1.updateInitialValue(
            monthPassport1.state.items[tglExpPaspor.month - 1]);
        yearPassport1.updateInitialValue(tglExpPaspor.year.toString());
      }
      // pj 2
      nama2.updateInitialValue(company.trader.companyResponsibleName2);
      jabatan2.updateInitialValue(company.trader.companyResponsiblePosition2);
      noKtp2.updateInitialValue(company.trader.companyResponsibleIdcardNumber2);
      npwp2.updateInitialValue(company.trader.companyResponsibleNpwp2);
      noPassport2
          .updateInitialValue(company.trader.companyResponsiblePassport2);
      if (company.trader.companyResponsibleExpiredDateIdcard2 != null) {
        ktpValidUntil2.updateInitialValue(ktpValidUntil2.state.items.last);
        try {
          // explode tanggal kadaluarsa ktp
          var tglExpKtp = DateTime.parse(
              company.trader.companyResponsibleExpiredDateIdcard2);
          await Future.delayed(Duration(milliseconds: 100));
          dayKtp2.updateInitialValue(
              "${tglExpKtp.day < 10 ? '0' + tglExpKtp.day.toString() : tglExpKtp.day}");
          monthKtp2
              .updateInitialValue(monthKtp2.state.items[tglExpKtp.month - 1]);
          yearKtp2.updateInitialValue(tglExpKtp.year.toString());
        } catch (e, stack) {
          // print(e);
        }
      } else {
        if (status != "empty") {
          if (company.trader.companyResponsibleIdcardNumber2 != null) {
            ktpValidUntil2.updateInitialValue(ktpValidUntil2.state.items.first);
          }
        }
      }
      if (company.trader.companyResponsibleExpiredDatePassport2 != null) {
        // explode tanggal kadaluarsa ktp
        var tglExpPaspor = DateTime.parse(
            company.trader.companyResponsibleExpiredDatePassport2);
        dayPassport2.updateInitialValue(
          "${tglExpPaspor.day < 10 ? '0' + tglExpPaspor.day.toString() : tglExpPaspor.day}",
        );
        monthPassport2.updateInitialValue(
            monthPassport2.state.items[tglExpPaspor.month - 1]);
        yearPassport2.updateInitialValue(tglExpPaspor.year.toString());
      }
      // pj 3
      nama3.updateInitialValue(company.trader.companyResponsibleName3);
      jabatan3.updateInitialValue(company.trader.companyResponsiblePosition3);
      noKtp3.updateInitialValue(company.trader.companyResponsibleIdcardNumber3);
      npwp3.updateInitialValue(company.trader.companyResponsibleNpwp3);
      noPassport3
          .updateInitialValue(company.trader.companyResponsiblePassport3);
      if (company.trader.companyResponsibleExpiredDateIdcard3 != null) {
        ktpValidUntil3.updateInitialValue(ktpValidUntil3.state.items.last);
        try {
          // explode tanggal kadaluarsa ktp
          var tglExpKtp = DateTime.parse(
              company.trader.companyResponsibleExpiredDateIdcard3);
          await Future.delayed(Duration(milliseconds: 100));
          dayKtp3.updateInitialValue(
              "${tglExpKtp.day < 10 ? '0' + tglExpKtp.day.toString() : tglExpKtp.day}");
          monthKtp3
              .updateInitialValue(monthKtp3.state.items[tglExpKtp.month - 1]);
          yearKtp3.updateInitialValue(tglExpKtp.year.toString());
        } catch (e, stack) {
          // print(e);
        }
      } else {
        if (status != "empty") {
          if (company.trader.companyResponsibleIdcardNumber3 != null) {
            ktpValidUntil3.updateInitialValue(ktpValidUntil3.state.items.first);
          }
        }
      }
      if (company.trader.companyResponsibleExpiredDatePassport3 != null) {
        // explode tanggal kadaluarsa ktp
        var tglExpPaspor = DateTime.parse(
            company.trader.companyResponsibleExpiredDatePassport3);
        dayPassport3.updateInitialValue(
          "${tglExpPaspor.day < 10 ? '0' + tglExpPaspor.day.toString() : tglExpPaspor.day}",
        );
        monthPassport3.updateInitialValue(
            monthPassport3.state.items[tglExpPaspor.month - 1]);
        yearPassport3.updateInitialValue(tglExpPaspor.year.toString());
      }
      // pj 4
      nama4.updateInitialValue(company.trader.companyResponsibleName4);
      jabatan4.updateInitialValue(company.trader.companyResponsiblePosition4);
      noKtp4.updateInitialValue(company.trader.companyResponsibleIdcardNumber4);
      npwp4.updateInitialValue(company.trader.companyResponsibleNpwp4);
      noPassport4
          .updateInitialValue(company.trader.companyResponsiblePassport4);
      if (company.trader.companyResponsibleExpiredDateIdcard4 != null) {
        ktpValidUntil4.updateInitialValue(ktpValidUntil4.state.items.last);
        try {
          // explode tanggal kadaluarsa ktp
          var tglExpKtp = DateTime.parse(
              company.trader.companyResponsibleExpiredDateIdcard4);
          await Future.delayed(Duration(milliseconds: 100));
          dayKtp4.updateInitialValue(
              "${tglExpKtp.day < 10 ? '0' + tglExpKtp.day.toString() : tglExpKtp.day}");
          monthKtp4
              .updateInitialValue(monthKtp4.state.items[tglExpKtp.month - 1]);
          yearKtp4.updateInitialValue(tglExpKtp.year.toString());
        } catch (e, stack) {
          // print(e);
        }
      } else {
        if (status != "empty") {
          if (company.trader.companyResponsibleIdcardNumber4 != null) {
            ktpValidUntil4.updateInitialValue(ktpValidUntil4.state.items.first);
          }
        }
      }
      if (company.trader.companyResponsibleExpiredDatePassport4 != null) {
        // explode tanggal kadaluarsa ktp
        var tglExpPaspor = DateTime.parse(
            company.trader.companyResponsibleExpiredDatePassport4);
        dayPassport4.updateInitialValue(
          "${tglExpPaspor.day < 10 ? '0' + tglExpPaspor.day.toString() : tglExpPaspor.day}",
        );
        monthPassport4.updateInitialValue(
            monthPassport4.state.items[tglExpPaspor.month - 1]);
        yearPassport4.updateInitialValue(tglExpPaspor.year.toString());
      }

      if (status == "rejected") {
        await getFieldErrorStatus();
      }
      emitLoaded();
    } catch (e) {
      emitLoaded();
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      // pj 1
      case "company_responsible_name1":
        addErrorMessage(nama1, _errMsg, "company_responsible_name1");
        break;
      case "company_responsible_position1":
        addErrorMessage(jabatan1, _errMsg, "company_responsible_position1");
        break;
      case "company_responsible_idcard_number1":
        addErrorMessage(noKtp1, _errMsg, "company_responsible_idcard_number1");
        break;
      case "company_responsible_npwp1":
        addErrorMessage(npwp1, _errMsg, "company_responsible_npwp1");
        break;
      case "company_responsible_passport1":
        addErrorMessage(noPassport1, _errMsg, "company_responsible_passport1");
        break;
      case "company_responsible_expired_date_idcard1":
        addErrorMessage(
            ktpExpDate1, _errMsg, "company_responsible_expired_date_idcard1");
        break;
      case "company_responsible_expired_date_passport1":
        addErrorMessage(passportExpDate1, _errMsg,
            "company_responsible_expired_date_passport1");
        break;
      // pj 2
      case "company_responsible_name2":
        addErrorMessage(nama2, _errMsg, "company_responsible_name2");
        break;
      case "company_responsible_position2":
        addErrorMessage(jabatan2, _errMsg, "company_responsible_position2");
        break;
      case "company_responsible_idcard_number2":
        addErrorMessage(noKtp2, _errMsg, "company_responsible_idcard_number2");
        break;
      case "company_responsible_npwp2":
        addErrorMessage(npwp2, _errMsg, "company_responsible_npwp2");
        break;
      case "company_responsible_passport2":
        addErrorMessage(noPassport2, _errMsg, "company_responsible_passport2");
        break;
      case "company_responsible_expired_date_idcard2":
        addErrorMessage(
            ktpExpDate2, _errMsg, "company_responsible_expired_date_idcard2");
        break;
      case "company_responsible_expired_date_passport2":
        addErrorMessage(passportExpDate2, _errMsg,
            "company_responsible_expired_date_passport2");
        break;
      // pj 3
      case "company_responsible_name3":
        addErrorMessage(nama3, _errMsg, "company_responsible_name3");
        break;
      case "company_responsible_position3":
        addErrorMessage(jabatan3, _errMsg, "company_responsible_position3");
        break;
      case "company_responsible_idcard_number3":
        addErrorMessage(noKtp3, _errMsg, "company_responsible_idcard_number3");
        break;
      case "company_responsible_npwp3":
        addErrorMessage(npwp3, _errMsg, "company_responsible_npwp3");
        break;
      case "company_responsible_passport3":
        addErrorMessage(noPassport3, _errMsg, "company_responsible_passport3");
        break;
      case "company_responsible_expired_date_idcard3":
        addErrorMessage(
            ktpExpDate3, _errMsg, "company_responsible_expired_date_idcard3");
        break;
      case "company_responsible_expired_date_passport3":
        addErrorMessage(passportExpDate3, _errMsg,
            "company_responsible_expired_date_passport3");
        break;
      // pj 4
      case "company_responsible_name4":
        addErrorMessage(nama4, _errMsg, "company_responsible_name4");
        break;
      case "company_responsible_position4":
        addErrorMessage(jabatan4, _errMsg, "company_responsible_position4");
        break;
      case "company_responsible_idcard_number4":
        addErrorMessage(noKtp4, _errMsg, "company_responsible_idcard_number4");
        break;
      case "company_responsible_npwp4":
        addErrorMessage(npwp4, _errMsg, "company_responsible_npwp4");
        break;
      case "company_responsible_passport4":
        addErrorMessage(noPassport4, _errMsg, "company_responsible_passport4");
        break;
      case "company_responsible_expired_date_idcard4":
        addErrorMessage(
            ktpExpDate4, _errMsg, "company_responsible_expired_date_idcard4");
        break;
      case "company_responsible_expired_date_passport4":
        addErrorMessage(passportExpDate4, _errMsg,
            "company_responsible_expired_date_passport4");
        break;
      default:
        emitFailure(
            failureResponse:
                "ERRORSUBMIT Tidak dapat mengirim data, cek kembali form anda!");
        break;
    }
  }

  bool isName1Error() {
    if (nama1.value != null && nama1.value.length > 0) {
      return false;
    } else {
      nama1.addFieldError("Nama Penanggung Jawab tidak boleh kosong");
      return true;
    }
  }

  bool isJabatan1Error() {
    if (jabatan1.value != null && jabatan1.value.length > 0) {
      return false;
    } else {
      jabatan1.addFieldError("Jabatan Penanggung Jawab tidak boleh kosong");
      return true;
    }
  }

  bool isKtp1Error() {
    if (noKtp1.value != null && noKtp1.value.length == 16) {
      return false;
    } else if (noKtp1.value == null) {
      noKtp1.addFieldError("No KTP Tidak Boleh Kosong");
      return true;
    } else if (noKtp1.value.length < 16) {
      noKtp1.addFieldError("No KTP Minimal 16 Digit");
      return true;
    } else if (noKtp1.value.length > 16) {
      noKtp1.addFieldError("No KTP Maksimal 16 Digit");
      return true;
    } else {
      noKtp1.addFieldError("No KTP Penanggung Jawab tidak boleh kosong");
      return true;
    }
  }

  bool isKtpLengthError(TextFieldBloc bloc) {
    if (bloc.value == null) {
      return false;
    } else if (bloc.value.length == 0) {
      return false;
    } else if (bloc.value != null && bloc.value.length < 16) {
      bloc.addFieldError("No Ktp Harus 16 Digit");
      return true;
    } else if (bloc.value != null && bloc.value.length > 16) {
      bloc.addFieldError("No KTP Harus 16 Digit");
      return true;
    } else {
      return false;
    }
  }

  bool isExpKTPError(TextFieldBloc d, SelectFieldBloc m, TextFieldBloc y,
      TextFieldBloc e, SelectFieldBloc validUntil) {
    if (validUntil.value == null) {
      validUntil.addFieldError("Tanggal kadaluarsa ktp wajib diisi!");
      return true;
    } else {
      try {
        if (validUntil.value.id == "1") {
          return false;
        } else {
          if (d.value == "" && m.value == null && y.value == "") {
            return false;
          } else if (!RegExp(r"^[0-9]*$").hasMatch(y.value) ||
              !RegExp(r"^[0-9]*$").hasMatch(d.value)) {
            e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
            return true;
          } else {
            var tgl = num.parse(d.value).toInt();
            var bln = num.parse(m.value.id).toInt();
            var thn = num.parse(y.value).toInt();
            if (thn < 1900) {
              e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
              return true;
            } else {
              if (bln == 2) {
                if (thn % 400 == 0 || thn % 4 == 0) {
                  if (tgl > 29) {
                    e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
                    return true;
                  } else {
                    return false;
                  }
                } else {
                  if (tgl > 28) {
                    e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
                    return true;
                  } else {
                    return false;
                  }
                }
              } else if (bln == 4 || bln == 6 || bln == 9 || bln == 11) {
                if (tgl > 30) {
                  e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
                  return true;
                } else {
                  return false;
                }
              } else {
                if (tgl > 31) {
                  e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
                  return true;
                } else {
                  return false;
                }
              }
            }
          }
        }
      } catch (_) {
        e.addFieldError("Tanggal kadaluarsa KTP tidak valid");
        return true;
      }
    }
  }

  bool isExpPassportError(
      TextFieldBloc d, SelectFieldBloc m, TextFieldBloc y, TextFieldBloc e) {
    try {
      if (d.value.length < 1 && m.value == null && y.value.length < 1) {
        return false;
      } else if (d.value.length < 1 || m.value == null || y.value.length < 1) {
        e.addFieldError("Tanggal kadaluarsa passport tidak valid");
        return true;
      } else if (!RegExp(r"^[0-9]*$").hasMatch(y.value) ||
          !RegExp(r"^[0-9]*$").hasMatch(d.value)) {
        e.addFieldError("Tanggal kadaluarsa passport tidak valid");
        return true;
      } else {
        var tgl = num.parse(d.value).toInt();
        var bln = num.parse(m.value.id).toInt();
        var thn = num.parse(y.value).toInt();
        if (thn < 1900) {
          e.addFieldError("Tanggal kadaluarsa passport tidak valid");
          return true;
        } else {
          if (bln == 2) {
            if (thn % 400 == 0 || thn % 4 == 0) {
              if (tgl > 29) {
                e.addFieldError("Tanggal kadaluarsa passport tidak valid");
                return true;
              } else {
                return false;
              }
            } else {
              if (tgl > 28) {
                e.addFieldError("Tanggal kadaluarsa passport tidak valid");
                return true;
              } else {
                return false;
              }
            }
          } else if (bln == 4 || bln == 6 || bln == 9 || bln == 11) {
            if (tgl > 30) {
              e.addFieldError("Tanggal kadaluarsa passport tidak valid");
              return true;
            } else {
              return false;
            }
          } else {
            if (tgl > 31) {
              e.addFieldError("Tanggal kadaluarsa passport tidak valid");
              return true;
            } else {
              return false;
            }
          }
        }
      }
    } catch (_) {
      e.addFieldError("Tanggal kadaluarsa passport tidak valid");
      return true;
    }
  }

  @override
  void onLoading() {
    // emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      // isExpKTPError(dayKtp1, monthKtp1, yearKtp1, ktpExpDate1, ktpValidUntil1);
      // isExpKTPError(dayKtp2, monthKtp2, yearKtp2, ktpExpDate2, ktpValidUntil2);
      // isExpKTPError(dayKtp3, monthKtp3, yearKtp3, ktpExpDate3, ktpValidUntil3);
      // isExpKTPError(dayKtp4, monthKtp4, yearKtp4, ktpExpDate4, ktpValidUntil4);
      isExpPassportError(
          dayPassport1, monthPassport1, yearPassport1, passportExpDate1);
      isExpPassportError(
          dayPassport2, monthPassport2, yearPassport2, passportExpDate2);
      isExpPassportError(
          dayPassport3, monthPassport3, yearPassport3, passportExpDate3);
      isExpPassportError(
          dayPassport4, monthPassport4, yearPassport4, passportExpDate4);
      if (isName1Error()) {
        emitFailure(failureResponse: "company_responsible_name1");
      } else if (isJabatan1Error()) {
        emitFailure(failureResponse: "company_responsible_position1");
      } else if (isKtpLengthError(noKtp1)) {
        emitFailure(failureResponse: "company_responsible_idcard_number1");
      } else if (isKtpLengthError(noKtp2)) {
        emitFailure(failureResponse: "company_responsible_idcard_number2");
      } else if (isKtpLengthError(noKtp3)) {
        emitFailure(failureResponse: "company_responsible_idcard_number3");
      } else if (isKtpLengthError(noKtp4)) {
        emitFailure(failureResponse: "company_responsible_idcard_number4");
      }
      // else if (isExpKTPError(
      //     dayKtp1, monthKtp1, yearKtp1, ktpExpDate1, ktpValidUntil1)) {
      //   emitFailure(failureResponse: "company_responsible_expired_idcard1");
      // }
      // else if (isExpKTPError(
      //     dayKtp2, monthKtp2, yearKtp2, ktpExpDate2, ktpValidUntil2)) {
      //   emitFailure(failureResponse: "company_responsible_expired_idcard2");
      // } else if (isExpKTPError(
      //     dayKtp3, monthKtp3, yearKtp3, ktpExpDate3, ktpValidUntil3)) {
      //   emitFailure(failureResponse: "company_responsible_expired_idcard3");
      // } else if (isExpKTPError(
      //     dayKtp4, monthKtp4, yearKtp4, ktpExpDate4, ktpValidUntil4)) {
      //   emitFailure(failureResponse: "company_responsible_expired_idcard4");
      // }
      else if (isExpPassportError(
          dayPassport1, monthPassport1, yearPassport1, passportExpDate1)) {
        emitFailure(
            failureResponse: "company_responsible_expired_date_passport1");
      } else if (isExpPassportError(
          dayPassport2, monthPassport2, yearPassport2, passportExpDate2)) {
        emitFailure(
            failureResponse: "company_responsible_expired_date_passport2");
      } else if (isExpPassportError(
          dayPassport3, monthPassport3, yearPassport3, passportExpDate3)) {
        emitFailure(
            failureResponse: "company_responsible_expired_date_passport3");
      } else if (isExpPassportError(
          dayPassport4, monthPassport4, yearPassport4, passportExpDate4)) {
        emitFailure(
            failureResponse: "company_responsible_expired_date_passport4");
      } else {
        try {
          PjPerusahaanData data = PjPerusahaanData(
            //pj 1
            nama1: nama1.value,
            jabatan1: jabatan1.value,
            noKtp1: noKtp1.value,
            npwp1: npwp1.value,
            noPassport1: noPassport1.value,
            dateExpKtp1: yearKtp1.value.length < 1 ||
                    monthKtp1.value == null ||
                    dayKtp1.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearKtp1.value).toInt(),
                    num.parse(monthKtp1.value.id).toInt(),
                    num.parse(dayKtp1.value).toInt()),
            dateExpPassport1: yearPassport1.value.length < 1 ||
                    monthPassport1.value == null ||
                    dayPassport1.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearPassport1.value).toInt(),
                    num.parse(monthPassport1.value.id).toInt(),
                    num.parse(dayPassport1.value).toInt()),
            //pj 2
            nama2: nama2.value,
            jabatan2: jabatan2.value,
            noKtp2: noKtp2.value,
            npwp2: npwp2.value,
            noPassport2: noPassport2.value,
            dateExpKtp2: yearKtp2.value.length < 1 ||
                    monthKtp2.value == null ||
                    dayKtp2.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearKtp2.value).toInt(),
                    num.parse(monthKtp2.value.id).toInt(),
                    num.parse(dayKtp2.value).toInt()),
            dateExpPassport2: yearPassport2.value.length < 1 ||
                    monthPassport2.value == null ||
                    dayPassport2.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearPassport2.value).toInt(),
                    num.parse(monthPassport2.value.id).toInt(),
                    num.parse(dayPassport2.value).toInt()),
            //pj 3
            nama3: nama3.value,
            jabatan3: jabatan3.value,
            noKtp3: noKtp3.value,
            npwp3: npwp3.value,
            noPassport3: noPassport3.value,
            dateExpKtp3: yearKtp3.value.length < 1 ||
                    monthKtp3.value == null ||
                    dayKtp3.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearKtp3.value).toInt(),
                    num.parse(monthKtp3.value.id).toInt(),
                    num.parse(dayKtp3.value).toInt()),
            dateExpPassport3: yearPassport3.value.length < 1 ||
                    monthPassport3.value == null ||
                    dayPassport3.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearPassport3.value).toInt(),
                    num.parse(monthPassport3.value.id).toInt(),
                    num.parse(dayPassport3.value).toInt()),
            // pj 4
            nama4: nama4.value,
            jabatan4: jabatan4.value,
            noKtp4: noKtp4.value,
            npwp4: npwp4.value,
            noPassport4: noPassport4.value,
            dateExpKtp4: yearKtp4.value.length < 1 ||
                    monthKtp4.value == null ||
                    dayKtp4.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearKtp4.value).toInt(),
                    num.parse(monthKtp4.value.id).toInt(),
                    num.parse(dayKtp4.value).toInt()),
            dateExpPassport4: yearPassport4.value.length < 1 ||
                    monthPassport4.value == null ||
                    dayPassport4.value.length < 1
                ? null
                : DateTime(
                    num.parse(yearPassport4.value).toInt(),
                    num.parse(monthPassport4.value.id).toInt(),
                    num.parse(dayPassport4.value).toInt()),
          );
          await _service.pjBankPerusahaan(data, status).then((value) {
            if (value.statusCode == 200) {
              emitSuccess(
                canSubmitAgain: false,
                successResponse:
                    "Berhasil menyimpan penanggung jawab perusahaan",
              );
            } else if (value.statusCode == 401) {
              emitFailure(failureResponse: "UNAUTHORIZED");
            } else if (value.statusCode == 400) {
              try {
                value.data.forEach((res) {
                  var dummy = ValidationResultModel.fromJson(res);
                  handleApiValidation(dummy.field, dummy.message);
                });
              } catch (e) {
                // print(e);
                emitFailure(
                  failureResponse: "ERRORSUBMIT ${value.data['message']}",
                );
              }
            } else {
              // jika status code bukan 200 / 400
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
              );
            }
          });
        } catch (e, stack) {
          emitFailure(
            failureResponse: "ERRORSUBMIT Tidak dapat mengirimkan data!",
          );
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      // print(e);
      // print(stack);
      emitFailure(
          failureResponse: "ERRORSUBMIT Error saat mencoba mengirimkan data!");
    }
  }
}
