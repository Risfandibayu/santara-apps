import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/Country.dart';
import '../../../../models/Province.dart';
import '../../../../models/Regency.dart';
import '../../../../models/kyc/company/AlamatPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/alamat.perusahaan.bloc.dart';

class AlamatPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  AlamatPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (AlamatPerusahaanHelper.dokumen.isEdited != null) {
            if (AlamatPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                AlamatPerusahaanHelper.dokumen = AlamatPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              AlamatPerusahaanHelper.dokumen = AlamatPerusahaanData();
              return true;
            }
          } else {
            AlamatPerusahaanHelper.dokumen = AlamatPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Alamat",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "3/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => AlamatPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: AlamatPerusahaanForm(),
          )),
    );
  }
}

class AlamatPerusahaanForm extends StatefulWidget {
  @override
  _AlamatPerusahaanFormState createState() => _AlamatPerusahaanFormState();
}

class _AlamatPerusahaanFormState extends State<AlamatPerusahaanForm> {
  AlamatPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    AlamatPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<AlamatPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  _reload(VoidCallback onPressed) {
    return FlatButton(
      onPressed: onPressed,
      shape: Border.all(
        width: 1,
        color: Colors.grey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.replay,
            size: FontSize.s15,
          ),
          Container(
            width: Sizes.s10,
          ),
          Text(
            "Muat Ulang",
            style: TextStyle(
              fontSize: FontSize.s15,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<AlamatPerusahaanBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, "${state.successResponse}");
            Navigator.pop(context);
            AlamatPerusahaanHelper.dokumen = AlamatPerusahaanData();
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            if (AlamatPerusahaanHelper.dokumen.isSubmitted != null) {
              Navigator.pop(context);
            }

            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
            }

            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              switch (state.failureResponse.toLowerCase()) {
                case "company_country_address":
                  fieldWrapper.scrollTo(1);
                  break;
                case "company_province_address":
                  fieldWrapper.scrollTo(2);
                  break;
                case "company_regency_address":
                  fieldWrapper.scrollTo(3);
                  break;
                case "company_address":
                  fieldWrapper.scrollTo(4);
                  break;
                case "company_postal_code":
                  fieldWrapper.scrollTo(5);
                  break;
                case "company_phone_number":
                  fieldWrapper.scrollTo(6);
                  break;
                case "company_fax":
                  fieldWrapper.scrollTo(7);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<AlamatPerusahaanBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                if (state is FormBlocLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is FormBlocLoadFailed) {
                  return Center(
                    child: Text("Failed to load!"),
                  );
                } else {
                  return Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Colors.white,
                      child: ListView(
                          scrollDirection: scrollDirection,
                          padding: EdgeInsets.all(Sizes.s20),
                          controller: controller,
                          children: [
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Alamat Perusahaan",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // Negara
                            fieldWrapper.viewWidget(
                              1,
                              null,
                              BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                bloc: bloc.countries,
                                builder: (context, state) {
                                  return state.extraData.isError
                                      ? _reload(() {
                                          bloc.getCountries();
                                        })
                                      : DropdownFieldBlocBuilder<Country>(
                                          selectFieldBloc: bloc.countries,
                                          isEnabled:
                                              KycHelpers.fieldCheck(bloc.status)
                                                  ? state.extraData.isActive ??
                                                      true
                                                  : false,
                                          decoration: InputDecoration(
                                            errorMaxLines: 5,
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            labelText: "Negara",
                                            hintText: "Indonesia",
                                            hintStyle: TextStyle(
                                              color: Colors.grey,
                                            ),
                                            suffixIcon: state.extraData.isLoaded
                                                ? Icon(Icons.arrow_drop_down)
                                                : CupertinoActivityIndicator(),
                                          ),
                                          itemBuilder: (context, value) =>
                                              value != null ? value.name : "",
                                        );
                                },
                              ),
                            ),
                            // Provinsi
                            fieldWrapper.viewWidget(
                              2,
                              null,
                              BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                bloc: bloc.provinces,
                                builder: (context, state) {
                                  return state.extraData.isError
                                      ? _reload(() {
                                          bloc.getProvinces();
                                        })
                                      : DropdownFieldBlocBuilder<Province>(
                                          selectFieldBloc: bloc.provinces,
                                          isEnabled:
                                              KycHelpers.fieldCheck(bloc.status)
                                                  ? state.extraData.isActive ??
                                                      true
                                                  : false,
                                          decoration: InputDecoration(
                                            errorMaxLines: 5,
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            labelText: "Provinsi",
                                            hintText: "Aceh",
                                            // hintText: bloc.provinceName != null &&
                                            //         bloc.status != "empty"
                                            //     ? bloc.provinceName
                                            //     : null,
                                            hintStyle: TextStyle(
                                              color: Colors.grey,
                                            ),
                                            suffixIcon: state.extraData.isLoaded
                                                ? Icon(Icons.arrow_drop_down)
                                                : CupertinoActivityIndicator(),
                                          ),
                                          itemBuilder: (context, value) =>
                                              value != null ? value.name : "",
                                        );
                                },
                              ),
                            ),
                            // Kota
                            fieldWrapper.viewWidget(
                              3,
                              null,
                              BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                bloc: bloc.cities,
                                builder: (context, state) {
                                  return state.extraData.isError
                                      ? _reload(() {
                                          bloc.getCities(
                                            bloc.provinces.value.id,
                                          );
                                        })
                                      : DropdownFieldBlocBuilder<Regency>(
                                          selectFieldBloc: bloc.cities,
                                          isEnabled:
                                              KycHelpers.fieldCheck(bloc.status)
                                                  ? state.extraData.isActive ??
                                                      true
                                                  : false,
                                          decoration: InputDecoration(
                                            errorMaxLines: 5,
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            labelText: "Kota",
                                            hintText: "Banda Aceh",
                                            // hintText: bloc.regencyName != null &&
                                            //         bloc.status != "empty"
                                            //     ? bloc.regencyName
                                            //     : null,
                                            hintStyle: TextStyle(
                                              color: Colors.grey,
                                            ),
                                            suffixIcon: state.extraData.isLoaded
                                                ? Icon(Icons.arrow_drop_down)
                                                : CupertinoActivityIndicator(),
                                          ),
                                          itemBuilder: (context, value) =>
                                              value != null ? value.name : "",
                                        );
                                },
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              4,
                              null,
                              TextFieldBlocBuilder(
                                isEnabled: KycHelpers.fieldCheck(bloc.status),
                                textFieldBloc: bloc.address,
                                minLines: 3,
                                maxLines: 3,
                                decoration: InputDecoration(
                                  errorMaxLines: 5,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.grey),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                  ),
                                  hintText:
                                      "Contoh : Perumahan Sidoarum Blok E-14, Yogyakarta",
                                  labelText: "Alamat Perusahaan",
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintStyle: TextStyle(color: Colors.grey),
                                ),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            fieldWrapper.viewWidget(
                              5,
                              null,
                              SantaraBlocTextField(
                                textFieldBloc: bloc.postalCode,
                                hintText:
                                    "*Kosongkan jika tidak ada (Optional)",
                                labelText: "Kode Pos",
                                inputType: TextInputType.number,
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(5),
                                ],
                              ),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Nomor Fax & Telepon",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s30,
                            ),
                            fieldWrapper.viewWidget(
                              8,
                              null,
                              SantaraBlocTextField(
                                textFieldBloc: bloc.handPhone,
                                hintText: "",
                                labelText: "No. Handphone Perusahaan",
                                inputType: TextInputType.number,
                                enabled: false,
                                onTapIfDisabled: () {
                                  PopupHelper.showDataChange(context);
                                },
                                suffixIcon: Icon(Icons.info),
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              6,
                              Sizes.s100,
                              SantaraBlocTextField(
                                enabled: KycHelpers.fieldCheck(bloc.status)
                                    ? true
                                    : false,
                                textFieldBloc: bloc.phone,
                                hintText:
                                    "*Kosongkan jika tidak ada (Optional)",
                                labelText: "No. Telepon Perusahaan",
                                inputType: TextInputType.number,
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              7,
                              Sizes.s100,
                              SantaraBlocTextField(
                                textFieldBloc: bloc.fax,
                                hintText:
                                    "*Kosongkan jika tidak ada (Optional)",
                                labelText: "No. Fax",
                                inputType: TextInputType.number,
                                enabled: KycHelpers.fieldCheck(bloc.status),
                              ),
                            ),
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            !KycHelpers.fieldCheck(bloc.status)
                                ? SantaraDisabledButton()
                                : SantaraMainButton(
                                    title: "Simpan",
                                    onPressed: () {
                                      AlamatPerusahaanHelper
                                          .dokumen.isSubmitted = true;
                                      FocusScope.of(context).unfocus();
                                      bloc.submit();
                                    },
                                  )
                          ]));
                }
              })),
    );
  }
}
