import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Province.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/KycFieldStatus.dart';
import 'package:santaraapp/models/kyc/company/AlamatPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';

class AlamatPerusahaanBloc extends FormBloc<String, String> {
  final String submissionId;
  final String status;

  final KycPerusahaanService _service = KycPerusahaanService();
  User company = User();
  // identifier untuk nandain semua data perusahaan telah diupdate kedalam field
  TextFieldBloc completed = TextFieldBloc();
  // identifier untuk kota, provinsi
  String regencyName;
  String provinceName;

  // inisialisasi service api
  List<Country> allCountries = List<Country>();
  // END
  // bloc untuk country
  final countries = SelectFieldBloc<Country, FieldStatus>();
  // bloc untuk provinces
  final provinces = SelectFieldBloc<Province, FieldStatus>();
  // bloc untuk cities
  final cities = SelectFieldBloc<Regency, FieldStatus>();
  // raw field status = status defaultnya
  final rawFieldStatus = FieldStatus(
    isActive: true,
    isLoaded: true,
  );
  // bloc untuk alamat sesuai kartu identitas
  final address = TextFieldBloc();
  // bloc untuk kode pos
  final postalCode = TextFieldBloc();
  // bloc untuk no telpon
  final phone = TextFieldBloc();
  // bloc untuk no handphone
  final handPhone = TextFieldBloc();
  // bloc untuk no fax
  final fax = TextFieldBloc();

  AlamatPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true, isEditing: false) {
    countries.updateExtraData(rawFieldStatus);
    provinces.updateExtraData(rawFieldStatus);
    cities.updateExtraData(rawFieldStatus);
    // menambahkan bloc tiap field
    addFieldBlocs(fieldBlocs: [
      countries,
      provinces,
      cities,
      address,
      postalCode,
      phone,
      handPhone,
      fax
    ]);
    // fetch data negara
    getCountries().then((value) async {
      await updateCompanyData().then((value) async {
        // handling ketika ada perubahan data negara dari user
        await handleCountryChanges();
        if (status == "empty") onFieldsChange();
      });
    });

    completed.onValueChanges(onData: (previous, current) async* {
      onFieldsChange();
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) async {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "3").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e, stack) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    // emitLoading();
    try {
      await getCompanyData();
      // negara
      // cari negara yg dipilih oleh user sebelumnya
      if (company.trader != null &&
          company.trader.companyCountryAddress != null) {
        var usrCountry = allCountries.where(
            (element) => element.id == company.trader.companyCountryAddress);
        if (usrCountry != null) countries.updateInitialValue(usrCountry.first);
      }

      if (provinces.state.extraData.isLoaded) {
        // update provinsi
        if (company.trader.companyProvinceAddress != null) {
          var selectedProvince = provinces.state.items.where(
              (element) => element.id == company.trader.companyProvinceAddress);
          if (selectedProvince != null && selectedProvince.length > 0) {
            provinces.updateInitialValue(selectedProvince.first);
          }
        }
      }

      // alamat perusahaan
      if (company.trader.companyAddress != null) {
        address.updateInitialValue(company.trader.companyAddress);
      }

      // kode pos
      if (company.trader.postalCode != null) {
        postalCode.updateInitialValue(company.trader.postalCode);
      }

      // nomor handphone perusahaan
      if (company.phone != null) {
        handPhone.updateInitialValue(company.phone);
      }

      // nomor telepon perusahaan
      if (company.trader.companyPhoneNumber != null) {
        phone.updateInitialValue(company.trader.companyPhoneNumber);
      }

      // nomor fax
      if (company.trader.fax != null) {
        fax.updateInitialValue(company.trader.fax);
      }
      // end

      if (status == "rejected") {
        await getFieldErrorStatus();
      }

      emitLoaded();
    } catch (e) {
      emitLoaded();
    }
  }

  onFieldsChange() {
    detectChanges(countries);
    detectChanges(provinces);
    detectChanges(cities);
    detectChanges(address);
    detectChanges(postalCode);
    detectChanges(phone);
    detectChanges(fax);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      AlamatPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // validators
  bool isCountryError() {
    if (countries.value == null) {
      countries.addFieldError("Negara tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isProvinceError() {
    if (provinces.value == null) {
      provinces.addFieldError("Provinsi tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isRegencyError() {
    if (cities.value == null) {
      cities.addFieldError("Kota tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isAddressError() {
    if (address.value == null) {
      address.addFieldError("Alamat tidak boleh kosong!");
      return true;
    } else if (address.value.length < 1) {
      address.addFieldError("Alamat tidak boleh kosong!");
      return true;
    } else if (address.value.length > 180) {
      address.addFieldError("Alamat maksimal 180 karakter!");
      return true;
    } else {
      return false;
    }
  }

  bool isPostalCodeError() {
    if (postalCode.value.length > 5) {
      postalCode.addFieldError("Kode pos maksimal 5 karakter!");
      return true;
    } else {
      return false;
    }
  }

  bool isFaxError() {
    if (fax.value != null && fax.value.length > 0) {
      if (fax.value.length > 30) {
        fax.addFieldError("Nomor fax maksimal 30 karakter!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      case "company_country_address":
        addErrorMessage(countries, _errMsg, "company_country_address");
        break;
      case "company_province_address":
        addErrorMessage(provinces, _errMsg, "company_province_address");
        break;
      case "company_regency_address":
        addErrorMessage(cities, _errMsg, "company_regency_address");
        break;
      case "company_address":
        addErrorMessage(address, _errMsg, "company_address");
        break;
      case "company_postal_code":
        addErrorMessage(postalCode, _errMsg, "company_postal_code");
        break;
      case "company_phone_number":
        addErrorMessage(phone, _errMsg, "company_phone_number");
        break;
      case "company_fax":
        addErrorMessage(fax, _errMsg, "company_fax");
        break;
      default:
        emitFailure(
          failureResponse: "ERRORSUBMIT Tidak dapat menerima status penolakan",
        );
        break;
    }
  }

  @override
  void onLoading() {
    // emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      if (isCountryError()) {
        emitFailure(failureResponse: "company_country_address");
      } else if (isProvinceError()) {
        emitFailure(failureResponse: "company_province_address");
      } else if (isRegencyError()) {
        emitFailure(failureResponse: "company_regency_address");
      } else if (isAddressError()) {
        emitFailure(failureResponse: "company_address");
      } else if (isPostalCodeError()) {
        emitFailure(failureResponse: "company_postal_code");
      } else if (isFaxError()) {
        emitFailure(failureResponse: "fax");
      } else {
        AlamatPerusahaanData data = AlamatPerusahaanData(
          country: "${countries.value.id}",
          province: "${provinces.value.id}",
          city: "${cities.value.id}",
          address: "${address.value}",
          postalCode: "${postalCode.value}",
          phone: "${phone.value}",
          fax: "${fax.value}",
        );

        await _service.postAlamat(data, status).then((value) {
          if (value.statusCode == 200) {
            emitSuccess(
              canSubmitAgain: false,
              successResponse: "Berhasil menyimpan pajak perizinan",
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else if (value.statusCode == 400) {
            try {
              value.data.forEach((res) {
                var dummy = ValidationResultModel.fromJson(res);
                handleApiValidation(dummy.field, dummy.message);
              });
            } catch (e) {
              emitFailure(
                failureResponse: "ERRORSUBMIT ${value.data['message']}",
              );
            }
          } else {
            // jika status code bukan 200 / 400
            emitFailure(
              failureResponse:
                  "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
            );
          }
        });
      }
    } catch (e) {
      emitFailure(
          failureResponse:
              "ERRORSUBMIT Terjadi kesalahan saat mengirimkan data!");
    }
  }

  handleCountryChanges() async {
    // ketika value negara berubah, maka update field dibawahnya
    // 1. Provinsi & Kota
    // 2. Jika != Indonesia, default value = OTHERS
    // 3. Jika == Indonesia, lanjutkan dengan fetch data provinsi
    countries.onValueChanges(
        // catch value change event
        // ignore: missing_return
        onData: (previous, current) async* {
      // apakah Indonesia?
      if (current.value.name.toLowerCase() != "indonesia") {
        try {
          // provinces setup if country isn't indonesia
          provinces
              .clear(); // menghapus provinsi jika user sebelumnya telah mengisi provinsi
          provinces.state.items.clear(); // menghapus item provinsi
          provinces.addItem(Province(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam provinsi
          provinces.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // mengupdate value extra data
          );
          // cities setup if country isn't indonesia
          cities.clear(); // menghapus field kota jika user udah ngisi kotanya
          cities.state.items
              .clear(); // menghapus daftar kota didalam dropdownnya
          cities.addItem(Regency(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam kota
          cities.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // update value extra data
          );
        } catch (e) {
          // debug kalau ada error
          // print(e);
        }
      } else {
        // jika value == Indonesia
        provinces
            .clear(); // clear field ( mulai lagi dari awal, ada case dimana user udah ngisi semua field, lalu reset negaranya)
        provinces.state.items.clear(); // clear item
        provinces
            .updateExtraData(rawFieldStatus); // update field status to default
        provinces.updateValue(null); // clear default value
        getProvinces();
        cities.clear(); // clear field
        cities.state.items.clear(); // clear items
        cities.updateValue(null); // clear default value
      }
    });

    // ignore: missing_return
    provinces.onValueChanges(onData: (previous, current) async* {
      if (current.value.id != 999) {
        cities.clear();
        cities.state.items.clear();
        getCities(current.value.id).then((value) async {
          // kota terpilih,
          // handling jika value == null
          if (company.trader.companyRegencyAddress != null) {
            var selectedRegency = cities.state.items.where((element) =>
                element.id == company.trader.companyRegencyAddress);
            if (selectedRegency != null && selectedRegency.length > 0) {
              cities.updateInitialValue(selectedRegency.first);
            }
          }
          completed.updateInitialValue("loaded");
        });
      }
    });

    provinces.listen((data) {
      if (countries.value != null &&
          countries.value.name.toLowerCase() != "indonesia") {
        provinces.updateInitialValue(provinces.state.items.first);
      }
    });

    cities.listen((data) {
      if (countries.value != null &&
          countries.value.name.toLowerCase() != "indonesia") {
        cities.updateInitialValue(cities.state.items.first);
      }
    });
  }

  // API : Fetch daftar negara
  Future getCountries() async {
    emitLoading();
    // update extra data, karena saat ini sedang memuat data, maka status aktif & loaded = false
    countries.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    try {
      await _service.getCountries().then((value) {
        // jika nilai tidak null
        if (value != null) {
          // jika respons dari server OK
          if (value.statusCode == 200) {
            // embed value negara kedalam bloc countries
            value.data.forEach((val) {
              var dummy = Country.fromJson(val);
              allCountries.add(dummy);
              countries.addItem(dummy);
            });
            // update extra data, akitf = true, isloaded = true, error = tidak error
            // Fix dari mas mario
            // Negara harus set ke Indonesia, jika tidak, maka gunakan updateExtraData yg dibawah
            countries.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
            // var idn = allCountries
            //     .where((element) => element.name.toLowerCase() == "indonesia");
            // countries.updateInitialValue(idn.first);
            // countries.updateExtraData(
            //   FieldStatus(isActive: false, isLoaded: true, isError: false),
            // );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            // jika respons dari server 500 / error
            countries.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
          }
        } else {
          // jika ada error
          countries.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      // jika ada erro lagi
      countries.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
    }
  }

  // API : Fetch daftar provinsi
  Future getProvinces() async {
    // List<Province> allProvinces = List<Province>();
    // mekanismenya sama dengan fetch countries
    provinces.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    // await Future.delayed(Duration(milliseconds: 2000));
    try {
      await _service.getProvinces().then((value) {
        if (value != null) {
          if (value.statusCode == 200) {
            value.data.forEach((val) {
              var dummy = Province.fromJson(val);
              if (dummy.id == company.trader.companyProvinceAddress) {
                provinceName = dummy.name;
                provinces.updateInitialValue(dummy);
              }
              provinces.addItem(dummy);
            });
            provinces.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            provinces.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: true),
            );
          }
        } else {
          provinces.updateExtraData(
            FieldStatus(isActive: true, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      provinces.updateExtraData(
        FieldStatus(isActive: true, isLoaded: true, isError: true),
      );
    }
  }

  // API : Fetch daftar kota
  Future getCities(int id) async {
    cities.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    // await Future.delayed(Duration(milliseconds: 2000));
    try {
      await _service.getCities(id).then((value) {
        if (value != null) {
          if (value.statusCode == 200) {
            value.data.forEach((val) {
              var dummy = Regency.fromJson(val);
              if (dummy.id == company.trader.companyRegencyAddress) {
                regencyName = dummy.name;
                cities.updateInitialValue(dummy);
              }
              cities.addItem(dummy);
            });
            cities.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            cities.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: true),
            );
          }
        } else {
          cities.updateExtraData(
            FieldStatus(isActive: true, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      cities.updateExtraData(
        FieldStatus(isActive: true, isLoaded: true, isError: true),
      );
    }
  }
}
