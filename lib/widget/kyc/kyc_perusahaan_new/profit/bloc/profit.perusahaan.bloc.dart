import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/ProfitPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';

class ProfitPerusahaanBloc extends FormBloc<String, String> {
  User company = User();
  final KycPerusahaanService _service = KycPerusahaanService();
  final String submissionId;
  final String status;

  // value profit untuk dropdown
  static var rawProfitCorp = [
    SubDataBiodata(id: "1", name: "< Rp 1 Milyar"),
    SubDataBiodata(id: "2", name: "Rp 1 Milyar - Rp 5 Milyar"),
    SubDataBiodata(id: "3", name: "Rp 5 Milyar - Rp 10 Milyar"),
    SubDataBiodata(id: "4", name: "Rp 10 Milyar - Rp 50 Milyar"),
    SubDataBiodata(id: "5", name: "> Rp 50 Milyar"),
  ];

  // value motivasi untuk dropdown
  static var rawInvestmentCorp = [
    SubDataBiodata(id: "Lain-lain", name: "Lain-lain"),
    SubDataBiodata(id: "Price appreciation", name: "Price appreciation"),
    SubDataBiodata(
        id: "Investasi jangka panjang", name: "Investasi jangka panjang"),
    SubDataBiodata(id: "Spekulasi", name: "Spekulasi"),
    SubDataBiodata(id: "Pendapatan", name: "Pendapatan"),
  ];

  final lastYearProfit = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawProfitCorp,
  );
  final twoYearProfit = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawProfitCorp,
  );
  final threeYearProfit = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawProfitCorp,
  );
  final investmentCorp = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawInvestmentCorp,
  );

  ProfitPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true, isEditing: false) {
    addFieldBlocs(fieldBlocs: [
      lastYearProfit,
      twoYearProfit,
      threeYearProfit,
      investmentCorp,
    ]);
    updateCompanyData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "6").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // Update value setiap fields
      // profit 1 tahun terakhir
      if (company.trader.companyIncome1 != null) {
        var selected = lastYearProfit.state.items
            .where((element) => element.id == company.trader.companyIncome1);
        if (selected != null) lastYearProfit.updateInitialValue(selected.first);
      }

      // profit 2 tahun terakhir
      if (company.trader.companyIncome2 != null) {
        var selected = twoYearProfit.state.items
            .where((element) => element.id == company.trader.companyIncome2);
        if (selected != null) twoYearProfit.updateInitialValue(selected.first);
      }

      // profit 3 tahun terakhir
      if (company.trader.companyIncome3 != null) {
        var selected = threeYearProfit.state.items
            .where((element) => element.id == company.trader.companyIncome3);
        if (selected != null)
          threeYearProfit.updateInitialValue(selected.first);
      }

      // tujuan investasi
      if (company.trader.reasonToJoin != null) {
        var selected = investmentCorp.state.items.where((element) =>
            element.name.toLowerCase() ==
            company.trader.reasonToJoin.toLowerCase());
        if (selected != null) investmentCorp.updateInitialValue(selected.first);
      }

      if (status == "rejected") {
        await getFieldErrorStatus();
      }

      emitLoaded();
    } catch (e) {
      emitLoaded();
    }
  }

  onFieldsChange() {
    detectChanges(lastYearProfit);
    detectChanges(twoYearProfit);
    detectChanges(threeYearProfit);
    detectChanges(investmentCorp);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      ProfitPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // validators
  bool isLastYearProfitError() {
    if (lastYearProfit.value == null) {
      lastYearProfit.addFieldError("Profit 1 tahun terakhir wajib diisi!");
      return true;
    } else {
      return false;
    }
  }

  bool isInvestmentCorpError() {
    if (investmentCorp.value == null) {
      investmentCorp.addFieldError("Tujuan investasi tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      // jika yg error field name
      case "company_income1":
        addErrorMessage(lastYearProfit, _errMsg, "company_income1");
        break;
      case "company_income2":
        addErrorMessage(twoYearProfit, _errMsg, "company_income2");
        break;
      case "company_income3":
        addErrorMessage(threeYearProfit, _errMsg, "company_income3");
        break;
      case "company_reason_to_join":
        addErrorMessage(investmentCorp, _errMsg, "company_reason_to_join");
        break;
      case "reason_to_join":
        addErrorMessage(investmentCorp, _errMsg, "company_reason_to_join");
        break;
      default:
        emitFailure(
            failureResponse:
                "ERRORSUBMIT Tidak dapat mengirim data, cek kembali form anda!");
        break;
    }
  }

  @override
  void onSubmitting() async {
    try {
      if (isLastYearProfitError()) {
        emitFailure(failureResponse: "company_income1");
      } else if (isInvestmentCorpError()) {
        emitFailure(failureResponse: "reason_to_join");
      } else {
        ProfitPerusahaanData data = ProfitPerusahaanData(
          lastYearProfit: lastYearProfit?.value?.id ?? '',
          twoYearProfit:
              twoYearProfit.value == null ? null : twoYearProfit.value.id,
          threeYearProfit:
              threeYearProfit.value == null ? null : threeYearProfit.value.id,
          investmentCorp: investmentCorp?.value?.id ?? '',
        );
        await _service.postProfitPerusahaan(data, status).then((value) {
          if (value.statusCode == 200) {
            emitSuccess(
              canSubmitAgain: false,
              successResponse: "Berhasil menyimpan Profit Perusahaan",
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else if (value.statusCode == 400) {
            try {
              value.data.forEach((res) {
                var dummy = ValidationResultModel.fromJson(res);
                handleApiValidation(dummy.field, dummy.message);
              });
            } catch (e) {
              // print(e);
              emitFailure(
                failureResponse: "ERRORSUBMIT ${value.data['message']}",
              );
            }
          } else {
            // jika status code bukan 200 / 400
            emitFailure(
              failureResponse:
                  "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
            );
          }
        });
      }
    } catch (e) {
      emitFailure(
        failureResponse: "ERRORSUBMIT Terjadi kesalahan saat mengirimkan data!",
      );
    }
  }
}
