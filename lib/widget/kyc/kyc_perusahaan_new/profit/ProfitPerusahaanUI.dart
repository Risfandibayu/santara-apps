import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/company/ProfitPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/profit.perusahaan.bloc.dart';

class ProfitPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;

  ProfitPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (ProfitPerusahaanHelper.dokumen.isEdited != null) {
            if (ProfitPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                ProfitPerusahaanHelper.dokumen = ProfitPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              ProfitPerusahaanHelper.dokumen = ProfitPerusahaanData();
              return true;
            }
          } else {
            ProfitPerusahaanHelper.dokumen = ProfitPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Profit Perusahaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "6/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => ProfitPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: ProfitPerusahaanForm(),
          )),
    );
  }
}

class ProfitPerusahaanForm extends StatefulWidget {
  @override
  _ProfitPerusahaanFormState createState() => _ProfitPerusahaanFormState();
}

class _ProfitPerusahaanFormState extends State<ProfitPerusahaanForm> {
  ProfitPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    ProfitPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<ProfitPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<ProfitPerusahaanBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, "${state.successResponse}");
            Navigator.pop(context);
            ProfitPerusahaanHelper.dokumen = ProfitPerusahaanData();
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            if (ProfitPerusahaanHelper.dokumen.isSubmitted != null) {
              Navigator.pop(context);
              ProfitPerusahaanHelper.dokumen = ProfitPerusahaanData();
            }
            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
              return;
            }

            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              switch (state.failureResponse.toLowerCase()) {
                case "":
                  fieldWrapper.scrollTo(1);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<ProfitPerusahaanBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                if (state is FormBlocLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is FormBlocLoadFailed) {
                  return Center(
                    child: Text("Failed to load!"),
                  );
                } else {
                  return Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Colors.white,
                      child: ListView(
                          scrollDirection: scrollDirection,
                          padding: EdgeInsets.all(Sizes.s20),
                          controller: controller,
                          children: [
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Informasi Profit",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            fieldWrapper.viewWidget(
                              1,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                selectFieldBloc: bloc.lastYearProfit,
                                label: "Profit Operasional 1 Tahun terakhir",
                                hintText: "< Rp 1 Milyar",
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              2,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                selectFieldBloc: bloc.twoYearProfit,
                                label: "Profit Operasional 2 Tahun terakhir",
                                hintText:
                                    "*Kosongkan jika operasional <2 Tahun ",
                              ),
                            ),
                            fieldWrapper.viewWidget(
                              3,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                selectFieldBloc: bloc.threeYearProfit,
                                label: "Profit Operasional 3 Tahun terakhir",
                                hintText:
                                    "*Kosongkan jika operasional <3 Tahun ",
                              ),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            // data diri title
                            Text(
                              "Informasi Profit",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            // divider line
                            Container(
                              margin: EdgeInsets.only(top: Sizes.s10),
                              height: 4,
                              width: double.maxFinite,
                              color: Color(0xffF4F4F4),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            fieldWrapper.viewWidget(
                              4,
                              Sizes.s100,
                              SantaraBlocDropDown(
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                selectFieldBloc: bloc.investmentCorp,
                                label: "Tujuan Investasi",
                                hintText: "Lain-Lain",
                              ),
                            ),
                            // divider
                            SizedBox(
                              height: Sizes.s20,
                            ),
                            !KycHelpers.fieldCheck(bloc.status)
                                ? SantaraDisabledButton()
                                : SantaraMainButton(
                                    title: "Simpan",
                                    onPressed: () {
                                      ProfitPerusahaanHelper
                                          .dokumen.isSubmitted = true;
                                      FocusScope.of(context).unfocus();
                                      bloc.submit();
                                    },
                                  )
                          ]));
                }
              })),
    );
  }
}
