import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/company/BiodataPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraCachedImage.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/biodata.perusahaan.bloc.dart';

class BiodataPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  BiodataPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (BiodataPerusahaanHelper.dokumen.isEdited != null) {
            if (BiodataPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                BiodataPerusahaanHelper.dokumen = BiodataPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              BiodataPerusahaanHelper.dokumen = BiodataPerusahaanData();
              return true;
            }
          } else {
            BiodataPerusahaanHelper.dokumen = BiodataPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Biodata Perusahaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "1/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => BiodataPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: BiodataPerusahaanForm(),
          )),
    );
  }
}

class BiodataPerusahaanForm extends StatefulWidget {
  @override
  _BiodataPerusahaanFormState createState() => _BiodataPerusahaanFormState();
}

class _BiodataPerusahaanFormState extends State<BiodataPerusahaanForm> {
  BiodataPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  // avatar picker
  Widget avatarPicker(InputFieldBlocState state) {
    return CircleAvatar(
      radius: Sizes.s80,
      backgroundColor:
          state.value == null ? Color(0xfff4f4f4) : Colors.transparent,
      backgroundImage: state.value == null ? null : FileImage(state.value),
      child: state.value == null
          ? Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.camera_alt,
                  color: Color(0xffB8B8B8),
                  size: FontSize.s24,
                ),
                SizedBox(
                  height: Sizes.s5,
                ),
                Text(
                  "Unggah Foto",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xffB8B8B8),
                  ),
                )
              ],
            )
          : Container(),
    );
  }

  @override
  void initState() {
    super.initState();
    BiodataPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<BiodataPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<BiodataPerusahaanBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(context, "${state.successResponse}");
          Navigator.pop(context);
          BiodataPerusahaanHelper.dokumen = BiodataPerusahaanData();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          // print("${BiodataPerusahaanHelper.dokumen.isSubmitted}");
          if (BiodataPerusahaanHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }

          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }

          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse.toLowerCase()) {
              case "company_photo":
                fieldWrapper.scrollTo(1);
                break;
              case "name":
                fieldWrapper.scrollTo(2);
                break;
              case "company_type":
                fieldWrapper.scrollTo(3);
                break;
              case "company_type2_code":
                fieldWrapper.scrollTo(4);
                break;
              case "company_syariah":
                fieldWrapper.scrollTo(5);
                break;
              case "company_country_domicile":
                fieldWrapper.scrollTo(6);
                break;
              case "company_establishment_place":
                fieldWrapper.scrollTo(7);
                break;
              case "company_date_establishment":
                fieldWrapper.scrollTo(8);
                break;
              case "another_email":
                fieldWrapper.scrollTo(10);
                break;
              case "description":
                fieldWrapper.scrollTo(11);
                break;
              case "company_character":
                fieldWrapper.scrollTo(12);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<BiodataPerusahaanBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                child: ListView(
                  scrollDirection: scrollDirection,
                  padding: EdgeInsets.all(Sizes.s20),
                  controller: controller,
                  children: [
                    // divider
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    // data diri title
                    Text(
                      "Profil Perusahaan",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    // divider line
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s10),
                      height: 4,
                      width: double.maxFinite,
                      color: Color(0xffF4F4F4),
                    ),
                    // divider
                    SizedBox(
                      height: Sizes.s50,
                    ),
                    // foto profil perusahaan
                    BlocBuilder<InputFieldBloc, InputFieldBlocState>(
                      bloc: bloc.companyPhoto,
                      builder: (context, state) {
                        return Column(
                          children: [
                            BlocBuilder<InputFieldBloc, InputFieldBlocState>(
                              bloc: bloc.companyPhoto,
                              builder: (context, state) {
                                return fieldWrapper.viewWidget(
                                  1,
                                  Sizes.s165,
                                  Center(
                                      child: DashedCircle(
                                    gapSize: 1.0,
                                    dashes: 50,
                                    color: Colors.grey[350],
                                    child: InkWell(
                                      onTap: () {
                                        if (KycHelpers.fieldCheck(
                                            bloc.status)) {
                                          bloc.pickImage();
                                        }
                                      },
                                      child: state.value == null
                                          ? state.extraData != null
                                              ? ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(Sizes.s120),
                                                  ),
                                                  child: SantaraCachedImage(
                                                    image:
                                                        state.extraData["url"],
                                                    width: Sizes.s120,
                                                    height: Sizes.s120,
                                                  ),
                                                )
                                              : avatarPicker(state)
                                          : avatarPicker(state),
                                    ),
                                  )),
                                );
                              },
                            ),
                            SizedBox(height: Sizes.s10),
                            state.hasError
                                ? Text(
                                    "${state.error}",
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  )
                                : Container()
                          ],
                        );
                      },
                    ),
                    // Nama Perusahaan
                    fieldWrapper.viewWidget(
                      2,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.companyName,
                        hintText: "Contoh : PT ABC INDONESIA",
                        labelText: "Nama Perusahaan",
                        enabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.companyJenis,
                        label: "Jenis Perusahaan",
                        hintText: "Lainnya",
                        enabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                      ),
                    ),
                    // Karakteristik perusahaan berubah jadi tipe perusahaan
                    fieldWrapper.viewWidget(
                      12,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.companyCharacter,
                        label: "Tipe Perusahaan",
                        hintText: "Lain-lain",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    // fieldWrapper.viewWidget(
                    //   4,
                    //   Sizes.s100,
                    //   SantaraBlocDropDown(
                    //     selectFieldBloc: bloc.companyType,
                    //     label: "Tipe Perusahaan",
                    //     hintText: "Bank Indonesia",
                    //     enabled: KycHelpers.isVerified(bloc.status)
                    //         ? KycHelpers.fieldCheck(bloc.status)
                    //         : false,
                    //   ),
                    // ),
                    Text(
                      "Apakah perusahaan anda berbasis syariah ?",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s130,
                      SantaraBlocRadioGroup(
                        selectFieldBloc: bloc.companySyariah,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      6,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.companyCountryDomicile,
                        label: "Domisili Perusahaan",
                        enabled: false,
                        onTapIfDisabled: () {
                          PopupHelper.showMessage(
                            context,
                            "Anda tidak bisa merubah data ini sekarang",
                          );
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      7,
                      Sizes.s100,
                      SearchRegencyField(
                        regencyBloc: bloc.companyEstablishmentPlaceNew,
                        selectedRegency: bloc.companyEstablishmentPlace?.value,
                        hintText: "Pilih Tempat Pendirian Usaha",
                        label: "Tempat Pendirian Usaha",
                        enabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                        onTapIfDisabled: () {
                          PopupHelper.showMessage(
                            context,
                            "Anda tidak bisa merubah data ini sekarang",
                          );
                        },
                      ),
                    ),
                    Text(
                      "Tanggal Pendirian Usaha",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      8,
                      Sizes.s110,
                      Padding(
                        padding: EdgeInsets.only(top: Sizes.s10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: Sizes.s100,
                                child: SantaraBlocTextField(
                                  enabled: KycHelpers.isVerified(bloc.status)
                                      ? KycHelpers.fieldCheck(bloc.status)
                                      : false,
                                  textFieldBloc: bloc.dayEstablish,
                                  hintText: "01",
                                  labelText: "Tanggal",
                                  inputType: TextInputType.datetime,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(2),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: Sizes.s10,
                                  right: Sizes.s10,
                                ),
                                height: Sizes.s100,
                                child: SantaraBlocDropDown(
                                  enabled: KycHelpers.isVerified(bloc.status)
                                      ? KycHelpers.fieldCheck(bloc.status)
                                      : false,
                                  selectFieldBloc: bloc.monthEstablish,
                                  label: "Bulan",
                                  hintText: "Januari",
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: Sizes.s100,
                                child: SantaraBlocTextField(
                                  enabled: KycHelpers.isVerified(bloc.status)
                                      ? KycHelpers.fieldCheck(bloc.status)
                                      : false,
                                  textFieldBloc: bloc.yearEstablish,
                                  hintText: "2020",
                                  labelText: "Tahun",
                                  inputType: TextInputType.datetime,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(4),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    KycErrorWrapper(bloc: bloc.companyDateEstablishment),
                    // email
                    fieldWrapper.viewWidget(
                      9,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.companyDefaultEmail,
                        hintText: "Email",
                        labelText: "Email",
                        inputType: TextInputType.emailAddress,
                        enabled: false,
                        suffixIcon: Icon(Icons.info),
                        onTapIfDisabled: () {
                          PopupHelper.showDataChange(context);
                        },
                      ),
                    ),
                    // email lain
                    fieldWrapper.viewWidget(
                      10,
                      Sizes.s100,
                      SantaraBlocTextField(
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        textFieldBloc: bloc.companyAnotherEmail,
                        hintText: "*Kosongkan jika tidak ada (Opsional)",
                        labelText: "Email Lain (Opsional)",
                        inputType: TextInputType.emailAddress,
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      11,
                      Sizes.s100,
                      TextFieldBlocBuilder(
                        isEnabled: KycHelpers.fieldCheck(bloc.status),
                        textFieldBloc: bloc.companyDescription,
                        minLines: 3,
                        maxLines: 3,
                        decoration: InputDecoration(
                          errorMaxLines: 5,
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),
                          hintText: "*Kosongkan jika tidak ada (Opsional)",
                          labelText: "Deskripsi Singkat Usaha",
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    fieldWrapper.divider,
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              BiodataPerusahaanHelper.dokumen.isSubmitted =
                                  true;
                              FocusScope.of(context).unfocus();
                              bloc.submit();
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
