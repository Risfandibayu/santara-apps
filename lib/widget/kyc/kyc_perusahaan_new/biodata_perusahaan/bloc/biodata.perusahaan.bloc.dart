import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/helpers/ImageProcessor.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/BiodataPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/logger.dart';

class BiodataPerusahaanBloc extends FormBloc<String, String> {
  KycPerusahaanService _service = KycPerusahaanService();
  User company = User();
  final String submissionId;
  final String status;

  static var rawCompanyJenis = [
    SubDataBiodata(id: "1", name: "Others"),
    SubDataBiodata(id: "2", name: "CP (Corporate)"),
    SubDataBiodata(id: "3", name: "FD (Foundation)"),
    SubDataBiodata(id: "4", name: "IB (Financial Institution)"),
    SubDataBiodata(id: "5", name: "IS (Insurance)"),
    SubDataBiodata(id: "6", name: "MF (Mutual Fund)"),
    SubDataBiodata(id: "7", name: "PF (Pension Fund)"),
    SubDataBiodata(id: "8", name: "SC (Securities Company)"),
  ];
  static var rawCompanyChar = [
    SubDataBiodata(id: "1", name: "Lain-lain"),
    SubDataBiodata(id: "2", name: "BUMN"),
    SubDataBiodata(id: "3", name: "Perusahaan investasi lokal"),
    SubDataBiodata(id: "4", name: "Sosial"),
    SubDataBiodata(id: "5", name: "Joint venture"),
    SubDataBiodata(id: "6", name: "Perusahaan investasi asing"),
    SubDataBiodata(id: "7", name: "Perusahaan keluarga"),
    SubDataBiodata(id: "8", name: "Afiliasi"),
  ];
  static var rawCompanyType = [
    SubDataBiodata(id: "001", name: "Bank Indonesia"),
    SubDataBiodata(id: "200", name: "Bank Sentral di LN"),
    SubDataBiodata(id: "290", name: "Bank Umum di Indonesia"),
    SubDataBiodata(id: "299", name: "Bank Umum di Luar Negeri"),
    SubDataBiodata(id: "310", name: "Perusahaan Pembiayaan"),
    SubDataBiodata(id: "320", name: "Modal Ventura"),
    SubDataBiodata(id: "330", name: "Perusahaan Sekuritas"),
    SubDataBiodata(id: "340", name: "Perusahaan Asuransi"),
    SubDataBiodata(id: "350", name: "Dana Pensiun"),
    SubDataBiodata(id: "360", name: "Reksadana"),
    SubDataBiodata(id: "410", name: "BUMN Non Lembaga Keuangan"),
    SubDataBiodata(id: "430", name: "Perusahaan Lainnya"),
    SubDataBiodata(id: "XXX", name: "Perusahaan Asing"),
    SubDataBiodata(id: "999", name: "Lainnya"),
  ];
  static var rawCompanySyariah = [
    SubDataBiodata(id: "Y", name: "Ya"),
    SubDataBiodata(id: "N", name: "Tidak"),
  ];
  static var rawCountries = [SubDataBiodata(id: "78", name: "Indonesia")];
  // value bulan untuk dropdown
  static var rawMonths = [
    SubDataBiodata(id: "01", name: "Januari"),
    SubDataBiodata(id: "02", name: "Februari"),
    SubDataBiodata(id: "03", name: "Maret"),
    SubDataBiodata(id: "04", name: "April"),
    SubDataBiodata(id: "05", name: "Mei"),
    SubDataBiodata(id: "06", name: "Juni"),
    SubDataBiodata(id: "07", name: "Juli"),
    SubDataBiodata(id: "08", name: "Agustus"),
    SubDataBiodata(id: "09", name: "September"),
    SubDataBiodata(id: "10", name: "Oktober"),
    SubDataBiodata(id: "11", name: "November"),
    SubDataBiodata(id: "12", name: "Desember"),
  ];
  final companyPhoto = InputFieldBloc<File, Object>();
  final companyName = TextFieldBloc();
  final companyType = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawCompanyType,
  );
  final companyJenis = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawCompanyJenis,
  );
  final companyCharacter = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawCompanyChar,
  );
  final companySyariah = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawCompanySyariah,
  );
  final companyCountryDomicile = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawCountries,
  );
  // ada perubahan mendadak, tempat pendirian usaha harus dibikin dropdown
  final companyEstablishmentPlace = TextFieldBloc();
  // new value pakai ini
  // ignore: close_sinks
  final companyEstablishmentPlaceNew = SelectFieldBloc<Regency, dynamic>();
  // date establishment
  // ignore: close_sinks
  final companyDateEstablishment = TextFieldBloc();
  final dayEstablish = TextFieldBloc();
  final monthEstablish = SelectFieldBloc<SubDataBiodata, dynamic>(
    items: rawMonths,
  );
  final yearEstablish = TextFieldBloc();
  // email
  final companyDefaultEmail = TextFieldBloc();
  final companyAnotherEmail = TextFieldBloc();
  final companyDescription = TextFieldBloc();

  void pickImage() async {
    await ImagePicker.pickImage(source: ImageSource.gallery)
        .then((image) async {
      final bool allowed = await ImageProcessor.checkImageExtension(
          image, [".png", ".jpg", ".jpeg"]);
      if (allowed) {
        companyPhoto.updateValue(image);
      } else {
        companyPhoto.addFieldError("Format File Tidak Diijinkan!");
      }
    });
  }

  BiodataPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      companyPhoto,
      companyName,
      companyJenis,
      companyType,
      companyCharacter,
      companySyariah,
      companyCountryDomicile,
      companyEstablishmentPlace,
      companyEstablishmentPlaceNew,
      companyDateEstablishment,
      dayEstablish,
      monthEstablish,
      yearEstablish,
      companyDefaultEmail,
      companyAnotherEmail,
      companyDescription,
    ]);
    // default indonesia
    companyCountryDomicile.updateInitialValue(rawCountries.first);
    // end
    updateCompanyData().then((value) async {
      // print(">> HELLOW");
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
    });
  }

  onFieldsChange() {
    detectChanges(companyPhoto);
    detectChanges(companyEstablishmentPlaceNew);
    detectChanges(companyName);
    detectChanges(companyType);
    detectChanges(companyJenis);
    detectChanges(companyCharacter);
    detectChanges(companySyariah);
    detectChanges(companyCountryDomicile);
    detectChanges(companyEstablishmentPlace);
    detectChanges(companyDateEstablishment);
    detectChanges(dayEstablish);
    detectChanges(monthEstablish);
    detectChanges(yearEstablish);
    detectChanges(companyDefaultEmail);
    detectChanges(companyAnotherEmail);
    detectChanges(companyDescription);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      BiodataPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        // print(value.toString());
        company = User.fromJson(value.data);
        // print(">> COMPANY NAME : ${company.trader.name}");
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "1").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // foto profil
      if (company.trader.companyPhoto != null) {
        companyPhoto.updateExtraData({
          "hasPhoto": true,
          "url": GetAuthenticatedFile.convertUrl(
            image: company.trader.companyPhoto,
            type: PathType.traderPhoto,
          ),
        });
      }
      // nama perusahaan
      if (company.trader.name != null) {
        companyName.updateInitialValue(company.trader.name);
      }
      // dropdown
      // update jenis
      if (company.trader.companyType != null) {
        var selectedJenis = companyJenis.state.items
            .where((element) => element.id == "${company.trader.companyType}")
            .first;
        companyJenis.updateInitialValue(selectedJenis);
      }
      // update karakteristik
      if (company.trader.companyCharacter != null) {
        var selectedChars = companyCharacter.state.items
            .where(
                (element) => element.id == "${company.trader.companyCharacter}")
            .first;
        companyCharacter.updateInitialValue(selectedChars);
      }
      // update tipe
      if (company.trader.companyType2Code != null) {
        var selectedType = companyType.state.items
            .where((element) => element.id == company.trader.companyType2Code);
        if (selectedType != null && selectedType.length > 0) {
          companyType.updateInitialValue(selectedType.first);
        }
      }

      // update syariah
      if (company.trader.companySyariah != null) {
        var selectedSyariah = companySyariah.state.items
            .where((element) => element.id == company.trader.companySyariah)
            .first;
        companySyariah.updateInitialValue(selectedSyariah);
      }
      // domisili perusahaan
      companyCountryDomicile.updateInitialValue(rawCountries.first);
      // tempat pendirian usaha
      if (company.trader.companyEstablishmentPlace != null) {
        companyEstablishmentPlace
            .updateInitialValue(company.trader.companyEstablishmentPlace);
      }
      // tanggal pendirian usaha
      if (company.trader.companyDateEstablishment != null) {
        var tglEst = DateTime.tryParse(company.trader.companyDateEstablishment);
        if (tglEst != null) {
          dayEstablish.updateInitialValue(
              "${tglEst.day < 10 ? '0' + tglEst.day.toString() : tglEst.day}");
          monthEstablish
              .updateInitialValue(monthEstablish.state.items[tglEst.month - 1]);
          yearEstablish.updateInitialValue(tglEst.year.toString());
        }
      }
      // email
      if (company.email != null) {
        companyDefaultEmail.updateInitialValue(company.email);
      }
      // another email
      if (company.trader.anotherEmail != null) {
        companyAnotherEmail.updateInitialValue(company.trader.anotherEmail);
      }
      // description
      if (company.trader.description != null) {
        companyDescription.updateInitialValue(company.trader.description);
      }

      if (status == "rejected") {
        await getFieldErrorStatus();
      }

      emitLoaded();
    } catch (e, stack) {
      // print(">> ERR : $e");
      // print(stack);
      emitLoaded();
    }
  }

  // VALIDATORS
  bool isCompanyPhotoError() {
    if (company.trader.companyPhoto != null) {
      return false;
    } else {
      if (companyPhoto.value == null) {
        companyPhoto.addFieldError("Foto tidak boleh kosong!");
        return true;
      } else {
        return false;
      }
    }
  }

  bool isCompanyNameError() {
    if (companyName.value == null) {
      companyName.addFieldError("Nama perusahaan tidak boleh kosong!");
      return true;
    } else if (companyName.value.length < 1) {
      companyName.addFieldError("Nama perusahaan tidak boleh kosong!");
      return true;
    } else if (companyName.value.contains(RegExp(r'[0-9]'), 1)) {
      companyName.addFieldError("Nama perusahaan tidak boleh mengandung angka");
      return true;
    } else if (companyName.value.length > 100) {
      companyName.addFieldError("Nama perusahaan maksimal 100 karakter");
      return true;
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(companyName.value)) {
        return false;
      } else {
        companyName.addFieldError("Nama tidak boleh mengandung simbol");
        return true;
      }
    }
  }

  bool isCompanyJenisError() {
    if (companyJenis.value == null) {
      companyJenis.addFieldError("Jenis perusahaan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanyTypeError() {
    if (companyType.value == null) {
      companyType.addFieldError("Tipe perusahaan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanyCharacteristicError() {
    if (companyCharacter.value == null) {
      // karakteristik perusahaan (menjadi jenis perusahaan)
      companyCharacter.addFieldError("Tipe Perusahaan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanySyariahError() {
    if (companySyariah.value == null) {
      companySyariah.addFieldError("Field ini harus diisi!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanyDomicileError() {
    if (companyCountryDomicile.value == null) {
      companyCountryDomicile
          .addFieldError("Domisili perusahaan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanyEstablishedPlaceError() {
    if (companyEstablishmentPlace.value == null &&
        companyEstablishmentPlaceNew.value == null) {
      companyEstablishmentPlaceNew
          .addFieldError("Tempat pendirian usaha tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isCompanyEmailError() {
    // if (companyDefaultEmail.value == null) {
    //   companyDefaultEmail.addFieldError("Email tidak boleh kosong!");
    //   return true;
    // } else {
    //   return false;
    // }
    return false;
  }

  bool isCompanyDescriptionError() {
    if (companyDescription.value.length > 100) {
      companyDescription
          .addFieldError("Deskripsi perusahaan maksimal 100 karakter!");
      return true;
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      // jika yg error field name
      case "name":
        addErrorMessage(companyName, _errMsg, "name");
        break;
      case "company_name":
        addErrorMessage(companyName, _errMsg, "name");
        break;
      case "company_type":
        addErrorMessage(companyJenis, _errMsg, "company_type");
        break;
      case "company_type2":
        addErrorMessage(companyJenis, _errMsg, "company_type");
        break;
      case "company_character":
        addErrorMessage(companyCharacter, _errMsg, "company_character");
        break;
      case "company_type2_code":
        addErrorMessage(companyType, _errMsg, "company_type2_code");
        break;
      case "company_syariah":
        addErrorMessage(companySyariah, _errMsg, "company_syariah");
        break;
      case "company_country_domicile":
        addErrorMessage(
            companyCountryDomicile, _errMsg, "company_country_domicile");
        break;
      case "company_establishment_place":
        addErrorMessage(companyEstablishmentPlaceNew, _errMsg,
            "company_establishment_place");
        break;
      case "company_date_establishment":
        addErrorMessage(
            companyDateEstablishment, _errMsg, "company_date_establishment");
        break;
      case "company_email":
        addErrorMessage(companyDefaultEmail, _errMsg, "company_email");
        break;
      case "another_email":
        addErrorMessage(companyAnotherEmail, _errMsg, "another_email");
        break;
      case "company_another_email":
        addErrorMessage(companyAnotherEmail, _errMsg, "another_email");
        break;
      case "company_photo":
        addErrorMessage(companyPhoto, _errMsg, "company_photo");
        break;
      case "description":
        addErrorMessage(companyDescription, _errMsg, "description");
        break;
      case "company_description":
        addErrorMessage(companyDescription, _errMsg, "description");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT $_errMsg");
        break;
    }
  }

  @override
  void onLoading() {
    // emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      if (isCompanyPhotoError()) {
        emitFailure(failureResponse: "company_photo");
      } else if (isCompanyNameError()) {
        emitFailure(failureResponse: "name");
      } else if (isCompanyJenisError()) {
        emitFailure(failureResponse: "company_type");
      } else if (isCompanyCharacteristicError()) {
        emitFailure(failureResponse: "company_character");
      } else if (isCompanySyariahError()) {
        emitFailure(failureResponse: "company_syariah");
      } else if (isCompanyDomicileError()) {
        emitFailure(failureResponse: "company_country_domicile");
      } else if (isCompanyEstablishedPlaceError()) {
        emitFailure(failureResponse: "company_establishment_place");
      }
      // untuk tanggal pendirian usaha
      // jika validasi terkait dengan tanggal, gunakan class KycHelpers
      else if (KycHelpers.dateChecker(dayEstablish, monthEstablish,
          yearEstablish, companyDateEstablishment, true)) {
        emitFailure(failureResponse: "company_date_establishment");
      } else if (isCompanyEmailError()) {
        emitFailure(failureResponse: "another_email");
      }
      // untuk email lain usaha
      // jika validasi terkait dengan email, gunakan class kychelpers
      else if (isCompanyDescriptionError()) {
        emitFailure(failureResponse: "description");
      } else {
        try {
          BiodataPerusahaanData data = BiodataPerusahaanData(
            companyPhoto: companyPhoto?.value ?? null,
            companyName: companyName?.value ?? '',
            companyJenis: companyJenis?.value?.id ?? '',
            // jenis perusahaan dihilangkan, valuenya pakai karakteristik perusahaan
            // companyJenis: '',
            // companyType: companyType?.value?.id ?? '',
            companyCharacter: companyCharacter?.value?.id ?? '',
            companySyariah: companySyariah?.value?.id ?? '',
            companyCountryDomicile: companyCountryDomicile?.value?.id ?? '',
            companyEstablishmentPlace:
                companyEstablishmentPlaceNew.value?.id == null
                    ? companyEstablishmentPlace?.value
                    : "${companyEstablishmentPlaceNew?.value?.id}",
            companyDateEstablishment:
                "${yearEstablish?.value}-${monthEstablish?.value?.id}-${dayEstablish?.value}",
            companyAnotherEmail: companyAnotherEmail?.value ?? '',
            companyDescription: companyDescription?.value ?? '',
          );
          await _service.postBiodataPerusahaan(data, status).then((value) {
            if (value.statusCode == 200) {
              emitSuccess(
                canSubmitAgain: false,
                successResponse: "Berhasil menyimpan Biodata Perusahaan",
              );
            } else if (value.statusCode == 401) {
              emitFailure(failureResponse: "UNAUTHORIZED");
            } else if (value.statusCode == 400) {
              try {
                value.data.forEach((res) {
                  var dummy = ValidationResultModel.fromJson(res);
                  handleApiValidation(dummy.field, dummy.message);
                });
              } catch (e) {
                emitFailure(
                  failureResponse: "ERRORSUBMIT ${value.data['message']}",
                );
              }
            } else {
              // jika status code bukan 200 / 400
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
              );
            }
          });
        } catch (e, stack) {
          santaraLog(e, stack);
          emitFailure(
            failureResponse: "ERRORSUBMIT [$e] Tidak dapat mengirimkan data!",
          );
        }
      }
    } catch (e, stack) {
      // print(stack);
      emitFailure(
          failureResponse: "ERRORSUBMIT Error saat mencoba mengirimkan data!");
    }
  }
}
