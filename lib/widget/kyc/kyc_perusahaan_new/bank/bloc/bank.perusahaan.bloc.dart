import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/KycFieldStatus.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/company/BankPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';

class BankPerusahaanBloc extends FormBloc<String, String> {
  final KycPerusahaanService _service = KycPerusahaanService();
  User company = User();
  final String submissionId;
  final String status;

  List<BankInvestor> banks = List<BankInvestor>();
  static var rawCurrency = [
    SubDataBiodata(id: "IDR", name: "IDR (Indonesian Rupiah)"),
    SubDataBiodata(id: "USD", name: "USD (United State Dollar)"),
    SubDataBiodata(id: "SGD", name: "SGD (Singapore Dollar)"),
  ];
  // FIELD 1
  final bank1 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name1 = TextFieldBloc();
  final account1 = TextFieldBloc();
  final currency1 =
      SelectFieldBloc<SubDataBiodata, FieldStatus>(items: rawCurrency);
  // FIELD 2
  final bank2 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name2 = TextFieldBloc();
  final account2 = TextFieldBloc();
  final currency2 =
      SelectFieldBloc<SubDataBiodata, FieldStatus>(items: rawCurrency);
  // FIELD 3
  final bank3 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name3 = TextFieldBloc();
  final account3 = TextFieldBloc();
  final currency3 =
      SelectFieldBloc<SubDataBiodata, FieldStatus>(items: rawCurrency);

  BankPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      bank1,
      name1,
      account1,
      currency1,
      bank2,
      name2,
      account2,
      currency2,
      bank3,
      name3,
      account3,
      currency3,
    ]);
    fetchBanks().then((value) async {
      await updateCompanyData().then((value) async {
        await Future.delayed(Duration(milliseconds: 100));
        onFieldsChange();
      });
    });
  }

  onFieldsChange() {
    detectChanges(bank1);
    detectChanges(name1);
    detectChanges(account1);
    detectChanges(currency1);
    detectChanges(bank2);
    detectChanges(name2);
    detectChanges(account2);
    detectChanges(currency2);
    detectChanges(bank3);
    detectChanges(name3);
    detectChanges(account3);
    detectChanges(currency3);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      BankPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "8").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      await getCompanyData();
      // bank 1
      if (company?.traderBank?.bankInvestor1 != null) {
        var selectedBank1 = bank1.state.items
            .where((element) => element.id == company.traderBank.bankInvestor1)
            .first;
        bank1.updateInitialValue(selectedBank1);
        name1.updateInitialValue(company.traderBank.accountName1);
        account1.updateInitialValue(company.traderBank.accountNumber1);
        var selectedCurrency1 = currency1.state.items
            .where(
                (element) => element.id == company.traderBank.accountCurrency1)
            .first;
        currency1.updateInitialValue(selectedCurrency1);
      }

      // bank 2
      if (company?.traderBank?.bankInvestor2 != null) {
        var selectedBank2 = bank2.state.items
            .where((element) => element.id == company.traderBank.bankInvestor2)
            .first;
        bank2.updateInitialValue(selectedBank2);
        name2.updateInitialValue(company.traderBank.accountName2);
        account2.updateInitialValue(company.traderBank.accountNumber2);
        var selectedCurrency2 = currency2.state.items
            .where(
                (element) => element.id == company.traderBank.accountCurrency2)
            .first;
        currency2.updateInitialValue(selectedCurrency2);
      }

      // bank 3
      if (company?.traderBank?.bankInvestor3 != null) {
        var selectedBank3 = bank3.state.items
            .where((element) => element.id == company.traderBank.bankInvestor3)
            .first;
        bank3.updateInitialValue(selectedBank3);
        name3.updateInitialValue(company.traderBank.accountName3);
        account3.updateInitialValue(company.traderBank.accountNumber3);
        var selectedCurrency3 = currency3.state.items
            .where(
                (element) => element.id == company.traderBank.accountCurrency3)
            .first;
        currency3.updateInitialValue(selectedCurrency3);
      }
      if (status == "rejected") {
        await getFieldErrorStatus();
      }
      emitLoaded();
    } catch (e, stack) {
      // print(">> ERROR : $e");
      // print(stack);
      emitLoaded();
    }
  }

  bool isBank1Error() {
    if (bank1.value == null) {
      bank1.addFieldError("Nama bank perusahaan tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isName1Error() {
    if (name1.value == null) {
      name1.addFieldError("Nama pemilik rekening tidak boleh kosong");
      return true;
    } else if (name1.value.length < 1) {
      name1.addFieldError("Nama pemilik rekening tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isAccount1Error() {
    if (account1.value == null) {
      account1.addFieldError("Nomor rekening tidak boleh kosong");
      return true;
    } else if (account1.value.length < 1) {
      account1.addFieldError("Nomor rekening tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isCurrency1Error() {
    if (currency1.value == null) {
      currency1.addFieldError("Mata uang bank perusahaan tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  Future fetchBanks() async {
    // update extra data, karena saat ini sedang memuat data, maka status aktif & loaded = false
    bank1.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    bank2.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    bank3.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    try {
      await _service.getBanks().then((value) {
        // jika nilai tidak null
        if (value != null) {
          // jika respons dari server OK
          if (value.statusCode == 200) {
            // embed value negara kedalam bloc countries
            value.data.forEach((val) {
              var dummy = BankInvestor.fromJson(val);
              banks.add(dummy);
              bank1.addItem(dummy);
              bank2.addItem(dummy);
              bank3.addItem(dummy);
            });
            // update extra data, akitf = true, isloaded = true, error = tidak error
            bank1.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
            bank2.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
            bank3.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            // jika respons dari server 500 / error
            bank1.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
            bank2.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
            bank3.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
          }
        } else {
          // jika ada error
          bank1.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
          bank2.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
          bank3.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      // jika ada erro lagi
      bank1.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
      bank2.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
      bank3.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // case field ( detek field apa yg error )
    switch (field) {
      case "bank_investor1":
        addErrorMessage(bank1, _errMsg, "bank_investor1");
        break;
      case "account_name1":
        addErrorMessage(name1, _errMsg, "account_name1");
        break;
      case "account_number1":
        addErrorMessage(account1, _errMsg, "account_number1");
        break;
      case "account_currency1":
        addErrorMessage(currency1, _errMsg, "account_currency1");
        break;
      case "bank_investor2":
        addErrorMessage(bank2, _errMsg, "bank_investor2");
        break;
      case "account_name2":
        addErrorMessage(name2, _errMsg, "account_name2");
        break;
      case "account_number2":
        addErrorMessage(account2, _errMsg, "account_number2");
        break;
      case "account_currency2":
        addErrorMessage(currency2, _errMsg, "account_currency2");
        break;
      case "bank_investor3":
        addErrorMessage(bank3, _errMsg, "bank_investor3");
        break;
      case "account_name3":
        addErrorMessage(name3, _errMsg, "account_name3");
        break;
      case "account_number3":
        addErrorMessage(account3, _errMsg, "account_number3");
        break;
      case "account_currency3":
        addErrorMessage(currency3, _errMsg, "account_currency3");
        break;
      default:
        emitFailure(
            failureResponse:
                "ERRORSUBMIT Tidak dapat mengirim data, cek kembali form anda!");
        break;
    }
  }

  @override
  void onSubmitting() async {
    try {
      if (isBank1Error()) {
        emitFailure(failureResponse: "bank_investor1");
      } else if (isName1Error()) {
        emitFailure(failureResponse: "account_name1");
      } else if (isAccount1Error()) {
        emitFailure(failureResponse: "account_number1");
      } else if (isCurrency1Error()) {
        emitFailure(failureResponse: "account_currency1");
      } else {
        try {
          BankPerusahaanData data = BankPerusahaanData(
            bank1: bank1?.value?.id,
            name1: name1?.value,
            account1: account1?.value,
            currency1: currency1?.value?.id,
            bank2: bank2.value == null ? null : bank2.value.id,
            name2: name2.value,
            account2: account2.value,
            currency2: currency2.value == null ? null : currency2.value.id,
            bank3: bank3.value == null ? null : bank3.value.id,
            name3: name3.value,
            account3: account3.value,
            currency3: currency3.value == null ? null : currency3.value.id,
          );
          await _service.postBankPerusahaan(data, status).then((value) {
            if (value.statusCode == 200) {
              emitSuccess(
                canSubmitAgain: false,
                successResponse: "Berhasil menyimpan bank perusahaan",
              );
            } else if (value.statusCode == 401) {
              emitFailure(failureResponse: "UNAUTHORIZED");
            } else if (value.statusCode == 400) {
              try {
                value.data.forEach((res) {
                  var dummy = ValidationResultModel.fromJson(res);
                  handleApiValidation(dummy.field, dummy.message);
                });
              } catch (e) {
                emitFailure(
                  failureResponse: "ERRORSUBMIT ${value.data['message']}",
                );
              }
            } else {
              // jika status code bukan 200 / 400
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
              );
            }
          });
        } catch (e, stack) {
          emitFailure(
            failureResponse: "ERRORSUBMIT Tidak dapat mengirimkan data!",
          );
        }
      }
    } catch (e, stack) {
      emitFailure(
          failureResponse: "ERRORSUBMIT Error saat mencoba mengirimkan data!");
    }
  }
}
