import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/BankInvestor.dart';
import '../../../../models/kyc/company/BankPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/bank.perusahaan.bloc.dart';

class BankPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  BankPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (BankPerusahaanHelper.dokumen.isEdited != null) {
            if (BankPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                BankPerusahaanHelper.dokumen = BankPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              BankPerusahaanHelper.dokumen = BankPerusahaanData();

              return true;
            }
          } else {
            BankPerusahaanHelper.dokumen = BankPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Bank Perusahaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "8/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => BankPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: BankPerusahaanField(),
          )),
    );
  }
}

class BankPerusahaanField extends StatefulWidget {
  @override
  _BankPerusahaanFieldState createState() => _BankPerusahaanFieldState();
}

class _BankPerusahaanFieldState extends State<BankPerusahaanField> {
  BankPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  _reload(VoidCallback onPressed) {
    return FlatButton(
      onPressed: onPressed,
      shape: Border.all(
        width: 1,
        color: Colors.grey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.replay,
            size: FontSize.s15,
          ),
          Container(
            width: Sizes.s10,
          ),
          Text(
            "Muat Ulang",
            style: TextStyle(
              fontSize: FontSize.s15,
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    BankPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<BankPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return KycHideKeyboard(
      child: FormBlocListener<BankPerusahaanBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, "${state.successResponse}");
            Navigator.pop(context);
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            if (BankPerusahaanHelper.dokumen.isSubmitted != null) {
              Navigator.pop(context);
            }

            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
            }

            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              switch (state.failureResponse.toLowerCase()) {
                case "bank_investor1":
                case "account_name1":
                case "account_number1":
                case "account_currency1":
                  fieldWrapper.scrollTo(0);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<BankPerusahaanBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                if (state is FormBlocLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is FormBlocLoadFailed) {
                  return Center(
                    child: Text("Failed to load!"),
                  );
                } else {
                  return Container(
                      width: double.maxFinite,
                      height: double.maxFinite,
                      color: Colors.white,
                      child: ListView(
                          scrollDirection: scrollDirection,
                          padding: EdgeInsets.symmetric(vertical: Sizes.s20),
                          controller: controller,
                          children: [
                            // field bank 1
                            fieldWrapper.viewWidget(
                              0,
                              null,
                              Theme(
                                  data: theme,
                                  child: ExpansionTile(
                                    backgroundColor: Colors.transparent,
                                    initiallyExpanded: true,
                                    title: Text(
                                      "Akun Bank",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                                    subtitle: Container(
                                      margin: EdgeInsets.only(top: Sizes.s5),
                                      color: Color(0xffF4F4F4),
                                      height: 3,
                                      width: double.maxFinite,
                                    ),
                                    children: [
                                      // bank 1
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20,
                                        ),
                                        child: SizedBox(
                                          child: BlocBuilder<SelectFieldBloc,
                                              FieldBlocState>(
                                            bloc: bloc.bank1,
                                            builder: (context, state) {
                                              return state.extraData.isError
                                                  ? _reload(() {
                                                      bloc.fetchBanks();
                                                    })
                                                  : DropdownFieldBlocBuilder<
                                                      BankInvestor>(
                                                      selectFieldBloc:
                                                          bloc.bank1,
                                                      isEnabled: KycHelpers
                                                              .fieldCheck(
                                                                  bloc.status)
                                                          ? state.extraData
                                                                  .isActive ??
                                                              true
                                                          : false,
                                                      decoration:
                                                          InputDecoration(
                                                        errorMaxLines: 5,
                                                        border: OutlineInputBorder(
                                                            borderSide:
                                                                BorderSide(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            5))),
                                                        labelText:
                                                            "Nama Bank Perusahaan",
                                                        suffixIcon: state
                                                                .extraData
                                                                .isLoaded
                                                            ? Icon(Icons
                                                                .arrow_drop_down)
                                                            : CupertinoActivityIndicator(),
                                                      ),
                                                      itemBuilder:
                                                          (context, value) =>
                                                              value != null
                                                                  ? value.bank
                                                                  : "",
                                                    );
                                            },
                                          ),
                                        ),
                                      ),
                                      // account name 1
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            textFieldBloc: bloc.name1,
                                            hintText: "Contoh : Putri Aisyah",
                                            labelText:
                                                "Nama Pemilik Rekening Perusahaan",
                                          ),
                                        ),
                                      ),
                                      // account number 1
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                            textFieldBloc: bloc.account1,
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            hintText:
                                                "Contoh : 10834545890019810",
                                            labelText:
                                                "Nomor Rekening Perusahaan",
                                          ),
                                        ),
                                      ),
                                      // currency bank 1
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocDropDown(
                                            selectFieldBloc: bloc.currency1,
                                            label: "Mata Uang Bank",
                                            enabled: KycHelpers.fieldCheck(
                                              bloc.status,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                            fieldWrapper.divider,
                            // field bank 2
                            fieldWrapper.viewWidget(
                              1,
                              null,
                              Theme(
                                data: theme,
                                child: ExpansionTile(
                                  title: Text(
                                    "Akun Bank 2",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: FontSize.s16,
                                    ),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Kosongkan jika tidak memiliki Akun Bank lain (Optional)",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: Sizes.s5),
                                        color: Color(0xffF4F4F4),
                                        height: 3,
                                        width: double.maxFinite,
                                      ),
                                      SizedBox(
                                        height: Sizes.s20,
                                      ),
                                    ],
                                  ),
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: BlocBuilder<SelectFieldBloc,
                                            FieldBlocState>(
                                          bloc: bloc.bank2,
                                          builder: (context, state) {
                                            return state.extraData.isError
                                                ? _reload(() {
                                                    bloc.fetchBanks();
                                                  })
                                                : DropdownFieldBlocBuilder<
                                                    BankInvestor>(
                                                    selectFieldBloc: bloc.bank2,
                                                    isEnabled:
                                                        KycHelpers.fieldCheck(
                                                                bloc.status)
                                                            ? state.extraData
                                                                    .isActive ??
                                                                true
                                                            : false,
                                                    decoration: InputDecoration(
                                                      errorMaxLines: 5,
                                                      border:
                                                          OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            width: 1,
                                                            color: Colors.grey),
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(
                                                            5,
                                                          ),
                                                        ),
                                                      ),
                                                      labelText:
                                                          "Nama Bank Perusahaan",
                                                      suffixIcon: state
                                                              .extraData
                                                              .isLoaded
                                                          ? Icon(Icons
                                                              .arrow_drop_down)
                                                          : CupertinoActivityIndicator(),
                                                    ),
                                                    itemBuilder: (context,
                                                            value) =>
                                                        value != null
                                                            ? value.bank
                                                            : "",
                                                  );
                                          },
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                          enabled: KycHelpers.fieldCheck(
                                            bloc.status,
                                          ),
                                          textFieldBloc: bloc.name2,
                                          hintText: "Contoh : Putri Aisyah",
                                          labelText:
                                              "Nama Pemilik Rekening Perusahaan",
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: Sizes.s20,
                                      ),
                                      child: SizedBox(
                                        child: SantaraBlocTextField(
                                          enabled: KycHelpers.fieldCheck(
                                              bloc.status),
                                          textFieldBloc: bloc.account2,
                                          hintText:
                                              "Contoh : 10834545890019810",
                                          labelText:
                                              "Nomor Rekening Perusahaan",
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: Sizes.s20,
                                      ),
                                      child: SizedBox(
                                        child: SantaraBlocDropDown(
                                          enabled: KycHelpers.fieldCheck(
                                            bloc.status,
                                          ),
                                          selectFieldBloc: bloc.currency2,
                                          label: "Mata Uang Bank",
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            fieldWrapper.divider,
                            // field bank 3
                            fieldWrapper.viewWidget(
                              2,
                              null,
                              Theme(
                                data: theme,
                                child: ExpansionTile(
                                    title: Text(
                                      "Akun Bank 3",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: FontSize.s16,
                                      ),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Kosongkan jika tidak memiliki Akun Bank lain (Optional)",
                                          style: TextStyle(
                                            fontSize: FontSize.s12,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.only(top: Sizes.s5),
                                          color: Color(0xffF4F4F4),
                                          height: 3,
                                          width: double.maxFinite,
                                        ),
                                        SizedBox(
                                          height: Sizes.s20,
                                        ),
                                      ],
                                    ),
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.s20,
                                        ),
                                        child: SizedBox(
                                          child: BlocBuilder<SelectFieldBloc,
                                              FieldBlocState>(
                                            bloc: bloc.bank3,
                                            builder: (context, state) {
                                              return state.extraData.isError
                                                  ? _reload(() {
                                                      bloc.fetchBanks();
                                                    })
                                                  : DropdownFieldBlocBuilder<
                                                      BankInvestor>(
                                                      selectFieldBloc:
                                                          bloc.bank3,
                                                      isEnabled: KycHelpers
                                                              .fieldCheck(
                                                        bloc.status,
                                                      )
                                                          ? state.extraData
                                                                  .isActive ??
                                                              true
                                                          : false,
                                                      decoration:
                                                          InputDecoration(
                                                        errorMaxLines: 5,
                                                        border:
                                                            OutlineInputBorder(
                                                          borderSide:
                                                              BorderSide(
                                                            width: 1,
                                                            color: Colors.grey,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(
                                                              5,
                                                            ),
                                                          ),
                                                        ),
                                                        labelText:
                                                            "Nama Bank Perusahaan",
                                                        suffixIcon: state
                                                                .extraData
                                                                .isLoaded
                                                            ? Icon(Icons
                                                                .arrow_drop_down)
                                                            : CupertinoActivityIndicator(),
                                                      ),
                                                      itemBuilder:
                                                          (context, value) =>
                                                              value != null
                                                                  ? value.bank
                                                                  : "",
                                                    );
                                            },
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.name3,
                                              hintText: "Contoh : Putri Aisyah",
                                              labelText:
                                                  "Nama Pemilik Rekening Perusahaan"),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc: bloc.account3,
                                              hintText:
                                                  "Contoh : 10834545890019810",
                                              labelText:
                                                  "Nomor Rekening Perusahaan"),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Sizes.s20),
                                        child: SizedBox(
                                          child: SantaraBlocDropDown(
                                            enabled: KycHelpers.fieldCheck(
                                                bloc.status),
                                            selectFieldBloc: bloc.currency3,
                                            label: "Mata Uang Bank",
                                          ),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                            fieldWrapper.divider,
                            // submit bank
                            Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: Sizes.s20),
                              child: !KycHelpers.fieldCheck(bloc.status)
                                  ? SantaraDisabledButton()
                                  : SantaraMainButton(
                                      onPressed: () {
                                        BankPerusahaanHelper
                                            .dokumen.isSubmitted = true;
                                        FocusScope.of(context).unfocus();
                                        bloc.submit();
                                      },
                                      title: "Simpan",
                                    ),
                            )
                          ]));
                }
              })),
    );
  }
}
