import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/ImageProcessor.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/company/DokPerusahaanData.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';

class DokPerusahaanBloc extends FormBloc<String, String> {
  User company = User();
  final String submissionId;
  final String status;

  final KycPerusahaanService _service = KycPerusahaanService();

  final dokAktaPendirianUsaha = InputFieldBloc<File, Object>();
  final dokAktaPerubahanTerakhir = InputFieldBloc<File, Object>();
  final dokSkKemenhumkam = InputFieldBloc<File, Object>();
  final dokNpwp = InputFieldBloc<File, Object>();
  final dokSiup = InputFieldBloc<File, Object>();
  final ktpDireksi = InputFieldBloc<File, Object>();

  DokPerusahaanBloc({
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      dokAktaPendirianUsaha,
      dokAktaPerubahanTerakhir,
      dokSkKemenhumkam,
      dokNpwp,
      dokSiup,
      ktpDireksi
    ]);
    updateCompanyData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
    });
  }

  void pickFile(InputFieldBloc bloc) async {
    var files = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
      allowMultiple: false,
    );
    var file = File(files.files[0].path);
    final bool allowed =
        await ImageProcessor.checkImageExtension(file, [".pdf"]);
    if (allowed) {
      bloc.updateInitialValue(file);
      bloc.updateExtraData(null);
    } else {
      bloc.addFieldError("Format File Tidak Diijinkan!");
    }
  }

  // get semua data perusahaan
  getCompanyData() async {
    await _service.getPerusahaanData().then((value) {
      if (value.statusCode == 200) {
        company = User.fromJson(value.data);
      } else if (value.statusCode == 401) {
        emitFailure(failureResponse: "UNAUTHORIZED");
      } else {
        emitLoaded();
        emitFailure();
      }
    });
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "7").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          logger.i(">> Status Code : ${value.statusCode}");
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // update value setiap fieldnya
  Future updateCompanyData() async {
    emitLoading();
    try {
      final storage = new FlutterSecureStorage(); // storage
      await getCompanyData();
      // Update value setiap fields
      // dokumen akta pendirian perusahaan
      if (company.trader.companyDocument != null) {
        dokAktaPendirianUsaha.updateExtraData({
          "hasDocument": true,
          "url":
              company.trader.companyDocument.replaceAll('/uploads/trader/', ''),
        });
      }

      // akta perubahan terakhir
      if (company.trader.companyDocumentChange != null) {
        dokAktaPerubahanTerakhir.updateExtraData({
          "hasDocument": true,
          "url": company.trader.companyDocumentChange
              .replaceAll('/uploads/trader/', ''),
        });
      }

      // dokumen sk kemenkumham
      if (company.trader.documentSkKemenkumham != null) {
        dokSkKemenhumkam.updateExtraData({
          "hasDocument": true,
          "url": company.trader.documentSkKemenkumham
              .replaceAll('/uploads/trader/', ''),
        });
      }

      // dokumen npwp perusahaan
      if (company.trader.documentNpwp != null) {
        dokNpwp.updateExtraData({
          "hasDocument": true,
          "url": company.trader.documentNpwp.replaceAll('/uploads/trader/', ''),
        });
      }

      // dokumen siup/nib perusahaan
      if (company.trader.siupFile != null) {
        dokSiup.updateExtraData({
          "hasDocument": true,
          "url": company.trader.siupFile.replaceAll('/uploads/trader/', ''),
        });
      }

      // ktp direktur utama/direksi
      final token = await storage.read(key: 'token');
      // print(">> KTP DIREKTUR UTAMA : " +
      //     apiLocalImage +
      //     company.trader.idCardDirector);
      if (company.trader.idCardDirector != null) {
        ktpDireksi.updateExtraData({
          "hasPhoto": true,
          "url": GetAuthenticatedFile.convertUrl(
            image: company.trader.idCardDirector,
            type: PathType.traderIdCardPhoto,
          ),
        });
      }

      if (status == "rejected") {
        await getFieldErrorStatus();
      }

      emitLoaded();
    } catch (e) {
      emitLoaded();
    }
  }

  onFieldsChange() {
    detectChanges(dokAktaPendirianUsaha);
    detectChanges(dokAktaPerubahanTerakhir);
    detectChanges(dokSkKemenhumkam);
    detectChanges(dokNpwp);
    detectChanges(dokSiup);
    detectChanges(ktpDireksi);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      DokPerusahaanHelper.dokumen.isEdited = true;
    });
  }

  bool isDokAktaPendirianUsahaError() {
    if (dokAktaPendirianUsaha.value == null) {
      if (dokAktaPendirianUsaha.state.extraData != null) {
        return false;
      } else {
        dokAktaPendirianUsaha
            .addFieldError("Dok. Akta Pendirian Usaha tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isDokSkKemenhumkamError() {
    if (dokSkKemenhumkam.value == null) {
      if (dokSkKemenhumkam.state.extraData != null) {
        return false;
      } else {
        dokSkKemenhumkam
            .addFieldError("Dok. SK Kemenhumkam tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isDokNpwpError() {
    if (dokNpwp.value == null) {
      if (dokNpwp.state.extraData != null) {
        return false;
      } else {
        dokNpwp.addFieldError("Dok. NPWP Perusahaan tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isDokSiupError() {
    if (dokSiup.value == null) {
      if (dokSiup.state.extraData != null) {
        return false;
      } else {
        dokSiup.addFieldError("Dok. SIUP/NIB Perusahaan tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isFotoKtpError() {
    if (ktpDireksi.value == null) {
      if (ktpDireksi.state.extraData != null) {
        return false;
      } else {
        ktpDireksi
            .addFieldError("KTP Direktur Utama/Direksi tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      case "company_document":
        addErrorMessage(dokAktaPendirianUsaha, _errMsg, "company_document");
        break;
      case "company_document_change":
        addErrorMessage(
            dokAktaPerubahanTerakhir, _errMsg, "company_document_change");
        break;
      case "document_sk_kemenkumham":
        addErrorMessage(dokSkKemenhumkam, _errMsg, "document_sk_kemenkumham");
        break;
      case "document_npwp":
        addErrorMessage(dokNpwp, _errMsg, "document_npwp");
        break;
      case "document_siup":
        addErrorMessage(dokSiup, _errMsg, "document_siup_nib");
        break;
      case "idcard_director":
        addErrorMessage(ktpDireksi, _errMsg, "idcard_director");
        break;
      default:
        emitFailure(
            failureResponse:
                "ERRORSUBMIT Tidak dapat menerima data error, cek kembali form anda!");
        break;
    }
  }

  @override
  void onSubmitting() async {
    try {
      if (isDokAktaPendirianUsahaError()) {
        emitFailure(failureResponse: "company_document");
      } else if (isDokSkKemenhumkamError()) {
        emitFailure(failureResponse: "document_sk_kemenkumham");
      } else if (isDokNpwpError()) {
        emitFailure(failureResponse: "document_npwp");
      } else if (isDokSiupError()) {
        emitFailure(failureResponse: "document_siup_nib");
      } else if (isFotoKtpError()) {
        emitFailure(failureResponse: "ktp_direksi");
      } else {
        DokPerusahaanData data = DokPerusahaanData(
          dokAktaPendirianUsaha: dokAktaPendirianUsaha?.value ?? null,
          dokAktaPerubahanTerakhir: dokAktaPerubahanTerakhir?.value ?? null,
          dokSkKemenhumkam: dokSkKemenhumkam?.value ?? null,
          dokNpwp: dokNpwp?.value ?? null,
          dokSiup: dokSiup?.value ?? null,
          ktpDireksi: ktpDireksi?.value ?? null,
        );
        // company.trader.companyDocument != null ? "rejected" : status ==> Jika step 7 memiliki file, maka otomatis pakai method put, makanya statusnya jadi rejected or whatever lah...
        await _service
            .postDokumenPerusahaan(data,
                company.trader.companyDocument != null ? "rejected" : status)
            .then((value) {
          if (value.statusCode == 200) {
            String message;
            if (value.data["message"] != null) {
              message = value.data["message"];
            }
            emitSuccess(
              canSubmitAgain: false,
              successResponse:
                  message != null ? message : "Berhasil menyimpan data!",
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else if (value.statusCode == 400) {
            try {
              value.data.forEach((res) {
                var dummy = ValidationResultModel.fromJson(res);
                handleApiValidation(dummy.field, dummy.message);
              });
              // emitFailu
            } catch (e, stack) {
              santaraLog(e, stack);
              emitFailure(
                failureResponse: "ERRORSUBMIT ${value.data['message']}",
              );
            }
          } else {
            logger.i(">> Status Code : ${value.statusCode}");
            // jika status code bukan 200 / 400
            emitFailure(
              failureResponse:
                  "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
            );
          }
        });
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: "ERRORSUBMIT $e Tidak dapat mengirimkan data!",
      );
    }
  }
}
