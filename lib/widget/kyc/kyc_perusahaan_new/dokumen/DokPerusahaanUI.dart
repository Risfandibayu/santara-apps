import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/company/DokPerusahaanData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/Camera.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraCachedImage.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../kyc_pribadi_new/biodata/camera/PhotoPreview.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/dok.perusahaan.bloc.dart';

class DokPerusahaanUI extends StatelessWidget {
  final String submissionId;
  final String status;
  DokPerusahaanUI({
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (status == "verifying") {
          return true;
        } else {
          if (DokPerusahaanHelper.dokumen.isEdited != null) {
            if (DokPerusahaanHelper.dokumen.isEdited) {
              PopupHelper.handleCloseKyc(context, () {
                // yakin handler
                // reset helper jadi null
                Navigator.pop(context); // close alert
                Navigator.pop(context); // close page
                DokPerusahaanHelper.dokumen = DokPerusahaanData();
                return true;
              }, () {
                // batal handler
                Navigator.pop(context); // close alert
                return false;
              });
              return false;
            } else {
              DokPerusahaanHelper.dokumen = DokPerusahaanData();
              return true;
            }
          } else {
            DokPerusahaanHelper.dokumen = DokPerusahaanData();
            return true;
          }
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Dok. Perusahaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "7/8",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => DokPerusahaanBloc(
              submissionId: submissionId,
              status: status,
            ),
            child: DokPerusahaanForm(),
          )),
    );
  }
}

class DokPerusahaanForm extends StatefulWidget {
  @override
  _DokPerusahaanFormState createState() => _DokPerusahaanFormState();
}

class _DokPerusahaanFormState extends State<DokPerusahaanForm> {
  DokPerusahaanBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  Future<File> pickPhoto() async {
    var cameras = await availableCameras();
    File file;
    if (cameras.length == 0) {
      file = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      try {
        file = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => Camera(isKyc: 2),
          ),
        );
      } catch (e, stack) {
        ToastHelper.showFailureToast(context, e);
        return null;
      }
    }
    return file;
  }

  @override
  void initState() {
    super.initState();
    DokPerusahaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<DokPerusahaanBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<DokPerusahaanBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(context, "${state.successResponse}");
          Navigator.pop(context);
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (DokPerusahaanHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }

          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }

          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse.toLowerCase()) {
              case "dok_akta_pendirian":
                fieldWrapper.scrollTo(1);
                break;
              case "dok_sk_kemenhumkam":
                fieldWrapper.scrollTo(3);
                break;
              case "company_document_change":
                fieldWrapper.scrollTo(2);
                break;
              case "dok_npwp":
                fieldWrapper.scrollTo(4);
                break;
              case "document_siup_nib":
                fieldWrapper.scrollTo(6);
                break;
              case "ktp_direksi":
                fieldWrapper.scrollTo(5);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<DokPerusahaanBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                child: ListView(
                  scrollDirection: scrollDirection,
                  padding: EdgeInsets.all(Sizes.s20),
                  controller: controller,
                  children: [
                    fieldWrapper.viewWidget(
                      1,
                      Sizes.s110,
                      SantaraPdfBlocField(
                        status: "verifying",
                        title: "Dok. Akta Pendiran Usaha",
                        selectFieldBloc: bloc.dokAktaPendirianUsaha,
                        onTap: () {
                          if (KycHelpers.fieldCheck(bloc.status))
                            bloc.pickFile(bloc.dokAktaPendirianUsaha);
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      2,
                      Sizes.s110,
                      SantaraPdfBlocField(
                        status: bloc.status,
                        title:
                            "Dok, Akta Perubahan Terakhir *Jika ada (Optional)",
                        selectFieldBloc: bloc.dokAktaPerubahanTerakhir,
                        onTap: () {
                          if (KycHelpers.fieldCheck(bloc.status))
                            bloc.pickFile(bloc.dokAktaPerubahanTerakhir);
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s110,
                      SantaraPdfBlocField(
                        status: bloc.status,
                        title: "Dok. SK Kemenkumham",
                        selectFieldBloc: bloc.dokSkKemenhumkam,
                        onTap: () async {
                          if (KycHelpers.fieldCheck(bloc.status))
                            bloc.pickFile(bloc.dokSkKemenhumkam);
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      4,
                      Sizes.s110,
                      SantaraPdfBlocField(
                        status: bloc.status,
                        title: "Dok. NPWP Perusahaan",
                        selectFieldBloc: bloc.dokNpwp,
                        onTap: () async {
                          if (KycHelpers.fieldCheck(bloc.status))
                            bloc.pickFile(bloc.dokNpwp);
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      6,
                      Sizes.s110,
                      SantaraPdfBlocField(
                        status: bloc.status,
                        title: "Dok. SIUP/NIB",
                        selectFieldBloc: bloc.dokSiup,
                        onTap: () async {
                          if (KycHelpers.fieldCheck(bloc.status))
                            bloc.pickFile(bloc.dokSiup);
                        },
                      ),
                    ),
                    Text(
                      "KTP Direktur Utama/Direksi",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    fieldWrapper.divider,
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s200,
                      InkWell(
                        onTap: () async {
                          if (KycHelpers.fieldCheck(bloc.status)) {
                            // final result = await Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (context) => CameraKTP(
                            //       ktpAction: KtpAction.takeFromDoc,
                            //     ),
                            //   ),
                            // );
                            final result = await pickPhoto();
                            if (result != null) {
                              bloc.ktpDireksi.updateValue(result);
                            }
                          }
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _imagePikcer(bloc.ktpDireksi),
                            KycErrorWrapper(bloc: bloc.ktpDireksi),
                          ],
                        ),
                      ),
                    ),
                    fieldWrapper.divider,
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              DokPerusahaanHelper.dokumen.isSubmitted = true;
                              FocusScope.of(context).unfocus();
                              bloc.submit();
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }

  // Image picker, ketika user sudah ambil foto, ubah foto
  Widget _imagePikcer(InputFieldBloc fieldBloc) {
    return BlocBuilder<InputFieldBloc, InputFieldBlocState>(
      bloc: fieldBloc,
      builder: (context, state) {
        File file = state.value;
        if (bloc.status != "data_update") {
          return state.value == null
              ? state.extraData != null // extra data
                  ? Center(
                      child: DottedBorder(
                        color: Color(0xFFB8B8B8),
                        strokeWidth: 2,
                        radius: Radius.circular(Sizes.s15),
                        dashPattern: [5, 5],
                        child: file != null
                            ? Container(
                                width: Sizes.s250,
                                height: Sizes.s150,
                                decoration: BoxDecoration(
                                  color: Color(0xffF4F4F4),
                                  image: file != null
                                      ? DecorationImage(image: FileImage(file))
                                      : null,
                                ),
                                child: file != null
                                    ? Container()
                                    : Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.camera_alt,
                                            color: Color(0xFFB8B8B8),
                                            size: Sizes.s30,
                                          ),
                                          SizedBox(
                                            height: Sizes.s10,
                                          ),
                                          Text(
                                            "Ambil Foto",
                                            style: TextStyle(
                                              fontSize: FontSize.s15,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFFB8B8B8),
                                            ),
                                          )
                                        ],
                                      ),
                              )
                            : Container(
                                width: Sizes.s250,
                                height: Sizes.s150,
                                child: SantaraCachedImage(
                                  // placeholder:
                                  //     ExactAssetImage('assets/Preload.jpeg'),
                                  image: state.extraData["url"],
                                  fit: BoxFit.contain,
                                ),
                              ),
                      ),
                    )
                  : _pickerView(file)
              : _pickerView(file);
        } else {
          return _pickerView(file);
        }
      },
    );
  }

  // image picker widget (ktp direksi)
  Widget _pickerView(File file) {
    return Center(
      child: Column(children: <Widget>[
        LimitedBox(
          maxHeight: Sizes.s150,
          child: DottedBorder(
            color: Color(0xFFB8B8B8),
            strokeWidth: 2,
            radius: Radius.circular(Sizes.s15),
            dashPattern: [5, 5],
            child: Container(
              width: Sizes.s250,
              height: Sizes.s150,
              decoration: BoxDecoration(
                color: Color(0xffF4F4F4),
                image: file != null
                    ? DecorationImage(image: FileImage(file))
                    : null,
              ),
              child: file != null
                  ? Container()
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.camera_alt,
                          color: Color(0xFFB8B8B8),
                          size: Sizes.s30,
                        ),
                        SizedBox(
                          height: Sizes.s10,
                        ),
                        Text(
                          "Ambil Foto",
                          style: TextStyle(
                            fontSize: FontSize.s15,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFFB8B8B8),
                          ),
                        )
                      ],
                    ),
            ),
          ),
        ),
        file == null
            ? Container()
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MaterialButton(
                    color: Colors.white,
                    elevation: 0,
                    onPressed: () async {
                      final result = await pickPhoto();
                      if (result != null) {
                        bloc.ktpDireksi.updateValue(result);
                      }
                    },
                    child: Text(
                      "Ubah Foto",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: Sizes.s10,
                  ),
                  MaterialButton(
                    color: Colors.white,
                    elevation: 0,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PhotoPreviewUI(image: file)));
                    },
                    child: Text(
                      "Lihat Foto",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                    ),
                  ),
                ],
              ),
      ]),
    );
  }
}
