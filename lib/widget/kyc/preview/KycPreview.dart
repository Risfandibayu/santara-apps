import 'package:flutter/material.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:santaraapp/models/kyc/KycPreview.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycWrapper.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

enum PreviewType { personal, company }

class KycPreviewUI extends StatefulWidget {
  final PreviewType previewType;
  KycPreviewUI({@required this.previewType});

  @override
  _KycPreviewUIState createState() => _KycPreviewUIState();
}

class _KycPreviewUIState extends State<KycPreviewUI> {
  double _currentIndex = 0;

  PageController _controller = PageController(
    initialPage: 0,
  );

  var previews = [];

  var rawPersonal = [
    KycPreview(
      title: "Makin Nyaman & Aman",
      subtitle:
          "Dengan Mengisi Form KYC ini data dan portfolio saham Anda nantinya akan terdaftar resmi di Kustodian Sentral Efek Indonesia.",
      description: "",
      image: "assets/preview_kyc/personal/clip-0.png",
      isCenter: true,
    ),
    KycPreview(
      title: "Akun Perseorangan",
      subtitle:
          "Akun perseorangan digunakan bagi Anda yang ingin berinvestasi atas nama pribadi. Untuk memberikan pengalaman investasi yang aman dan menyenangkan. Sesaat lagi Anda akan dipersilakan mengisi beberapa data yang dibutuhkan. Semangat!",
      description: "",
      image: "assets/preview_kyc/personal/clip-1.png",
      isCenter: true,
    ),
    KycPreview(
      title: "Biodata Pribadi",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti :",
      description:
          "- Mengunggah foto profil\n- Nama lengkap\n- Tempat tanggal lahir\n- Pendidikan terakhir\n- Unggah foto KTP dan unggah foto selfie memegang KTP",
      image: "assets/preview_kyc/personal/clip-2.png",
    ),
    KycPreview(
      title: "Biodata Keluarga",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Status pernikahan\n- Nama pasangan (jika ada)\n- Nama ibu kandung\n- Nama ahli waris\n- Nomor telepon ahli waris",
      image: "assets/preview_kyc/personal/clip-3.png",
    ),
    KycPreview(
      title: "Alamat",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description: "- Alamat lengkap sesuai KTP\n- Alamat tinggal sekarang",
      image: "assets/preview_kyc/personal/clip-4.png",
    ),
    KycPreview(
      title: "Pekerjaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Pekerjaan\n- Total Asset\n- Sumber Dana\n- Motivasi Berinvestasi",
      image: "assets/preview_kyc/personal/clip-5.png",
    ),
    KycPreview(
      title: "Informasi Pajak",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Pendapatan pertahun\n- Nomor NPWP\n- Tanggal registrasi NPWP\n- Informasi rekening efek (Jika ada)",
      image: "assets/preview_kyc/personal/clip-6.png",
    ),
    KycPreview(
      title: "Bank & Rekening",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Nama pemilik rekening\n- Nama bank\n- Nomor rekening bank",
      image: "assets/preview_kyc/personal/clip-7.png",
    ),
  ];

  var rawCompany = [
    KycPreview(
      title: "Makin Nyaman & Aman",
      subtitle:
          "Dengan Mengisi Form KYC ini data dan portfolio saham Anda nantinya akan terdaftar resmi di Kustodian Sentral Efek Indonesia.",
      description: "",
      image: "assets/preview_kyc/company/clip-0.png",
      isCenter: true,
    ),
    KycPreview(
      title: "Akun Perusahaan",
      subtitle:
          "Akun perusahaan digunakan bagi Anda yang ingin berinvestasi atas nama perusahaan berbadan hukum. Untuk memberikan pengalaman investasi yang aman dan menyenangkan. Sesaat lagi Anda akan dipersilakan mengisi beberapa data yang dibutuhkan. Semangat!",
      description: "",
      image: "assets/preview_kyc/company/clip-01.png",
      isCenter: true,
    ),
    KycPreview(
      title: "Biodata Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Mengunggah foto perusahaan\n- Nama perusahaan\n- Tanggal pendirian usaha\n- Email perusahaan\n- Deskripsi singkat perusahaan",
      image: "assets/preview_kyc/company/clip-2.png",
    ),
    KycPreview(
      title: "Pajak & Perizinan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Nomor NPWP perusahaan\n- Nomor SIUP\n- Nomor akta pendiran usaha\n- Nomor NIB perusahaan",
      image: "assets/preview_kyc/company/clip-3.png",
    ),
    KycPreview(
      title: "Alamat Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description: "- Alamat lengkap perusahaan\n- Nomor telepon perusahaan",
      image: "assets/preview_kyc/company/clip-4.png",
    ),
    KycPreview(
      title: "Penanggung Jawab Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Nama lengkap penanggung jawab\n- Jabatan penanggung jawab\n- Nomor KTP penanggung jawab",
      image: "assets/preview_kyc/company/clip-5.png",
    ),
    KycPreview(
      title: "Asset Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Total aset perusahaan dalam 3 tahun terakhir\n- Sumber dana\n- Motivasi investasi",
      image: "assets/preview_kyc/company/clip-6.png",
    ),
    KycPreview(
      title: "Profit Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description: "- Profit perusahaan dalam 3 tahun terakhir",
      image: "assets/preview_kyc/company/clip-7.png",
    ),
    KycPreview(
      title: "Dokumen Perusahaan",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Akta pendirian usaha\n- SK kemenkuham\n- NPWP perusahaan\n- KTP direktur utama / direksi lain yang  ditunjuk secara resmi oleh perusahaan\n- SIUP perusahaan",
      image: "assets/preview_kyc/company/clip-8.png",
    ),
    KycPreview(
      title: "Bank & Rekening",
      subtitle:
          "Pada halaman ini anda diarahkan untuk melengkapi beberapa data seperti : ",
      description:
          "- Nama pemilik rekening\n- Nama bank\n- Nomor rekening bank",
      image: "assets/preview_kyc/company/clip-9.png",
    ),
  ];

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      previews =
          widget.previewType == PreviewType.personal ? rawPersonal : rawCompany;
    });
    _controller.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 2;
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            onPageChanged: (val) {
              setState(() {
                _currentIndex = val.toDouble();
              });
            },
            controller: _controller,
            children: [
              for (var preview in previews)
                KycWrapper(
                  title: "${preview.title}",
                  subtitle: Text(
                    "${preview.subtitle}",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                    ),
                    textAlign:
                        preview.isCenter ? TextAlign.center : TextAlign.left,
                  ),
                  description: Container(
                    width: double.maxFinite,
                    child: preview.description != null
                        ? Text(
                            "${preview.description}",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                            ),
                            textAlign: TextAlign.left,
                          )
                        : null,
                  ),
                  image: "${preview.image}",
                ),
            ],
          ),
          _currentIndex == (previews.length - 1)
              ? Container()
              : Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.only(top: height / 8, right: Sizes.s10),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: Sizes.s70,
                          height: Sizes.s30,
                          child: Text(
                            "Tutup",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s18,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: height / 12),
              child: _currentIndex == (previews.length - 1)
                  ? Padding(
                      padding: EdgeInsets.only(
                        left: Sizes.s40,
                        right: Sizes.s40,
                      ),
                      child: SantaraMainButton(
                        onPressed: () => Navigator.pop(context),
                        title: widget.previewType == PreviewType.company
                            ? "Yuk, Isi Datanya Sekarang !"
                            : "Yuk, Isi KYC Sekarang !",
                      ),
                    )
                  : DotsIndicator(
                      dotsCount: previews.length,
                      position: _currentIndex,
                      decorator: DotsDecorator(
                        color: Color(0xffDDDDDD), // Inactive color
                        activeColor: Color(0xffBF2D30),
                      ),
                    ),
            ),
          )
        ],
      ),
    );
  }
}
