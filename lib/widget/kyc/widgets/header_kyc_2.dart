import 'package:flutter/material.dart';

class HeaderKyc2 extends StatefulWidget {
  final int tahap;
  HeaderKyc2({this.tahap});

  @override
  _HeaderKyc2State createState() => _HeaderKyc2State();
}

class _HeaderKyc2State extends State<HeaderKyc2> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    Future.delayed(Duration(milliseconds: 1500),() {
      _controller.animateTo(_controller.offset + ((widget.tahap-1) * 200),
          curve: Curves.linear, duration: Duration(milliseconds: 500));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            child: ListView(
              controller: _controller,
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.symmetric(horizontal: 30),
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                _itemJourney(1, "Biodata Perusahaan"),
                _lineJourney(1),
                _itemJourney(2, "Alamat Perusahaan"),
                _lineJourney(2),
                _itemJourney(3, "Penanggung Jawab Perusahaan"),
                _lineJourney(3),
                _itemJourney(4, "Aset Perusahaan"),
                _lineJourney(4),
                _itemJourney(5, "Dokumen Perusahaan"),
                _lineJourney(5),
                _itemJourney(6, "Bank Perusahaan"),
                // _lineJourney(6),
                // _itemJourney(7, "Motivasi Perusahaan"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: MySeparator(height: 1, color: Colors.black,),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 15, 30, 15),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/icon/warning.png"),
                Container(width: 12),
                Expanded(
                    child: Text(
                        "Kerahasiaan Informasi Pribadi anda adalah hal yang terpenting bagi kami. Kami memberlakukan upaya terbaik untuk melindungi dan mengamankan data dan Informasi Pribadi anda.",
                        style:
                            TextStyle(color: Color(0xFF676767), fontSize: 12)))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemJourney(int index, String name) {
    return Container(
      width: 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: index <= widget.tahap ? Color(0xFFBF2D30) : Colors.grey,
                ),
                child: Center(
                    child:
                        Text("$index", style: TextStyle(color: Colors.white))),
              ),
              Container(width: 16),
              Expanded(
                  child: Text(name,
                      style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold))),
            ],
          ),
        ],
      ),
    );
  }

  Widget _lineJourney(int index) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(right: 20),
        height: 2,
        width: 50,
        color: index <= widget.tahap ? Color(0xFFBF2D30) : Colors.grey,
      ),
    );
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 5.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
