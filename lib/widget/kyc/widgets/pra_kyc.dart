import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../../models/User.dart';
import '../../../pages/Home.dart';
import '../../../utils/api.dart';
import '../../../utils/helper.dart';
import '../../../utils/sizes.dart';
import '../../setting_account/kyc_perusahaan.dart';
import '../../setting_account/kyc_pribadi.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../preview/KycPreview.dart';

class PraKyc extends StatefulWidget {
  final int isEmpty;
  PraKyc({this.isEmpty});
  @override
  _PraKycState createState() => _PraKycState();
}

class _PraKycState extends State<PraKyc> {
  final storage = new FlutterSecureStorage();
  var token;
  var uuid;
  var refreshToken;
  var userId;
  var isLoading = false;

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
    uuid = await storage.read(key: 'uuid');
    refreshToken = await storage.read(key: 'refreshToken');
    userId = await storage.read(key: 'userId');
  }

  Future getUserByUuid(String uuid, String token, Widget next) async {
    final response = await http.get('$apiLocal/users/by-uuid/$uuid',
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      await storage.write(key: 'user', value: response.body);
      var user = userFromJson(response.body);
      setState(() {
        isLoading = false;
        Helper.username = user.trader == null ? "" : user.trader.name;
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => Home(index: 4)),
            (Route<dynamic> route) => false);
        Navigator.push(context, MaterialPageRoute(builder: (_) => next));
      });
    }
  }

  Future sendData(Widget next, String type) async {
    setState(() => isLoading = true);
    final http.Response response = await http.post(
        '$apiLocal/traders/select-trader',
        headers: {"Authorization": "Bearer $token"},
        body: {"trader_type": type});
    if (response.statusCode == 200) {
      // // print(">> KYC Result : ");
      // // print(response.body);
      getUserByUuid(uuid, token, next);
    } else {
      setState(() => isLoading = false);
      final data = jsonDecode(response.body);
      getUserByUuid(uuid, token, next);
      showDialog(
          context: context,
          builder: (_) {
            return AlertDialog(
              content: Text(data["message"]),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text("Mengerti"))
              ],
            );
          });
    }
  }

  Future logout() async {
    if (token != null) {
      await http.post('$apiLocal/auth/logout', body: {
        "refreshToken": "$refreshToken",
        "user_id": "$userId",
      }).timeout(Duration(seconds: 20));
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'userId');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  Future<bool> onWillPop() async {
    if (widget.isEmpty == 1) {
      logout().then((_) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => Home()),
            (Route<dynamic> route) => false);
      });
    } else {
      Navigator.pop(context);
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Scaffold(
          appBar: AppBar(
            title: Image.asset('assets/santara/1.png', width: 110.0),
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            leading: IconButton(
                icon: Platform.isIOS
                    ? Icon(Icons.arrow_back_ios)
                    : Icon(Icons.arrow_back),
                onPressed: () {
                  if (widget.isEmpty == 1) {
                    logout().then((_) {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home()),
                          (Route<dynamic> route) => false);
                    });
                  } else {
                    Navigator.pop(context);
                  }
                }),
          ),
          body: ListView(
            padding: EdgeInsets.all(30),
            children: <Widget>[
              Center(
                child: Text(
                  "Jenis Akun",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
              ),
              Container(height: 15),
              _jenisAkun(
                  "assets/icon_kyc/opsi_person.png",
                  "Perseorangan",
                  "Saya ingin berinvestasi untuk\ndiri saya sendiri",
                  KycPribadiSetting(
                    preview: false,
                  ),
                  "personal"),
              _jenisAkun(
                  "assets/icon_kyc/opsi_perusahaan.png",
                  "Perusahaan",
                  "Saya ingin berinvestasi atas nama\nperusahaan / lembaga",
                  KycPerusahaanSetting(
                    preview: false,
                  ),
                  "company")
            ],
          ),
        ),
      ),
    );
  }

  // Dialg konfirmasi
  void _confirmationDialog(Widget next, String type) {
    var alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      content: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Konfirmasi Jenis Akun",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s20,
              ),
            ),
            SizedBox(
              height: Sizes.s10,
            ),
            Text(
              "Apakah Anda yakin ingin memilih jenis akun ini ? Jenis akun yang dipilih tidak dapat dirubah kembali",
              style: TextStyle(
                fontSize: FontSize.s14,
                color: Color(0xff676767),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: Sizes.s20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    height: Sizes.s40,
                    child: SantaraOutlineButton(
                      title: "Batal",
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                Container(
                  width: 15,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    height: Sizes.s40,
                    child: SantaraMainButton(
                      title: "Yakin",
                      onPressed: () async {
                        Navigator.pop(context);
                        await sendData(next, type);
                      },
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: Sizes.s15,
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => KycPreviewUI(
                          previewType: type == "personal"
                              ? PreviewType.personal
                              : PreviewType.company),
                    ),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  height: Sizes.s30,
                  child: Text(
                    "Preview Halaman",
                    style: TextStyle(
                      color: Color(0xffBF2D30),
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _jenisAkun(
      String image, String type, String desc, Widget next, String v) {
    return GestureDetector(
      // onTap: () => sendData(next, v),
      onTap: () => _confirmationDialog(next, v),
      child: Container(
        padding: EdgeInsets.all(30),
        margin: EdgeInsets.symmetric(vertical: 15),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
              BoxShadow(
                color: Color(0xFFE6EAFA),
                blurRadius: 10,
              )
            ]),
        child: Column(
          children: <Widget>[
            Image.asset(image),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                type,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: Color(0xFF676767)),
            ),
          ],
        ),
      ),
    );
  }
}
