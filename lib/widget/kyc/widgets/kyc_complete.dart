import 'package:flutter/material.dart';

import '../../../pages/Home.dart';
import '../../../utils/sizes.dart';
import '../../widget/components/main/SantaraButtons.dart';

class KycCompleteUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(Sizes.s20),
          alignment: Alignment.center,
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/icon/success.png",
                width: Sizes.s200,
              ),
              SizedBox(
                height: Sizes.s50,
              ),
              Text(
                "Selamat Anda sudah selesai mengisi form KYC.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: FontSize.s20,
                ),
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              Text(
                "Dalam estimasi 1x 24jam data Anda akan diverifikasi oleh Admin. Setelahnya, Anda dapat langsung membeli saham bisnis-bisnis terbaik di Santara. Happy Investing :)",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: FontSize.s14,
                  color: Color(0xff676767),
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: Sizes.s20,
              ),
              SantaraMainButton(
                title: "Mengerti",
                onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => Home(index: 0)),
                    (Route<dynamic> route) => false),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
