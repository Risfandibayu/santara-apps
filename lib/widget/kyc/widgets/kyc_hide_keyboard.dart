import 'package:flutter/material.dart';

class KycHideKeyboard extends StatelessWidget {
  final Widget child;

  const KycHideKeyboard({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: child,
    );
  }
}
