import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../helpers/kyc/PekerjaanHelper.dart';
import '../../../../models/kyc/SubData.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/pekerjaan.bloc.dart';

class KycPekerjaanUI extends StatelessWidget {
  final bool isEdit;
  final String submissionId;
  final String status;
  KycPekerjaanUI({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycPekerjaanHelper.dokumen.isEdited != null) {
          if (KycPekerjaanHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycPekerjaanHelper.dokumen = PekerjaanUser();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              return false;
            });
            return false;
          } else {
            KycPekerjaanHelper.dokumen = PekerjaanUser();
            return true;
          }
        } else {
          KycPekerjaanHelper.dokumen = PekerjaanUser();
          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Pekerjaan",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "4/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => PekerjaanUserBloc(
              isEdit: isEdit,
              submissionId: submissionId,
              status: status,
            ),
            child: PekerjaanUserForm(),
          )),
    );
  }
}

class PekerjaanUserForm extends StatefulWidget {
  @override
  _PekerjaanUserFormState createState() => _PekerjaanUserFormState();
}

class _PekerjaanUserFormState extends State<PekerjaanUserForm> {
  PekerjaanUserBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    KycPekerjaanHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<PekerjaanUserBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<PekerjaanUserBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          state.successResponse != null
              ? ToastHelper.showBasicToast(context, "${state.successResponse}")
              : null;
          Navigator.pop(context);
          KycPekerjaanHelper.dokumen = PekerjaanUser();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (KycPekerjaanHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }
          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }
          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse.toLowerCase()) {
              case "occupation":
                fieldWrapper.scrollTo(1);
                break;
              case "business_desc":
                fieldWrapper.scrollTo(2);
                break;
              case "total_assets":
                fieldWrapper.scrollTo(3);
                break;
              case "funds_source":
                fieldWrapper.scrollTo(4);
                break;
              case "invest_purposes":
                fieldWrapper.scrollTo(5);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<PekerjaanUserBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                  // child: Text("Failed to load!"),
                  );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                padding: EdgeInsets.all(Sizes.s20),
                child: ListView(
                  scrollDirection: scrollDirection,
                  controller: controller,
                  children: [
                    Text(
                      "Pekerjaan",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    fieldWrapper.viewWidget(
                      1,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.occupation,
                        label: "Pekerjaan",
                        hintText: "Pilih",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
                        bloc: bloc.occupation,
                        builder: (context, state) {
                          if (state.hasValue) {
                            if (state.value.id == "4" ||
                                state.value.id == "OTH") {
                              return fieldWrapper.viewWidget(
                                2,
                                Sizes.s100,
                                SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.businessDesc,
                                  hintText: "Contoh : Bisnis Rumah Makan",
                                  labelText: "Bidang Usaha",
                                ),
                              );
                            } else {
                              return Container();
                            }
                          } else {
                            return Container();
                          }
                        }),
                    Text(
                      "Total Aset",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s100,
                      SantaraRevenueField(
                        bloc: bloc.totalAsset,
                        hintText: "Total aset",
                        labelText: "*Kosongkan jika tidak ada (Optional)",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        onChanged: (String value) {
                          bloc.totalAsset.updateValue(value);
                        },
                      ),
                    ),
                    Text(
                      "Sumber Dana",
                      style: TextStyle(
                        fontSize: FontSize.s15,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      "(Dapat memilih lebih dari satu)",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        color: Color(0xff858585),
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      4,
                      Sizes.s700,
                      SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        child: CheckboxGroupFieldBlocBuilder<SubDataBiodata>(
                          padding: EdgeInsets.all(0),
                          multiSelectFieldBloc: bloc.fundsSource,
                          itemBuilder: (context, item) => item.name,
                          isEnabled: KycHelpers.fieldCheck(bloc.status),
                          decoration: InputDecoration(
                            labelText: '',
                            errorMaxLines: 5,
                            prefixIcon: SizedBox(),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      "Motivasi Investasi",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.investPurposes,
                        label: "Tujuan Investasi",
                        hintText: "Lain-lain",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              if (bloc.status == "verified") {
                                PopupHelper.showConfirmationDataChange(context,
                                    () {
                                  Navigator.pop(context);
                                  KycPekerjaanHelper.dokumen.isSubmitted = true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                });
                              } else {
                                KycPekerjaanHelper.dokumen.isSubmitted = true;
                                FocusScope.of(context).unfocus();
                                bloc.submit();
                              }
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
