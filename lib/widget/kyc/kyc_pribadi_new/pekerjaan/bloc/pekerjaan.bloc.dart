import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/CurrencyFormat.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/kyc/PekerjaanHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/pribadi/PekerjaanModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';

class PekerjaanUserBloc extends FormBloc<String, String> {
  final bool isEdit;
  final String submissionId;
  final String status;

  final KycPersonalService _service = KycPersonalService();
  PekerjaanModel pekerjaan = PekerjaanModel();
  static var rawOccupation = [
    SubDataBiodata(id: "1", name: "Pegawai Swasta"),
    SubDataBiodata(id: "2", name: "Pegawai Negeri Sipil"),
    SubDataBiodata(id: "3", name: "Ibu Rumah Tangga"),
    SubDataBiodata(id: "4", name: "Wiraswasta / Pengusaha"),
    SubDataBiodata(id: "5", name: "Pelajar"),
    SubDataBiodata(id: "6", name: "TNI / Polisi"),
    SubDataBiodata(id: "7", name: "Pensiunan"),
    SubDataBiodata(id: "8", name: "Guru"),
    SubDataBiodata(id: "OTH", name: "Lainnya"),
  ];
  static var rawFundsSoucre = [
    SubDataBiodata(id: "1", name: "Gaji"),
    SubDataBiodata(id: "2", name: "Profit Bisnis"),
    SubDataBiodata(id: "3", name: "Bunga"),
    SubDataBiodata(id: "4", name: "Warisan"),
    SubDataBiodata(id: "5", name: "Hibah dari Orang Tua atau Anak"),
    SubDataBiodata(id: "6", name: "Hibah dari Pasangan"),
    SubDataBiodata(id: "7", name: "Dana Pensiun"),
    SubDataBiodata(id: "8", name: "Undian"),
    SubDataBiodata(id: "9", name: "Hasil Investasi"),
    SubDataBiodata(id: "10", name: "Deposito"),
    SubDataBiodata(id: "11", name: "Modal"),
    SubDataBiodata(id: "12", name: "Pinjaman"),
    SubDataBiodata(id: "13", name: "Lain-lain"),
  ];
  static var rawInvestPurposes = [
    SubDataBiodata(id: "1", name: "Lain-lain"),
    SubDataBiodata(id: "2", name: "Price appreciation"),
    SubDataBiodata(id: "3", name: "Investasi jangka panjang"),
    SubDataBiodata(id: "4", name: "Spekulasi"),
    SubDataBiodata(id: "5", name: "Pendapatan"),
  ];
  final occupation =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawOccupation);
  final businessDesc = TextFieldBloc();
  // final totalAssets = TextFieldBloc();
  final totalAsset = InputFieldBloc();
  final fundsSource =
      MultiSelectFieldBloc<SubDataBiodata, dynamic>(items: rawFundsSoucre);
  final investPurposes =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawInvestPurposes);

  PekerjaanUserBloc({
    this.isEdit,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      occupation,
      // totalAssets,
      totalAsset,
      fundsSource,
      investPurposes,
    ]);
    getPekerjaanData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
      updateAllFields();
    });
    // handling jika user memilih wiraswasta
    occupation.onValueChanges(onData: (previous, current) async* {
      if (current.value.id == "4" || current.value.id == "OTH") {
        addFieldBloc(fieldBloc: businessDesc);
      } else {
        businessDesc.clear();
        RemoveFieldBloc(fieldBloc: businessDesc);
      }
    });
  }

  // TOTAL ASSETS
  // HANDLING PERUBAHAN VALUE UNTUK TOTAL ASET USER
  void updateAssetValue(String value) {}
  // END

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("description_job", businessDesc);
    // updateTextFieldStatus("total_assets", totalAssets);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("job_name", occupation);
    updateFieldStatus("source_of_investor_funds", fundsSource);
    updateFieldStatus("reason", investPurposes);
    updateFieldStatus("total_assets", totalAsset);
  }

  // UPDATE FIELD JIKA USER MELAKUKAN EDITING
  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "4", 1);
    });
  }

  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      await _service.updateFieldStatus(submissionId, field, "4", 1);
    });
  }

  onFieldsChange() {
    detectChanges(occupation);
    // detectChanges(totalAssets);
    detectChanges(totalAsset);
    detectChanges(fundsSource);
    detectChanges(investPurposes);
    detectChanges(totalAsset);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      KycPekerjaanHelper.dokumen.isEdited = true;
    });
  }

  // method untuk ngedapetin value setiap field
  // dijalankan ketika state adalah isEdit
  Future getPekerjaanData() async {
    emitLoading(); // update state ke loading
    try {
      await _service.getPekerjaanData().then((value) async {
        if (value.statusCode == 200) {
          pekerjaan = PekerjaanModel.fromJson(value.data); // parsing data
          getFieldErrorStatus();
          // update field
          // pekerjaan
          var userOccupation = occupation.state.items.where((element) =>
              element.name.toLowerCase() == pekerjaan.name.toLowerCase());
          occupation.updateInitialValue(userOccupation.first);
          // deskripsi pekerjaan
          businessDesc.updateInitialValue(pekerjaan.descriptionJob);
          // total aset
          // totalAssets.updateInitialValue(
          //   RupiahFormatter.initialValueFormat(
          //       pekerjaan.totalAssetsOfInvestors),
          // );
          // total aset 2
          totalAsset.updateInitialValue(RupiahFormatter.initialValueFormat(
              pekerjaan.totalAssetsOfInvestors));
          // sumber dana
          List<SubDataBiodata> datas = List<SubDataBiodata>();
          var userFundsSource = pekerjaan.sourceOfInvestorFunds.split(",");
          userFundsSource.forEach((element) {
            fundsSource.state.items.forEach((val) {
              if (element.toLowerCase() == val.name.toLowerCase()) {
                datas.add(val);
              }
            });
          });
          fundsSource.updateInitialValue(datas);
          // motivasi investasi
          var userMotivation = investPurposes.state.items.where((element) =>
              element.name.toLowerCase() ==
              pekerjaan.reasonToJoin.toLowerCase());
          // print(userMotivation.first.name);
          investPurposes.updateInitialValue(userMotivation.first);
          // emitLoaded();
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitLoadFailed(
              failureResponse:
                  "SUBMITERROR Tidak dapat mengambil data pekerjaan");
        }
      });
    } catch (e) {
      emitLoadFailed(
          failureResponse: "SUBMITERROR Tidak dapat mengambil data pekerjaan");
    }
  }

  // Jika user melakukan edit
  // Ketika KYC Ditolak
  // Get alasan ditolak pada setiap field
  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "4").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                // print("${element.fieldId} : ${element.error}");
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          // print(">> Here the error!");
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      // print(">> Here the error!");
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  bool isOccupationError() {
    if (occupation.value == null) {
      occupation.addFieldError("Pekerjaan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isBusinessDescError() {
    if (occupation.value != null) {
      if (occupation.value.id == "4" || occupation.value.id == "OTH") {
        if (businessDesc.value == null) {
          businessDesc.addFieldError("Bidang usaha tidak boleh kosong");
          return true;
        } else if (businessDesc.value.length > 120) {
          businessDesc.addFieldError("Bidang usaha maksimal 120 karakter");
          return true;
        } else {
          if (businessDesc.value.length < 1) {
            businessDesc.addFieldError("Bidang usaha tidak boleh kosong");
            return true;
          }
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // bool isTotalAssetsError() {
  //   if (totalAssets.value != null && totalAssets.value.length > 0) {
  //     var assets = DigitParser.parse(totalAssets.value);
  //     if ((!RegExp(r"^[0-9+#-]*$").hasMatch(assets.toString()))) {
  //       totalAssets.addFieldError("Total aset harus berisi angka!");
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   } else {
  //     return false;
  //   }
  // }

  bool isTotalAssetError() {
    if (totalAsset.value != null && totalAsset.value.length > 0) {
      var assets = DigitParser.parse(totalAsset.value);
      if ((!RegExp(r"^[0-9+#-]*$").hasMatch(assets.toString()))) {
        totalAsset.addFieldError("Total aset harus berisi angka!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isFundsSourceError() {
    if (fundsSource.value != null && fundsSource.value.length > 0) {
      return false;
    } else {
      fundsSource.addFieldError("Sumber dana tidak boleh kosong");
      return true;
    }
  }

  bool isInvestPurposesError() {
    if (investPurposes.value == null) {
      investPurposes.addFieldError("Tujuan Investasi tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message);
    emitFailure(failureResponse: emit);
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      case "name":
        addErrorMessage(occupation, _errMsg, "occupation");
        break;
      case "job_name":
        addErrorMessage(occupation, _errMsg, "occupation");
        break;
      case "description_job":
        addErrorMessage(businessDesc, _errMsg, "business_desc");
        break;
      case "source_of_investor_funds":
        addErrorMessage(fundsSource, _errMsg, "funds_source");
        break;
      case "total_assets":
        addErrorMessage(totalAsset, _errMsg, "total_assets");
        break;
      case "total_assets_of_investor":
        addErrorMessage(totalAsset, _errMsg, "total_assets");
        break;
      case "reason":
        addErrorMessage(investPurposes, _errMsg, "invest_purposes");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT No validation exist!");
        break;
    }
  }

  @override
  void onLoading() {
    // if (!isEdit) emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      // await Future.delayed(Duration(milliseconds: 1500));
      if (isOccupationError()) {
        emitFailure(failureResponse: "occupation");
      } else if (isBusinessDescError()) {
        emitFailure(failureResponse: "business_desc");
      } else if (isTotalAssetError()) {
        emitFailure(failureResponse: "total_assets");
      } else if (isFundsSourceError()) {
        emitFailure(failureResponse: "funds_source");
      } else if (isInvestPurposesError()) {
        emitFailure(failureResponse: "invest_purposes");
      } else {
        var dana = StringBuffer();
        fundsSource.value.forEach((val) {
          // sumberDana += "${val.name},";
          dana.write(val.name + ",");
        });

        PekerjaanUser data = PekerjaanUser(
          occupation: occupation?.value?.name ?? "",
          businessDesc: businessDesc.value != null ? businessDesc?.value : "",
          assetTotal: DigitParser.parse(totalAsset.value),
          fundsSource: dana.toString().substring(0, dana.toString().length - 1),
          investmentPurposes: investPurposes?.value?.name ?? "",
          investmentPurposesId: investPurposes?.value?.id ?? "",
          code: occupation?.value?.id ?? "",
        );

        if (isEdit) {
          await _service.updatePekerjaan(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                emitSuccess(successResponse: "Berhasil mengirim data!");
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else {
                emitFailure(failureResponse: "ERRORSUBMIT ${value.statusCode}");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        } else {
          await _service.sendPekerjaan(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                emitSuccess(successResponse: "Berhasil mengirim data!");
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else {
                emitFailure(failureResponse: "ERRORSUBMIT ${value.statusCode}");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        }
      }
    } catch (e, stack) {
      // print(">> Err : $e");
      // print(stack);
      emitFailure(
        failureResponse:
            "ERRORSUBMIT Terjadi kesalahan saat mencoba mengirimkan data!",
      );
    }
  }
}
