import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/helpers/CurrencyFormat.dart';
import 'package:santaraapp/helpers/ImageProcessor.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/kyc/InformasiPajakHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/pribadi/InformasiPajakModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/utils/logger.dart';

class InformasiPajakUserBloc extends FormBloc<String, String> {
  final bool isEdit;
  final String submissionId;
  final String status;
  final KycPersonalService _service = KycPersonalService();
  final storage = new secureStorage.FlutterSecureStorage(); // storage
  InformasiPajakModel informasiPajak = InformasiPajakModel();

  static var rawIncome = [
    SubDataBiodata(id: "1", name: "<Rp 10 Juta"),
    SubDataBiodata(id: "2", name: "Rp 10 juta - Rp 50 juta"),
    SubDataBiodata(id: "3", name: "Rp 50 juta - Rp 100 juta"),
    SubDataBiodata(id: "4", name: "Rp 100 juta - Rp 500 juta"),
    SubDataBiodata(id: "5", name: "Rp 500 juta - Rp 1 milyar"),
    SubDataBiodata(id: "6", name: "> 1 milyar"),
  ];
  static var rawTax = [
    SubDataBiodata(id: "1010", name: "1010 Individual - Domestik"),
    SubDataBiodata(id: "1011", name: "1011 Individual - Foreign"),
    SubDataBiodata(id: "1083", name: "1083 Individual Foreign - Kitas"),
    SubDataBiodata(id: "1242", name: "1242 Individual Foreign Kitas - NPWP"),
  ];
  static var rawSecuritiesAcc = [
    SubDataBiodata(id: "1", name: "Ya, Punya"),
    SubDataBiodata(id: "2", name: "Tidak Punya"),
  ];
  static var rawMonths = [
    SubDataBiodata(id: "01", name: "Januari"),
    SubDataBiodata(id: "02", name: "Februari"),
    SubDataBiodata(id: "03", name: "Maret"),
    SubDataBiodata(id: "04", name: "April"),
    SubDataBiodata(id: "05", name: "Mei"),
    SubDataBiodata(id: "06", name: "Juni"),
    SubDataBiodata(id: "07", name: "Juli"),
    SubDataBiodata(id: "08", name: "Agustus"),
    SubDataBiodata(id: "09", name: "September"),
    SubDataBiodata(id: "10", name: "Oktober"),
    SubDataBiodata(id: "11", name: "November"),
    SubDataBiodata(id: "12", name: "Desember"),
  ];
  final income = TextFieldBloc();
  // final income = SelectFieldBloc<SubDataBiodata, dynamic>(items: rawIncome);
  final taxAccountCode =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawTax);
  final npwp = TextFieldBloc();
  // tanggal registrasi npwp
  final dateRegNpwp =
      TextFieldBloc(); // tanggal registrasi npwp ( throw error kesini )
  final dayNpwp = TextFieldBloc();
  final monthNpwp = SelectFieldBloc<SubDataBiodata, dynamic>(items: rawMonths);
  final yearNpwp = TextFieldBloc();
  // end
  // rekening efek
  final sidNumber = TextFieldBloc();
  final haveSecuritiesAccount =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawSecuritiesAcc);
  // tanggal registrasi rekening efek
  final dateRegSecAcc = TextFieldBloc();
  final daySecAcc = TextFieldBloc();
  final monthSecAcc =
      SelectFieldBloc<SubDataBiodata, dynamic>(items: rawMonths);
  final yearSecAcc = TextFieldBloc();
  // end
  // photo
  final securitiesAccount = InputFieldBloc<File, Object>();
  // end

  InformasiPajakUserBloc({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      income,
      taxAccountCode,
      npwp,
      dateRegNpwp,
      dayNpwp,
      monthNpwp,
      yearNpwp,
      haveSecuritiesAccount,
    ]);

    getInformasiPajakData().then((value) async {
      await Future.delayed(Duration(milliseconds: 100));
      onFieldsChange();
      updateAllFields();
    });

    haveSecuritiesAccount.onValueChanges(onData: (previous, current) async* {
      if (current.value.id == "1") {
        addFieldBlocs(fieldBlocs: [
          dateRegSecAcc,
          daySecAcc,
          monthSecAcc,
          yearSecAcc,
          securitiesAccount,
          sidNumber,
        ]);
        detectChanges(dateRegSecAcc);
        detectChanges(daySecAcc);
        detectChanges(monthSecAcc);
        detectChanges(yearSecAcc);
        detectChanges(securitiesAccount);
        detectChanges(sidNumber);
      } else {
        removeFieldBlocs(fieldBlocs: [
          dateRegSecAcc,
          daySecAcc,
          monthSecAcc,
          yearSecAcc,
          securitiesAccount,
          sidNumber,
        ]);
      }
    });
  }

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("income", income);
    updateTextFieldStatus("npwp", npwp);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("tax_account_code", taxAccountCode);
    updateFieldStatus("have_securities_account", haveSecuritiesAccount);
    updateFieldStatus("securities_account", securitiesAccount);
    updateFieldStatus("sid_number", sidNumber);
  }

  // UPDATE FIELD JIKA USER MELAKUKAN EDITING
  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "5", 1);
    });
  }

  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      _service.updateFieldStatus(submissionId, field, "5", 1);
    });
  }

  Future getInformasiPajakData() async {
    emitLoading();
    try {
      await _service.getInformasiPajakData().then((value) async {
        if (value.statusCode == 200) {
          informasiPajak = InformasiPajakModel.fromJson(value.data);
          getFieldErrorStatus();

          // pendapatan pertahun
          if (informasiPajak.income != null) {
            income.updateInitialValue(
              RupiahFormatter.initialValueFormat(informasiPajak.income),
            );
          }

          // kode akun pajak
          if (informasiPajak.taxAccountCode != null) {
            var userCode = taxAccountCode.state.items.where(
                (element) => element.id == informasiPajak.taxAccountCode);
            taxAccountCode.updateInitialValue(userCode.first);
          }

          // nomor npwp
          if (informasiPajak.npwp != null) {
            npwp.updateInitialValue(informasiPajak.npwp);
          }

          // tanggal registrasi npwp
          if (informasiPajak.dateRegistrationOfNpwp != null) {
            var tglRegNpwp =
                DateTime.parse(informasiPajak.dateRegistrationOfNpwp);
            // update tanggal registrasi npwp
            dayNpwp.updateInitialValue(
                "${tglRegNpwp.day < 10 ? '0' + tglRegNpwp.day.toString() : tglRegNpwp.day}");
            monthNpwp.updateInitialValue(
                monthNpwp.state.items[tglRegNpwp.month - 1]);
            yearNpwp.updateInitialValue(tglRegNpwp.year.toString());
          }
          // end npwp

          // Rekening efek punya?
          if (informasiPajak.haveSecuritiesAccount != null) {
            haveSecuritiesAccount.updateInitialValue(haveSecuritiesAccount
                .state.items[informasiPajak.haveSecuritiesAccount ? 0 : 1]);

            // jika punya rekening efek
            if (informasiPajak.haveSecuritiesAccount) {
              // jika tanggal registrasi tidak kosong
              if (informasiPajak.securitiesAccountDateRegistration != null) {
                // tanggal registrasi rekening efek
                var tglRegSec = DateTime.parse(
                  informasiPajak.securitiesAccountDateRegistration,
                );
                // update tanggal registrasi npwp
                daySecAcc.updateInitialValue(
                    "${tglRegSec.day < 10 ? '0' + tglRegSec.day.toString() : tglRegSec.day}");
                monthSecAcc.updateInitialValue(
                    monthNpwp.state.items[tglRegSec.month - 1]);
                yearSecAcc.updateInitialValue(tglRegSec.year.toString());
              }

              // Nomor sid
              // Jika nomor sid tidak kosong
              if (informasiPajak.sidNumber != null) {
                sidNumber.updateValue(informasiPajak.sidNumber);
              }

              // foto rekening efek
              // Jika foto rekening efek tidak kosong
              if (informasiPajak.securitiesAccount != null) {
                final token = await storage.read(key: 'token');
                securitiesAccount.updateExtraData({
                  "hasPhoto": true,
                  "filename": informasiPajak.securitiesAccount,
                  "url": GetAuthenticatedFile.convertUrl(
                    image: informasiPajak.securitiesAccount,
                    type: PathType.traderPhoto,
                  ),
                });
              }
              // end rekening efek
            }
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitLoadFailed(
            failureResponse:
                "ERRORSUBMIT [${value.statusCode}] Tidak dapat menerima data informasi pajak",
          );
        }
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      emitLoadFailed(
        failureResponse:
            "ERRORSUBMIT Tidak dapat menerima data informasi pajak",
      );
    }
  }

  // pick file
  void pickFile(InputFieldBloc bloc) async {
    var files = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf', 'jpg', 'jpeg'],
      allowMultiple: false,
    );
    var file = File(files.files[0].path);
    final bool allowed = await ImageProcessor.checkImageExtension(
        file, [".pdf", ".jpeg", '.jpg']);
    if (allowed) {
      bloc.updateInitialValue(file);
      bloc.updateExtraData(null);
    } else {
      bloc.addFieldError("Format File Tidak Diijinkan!");
    }
  }

  // Jika user melakukan edit
  // Ketika KYC Ditolak
  // Get alasan ditolak pada setiap field
  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "5").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                // print("${element.fieldId} : ${element.error}");
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitLoadFailed(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitLoadFailed(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  onFieldsChange() {
    detectChanges(income);
    detectChanges(taxAccountCode);
    detectChanges(npwp);
    detectChanges(dateRegNpwp);
    detectChanges(dayNpwp);
    detectChanges(monthNpwp);
    detectChanges(yearNpwp);
    detectChanges(haveSecuritiesAccount);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      KycInformasiPajakHelper.dokumen.isEdited = true;
    });
  }

  updateSecuritiesAccountPhoto(File value) {
    if (isEdit) {
      informasiPajak.securitiesAccount = null;
      securitiesAccount.updateValue(value);
    } else {
      securitiesAccount.updateValue(value);
    }
  }

  bool notEmpty(String value) {
    try {
      if (value != null && value.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  bool dayOfMonthCheckError(int day, String month) {
    var monthParsed = month.toLowerCase();
    var months = [
      "januari",
      "maret",
      "mei",
      "juli",
      "agustus",
      "oktober",
      "desember"
    ];
    if (months.contains(monthParsed)) {
      return day > 31 ? true : false;
    } else {
      return day > 30 ? true : false;
    }
  }

  bool isIncomeError() {
    if (income.value != null && income.value.length > 0) {
      return false;
    } else {
      income.addFieldError(
          "Pendapatan pertahun sebelum pajak tidak boleh kosong");
      return true;
    }
  }

  bool isTaxCodeError() {
    if (taxAccountCode.value == null) {
      taxAccountCode.addFieldError("Kode akun pajak tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isNpwpError() {
    if (npwp.value != null && npwp.value.length > 0) {
      if (npwp.value.length > 20) {
        npwp.addFieldError("NPWP maksimal 20 karakter");
        return true;
      } else if (!RegExp(r"^[0-9+#-.]*$").hasMatch(npwp.value)) {
        npwp.addFieldError("NPWP tidak boleh berisi huruf");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isNpwpRegDateError() {
    if (notEmpty(dayNpwp.value) ||
        notEmpty(monthNpwp?.value?.id) ||
        notEmpty(yearNpwp.value)) {
      if (npwp.value != null && npwp.value.length > 0) {
        final today = DateTime.now();
        if (dayNpwp.value.length < 1 ||
            int.parse(dayNpwp.value) > 31 ||
            int.parse(dayNpwp.value) < 1) {
          dayNpwp.addFieldError("Tidak valid!");
          return true;
        } else if (monthNpwp.value == null) {
          monthNpwp.addFieldError("Bulan tidak valid!");
          return true;
        } else if (yearNpwp.value.length < 4) {
          yearNpwp.addFieldError("Tidak valid!");
          return true;
        } else if (int.parse(yearNpwp.value) > today.year) {
          yearNpwp.addFieldError("Tidak valid!");
          return true;
        } else if (!KycHelpers.isLeap(yearNpwp.value)) {
          if (monthNpwp.value.name.toLowerCase() == "februari") {
            if (int.parse(dayNpwp.value) > 28) {
              dayNpwp.addFieldError("Tidak valid");
              return true;
            } else {
              // print("hey");
              return false;
            }
          } else {
            if (dayOfMonthCheckError(
                int.parse(dayNpwp.value), monthNpwp.value.name)) {
              dayNpwp.addFieldError("Tidak valid");
              return true;
            } else {
              var newDay = dayNpwp.value.length < 2
                  ? '0' + dayNpwp.value
                  : dayNpwp.value;
              final DateTime userDob = DateTime.parse(
                  "${yearNpwp.value}-${monthNpwp.value.id}-$newDay");
              final diff = userDob.difference(DateTime.now()).inDays;
              if (diff > 0) {
                dateRegNpwp.addFieldError(
                    "Tanggal yang dipilih harus dari masa lampau!");
                return true;
              } else {
                dateRegNpwp.clear();
                return false;
              }
            }
          }
        } else {
          if (dayOfMonthCheckError(
              int.parse(dayNpwp.value), monthNpwp.value.name)) {
            dayNpwp.addFieldError("Tidak valid");
            return true;
          } else {
            var newDay =
                dayNpwp.value.length < 2 ? '0' + dayNpwp.value : dayNpwp.value;
            final DateTime userDob = DateTime.parse(
                "${yearNpwp.value}-${monthNpwp.value.id}-$newDay");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff > 0) {
              dateRegNpwp.addFieldError(
                  "Tanggal yang dipilih harus dari masa lampau!");
              return true;
            } else {
              dateRegNpwp.clear();
              return false;
            }
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isHaveSecuritiesAccountError() {
    if (haveSecuritiesAccount.value == null) {
      haveSecuritiesAccount.addFieldError("Field ini wajib diisi!");
      return true;
    } else {
      return false;
    }
  }

  bool isSidNumberError() {
    try {
      if (haveSecuritiesAccount.value.id == "1") {
        if (sidNumber.value != null) {
          if (sidNumber.value.length < 1) {
            sidNumber.addFieldError("Nomor SID Tidak Boleh Kosong!");
            return true;
          } else {
            return false;
          }
        } else {
          sidNumber.addFieldError("Nomor SID Tidak Boleh Kosong!");
          return true;
        }
      } else {
        return false;
      }
    } catch (e) {
      // print(e);
      return false;
    }
  }

  bool isRekeningEfekRegDateError() {
    if (haveSecuritiesAccount.value != null) {
      if (haveSecuritiesAccount.value.id == "1") {
        final today = DateTime.now();
        if (daySecAcc.value.length < 1 ||
            int.parse(daySecAcc.value) > 31 ||
            int.parse(daySecAcc.value) < 1) {
          daySecAcc.addFieldError("Tidak valid!");
          // print("heyx23");
          return true;
        } else if (monthSecAcc.value == null) {
          monthSecAcc.addFieldError("Bulan tidak valid!");
          // print("heyx54");
          return true;
        } else if (yearSecAcc.value.length < 4) {
          yearSecAcc.addFieldError("Tidak valid!");
          // print("heyxqwe");
          return true;
        } else if (int.parse(yearSecAcc.value) > today.year) {
          yearSecAcc.addFieldError("Tidak valid!");
          // print("heysax");
          return true;
        } else if (!KycHelpers.isLeap(yearSecAcc.value)) {
          if (monthSecAcc.value.name.toLowerCase() == "februari") {
            if (int.parse(daySecAcc.value) > 28) {
              daySecAcc.addFieldError("Tidak valid");
              // print("heyx");
              return true;
            } else {
              // print("hey");
              return false;
            }
          } else {
            if (dayOfMonthCheckError(
                int.parse(daySecAcc.value), monthSecAcc.value.name)) {
              daySecAcc.addFieldError("Tidak valid");
              return true;
            } else {
              var newDay = daySecAcc.value.length < 2
                  ? '0' + daySecAcc.value
                  : daySecAcc.value;
              final DateTime userDob = DateTime.parse(
                  "${yearSecAcc.value}-${monthSecAcc.value.id}-$newDay");
              final diff = userDob.difference(DateTime.now()).inDays;
              if (diff > 0) {
                dateRegSecAcc.addFieldError(
                    "Tanggal yang dipilih harus dari masa lampau!");
                return true;
              } else {
                dateRegSecAcc.clear();
                return false;
              }
            }
          }
        } else {
          if (dayOfMonthCheckError(
              int.parse(daySecAcc.value), monthSecAcc.value.name)) {
            daySecAcc.addFieldError("Tidak valid");
            // print("heyxz");
            return true;
          } else {
            var newDay = daySecAcc.value.length < 2
                ? '0' + daySecAcc.value
                : daySecAcc.value;
            final DateTime userDob = DateTime.parse(
                "${yearSecAcc.value}-${monthSecAcc.value.id}-$newDay");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff > 0) {
              dateRegSecAcc.addFieldError(
                  "Tanggal yang dipilih harus dari masa lampau!");
              // print("heyx123");
              return true;
            } else {
              dateRegSecAcc.clear();
              return false;
            }
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isPhotoSecuritiesAccountError() {
    if (haveSecuritiesAccount.value != null) {
      if (haveSecuritiesAccount.value.id == "1") {
        if (isEdit) {
          if (securitiesAccount.value == null) {
            if (informasiPajak.securitiesAccount == null) {
              securitiesAccount.addFieldError(
                  "Foto Bukti Kepemilikan Rekening Efek Tidak Boleh Kosong!");
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        } else {
          if (securitiesAccount.value == null) {
            securitiesAccount.addFieldError(
                "Foto Bukti Kepemilikan Rekening Efek Tidak Boleh Kosong!");
            return true;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message);
    emitFailure(failureResponse: emit);
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      case "income":
        addErrorMessage(income, _errMsg, "income");
        break;
      case "tax_account_code":
        addErrorMessage(taxAccountCode, _errMsg, "tax_account_code");
        break;
      case "npwp":
        addErrorMessage(npwp, _errMsg, "npwp");
        break;
      case "date_registration_of_npwp":
        addErrorMessage(dateRegNpwp, _errMsg, "date_registration_of_npwp");
        break;
      case "have_securities_account":
        addErrorMessage(
            haveSecuritiesAccount, _errMsg, "have_securities_account");
        break;
      case "securities_account_date_registration":
        addErrorMessage(
            dateRegSecAcc, _errMsg, "securities_account_date_registration");
        break;
      case "securities_account":
        addErrorMessage(securitiesAccount, _errMsg, "securities_account");
        break;
      case "sid_number":
        addErrorMessage(sidNumber, _errMsg, "sid_number");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat mengirim data!");
        break;
    }
  }

  @override
  void onLoading() {
    // if (!isEdit) emitLoaded();
  }

  @override
  void onSubmitting() async {
    try {
      if (isIncomeError()) {
        emitFailure(failureResponse: "income");
      } else if (isTaxCodeError()) {
        emitFailure(failureResponse: "tax_account_code");
      } else if (isNpwpError()) {
        emitFailure(failureResponse: "npwp");
      } else if (isNpwpRegDateError()) {
        emitFailure(failureResponse: "date_registration_of_npwp");
      } else if (isHaveSecuritiesAccountError()) {
        emitFailure(failureResponse: "have_securities_account");
      } else if (isSidNumberError()) {
        emitFailure(failureResponse: "sid_number");
      } else if (isRekeningEfekRegDateError()) {
        emitFailure(failureResponse: "securities_account_date_registration");
      } else if (isPhotoSecuritiesAccountError()) {
        emitFailure(failureResponse: "securities_account");
      } else {
        try {
          InformasiPajakUser data = InformasiPajakUser(
            income: DigitParser.parse(income.value),
            taxAccountCode: int.parse(taxAccountCode?.value?.id ?? 0),
            npwp: npwp?.value,
            dateRegNpwp: dayNpwp.value == null ||
                    monthNpwp.value == null ||
                    yearNpwp.value == null
                ? ""
                : "${yearNpwp.value}-${monthNpwp.value.id}-${dayNpwp.value}",
            haveSecurityAccount:
                haveSecuritiesAccount.value.id == "1" ? true : false,
            securityAccountRegDate: haveSecuritiesAccount.value != null
                ? haveSecuritiesAccount.value.id == "1"
                    ? "${yearSecAcc.value}-${monthSecAcc.value.id}-${daySecAcc.value}"
                    : ""
                : "",
            securitiesAccount: haveSecuritiesAccount.value != null
                ? haveSecuritiesAccount.value.id == "1"
                    ? securitiesAccount.value
                    : null
                : null,
            sidNumber: sidNumber?.value ?? "",
          );
          if (isEdit) {
            await _service.updateInformasiPajak(data).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess(successResponse: "Berhasil mengirim data!");
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else {
                  emitFailure(
                    failureResponse:
                        "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
                  );
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          } else {
            await _service.sendInformasiPajak(data).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess(successResponse: "Berhasil mengirim data!");
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else {
                  emitFailure(
                    failureResponse:
                        "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
                  );
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          }
        } catch (e, stack) {
          // print(e);
          // print(stack);
          emitFailure(failureResponse: "ERRORSUBMIT $e");
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      emitFailure(
          failureResponse: "ERRORSUBMIT Terjadi kesalahan saat mengirim data!");
    }
  }
}
