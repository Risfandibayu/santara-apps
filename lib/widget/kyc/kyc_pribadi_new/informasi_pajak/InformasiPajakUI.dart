import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/InformasiPajakHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/Camera.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraCachedImage.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/informasi.pajak.bloc.dart';

class InformasiPajakUI extends StatelessWidget {
  final bool isEdit;
  final String submissionId;
  final String status;
  InformasiPajakUI({
    this.isEdit,
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycInformasiPajakHelper.dokumen.isEdited != null) {
          if (KycInformasiPajakHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycInformasiPajakHelper.dokumen = InformasiPajakUser();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              return false;
            });
            return false;
          } else {
            KycInformasiPajakHelper.dokumen = InformasiPajakUser();
            return true;
          }
        } else {
          KycInformasiPajakHelper.dokumen = InformasiPajakUser();

          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Informasi Pajak",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "5/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => InformasiPajakUserBloc(
              isEdit: isEdit,
              submissionId: submissionId,
              status: status,
            ),
            child: InformasiPajakForm(),
          )),
    );
  }
}

class InformasiPajakForm extends StatefulWidget {
  @override
  _InformasiPajakFormState createState() => _InformasiPajakFormState();
}

class _InformasiPajakFormState extends State<InformasiPajakForm> {
  InformasiPajakUserBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  // informasi pajak
  Widget _content(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Sizes.s10),
      width: double.maxFinite,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(
                  top: Sizes.s10, left: Sizes.s10, right: Sizes.s10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Yang termasuk penghasilan berdasarkan UU Pajak Penghasilan no 36, diantaranya : ",
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: FontSize.s14),
                  ),
                  Text(
                    """-Gaji bulanan (12 bulan) 
-Tunjangan, Honor, Komisi ,THR 
-Keuntungan dari penjualan barang / jasa 
-Bunga, Dividen 
-Premi asuransi 
-Penerimaan atau pembayaran piutang berkala 
-Penghasilan lainya""",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    height: Sizes.s10,
                  ),
                  Text(
                    "Limit Investasi Berdasarkan POJK 57 Tahun 2020 :",
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: FontSize.s14),
                  ),
                  RichText(
                    text: TextSpan(
                      text: '',
                      children: <TextSpan>[
                        TextSpan(
                          text: "1. Untuk Anda yang berpenghasilan ",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: "dibawah atau sama dengan ",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        TextSpan(
                          text:
                              "Rp. 500.000.000 pertahun, dapat berinvestasi maksimal 5% dari jumlah penghasilan pertahun",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Sizes.s10,
                  ),
                  RichText(
                    text: TextSpan(
                      text: '',
                      children: <TextSpan>[
                        TextSpan(
                          text: "2. Untuk Anda yang berpenghasilan ",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                          text: "diatas ",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        TextSpan(
                          text:
                              "Rp. 500.000.000 pertahun, dapat berinvestasi hingga 10% dari jumlah penghasilan per tahun.",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Sizes.s10,
                  ),
                  RichText(
                    text: TextSpan(
                      text: '',
                      children: <TextSpan>[
                        TextSpan(
                          text: "Contoh : ",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Color(0xffBF2D30),
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        TextSpan(
                          text:
                              "Jika penghasilan pertahun anda adalah Rp. 100.000.000 maka batas maksimal investasi anda di SANTARA adalah Rp. 5.000.000",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Sizes.s10,
                  ),
                  Text(
                    "LIMIT INVESTASI DIATAS TIDAK BERLAKU",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Color(0xffBF2D30),
                      fontSize: FontSize.s14,
                    ),
                  ),
                  Text(
                    "Jika Anda dapat menyertakan foto kartu/bukti kepemilikan rekening efek (minimal sudah aktif selama 2 tahun). Silakan unggah bukti rekening tersebut pada field dibawah ini.",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: Sizes.s20,
            ),
            SantaraMainButton(
              onPressed: () {
                Navigator.pop(context);
              },
              title: "Mengerti",
            )
          ],
        ),
      ),
    );
  }

  // image picker widget (ktp & selfie)
  Widget _pickerView(File file) {
    return Column(
      children: [
        DottedBorder(
          color: Color(0xFFB8B8B8),
          strokeWidth: 2,
          radius: Radius.circular(Sizes.s15),
          dashPattern: [5, 5],
          child: checkIsPdf(file)
              ? Container(
                  width: Sizes.s250,
                  height: Sizes.s150,
                  child: file != null
                      ? Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.insert_drive_file,
                              color: Color(0xFFB8B8B8),
                              size: Sizes.s30,
                            ),
                            SizedBox(
                              height: Sizes.s10,
                            ),
                            Text(
                              "${file.path}",
                              style: TextStyle(
                                fontSize: FontSize.s15,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFFB8B8B8),
                              ),
                            )
                          ],
                        )
                      : _pickerView(file),
                )
              : Container(
                  width: Sizes.s250,
                  height: Sizes.s150,
                  decoration: BoxDecoration(
                    color: Color(0xffF4F4F4),
                    image: file != null
                        ? DecorationImage(image: FileImage(file))
                        : null,
                  ),
                  child: file != null
                      ? Container()
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.insert_drive_file,
                              color: Color(0xFFB8B8B8),
                              size: Sizes.s30,
                            ),
                            SizedBox(
                              height: Sizes.s10,
                            ),
                            Text(
                              "Unggah File",
                              style: TextStyle(
                                fontSize: FontSize.s15,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFFB8B8B8),
                              ),
                            )
                          ],
                        ),
                ),
        ),
        SizedBox(height: Sizes.s10),
        Text(
          "Jpeg/PDF Maks. 5Mb",
          style: TextStyle(
            fontSize: FontSize.s12,
            color: Color(0xffB8B8B8),
          ),
        )
      ],
    );
  }

  // Image picker, ketika user sudah ambil foto, ubah foto
  Widget _imagePikcer(InputFieldBloc fieldBloc) {
    return BlocBuilder<InputFieldBloc, InputFieldBlocState>(
      bloc: fieldBloc,
      builder: (context, state) {
        File file = state.value;
        if (bloc.isEdit) {
          return state.value == null
              ? state.extraData != null // extra data
                  ? Center(
                      child: DottedBorder(
                        color: Color(0xFFB8B8B8),
                        strokeWidth: 2,
                        radius: Radius.circular(Sizes.s15),
                        dashPattern: [5, 5],
                        child: file != null
                            ? checkIsPdf(file)
                                // jika file yg dipilih adalah PDF
                                ? Container(
                                    width: Sizes.s250,
                                    height: Sizes.s150,
                                    child: file != null
                                        ? Column(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.insert_drive_file,
                                                color: Colors.black,
                                                size: Sizes.s30,
                                              ),
                                              SizedBox(
                                                height: Sizes.s10,
                                              ),
                                              Text(
                                                "${file.path}",
                                                style: TextStyle(
                                                  fontSize: FontSize.s15,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.black,
                                                ),
                                              )
                                            ],
                                          )
                                        : _pickerView(file),
                                  )
                                // jika file adalah foto
                                : Container(
                                    width: Sizes.s250,
                                    height: Sizes.s150,
                                    decoration: BoxDecoration(
                                      color: Color(0xffF4F4F4),
                                      image: file != null
                                          ? DecorationImage(
                                              image: FileImage(file))
                                          : null,
                                    ),
                                    child: file != null
                                        ? Container()
                                        : _pickerView(file),
                                  )
                            : Container(
                                width: Sizes.s250,
                                height: Sizes.s150,
                                child:
                                    state.extraData["filename"].contains(".pdf")
                                        ? Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.insert_drive_file,
                                                color: Color(0xFFB8B8B8),
                                                size: Sizes.s30,
                                              ),
                                              SizedBox(
                                                height: Sizes.s10,
                                              ),
                                              Text(
                                                "${state.extraData["filename"]}"
                                                    .split('?')
                                                    .first
                                                    .split('/')
                                                    .last,
                                                style: TextStyle(
                                                  fontSize: FontSize.s15,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.grey,
                                                ),
                                              )
                                            ],
                                          )
                                        : SantaraCachedImage(
                                            image: state.extraData["url"],
                                            fit: BoxFit.contain,
                                          ),
                              ),
                      ),
                    )
                  : _pickerView(file)
              : _pickerView(file);
        } else {
          return _pickerView(file);
        }
      },
    );
  }

  // cek ekstensi file (apakah pdf?)
  bool checkIsPdf(File file) {
    if (file == null) {
      return false;
    } else {
      final String fileName = path.basename(file.path);
      final delim = fileName.lastIndexOf('.');
      String ext = fileName.substring(delim >= 0 ? delim : 0);
      return ext.contains("pdf") ? true : false;
    }
  }

  Future<File> pickPhoto() async {
    var cameras = await availableCameras();
    File file;
    if (cameras.length == 0) {
      file = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      try {
        file = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => Camera(isKyc: 2),
          ),
        );
      } catch (e, stack) {
        ToastHelper.showFailureToast(context, e);
        return null;
      }
    }
    return file;
  }

  // pick file (pdf/jpge)
  Future<void> _pickFile() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Container(
                  height: Sizes.s50,
                  margin: EdgeInsets.only(bottom: Sizes.s20),
                  child: SantaraMainButton(
                    title: "Kamera",
                    onPressed: () async {
                      Navigator.pop(context);
                      if (KycHelpers.fieldCheck(bloc.status)) {
                        FocusScope.of(context).unfocus();
                        var val = await pickPhoto();
                        bloc.updateSecuritiesAccountPhoto(val);
                      }
                    },
                  ),
                ),
                Container(
                  height: Sizes.s50,
                  child: SantaraMainButton(
                    title: "File Picker",
                    onPressed: () async {
                      Navigator.pop(context);
                      bloc.pickFile(bloc.securitiesAccount);
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    KycInformasiPajakHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<InformasiPajakUserBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<InformasiPajakUserBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(context, "${state.successResponse}");
          Navigator.pop(context);
          KycInformasiPajakHelper.dokumen = InformasiPajakUser();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (KycInformasiPajakHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }
          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }
          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse) {
              // CASE = identifier dari emitFailure, tiap value menandakan error pada field sesuai casenya
              // fieldWrapper.scrollTo(x), x = identifier / key widget yang ada di fieldWrapper.viewWidget
              case "income":
                fieldWrapper.scrollTo(1);
                break;
              case "tax_account_code":
                fieldWrapper.scrollTo(2);
                break;
              case "npwp":
                fieldWrapper.scrollTo(3);
                break;
              case "date_registration_of_npwp":
                fieldWrapper.scrollTo(4);
                break;
              case "have_securities_account":
                fieldWrapper.scrollTo(5);
                break;
              case "securities_account_date_registration":
                fieldWrapper.scrollTo(6);
                break;
              case "securities_account":
                fieldWrapper.scrollTo(7);
                break;
              case "sid_number":
                fieldWrapper.scrollTo(8);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<InformasiPajakUserBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                padding: EdgeInsets.all(Sizes.s20),
                child: ListView(
                  scrollDirection: scrollDirection,
                  controller: controller,
                  children: [
                    Text(
                      "Informasi Pajak",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    fieldWrapper.viewWidget(
                      1,
                      Sizes.s100,
                      SantaraRevenueField(
                        bloc: bloc.income,
                        hintText: "Rp.0",
                        labelText: "Penghasilan Pertahun Sebelum Pajak",
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        suffixIcon: InkWell(
                          onTap: () {
                            PopupHelper.showInfoPajak(
                                context, _content(context));
                          },
                          child: Icon(Icons.info),
                        ),
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        onChanged: (String val) {
                          bloc.income.updateValue(val);
                        },
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      2,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.taxAccountCode,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        label: "Kode Akun Pajak",
                        hintText: "1010",
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.npwp,
                        hintText: "*Kosongkan jika tidak ada (Optional)",
                        labelText: "No NPWP",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        inputType: TextInputType.number,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(20),
                        ],
                      ),
                    ),
                    // title tgl kadaluarsa
                    Text(
                      "Tanggal Registrasi NPWP ",
                      style: TextStyle(
                        fontSize: FontSize.s12,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      "*Kosongkan jika tidak mengetahui",
                      style: TextStyle(
                        fontSize: FontSize.s10,
                        color: Color(0xffB8B8B8),
                      ),
                    ),
                    // input tanggal kadaluarsa
                    fieldWrapper.viewWidget(
                      4,
                      Sizes.s110,
                      Padding(
                        padding: EdgeInsets.only(top: Sizes.s10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: Sizes.s100,
                                child: SantaraBlocTextField(
                                  textFieldBloc: bloc.dayNpwp,
                                  hintText: "01",
                                  labelText: "Tanggal",
                                  inputType: TextInputType.number,
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(2),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.only(
                                    left: Sizes.s10, right: Sizes.s10),
                                height: Sizes.s100,
                                child: SantaraBlocDropDown(
                                  selectFieldBloc: bloc.monthNpwp,
                                  label: "Bulan",
                                  hintText: "Januari",
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: Sizes.s100,
                                child: SantaraBlocTextField(
                                  textFieldBloc: bloc.yearNpwp,
                                  hintText: "2020",
                                  labelText: "Tahun",
                                  inputType: TextInputType.number,
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(4),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    KycErrorWrapper(bloc: bloc.dateRegNpwp),
                    fieldWrapper.divider,
                    Row(
                      children: [
                        Text(
                          "Rekening Efek ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: FontSize.s16,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            PopupHelper.showPopMessage(
                              context,
                              "POJK No.57 Tahun 2020",
                              "Apabila memiliki akun rekening efek berumur minimal 2 tahun, Anda tidak dikenakan limit investasi.",
                              textAlign: TextAlign.center,
                            );
                          },
                          child: Icon(
                            Icons.info,
                            size: FontSize.s20,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    Text(
                      "Apakah Anda Memiliki Rekening Efek ?",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s130,
                      SantaraBlocRadioGroup(
                        selectFieldBloc: bloc.haveSecuritiesAccount,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
                        bloc: bloc.haveSecuritiesAccount,
                        builder: (context, state) {
                          if (state.hasValue) {
                            if (state.value.id == "1") {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Nomor SID
                                  fieldWrapper.viewWidget(
                                    8,
                                    Sizes.s100,
                                    SantaraBlocTextField(
                                      textFieldBloc: bloc.sidNumber,
                                      hintText: "Nomor SID",
                                      labelText: "Nomor SID",
                                      enabled:
                                          KycHelpers.fieldCheck(bloc.status),
                                      suffixIcon: InkWell(
                                        onTap: () {
                                          PopupHelper.showPopMessage(
                                            context,
                                            "Single Investor Identification",
                                            "Adalah nomor tunggal identitas investor pasar modal Indonesia yang diterbitkan oleh KSEI. Silakan cek di Akses KSEI atau email pendaftaran efek.",
                                            textAlign: TextAlign.center,
                                          );
                                        },
                                        child: Icon(Icons.info),
                                      ),
                                    ),
                                  ),
                                  // title tgl kadaluarsa
                                  Text(
                                    "Tanggal Registrasi Rekening Efek",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  // input tanggal kadaluarsa
                                  fieldWrapper.viewWidget(
                                    6,
                                    Sizes.s110,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: Sizes.s100,
                                            child: SantaraBlocTextField(
                                              textFieldBloc: bloc.daySecAcc,
                                              hintText: "01",
                                              labelText: "Tanggal",
                                              inputType: TextInputType.number,
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    2),
                                                FilteringTextInputFormatter
                                                    .allow(
                                                  RegExp(r"[0-9]*$"),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                left: Sizes.s10,
                                                right: Sizes.s10),
                                            height: Sizes.s100,
                                            child: SantaraBlocDropDown(
                                              selectFieldBloc: bloc.monthSecAcc,
                                              label: "Bulan",
                                              hintText: "Januari",
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: Sizes.s100,
                                            child: SantaraBlocTextField(
                                              textFieldBloc: bloc.yearSecAcc,
                                              hintText: "2020",
                                              labelText: "Tahun",
                                              inputType: TextInputType.number,
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    4),
                                                FilteringTextInputFormatter
                                                    .allow(
                                                  RegExp(r"[0-9]*$"),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  KycErrorWrapper(bloc: bloc.dateRegSecAcc),
                                  fieldWrapper.divider,
                                  Text(
                                    "Unggah Foto Kepemilikan Rekening Efek",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  fieldWrapper.divider,
                                  InkWell(
                                    onTap: () async {
                                      await _pickFile();
                                    },
                                    child: fieldWrapper.viewWidget(
                                      7,
                                      null,
                                      _imagePikcer(bloc.securitiesAccount),
                                    ),
                                  ),
                                  KycErrorWrapper(bloc: bloc.securitiesAccount)
                                ],
                              );
                            } else {
                              return Container();
                            }
                          } else {
                            return Container();
                          }
                        }),
                    fieldWrapper.divider,
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              if (bloc.status == "verified") {
                                PopupHelper.showConfirmationDataChange(context,
                                    () {
                                  Navigator.pop(context);
                                  KycInformasiPajakHelper.dokumen.isSubmitted =
                                      true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                });
                              } else {
                                KycInformasiPajakHelper.dokumen.isSubmitted =
                                    true;
                                FocusScope.of(context).unfocus();
                                bloc.submit();
                              }
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
