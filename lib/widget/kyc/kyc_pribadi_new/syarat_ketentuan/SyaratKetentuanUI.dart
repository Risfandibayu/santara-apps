import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../models/User.dart';
import '../../../../services/kyc/KycPersonalService.dart';
import '../../../../utils/logger.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../widgets/kyc_complete.dart';

class KycSyaratKetentuanUI extends StatefulWidget {
  @override
  _KycSyaratKetentuanUIState createState() => _KycSyaratKetentuanUIState();
}

class _KycSyaratKetentuanUIState extends State<KycSyaratKetentuanUI> {
  final _service = KycPersonalService();
  bool checked = false;
  // 0 = not loaded, 1 = loaded, 2 = error
  int isLoaded = 0;
  User user;
  int retrial = 0;
  int tncRetrial = 0;

  Future<bool> getSubmissionId() async {
    try {
      var result = await _service.getSubmissionId();
      if (result.statusCode == 200) {
        return true;
      } else {
        if (retrial < 3) {
          setState(() {
            retrial += 1;
          });
          getSubmissionId();
        } else {
          return false;
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      if (retrial < 3) {
        setState(() {
          retrial += 1;
        });
        getSubmissionId();
      } else {
        return false;
      }
    }
  }

  Future sendData2() async {
    if (checked) {
      setState(() {
        isLoaded = 0;
      });
      try {
        await _service.acceptTnC().then((value) async {
          if (value.statusCode == 200) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => KycCompleteUI(),
              ),
            );
          } else if (value.statusCode == 401) {
            NavigatorHelper.pushToExpiredSession(context);
          } else {
            setState(() {
              isLoaded = 1;
            });
            ToastHelper.showFailureToast(
              context,
              "${value.data}",
            );
          }
        });
      } catch (e) {
        ToastHelper.showFailureToast(
            context, "Gagal menyetujui Syarat & Ketentuan");
      }
    } else {
      ToastHelper.showFailureToast(
          context, "Mohon checklist persetujuan diatas!");
    }
  }

  getTnCstatus() async {
    setState(() {
      isLoaded = 0;
    });
    try {
      await _service.getUserData().then((value) async {
        if (value.statusCode == 200) {
          var userData = User.fromJson(value.data);
          if (userData.trader.tnc == 0) {
            await getSubmissionId();
          }
          setState(() {
            isLoaded = 1;
            user = userData;
          });
        } else if (value.statusCode == 401) {
          NavigatorHelper.pushToExpiredSession(context);
        }
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      if (tncRetrial < 3) {
        setState(() {
          tncRetrial += 1;
        });
        getTnCstatus();
      } else {
        setState(() {
          isLoaded = 2;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getTnCstatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(Sizes.s20),
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Center(
          child: isLoaded == 1
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/santara/1.png",
                      height: Sizes.s30,
                    ),
                    SizedBox(height: Sizes.s30),
                    Image.asset(
                      "assets/icon/pra_penawaran.png",
                      height: Sizes.s200,
                    ),
                    SizedBox(height: Sizes.s30),
                    Text(
                      "Dalam estimasi 1x 24jam setelah anda menyetujui syarat dan ketentuan data Anda akan diverifikasi oleh Admin. ",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: Sizes.s20),
                    Text(
                      "Baca",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: FontSize.s14,
                      ),
                    ),
                    SizedBox(height: Sizes.s10),
                    InkWell(
                      onTap: () async {
                        var url =
                            "https://santara.co.id/syarat-ketentuan-pemodal";
                        try {
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: true);
                          } else {
                            ToastHelper.showFailureToast(
                              context,
                              "Tidak dapat membuka url",
                            );
                          }
                        } catch (e, stack) {
                          ToastHelper.showFailureToast(
                            context,
                            "Tidak dapat membuka url",
                          );
                        }
                      },
                      child: Container(
                        height: Sizes.s20,
                        child: Text(
                          "Syarat & Ketentuan Pemodal",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff218196),
                            decoration: TextDecoration.underline,
                            decorationStyle: TextDecorationStyle.solid,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          checked = checked ? false : true;
                        });
                      },
                      child: Row(
                        children: [
                          Container(
                            width: Sizes.s25,
                            height: Sizes.s25,
                            child: user.trader.tnc == 1
                                ? Checkbox(
                                    value: true,
                                    onChanged: (val) {
                                      // print(val);
                                    },
                                  )
                                : Checkbox(
                                    value: checked,
                                    onChanged: (val) {
                                      setState(() {
                                        checked = val;
                                      });
                                    },
                                  ),
                          ),
                          // Icon(Icons.check_box_outline_blank),
                          SizedBox(
                            width: Sizes.s10,
                          ),
                          Flexible(
                            child: RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text:
                                        "Dengan ini saya telah membaca dan menyetujui syarat dan ketentuan yang diberlakukan oleh ",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "SANTARA",
                                    style: TextStyle(
                                        fontSize: FontSize.s12,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: Sizes.s40,
                    ),
                    user.trader.tnc == 1
                        ? SantaraPickerButton(
                            title: "Setuju",
                          )
                        : SantaraMainButton(
                            title: "Selanjutnya",
                            onPressed: () => sendData2(),
                          )
                  ],
                )
              : isLoaded == 2
                  ? Center(
                      child: Text(
                      "Tidak dapat memuat, mohon coba lagi beberapa saat lagi!",
                    ))
                  : Center(
                      child: CupertinoActivityIndicator(),
                    ),
        ),
      ),
    );
  }
}
