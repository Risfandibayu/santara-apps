import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/kyc/AlamatHelper.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Province.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/KycFieldStatus.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/pribadi/AlamatModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';

class AlamatUserBloc extends FormBloc<String, String> {
  final bool isEdit;
  final String submissionId;
  final String status;
  AlamatModel data = AlamatModel();
  bool kycAlamatLoaded = false;
  // inisialisasi service api
  final KycPersonalService _service = KycPersonalService();
  // COUNTRIES
  List<Country> allCountries = List<Country>();
  // END
  // bloc untuk country
  final countries = SelectFieldBloc<Country, FieldStatus>();
  // bloc untuk provinces
  final provinces = SelectFieldBloc<Province, FieldStatus>();
  // bloc untuk cities
  final cities = SelectFieldBloc<Regency, FieldStatus>();
  // raw field status = status defaultnya
  final rawFieldStatus = FieldStatus(
    isActive: true,
    isLoaded: true,
  );
  // bloc untuk alamat sesuai kartu identitas
  final address = TextFieldBloc();
  // bloc untuk kode pos
  final postalCode = TextFieldBloc();
  // alamat tinggal sekarang
  final currentAddress = SelectFieldBloc<SubDataBiodata, dynamic>(items: [
    SubDataBiodata(id: "1", name: "Sesuai Data Diri"),
    SubDataBiodata(id: "2", name: "Alamat Berbeda")
  ]);
  // JIKA USER MEMILIH ALAMAT TIDAK SESUAI DENGAN DATA DIRI
  final subCountries = SelectFieldBloc<Country, FieldStatus>();
  // bloc untuk provinces
  final subProvinces = SelectFieldBloc<Province, FieldStatus>();
  // bloc untuk cities
  final subCities = SelectFieldBloc<Regency, FieldStatus>();
  // bloc untuk alamat sesuai kartu identitas
  final subAddress = TextFieldBloc();
  // bloc untuk kode pos
  final subPostalCode = TextFieldBloc();

  String subProvinceName;
  String subCityName;
  // END
  // constructor
  AlamatUserBloc({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true, isEditing: false) {
    // inisialisasi default value untuk extra data ( FieldStatus )
    countries.updateExtraData(rawFieldStatus);
    provinces.updateExtraData(rawFieldStatus);
    cities.updateExtraData(rawFieldStatus);
    // menambahkan bloc tiap field
    addFieldBlocs(fieldBlocs: [
      countries,
      provinces,
      cities,
      address,
      postalCode,
      currentAddress,
    ]);
    // fetch data negara
    getCountries().then((value) async {
      // handling ketika ada perubahan data negara dari user
      await getKycAlamat().then((val) async {
        // delay untuk menghindari conflict saat bloc mengupdate value yg didapat dari api
        // contoh : namaLengkap.updateInitialValue(blablabla)
        await Future.delayed(Duration(milliseconds: 500));
        kycAlamatLoaded = true;
        // delay terlewati
        // deteksi perubahan tiap field apabila user mengubah field
        onFieldsChange();
        updateAllFields();
      });
    });
    // handling ketika ada perubahan data negara dari user
    handleCountryChanges();
    handleSubCountryChanges();

    currentAddress.onValueChanges(onData: (previous, current) async* {
      try {
        // print(">> CHANGED");
        subCountries.updateExtraData(rawFieldStatus);
        subProvinces.updateExtraData(rawFieldStatus);
        subCities.updateExtraData(rawFieldStatus);
        // print(">> Sub countries : ${subCountries.state.items.length}");
        if (current.value.id == "2") {
          addFieldBlocs(fieldBlocs: [
            subCountries,
            subProvinces,
            subCities,
            subAddress,
            subPostalCode
          ]);
          detectChanges(subCountries);
          detectChanges(subProvinces);
          detectChanges(subCities);
          detectChanges(subAddress);
          detectChanges(subPostalCode);
        } else {
          subCountries.clear();
          // subCountries.state.items.clear(); // clear item daftar negara
          subProvinces.clear();
          subProvinces.state.items.clear(); // clear item daftar provinsi
          subCities.clear();
          subCities.state.items.clear(); // clear item daftar kota
          subAddress.clear();
          subPostalCode.clear();
          removeFieldBlocs(fieldBlocs: [
            subCountries,
            subProvinces,
            subCities,
            subAddress,
            subPostalCode
          ]);
        }
      } catch (e) {
        // print(">> An Error Occured : $e");
      }
    });
  }

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("idcard_address", address);
    updateTextFieldStatus("idcard_postal_code", postalCode);
    updateTextFieldStatus("address", subAddress);
    updateTextFieldStatus("postal_code", subPostalCode);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("idcard_country", countries);
    updateFieldStatus("idcard_province", provinces);
    updateFieldStatus("idcard_regency", cities);
    updateFieldStatus("address_same_with_idcard", currentAddress);
    updateFieldStatus("country_domicile", subCountries);
    updateFieldStatus("province", subProvinces);
    updateFieldStatus("regency", subCities);
  }

  // Jika user melakukan edit
  // Ketika KYC Ditolak
  // Get alasan ditolak pada setiap field
  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "3").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                // print(element.error);
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // Jika user melakukan edit
  // Get kyc biodata pribadi data
  Future getKycAlamat() async {
    emitLoading(); // update state ke loading
    try {
      // get biodata pribadi
      await _service.getAlamatData().then((value) async {
        if (value.statusCode == 200) {
          // print(">> WADADIDAW");
          data = AlamatModel.fromJson(value.data);
          getFieldErrorStatus();
          // set default value kedalam tiap field
          // find correct country
          var userCountry = countries.state.items
              .indexWhere((element) => element.id == data.countryId);
          countries.updateInitialValue(countries.state.items[userCountry]);
          address.updateInitialValue(data.idcardAddress);
          postalCode.updateInitialValue(data.idcardPostalCode);
          currentAddress.updateInitialValue(
            currentAddress.state.items[data.addressSameWithIdcard == 1 ? 0 : 1],
          );
          if (data.addressSameWithIdcard != 1) {
            if (data.countryDomicile != null) {
              var userSubCountry = subCountries.state.items
                  .where((element) => element.id == data.countryDomicile);
              if (userSubCountry != null) {
                subCountries.updateInitialValue(userSubCountry.first);
              }
            }
            subAddress.updateInitialValue(data.address);
            subPostalCode.updateInitialValue(data.postalCode);
          }
          // currentAddress,
          // subCountries,
          // subProvinces,
          // subCities,
          // subAddress,
          // subPostalCode
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          // // print(">> RESULT : ");
          // // print(value.statusCode);
          emitFailure(
              failureResponse:
                  "SUBMITERROR Tidak dapat mengambil data biodata");
        }
      });
    } catch (e, stack) {
      // print(">> ERROCC : $e");
      // print(stack);
      emitFailure(
          failureResponse: "SUBMITERROR Tidak dapat mengambil data biodata");
    }
  }

  handleCountryChanges() {
    // ketika value negara berubah, maka update field dibawahnya
    // 1. Provinsi & Kota
    // 2. Jika != Indonesia, default value = OTHERS
    // 3. Jika == Indonesia, lanjutkan dengan fetch data provinsi
    countries.onValueChanges(
        // catch value change event
        // ignore: missing_return
        onData: (previous, current) async* {
      // jika editing
      if (isEdit && kycAlamatLoaded) {
        data.provinceName = null;
        data.regencyName = null;
      }
      // apakah Indonesia?
      if (current.value.name.toLowerCase() != "indonesia") {
        try {
          // provinces setup if country isn't indonesia
          provinces
              .clear(); // menghapus provinsi jika user sebelumnya telah mengisi provinsi
          provinces.state.items.clear(); // menghapus item provinsi
          provinces.addItem(Province(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam provinsi
          provinces.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // mengupdate value extra data
          );
          // cities setup if country isn't indonesia
          cities.clear(); // menghapus field kota jika user udah ngisi kotanya
          cities.state.items
              .clear(); // menghapus daftar kota didalam dropdownnya
          cities.addItem(Regency(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam kota
          cities.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // update value extra data
          );
        } catch (e) {
          // debug kalau ada error
          // print(e);
        }
      } else {
        // jika value == Indonesia
        provinces
            .clear(); // clear field ( mulai lagi dari awal, ada case dimana user udah ngisi semua field, lalu reset negaranya)
        provinces.state.items.clear(); // clear item
        provinces
            .updateExtraData(rawFieldStatus); // update field status to default
        provinces.updateValue(null); // clear default value
        getProvinces(provinces); // fetch province data
        cities.clear(); // clear field
        cities.state.items.clear(); // clear items
        cities.updateValue(null); // clear default value
      }
    });

    provinces.onValueChanges(onData: (previous, current) async* {
      if (current.value.id != 999) {
        if (isEdit) {
          data.provinceName = null;
          data.regencyName = null;
        }
        cities.clear();
        cities.state.items.clear();
        getCities(current.value.id, cities);
      }
    });

    provinces.listen((data) {
      if (countries.value != null &&
          countries.value.name.toLowerCase() != "indonesia") {
        provinces.updateInitialValue(provinces.state.items.first);
      }
    });

    cities.listen((data) {
      if (countries.value != null &&
          countries.value.name.toLowerCase() != "indonesia") {
        cities.updateInitialValue(cities.state.items.first);
      }
    });
  }

  handleSubCountryChanges() {
    subCountries.onValueChanges(
        // ignore: missing_return
        onData: (previous, current) async* {
      // jika editing
      if (isEdit && kycAlamatLoaded) {
        data.province = null;
        data.regency = null;
      }
      if (current.value.name.toLowerCase() != "indonesia") {
        try {
          // provinces setup if country isn't indonesia
          subProvinces
              .clear(); // menghapus provinsi jika user sebelumnya telah mengisi provinsi
          subProvinces.state.items.clear(); // menghapus item provinsi
          subProvinces.addItem(Province(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam provinsi
          subProvinces.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // mengupdate value extra data
          );
          // cities setup if country isn't indonesia
          subCities
              .clear(); // menghapus field kota jika user udah ngisi kotanya
          subCities.state.items
              .clear(); // menghapus daftar kota didalam dropdownnya
          subCities.addItem(Regency(
              id: 999,
              name: "OTHERS")); // menambahkan default value kedalam kota
          subCities.updateExtraData(
            FieldStatus(
                isActive: false, isLoaded: true), // update value extra data
          );
        } catch (e) {
          // debug kalau ada error
          // print(e);
        }
      } else {
        // jika value == Indonesia
        subProvinces
            .clear(); // clear field ( mulai lagi dari awal, ada case dimana user udah ngisi semua field, lalu reset negaranya)
        subProvinces.state.items.clear(); // clear item
        subProvinces
            .updateExtraData(rawFieldStatus); // update field status to default
        subProvinces.updateValue(null); // clear default value
        getProvinces(subProvinces); // fetch province data
        subCities.clear(); // clear field
        subCities.state.items.clear(); // clear items
        subCities.updateValue(null); // clear default value
      }
    });

    subProvinces.onValueChanges(onData: (previous, current) async* {
      if (current.value.id != 999) {
        if (isEdit) {
          data.province = null;
          data.regency = null;
        }
        subCities.clear();
        subCities.state.items.clear();
        getCities(current.value.id, subCities);
      }
    });

    subProvinces.listen((data) {
      if (subCountries.value != null &&
          subCountries.value.name.toLowerCase() != "indonesia") {
        subProvinces.updateInitialValue(subProvinces.state.items.first);
      }
    });

    subCities.listen((data) {
      if (subCountries.value != null &&
          subCountries.value.name.toLowerCase() != "indonesia") {
        subCities.updateInitialValue(subCities.state.items.first);
      }
    });
  }

  onFieldsChange() {
    detectChanges(countries);
    detectChanges(provinces);
    detectChanges(cities);
    detectChanges(address);
    detectChanges(postalCode);
    detectChanges(currentAddress);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      KycAlamatHelper.dokumen.isEdited = true;
    });
  }

  // UPDATE FIELD JIKA USER MELAKUKAN EDITING
  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "3", 1);
    });
  }

  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      _service.updateFieldStatus(submissionId, field, "3", 1);
    });
  }

  // country validator
  bool isCountryError() {
    if (countries.value == null) {
      countries.addFieldError("Negara tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  // province validator
  bool isProvinceError() {
    if (isEdit) {
      if (data.provinceName == null) {
        if (provinces.value == null) {
          provinces.addFieldError("Provinsi tidak boleh kosong!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      if (provinces.value == null) {
        provinces.addFieldError("Provinsi tidak boleh kosong!");
        return true;
      } else {
        return false;
      }
    }
  }

  // cities validator
  bool isCityError() {
    if (isEdit) {
      if (data.regencyName == null) {
        if (cities.value == null) {
          cities.addFieldError("Kota tidak boleh kosong!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      if (cities.value == null) {
        cities.addFieldError("Kota tidak boleh kosong!");
        return true;
      } else {
        return false;
      }
    }
  }

  // address validator
  bool isAddressError() {
    if (address.value == null) {
      address.addFieldError("Alamat Sesuai KTP tidak boleh kosong!");
      return true;
    } else if (address.value.length < 1) {
      address.addFieldError("Alamat Sesuai KTP tidak boleh kosong!");
      return true;
    } else if (address.value.length > 180) {
      address.addFieldError("Alamat maksimal 180 karakter!");
      return true;
    } else {
      return false;
    }
  }

  // postal code validator
  bool isPostalCodeError() {
    if (postalCode.value == null) {
      return false;
    } else if (postalCode.value.length > 5) {
      postalCode.addFieldError("Kode pos maksimal 5 karakter!");
      return true;
    } else if ((!RegExp(r"^[0-9]*$").hasMatch(postalCode.value))) {
      postalCode.addFieldError("Kode pos harus berisi angka!");
      return true;
    } else {
      return false;
    }
  }

  // current address
  bool isCurrentAddressError() {
    if (currentAddress.value == null) {
      currentAddress.addFieldError("Field ini wajib diisi!");
      return true;
    } else {
      return false;
    }
  }

  // JIKA USER MEMILIH TIDAK SESUAI DATA DIRI
  bool isSubCountryError() {
    if (currentAddress.value != null) {
      if (currentAddress.value.id == "2") {
        if (subCountries.value == null) {
          subCountries.addFieldError("Negara tidak boleh kosong!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isSubProvinceError() {
    if (currentAddress.value != null) {
      if (currentAddress.value.id == "2") {
        if (isEdit) {
          if (data.province == null) {
            if (subProvinces.value == null) {
              subProvinces.addFieldError("Provinsi tidak boleh kosong!");
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        } else {
          if (subProvinces.value == null) {
            subProvinces.addFieldError("Provinsi tidak boleh kosong!");
            return true;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isSubCityError() {
    if (currentAddress.value != null) {
      if (currentAddress.value.id == "2") {
        if (isEdit) {
          if (data.regencyName == null) {
            if (subCities.value == null) {
              subCities.addFieldError("Kota tidak boleh kosong!");
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        } else {
          if (subCities.value == null) {
            subCities.addFieldError("Kota tidak boleh kosong!");
            return true;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isSubAddressError() {
    if (currentAddress.value != null) {
      if (currentAddress.value.id == "2") {
        if (subAddress.value == null) {
          subAddress.addFieldError("Alamat tidak boleh kosong!");
          return true;
        } else if (subAddress.value.length < 1) {
          subAddress.addFieldError("Alamat tidak boleh kosong!");
          return true;
        } else if (subAddress.value.length > 180) {
          subAddress.addFieldError("Alamat maksimal 180 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isSubPostalCodeError() {
    if (currentAddress.value != null) {
      if (currentAddress.value.id == "2") {
        if (subPostalCode.value == null) {
          return false;
        } else if (subPostalCode.value.length > 5) {
          subPostalCode.addFieldError("Kode pos maksimal 5 karakter!");
          return true;
        } else if ((!RegExp(r"^[0-9]*$").hasMatch(subPostalCode.value))) {
          subPostalCode.addFieldError("Kode pos harus berisi angka!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  // END

  // API : Fetch daftar negara
  Future getCountries() async {
    // update extra data, karena saat ini sedang memuat data, maka status aktif & loaded = false
    countries.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    try {
      await _service.getCountries().then((value) {
        // jika nilai tidak null
        if (value != null) {
          // jika respons dari server OK
          if (value.statusCode == 200) {
            // embed value negara kedalam bloc countries
            value.data.forEach((val) {
              var dummy = Country.fromJson(val);
              allCountries.add(dummy);
              countries.addItem(dummy);
              subCountries.addItem(dummy);
            });
            // update extra data, akitf = true, isloaded = true, error = tidak error
            // Fix dari mas mario
            // Negara harus set ke Indonesia, jika tidak, maka gunakan updateExtraData yg dibawah
            // countries.updateExtraData(
            //   FieldStatus(isActive: true, isLoaded: true, isError: false),
            // );
            var idn = allCountries
                .where((element) => element.name.toLowerCase() == "indonesia");
            countries.updateInitialValue(idn.first);
            countries.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            // jika respons dari server 500 / error
            countries.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
          }
        } else {
          // jika ada error
          countries.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      // jika ada erro lagi
      countries.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
    }
  }

  // API : Fetch daftar provinsi
  Future getProvinces(SelectFieldBloc bloc) async {
    // print(">> GETTIN PROVINCES..");
    // mekanismenya sama dengan fetch countries
    bloc.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    // await Future.delayed(Duration(milliseconds: 2000));
    try {
      await _service.getProvinces().then((value) {
        if (value != null) {
          if (value.statusCode == 200) {
            value.data.forEach((val) {
              var dummy = Province.fromJson(val);
              bloc.addItem(dummy);
            });
            bloc.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            bloc.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: true),
            );
          }
        } else {
          bloc.updateExtraData(
            FieldStatus(isActive: true, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      bloc.updateExtraData(
        FieldStatus(isActive: true, isLoaded: true, isError: true),
      );
    }
  }

  // API : Fetch daftar kota
  Future getCities(int id, SelectFieldBloc bloc) async {
    bloc.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    // await Future.delayed(Duration(milliseconds: 2000));
    try {
      await _service.getCities(id).then((value) {
        if (value != null) {
          if (value.statusCode == 200) {
            value.data.forEach((val) {
              var dummy = Regency.fromJson(val);
              bloc.addItem(dummy);
            });
            bloc.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            bloc.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: true),
            );
          }
        } else {
          bloc.updateExtraData(
            FieldStatus(isActive: true, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      bloc.updateExtraData(
        FieldStatus(isActive: true, isLoaded: true, isError: true),
      );
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message);
    emitFailure(failureResponse: emit);
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      case "idcard_country":
        addErrorMessage(countries, _errMsg, "country");
        break;
      case "idcard_province":
        addErrorMessage(provinces, _errMsg, "province");
        break;
      case "idcard_regency":
        addErrorMessage(cities, _errMsg, "city");
        break;
      case "idcard_address":
        addErrorMessage(address, _errMsg, "address");
        break;
      case "idcard_postal_code":
        addErrorMessage(postalCode, _errMsg, "postal_code");
        break;
      case "country_domicile":
        addErrorMessage(subCountries, _errMsg, "sub_country");
        break;
      case "address_same_with_idcard":
        addErrorMessage(currentAddress, _errMsg, "current_address");
        break;
      case "country_id":
        addErrorMessage(subCountries, _errMsg, "sub_country");
        break;
      case "province":
        addErrorMessage(subProvinces, _errMsg, "sub_province");
        break;
      case "regency":
        addErrorMessage(subCities, _errMsg, "sub_city");
        break;
      case "address":
        addErrorMessage(subAddress, _errMsg, "sub_address");
        break;
      case "postal_code":
        addErrorMessage(subPostalCode, _errMsg, "sub_postal_code");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT No Validation!");
        break;
    }
  }

  @override
  void onLoading() async {
    // print(isEdit);
    // event ketika loading
    // awalnya pas fetch data negara mau taro sini,
    // terlanjur semua ditaro di constructor
    // jadi next direfactor, ( DEADLINE BOSS 😭)
    // if (!isEdit) {
    //   // print(">> NOT EDITING");
    //   emitLoaded();
    // }
  }

  @override
  void onSubmitting() async {
    try {
      if (isCountryError()) {
        // print("country");
        emitFailure(failureResponse: "country");
      } else if (isProvinceError()) {
        // print("prov");
        emitFailure(failureResponse: "province");
      } else if (isCityError()) {
        // print("cit");
        emitFailure(failureResponse: "city");
      } else if (isAddressError()) {
        // print("add");
        emitFailure(failureResponse: "address");
      } else if (isPostalCodeError()) {
        // print("pc");
        emitFailure(failureResponse: "postal_code");
      } else if (isCurrentAddressError()) {
        // print("ca");
        emitFailure(failureResponse: "current_address");
      } else if (isSubCountryError()) {
        // print("sc");
        emitFailure(failureResponse: "sub_country");
      } else if (isSubProvinceError()) {
        // print("sp");
        emitFailure(failureResponse: "sub_province");
      } else if (isSubCityError()) {
        // print("sc");
        emitFailure(failureResponse: "sub_city");
      } else if (isSubAddressError()) {
        // print("sa");
        emitFailure(failureResponse: "sub_address");
      } else if (isSubPostalCodeError()) {
        // print("spc");
        emitFailure(failureResponse: "sub_postal_code");
      } else {
        // print("qwerty");
        try {
          if (isEdit) {
            AlamatUser userAlamat = AlamatUser(
              idCardCountry: "${countries.value.id ?? ''}",
              idCardProvince: provinces.value == null
                  ? data.provinceId.toString()
                  : "${provinces.value.id ?? ''}",
              idCardRegency: cities.value == null
                  ? data.regencyId.toString()
                  : "${cities.value.id ?? ''}",
              idCardAddress: "${address.value ?? ''}",
              idCardPostalCode: "${postalCode.value ?? ''}",
              // sameAddressWithIdCard:
              //     currentAddress.value.id == "1" ? true : false,
              sameAddressWithIdCard:
                  currentAddress.value != null ? currentAddress.value.id : "0",
              country: currentAddress.value.id == "2"
                  ? "${subCountries.value.id ?? ''}"
                  : "",
              province: currentAddress.value.id == "2"
                  ? subProvinces.value == null
                      ? data.province
                      : "${subProvinces.value.name ?? ''}"
                  : "",
              regency: currentAddress.value.id == "2"
                  ? subCities.value == null
                      ? data.regency
                      : "${subCities.value.name ?? ''}"
                  : "",
              address: currentAddress.value.id == "2"
                  ? "${subAddress.value ?? ''}"
                  : "",
              postalCode: currentAddress.value.id == "2"
                  ? "${subPostalCode.value ?? ''}"
                  : "",
            );
            await _service.updateAlamat(userAlamat).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess();
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else {
                  emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.statusCode}");
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          } else {
            AlamatUser data = AlamatUser(
              idCardCountry: "${countries.value.id ?? ''}",
              idCardProvince: "${provinces.value.id ?? ''}",
              idCardRegency: "${cities.value.id ?? ''}",
              idCardAddress: "${address.value ?? ''}",
              idCardPostalCode: "${postalCode.value ?? ''}",
              sameAddressWithIdCard:
                  currentAddress.value != null ? currentAddress.value.id : "0",
              country: currentAddress.value.id == "2"
                  ? "${subCountries.value.id ?? ''}"
                  : "",
              province: currentAddress.value.id == "2"
                  ? "${subProvinces.value.name ?? ''}"
                  : "",
              regency: currentAddress.value.id == "2"
                  ? "${subCities.value.name ?? ''}"
                  : "",
              address: currentAddress.value.id == "2"
                  ? "${subAddress.value ?? ''}"
                  : "",
              postalCode: currentAddress.value.id == "2"
                  ? "${subPostalCode.value ?? ''}"
                  : "",
            );
            await _service.sendAlamat(data).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess();
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else {
                  emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.statusCode}");
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          }
        } catch (e, stack) {
          // print(">> ERR : ");
          // print(e);
          // print(">> STACK : ");
          // print(stack);
          emitFailure();
        }
      }
    } catch (e, stack) {
      // print(">> ERROR : ");
      // print(stack);
      emitFailure(failureResponse: "ERRORSUBMIT $e");
    }
  }
}
