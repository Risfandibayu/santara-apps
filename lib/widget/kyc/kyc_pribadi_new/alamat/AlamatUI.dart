import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/AlamatHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/Country.dart';
import '../../../../models/Province.dart';
import '../../../../models/Regency.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/listing/SantaraNotification.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/alamat.user.bloc.dart';

class AlamatUI extends StatelessWidget {
  final bool isEdit;
  final String submissionId;
  final String status;
  AlamatUI({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycAlamatHelper.dokumen.isEdited != null) {
          if (KycAlamatHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycAlamatHelper.dokumen = AlamatUser();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              return false;
            });
            return false;
          } else {
            KycAlamatHelper.dokumen = AlamatUser();
            return true;
          }
        } else {
          KycAlamatHelper.dokumen = AlamatUser();
          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Alamat",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "3/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => AlamatUserBloc(
              isEdit: isEdit,
              submissionId: submissionId,
              status: status,
            ),
            child: AlamatUserForm(),
          )),
    );
  }
}

class AlamatUserForm extends StatefulWidget {
  @override
  _AlamatUserFormState createState() => _AlamatUserFormState();
}

class _AlamatUserFormState extends State<AlamatUserForm> {
  AlamatUserBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    KycAlamatHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<AlamatUserBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  _reload(VoidCallback onPressed) {
    return FlatButton(
      onPressed: onPressed,
      shape: Border.all(
        width: 1,
        color: Colors.grey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.replay,
            size: FontSize.s15,
          ),
          Container(
            width: Sizes.s10,
          ),
          Text(
            "Muat Ulang",
            style: TextStyle(
              fontSize: FontSize.s15,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<AlamatUserBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          if (state.successResponse != null)
            ToastHelper.showBasicToast(context, state.successResponse);
          Navigator.pop(context);
          KycAlamatHelper.dokumen = AlamatUser();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (KycAlamatHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }
          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }
          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse.toLowerCase()) {
              case "country":
                fieldWrapper.scrollTo(1);
                break;
              case "province":
                fieldWrapper.scrollTo(2);
                break;
              case "city":
                fieldWrapper.scrollTo(3);
                break;
              case "address":
                fieldWrapper.scrollTo(4);
                break;
              case "postal_code":
                fieldWrapper.scrollTo(5);
                break;
              case "current_address":
                fieldWrapper.scrollTo(6);
                break;
              case "sub_country":
                fieldWrapper.scrollTo(7);
                break;
              case "sub_province":
                fieldWrapper.scrollTo(8);
                break;
              case "sub_city":
                fieldWrapper.scrollTo(9);
                break;
              case "sub_address":
                fieldWrapper.scrollTo(10);
                break;
              case "sub_postal_code":
                fieldWrapper.scrollTo(11);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<AlamatUserBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                padding: EdgeInsets.all(Sizes.s20),
                child: ListView(
                  scrollDirection: scrollDirection,
                  controller: controller,
                  children: [
                    Text(
                      "Alamat Sesuai Dokumen Data Diri",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    // Negara
                    fieldWrapper.viewWidget(
                      1,
                      Sizes.s100,
                      BlocBuilder<SelectFieldBloc, FieldBlocState>(
                        bloc: bloc.countries,
                        builder: (context, state) {
                          return state.extraData.isError
                              ? _reload(() {
                                  bloc.getCountries();
                                })
                              : DropdownFieldBlocBuilder<Country>(
                                  selectFieldBloc: bloc.countries,
                                  isEnabled: KycHelpers.isVerified(bloc.status)
                                      ? state.extraData.isActive ?? true
                                      : false,
                                  decoration: InputDecoration(
                                    errorMaxLines: 5,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.grey),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    labelText: "Negara",
                                    suffixIcon: state.extraData.isLoaded
                                        ? Icon(Icons.arrow_drop_down)
                                        : CupertinoActivityIndicator(),
                                  ),
                                  itemBuilder: (context, value) =>
                                      value != null ? value.name : "",
                                );
                        },
                      ),
                    ),
                    // Provinsi
                    fieldWrapper.viewWidget(
                      2,
                      Sizes.s100,
                      BlocBuilder<SelectFieldBloc, FieldBlocState>(
                        bloc: bloc.provinces,
                        builder: (context, state) {
                          return state.extraData.isError
                              ? _reload(() {
                                  bloc.getProvinces(bloc.provinces);
                                })
                              : DropdownFieldBlocBuilder<Province>(
                                  selectFieldBloc: bloc.provinces,
                                  isEnabled: KycHelpers.isVerified(bloc.status)
                                      ? KycHelpers.fieldCheck(bloc.status)
                                          ? state.extraData.isActive ?? true
                                          : false
                                      : false,
                                  decoration: InputDecoration(
                                    errorMaxLines: 5,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.grey),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    labelText: "Provinsi",
                                    hintText: bloc.isEdit
                                        ? bloc.data.provinceName
                                        : "Aceh",
                                    hintStyle: TextStyle(color: Colors.grey),
                                    suffixIcon: state.extraData.isLoaded
                                        ? Icon(Icons.arrow_drop_down)
                                        : CupertinoActivityIndicator(),
                                  ),
                                  itemBuilder: (context, value) =>
                                      value != null ? value.name : "",
                                );
                        },
                      ),
                    ),
                    // Kota
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s100,
                      BlocBuilder<SelectFieldBloc, FieldBlocState>(
                        bloc: bloc.cities,
                        builder: (context, state) {
                          return state.extraData.isError
                              ? _reload(() {
                                  bloc.getCities(
                                    bloc.provinces.value.id,
                                    bloc.cities,
                                  );
                                })
                              : DropdownFieldBlocBuilder<Regency>(
                                  selectFieldBloc: bloc.cities,
                                  isEnabled: KycHelpers.isVerified(bloc.status)
                                      ? KycHelpers.fieldCheck(bloc.status)
                                          ? state.extraData.isActive ?? true
                                          : false
                                      : false,
                                  decoration: InputDecoration(
                                    errorMaxLines: 5,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.grey),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    labelText: "Kota",
                                    hintText: bloc.isEdit
                                        ? bloc.data.regencyName
                                        : "Banda Aceh",
                                    hintStyle: TextStyle(color: Colors.grey),
                                    suffixIcon: state.extraData.isLoaded
                                        ? Icon(Icons.arrow_drop_down)
                                        : CupertinoActivityIndicator(),
                                  ),
                                  itemBuilder: (context, value) =>
                                      value != null ? value.name : "",
                                );
                        },
                      ),
                    ),
                    // alamat lengkap sesuai ktp
                    fieldWrapper.viewWidget(
                      4,
                      Sizes.s120,
                      TextFieldBlocBuilder(
                        textFieldBloc: bloc.address,
                        minLines: 2,
                        maxLines: 2,
                        isEnabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                        decoration: InputDecoration(
                          errorMaxLines: 5,
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          hintText:
                              "Contoh : Perumahan Sidoarum Blok E-14, Yogyakarta",
                          labelText: "Alamat Sesuai KTP",
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                        ),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    // kode pos
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.postalCode,
                        enabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                        hintText: "*Kosongkan jika tidak ada (Optional)",
                        labelText: "Kode Pos",
                        inputType: TextInputType.number,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(5),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    Text(
                      "Alamat Tinggal Sekarang",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s16,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: Sizes.s5),
                      color: Color(0xffF4F4F4),
                      height: 3,
                      width: double.maxFinite,
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    // alamat tinggal sesuai kah ?
                    fieldWrapper.viewWidget(
                      6,
                      Sizes.s130,
                      SantaraBlocRadioGroup(
                        selectFieldBloc: bloc.currentAddress,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    BlocBuilder<SelectFieldBloc, FieldBlocState>(
                        bloc: bloc.currentAddress,
                        builder: (context, state) {
                          if (state.hasValue) {
                            if (state.value.id == "1") {
                              return SantaraNotification(
                                  margin: EdgeInsets.zero,
                                  message: Text(
                                    "Alamat yang telampir akan sama dengan Alamat KTP diatas.",
                                    style: TextStyle(
                                      fontSize: FontSize.s11,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  backgroundColor: Color(0xffEEF1FF));
                            } else {
                              return Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  // sub countries
                                  fieldWrapper.viewWidget(
                                    7,
                                    Sizes.s100,
                                    BlocBuilder<SelectFieldBloc,
                                        FieldBlocState>(
                                      bloc: bloc.subCountries,
                                      builder: (context, state) {
                                        return state.extraData.isError
                                            ? _reload(() {
                                                bloc.getCountries();
                                              })
                                            : DropdownFieldBlocBuilder<Country>(
                                                selectFieldBloc:
                                                    bloc.subCountries,
                                                isEnabled:
                                                    KycHelpers.fieldCheck(
                                                            bloc.status)
                                                        ? state.extraData
                                                                .isActive ??
                                                            true
                                                        : false,
                                                decoration: InputDecoration(
                                                  errorMaxLines: 5,
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          width: 1,
                                                          color: Colors.grey),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  labelText: "Negara",
                                                  hintText: "Indonesia",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey),
                                                  suffixIcon: state
                                                          .extraData.isLoaded
                                                      ? Icon(
                                                          Icons.arrow_drop_down)
                                                      : CupertinoActivityIndicator(),
                                                ),
                                                itemBuilder: (context, value) =>
                                                    value != null
                                                        ? value.name
                                                        : "",
                                              );
                                      },
                                    ),
                                  ),
                                  // Sub Provinsi
                                  fieldWrapper.viewWidget(
                                    8,
                                    Sizes.s100,
                                    BlocBuilder<SelectFieldBloc,
                                        FieldBlocState>(
                                      bloc: bloc.subProvinces,
                                      builder: (context, state) {
                                        return state.extraData.isError
                                            ? _reload(() {
                                                bloc.getProvinces(
                                                    bloc.subProvinces);
                                              })
                                            : DropdownFieldBlocBuilder<
                                                Province>(
                                                selectFieldBloc:
                                                    bloc.subProvinces,
                                                isEnabled:
                                                    KycHelpers.fieldCheck(
                                                            bloc.status)
                                                        ? state.extraData
                                                                .isActive ??
                                                            true
                                                        : false,
                                                decoration: InputDecoration(
                                                  errorMaxLines: 5,
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          width: 1,
                                                          color: Colors.grey),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  labelText: "Provinsi",
                                                  hintText: bloc.isEdit
                                                      ? bloc.data.province
                                                      : "Aceh",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey),
                                                  suffixIcon: state
                                                          .extraData.isLoaded
                                                      ? Icon(
                                                          Icons.arrow_drop_down)
                                                      : CupertinoActivityIndicator(),
                                                ),
                                                itemBuilder: (context, value) =>
                                                    value != null
                                                        ? value.name
                                                        : "",
                                              );
                                      },
                                    ),
                                  ),
                                  // Sub Kota
                                  fieldWrapper.viewWidget(
                                    9,
                                    Sizes.s100,
                                    BlocBuilder<SelectFieldBloc,
                                        FieldBlocState>(
                                      bloc: bloc.subCities,
                                      builder: (context, state) {
                                        return state.extraData.isError
                                            ? _reload(() {
                                                bloc.getCities(
                                                  bloc.subProvinces.value.id,
                                                  bloc.subCities,
                                                );
                                              })
                                            : DropdownFieldBlocBuilder<Regency>(
                                                selectFieldBloc: bloc.subCities,
                                                isEnabled:
                                                    KycHelpers.fieldCheck(
                                                            bloc.status)
                                                        ? state.extraData
                                                                .isActive ??
                                                            true
                                                        : false,
                                                decoration: InputDecoration(
                                                  errorMaxLines: 5,
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          width: 1,
                                                          color: Colors.grey),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  labelText: "Kota",
                                                  hintText: bloc.isEdit
                                                      ? bloc.data.regency
                                                      : "Banda Aceh",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey),
                                                  suffixIcon: state
                                                          .extraData.isLoaded
                                                      ? Icon(
                                                          Icons.arrow_drop_down)
                                                      : CupertinoActivityIndicator(),
                                                ),
                                                itemBuilder: (context, value) =>
                                                    value != null
                                                        ? value.name
                                                        : "",
                                              );
                                      },
                                    ),
                                  ),
                                  // alamat lengkap sesuai ktp
                                  fieldWrapper.viewWidget(
                                    10,
                                    Sizes.s120,
                                    TextFieldBlocBuilder(
                                      textFieldBloc: bloc.subAddress,
                                      isEnabled:
                                          KycHelpers.fieldCheck(bloc.status),
                                      minLines: 2,
                                      maxLines: 2,
                                      decoration: InputDecoration(
                                        errorMaxLines: 5,
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 1, color: Colors.grey),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        hintText:
                                            "Contoh : Perumahan Sidoarum Blok E-14, Yogyakarta",
                                        labelText: "Alamat Tinggal Sekarang",
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                      ),
                                      keyboardType: TextInputType.text,
                                    ),
                                  ),
                                  // postal code
                                  fieldWrapper.viewWidget(
                                    11,
                                    Sizes.s100,
                                    SantaraBlocTextField(
                                      textFieldBloc: bloc.subPostalCode,
                                      enabled:
                                          KycHelpers.fieldCheck(bloc.status),
                                      hintText:
                                          "*Kosongkan jika tidak ada (Optional)",
                                      labelText: "Kode Pos",
                                      inputType: TextInputType.number,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(5),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            }
                          } else {
                            return Container();
                          }
                        }),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              if (bloc.status == "verified") {
                                PopupHelper.showConfirmationDataChange(context,
                                    () {
                                  Navigator.pop(context);
                                  KycAlamatHelper.dokumen.isSubmitted = true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                });
                              } else {
                                KycAlamatHelper.dokumen.isSubmitted = true;
                                FocusScope.of(context).unfocus();
                                bloc.submit();
                              }
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
