import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/kyc/BiodataKeluargaHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/pribadi/BiodataKeluargaModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';

class BiodataKeluargaBloc extends FormBloc<String, String> {
  //
  final bool isEdit; // nandain apakah state edit
  final String submissionId;
  final String status;
  //

  static var rawMaritalStatus = [
    SubDataBiodata(id: "1", name: "Single"),
    SubDataBiodata(id: "2", name: "Menikah"),
    SubDataBiodata(id: "3", name: "Duda"),
    SubDataBiodata(id: "4", name: "Janda"),
  ];
  final KycPersonalService _service = KycPersonalService();

  final maritalStatus = SelectFieldBloc(items: rawMaritalStatus);
  final spouseName = TextFieldBloc();
  final motherMaidenName = TextFieldBloc();
  final heir = TextFieldBloc();
  final heirRelation = TextFieldBloc();
  final heirPhone = TextFieldBloc();

  BiodataKeluargaBloc({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      maritalStatus,
      spouseName,
      motherMaidenName,
      heir,
      heirRelation,
      heirPhone,
    ]);
    getBiodataKeluarga().then((val) async {
      // delay untuk menghindari conflict saat bloc mengupdate value yg didapat dari api
      // contoh : namaLengkap.updateInitialValue(blablabla)
      await Future.delayed(Duration(milliseconds: 500));
      // delay terlewati
      // deteksi perubahan tiap field apabila user mengubah field
      onFieldsChange();
      updateAllFields();
    });
  }

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("spouse_name", spouseName);
    updateTextFieldStatus("mother_maiden_name", motherMaidenName);
    updateTextFieldStatus("heir", heir);
    updateTextFieldStatus("heir_relation", heirRelation);
    updateTextFieldStatus("heir_phone", heirPhone);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("marital_status", maritalStatus);
  }

  Future getBiodataKeluarga() async {
    emitLoading(); // update state ke loading
    try {
      await _service.getBiodataKeluargaData().then((value) {
        if (value.statusCode == 200) {
          BiodataKeluargaModel data = BiodataKeluargaModel.fromJson(value.data);
          maritalStatus.updateInitialValue(
              maritalStatus.state.items[int.parse(data.maritalStatus) - 1]);
          spouseName.updateInitialValue(data.spouseName);
          motherMaidenName.updateInitialValue(data.motherMaidenName);
          heir.updateInitialValue(data.heir);
          heirRelation.updateInitialValue(data.heirRelation);
          heirPhone.updateInitialValue(data.heirPhone.substring(0, 2) == "62" ? data.heirPhone.substring(2, data.heirPhone.length) : data.heirPhone);
          getFieldErrorStatus();
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse:
                  "SUBMITERROR Tidak dapat mengambil data biodata");
        }
      });
    } catch (e, stack) {
      // print(e);
      // print(stack);
      emitFailure(failureResponse: "SUBMITERROR Tidak Dapat Mengirim Data!");
    }
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "2").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  onFieldsChange() {
    detectChanges(maritalStatus);
    detectChanges(spouseName);
    detectChanges(motherMaidenName);
    detectChanges(heir);
    detectChanges(heirRelation);
    detectChanges(heirPhone);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      KycBioKeluargaHelper.dokumen.isEdited = true;
    });
  }

  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "2", 1);
    });
  }

  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      _service.updateFieldStatus(submissionId, field, "2", 1);
    });
  }

  bool isMaritalError() {
    if (maritalStatus.value == null) {
      maritalStatus.addFieldError("Status pernikahan tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isSpouseNameError() {
    if (maritalStatus.value != null) {
      if (maritalStatus.value.id == "2") {
        if (spouseName.value.length < 1) {
          spouseName.addFieldError("Nama lengkap pasangan tidak boleh kosong");
          return true;
        } else if (spouseName.value.length > 100) {
          spouseName.addFieldError(
            "Nama lengkap pasangan maksimal 100 karakter",
          );
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isMotherMaidenNameError() {
    if (motherMaidenName.value == null) {
      motherMaidenName.addFieldError(
        " Nama Gadis Ibu Kandung tidak boleh kosong",
      );
      return true;
    } else if (motherMaidenName.value.length < 1) {
      motherMaidenName.addFieldError(
        " Nama Gadis Ibu Kandung tidak boleh kosong",
      );
      return true;
    } else if (motherMaidenName.value.length > 100) {
      motherMaidenName.addFieldError(
        " Nama Gadis Ibu Kandung maksimal 100 karakter",
      );
      return true;
    } else {
      return false;
    }
  }

  bool isHeirError() {
    if (heir.value != null) {
      if (heir.value.length > 120) {
        heir.addFieldError("Nama Lengkap Ahli Waris maksimal 120 karakter");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isHeirRelationError() {
    if (heirRelation.value != null) {
      if (heirRelation.value.length > 120) {
        heirRelation.addFieldError(
          "Hubungan Dengan Ahli Waris maksimal 120 karakter",
        );
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isHeirPhoneError() {
    if (heirPhone.value != null && heirPhone.value.length > 0) {
      if (heirPhone.value[0] == "0") {
        heirPhone.addFieldError("Nomor telepon tidak boleh diawali dengan 0");
        return true;
      } else if (heirPhone.value.length > 20) {
        heirPhone.addFieldError("Nomor telepon maksimal 20 karakter");
        return true;
      } else if (!RegExp(r"^[0-9+#-]*$").hasMatch(heirPhone.value)) {
        heirPhone.addFieldError("Nomor telepon tidak boleh berisi huruf!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message);
    emitFailure(failureResponse: emit);
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      case "marital_status":
        addErrorMessage(maritalStatus, _errMsg, "marital_status");
        break;
      case "spouse_name":
        addErrorMessage(spouseName, _errMsg, "spouse_name");
        break;
      case "mother_maiden_name":
        addErrorMessage(motherMaidenName, _errMsg, "mother_maiden_name");
        break;
      case "heir":
        addErrorMessage(heir, _errMsg, "heir");
        break;
      case "heir_relation":
        addErrorMessage(heirRelation, _errMsg, "heir_relation");
        break;
      case "heir_phone":
        addErrorMessage(heirPhone, _errMsg, "heir_phone");
        break;
      default:
        if (_errMsg.length < 1) {
          _errMsg = "Form tidak valid!";
        }
        emitFailure(failureResponse: "ERRORSUBMIT $_errMsg");
        break;
    }
  }

  @override
  void onLoading() async {
    // if (!isEdit) {
    //   emitLoaded();
    // }
    // try {
    //   // await Future<void>.delayed(Duration(milliseconds: 1500));
    //   emitLoaded();
    // } catch (e) {
    //   emitLoadFailed();
    // }
  }

  @override
  void onSubmitting() async {
    try {
      // await Future.delayed(Duration(milliseconds: 1500));
      if (isMaritalError()) {
        emitFailure(failureResponse: "marital_status");
      } else if (isSpouseNameError()) {
        emitFailure(failureResponse: "spouse_name");
      } else if (isMotherMaidenNameError()) {
        emitFailure(failureResponse: "mother_maiden_name");
      } else if (isHeirError()) {
        emitFailure(failureResponse: "heir");
      } else if (isHeirRelationError()) {
        emitFailure(failureResponse: "heir_relation");
      } else if (isHeirPhoneError()) {
        emitFailure(failureResponse: "heir_phone");
      } else {
        BiodataKeluarga data = BiodataKeluarga(
          maritalStatus: "${maritalStatus?.value?.id ?? ''}",
          spouseName: spouseName?.value ?? "",
          motherMaidenName: motherMaidenName?.value ?? "",
          heir: heir?.value ?? "",
          heriRelation: heirRelation?.value ?? "",
          heirPhone: '62${heirPhone?.value}' ?? "",
        );
        if (isEdit) {
          await _service.updateBiodataKeluarga(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                emitSuccess();
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else {
                emitFailure(
                    failureResponse:
                        "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        } else {
          await _service.sendBiodataKeluarga(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                emitSuccess();
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else {
                // print(">> Here u are");
                emitFailure(
                    failureResponse: "ERRORSUBMIT Tidak dapat mengirim data!");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        }
      }
    } catch (e, stack) {
      // print(e);
      // print(stack);
      var err = "$e".split("\n");
      emitFailure(
        failureResponse:
            "ERRORSUBMIT ${err[0]} Terjadi kesalahan saat mengirim data",
      );
    }
  }
}
