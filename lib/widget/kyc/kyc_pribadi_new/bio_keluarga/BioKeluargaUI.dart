import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/BiodataKeluargaHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/biodata.keluarga.bloc.dart';

class BiodataKeluargaUI extends StatelessWidget {
  final bool isEdit;
  final String submissionId;
  final String status;
  BiodataKeluargaUI({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycBioKeluargaHelper.dokumen.isEdited != null) {
          if (KycBioKeluargaHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycBioKeluargaHelper.dokumen = BiodataKeluarga();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              KycBioKeluargaHelper.dokumen = BiodataKeluarga();
              return false;
            });
            return false;
          } else {
            KycBioKeluargaHelper.dokumen = BiodataKeluarga();
            return true;
          }
        } else {
          KycBioKeluargaHelper.dokumen = BiodataKeluarga();
          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Biodata Keluarga",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "2/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => BiodataKeluargaBloc(
              isEdit: isEdit,
              submissionId: submissionId,
              status: status,
            ),
            child: BiodataKeluargaForm(),
          )),
    );
  }
}

class BiodataKeluargaForm extends StatefulWidget {
  @override
  _BiodataKeluargaFormState createState() => _BiodataKeluargaFormState();
}

class _BiodataKeluargaFormState extends State<BiodataKeluargaForm> {
  BiodataKeluargaBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  TextEditingController ctrl = TextEditingController();
  KycFieldWrapper fieldWrapper;

  @override
  void initState() {
    super.initState();
    KycBioKeluargaHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<BiodataKeluargaBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return KycHideKeyboard(
      child: FormBlocListener<BiodataKeluargaBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(
            context,
            "Berhasil menyimpan biodata keluarga!",
          );
          Navigator.pop(context);
          KycBioKeluargaHelper.dokumen = BiodataKeluarga();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (KycBioKeluargaHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
            KycBioKeluargaHelper.dokumen = BiodataKeluarga();
          }

          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }

          if (state.failureResponse != null &&
              state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse) {
              // CASE = identifier dari emitFailure, tiap value menandakan error pada field sesuai casenya
              // fieldWrapper.scrollTo(x), x = identifier / key widget yang ada di fieldWrapper.viewWidget
              case "marital_status":
                fieldWrapper.scrollTo(1);
                break;
              case "spouse_name":
                fieldWrapper.scrollTo(2);
                break;
              case "mother_maiden_name":
                fieldWrapper.scrollTo(3);
                break;
              case "heir":
                fieldWrapper.scrollTo(4);
                break;
              case "heir_relation":
                fieldWrapper.scrollTo(5);
                break;
              case "heir_phone":
                fieldWrapper.scrollTo(6);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<BiodataKeluargaBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                padding: EdgeInsets.all(Sizes.s20),
                child: ListView(
                  scrollDirection: scrollDirection,
                  controller: controller,
                  children: [
                    fieldWrapper.viewWidget(
                      1,
                      Sizes.s100,
                      SantaraBlocDropDown(
                        selectFieldBloc: bloc.maritalStatus,
                        label: "Status Pernikahan",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        hintText: "Single",
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      2,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.spouseName,
                        hintText: "Contoh : Putri Ashriyah",
                        labelText: "Nama Lengkap Pasangan",
                        inputType: TextInputType.text,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                            RegExp(r"[a-zA-Z\d\-_\s]*$"),
                          )
                        ],
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      3,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.motherMaidenName,
                        hintText: "Contoh : Putri Ashriyah",
                        labelText: "Nama Gadis Ibu Kandung",
                        inputType: TextInputType.text,
                        enabled: KycHelpers.isVerified(bloc.status)
                            ? KycHelpers.fieldCheck(bloc.status)
                            : false,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                            RegExp(r"[a-zA-Z\d\-_\s]*$"),
                          )
                        ],
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      4,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.heir,
                        hintText: "Contoh : Putri Ashriyah",
                        labelText: "Nama Lengkap Ahli Waris",
                        inputType: TextInputType.text,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                            RegExp(r"[a-zA-Z\d\-_\s]*$"),
                          )
                        ],
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      5,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.heirRelation,
                        hintText: "Contoh : Ayah dan anak",
                        labelText: "Hubungan Dengan Ahli Waris",
                        enabled: KycHelpers.fieldCheck(bloc.status),
                      ),
                    ),
                    fieldWrapper.viewWidget(
                      6,
                      Sizes.s100,
                      SantaraBlocTextField(
                        textFieldBloc: bloc.heirPhone,
                        isPhone: true,
                        hintText: "Contoh : 85292550564",
                        labelText: "No Handphone Ahli Waris",
                        inputType: TextInputType.phone,
                        enabled: KycHelpers.fieldCheck(bloc.status),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                            RegExp(r"[0-9+#-]*$"),
                          )
                        ],
                      ),
                    ),
                    fieldWrapper.divider,
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              if (bloc.status == "verified") {
                                PopupHelper.showConfirmationDataChange(context,
                                    () {
                                  Navigator.pop(context);
                                  KycBioKeluargaHelper.dokumen.isSubmitted =
                                      true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                });
                              } else {
                                KycBioKeluargaHelper.dokumen.isSubmitted = true;
                                FocusScope.of(context).unfocus();
                                bloc.submit();
                              }
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
