import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/Camera.dart';
import 'camera/DataDiriPreview.dart';

enum CamType { ktp, selfie }

class PreTakePhotoUI extends StatelessWidget {
  final CamType type;
  PreTakePhotoUI({@required this.type});

  Widget _ktpBuilder(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "assets/icon/ktp_benar.png",
          width: Sizes.s100,
        ),
        SizedBox(
          height: Sizes.s30,
        ),
        Text(
          "Unggah Foto Data Diri",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: FontSize.s20,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: Sizes.s30,
        ),
        Text(
          "Unggah foto KTP/Passport Anda untuk mevalidasi\ndata diri Anda. Syarat dan Ketentuan Foto :",
          style: TextStyle(fontSize: FontSize.s14),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: Sizes.s10,
        ),
        Text(
          "1. Foto bewarna, terbaca jelas (tidak blur), dan tidak boleh terpotong",
          style: TextStyle(
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(
          height: Sizes.s10,
        ),
        Text(
          "2. Menggunakan KTP asli (bukan fotocopy dan milik pribadi",
          style: TextStyle(
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(
          height: Sizes.s40,
        ),
        Container(
          height: Sizes.s40,
          child: SantaraPickerButton(
            title: "Unggah Foto",
            onPressed: () async {
              var cameras = await availableCameras();
              if (cameras.length == 0) {
                await ImagePicker.pickImage(source: ImageSource.camera)
                    .then((image) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DataDiriPreviewUI(
                        ktp: image,
                      ),
                    ),
                  );
                });
              } else {
                try {
                  final File camera = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => Camera(isKyc: 2, isSelfie: 0),
                    ),
                  );
                  if (camera != null) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DataDiriPreviewUI(
                          ktp: camera,
                        ),
                      ),
                    );
                  }
                } catch (e, stack) {
                  ToastHelper.showFailureToast(context, e);
                }
              }
            },
          ),
        )
      ],
    );
  }

  Widget _selfieBuilder(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "assets/icon/selfie_benar.png",
          width: Sizes.s100,
        ),
        SizedBox(
          height: Sizes.s30,
        ),
        Text(
          "Unggah Foto Selfie\nDengan Data Diri",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: FontSize.s20,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: Sizes.s30,
        ),
        Text(
          "Unggah foto selfie dengan KTP/Passport\nuntuk mevalidasi data diri Anda. Syarat dan\nKetentuan Foto :",
          style: TextStyle(fontSize: FontSize.s14),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: Sizes.s10,
        ),
        Text(
          "1. Foto bewarna, terbaca jelas (tidak blur), dan tidak  boleh terpotong",
          style: TextStyle(
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(
          height: Sizes.s10,
        ),
        Text(
          "2. Menggunakan KTP asli (bukan fotocopy dan milik pribadi) ",
          style: TextStyle(
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(
          height: Sizes.s10,
        ),
        Text(
          "3.Muka tampak jelas tidak tertutup objek apapun (termasuk kacamata)",
          style: TextStyle(
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(
          height: Sizes.s40,
        ),
        Container(
          height: Sizes.s40,
          child: SantaraPickerButton(
            title: "Unggah Foto",
            onPressed: () async {
              var cameras = await availableCameras();
              if (cameras.length == 0) {
                await ImagePicker.pickImage(source: ImageSource.camera)
                    .then((image) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DataDiriPreviewUI(
                        selfie: image,
                      ),
                    ),
                  );
                });
              } else {
                try {
                  final File camera = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => Camera(isKyc: 2, isSelfie: 1),
                    ),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DataDiriPreviewUI(
                        selfie: camera,
                      ),
                    ),
                  );
                } catch (e, stack) {
                  ToastHelper.showFailureToast(context, e);
                }
              }
            },
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.only(left: Sizes.s40, right: Sizes.s40),
        child: type == CamType.ktp
            ? _ktpBuilder(context)
            : _selfieBuilder(context),
      ),
    );
  }
}
