import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/helpers/ImageProcessor.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/helpers/kyc/BiodataPribadiHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/SubData.dart';
import 'package:santaraapp/models/kyc/KycDataDokumen.dart';
import 'package:santaraapp/models/kyc/pribadi/BiodataPribadiModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/utils/logger.dart';

class BioPribadiBloc extends FormBloc<String, String> {
  final bool isEdit; // nandain apakah state sedang edit ( jika kyc ditolak )
  final String submissionId; // submission id
  final String status; // status kyc

  final storage = secureStorage.FlutterSecureStorage(); // storage
  // service api yg terkait kyc personal
  final KycPersonalService _service = KycPersonalService();
  // get user data
  User userData = User();
  // value bulan untuk dropdown
  static var rawMonths = [
    SubDataBiodata(id: "01", name: "Januari"),
    SubDataBiodata(id: "02", name: "Februari"),
    SubDataBiodata(id: "03", name: "Maret"),
    SubDataBiodata(id: "04", name: "April"),
    SubDataBiodata(id: "05", name: "Mei"),
    SubDataBiodata(id: "06", name: "Juni"),
    SubDataBiodata(id: "07", name: "Juli"),
    SubDataBiodata(id: "08", name: "Agustus"),
    SubDataBiodata(id: "09", name: "September"),
    SubDataBiodata(id: "10", name: "Oktober"),
    SubDataBiodata(id: "11", name: "November"),
    SubDataBiodata(id: "12", name: "Desember"),
  ];
  // value pendidikan untuk dropdown
  static var rawPendidikan = [
    SubDataBiodata(id: "1", name: "Lainnya"),
    SubDataBiodata(id: "2", name: "SD"),
    SubDataBiodata(id: "3", name: "SMP"),
    SubDataBiodata(id: "4", name: "SMA / SMK / D1 / D2"),
    SubDataBiodata(id: "5", name: "D3 / Akademi"),
    SubDataBiodata(id: "6", name: "S1"),
    SubDataBiodata(id: "7", name: "S2"),
    SubDataBiodata(id: "8", name: "S3"),
  ];
  // values jenis kelamin
  static var rawJenisKelamin = [
    SubDataBiodata(id: "M", name: "Pria"),
    SubDataBiodata(id: "F", name: "Wanita"),
  ];
  // raw ktp valid until
  static var rawKtpValidUntil = [
    SubDataBiodata(id: "1", name: "Seumur Hidup"),
    SubDataBiodata(id: "2", name: "Berlaku Hingga"),
  ];
  // field nama lengkap
  final namaLengkap = TextFieldBloc(
    name: 'name',
    // validators: [FieldBlocValidators.required],
  );
  // tanggal lahir
  final tglLahir = TextFieldBloc();
  // field tanggal lahir ( field tanggal )
  final dayLahir = TextFieldBloc();
  // field tanggal lahir ( field bulan (dropdown) )
  final monthLahir = SelectFieldBloc(items: rawMonths);
  // field tanggal lahir ( field tahun )
  final yearLahir = TextFieldBloc();
  // end tanggal lahir
  // field jenis kelamin
  final jenisKelamin = SelectFieldBloc(
      items: rawJenisKelamin); // items : default value (initial valuenya)
  // field pendidikan
  final pendidikanTerakhir = SelectFieldBloc(
    items: rawPendidikan,
  );
  // field kewarganegaraan
  final kewarganegaraan = SelectFieldBloc<SubDataBiodata, dynamic>(items: [
    SubDataBiodata(id: "78", name: "Indonesia"),
    SubDataBiodata(id: "200", name: "Zimbabwe"),
  ]);
  // field email
  final email = TextFieldBloc();
  // field email lain
  final emailLain = TextFieldBloc();
  // field no telepon
  final noTelepon = TextFieldBloc();
  // field no telepon lain
  final noTeleponLain = TextFieldBloc();
  // baru
  // select regencies tempat lahir
  final birthPlace = SelectFieldBloc<Regency, dynamic>(initialValue: null);
  String selectedBirthPlace; // tempat lahir terpilih
  // Kartu Tanda Penduduk
  final nik = TextFieldBloc();
  // tanggal registrasi ktp
  final dateKtp = TextFieldBloc();
  final dayKtp = TextFieldBloc(); // field tanggal
  final monthKtp = SelectFieldBloc(items: rawMonths); // field bulan
  final yearKtp = TextFieldBloc(); // field tahun
  final dateExpKtp =
      TextFieldBloc(); // untuk throw error ( menggunakan blocbuilder )
  // end
  // ktp valid
  // subdatabiodata = model data berisi id & name
  final ktpValidUntil = SelectFieldBloc(items: rawKtpValidUntil);
  // final expKtp = ListFieldBloc<dynamic>(name: "expKtp");
  final expKtp = ListFieldBloc(name: "expKtp");
  // paspor
  final paspor = TextFieldBloc();
  // paspor exp
  final dayPaspor = TextFieldBloc();
  final monthPaspor = SelectFieldBloc(items: rawMonths);
  final yearPaspor = TextFieldBloc();
  final datePaspor = TextFieldBloc(); // paspor expired
  final avatarBloc = InputFieldBloc<File, Object>();
  // foto ktp
  final fotoKtp = InputFieldBloc<File, Object>();
  final selfieKtp = InputFieldBloc<File, Object>();
  KycBiodataPribadiHelper bioHelper = KycBiodataPribadiHelper(); // helper
  String token = "";
  BioPribadiBloc({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      namaLengkap,
      birthPlace,
      dayLahir,
      monthLahir,
      yearLahir,
      tglLahir,
      jenisKelamin,
      pendidikanTerakhir,
      kewarganegaraan,
      email,
      emailLain,
      noTelepon,
      noTeleponLain,
      nik,
      dateKtp,
      dateExpKtp,
      dayKtp,
      monthKtp,
      yearKtp,
      ktpValidUntil,
      expKtp,
      paspor,
      dayPaspor,
      monthPaspor,
      yearPaspor,
      datePaspor,
      avatarBloc,
      fotoKtp,
      selfieKtp,
    ]);
    // async validators
    // namaLengkap.addAsyncValidators([KycValidatorHelper.namaLengkap]);
    // set default indonesia
    kewarganegaraan.updateValue(kewarganegaraan.state.items[0]);
    // deteksi ktp apakah seumur hidup / tidak
    // jika iya nambah field baru dengan method showKtpExpiredDate()
    ktpValidUntil.onValueChanges(onData: (data, v) async* {
      if (v.value.id == "2") {
        showKtpExpiredDate();
      } else {
        // jika tidak remove field yg telah ditambahkan
        removeKtpExpiredDate();
      }
    });
    // nandain apakah user melakukan edit
    getKycBiodataPribadi().then((val) async {
      // delay untuk menghindari conflict saat bloc mengupdate value yg didapat dari api
      // contoh : namaLengkap.updateInitialValue(blablabla)
      Constants.getUserData().then((value) {
        email.updateInitialValue(value.email);
        noTelepon.updateInitialValue(value.phone.substring(0, 2) == "62" ? value.phone.substring(2, value.phone.length) : value.phone);
      });
      await Future.delayed(Duration(milliseconds: 500));
      // delay terlewati
      // deteksi perubahan tiap field apabila user mengubah field
      onFieldsChange();
      updateAllFields();
    });
  }

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("full_name", namaLengkap);
    updateFieldStatus("birth_place", birthPlace);
    updateTextFieldStatus("another_email", emailLain);
    updateTextFieldStatus("alt_phone", noTeleponLain);
    updateTextFieldStatus("idcard_number", nik);
    updateTextFieldStatus("passport_number", paspor);
    // UPDATE IMAGE
    updateFieldStatus("photo", avatarBloc);
    updateFieldStatus("idcard_photo", fotoKtp);
    updateFieldStatus("verification_photo", selfieKtp);
    // UPDATE RADIO BUTTON
    updateFieldStatus("gender", jenisKelamin);
    updateFieldStatus("type_idcard", ktpValidUntil);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("education_id", pendidikanTerakhir);
  }

  String _parseBirthPlaceString(String birthPlace) {
    try {
      return "${int.parse(birthPlace)}";
    } catch (e) {
      return null;
    }
  }

  // Jika user melakukan edit
  // Ketika KYC Ditolak
  // Get alasan ditolak pada setiap field
  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "1").then((value) {
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  // Jika user melakukan edit
  // Get kyc biodata pribadi data
  Future getKycBiodataPribadi() async {
    emitLoading(); // update state ke loading
    try {
      // get biodata pribadi
      await _service.getBiodataPribadiData().then((value) async {
        // jika get biodata pribadi berhasil
        if (value.statusCode == 200) {
          BiodataPribadiModel data = BiodataPribadiModel.fromJson(value.data);
          // set default value kedalam tiap field
          if (data?.photo != null) {
            avatarBloc.updateExtraData({
              "hasPhoto": true,
              "url": GetAuthenticatedFile.convertUrl(
                image: data?.photo,
                type: PathType.traderPhoto,
              ),
            });
          }
          //
          getFieldErrorStatus(); // mendapatkan status error setiap field
          namaLengkap.updateInitialValue(data.name); // update nama lengkap
          selectedBirthPlace = _parseBirthPlaceString(
              data.birthPlace); // update tempat lahir terpilih
          var tglLahir =
              DateTime.parse(data.birthDate); // parsing tanggal lahir
          dayLahir.updateInitialValue(
            "${tglLahir.day < 10 ? '0' + tglLahir.day.toString() : tglLahir.day}",
          ); // update tanggal lahir
          monthLahir.updateInitialValue(
            monthLahir.state.items[tglLahir.month - 1],
          ); // update bulan lahir
          yearLahir.updateInitialValue(
              tglLahir.year.toString()); // update tahun lahir
          // jika jenis kelamin == m, maka default valuenya index ke 0 dari variable rawJenisKelamin
          jenisKelamin.updateInitialValue(
            jenisKelamin.state.items[data.gender.toLowerCase() == "m" ? 0 : 1],
          );
          pendidikanTerakhir.updateInitialValue(
            pendidikanTerakhir.state.items[data.educationId - 1],
          ); // update pendidikan terakhir
          email.updateInitialValue(data.email); // update email
          if (data.anotherEmail != null)
            // jika email lain tidak kosong, update email lain
            emailLain.updateInitialValue(data.anotherEmail);
          noTelepon.updateInitialValue(data.phone.substring(0, 2) == "62" ? data.phone.substring(2, data.phone.length) : data.phone); // update nomor telepon
          if (data.altPhone != null)
            // jika nomor telepon lain tidak kosong, update nomor telepon
            noTeleponLain.updateInitialValue(data.altPhone.substring(0, 2) == "62" ? data.altPhone.substring(2, data.altPhone.length) : data.altPhone);
          nik.updateInitialValue(data.idcardNumber); // update nik
          var tglRegKtp = DateTime.parse(
              data.regisDateIdcard); // parsing tanggal registrasi ktp
          dayKtp.updateInitialValue(
            "${tglRegKtp.day < 10 ? '0' + tglRegKtp.day.toString() : tglRegKtp.day}",
          ); // update value tanggal registrasi ktp
          monthKtp.updateInitialValue(
            monthKtp.state.items[tglRegKtp.month - 1],
          ); // update value bulan registrasi ktp
          yearKtp.updateInitialValue(
              tglRegKtp.year.toString()); // update value tahun registrasi ktp
          // tanggal kadaluarsa ktp
          // ktp = berlaku hingga / ektp = seumur hidup
          ktpValidUntil.updateInitialValue(
            ktpValidUntil.state.items[data.typeIdcard == "KTP" ? 1 : 0],
          );
          // jika tanggal kadaluarsa ktp != null / tipe id card = ktp
          if (data.typeIdcard == "KTP") {
            if (data.expiredDateIdcard != null) {
              try {
                // parsing tanggal kadaluarsa ktp
                var tglExpKtp = DateTime.parse(data.expiredDateIdcard);
                await Future.delayed(Duration(milliseconds: 100));
                // update tanggal lahir
                // ignore: close_sinks
                final dayExpKtp = expKtp.state.fieldBlocs[0] as TextFieldBloc;
                // update bulan lahir
                // ignore: close_sinks
                final monthExpKtp = expKtp.state.fieldBlocs[1]
                    as SelectFieldBloc<SubDataBiodata, dynamic>;
                // update tahun lahir
                // ignore: close_sinks
                final yearExpKtp = expKtp.state.fieldBlocs[2] as TextFieldBloc;
                dayExpKtp.updateInitialValue(
                  "${tglExpKtp.day < 10 ? '0' + tglExpKtp.day.toString() : tglExpKtp.day}",
                );
                monthExpKtp.updateInitialValue(
                  monthExpKtp.state.items[tglExpKtp.month - 1],
                );
                yearExpKtp.updateInitialValue(tglExpKtp.year.toString());
              } catch (e) {
                // print(e);
              }
            }
          }

          // update data paspor
          paspor.updateInitialValue(data.passportNumber);
          // jika nomor paspor tidak kosong
          if (data.passportNumber != null) {
            // jika tanggal kadaluarsa paspor tidak kosong
            if (data.expiredDatePassport != null) {
              // parsing tanggal kadaluarsa paspor
              var tglExpPaspor = DateTime.parse(data.expiredDatePassport);
              // update tanggal kadaluarsa paspor
              dayPaspor.updateInitialValue(
                "${tglExpPaspor.day < 10 ? '0' + tglExpPaspor.day.toString() : tglExpPaspor.day}",
              );
              // update bulan kadaluarsa paspor
              monthPaspor.updateInitialValue(
                monthPaspor.state.items[tglExpPaspor.month - 1],
              );
              // update tahun kadaluarsa paspor
              yearPaspor.updateInitialValue(tglExpPaspor.year.toString());
            }
          }

          // update asset pengguna (foto ktp)
          // mendapatkan token pengguna
          final token = await storage.read(key: 'token');
          // update foto ktp pengguna
          fotoKtp.updateExtraData({
            "hasPhoto": true, // jika punya foto, set value menjadi true
            "url": GetAuthenticatedFile.convertUrl(
              image: data?.photoIdcard,
              type: PathType.traderIdCardPhoto,
            ),
          });
          // update foto selfie pengguna
          selfieKtp.updateExtraData({
            "hasPhoto": true, // jika punya foto, set value menjadi true
            "url": GetAuthenticatedFile.convertUrl(
              image: data?.photoVerification,
              type: PathType.traderVerificationPhoto,
            ),
          });
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          // jika terjadi kesalahan saat mengambil biodata pengguna
          emitFailure(
            failureResponse: "SUBMITERROR Tidak dapat mengambil data biodata",
          );
        }
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      // jika terjadi unexpected error saat mengambil biodata pengguna
      emitFailure(
        failureResponse: "SUBMITERROR Tidak dapat mengambil data biodata",
      );
    }
  }

  // deteksi perubahan setiap field
  onFieldsChange() {
    detectChanges([
      namaLengkap,
      birthPlace,
      dayLahir,
      monthLahir,
      yearLahir,
      tglLahir,
      jenisKelamin,
      pendidikanTerakhir,
      email,
      emailLain,
      noTelepon,
      noTeleponLain,
      nik,
      dayKtp,
      monthKtp,
      yearKtp,
      ktpValidUntil,
      paspor,
      dayPaspor,
      monthPaspor,
      yearPaspor,
      avatarBloc,
      fotoKtp,
      selfieKtp,
    ]);
  }

  detectChanges(List<SingleFieldBloc> blocs) {
    blocs.forEach((bloc) {
      bloc.onValueChanges(onData: (previous, current) async* {
        KycDataDokumenHelper.dokumen.isEdited = true;
      });
    });
  }

  // update submission status tiap field (khusus textfield)
  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "1", 1);
    });
  }

  // update submission status tiap field
  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      _service.updateFieldStatus(submissionId, field, "1", 1);
    });
  }

  // handling method ketika user memilih foto avatar
  void pickImage() async {
    await ImagePicker.pickImage(source: ImageSource.gallery)
        .then((image) async {
      final bool allowed = await ImageProcessor.checkImageExtension(
          image, [".png", ".jpg", ".jpeg"]);
      if (allowed) {
        avatarBloc.updateValue(image);
      } else {
        avatarBloc.addFieldError("Format File Tidak Diijinkan!");
      }
    });
  }

  // update fotoKtp ( callback dari ui, ketika user sudah ambil foto ktp )
  void updateKtpImage(File value) {
    fotoKtp.updateValue(value);
  }

  // update fotoSelfie ( callback dari ui, ketika user sudah ambil foto ktp )
  void updateSelfieImage(File value) {
    selfieKtp.updateValue(value);
  }

  // tampilkan field kadaluarsa ktp
  void showKtpExpiredDate() {
    expKtp.addFieldBloc(TextFieldBloc(name: "expDay"));
    expKtp.addFieldBloc(SelectFieldBloc(items: rawMonths, name: "expMonth"));
    expKtp.addFieldBloc(TextFieldBloc(name: "expYear"));
  }

  // remove field kadaluarsa ktp
  void removeKtpExpiredDate() {
    expKtp.removeFieldBlocsWhere((element) => true);
  }

  // Validation Method
  validateForms() {
    namaLengkap.handleError(() {
      // print(">> Nama Lengkap Error");
    });
  }

  bool dayOfMonthCheckError(int day, String month) {
    var monthParsed = month.toLowerCase();
    var months = [
      "januari",
      "maret",
      "mei",
      "juli",
      "agustus",
      "oktober",
      "desember"
    ];
    if (months.contains(monthParsed)) {
      return day > 31 ? true : false;
    } else {
      return day > 30 ? true : false;
    }
  }

  bool isAvatarError() {
    if (avatarBloc.value == null) {
      // print(">> Foto Profil Kosong");
      if (isEdit) {
        return false;
      } else {
        avatarBloc.addFieldError("Foto Profil tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isNameError() {
    if (namaLengkap.value == null) {
      namaLengkap.addFieldError("Nama lengkap tidak boleh kosong");
      return true;
    } else if (namaLengkap.value.length < 1) {
      namaLengkap.addFieldError("Nama lengkap tidak boleh kosong");
      return true;
    } else if (namaLengkap.value.contains(RegExp(r'[0-9]'), 1)) {
      namaLengkap.addFieldError("Nama tidak boleh mengandung angka");
      return true;
    } else if (namaLengkap.value.length > 420) {
      namaLengkap.addFieldError("Nama lengkap maksimal 420 karakter");
      return true;
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(namaLengkap.value)) {
        return false;
      } else {
        namaLengkap.addFieldError("Nama tidak boleh mengandung simbol");
        return true;
      }
    }
  }

  bool isTempatLahirError() {
    if (selectedBirthPlace == null && birthPlace.value == null) {
      birthPlace.addFieldError("Tempat lahir tidak boleh kosong");
      return true;
    } else {
      return false;
    }
  }

  bool isTglLahirError() {
    final today = DateTime.now();
    if (dayLahir.value.length < 2 ||
        int.parse(dayLahir.value) > 31 ||
        int.parse(dayLahir.value) < 1) {
      dayLahir.addFieldError("Tidak valid!");
      return true;
    } else if (monthLahir.value == null) {
      monthLahir.addFieldError("Bulan tidak valid!");
      return true;
    } else if (yearLahir.value.length < 4) {
      yearLahir.addFieldError("Tidak valid!");
      return true;
    } else if (int.parse(yearLahir.value) > today.year) {
      yearLahir.addFieldError("Tidak valid!");
      return true;
    } else if (!KycHelpers.isLeap(yearLahir.value)) {
      if (monthLahir.value.name.toLowerCase() == "februari") {
        if (int.parse(dayLahir.value) > 28) {
          dayLahir.addFieldError("Tidak valid");
          return true;
        } else {
          // print("hey");
          return false;
        }
      } else {
        if (dayOfMonthCheckError(
            int.parse(dayLahir.value), monthLahir.value.name)) {
          dayLahir.addFieldError("Tidak valid");
          return true;
        } else {
          final DateTime userDob = DateTime.parse(
              "${yearLahir.value}-${monthLahir.value.id}-${dayLahir.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          if (diff > 0) {
            tglLahir.addFieldError("Tanggal lahir tidak valid!");
            return true;
          } else {
            tglLahir.clear();
            return false;
          }
        }
      }
    } else {
      if (dayOfMonthCheckError(
          int.parse(dayLahir.value), monthLahir.value.name)) {
        dayLahir.addFieldError("Tidak valid");
        return true;
      } else {
        final DateTime userDob = DateTime.parse(
            "${yearLahir.value}-${monthLahir.value.id}-${dayLahir.value}");
        final diff = userDob.difference(DateTime.now()).inDays;
        if (diff > 0) {
          tglLahir.addFieldError("Tanggal lahir tidak valid!");
          return true;
        } else {
          tglLahir.clear();
          return false;
        }
      }
    }
  }

  bool isJenisKelaminError() {
    if (jenisKelamin.value == null) {
      jenisKelamin.addFieldError("Jenis kelamin belum dipilih!");
      return true;
    } else {
      return false;
    }
  }

  bool isPendidikanError() {
    if (pendidikanTerakhir.value == null) {
      pendidikanTerakhir
          .addFieldError("Pendidikan terakhir tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isKewarganaegaraanError() {
    if (kewarganegaraan.value == null) {
      kewarganegaraan.addFieldError("Kewarganegaraan tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isEmailError() {
    // if (email.value == null) {
    //   email.addFieldError("Email tidak boleh kosong!");
    //   return true;
    // } else if (!RegExp(
    //         r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
    //     .hasMatch(email.value)) {
    //   email.addFieldError("Format email salah!");
    //   return true;
    // } else if (email.value.length > 55) {
    //   email.addFieldError("Maksimal email berjumlah 55 karakter!");
    //   return true;
    // } else {
    //   return false;
    // }
    return false;
  }

  bool isEmailLainError() {
    if (emailLain.value != null && emailLain.value.length > 0) {
      if (!RegExp(
              r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
          .hasMatch(emailLain.value)) {
        emailLain.addFieldError("Format email salah!");
        return true;
      } else if (emailLain.value.length > 55) {
        emailLain.addFieldError("Maksimal email berjumlah 55 karakter!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isNomorTeleponError() {
    // if (noTelepon.value == null) {
    //   noTelepon.addFieldError("Nomor telepon tidak boleh kosong!");
    //   return true;
    // } else if (noTelepon.value.length > 25) {
    //   noTelepon.addFieldError("Nomor telepon maksimal 25 karakter!");
    //   return true;
    // } else if (noTelepon.value[0] == "0") {
    //   noTelepon.addFieldError("Nomor telepon tidak boleh diawali dengan 0");
    //   return true;
    // } else if (!RegExp(r"^[0-9+#-]*$").hasMatch(noTelepon.value)) {
    //   noTelepon.addFieldError("Nomor telepon harus berisi angka!");
    //   return true;
    // } else {
    //   return false;
    // }
    return false;
  }

  bool isNomorTeleponLainError() {
    if (noTeleponLain.value != null && noTeleponLain.value.length > 0) {
      if (noTeleponLain.value.length > 25) {
        noTeleponLain.addFieldError("Nomor telepon maksimal 25 karakter!");
        return true;
      } else if (noTeleponLain.value[0] == "0") {
        noTeleponLain
            .addFieldError("Nomor telepon tidak boleh diawali dengan 0");
        return true;
      } else if (!RegExp(r"^[0-9+#-]*$").hasMatch(noTeleponLain.value)) {
        noTeleponLain.addFieldError("Nomor telepon harus berisi angka!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isNikError() {
    if (nik.value == null) {
      nik.addFieldError("NIK tidak boleh kosong!");
      return true;
    } else if (nik.value.length < 16) {
      nik.addFieldError("NIK Harus 16 Digit!");
      return true;
    } else if (nik.value.length > 16) {
      nik.addFieldError("NIK Harus 16 Digit!");
      return true;
    } else if (!RegExp(r"^[0-9]*$").hasMatch(nik.value)) {
      nik.addFieldError("NIK harus berisi angka!");
      return true;
    } else {
      return false;
    }
  }

  bool isTglRegistrasiError() {
    final today = DateTime.now();
    if (dayKtp.value.length < 2 ||
        int.parse(dayKtp.value) > 31 ||
        int.parse(dayKtp.value) < 1) {
      dayKtp.addFieldError("Tidak valid!");
      return true;
    } else if (monthKtp.value == null) {
      monthKtp.addFieldError("Bulan tidak valid!");
      return true;
    } else if (yearKtp.value.length < 4) {
      yearKtp.addFieldError("Tidak valid!");
      return true;
    } else if (int.parse(yearKtp.value) > today.year) {
      yearKtp.addFieldError("Tidak valid!");
      return true;
    } else if (!KycHelpers.isLeap(yearKtp.value)) {
      if (monthKtp.value.name.toLowerCase() == "februari") {
        if (int.parse(dayKtp.value) > 28) {
          dayKtp.addFieldError("Tidak valid");
          return true;
        } else {
          final DateTime userDob = DateTime.parse(
              "${yearKtp.value}-${monthKtp.value.id}-${dayKtp.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          if (diff > 0) {
            dateKtp.addFieldError(
                "Tanggal Registrasi KTP harus dari masa lampau / hari ini");
            return true;
          } else {
            dateKtp.clear();
            return false;
          }
        }
      } else {
        if (dayOfMonthCheckError(
            int.parse(dayKtp.value), monthKtp.value.name)) {
          dayKtp.addFieldError("Tidak valid");
          return true;
        } else {
          final DateTime userDob = DateTime.parse(
              "${yearKtp.value}-${monthKtp.value.id}-${dayKtp.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          if (diff > 0) {
            dateKtp.addFieldError(
                "Tanggal Registrasi KTP harus dari masa lampau / hari ini");
            return true;
          } else {
            dateKtp.clear();
            return false;
          }
        }
      }
    } else {
      if (dayOfMonthCheckError(int.parse(dayKtp.value), monthKtp.value.name)) {
        dayKtp.addFieldError("Tidak valid");
        return true;
      } else {
        final DateTime userDob = DateTime.parse(
            "${yearKtp.value}-${monthKtp.value.id}-${dayKtp.value}");
        final diff = userDob.difference(DateTime.now()).inDays;
        if (diff > 0) {
          dateKtp.addFieldError(
              "Tanggal Registrasi KTP harus dari masa lampau / hari ini");
          return true;
        } else {
          dateKtp.clear();
          return false;
        }
      }
    }
  }

  bool isTglKadaluarsaError() {
    if (ktpValidUntil.value == null) {
      ktpValidUntil.addFieldError("Tanggal kadaluarsa tidak boleh kosong!");
      return true;
    } else {
      return false;
    }
  }

  bool isTglKadaluarsaValid() {
    if (ktpValidUntil.value.name == "Berlaku Hingga") {
      // print(">> Tgl Kadaluarsa Validator");
      // ignore: close_sinks
      final dayExpKtp = expKtp.state.fieldBlocs[0] as TextFieldBloc;
      // ignore: close_sinks
      final monthExpKtp = expKtp.state.fieldBlocs[1]
          as SelectFieldBloc<SubDataBiodata, dynamic>;
      // ignore: close_sinks
      final yearExpKtp = expKtp.state.fieldBlocs[2] as TextFieldBloc;

      final today = DateTime.now();
      if (dayExpKtp.value.length < 2 ||
          int.parse(dayExpKtp.value) > 31 ||
          int.parse(dayExpKtp.value) < 1) {
        dayExpKtp.addFieldError("Tidak valid!");
        return true;
      } else if (monthExpKtp.value == null) {
        monthExpKtp.addFieldError("Bulan tidak valid!");
        return true;
      } else if (yearExpKtp.value.length < 4) {
        yearExpKtp.addFieldError("Tidak valid!");
        return true;
      } else if (int.parse(yearExpKtp.value) < today.year) {
        yearExpKtp.addFieldError("Tidak valid!");
        return true;
      } else if (!KycHelpers.isLeap(yearExpKtp.value)) {
        if (monthExpKtp.value.name.toLowerCase() == "februari") {
          if (int.parse(dayExpKtp.value) > 28) {
            dayExpKtp.addFieldError("Tidak valid");
            return true;
          } else {
            return false;
          }
        } else {
          if (dayOfMonthCheckError(
              int.parse(dayExpKtp.value), monthExpKtp.value.name)) {
            dayExpKtp.addFieldError("Tidak valid");
            return true;
          } else {
            // print(">> Ahsiap");
            final DateTime userDob = DateTime.parse(
                "${yearExpKtp.value}-${monthExpKtp.value.id}-${dayExpKtp.value}");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff < 0) {
              dateExpKtp.addFieldError(
                  "Tanggal Kadaluarsa KTP harus lebih dari hari ini");
              return true;
            } else {
              dateExpKtp.clear();
              return false;
            }
          }
        }
      } else {
        if (dayOfMonthCheckError(
            int.parse(dayExpKtp.value), monthLahir.value.name)) {
          dayExpKtp.addFieldError("Tidak valid");
          return true;
        } else {
          // print(">> Ahsiapx");
          final DateTime userDob = DateTime.parse(
              "${yearExpKtp.value}-${monthExpKtp.value.id}-${dayExpKtp.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          // print(diff);
          if (diff < 0) {
            dateExpKtp.addFieldError(
                "Tanggal Kadaluarsa KTP harus lebih dari hari ini");
            return true;
          } else {
            dateExpKtp.clear();
            return false;
          }
        }
      }
    } else {
      return false;
    }
  }

  bool isPasporError() {
    if (paspor.value != null) {
      if (paspor.value.length > 0) {
        if (paspor.value.length > 50) {
          paspor.addFieldError("Paspor maksimal 50 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isPasporExpValid() {
    if (paspor.value != null && paspor.value.length > 0) {
      final today = DateTime.now();
      if (dayPaspor.value.length < 2 ||
          int.parse(dayPaspor.value) > 31 ||
          int.parse(dayPaspor.value) < 1) {
        dayPaspor.addFieldError("Tidak valid!");
        return true;
      } else if (monthPaspor.value == null) {
        monthPaspor.addFieldError("Bulan tidak valid!");
        return true;
      } else if (yearPaspor.value.length < 4) {
        yearPaspor.addFieldError("Tidak valid!");
        return true;
      } else if (int.parse(yearPaspor.value) < today.year) {
        yearPaspor.addFieldError("Tidak valid!");
        return true;
      } else if (!KycHelpers.isLeap(yearPaspor.value)) {
        if (monthPaspor.value.name.toLowerCase() == "februari") {
          if (int.parse(dayPaspor.value) > 28) {
            dayPaspor.addFieldError("Tidak valid");
            return true;
          } else {
            final DateTime userDob = DateTime.parse(
                "${yearPaspor.value}-${monthPaspor.value.id}-${dayPaspor.value}");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff < 0) {
              datePaspor.addFieldError(
                  "Tanggal Kadaluarsa Paspor harus lebih dari hari ini");
              return true;
            } else {
              datePaspor.clear();
              return false;
            }
          }
        } else {
          if (dayOfMonthCheckError(
              int.parse(dayPaspor.value), monthPaspor.value.name)) {
            dayPaspor.addFieldError("Tidak valid");
            return true;
          } else {
            final DateTime userDob = DateTime.parse(
                "${yearPaspor.value}-${monthPaspor.value.id}-${dayPaspor.value}");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff < 0) {
              datePaspor.addFieldError(
                  "Tanggal Kadaluarsa Paspor harus lebih dari hari ini");
              return true;
            } else {
              datePaspor.clear();
              return false;
            }
          }
        }
      } else {
        if (dayOfMonthCheckError(
            int.parse(dayPaspor.value), monthLahir.value.name)) {
          dayPaspor.addFieldError("Tidak valid");
          return true;
        } else {
          final DateTime userDob = DateTime.parse(
              "${yearPaspor.value}-${monthPaspor.value.id}-${dayPaspor.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          if (diff < 0) {
            datePaspor.addFieldError(
                "Tanggal Kadaluarsa Paspor harus lebih dari hari ini");
            return true;
          } else {
            datePaspor.clear();
            return false;
          }
        }
      }
    } else {
      return false;
    }
  }

  bool isFotoKtpError() {
    if (fotoKtp.value == null) {
      if (isEdit) {
        return false;
      } else {
        fotoKtp.addFieldError("Foto KTP tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  bool isFotoSelfieKtpError() {
    if (selfieKtp.value == null) {
      if (isEdit) {
        return false;
      } else {
        selfieKtp.addFieldError("Foto selfie KTP tidak boleh kosong!");
        return true;
      }
    } else {
      return false;
    }
  }

  @override
  void onLoading() async {
    // if (!isEdit) {
    //   emitLoaded();
    // }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message); // nambah error ke bloc
    emitFailure(failureResponse: emit); // ngirim error ke UI
  }

  handleApiValidation(String field, String _errMsg) {
    // logger.i("""
    // >> Field : $field \n
    // >> Message : $_errMsg
    // """);
    // case field ( detek field apa yg error )
    switch (field) {
      // jika yg error field name
      case "name":
        addErrorMessage(namaLengkap, _errMsg, "nama");
        break;
      // jika yg error field full_name
      case "full_name":
        addErrorMessage(namaLengkap, _errMsg, "nama");
        break;
      case "birth_place":
        addErrorMessage(birthPlace, _errMsg, "tempat_lahir");
        break;
      case "birth_date":
        addErrorMessage(tglLahir, _errMsg, "tanggal_lahir");
        break;
      case "gender":
        addErrorMessage(jenisKelamin, _errMsg, "jenis_kelamin");
        break;
      case "education_id":
        addErrorMessage(pendidikanTerakhir, _errMsg, "pendidikan");
        break;
      case "country_id":
        addErrorMessage(kewarganegaraan, _errMsg, "kewarganegaraan");
        break;
      case "country":
        addErrorMessage(kewarganegaraan, _errMsg, "kewarganegaraan");
        break;
      case "email":
        addErrorMessage(email, _errMsg, "email");
        break;
      case "idcard_number":
        addErrorMessage(nik, _errMsg, "nik");
        break;
      case "regis_date_idcard":
        addErrorMessage(dateKtp, _errMsg, "tanggal_registrasi");
        break;
      case "expired_date_idcard":
        addErrorMessage(dateExpKtp, _errMsg, "kadaluarsa_ktp");
        break;
      case "photo":
        addErrorMessage(avatarBloc, _errMsg, "avatar");
        break;
      case "idcard_photo":
        addErrorMessage(fotoKtp, _errMsg, "foto_ktp");
        break;
      case "verification_photo":
        addErrorMessage(selfieKtp, _errMsg, "selfie_ktp");
        break;
      case "passport_number":
        addErrorMessage(paspor, _errMsg, "paspor");
        break;
      case "expired_date_passport":
        addErrorMessage(datePaspor, _errMsg, "kadaluarsa_paspor");
        break;
      case "another_email":
        addErrorMessage(emailLain, _errMsg, "email_lain");
        break;
      case "phone":
        addErrorMessage(noTelepon, _errMsg, "phone");
        break;
      case "type_idcard":
        addErrorMessage(ktpValidUntil, _errMsg, "type_idcard");
        break;
      case "alt_phone":
        addErrorMessage(noTeleponLain, _errMsg, "nomor_telepon_lain");
        break;
      default:
        emitFailure(
          failureResponse:
              "ERRORSUBMIT ${_errMsg != null && _errMsg != '' ? _errMsg : 'Tidak dapat mengirim data, cek kembali form anda!'}",
        );
        break;
    }
  }

  // event ketika user submit data
  @override
  void onSubmitting() async {
    try {
      if (isNameError()) {
        emitFailure(failureResponse: "nama");
      } else if (isTempatLahirError()) {
        emitFailure(failureResponse: "tempat_lahir");
      } else if (isTglLahirError()) {
        emitFailure(failureResponse: "tanggal_lahir");
      } else if (isJenisKelaminError()) {
        emitFailure(failureResponse: "jenis_kelamin");
      } else if (isPendidikanError()) {
        emitFailure(failureResponse: "pendidikan");
      } else if (isKewarganaegaraanError()) {
        emitFailure(failureResponse: "kewarganegaraan");
      } else if (isEmailLainError()) {
        emitFailure(failureResponse: "email_lain");
      } else if (isNomorTeleponLainError()) {
        emitFailure(failureResponse: "nomor_telepon_lain");
      } else if (isNikError()) {
        emitFailure(failureResponse: "nik");
      } else if (isTglRegistrasiError()) {
        emitFailure(failureResponse: "tanggal_registrasi");
      } else if (isTglKadaluarsaError()) {
        emitFailure(failureResponse: "berlaku_ktp");
      } else if (isTglKadaluarsaValid()) {
        emitFailure(failureResponse: "kadaluarsa_ktp");
      } else if (isPasporError()) {
        emitFailure(failureResponse: "paspor");
      } else if (isPasporExpValid()) {
        emitFailure(failureResponse: "kadaluarsa_paspor");
      } else if (isFotoKtpError()) {
        emitFailure(failureResponse: "foto_ktp");
      } else if (isFotoSelfieKtpError()) {
        emitFailure(failureResponse: "selfie_ktp");
      } else {
        // var nama = state.valueOf("name");
        // jika semua field OK
        try {
          // KTP VALID UNTIL
          KycDataDokumen data = KycDataDokumen(
            avatar: avatarBloc?.value ?? null,
            name: namaLengkap?.value ?? null,
            bop: birthPlace.value == null
                ? selectedBirthPlace
                : "${birthPlace?.value?.id}",
            dateBorn: dayLahir?.value ?? '',
            monthBorn: monthLahir?.value?.id ?? '',
            yearBorn: yearLahir?.value ?? '',
            gender: jenisKelamin?.value?.id ?? '',
            education: pendidikanTerakhir?.value?.id ?? '',
            nationality: kewarganegaraan?.value?.id ?? '',
            email: email?.value ?? '',
            altEmail: emailLain?.value ?? '',
            phone: '62${noTelepon?.value}' ?? '',
            altPhone: '62${noTeleponLain?.value}' ?? '',
            nik: nik?.value ?? '',
            idCardType: ktpValidUntil?.value?.id != null &&
                    ktpValidUntil?.value?.id == "1"
                ? "E-KTP"
                : "KTP",
            dayRegId: dayKtp?.value ?? '',
            monthRegId: monthKtp?.value?.id ?? '',
            yearRegId: yearKtp?.value ?? '',
            fotoKtp: fotoKtp?.value ?? null,
            fotoSelfie: selfieKtp?.value ?? null,
          );
          // jika ktp memiliki masa berlakku
          if (ktpValidUntil.value.name == "Berlaku Hingga") {
            // ignore: close_sinks
            final dayExpKtp = expKtp.state.fieldBlocs[0] as TextFieldBloc;
            // ignore: close_sinks
            final monthExpKtp = expKtp.state.fieldBlocs[1]
                as SelectFieldBloc<SubDataBiodata, dynamic>;
            // ignore: close_sinks
            final yearExpKtp = expKtp.state.fieldBlocs[2] as TextFieldBloc;
            data.dayExpId = dayExpKtp.value;
            data.monthExpId = monthExpKtp.value.id;
            data.yearExpId = yearExpKtp.value;
          }

          // jika passport memiliki nilai
          if (paspor.value != null && paspor.value.length > 0) {
            data.passport = paspor.value;
            data.dayExpPassport = dayPaspor.value;
            data.monthExpPassport = monthPaspor.value.id;
            data.yearExpPassport = yearPaspor.value;
          }

          // submitting
          if (isEdit) {
            // jika edit maka gunakan api yg berbeda
            await _service.updateBiodataPribadi(data).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess(
                    successResponse: "Berhasil menyimpan Biodata Pribadi",
                  );
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else {
                  // jika status code bukan 200 / 400
                  emitFailure(
                    failureResponse:
                        "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
                  );
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          } else {
            await _service.sendBiodataPribadi(data).then((value) {
              if (value != null) {
                if (value.statusCode == 200) {
                  emitSuccess(
                    canSubmitAgain: true,
                    successResponse: "Berhasil menyimpan Biodata Pribadi",
                  );
                } else if (value.statusCode == 400) {
                  try {
                    var dummy = ValidationResultModel.fromJson(value.data);
                    handleApiValidation(dummy.field, dummy.message);
                  } catch (e) {
                    emitFailure(
                      failureResponse: "ERRORSUBMIT ${value.data['message']}",
                    );
                  }
                } else if (value.statusCode == 401) {
                  emitFailure(failureResponse: "UNAUTHORIZED");
                } else {
                  // jika status code bukan 200 / 400
                  emitFailure(
                    failureResponse:
                        "ERRORSUBMIT Tidak dapat menjangkau server, gagal mengirimkan data!",
                  );
                }
              } else {
                emitFailure(
                  failureResponse:
                      "ERRORSUBMIT Error timeout, periksa koneksi anda!",
                );
              }
            });
          }
        } catch (e, stack) {
          santaraLog(e, stack);
          // print(e);
          // print(stack);
          emitFailure(
            failureResponse:
                "ERRORSUBMIT Terjadi kesalahan saat mengirimkan data!",
          );
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      // print(e);
      // print(stack);
      emitFailure(
        failureResponse: "ERRORSUBMIT Terjadi kesalahan saat mengirim data",
      );
    }
    // }
  }
}

class Pendidikan {
  final int id;
  final String name;
  Pendidikan({this.id, this.name});
}

class JenisKelamin {
  final int id;
  final String name;
  JenisKelamin({this.id, this.name});
}

class Month {
  final String id;
  final String name;
  Month({this.id, this.name});
}
