import 'dart:io';

import 'package:dashed_circle/dashed_circle.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/kyc/KycDataDokumen.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycErrorWrapper.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraCachedImage.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'PreTakePhotoUI.dart';
import 'bloc/biodata.bloc.dart';

class BiodataPribadiUI extends StatelessWidget {
  final String submissionId;
  final bool isEdit;
  final String status;
  BiodataPribadiUI({
    @required this.submissionId,
    this.isEdit = false,
    @required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycDataDokumenHelper.dokumen.isEdited != null) {
          if (KycDataDokumenHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycDataDokumenHelper.dokumen = KycDataDokumen();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              return false;
            });
            return false;
          } else {
            KycDataDokumenHelper.dokumen = KycDataDokumen();
            return true;
          }
        } else {
          KycDataDokumenHelper.dokumen = KycDataDokumen();
          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(ColorRev.mainBlack),
            title: Text(
              "Biodata Pribadi",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "1/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ))
            ],
          ),
          backgroundColor: Colors.red,
          body: BlocProvider(
            create: (context) => BioPribadiBloc(
              submissionId: this.submissionId,
              isEdit: this.isEdit,
              status: this.status,
            ),
            child: BiodataPribadiForm(),
          )),
    );
  }
}

class BiodataPribadiForm extends StatefulWidget {
  @override
  _BiodataPribadiFormState createState() => _BiodataPribadiFormState();
}

class _BiodataPribadiFormState extends State<BiodataPribadiForm> {
  // dropdown search
  String birthPlace;
  BioPribadiBloc bloc; // inisialisasi bloc
  final scrollDirection = Axis.vertical; // inisialisasi arah scroll
  AutoScrollController
      controller; // untuk auto scroll jika ada error, maka lempar ke field yg error
  KycFieldWrapper fieldWrapper;

  // image picker widget (ktp & selfie)
  Widget _pickerView(File file) {
    return Center(
      child: DottedBorder(
        color: Color(0xFFB8B8B8),
        strokeWidth: 2,
        radius: Radius.circular(Sizes.s15),
        dashPattern: [5, 5],
        child: Container(
          width: Sizes.s250,
          height: Sizes.s150,
          decoration: BoxDecoration(
            color: Color(0xffF4F4F4),
            image:
                file != null ? DecorationImage(image: FileImage(file)) : null,
          ),
          child: file != null
              ? Container()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera_alt,
                      color: Color(0xFFB8B8B8),
                      size: Sizes.s30,
                    ),
                    SizedBox(
                      height: Sizes.s10,
                    ),
                    Text(
                      "Ambil Foto",
                      style: TextStyle(
                        fontSize: FontSize.s15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFB8B8B8),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }

  // Image picker, ketika user sudah ambil foto, ubah foto
  Widget _imagePikcer(InputFieldBloc fieldBloc) {
    return BlocBuilder<InputFieldBloc, InputFieldBlocState>(
      bloc: fieldBloc,
      builder: (context, state) {
        File file = state.value;
        if (bloc.isEdit) {
          return state.value == null
              ? state.extraData != null // extra data
                  ? Center(
                      child: DottedBorder(
                        color: Color(0xFFB8B8B8),
                        strokeWidth: 2,
                        radius: Radius.circular(Sizes.s15),
                        dashPattern: [5, 5],
                        child: file != null
                            ? Container(
                                width: Sizes.s250,
                                height: Sizes.s150,
                                decoration: BoxDecoration(
                                  color: Color(0xffF4F4F4),
                                  image: file != null
                                      ? DecorationImage(image: FileImage(file))
                                      : null,
                                ),
                                child: file != null
                                    ? Container()
                                    : Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.camera_alt,
                                            color: Color(0xFFB8B8B8),
                                            size: Sizes.s30,
                                          ),
                                          SizedBox(
                                            height: Sizes.s10,
                                          ),
                                          Text(
                                            "Ambil Foto",
                                            style: TextStyle(
                                              fontSize: FontSize.s15,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFFB8B8B8),
                                            ),
                                          )
                                        ],
                                      ),
                              )
                            : Container(
                                width: Sizes.s250,
                                height: Sizes.s150,
                                child: SantaraCachedImage(
                                  image: state.extraData["url"],
                                  fit: BoxFit.contain,
                                ),
                              ),
                      ),
                    )
                  : _pickerView(file)
              : _pickerView(file);
        } else {
          return _pickerView(file);
        }
      },
    );
  }

  // avatar picker
  Widget avatarPicker(InputFieldBlocState state) {
    return CircleAvatar(
      radius: Sizes.s80,
      backgroundColor:
          state.value == null ? Color(0xfff4f4f4) : Colors.transparent,
      backgroundImage: state.value == null ? null : FileImage(state.value),
      child: state.value == null
          ? Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.camera_alt,
                  color: Color(0xffB8B8B8),
                  size: FontSize.s24,
                ),
                SizedBox(
                  height: Sizes.s5,
                ),
                Text(
                  "Unggah Foto",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xffB8B8B8),
                  ),
                )
              ],
            )
          : Container(),
    );
  }

  @override
  void initState() {
    super.initState();
    KycDataDokumenHelper.dokumen.isSubmited = null;
    // init bloc
    bloc = BlocProvider.of<BioPribadiBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    // init form bloc
    return KycHideKeyboard(
      child: FormBlocListener<BioPribadiBloc, String, String>(
          // ketika user hit submit button
          onSubmitting: (context, state) {
            PopupHelper.showLoading(context);
          },
          // ketika validasi berhasil / hit api berhasil
          onSuccess: (context, state) {
            Navigator.pop(context);
            ToastHelper.showBasicToast(context, state.successResponse);
            Navigator.pop(context);
            // reset value helper jadi null semua
            KycDataDokumenHelper.dokumen = KycDataDokumen();
          },
          // ketika terjadi kesalahan (validasi / hit api)
          onFailure: (context, state) {
            if (KycDataDokumenHelper.dokumen.isSubmited != null) {
              Navigator.pop(context);
              // reset value helper jadi null semua
              KycDataDokumenHelper.dokumen = KycDataDokumen();
            }
            if (state.failureResponse == "UNAUTHORIZED") {
              NavigatorHelper.pushToExpiredSession(context);
            }
            if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
              // JIKA ERROR MEMILIKI STRING "ERRORSUBMIT"
              ToastHelper.showFailureToast(
                context,
                state.failureResponse.replaceAll("ERRORSUBMIT", ""),
              );
            } else {
              // JIKA ERROR YG DIKIRIM ADALAH IDENTIFIER SETIAP FIELD
              switch (state.failureResponse) {
                // CASE = identifier dari emitFailure, tiap value menandakan error pada field sesuai casenya
                // _scrollTo(x), x = identifier / key widget yang ada di viewWidget
                case "avatar":
                  fieldWrapper.scrollTo(1); // scroll ke setiap field
                  break;
                case "nama":
                  fieldWrapper.scrollTo(2);
                  break;
                case "tempat_lahir":
                  fieldWrapper.scrollTo(3);
                  break;
                case "tanggal_lahir":
                  fieldWrapper.scrollTo(4);
                  break;
                case "jenis_kelamin":
                  fieldWrapper.scrollTo(5);
                  break;
                case "pendidikan":
                  fieldWrapper.scrollTo(6);
                  break;
                case "kewarganegaraan":
                  fieldWrapper.scrollTo(7);
                  break;
                case "email":
                  fieldWrapper.scrollTo(8);
                  break;
                case "email_lain":
                  fieldWrapper.scrollTo(9);
                  break;
                case "nomor_telepon":
                  fieldWrapper.scrollTo(10);
                  break;
                case "nomor_telepon_lain":
                  fieldWrapper.scrollTo(11);
                  break;
                case "nik":
                  fieldWrapper.scrollTo(12);
                  break;
                case "tanggal_registrasi":
                  fieldWrapper.scrollTo(13);
                  break;
                case "berlaku_ktp":
                  fieldWrapper.scrollTo(14);
                  break;
                case "kadaluarsa_ktp":
                  fieldWrapper.scrollTo(15);
                  break;
                case "paspor":
                  fieldWrapper.scrollTo(16);
                  break;
                case "kadaluarsa_paspor":
                  fieldWrapper.scrollTo(17);
                  break;
                case "foto_ktp":
                  fieldWrapper.scrollTo(18);
                  break;
                case "selfie_ktp":
                  fieldWrapper.scrollTo(19);
                  break;
                default:
                  break;
              }
            }
          },
          child: BlocBuilder<BioPribadiBloc, FormBlocState>(
            buildWhen: (previous, current) =>
                previous.runtimeType != current.runtimeType ||
                previous is FormBlocLoading && current is FormBlocLoading,
            builder: (context, state) {
              if (state is FormBlocLoading) {
                // jika sedang loading
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (state is FormBlocLoadFailed) {
                // jika gagal memuat
                return Center(
                  child: Text("Failed to load!"),
                );
              } else {
                // jika berhasil memuat
                return Container(
                  width: double.maxFinite,
                  height: double.maxFinite,
                  color: Color(ColorRev.mainBlack),
                  padding: EdgeInsets.all(Sizes.s20),
                  child: ListView(
                    scrollDirection: scrollDirection,
                    controller: controller,
                    children: [
                      // divider
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      // data diri title
                      Text(
                        "Data Diri",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s16,
                        ),
                      ),
                      // divider line
                      Container(
                        margin: EdgeInsets.only(top: Sizes.s10),
                        height: 0.5,
                        width: double.maxFinite,
                        color: Color(0xffF4F4F4),
                      ),
                      // divider
                      SizedBox(
                        height: Sizes.s20,
                      ),
                      // avatar
                      BlocBuilder<InputFieldBloc, InputFieldBlocState>(
                        bloc: bloc.avatarBloc,
                        builder: (context, state) {
                          return Column(
                            children: [
                              BlocBuilder<InputFieldBloc, InputFieldBlocState>(
                                bloc: bloc.avatarBloc,
                                builder: (context, state) {
                                  return fieldWrapper.viewWidget(
                                    1,
                                    Sizes.s165,
                                    Center(
                                        child: DashedCircle(
                                      gapSize: 1.0,
                                      dashes: 50,
                                      color: Colors.grey[350],
                                      child: InkWell(
                                        onTap: () {
                                          if (KycHelpers.fieldCheck(
                                              bloc.status)) bloc.pickImage();
                                        },
                                        child: bloc.isEdit
                                            ? state.value == null
                                                ? state.extraData != null
                                                    ? Container(
                                                        width: Sizes.s100,
                                                        height: Sizes.s100,
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                            Radius.circular(
                                                              Sizes.s50,
                                                            ),
                                                          ),
                                                          child:
                                                              SantaraCachedImage(
                                                            image:
                                                                state.extraData[
                                                                    "url"],
                                                          ),
                                                        ),
                                                      )
                                                    : avatarPicker(state)
                                                : avatarPicker(state)
                                            : avatarPicker(state),
                                      ),
                                    )),
                                  );
                                },
                              ),
                              SizedBox(height: Sizes.s10),
                              state.hasError
                                  ? Text(
                                      "${state.error}",
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: FontSize.s12),
                                    )
                                  : Container()
                            ],
                          );
                        },
                      ),
                      Center(
                        child: Text(
                          "*Opsional",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: FontSize.s12,
                          ),
                        ),
                      ),
                      // divider
                      SizedBox(height: Sizes.s50),
                      // nama lengkap
                      fieldWrapper.viewWidget(
                        2,
                        Sizes.s100,
                        SantaraBlocTextField(
                          key: Key('full_name'),
                          textFieldBloc: bloc.namaLengkap,
                          hintText: "Contoh : Putri Aisyah",
                          labelText: "Nama Lengkap",
                          inputType: TextInputType.text,
                          enabled: KycHelpers.fieldCheck(bloc.status),
                          // onChanged: (val) {
                          //   bloc.updateFieldStatus("name", bloc.namaLengkap);
                          // },
                        ),
                      ),
                      // tempat lahir
                      fieldWrapper.viewWidget(
                        3,
                        Sizes.s100,
                        SearchRegencyField(
                          regencyBloc: bloc.birthPlace,
                          selectedRegency: bloc.selectedBirthPlace,
                          hintText: "Pilih Tempat Lahir Anda",
                          label: "Tempat Lahir",
                          enabled: KycHelpers.isVerified(bloc.status)
                              ? KycHelpers.fieldCheck(bloc.status)
                              : false,
                          onTapIfDisabled: () {
                            PopupHelper.showMessage(
                              context,
                              "Anda tidak bisa merubah data ini sekarang",
                            );
                          },
                        ),
                      ),
                      // tanggal lahir title
                      Text(
                        "Tanggal Lahir",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      // tanggal lahir input
                      fieldWrapper.viewWidget(
                        4,
                        Sizes.s110,
                        Padding(
                          padding: EdgeInsets.only(top: Sizes.s10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  height: Sizes.s100,
                                  child: SantaraBlocTextField(
                                    textFieldBloc: bloc.dayLahir,
                                    hintText: "01",
                                    labelText: "Tanggal",
                                    inputType: TextInputType.number,
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(2),
                                      FilteringTextInputFormatter.allow(
                                        RegExp(r"[0-9]*$"),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  height: Sizes.s100,
                                  margin: EdgeInsets.only(
                                    right: Sizes.s10,
                                    left: Sizes.s10,
                                  ),
                                  child: SantaraBlocDropDown(
                                    selectFieldBloc: bloc.monthLahir,
                                    label: "Bulan",
                                    hintText: "Januari",
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  height: Sizes.s100,
                                  child: SantaraBlocTextField(
                                    textFieldBloc: bloc.yearLahir,
                                    hintText: "2020",
                                    labelText: "Tahun",
                                    inputType: TextInputType.number,
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(4),
                                      FilteringTextInputFormatter.allow(
                                        RegExp(r"[0-9]*$"),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      KycErrorWrapper(bloc: bloc.tglLahir),
                      // jenis kelamin title
                      Text(
                        "Jenis Kelamin",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      // jenis kelamin input
                      fieldWrapper.viewWidget(
                        5,
                        Sizes.s130,
                        SantaraBlocRadioGroup(
                          selectFieldBloc: bloc.jenisKelamin,
                          enabled: KycHelpers.isVerified(bloc.status)
                              ? KycHelpers.fieldCheck(bloc.status)
                              : false,
                        ),
                      ),
                      // pendidikan
                      fieldWrapper.viewWidget(
                        6,
                        Sizes.s100,
                        SantaraBlocDropDown(
                          selectFieldBloc: bloc.pendidikanTerakhir,
                          label: "Pendidikan Terakhir",
                          enabled: KycHelpers.fieldCheck(bloc.status),
                        ),
                      ),
                      // kewarganegaraan
                      fieldWrapper.viewWidget(
                        7,
                        Sizes.s100,
                        SantaraBlocDropDown(
                          selectFieldBloc: bloc.kewarganegaraan,
                          label: "Kewarganaegaraan",
                          enabled: false,
                        ),
                      ),
                      // email
                      fieldWrapper.viewWidget(
                        8,
                        Sizes.s100,
                        SantaraBlocTextField(
                          textFieldBloc: bloc.email,
                          hintText: "Email",
                          labelText: "Email",
                          inputType: TextInputType.emailAddress,
                          enabled: false,
                          suffixIcon: Icon(Icons.info),
                          onTapIfDisabled: () {
                            PopupHelper.showDataChange(context);
                          },
                        ),
                      ),
                      // email lain
                      fieldWrapper.viewWidget(
                        9,
                        Sizes.s100,
                        SantaraBlocTextField(
                          textFieldBloc: bloc.emailLain,
                          hintText: "*Kosongkan jika tidak ada (Opsional)",
                          labelText: "Email Lain (Opsional)",
                          inputType: TextInputType.emailAddress,
                          enabled: KycHelpers.fieldCheck(bloc.status),
                        ),
                      ),
                      //  no telepon
                      fieldWrapper.viewWidget(
                          10,
                          Sizes.s100,
                          SantaraBlocTextField(
                            textFieldBloc: bloc.noTelepon,
                            hintText: "Contoh : 85292550564",
                            labelText: "No Telepon",
                            isPhone: true,
                            inputType: TextInputType.phone,
                            enabled: false,
                            suffixIcon: Icon(Icons.info),
                            onTapIfDisabled: () {
                              PopupHelper.showDataChange(context);
                            },
                          )),
                      fieldWrapper.viewWidget(
                        11,
                        Sizes.s100,
                        SantaraBlocTextField(
                          textFieldBloc: bloc.noTeleponLain,
                          isPhone: true,
                          hintText: "*Kosongkan jika tidak ada (Opsional)",
                          labelText: "No Telepon Lain (Opsional)",
                          inputType: TextInputType.phone,
                          enabled: KycHelpers.fieldCheck(bloc.status),
                        ),
                      ),
                      fieldWrapper.divider,
                      Text(
                        "Dokumen Data Diri",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s16,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: Sizes.s10),
                        height: 4,
                        width: double.maxFinite,
                        color: Color(0xffF4F4F4),
                      ),
                      fieldWrapper.divider,
                      fieldWrapper.viewWidget(
                        12,
                        Sizes.s100,
                        SantaraBlocTextField(
                          textFieldBloc: bloc.nik,
                          hintText: "NIK",
                          labelText: "Nomor Induk Kependudukan (NIK)",
                          inputType: TextInputType.number,
                          enabled: KycHelpers.fieldCheck(bloc.status),
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(16),
                          ],
                        ),
                      ),
                      // title registrasi ktp
                      Text(
                        "Tanggal Registrasi KTP",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      // Tanggal registrasi KTP
                      fieldWrapper.viewWidget(
                        13,
                        Sizes.s110,
                        Padding(
                          padding: EdgeInsets.only(top: Sizes.s10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  height: Sizes.s100,
                                  child: SantaraBlocTextField(
                                    textFieldBloc: bloc.dayKtp,
                                    hintText: "01",
                                    labelText: "Tanggal",
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                    inputType: TextInputType.number,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(2),
                                      FilteringTextInputFormatter.allow(
                                        RegExp(r"[0-9]*$"),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.only(
                                    left: Sizes.s10,
                                    right: Sizes.s10,
                                  ),
                                  height: Sizes.s100,
                                  child: SantaraBlocDropDown(
                                    selectFieldBloc: bloc.monthKtp,
                                    label: "Bulan",
                                    hintText: "Januari",
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(
                                  height: Sizes.s100,
                                  child: SantaraBlocTextField(
                                    textFieldBloc: bloc.yearKtp,
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                    hintText: "2020",
                                    labelText: "Tahun",
                                    inputType: TextInputType.number,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(4),
                                      FilteringTextInputFormatter.allow(
                                        RegExp(r"[0-9]*$"),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      KycErrorWrapper(bloc: bloc.dateKtp),
                      Text(
                        "Tanggal Kadaluarsa KTP",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      fieldWrapper.viewWidget(
                        14,
                        Sizes.s130,
                        SantaraBlocRadioGroup(
                          selectFieldBloc: bloc.ktpValidUntil,
                          enabled: KycHelpers.fieldCheck(bloc.status),
                        ),
                      ),
                      BlocBuilder<ListFieldBloc, ListFieldBlocState>(
                        bloc: bloc.expKtp,
                        builder: (context, state) {
                          if (state.fieldBlocs.isNotEmpty) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                fieldWrapper.viewWidget(
                                  15,
                                  Sizes.s110,
                                  Padding(
                                    padding: EdgeInsets.only(top: Sizes.s10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: Sizes.s100,
                                            child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc:
                                                  state.fieldBlocs[0],
                                              hintText: "01",
                                              labelText: "Tanggal",
                                              inputType: TextInputType.number,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    2),
                                                FilteringTextInputFormatter
                                                    .allow(
                                                  RegExp(r"[0-9]*$"),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                              left: Sizes.s10,
                                              right: Sizes.s10,
                                            ),
                                            height: Sizes.s100,
                                            child: SantaraBlocDropDown(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              selectFieldBloc:
                                                  state.fieldBlocs[1],
                                              label: "Bulan",
                                              hintText: "Januari",
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            height: Sizes.s100,
                                            child: SantaraBlocTextField(
                                              enabled: KycHelpers.fieldCheck(
                                                  bloc.status),
                                              textFieldBloc:
                                                  state.fieldBlocs[2],
                                              hintText: "2020",
                                              labelText: "Tahun",
                                              inputType: TextInputType.number,
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    4),
                                                FilteringTextInputFormatter
                                                    .allow(
                                                  RegExp(r"[0-9]*$"),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                KycErrorWrapper(bloc: bloc.dateExpKtp),
                              ],
                            );
                          } else {
                            return Container();
                          }
                        },
                      ),
                      fieldWrapper.viewWidget(
                        16,
                        Sizes.s100,
                        SantaraBlocTextField(
                          textFieldBloc: bloc.paspor,
                          hintText: "*Kosongkan jika tidak ada (Optional)",
                          labelText: "Nomor Passport",
                          enabled: KycHelpers.fieldCheck(bloc.status),
                        ),
                      ),
                      // title tgl kadaluarsa
                      Text(
                        "Tanggal Kadaluarsa Passport",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        "*Kosongkan jika field Passport kosong (Opsional)",
                        style: TextStyle(
                          fontSize: FontSize.s10,
                          color: Color(0xffB8B8B8),
                        ),
                      ),
                      // input tanggal kadaluarsa
                      fieldWrapper.viewWidget(
                        17,
                        Sizes.s110,
                        Padding(
                          padding: EdgeInsets.only(top: Sizes.s10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: Sizes.s100,
                                height: Sizes.s100,
                                child: SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.dayPaspor,
                                  hintText: "01",
                                  labelText: "Tanggal",
                                  inputType: TextInputType.number,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(2),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: Sizes.s100,
                                width: Sizes.s150,
                                child: SantaraBlocDropDown(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  selectFieldBloc: bloc.monthPaspor,
                                  label: "Bulan",
                                  hintText: "Januari",
                                ),
                              ),
                              Container(
                                height: Sizes.s100,
                                width: Sizes.s100,
                                child: SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.yearPaspor,
                                  hintText: "2020",
                                  labelText: "Tahun",
                                  inputType: TextInputType.number,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(4),
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r"[0-9]*$"),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      KycErrorWrapper(bloc: bloc.datePaspor),
                      Text(
                        "Unggah Foto Kartu Data Diri",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      fieldWrapper.divider,
                      // image picker
                      fieldWrapper.viewWidget(
                        18,
                        Sizes.s150,
                        InkWell(
                          onTap: () async {
                            if (KycHelpers.isVerified(bloc.status) &&
                                KycHelpers.fieldCheck(bloc.status)) {
                              final KycDataDokumen result =
                                  await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PreTakePhotoUI(
                                    type: CamType.ktp,
                                  ),
                                ),
                              );
                              if (result != null) {
                                if (result.fotoKtp != null) {
                                  bloc.updateKtpImage(result.fotoKtp);
                                }

                                if (result.fotoSelfie != null) {
                                  bloc.updateSelfieImage(result.fotoSelfie);
                                }
                              }
                            }
                          },
                          child: _imagePikcer(bloc.fotoKtp),
                        ),
                      ),
                      KycErrorWrapper(bloc: bloc.fotoKtp),
                      fieldWrapper.divider,
                      Text(
                        "Unggah Foto Selfie dengan Kartu Data Diri",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      fieldWrapper.divider,
                      fieldWrapper.viewWidget(
                        19,
                        Sizes.s150,
                        InkWell(
                          onTap: () async {
                            if (KycHelpers.isVerified(bloc.status) &&
                                KycHelpers.fieldCheck(bloc.status)) {
                              final KycDataDokumen result =
                                  await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PreTakePhotoUI(
                                    type: CamType.selfie,
                                  ),
                                ),
                              );
                              if (result != null) {
                                if (result.fotoKtp != null) {
                                  bloc.updateKtpImage(result.fotoKtp);
                                }

                                if (result.fotoSelfie != null) {
                                  bloc.updateSelfieImage(result.fotoSelfie);
                                }
                              }
                            }
                          },
                          child: _imagePikcer(bloc.selfieKtp),
                        ),
                      ),
                      KycErrorWrapper(bloc: bloc.selfieKtp),
                      fieldWrapper.divider,
                      !KycHelpers.fieldCheck(bloc.status)
                          ? SantaraDisabledButton()
                          : SantaraMainButton(
                              onPressed: () {
                                if (bloc.status == "verified") {
                                  PopupHelper.showConfirmationDataChange(
                                      context, () {
                                    Navigator.pop(context);
                                    KycDataDokumenHelper.dokumen.isSubmited =
                                        true;
                                    FocusScope.of(context).unfocus();
                                    bloc.submit();
                                  });
                                } else {
                                  KycDataDokumenHelper.dokumen.isSubmited =
                                      true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                }
                              },
                              title: "Simpan",
                            )
                    ],
                  ),
                );
              }
            },
          )),
    );
  }
}
