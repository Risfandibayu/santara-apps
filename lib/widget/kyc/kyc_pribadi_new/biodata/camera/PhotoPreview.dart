import 'dart:io';
import 'package:flutter/material.dart';

class PhotoPreviewUI extends StatefulWidget {
  final File image;
  PhotoPreviewUI({@required this.image});

  @override
  _PhotoPreviewUIState createState() => _PhotoPreviewUIState();
}

class _PhotoPreviewUIState extends State<PhotoPreviewUI> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 1.5;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: Center(
            child: Container(
              height: height,
              decoration: BoxDecoration(
                  image: DecorationImage(image: FileImage(widget.image))),
            ),
          )),
    );
  }
}
