// import 'dart:io';

// import 'package:camera_camera/camera_camera.dart';
// import 'package:dotted_border/dotted_border.dart';
// import 'package:flutter/material.dart';
// import 'package:santaraapp/helpers/ImageProcessor.dart';
// import 'package:santaraapp/utils/sizes.dart';
// import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/camera/DataDiriPreview.dart';

// enum KtpAction { takeFromPage, takeFromDoc }

// class CameraKTP extends StatefulWidget {
//   final KtpAction ktpAction;
//   CameraKTP({@required this.ktpAction});

//   @override
//   _CameraKTPState createState() => _CameraKTPState();
// }

// class _CameraKTPState extends State<CameraKTP> {
//   File val;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Camera(
//       enableCameraChange: false,
//       imageMask: Center(
//         child: Container(
//           height: Sizes.s350,
//           margin: EdgeInsets.only(
//             left: Sizes.s10,
//             right: Sizes.s10,
//           ),
//           child: Column(
//             children: [
//               DottedBorder(
//                   color: Colors.black,
//                   strokeWidth: 2,
//                   radius: Radius.circular(Sizes.s15),
//                   dashPattern: [5, 5],
//                   child: Stack(
//                     children: [
//                       Align(
//                         alignment: Alignment.topRight,
//                         child: Padding(
//                           padding: EdgeInsets.only(
//                             top: Sizes.s50,
//                             right: Sizes.s25,
//                           ),
//                           child: DottedBorder(
//                             color: Colors.black,
//                             strokeWidth: 2,
//                             radius: Radius.circular(Sizes.s15),
//                             dashPattern: [5, 5],
//                             child: Container(
//                               width: Sizes.s90,
//                               height: Sizes.s110,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Container(
//                         // width: Sizes.s250,
//                         height: Sizes.s250,
//                         decoration: BoxDecoration(color: Colors.transparent),
//                       ),
//                     ],
//                   )),
//               SizedBox(
//                 height: Sizes.s20,
//               ),
//               Text("Pastikan foto masuk dalam area box")
//             ],
//           ),
//         ),
//       ),
//       mode: CameraMode.fullscreen,
//       onFile: (File file) async {
//         var newFile = await ImageProcessor.cropSquare(file.path, false);
//         widget.ktpAction == KtpAction.takeFromDoc
//             ? Navigator.pop(context, newFile)
//             : Navigator.pushReplacement(
//                 context,
//                 MaterialPageRoute(
//                     builder: (context) => DataDiriPreviewUI(
//                           ktp: newFile,
//                         )));
//       },
//     ));
//   }
// }
