import 'dart:io';
import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/kyc/KycDataDokumen.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/camera/PhotoPreview.dart';
import 'package:santaraapp/widget/widget/Camera.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class DataDiriPreviewUI extends StatefulWidget {
  final File ktp;
  final File selfie;
  DataDiriPreviewUI({this.ktp, this.selfie});

  @override
  _DataDiriPreviewUIState createState() => _DataDiriPreviewUIState(ktp, selfie);
}

class _DataDiriPreviewUIState extends State<DataDiriPreviewUI> {
  File ktp;
  File selfie;
  _DataDiriPreviewUIState(this.ktp, this.selfie);

  @override
  void initState() {
    super.initState();
    if (KycDataDokumenHelper.dokumen.fotoKtp != null) {
      widget.ktp == null
          ? setState(() => ktp = KycDataDokumenHelper.dokumen.fotoKtp)
          : null;
    }

    if (KycDataDokumenHelper.dokumen.fotoSelfie != null) {
      widget.selfie == null
          ? setState(() => selfie = KycDataDokumenHelper.dokumen.fotoSelfie)
          : null;
    }
  }

  Future<File> pickPhoto() async {
    var cameras = await availableCameras();
    File file;
    if (cameras.length == 0) {
      file = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      try {
        file = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => Camera(isKyc: 2),
          ),
        );
      } catch (e, stack) {
        ToastHelper.showFailureToast(context, e);
        return null;
      }
    }
    return file;
  }

  void _changePhoto(String type) async {
    final result = await pickPhoto();
    if (result != null) {
      if (type == 'ktp') {
        setState(() {
          ktp = result;
        });
      } else {
        setState(() {
          selfie = result;
        });
      }
    }
  }

  Widget _imagePikcer() {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Center(
        child: DottedBorder(
          color: Color(0xFFB8B8B8),
          strokeWidth: 2,
          radius: Radius.circular(Sizes.s15),
          dashPattern: [5, 5],
          child: Container(
            width: Sizes.s250,
            height: Sizes.s150,
            decoration: BoxDecoration(
              color: Color(0xffF4F4F4),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.camera_alt,
                  color: Color(0xFFB8B8B8),
                  size: Sizes.s30,
                ),
                SizedBox(
                  height: Sizes.s10,
                ),
                Text(
                  "Ambil Foto",
                  style: TextStyle(
                    fontSize: FontSize.s15,
                    fontWeight: FontWeight.w600,
                    color: Color(0xFFB8B8B8),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _fotoBuilder(BuildContext context, File file, String type) {
    return file != null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: Sizes.s20,
              ),
              LimitedBox(
                maxHeight: Sizes.s200,
                child: Container(
                  width: Sizes.s250,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: FileImage(file),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MaterialButton(
                    color: Colors.white,
                    elevation: 0,
                    onPressed: () => _changePhoto(type),
                    child: Text(
                      "Ubah Foto",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: Sizes.s10,
                  ),
                  MaterialButton(
                    color: Colors.white,
                    elevation: 0,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PhotoPreviewUI(image: file)));
                    },
                    child: Text(
                      "Lihat Foto",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )
        : InkWell(onTap: () => _changePhoto(type), child: _imagePikcer());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.only(
          left: Sizes.s25,
          right: Sizes.s25,
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Unggah foto Dokumen Data Diri",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s14,
                  ),
                ),
              ),
              _fotoBuilder(context, ktp, 'ktp'),
              SizedBox(
                height: Sizes.s50,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Unggah Foto Selfie dengan Dokumen Data Diri",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s14,
                  ),
                ),
              ),
              _fotoBuilder(context, selfie, 'selfie'),
              SizedBox(
                height: Sizes.s20,
              ),
              SantaraMainButton(
                title: "Simpan",
                onPressed: () {
                  final KycDataDokumen dokumen = KycDataDokumen(
                    fotoKtp: ktp,
                    fotoSelfie: selfie,
                  );

                  setState(() {
                    if (dokumen.fotoKtp != null) {
                      KycDataDokumenHelper.dokumen.fotoKtp = dokumen.fotoKtp;
                    }

                    if (dokumen.fotoSelfie != null) {
                      KycDataDokumenHelper.dokumen.fotoSelfie =
                          dokumen.fotoSelfie;
                    }
                  });

                  Navigator.pop(context);
                  Navigator.pop(context, dokumen);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
