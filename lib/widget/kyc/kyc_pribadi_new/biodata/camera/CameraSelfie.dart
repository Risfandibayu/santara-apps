// import 'dart:io';
// import 'package:camera/camera.dart';
// import 'package:camera_camera/camera_camera.dart';
// import 'package:dotted_border/dotted_border.dart';
// import 'package:flutter/material.dart';
// import 'package:santaraapp/utils/sizes.dart';
// import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/camera/DataDiriPreview.dart';

// enum CameraSelfieAction { takeFromPage, takeFromDoc }

// class CameraSelfie extends StatefulWidget {
//   final CameraSelfieAction cameraSelfieAction;
//   CameraSelfie({@required this.cameraSelfieAction});

//   @override
//   _CameraSelfieState createState() => _CameraSelfieState();
// }

// class _CameraSelfieState extends State<CameraSelfie> {
//   File val;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Camera(
//         imageMask: Center(
//           child: Container(
//             height: Sizes.s450,
//             margin: EdgeInsets.only(
//               left: Sizes.s10,
//               right: Sizes.s10,
//             ),
//             child: Column(
//               children: [
//                 Container(
//                   decoration: BoxDecoration(
//                       border: Border.all(width: 2, color: Colors.black),
//                       borderRadius:
//                           BorderRadius.all(Radius.circular(Sizes.s150))),
//                   width: Sizes.s120,
//                   height: Sizes.s180,
//                 ),
//                 SizedBox(
//                   height: Sizes.s50,
//                 ),
//                 DottedBorder(
//                   color: Colors.black,
//                   strokeWidth: 2,
//                   radius: Radius.circular(Sizes.s15),
//                   dashPattern: [5, 5],
//                   child: Container(
//                     // width: Sizes.s250,
//                     height: Sizes.s200,
//                     decoration: BoxDecoration(color: Colors.transparent),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         mode: CameraMode.fullscreen,
//         resolution: ResolutionPreset.max,
//         onFile: (File file) {
//           widget.cameraSelfieAction == CameraSelfieAction.takeFromDoc
//               ? Navigator.pop(context, file)
//               : Navigator.pushReplacement(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) => DataDiriPreviewUI(
//                       selfie: file,
//                     ),
//                   ),
//                 );
//         },
//       ),
//     );
//   }
// }
