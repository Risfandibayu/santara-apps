import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../helpers/NavigatorHelper.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../helpers/kyc/BankPribadiHelper.dart';
import '../../../../helpers/kyc/KycHelper.dart';
import '../../../../models/BankInvestor.dart';
import '../../../../utils/sizes.dart';
import '../../../widget/components/kyc/KycFieldWrapper.dart';
import '../../../widget/components/main/SantaraButtons.dart';
import '../../../widget/components/main/SantaraField.dart';
import '../../widgets/kyc_hide_keyboard.dart';
import 'bloc/bank.bloc.dart';

class KycBankUI extends StatelessWidget {
  final bool isEdit;
  final String submissionId;
  final String status;
  KycBankUI({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  });
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (KycBankUserHelper.dokumen.isEdited != null) {
          if (KycBankUserHelper.dokumen.isEdited) {
            PopupHelper.handleCloseKyc(context, () {
              // yakin handler
              // reset helper jadi null
              Navigator.pop(context); // close alert
              Navigator.pop(context); // close page
              KycBankUserHelper.dokumen = BankUser();
              return true;
            }, () {
              // batal handler
              Navigator.pop(context); // close alert
              return false;
            });
            return false;
          } else {
            KycBankUserHelper.dokumen = BankUser();
            return true;
          }
        } else {
          KycBankUserHelper.dokumen = BankUser();
          return true;
        }
      },
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "Bank",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            actions: [
              FlatButton(
                  onPressed: null,
                  child: Text(
                    "6/6",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
          body: BlocProvider(
            create: (context) => BankUserBloc(
              isEdit: isEdit,
              submissionId: submissionId,
              status: status,
            ),
            child: BankUserField(),
          )),
    );
  }
}

class BankUserField extends StatefulWidget {
  @override
  _BankUserFieldState createState() => _BankUserFieldState();
}

class _BankUserFieldState extends State<BankUserField> {
  BankUserBloc bloc;
  final scrollDirection = Axis.vertical;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;

  _reload(VoidCallback onPressed) {
    return FlatButton(
      onPressed: onPressed,
      shape: Border.all(
        width: 1,
        color: Colors.grey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.replay,
            size: FontSize.s15,
          ),
          Container(
            width: Sizes.s10,
          ),
          Text(
            "Muat Ulang",
            style: TextStyle(
              fontSize: FontSize.s15,
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    KycBankUserHelper.dokumen.isSubmitted = null;
    // init bloc
    bloc = BlocProvider.of<BankUserBloc>(context);
    // init auto scroll
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return KycHideKeyboard(
      child: FormBlocListener<BankUserBloc, String, String>(
        // ketika user hit submit button
        onSubmitting: (context, state) {
          PopupHelper.showLoading(context);
        },
        // ketika validasi berhasil / hit api berhasil
        onSuccess: (context, state) {
          Navigator.pop(context);
          ToastHelper.showBasicToast(context, "${state.successResponse}");
          Navigator.pop(context);
          KycBankUserHelper.dokumen = BankUser();
        },
        // ketika terjadi kesalahan (validasi / hit api)
        onFailure: (context, state) {
          if (KycBankUserHelper.dokumen.isSubmitted != null) {
            Navigator.pop(context);
          }
          if (state.failureResponse == "UNAUTHORIZED") {
            NavigatorHelper.pushToExpiredSession(context);
          }
          if (state.failureResponse.toUpperCase().contains("ERRORSUBMIT")) {
            ToastHelper.showFailureToast(
              context,
              state.failureResponse.replaceAll("ERRORSUBMIT", ""),
            );
          } else {
            switch (state.failureResponse) {
              // CASE = identifier dari emitFailure, tiap value menandakan error pada field sesuai casenya
              // fieldWrapper.scrollTo(x), x = identifier / key widget yang ada di fieldWrapper.viewWidget
              case "name1":
                fieldWrapper.scrollTo(0);
                break;
              case "bank1":
                fieldWrapper.scrollTo(0);
                break;
              case "account1":
                fieldWrapper.scrollTo(0);
                break;
              case "name2":
                fieldWrapper.scrollTo(1);
                break;
              case "bank2":
                fieldWrapper.scrollTo(1);
                break;
              case "account2":
                fieldWrapper.scrollTo(1);
                break;
              case "name3":
                fieldWrapper.scrollTo(2);
                break;
              case "bank3":
                fieldWrapper.scrollTo(2);
                break;
              case "account3":
                fieldWrapper.scrollTo(2);
                break;
              default:
                break;
            }
          }
        },
        child: BlocBuilder<BankUserBloc, FormBlocState>(
          buildWhen: (previous, current) =>
              previous.runtimeType != current.runtimeType ||
              previous is FormBlocLoading && current is FormBlocLoading,
          builder: (context, state) {
            if (state is FormBlocLoading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is FormBlocLoadFailed) {
              return Center(
                child: Text("Failed to load!"),
              );
            } else {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                padding: EdgeInsets.all(Sizes.s20),
                child: ListView(
                  scrollDirection: scrollDirection,
                  controller: controller,
                  children: [
                    fieldWrapper.viewWidget(
                      0,
                      null,
                      Theme(
                          data: theme,
                          child: ListTileTheme(
                            contentPadding: EdgeInsets.all(0),
                            child: ExpansionTile(
                              backgroundColor: Colors.transparent,
                              initiallyExpanded: true,
                              // tilePadding: EdgeInsets.all(0),
                              // childrenPadding: EdgeInsets.all(0),
                              title: Text(
                                "Akun Bank",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSize.s16,
                                ),
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: Sizes.s5),
                                    color: Color(0xffF4F4F4),
                                    height: 3,
                                    width: double.maxFinite,
                                  ),
                                  SizedBox(
                                    height: Sizes.s20,
                                  ),
                                ],
                              ),
                              children: [
                                SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.name1,
                                  hintText: "Contoh : Putri Aisyah",
                                  labelText: "Nama Pemilik Rekening",
                                ),
                                BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                  bloc: bloc.bank1,
                                  builder: (context, state) {
                                    return state.extraData.isError
                                        ? _reload(() {
                                            bloc.fetchBanks();
                                          })
                                        : DropdownFieldBlocBuilder<
                                            BankInvestor>(
                                            selectFieldBloc: bloc.bank1,
                                            isEnabled: KycHelpers.fieldCheck(
                                                    bloc.status)
                                                ? state.extraData.isActive ??
                                                    true
                                                : false,
                                            decoration: InputDecoration(
                                              errorMaxLines: 5,
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              labelText: "Nama Bank",
                                              suffixIcon: state
                                                      .extraData.isLoaded
                                                  ? Icon(Icons.arrow_drop_down)
                                                  : CupertinoActivityIndicator(),
                                            ),
                                            itemBuilder: (context, value) =>
                                                value != null ? value.bank : "",
                                          );
                                  },
                                ),
                                SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.account1,
                                  inputType: TextInputType.number,
                                  hintText: "Contoh : 1122334455667788",
                                  labelText: "Nomor Rekening",
                                ),
                              ],
                            ),
                          )),
                    ),
                    fieldWrapper.divider,
                    fieldWrapper.viewWidget(
                      1,
                      null,
                      Theme(
                        data: theme,
                        child: ListTileTheme(
                          contentPadding: EdgeInsets.all(0),
                          child: ExpansionTile(
                            // tilePadding: EdgeInsets.all(0),
                            // childrenPadding: EdgeInsets.all(0),
                            title: Text(
                              "Akun Bank 2",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Kosongkan Jika tidak memiliki Akun Bank lain (Optional)",
                                  style: TextStyle(
                                    fontSize: FontSize.s12,
                                    color: Colors.grey,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: Sizes.s5),
                                  color: Color(0xffF4F4F4),
                                  height: 3,
                                  width: double.maxFinite,
                                ),
                                SizedBox(
                                  height: Sizes.s20,
                                ),
                              ],
                            ),
                            children: [
                              SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.name2,
                                  hintText: "Contoh : Putri Aisyah",
                                  labelText: "Nama Pemilik Rekening"),
                              BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                bloc: bloc.bank2,
                                builder: (context, state) {
                                  return state.extraData.isError
                                      ? _reload(() {
                                          bloc.fetchBanks();
                                        })
                                      : DropdownFieldBlocBuilder<BankInvestor>(
                                          selectFieldBloc: bloc.bank2,
                                          isEnabled:
                                              KycHelpers.fieldCheck(bloc.status)
                                                  ? state.extraData.isActive ??
                                                      true
                                                  : false,
                                          decoration: InputDecoration(
                                            errorMaxLines: 5,
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5))),
                                            labelText: "Nama Bank",
                                            suffixIcon: state.extraData.isLoaded
                                                ? Icon(Icons.arrow_drop_down)
                                                : CupertinoActivityIndicator(),
                                          ),
                                          itemBuilder: (context, value) =>
                                              value != null ? value.bank : "",
                                        );
                                },
                              ),
                              SantaraBlocTextField(
                                enabled: KycHelpers.fieldCheck(bloc.status),
                                textFieldBloc: bloc.account2,
                                inputType: TextInputType.number,
                                hintText: "Contoh : 1122334455667788",
                                labelText: "Nomor Rekening",
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: RaisedButton.icon(
                                  onPressed: () => bloc.clearBank2(),
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  label: Text(
                                    "Hapus",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    fieldWrapper.divider,
                    fieldWrapper.viewWidget(
                      2,
                      null,
                      Theme(
                        data: theme,
                        child: ListTileTheme(
                          contentPadding: EdgeInsets.all(0),
                          child: ExpansionTile(
                              // tilePadding: EdgeInsets.all(0),
                              // childrenPadding: EdgeInsets.all(0),
                              title: Text(
                                "Akun Bank 3",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSize.s16,
                                ),
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Kosongkan Jika tidak memiliki Akun Bank lain (Optional)",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: Sizes.s5),
                                    color: Color(0xffF4F4F4),
                                    height: 3,
                                    width: double.maxFinite,
                                  ),
                                  SizedBox(
                                    height: Sizes.s20,
                                  ),
                                ],
                              ),
                              children: [
                                SantaraBlocTextField(
                                    enabled: KycHelpers.fieldCheck(bloc.status),
                                    textFieldBloc: bloc.name3,
                                    hintText: "Contoh : Putri Aisyah",
                                    labelText: "Nama Pemilik Rekening"),
                                BlocBuilder<SelectFieldBloc, FieldBlocState>(
                                  bloc: bloc.bank3,
                                  builder: (context, state) {
                                    return state.extraData.isError
                                        ? _reload(() {
                                            bloc.fetchBanks();
                                          })
                                        : DropdownFieldBlocBuilder<
                                            BankInvestor>(
                                            selectFieldBloc: bloc.bank3,
                                            isEnabled: KycHelpers.fieldCheck(
                                                    bloc.status)
                                                ? state.extraData.isActive ??
                                                    true
                                                : false,
                                            decoration: InputDecoration(
                                              errorMaxLines: 5,
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(5))),
                                              labelText: "Nama Bank",
                                              suffixIcon: state
                                                      .extraData.isLoaded
                                                  ? Icon(Icons.arrow_drop_down)
                                                  : CupertinoActivityIndicator(),
                                            ),
                                            itemBuilder: (context, value) =>
                                                value != null ? value.bank : "",
                                          );
                                  },
                                ),
                                SantaraBlocTextField(
                                  enabled: KycHelpers.fieldCheck(bloc.status),
                                  textFieldBloc: bloc.account3,
                                  inputType: TextInputType.number,
                                  hintText: "Contoh : 1122334455667788",
                                  labelText: "Nomor Rekening",
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: RaisedButton.icon(
                                    onPressed: () => bloc.clearBank3(),
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                    label: Container(
                                      child: Text(
                                        "Hapus",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                      ),
                    ),
                    fieldWrapper.divider,
                    !KycHelpers.fieldCheck(bloc.status)
                        ? SantaraDisabledButton()
                        : SantaraMainButton(
                            onPressed: () {
                              if (bloc.status == "verified") {
                                PopupHelper.showConfirmationDataChange(context,
                                    () {
                                  Navigator.pop(context);
                                  KycBankUserHelper.dokumen.isSubmitted = true;
                                  FocusScope.of(context).unfocus();
                                  bloc.submit();
                                });
                              } else {
                                KycBankUserHelper.dokumen.isSubmitted = true;
                                FocusScope.of(context).unfocus();
                                bloc.submit();
                              }
                            },
                            title: "Simpan",
                          )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
