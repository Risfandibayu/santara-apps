import 'package:flutter/foundation.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/helpers/kyc/BankPribadiHelper.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/models/KycFieldStatus.dart';
import 'package:santaraapp/models/Validation.dart';
import 'package:santaraapp/models/kyc/KycFieldStatus.dart';
import 'package:santaraapp/models/kyc/pribadi/BankModel.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:rxdart/rxdart.dart';

class BankUserBloc extends FormBloc<String, String> {
  final KycPersonalService _service = KycPersonalService();
  List<BankInvestor> banks = List<BankInvestor>();
  final bool isEdit;
  final String submissionId;
  final String status;
  BankModel dataBank = BankModel();
  // FIELD 1
  final bank1 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name1 = TextFieldBloc();
  final account1 = TextFieldBloc();
  // FIELD 2
  final bank2 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name2 = TextFieldBloc();
  final account2 = TextFieldBloc();
  // FIELD 3
  final bank3 = SelectFieldBloc<BankInvestor, FieldStatus>();
  final name3 = TextFieldBloc();
  final account3 = TextFieldBloc();

  BankUserBloc({
    this.isEdit = false,
    @required this.submissionId,
    @required this.status,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      bank1,
      name1,
      account1,
      bank2,
      name2,
      account2,
      bank3,
      name3,
      account3,
    ]);
    fetchBanks().then((value) async {
      await getDataBank().then((value) async {
        await Future.delayed(Duration(milliseconds: 100));
        onFieldsChange();
        updateAllFields();
      });
    });
  }

  updateAllFields() {
    // UPDATE TEXTFIELDS
    updateTextFieldStatus("account_name1", name1);
    updateTextFieldStatus("account_name2", name2);
    updateTextFieldStatus("account_name3", name3);
    updateTextFieldStatus("account_number1", account1);
    updateTextFieldStatus("account_number2", account2);
    updateTextFieldStatus("account_number3", account3);
    // UPDATE DROPDOWN BUTTON
    updateFieldStatus("bank_investor1", bank1);
    updateFieldStatus("bank_investor2", bank2);
    updateFieldStatus("bank_investor3", bank3);
  }

  // UPDATE FIELD JIKA USER MELAKUKAN EDITING
  void updateTextFieldStatus(String field, TextFieldBloc bloc) {
    bloc
        .debounceTime(Duration(milliseconds: 1000))
        .where((field) =>
            field.value != null &&
            field.value.isNotEmpty &&
            field.value.toString().length > 1)
        .distinct()
        .listen((event) async {
      await _service.updateFieldStatus(submissionId, field, "6", 1);
    });
  }

  void updateFieldStatus(String field, SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (previous, current) async* {
      _service.updateFieldStatus(submissionId, field, "6", 1);
    });
  }

  onFieldsChange() {
    detectChanges(bank1);
    detectChanges(name1);
    detectChanges(account1);
    detectChanges(bank2);
    detectChanges(name2);
    detectChanges(account2);
    detectChanges(bank3);
    detectChanges(name3);
    detectChanges(account3);
  }

  detectChanges(SingleFieldBloc bloc) {
    bloc.onValueChanges(onData: (data, v) async* {
      KycBankUserHelper.dokumen.isEdited = true;
    });
  }

  Future getDataBank() async {
    emitLoading();
    try {
      await _service.getBankData().then((value) {
        dataBank = BankModel.fromJson(value.data);
        getFieldErrorStatus();
        // update value setiap field
        // bank 1
        if (dataBank.bank1 != null) {
          var userBank1 = bank1.state.items.where((element) =>
              element.bank.toLowerCase() == dataBank.bank1.toLowerCase());
          // nama bank
          bank1.updateInitialValue(userBank1.first);
          // nama akun pemilik bank
          name1.updateInitialValue(dataBank.accountName1);
          // nomor akun pemilik bank
          account1.updateInitialValue(dataBank.accountNumber1);
        }
        // bank 2
        if (dataBank.bank2 != null) {
          var userBank2 = bank2.state.items.where((element) =>
              element.bank.toLowerCase() == dataBank.bank2.toLowerCase());
          // nama bank
          bank2.updateInitialValue(userBank2.first);
          // nama akun pemilik bank
          name2.updateInitialValue(dataBank.accountName2);
          // nomor akun pemilik bank
          account2.updateInitialValue(dataBank.accountNumber2);
        }

        if (dataBank.bank3 != null) {
          // bank 3
          var userBank3 = bank3.state.items.where((element) =>
              element.bank.toLowerCase() == dataBank.bank3.toLowerCase());
          // nama bank
          bank3.updateInitialValue(userBank3.first);
          // nama akun pemilik bank
          name3.updateInitialValue(dataBank.accountName3);
          // nomor akun pemilik bank
          account3.updateInitialValue(dataBank.accountNumber3);
        }
      });
    } catch (e) {
      print(">> FAILED BANK $e");
      emitLoadFailed(failureResponse: "ERRORSUBMIT Tidak dapat memuat data!");
    }
  }

  getFieldErrorStatus() async {
    try {
      List<FieldErrorStatus> errorStatus = List<FieldErrorStatus>();
      await _service.getErrorStatus(submissionId, "6").then((value) {
        logger.i(value.data);
        if (value.statusCode == 200) {
          // emit / update state ke loaded
          emitLoaded();
          // push value error tiap field kedalam errorStatus
          value.data.forEach((val) {
            var dummy = FieldErrorStatus.fromJson(val);
            errorStatus.add(dummy);
          });
          // jika error status > 0
          if (errorStatus.length > 0) {
            // loop tiap value
            errorStatus.forEach((element) {
              // jika value memiliki pesan error
              if (element.status == 0) {
                // maka kirim pesan error ke setiap field
                handleApiValidation(element.fieldId, element.error);
              }
            });
          }
        } else if (value.statusCode == 401) {
          emitFailure(failureResponse: "UNAUTHORIZED");
        } else {
          print(">> HELLO");
          emitFailure(
              failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
        }
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      // ERRORSUBMIT (UNTUK NANDAIN ERROR)
      print(">> HELLO there");
      emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat menerima data!");
    }
  }

  bool isBankError(SingleFieldBloc bloc, bool mandatory) {
    if (bloc.value == null) {
      if (mandatory) {
        bloc.addFieldError("Bank tidak boleh kosong!");
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool isNameError(SingleFieldBloc bloc, bool mandatory,
      {SelectFieldBloc bankBloc}) {
    if (bankBloc != null && bankBloc.value != null) {
      mandatory = true;
    }

    if (mandatory) {
      if (bloc.value != null) {
        if (bloc.value.length < 1) {
          bloc.addFieldError("Nama tidak boleh kosong!");
          return true;
        } else if (bloc.value.length > 60) {
          bloc.addFieldError("Nama maksimal 60 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        bloc.addFieldError("Nama tidak boleh kosong!");
        return true;
      }
    } else {
      if (bloc != null && bloc.value != null) {
        if (bloc.value.length > 60) {
          bloc.addFieldError("Nama maksimal 60 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  bool isAccountError(SingleFieldBloc bloc, bool mandatory,
      {SelectFieldBloc bankBloc}) {
    if (bankBloc != null && bankBloc.value != null) {
      mandatory = true;
    }

    if (mandatory) {
      if (bloc.value != null) {
        if (bloc.value.length < 1) {
          bloc.addFieldError("Nomor rekening tidak boleh kosong!");
          return true;
        } else if (bloc.value.length > 60) {
          bloc.addFieldError("Nomor rekening maksimal 60 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        bloc.addFieldError("Nomor rekening tidak boleh kosong!");
        return true;
      }
    } else {
      if (bloc.value != null && bloc.value != null) {
        if (bloc.value.length > 60) {
          bloc.addFieldError("Nomor rekening maksimal 60 karakter!");
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  Future fetchBanks() async {
    // update extra data, karena saat ini sedang memuat data, maka status aktif & loaded = false
    bank1.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    bank2.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    bank3.updateExtraData(
      FieldStatus(isActive: false, isLoaded: false),
    );
    try {
      await _service.getBanks().then((value) {
        // jika nilai tidak null
        if (value != null) {
          // jika respons dari server OK
          if (value.statusCode == 200) {
            // embed value negara kedalam bloc countries
            value.data.forEach((val) {
              var dummy = BankInvestor.fromJson(val);
              banks.add(dummy);
              bank1.addItem(dummy);
              bank2.addItem(dummy);
              bank3.addItem(dummy);
            });
            // update extra data, akitf = true, isloaded = true, error = tidak error
            bank1.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
            bank2.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
            bank3.updateExtraData(
              FieldStatus(isActive: true, isLoaded: true, isError: false),
            );
          } else if (value.statusCode == 401) {
            emitFailure(failureResponse: "UNAUTHORIZED");
          } else {
            // jika respons dari server 500 / error
            bank1.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
            bank2.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
            bank3.updateExtraData(
              FieldStatus(isActive: false, isLoaded: true, isError: true),
            );
          }
        } else {
          // jika ada error
          bank1.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
          bank2.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
          bank3.updateExtraData(
            FieldStatus(isActive: false, isLoaded: true, isError: true),
          );
        }
      });
    } catch (e) {
      // jika ada erro lagi
      bank1.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
      bank2.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
      bank3.updateExtraData(
        FieldStatus(isActive: false, isLoaded: true, isError: true),
      );
    }
  }

  addErrorMessage(SingleFieldBloc bloc, String message, String emit) {
    bloc.addFieldError(message);
    emitFailure(failureResponse: emit);
  }

  handleApiValidation(String field, String _errMsg) {
    switch (field) {
      // BANK 1
      case "account_name1":
        addErrorMessage(name1, _errMsg, "name1");
        break;
      case "bank_investor1":
        addErrorMessage(bank1, _errMsg, "bank1");
        break;
      case "account_number1":
        addErrorMessage(account1, _errMsg, "account1");
        break;
      // BANK 2
      case "account_name2":
        addErrorMessage(name2, _errMsg, "name2");
        break;
      case "bank_investor2":
        addErrorMessage(bank2, _errMsg, "bank2");
        break;
      case "account_number2":
        addErrorMessage(account2, _errMsg, "account2");
        break;
      // BANK 3
      case "account_name3":
        addErrorMessage(name3, _errMsg, "name3");
        break;
      case "bank_investor3":
        addErrorMessage(bank3, _errMsg, "bank3");
        break;
      case "account_number3":
        addErrorMessage(account3, _errMsg, "account3");
        break;
      default:
        emitFailure(failureResponse: "ERRORSUBMIT Tidak dapat mengirim data!");
        break;
    }
  }

  @override
  void onLoading() {
    // if (!isEdit) emitLoaded();
  }

  clearBank2() {
    name2.clear();
    bank2.clear();
    account2.clear();
  }

  clearBank3() {
    name3.clear();
    bank3.clear();
    account3.clear();
  }

  @override
  void onSubmitting() async {
    if (isNameError(name1, true)) {
      emitFailure(failureResponse: "name1");
    } else if (isBankError(bank1, true)) {
      emitFailure(failureResponse: "bank1");
    } else if (isAccountError(account1, true)) {
      emitFailure(failureResponse: "account1");
    } else if (isNameError(name2, false, bankBloc: bank2)) {
      emitFailure(failureResponse: "name2");
    } else if (isBankError(bank2, false)) {
      emitFailure(failureResponse: "bank2");
    } else if (isAccountError(account2, false, bankBloc: bank2)) {
      emitFailure(failureResponse: "account2");
    } else if (isNameError(name3, false, bankBloc: bank3)) {
      emitFailure(failureResponse: "name3");
    } else if (isBankError(bank3, false)) {
      emitFailure(failureResponse: "bank3");
    } else if (isAccountError(account3, false, bankBloc: bank3)) {
      emitFailure(failureResponse: "account3");
    } else {
      try {
        BankUser data = BankUser(
          // bank 1
          accountName1: name1?.value ?? '',
          bankInvestor1: bank1?.value?.id ?? '',
          accountNumber1: account1?.value ?? '',
          // bank 2
          accountName2: name2?.value ?? '',
          bankInvestor2: bank2?.value != null ? bank2.value.id : null,
          accountNumber2: account2?.value,
          // bank 3
          accountName3: name3?.value ?? '',
          bankInvestor3: bank3?.value != null ? bank3.value.id : null,
          accountNumber3: account3?.value,
        );

        if (isEdit) {
          await _service.updateBanks(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                // print(">> RESPONSE : ");
                // print(value.data);
                emitSuccess(successResponse: "Berhasil menyimpan Bank");
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else {
                emitFailure(failureResponse: "ERRORSUBMIT ${value.statusCode}");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        } else {
          await _service.sendBanks(data).then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                // print(">> RESPONSE : ");
                // print(value.data);
                emitSuccess(successResponse: "Berhasil menyimpan Bank");
              } else if (value.statusCode == 401) {
                emitFailure(failureResponse: "UNAUTHORIZED");
              } else if (value.statusCode == 400) {
                try {
                  var dummy = ValidationResultModel.fromJson(value.data);
                  handleApiValidation(dummy.field, dummy.message);
                } catch (e) {
                  emitFailure(
                    failureResponse: "ERRORSUBMIT ${value.data['message']}",
                  );
                }
              } else {
                emitFailure(failureResponse: "ERRORSUBMIT ${value.statusCode}");
              }
            } else {
              emitFailure(
                failureResponse:
                    "ERRORSUBMIT Error timeout, periksa koneksi anda!",
              );
            }
          });
        }
      } catch (e, stack) {
        // print(">> STACK : ");
        // print(stack);
        emitFailure(failureResponse: "ERRORSUBMIT $e");
      }
    }
  }
}
