import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/business.profile.bloc.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/chart.bloc.dart';
import 'package:santaraapp/widget/home/VideoProfile.dart';
import 'package:santaraapp/widget/widget/components/charts/build_chart.dart';
import 'package:santaraapp/widget/widget/components/listing/HeroPhotoViewer.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBusinessWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinanceTabs.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinancialStatements.dart';

class BusinessProfile extends StatefulWidget {
  @override
  _BusinessProfileState createState() => _BusinessProfileState();
}

class _BusinessProfileState extends State<BusinessProfile>
    with SingleTickerProviderStateMixin {
  BusinessProfileBloc bloc;
  final rupiah = NumberFormat("#,##0");

  get divider {
    return Container(
      height: 3,
      color: Color(0xfffafafa),
      width: double.maxFinite,
    );
  }

  // profil bisnis
  Widget _buildBusinessProfile(Emiten emiten) {
    return Container(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "${emiten.name}",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "${emiten.bio}",
          )
        ],
      ),
    );
  }

  // tentang penerbit
  Widget _buildBusinessDetail(Emiten emiten) {
    return Container(
      padding: EdgeInsets.only(left: Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Kode Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
              ),
            ),
            subtitle: Text(
              "${emiten.codeEmiten}",
              style: TextStyle(
                fontSize: FontSize.s15,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Harga Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
              ),
            ),
            subtitle: Text(
              "Rp. ${rupiah.format(emiten.price)}",
              style: TextStyle(
                fontSize: FontSize.s15,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Total Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
              ),
            ),
            subtitle: Text(
              "${rupiah.format(emiten.supply)} Lembar",
              style: TextStyle(
                fontSize: FontSize.s15,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            ),
          ),
          ListTile(
            isThreeLine: true,
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Total Saham (Rp.)",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Rp ${rupiah.format(emiten.supply * emiten.price)}",
                  style: TextStyle(
                    fontSize: FontSize.s15,
                    fontWeight: FontWeight.w700,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "(${terbilang((emiten.supply * emiten.price).toDouble())})",
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: FontSize.s12,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: Sizes.s30)
        ],
      ),
    );
  }

  // charts
  Widget _buildCharts(Emiten emiten) {
    return BlocBuilder<FinanceChartBloc, ChartState>(
      builder: (context, state) {
        if (state is ChartUninitialized) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ChartError) {
          return Center(
            child: Text("${state.error}"),
          );
        } else if (state is ChartEmpty) {
          return Center(
            child: Container(
              padding: EdgeInsets.all(Sizes.s10),
              // color: Colors.pinkAccent,
              height: Sizes.s120,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    height: Sizes.s200,
                  ),
                  Text(
                    "Belum Ada Data",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          );
        } else if (state is ChartLoaded) {
          return BuildFinancialChart(data: state.data);
        } else {
          return Container();
        }
      },
    );
  }

  // Lokasi penerbit
  Widget _buildBusinessLocation(Emiten emiten) {
    final Set<Marker> _markers = {};
    Completer<GoogleMapController> _controller = Completer();
    _markers.add(
      Marker(
        markerId: MarkerId("${emiten.uuid}"),
        position: LatLng(emiten.latitude, emiten.longitude),
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(
          title: "${emiten.name}",
          snippet: "${emiten.companyName}",
        ),
      ),
    );
    return Padding(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.maxFinite,
            height: Sizes.s200,
            child: GoogleMap(
              mapType: MapType.normal,
              markers: _markers,
              initialCameraPosition: CameraPosition(
                target: LatLng(emiten.latitude, emiten.longitude),
                zoom: 15,
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text("${emiten.address}"),
        ],
      ),
    );
  }

  // Build informasi saham
  Widget _buildInfoSaham(Emiten emiten) {
    return Container(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextValueEmiten(
            title: "Saham Tersisa",
            value:
                "${((emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)) / emiten.supply * 100).toStringAsFixed(2)}%",
            isBold: true,
          ),
          TextValueEmiten(
            title: "Dalam Lembar",
            value:
                "${rupiah.format(emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual))} Lembar",
          ),
          TextValueEmiten(
            title: "Dalam Rupiah",
            value:
                "Rp. ${(rupiah.format((emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)) * emiten.price))}",
          ),
          SizedBox(height: Sizes.s20),
          TextValueEmiten(
            title: "Saham Terjual",
            value:
                "${((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) / emiten.supply * 100).toStringAsFixed(2)}%",
            isBold: true,
          ),
          TextValueEmiten(
            title: "Dalam Lembar",
            value:
                "${rupiah.format(emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)} Lembar",
          ),
          TextValueEmiten(
            title: "Dalam Rupiah",
            value:
                "Rp. ${(rupiah.format((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) * emiten.price))}",
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<BusinessProfileBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BusinessProfileBloc, BusinessProfileState>(
      builder: (context, state) {
        if (state is BusinessProfileUninitialized) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is BusinessProfileLoaded) {
          var emiten = state.emiten;
          double percent = num.parse(((emiten.terjual > emiten.supply
                      ? emiten.supply
                      : emiten.terjual) /
                  emiten.supply)
              .toStringAsFixed(1));
          return Container(
            width: double.maxFinite,
            height: double.maxFinite,
            color: Colors.white,
            // padding: EdgeInsets.only(left: Sizes.s25, top: Sizes.s25),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Header section (Gambar & bisnis)
                  // Emiten Image & Title
                  SizedBox(height: Sizes.s20),
                  Container(
                    padding: EdgeInsets.only(left: Sizes.s20, top: 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: Sizes.s200,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              emiten?.videoUrl != null
                                  ? emiten.videoUrl.isNotEmpty
                                      ? SantaraFinanceImage(
                                          image: YoutubeThumbnailGenerator
                                              .getThumbnail(
                                            "${emiten.videoUrl}",
                                            ThumbnailQuality.high,
                                          ),
                                          isVideo: true,
                                          onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => VideoProfile(
                                                videoUrl: "${emiten.videoUrl}",
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container()
                                  : Container(),
                              // ignore: sdk_version_ui_as_code
                              for (var i in emiten?.pictures)
                                SantaraFinanceImage(
                                  image: i.picture,
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          HeroPhotoViewWrapper(
                                        tag: "${i.picture}",
                                        imageProvider: NetworkImage(
                                          i.picture,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                        SizedBox(height: Sizes.s15),
                        businessType("${emiten.category}"),
                        businessName("${emiten.trademark}"),
                        businessCompanyName("${emiten.companyName}"),
                        SizedBox(height: Sizes.s25),
                      ],
                    ),
                  ),
                  // Progress Penjualan
                  divider,
                  Container(
                    padding: EdgeInsets.all(Sizes.s20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Dana Terkumpul",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: FontSize.s14,
                                  ),
                                ),
                                Text(
                                  "Rp. ${rupiah.format(percent * (emiten.price * emiten.supply))}",
                                  style: TextStyle(
                                    color: Color(0xff0E7E4A),
                                    fontWeight: FontWeight.w700,
                                    fontSize: FontSize.s16,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Dari Target ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.s12,
                                      ),
                                    ),
                                    Text(
                                      "Rp. ${rupiah.format(emiten.price * emiten.supply)}",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: FontSize.s12,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Periode Dividen",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: FontSize.s14,
                                  ),
                                ),
                                Text(
                                  "6 Bulan",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: FontSize.s16,
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        SantaraBusinessProgressIndicator(
                          margin: EdgeInsets.fromLTRB(
                            Sizes.s8,
                            Sizes.s15,
                            Sizes.s8,
                            Sizes.s5,
                          ),
                          lineHeight: Sizes.s20,
                          percent: percent,
                          // caption: "Sisa Waktu : 50 Hari - 100 Investor",
                          // caption: _buildCaption(data.now, data.emiten),
                          text: Text(
                            "${((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) / emiten.supply * 100) > 100.00 ? 100.00 : ((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) / emiten.supply * 100).toStringAsFixed(2)} %",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: FontSize.s12,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  divider,
                  SantaraFinanceTabs(
                    stockInfo: _buildInfoSaham(emiten),
                    stockDetail: _buildBusinessDetail(emiten),
                    about: _buildBusinessProfile(emiten),
                    financialStatement: _buildCharts(emiten),
                    location: _buildBusinessLocation(emiten),
                  )
                ],
              ),
            ),
          );
        } else if (state is BusinessProfileEmpty) {
          return Center(
            child: Container(
              padding: EdgeInsets.all(Sizes.s10),
              // color: Colors.pinkAccent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    height: Sizes.s250,
                  ),
                  Text(
                    "Emiten Tidak Ditemukan",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          );
        } else if (state is BusinessProfileError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(state.error),
              IconButton(
                icon: Icon(Icons.replay),
                onPressed: () => bloc
                  ..add(
                    LoadProfileEmiten(state.uuid),
                  ),
              )
            ],
          );
        } else {
          return Container(
            child: Text("Unknown State!"),
          );
        }
      },
    );
  }
}
