// Profil bisnis ketika membuka laporan keuangan

// Event
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';
import 'package:santaraapp/utils/logger.dart';

class BusinessProfileEvent {}

// State
class BusinessProfileState {}

// Load Emiten Event
class LoadProfileEmiten extends BusinessProfileEvent {
  final String uuid;
  LoadProfileEmiten(this.uuid);
}

// Uninitialized state (pertama kali buka halaman profile)
class BusinessProfileUninitialized extends BusinessProfileState {}

// Error state (jika ada error)
class BusinessProfileError extends BusinessProfileState {
  final String uuid; // emiten (untuk reload data jika terjadi error)
  final String error; // pesan error
  BusinessProfileError({this.uuid, this.error});
}

// Empty state (jika data tidak ditemukan (statusCode == 404) )
class BusinessProfileEmpty extends BusinessProfileState {}

// Loaded state (jika berhasil memuat data profile emiten)
class BusinessProfileLoaded extends BusinessProfileState {
  final Emiten emiten; // data emiten
  BusinessProfileLoaded({this.emiten});
}

// Bloc
class BusinessProfileBloc
    extends Bloc<BusinessProfileEvent, BusinessProfileState> {
  // Constructor
  BusinessProfileBloc(BusinessProfileState initialState) : super(initialState);
  FinanceService financeService =
      FinanceService(); // api service financial statements
  ApiService apiService = ApiService(); // api service

  @override
  Stream<BusinessProfileState> mapEventToState(
      BusinessProfileEvent event) async* {
    // jika event adalah load profile emiten
    if (event is LoadProfileEmiten) {
      try {
        // buat state menjadi uninitialized
        yield BusinessProfileUninitialized();
        // calling api (ngedapetin detail emiten)
        var result = await financeService.getBusinessProfile(event.uuid);
        // var chart = await financeService.getEmitenChart(event.uuid);
        // jika statusCode == 200 (berhasil)
        if (result.status == ApiStatus.success) {
          // parsing data
          var data = Emiten.fromJson(result.data);
          yield BusinessProfileLoaded(emiten: data);
        } else if (result.status == ApiStatus.notFound) {
          yield BusinessProfileEmpty();
        } else {
          // jika statusCode != 200 (tidak berhasil request)
          yield BusinessProfileError(
            uuid: event.uuid,
            error:
                "[${result.message}] Terjadi kesalahan, tidak dapat memuat data!",
          );
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        yield BusinessProfileError(
          uuid: event.uuid,
          error: "[03] Terjadi kesalahan!",
        );
      }
    }
  }
}
