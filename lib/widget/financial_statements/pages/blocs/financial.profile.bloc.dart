import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/FinancialStatementsStatusHelper.dart';
import 'package:santaraapp/models/FinancialStatementModel.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';

/// Digunakan Untuk tabs laporan keuangan
/// FinancialStatementsBloc.dart digunakan untuk keseluruhan data (profil, laporan keuangan, rencana penggunaan dana)
/// FinancialProfileBloc.dart digunakan untuk tab laporan keuangan saja

// BloC untuk Laporan Keuangan Terakhir

// Event
class LastFinancialStatementsEvent {}

// State
class LastFinancialStatementsState {}

// Event load data laporan keuangan terakhir
class LoadLastFinancialStatements extends LastFinancialStatementsEvent {
  final String uuid;
  LoadLastFinancialStatements(this.uuid);
}

// State untuk nandain laporan keuangan terakhir belum dimuat (pertama buka page) untuk menampilkan loading
class LastFinancialStatementsUninitialized
    extends LastFinancialStatementsState {}

// State untuk nandain error
class LastFinancialStatementsError extends LastFinancialStatementsState {
  final String uuid; // uuid emiten
  final String error; // error message
  LastFinancialStatementsError({this.uuid, this.error});
}

// State untuk nandain ketika data laporan keuangan terakhir kosong
class LastFinancialStatementsEmpty extends LastFinancialStatementsState {}

// State untuk nandain data telah dimuat
class LastFinancialStatementsLoaded extends LastFinancialStatementsState {
  final FinanceLastReportModel data;
  final FinancialStatementsStatusModel status;
  LastFinancialStatementsLoaded({this.data, this.status});
}

// Bloc logic
class LastFinancialStatementsBloc
    extends Bloc<LastFinancialStatementsEvent, LastFinancialStatementsState> {
  LastFinancialStatementsBloc(LastFinancialStatementsState initialState)
      : super(initialState); // constructor
  FinanceService financeService = FinanceService(); // service

  @override
  Stream<LastFinancialStatementsState> mapEventToState(
      LastFinancialStatementsEvent event) async* {
    // jika event adalah untuk memuat last report
    if (event is LoadLastFinancialStatements) {
      try {
        // Update state menjadi uninitialized
        yield LastFinancialStatementsUninitialized();
        // Memanggil api untuk ngedapetin data laporan keuangan terakhir
        var result =
            await financeService.getLastFinancialStatements(event.uuid);
        if (result != null) {
          // jika request berhasil dijalankan
          if (result.status == ApiStatus.success) {
            var data = FinanceLastReportModel.fromJson(result.data['data']);
            yield LastFinancialStatementsLoaded(
              data: data,
              status: parseStatus(data.reportStatus, data.reportDeadline, 0),
            );
          } else if (result.status == ApiStatus.notFound) {
            yield LastFinancialStatementsEmpty();
          } else {
            // jika statusCode != 200
            yield LastFinancialStatementsError(
              uuid: event.uuid,
              error: "[${result.message}] Tidak dapat memuat data!",
            );
          }
        } else {
          // jika resultnya null
          yield LastFinancialStatementsError(
            uuid: event.uuid,
            error: "[${result.message}] Tidak dapat memuat data!",
          );
        }
      } catch (e) {
        // jika terjadi unexpedcted error
        yield LastFinancialStatementsError(
          uuid: event.uuid,
          error: "[04] Tidak dapat memuat data!",
        );
      }
    }
  }
}
