// Digunakan untuk tab rencana penggunaan dana

// Event
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/FinancialStatementsStatusHelper.dart';
import 'package:santaraapp/models/FundsPlanModel.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';
import 'package:santaraapp/utils/logger.dart';

class FundsPlanEvent {}

// State
class FundsPlanState {}

// Event load rencana penggunaan dana
class LoadFundsPlan extends FundsPlanEvent {
  final String uuid;
  LoadFundsPlan(this.uuid);
}

// State ketika pertama buka page (uninitialized)
class FundsPlanUninitialized extends FundsPlanState {}

// State ketika terjadi error saat memuat rencana penggunaan dana
class FundsPlanError extends FundsPlanState {
  final String uuid;
  final String error;
  FundsPlanError({this.uuid, this.error});
}

// State ketika berhasil memuat rencana penggunaan dana
class FundsPlanLoaded extends FundsPlanState {
  final FundsPlanModel data;
  final FinancialStatementsStatusModel status;
  FundsPlanLoaded({this.data, this.status});
}

// State ketika data tidak ditemukan
class FundsPlanEmpty extends FundsPlanState {}

class FundsPlanBloc extends Bloc<FundsPlanEvent, FundsPlanState> {
  FundsPlanBloc(FundsPlanState initialState) : super(initialState);
  FinanceService financeService = FinanceService();

  @override
  Stream<FundsPlanState> mapEventToState(FundsPlanEvent event) async* {
    // jika event adalah untuk memuat last report
    if (event is LoadFundsPlan) {
      try {
        // Update state menjadi uninitialized
        yield FundsPlanUninitialized();
        // Memanggil api untuk ngedapetin data rencana penggunaan data
        var result = await financeService.getFundsPlan(event.uuid);
        if (result != null) {
          // jika request berhasil dijalankan
          if (result.status == ApiStatus.success) {
            var data = FundsPlanModel.fromJson(result.data['data']);
            // nambahin subtotal ke akhir sublist
            data.listFundPlans.forEach((val) {
              int subtotal = 0;
              val.sublist.forEach((tot) {
                subtotal += tot.amount;
              });
              val.sublist.add(Sublist(desc: "Subtotal", amount: subtotal));
            });
            yield FundsPlanLoaded(
              data: data,
              status: parseStatus(data.planStatus, data.planDeadline, 1),
            );
          } else if (result.status == ApiStatus.notFound) {
            yield FundsPlanEmpty();
          } else {
            // jika statusCode != 200
            yield FundsPlanError(
              uuid: event.uuid,
              error: "[${result.message}] Tidak dapat memuat data!",
            );
          }
        } else {
          // jika resultnya null
          yield FundsPlanError(
            uuid: event.uuid,
            error: "[${result.message}] Tidak dapat memuat data!",
          );
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        // jika terjadi unexpedcted error
        yield FundsPlanError(
          uuid: event.uuid,
          error: "[05] Tidak dapat memuat data!",
        );
      }
    }
  }
}
