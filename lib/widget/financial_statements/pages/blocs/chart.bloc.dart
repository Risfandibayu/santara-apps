// Untuk chart di profil bisnis

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';
import 'package:santaraapp/utils/logger.dart';

class ChartEvent {}

class ChartState {}

// Event memuat chart
class LoadChart extends ChartEvent {
  final String uuid;
  LoadChart(this.uuid);
}

// State ketika chart belum dimuat
class ChartUninitialized extends ChartState {}

// State ketika chart error
class ChartError extends ChartState {
  final String uuid;
  final String error;
  ChartError({this.uuid, this.error});
}

// State ketika chart loaded / dimuat
class ChartLoaded extends ChartState {
  final SahamData data;
  ChartLoaded({this.data});
}

// State ketika chart masih kosong
class ChartEmpty extends ChartState {}

class FinanceChartBloc extends Bloc<ChartEvent, ChartState> {
  FinanceChartBloc(ChartState initialState) : super(initialState);
  FinanceService apiService = FinanceService();

  @override
  Stream<ChartState> mapEventToState(ChartEvent event) async* {
    if (event is LoadChart) {
      try {
        var result = await apiService.getEmitenChart(event.uuid);
        if (result.status == ApiStatus.success) {
          SahamData data = SahamData();
          var resp = result.data;
          data = SahamData.fromJson(resp['data']);
          yield ChartLoaded(data: data);
        } else if (result.status == ApiStatus.notFound) {
          yield ChartEmpty();
        } else {
          yield ChartError(
            uuid: event.uuid,
            error: "[${result.message}] Tidak dapat memuat data finansial",
          );
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        yield ChartError(uuid: event.uuid, error: e);
      }
    }
  }
}
