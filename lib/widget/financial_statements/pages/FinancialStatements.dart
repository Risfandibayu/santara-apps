import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../helpers/PopupHelper.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../utils/sizes.dart';
import '../../portfolio/blocs/portfolio.financial.statement.bloc.dart';
import '../../widget/components/main/SantaraBusinessWidget.dart';
import '../../widget/components/main/SantaraFinancialStatements.dart';
import '../widgets/financial_report_history_widget.dart';
import 'blocs/financial.profile.bloc.dart';

class ListItem {
  final int value;
  final String name;
  ListItem(this.value, this.name);
}

class FinancialStatements extends StatefulWidget {
  @override
  _FinancialStatementsState createState() => _FinancialStatementsState();
}

class _FinancialStatementsState extends State<FinancialStatements> {
  final rupiah = NumberFormat("#,##0");
  LastFinancialStatementsBloc bloc;
  PortfolioFinancialStatementBloc financialStatementBloc;

  // riwayat laporan keuangan
  final DateFormat dateFormatFinancial = DateFormat("dd/MM/yyyy", "id");

  get divider {
    return Container(
      height: 3,
      color: Color(0xfffafafa),
      width: double.maxFinite,
    );
  }

  void initState() {
    super.initState();
    bloc = BlocProvider.of<LastFinancialStatementsBloc>(context);
    financialStatementBloc =
        BlocProvider.of<PortfolioFinancialStatementBloc>(context);
  }

  // Laporan keuangan terakhir
  Widget _buildLastFinancialStatements() {
    return BlocBuilder<LastFinancialStatementsBloc,
        LastFinancialStatementsState>(
      builder: (context, state) {
        if (state is LastFinancialStatementsUninitialized) {
          return Container(
            height: Sizes.s150,
            child: Center(
              child: CupertinoActivityIndicator(),
            ),
          );
        } else if (state is LastFinancialStatementsLoaded) {
          return Container(
            padding: EdgeInsets.all(Sizes.s20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                state.status != null
                    ? SantaraFinancialStatementsStatus(
                        color: state.status.color,
                        title: "${state.status.title}",
                        subtitle: "${state.status.subtitle}",
                      )
                    : Container(),
                SizedBox(height: Sizes.s20),
                Text(
                  "Laporan Keuangan Terakhir",
                  style: TextStyle(
                    fontSize: FontSize.s16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: Sizes.s15, bottom: Sizes.s15),
                  height: 3,
                  color: Colors.grey[100],
                  width: double.maxFinite,
                ),
                FinanceTextRow(
                  title: "Tanggal Upload",
                  value: state?.data?.updatedAt != null
                      ? dateFormatFinancial
                          .format(DateTime.parse(state.data.updatedAt))
                      : "-",
                ),
                FinanceTextRow(
                  title: "Profit",
                  value: "Rp. ${rupiah.format(state?.data?.netIncome ?? 0)}",
                ),
                FinanceTextRow(
                  title: "Omset",
                  value: "Rp. ${rupiah.format(state?.data?.salesRevenue ?? 0)}",
                ),
                state?.data?.financeReport == null
                    ? Container()
                    : SantaraFinanceDownloadButton(
                        title: "Unduh Laporan Keuangan",
                        onTap: () => PopupHelper.popConfirmation(
                          context,
                          () async {
                            Navigator.pop(context);
                            try {
                              var url = "${state?.data?.financeReport ?? ''}";
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Cannot launch $url';
                              }
                            } catch (e) {
                              ToastHelper.showFailureToast(
                                context,
                                "Tidak dapat membuka laporan keuangan!",
                              );
                            }
                          },
                          "Unduh Laporan Keuangan",
                          "Apakah Anda yakin ingin mengunduh laporan keuangan ini ?",
                        ),
                      )
              ],
            ),
          );
        } else if (state is LastFinancialStatementsError) {
          return Column(
            children: [
              Text(state.error),
              IconButton(
                icon: Icon(Icons.replay),
                onPressed: () => bloc
                  ..add(
                    LoadLastFinancialStatements(state.uuid),
                  ),
              )
            ],
          );
        } else if (state is LastFinancialStatementsEmpty) {
          return Container(
            padding: EdgeInsets.all(Sizes.s20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Laporan Keuangan Terakhir",
                  style: TextStyle(
                    fontSize: FontSize.s16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: Sizes.s15, bottom: Sizes.s15),
                  height: 3,
                  color: Colors.grey[100],
                  width: double.maxFinite,
                ),
                FinanceTextRow(
                  title: "Tanggal Upload",
                  value: "-",
                ),
                FinanceTextRow(
                  title: "Profit",
                  value: "-",
                ),
                FinanceTextRow(
                  title: "Omset",
                  value: "-",
                ),
              ],
            ),
          );
        } else {
          return Container(
            child: Text("Unknown State!"),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildLastFinancialStatements(),
            divider,
            // Configuration Message
            ListTile(
              contentPadding: EdgeInsets.all(Sizes.s20),
              leading: Icon(
                Icons.info,
                color: Colors.orange,
                size: Sizes.s30,
              ),
              title: Text(
                "Konfigurasi pembuatan laporan keuangan",
                style: TextStyle(
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w700,
                ),
              ),
              subtitle: Text(
                "Silahkan mengakses halaman ini melalui Website menggunakan PC/Laptop untuk pengalaman pengguna yang maksimal",
                style: TextStyle(
                  fontSize: FontSize.s11,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            divider,
            FinancialReportHistoryWidget(
              financialStatementBloc: financialStatementBloc,
            ),
          ],
        ),
      ),
    );
  }
}
