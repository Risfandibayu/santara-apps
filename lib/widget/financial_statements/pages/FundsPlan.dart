import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/models/FundsPlanModel.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBusinessWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinancialStatements.dart';
import 'blocs/funds.plan.bloc.dart';

class FundsPlan extends StatefulWidget {
  @override
  _FundsPlanState createState() => _FundsPlanState();
}

class _FundsPlanState extends State<FundsPlan> {
  FundsPlanBloc bloc;
  bool isEmpty = false;
  // Formatter rupiah
  final rupiah = NumberFormat("#,##0");
  final scrollController = ScrollController();

  get divider {
    return Container(
      height: 3,
      color: Color(0xfffafafa),
      width: double.maxFinite,
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<FundsPlanBloc>(context);
  }

  Widget _buildRencanaPenggunaanDana(FundsPlanLoaded data) {
    return ListView.builder(
        padding: EdgeInsets.all(Sizes.s20),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: data?.data?.listFundPlans?.length ?? 0,
        itemBuilder: (context, index) {
          int i = 0;
          ListFundPlans fundPlan = data.data.listFundPlans[index];
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(Sizes.s20),
                child: Text(
                  "${fundPlan?.desc ?? ''}",
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: FontSize.s12,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(Sizes.s20),
                child: Text(
                  "${fundPlan?.name ?? ''}",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: FontSize.s16,
                  ),
                ),
              ),
              Scrollbar(
                controller: scrollController,
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                    columns: <DataColumn>[
                      DataColumn(
                        label: Text(
                          "No",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "Keterangan",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "Nilai",
                          style: TextStyle(
                            fontSize: FontSize.s14,
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                    rows: fundPlan.sublist.map(
                      ((val) {
                        return DataRow(
                          selected: val.desc == "Subtotal" ? true : false,
                          cells: <DataCell>[
                            DataCell(
                              Text(
                                val.desc == "Subtotal" ? "" : "${i += 1}",
                              ),
                            ),
                            DataCell(
                              Text(
                                "${val.desc}",
                              ),
                            ),
                            DataCell(
                              Text(
                                "Rp. ${rupiah.format(val?.amount ?? 0)}",
                              ),
                            ),
                          ],
                        );
                      }),
                    ).toList(),
                  ),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FundsPlanBloc, FundsPlanState>(
        builder: (context, state) {
      if (state is FundsPlanUninitialized) {
        return Center(
          child: CupertinoActivityIndicator(),
        );
      } else if (state is FundsPlanError) {
        return Column(
          children: [
            Text(state.error),
            IconButton(
              icon: Icon(Icons.replay),
              onPressed: () => bloc..add(LoadFundsPlan(state.uuid)),
            )
          ],
        );
      } else if (state is FundsPlanLoaded) {
        return Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Notifikasi
                state.status != null
                    ? Padding(
                        padding: EdgeInsets.all(Sizes.s20),
                        child: SantaraFinancialStatementsStatus(
                          color: state.status.color,
                          title: "${state.status.title}",
                          subtitle: "${state.status.subtitle}",
                        ),
                      )
                    : Container(),
                // Configuration Message
                ListTile(
                  contentPadding: EdgeInsets.all(Sizes.s20),
                  leading: Icon(
                    Icons.info,
                    color: Colors.orange,
                    size: Sizes.s30,
                  ),
                  title: Text(
                    "Konfigurasi pembuatan laporan keuangan",
                    style: TextStyle(
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  subtitle: Text(
                    "Silahkan mengakses halaman ini melalui Website menggunakan PC/Laptop untuk pengalaman pengguna yang maksimal",
                    style: TextStyle(
                      fontSize: FontSize.s11,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                divider,
                // SizedBox(
                //   height: Sizes.s50,
                // ),
                state?.data?.listFundPlans != null
                    ? state.data.listFundPlans.length < 1
                        ? Center(
                            child: SantaraNoFinancialData(
                              title: "Belum Ada Rencana Penggunaan Dana",
                              subtitle:
                                  "Anda belum memiliki rencana penggunaan dana",
                            ),
                          )
                        : _buildRencanaPenggunaanDana(state)
                    : _buildRencanaPenggunaanDana(state)
              ],
            ),
          ),
        );
      } else if (state is FundsPlanEmpty) {
        return Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Configuration Message
                ListTile(
                  contentPadding: EdgeInsets.all(Sizes.s20),
                  leading: Icon(
                    Icons.info,
                    color: Colors.orange,
                    size: Sizes.s30,
                  ),
                  title: Text(
                    "Konfigurasi pembuatan laporan keuangan",
                    style: TextStyle(
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  subtitle: Text(
                    "Silahkan mengakses halaman ini melalui Website menggunakan PC/Laptop untuk pengalaman pengguna yang maksimal",
                    style: TextStyle(
                      fontSize: FontSize.s11,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                divider,
                Center(
                  child: SantaraNoFinancialData(
                    title: "Belum Ada Rencana Penggunaan Dana",
                    subtitle: "Anda belum memiliki rencana penggunaan dana",
                  ),
                )
              ],
            ),
          ),
        );
      } else {
        return Container(
          child: Text("Unknown State!"),
        );
      }
    });
  }
}
