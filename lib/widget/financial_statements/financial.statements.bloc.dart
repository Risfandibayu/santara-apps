// Digunakan untuk memuat data :
// Judul bisnis, url bisnis, status sudah membuat laporan keuangan, status sudah membuat rencana penggunaan dana

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/FinancePreloadModel.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';

// Event
class FinancialStatementsEvent {}

// State
class FinancialStatementsState {}

// Event load financial statement
class LoadFinancialStatements extends FinancialStatementsEvent {
  final String uuid;
  LoadFinancialStatements(this.uuid);
}

// Event share emiten
class ShareEmiten extends FinancialStatementsEvent {
  final String url; // emiten url
  final String title; // emiten name (nama bisnis)
  ShareEmiten({this.url, this.title});
}

// State ketika data masih kosong
class FinancialStatementsUninitialized extends FinancialStatementsState {}

// Ketika terjadi error saat memuat data
class FinancialStatementsError extends FinancialStatementsState {
  final String uuid;
  final String error;

  FinancialStatementsError({this.uuid, this.error});
}

// Ketika berhasil memuat data2 yang dibutuhkan
class FinancialStatementsLoaded extends FinancialStatementsState {
  final FinancePreloadModel data;
  FinancialStatementsLoaded({this.data});
}

// Ketika data tidak ditemukan
class FinancialStatementsEmpty extends FinancialStatementsState {}

// Bloc
class FinancialStatementsBloc
    extends Bloc<FinancialStatementsEvent, FinancialStatementsState> {
  // Constructor
  FinancialStatementsBloc(FinancialStatementsState initialState)
      : super(initialState);
  FinanceService financeService =
      FinanceService(); // api service financial statements
  ApiService apiService = ApiService(); // api service

  NetworkRequest networkRequest = NetworkRequest(); // network request service

  @override
  Stream<FinancialStatementsState> mapEventToState(
      FinancialStatementsEvent event) async* {
    if (event is LoadFinancialStatements) {
      try {
        // set state menjadi uninitialized (loading)
        yield FinancialStatementsUninitialized();
        // calling api
        var result = await financeService.getPreloadReport(event.uuid);
        // jika berhasil request data
        if (result.status == ApiStatus.success) {
          var data = FinancePreloadModel.fromJson(result.data['data']);
          yield FinancialStatementsLoaded(data: data);
        } else if (result.status == ApiStatus.notFound) {
          yield FinancialStatementsEmpty();
        } else {
          // jika gagal request data
          yield FinancialStatementsError(
            error: "[${result.message}] Tidak dapat memuat data",
            uuid: event.uuid,
          );
        }
      } catch (e) {
        // jika terjadi kesalahan ( error )
        yield FinancialStatementsError(
          error: "[02] Gagal memuat data",
          uuid: event.uuid,
        );
      }
    } else if (event is ShareEmiten) {
      // Jika event adalah share emiten
      // print(">> Share Emiten : ${event.url}");
    }
  }
}
