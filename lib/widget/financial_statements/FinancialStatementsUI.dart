import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/chart.bloc.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/funds.plan.bloc.dart';
import 'package:santaraapp/widget/portfolio/blocs/portfolio.financial.statement.bloc.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinancialStatements.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraScaffoldNoState.dart';
import 'financial.statements.bloc.dart';
import 'pages/BusinessProfile.dart';
import 'pages/FinancialStatements.dart';
import 'pages/FundsPlan.dart';
import 'pages/blocs/business.profile.bloc.dart';
import 'pages/blocs/financial.profile.bloc.dart';

class FinancialStatementsUI extends StatelessWidget {
  final String uuid;
  FinancialStatementsUI({@required this.uuid});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              FinancialStatementsBloc(FinancialStatementsUninitialized())
                ..add(LoadFinancialStatements(uuid)),
        ),
        BlocProvider(
          create: (context) =>
              FinanceChartBloc(ChartUninitialized())..add(LoadChart(uuid)),
        ),
        BlocProvider(
          create: (context) =>
              BusinessProfileBloc(BusinessProfileUninitialized())
                ..add(LoadProfileEmiten(uuid)),
        ),
        BlocProvider(
          create: (context) => LastFinancialStatementsBloc(
              LastFinancialStatementsUninitialized())
            ..add(LoadLastFinancialStatements(uuid)),
        ),
        BlocProvider(
          create: (context) => PortfolioFinancialStatementBloc(
              PortfolioFinancialStatementUninitialized())
            ..add(LoadPortfolioFinancialStatement(1, uuid, null, null)),
        ),
        BlocProvider(
          create: (context) =>
              FundsPlanBloc(FundsPlanUninitialized())..add(LoadFundsPlan(uuid)),
        ),
      ],
      child: FinancialStatementsBody(),
    );
  }
}

class FinancialStatementsBody extends StatefulWidget {
  @override
  _FinancialStatementsBodyState createState() =>
      _FinancialStatementsBodyState();
}

class _FinancialStatementsBodyState extends State<FinancialStatementsBody> {
  FinancialStatementsBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<FinancialStatementsBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FinancialStatementsBloc, FinancialStatementsState>(
      builder: (context, state) {
        if (state is FinancialStatementsUninitialized) {
          return BuildScaffoldNoState(
            content: Center(
              child: CupertinoActivityIndicator(),
            ),
          );
        } else if (state is FinancialStatementsLoaded) {
          return DefaultTabController(
            length: 3,
            child: Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text(
                  "${state.data.businessName}",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.black),
                actions: [
                  // IconButton(
                  //   icon: Icon(Icons.share),
                  //   onPressed: () => bloc
                  //     ..add(ShareEmiten(
                  //       url: state.data.url,
                  //       title: state.data.businessName,
                  //     )),
                  // )
                ],
                bottom: TabBar(
                    indicatorColor: Color((0xFFBF2D30)),
                    labelColor: Color((0xFFBF2D30)),
                    unselectedLabelColor: Colors.black,
                    isScrollable: true,
                    tabs: [
                      Tab(
                        child: IconTabBar(
                          title: "Profil",
                          isNotified: false,
                        ),
                      ),
                      Tab(
                        child: IconTabBar(
                          title: "Laporan Keuangan",
                          isNotified: state.data.notifyFinancialReport,
                        ),
                      ),
                      Tab(
                        child: IconTabBar(
                          title: "Rencana Penggunaan Dana",
                          isNotified: state.data.notifyFundsPlan,
                        ),
                      ),
                    ]),
              ),
              body: TabBarView(
                children: [
                  BusinessProfile(),
                  FinancialStatements(),
                  FundsPlan(),
                ],
              ),
            ),
          );
        } else if (state is FinancialStatementsError) {
          return BuildScaffoldNoState(
            content: Column(
              children: [
                Text(state.error),
                IconButton(
                  icon: Icon(Icons.replay),
                  onPressed: () =>
                      bloc..add(LoadFinancialStatements(state.uuid)),
                ),
              ],
            ),
          );
        } else if (state is FinancialStatementsEmpty) {
          return BuildScaffoldNoState(
              content: SantaraNoFinancialData(
            title: "Belum Ada Laporan Keuangan",
            subtitle: "Bisnis ini belum memiliki laporan keuangan.",
          ));
        } else {
          return BuildScaffoldNoState(
            content: Text("Unknown State"),
          );
        }
      },
    );
  }
}
