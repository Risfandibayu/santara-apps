import 'package:flutter/material.dart';

import '../../../core/utils/ui/screen_sizes.dart';
import '../../widget/components/main/SantaraScaffoldNoState.dart';

class FinancialReportLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(Sizes.s20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildShimmer(Sizes.s20, Sizes.s250),
          SizedBox(height: Sizes.s10),
          buildShimmer(Sizes.s5, double.maxFinite),
          SizedBox(height: Sizes.s30),
          buildShimmer(Sizes.s15, Sizes.s165),
          SizedBox(height: Sizes.s20),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: buildShimmer(Sizes.s40, double.maxFinite),
              ),
              Container(
                margin: EdgeInsets.only(left: Sizes.s10, right: Sizes.s10),
                child: Text("-"),
              ),
              Expanded(
                flex: 1,
                child: buildShimmer(Sizes.s40, double.maxFinite),
              ),
              SizedBox(
                width: Sizes.s15,
              ),
              buildShimmer(Sizes.s40, Sizes.s40),
            ],
          ),
          SizedBox(
            height: Sizes.s15,
          ),
          buildShimmer(Sizes.s300, double.maxFinite),
        ],
      ),
    );
  }
}
