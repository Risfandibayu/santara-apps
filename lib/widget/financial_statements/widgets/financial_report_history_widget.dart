import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/utils/formatter/currency_formatter.dart';
import '../../../core/utils/formatter/date_formatter.dart';
import '../../../core/utils/tools/logger.dart';
import '../../../core/utils/ui/screen_sizes.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/ToastHelper.dart';
import '../../portfolio/blocs/portfolio.financial.statement.bloc.dart';
import '../../widget/components/main/SantaraFinancialStatements.dart';
import 'financial_report_loading_widget.dart';

class FinancialReportHistoryWidget extends StatefulWidget {
  final PortfolioFinancialStatementBloc financialStatementBloc;

  FinancialReportHistoryWidget({Key key, this.financialStatementBloc})
      : super(key: key);

  @override
  _FinancialReportHistoryWidgetState createState() =>
      _FinancialReportHistoryWidgetState();
}

class _FinancialReportHistoryWidgetState
    extends State<FinancialReportHistoryWidget> {
  ScrollController _scrollController;
  TextEditingController _dateStart = TextEditingController();
  TextEditingController _dateEnd = TextEditingController();

  Widget get divider {
    return Container(
      height: 2,
      width: double.maxFinite,
      color: Color(0xffF4F4F4),
    );
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PortfolioFinancialStatementBloc,
        PortfolioFinancialStatementState>(
      builder: (context, state) {
        if (state is PortfolioFinancialStatementUninitialized) {
          return FinancialReportLoadingWidget();
        } else if (state is PortfolioFinancialStatementLoaded) {
          var i = 0;
          _dateStart.text = DateFormatter.dLLLyyyy(state.dateStart);
          _dateEnd.text = DateFormatter.dLLLyyyy(state.dateEnd);
          return Container(
            margin: EdgeInsets.all(Sizes.s20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Riwayat Laporan Penerbit",
                  style: TextStyle(
                    fontSize: FontSize.s16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Container(height: Sizes.s10),
                divider,
                Container(height: Sizes.s30),
                Text(
                  "Tampilkan Berdasarkan Tanggal",
                  style: TextStyle(
                    fontSize: FontSize.s12,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Container(height: Sizes.s20),
                Container(
                  height: Sizes.s50,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () async {
                            // periode awal
                            try {
                              await showMonthPicker(
                                context: context,
                                firstDate: state.firstDate,
                                lastDate: DateTime.now(),
                                initialDate: state.dateStart,
                              ).then((date) => widget.financialStatementBloc
                                ..add(SelectDateStart(dateStart: date)));
                            } catch (e, stack) {
                              santaraLog(e, stack);
                            }
                          },
                          child: Container(
                            child: TextField(
                              controller: _dateStart,
                              style: TextStyle(fontSize: FontSize.s12),
                              enabled: false,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(Sizes.s10),
                                // enabled: false,
                                border: OutlineInputBorder(),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xffDDDDDD),
                                    width: 1.5,
                                  ),
                                ),
                                labelText: 'Periode Awal',
                                suffixIcon: Icon(Icons.arrow_drop_down),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: Sizes.s15, right: Sizes.s15),
                        child: Text("-"),
                      ),
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () async {
                            // periode awal
                            try {
                              await showMonthPicker(
                                context: context,
                                firstDate: state.firstDate,
                                lastDate: DateTime.now(),
                                initialDate: state.dateEnd,
                              ).then((date) => widget.financialStatementBloc
                                ..add(SelectDateEnd(dateEnd: date)));
                            } catch (e, stack) {
                              santaraLog(e, stack);
                            }
                          },
                          child: Container(
                            child: TextField(
                              controller: _dateEnd,
                              enabled: false,
                              style: TextStyle(fontSize: FontSize.s12),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(Sizes.s10),
                                border: OutlineInputBorder(),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xffDDDDDD),
                                    width: 1.5,
                                  ),
                                ),
                                labelText: 'Periode Akhir',
                                suffixIcon: Icon(Icons.arrow_drop_down),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: Sizes.s15),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Sizes.s5),
                          color: Color(0xffBF2D30),
                        ),
                        child: IconButton(
                          icon: Icon(
                            Icons.filter_list,
                            color: Colors.white,
                          ),
                          onPressed: () => widget.financialStatementBloc
                            ..add(
                              LoadPortfolioFinancialStatement(
                                0,
                                state.uuid,
                                state.dateStart,
                                state.dateEnd,
                              ),
                            ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(height: Sizes.s20),
                state?.datas?.length == null || state.datas.length < 1
                    ? SantaraNoFinancialData(
                        title: "Belum Ada Laporan Keuangan",
                        subtitle:
                            "Bisnis ini belum memiliki laporan keuangan.Anda akan menerima laporan keuangan secara berkala setiap bulan",
                      )
                    : Scrollbar(
                        controller: _scrollController,
                        isAlwaysShown: true,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                            columns: <DataColumn>[
                              DataColumn(
                                label: Text(
                                  "No",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "Periode",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "Sales Revenue",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "Net Income",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "Status",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ],
                            rows: state.datas.map(
                              ((val) {
                                return DataRow(
                                  cells: <DataCell>[
                                    DataCell(
                                      Text(
                                        "${i += 1}",
                                      ),
                                    ),
                                    DataCell(Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "${val.periode}",
                                        ),
                                        Container(width: 20),
                                        Container(
                                            height: Sizes.s30,
                                            width: Sizes.s40,
                                            child: FlatButton(
                                              padding: EdgeInsets.zero,
                                              disabledColor: Color(0xFFB8B8B8),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              child: Center(
                                                  child: Image.asset(
                                                      "assets/icon/download.png")),
                                              color: Color(0xffBF2D30),
                                              textColor: Colors.white,
                                              onPressed: val.status !=
                                                      "Terverifikasi"
                                                  ? null
                                                  : () {
                                                      PopupHelper
                                                          .popConfirmation(
                                                        context,
                                                        () async {
                                                          Navigator.pop(
                                                              context);
                                                          try {
                                                            var url =
                                                                "${val?.financeReport ?? ''}";
                                                            if (await canLaunch(
                                                                url)) {
                                                              await launch(url);
                                                            } else {
                                                              throw 'Cannot launch $url';
                                                            }
                                                          } catch (e) {
                                                            ToastHelper
                                                                .showFailureToast(
                                                              context,
                                                              "Tidak dapat mengunduh laporan keuangan!",
                                                            );
                                                          }
                                                        },
                                                        "Unduh Laporan Keuangan",
                                                        "Apakah Anda yakin ingin mengunduh laporan keuangan ini ?",
                                                      );
                                                    },
                                            )),
                                      ],
                                    )),
                                    DataCell(
                                      Text(
                                        val.periode
                                                    .toLowerCase()
                                                    .contains("insidentil") ||
                                                val.periode
                                                    .toLowerCase()
                                                    .contains("tahunan")
                                            ? "-"
                                            : val?.salesRevenue != null
                                                ? CurrencyFormatter.format(
                                                    val?.salesRevenue ?? 0)
                                                : "-",
                                      ),
                                    ),
                                    DataCell(
                                      Text(
                                        val.periode
                                                    .toLowerCase()
                                                    .contains("insidentil") ||
                                                val.periode
                                                    .toLowerCase()
                                                    .contains("tahunan")
                                            ? "-"
                                            : val?.netIncome != null
                                                ? CurrencyFormatter.format(
                                                    val?.netIncome ?? 0)
                                                : "-",
                                      ),
                                    ),
                                    DataCell(
                                      Text(val.status),
                                    ),
                                  ],
                                );
                              }),
                            ).toList(),
                          ),
                        ),
                      )
              ],
            ),
          );
        } else if (state is PortfolioFinancialStatementError) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(state.error),
              IconButton(
                icon: Icon(Icons.replay),
                onPressed: () => widget.financialStatementBloc
                  ..add(
                    LoadPortfolioFinancialStatement(
                      0,
                      state.uuid,
                      state.dateStart,
                      state.dateEnd,
                    ),
                  ),
              ),
            ],
          );
        } else if (state is PortfolioFinancialStatementEmpty) {
          return SantaraNoFinancialData(
            title: "Belum Ada Laporan Keuangan",
            subtitle:
                "Bisnis ini belum memiliki laporan keuangan.Anda akan menerima laporan keuangan secara berkala setiap bulan",
          );
        } else {
          return Container(
            child: Text("Unknown state!"),
          );
        }
      },
    );
  }
}
