import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class BubbleChat extends StatefulWidget {
  final detail;
  final date;
  final isMe;
  final email;
  final traderId;
  BubbleChat({this.detail, this.date, this.isMe, this.email, this.traderId});
  @override
  _BubbleChatState createState() => _BubbleChatState();
}

class _BubbleChatState extends State<BubbleChat> {
  final dateFormat = DateFormat("dd-MM-yyyy hh:mm:ss");

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: widget.isMe == widget.traderId || widget.isMe == null
          ? CrossAxisAlignment.end
          : CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: widget.isMe == widget.traderId || widget.isMe == null
              ? EdgeInsets.only(left: 50, right: 20, bottom: 20)
              : EdgeInsets.only(right: 50, left: 20, bottom: 20),
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  blurRadius: .5,
                  spreadRadius: 1.0,
                  color: Colors.black.withOpacity(.12))
            ],
            color: widget.isMe == widget.traderId || widget.isMe == null
                ? Color(0xFFB2FFB0)
                : Color(0xFFF3F3F3),
            borderRadius: widget.isMe == widget.traderId || widget.isMe == null
                ? BorderRadius.only(
                    topLeft: Radius.circular(5.0),
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(10.0),
                  )
                : BorderRadius.only(
                    topRight: Radius.circular(5.0),
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(5.0),
                  ),
          ),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 48.0),
                child: Linkify(
                  onOpen: (link) async {
                    if (await canLaunch(link.url)) {
                      await launch(link.url);
                    } else {
                      throw 'Could not launch $link';
                    }
                  },
                  text: widget.detail,
                  linkStyle: TextStyle(color: Colors.blue),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
