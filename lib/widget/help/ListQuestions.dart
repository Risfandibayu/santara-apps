import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/models/HelpTicket.dart';
import 'package:santaraapp/services/question.dart';
import 'package:shimmer/shimmer.dart';

//components
import 'ChatQuestion.dart';

class ListQuestions extends StatefulWidget {
  @override
  _ListQuestionsState createState() => _ListQuestionsState();
}

class _ListQuestionsState extends State<ListQuestions> {
  final date = DateFormat("dd-MM-yyyy HH:mm:ss");

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (Helper.check) {
    //   Helper.restartTimer(context, true);
    // }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll){
          overscroll.disallowGlow();
        },
        child: FutureBuilder<List<HelpTicket>>(
          future: getUserQuestion(context),
          builder: (BuildContext context, AsyncSnapshot<List<HelpTicket>> snapshot){
            switch(snapshot.connectionState){
              case ConnectionState.waiting:
                return ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 4,
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  itemBuilder: (BuildContext context, int index){
                    return Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          decoration: BoxDecoration(
                            color: Color(0XFFF3F3F3),
                            borderRadius: BorderRadius.circular(20)
                          ),
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Shimmer.fromColors(
                                        baseColor: Colors.grey,
                                        highlightColor: Colors.white,
                                        child: Container(
                                          color: Colors.black38,
                                          child: Text('santara', style: TextStyle(color: Colors.transparent)),
                                        ),
                                      )
                                    ),
                                  ],
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey,
                                  highlightColor: Colors.white,
                                  child: Container(
                                    width: 200,
                                    margin: EdgeInsets.only(top: 10),
                                    color: Colors.black38,
                                    child: Text('santara', style: TextStyle(color: Colors.transparent),),
                                  ),
                                )
                              ],
                            ),
                          )
                        )
                      ],
                    );
                  },
                );
              default:
                if(snapshot.hasData){
                return ListView.builder(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  scrollDirection: Axis.vertical,
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index){
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context){
                              return ChatQuestion(
                                id: snapshot.data[index].serialTicket,
                                uuid: snapshot.data[index].uuid,
                                status: snapshot.data[index].status,
                                judul: snapshot.data[index].title,
                                prioritas: snapshot.data[index].priority,
                              );
                            }
                          )
                        );
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 8),
                        decoration: BoxDecoration(
                          color: Color(0XFFF3F3F3),
                          borderRadius: BorderRadius.circular(20)
                        ),
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text('[${snapshot.data[index].status.toString().toUpperCase()}][T00000${snapshot.data[index].serialTicket}]', style: TextStyle(fontSize: 14),),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Text('${date.format(DateTime.parse(snapshot.data[index].createdAt)).toString()}', textAlign: TextAlign.right, style: TextStyle(fontSize: 12),),
                                  )
                                ],
                              ),
                              Text('${snapshot.data[index].title}')
                            ],
                          ),
                        )
                      )
                    );
                  },
                );
              }else{
                return Center(
                  child: Text('Tidak ada riwayat'),
                );
              }
            }
          },
        )
      )
    );
  }
}