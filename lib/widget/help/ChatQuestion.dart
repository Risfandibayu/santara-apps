import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/ChatHistory.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/question.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
//components
import 'BubbleChat.dart';

class ChatQuestion extends StatefulWidget {
  final id;
  final uuid;
  final status;
  final prioritas;
  final judul;
  final email;

  ChatQuestion(
      {this.id,
      this.uuid,
      this.status,
      this.judul,
      this.prioritas,
      this.email});

  @override
  _ChatQuestionState createState() {
    return _ChatQuestionState(
      id: id,
      uuid: uuid,
      status: status,
      judul: judul,
      prioritas: prioritas,
      email: email,
    );
  }
}

class _ChatQuestionState extends State<ChatQuestion> {
  TextEditingController pesan = new TextEditingController();
  var kat = "";
  final uuid;
  final id;
  final status;
  final judul;
  final prioritas;
  final email;

  var token;
  var datas;
  var refreshToken;
  var traderId;

  _ChatQuestionState(
      {this.id,
      this.uuid,
      this.status,
      this.judul,
      this.prioritas,
      this.email});

  final storage = new FlutterSecureStorage();
  List<ChatHistory> chat;

  Future getConversation() async {
    try {
      final headers = await UserAgent.headers();
      final response =
          await http.get('$apiLocal/ticket/$uuid', headers: headers);
      if (response.statusCode == 200) {
        setState(() {
          chat = chatHistoryFromJson(response.body);
        });
      } else {
        setState(() {
          chat = [];
        });
      }
    } on SocketException catch (_) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Proses mengalami kendala, harap coba kembali'),
        backgroundColor: Colors.red,
      ));
      setState(() {
        chat = [];
      });
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    datas = userFromJson(await storage.read(key: 'user'));
    refreshToken = await storage.read(key: "refreshToken");
    if (datas != null) {
      traderId = datas.trader.id;
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((_) {
      getConversation();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '[${status.toString().toUpperCase()}][T00000$id]',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Container(
            width: 60,
            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                color: status == "Open" || status == "ReOpen"
                    ? Colors.red[700]
                    : Colors.green,
                onPressed: () {
                  setStatusChat(context, uuid);
                },
                child: status == "Closed"
                    ? Text('Buka',
                        style: TextStyle(color: Colors.white, fontSize: 8))
                    : Text('Tutup',
                        style: TextStyle(color: Colors.white, fontSize: 8))),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          //information
          Container(
            margin: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text('Prioritas'),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(':'),
                      ),
                      Expanded(
                        flex: 5,
                        child: Text(
                          '$prioritas',
                          textAlign: TextAlign.justify,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text('Judul'),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(':'),
                      ),
                      Expanded(
                        flex: 5,
                        child: Text(
                          '$judul',
                          textAlign: TextAlign.justify,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Colors.grey[300],
          ),

          chat == null
              ? Flexible(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : chat.length == 0
                  ? Flexible(
                      child: Center(
                      child: Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text('Tidak ada riwayat')),
                    ))
                  : Flexible(
                      child: ListView.builder(
                        padding: EdgeInsets.only(top: 12),
                        scrollDirection: Axis.vertical,
                        itemCount: chat.length,
                        itemBuilder: (BuildContext context, int index) {
                          if (chat[index].status != 'open') {
                            return BubbleChat(
                              detail: chat[index].comment,
                              isMe: chat[index].createdBy,
                              date: chat[index].createdAt,
                              email: email,
                              traderId: Helper.username,
                            );
                          } else {
                            kat = chat[index].status;
                            return Center(
                                child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: Linkify(
                                onOpen: (link) async {
                                  if (await canLaunch(link.url)) {
                                    await launch(link.url);
                                  } else {
                                    throw 'Could not launch $link';
                                  }
                                },
                                text: '${chat[index].comment}',
                                linkStyle: TextStyle(color: Colors.blue),
                              ),
                            ));
                          }
                        },
                      ),
                    ),

          //chat screen
          // FutureBuilder<List<ChatHistory>>(
          //   future: getConvertation(context, uuid),
          //   builder: (BuildContext context, AsyncSnapshot<List<ChatHistory>> snapshot){
          //     switch(snapshot.connectionState){
          //       default:
          //       if(snapshot.hasData){
          //         return Flexible(
          //           child: ListView.builder(
          //             padding: EdgeInsets.only(top: 12),
          //             scrollDirection: Axis.vertical,
          //             itemCount: snapshot.data.length,
          //             itemBuilder: (BuildContext context, int index){
          //               if(snapshot.data[index].status != 'open'){
          //                 return BubbleChat(
          //                   detail: snapshot.data[index].comment,
          //                   isMe: snapshot.data[index].createdBy,
          //                   date: snapshot.data[index].createdAt,
          //                   email: email,
          //                   traderId: Helper.username,
          //                 );
          //               }else{
          //                 kat = snapshot.data[index].status;
          //                 return Center(
          //                   child: Container(
          //                     margin: EdgeInsets.only(bottom: 10),
          //                     child: Linkify(
          //                       onOpen: (link) async {
          //                         if (await canLaunch(link.url)) {
          //                             await launch(link.url);
          //                           } else {
          //                             throw 'Could not launch $link';
          //                           }
          //                       },
          //                       text: '${snapshot.data[index].comment}',
          //                       linkStyle: TextStyle(color: Colors.blue),
          //                     ),
          //                   )
          //                 );
          //               }
          //             },
          //           ),
          //         );
          //       }else{
          //         return Flexible(
          //           child: Center(
          //             child: Container(
          //               margin: EdgeInsets.only(bottom: 10),
          //               child:Text('Tidak ada riwayat')
          //             ),
          //           )
          //         );
          //       }
          //     }
          //   },
          // ),
          Divider(
            height: 1.0,
          ),
          //text field
          status != "Closed"
              ? Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).cardColor,
                  ),
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Container(
                              margin: EdgeInsets.only(top: 4, bottom: 4),
                              child: TextField(
                                controller: pesan,
                                maxLength: 255,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    counter: Container(),
                                    hintText: "Balas..."),
                                onSubmitted: (v) {
                                  if (v.isNotEmpty) {
                                    sendChat(context, pesan.text, uuid)
                                        .then((_) {
                                      getConversation();
                                    });
                                    pesan.clear();
                                  }
                                },
                              )),
                        ),
                      ],
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
