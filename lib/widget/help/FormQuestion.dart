import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';

class FormQuestion extends StatefulWidget {
  @override
  _FormQuestionState createState() => _FormQuestionState();
}

class _FormQuestionState extends State<FormQuestion> {
  final storage = new FlutterSecureStorage();
  final TextEditingController judul = new TextEditingController();
  final TextEditingController pesan = new TextEditingController();
  final formKey = GlobalKey<FormState>();

  List<Map> priority = [
    {"id": "1", "category": "Low", "value": "Low"},
    {"id": "2", "category": "Medium", "value": "Medium"},
    {"id": "3", "category": "High", "value": "High"}
  ];
  List<DropdownMenuItem<String>> dropDownItem;
  String kategori;
  String prioritas;
  String token;
  bool isLoading = false;

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    priority.map((prio) {
      items.add(new DropdownMenuItem(
          value: prio['value'].toString(), child: Text(prio['category'])));
    }).toList();
    return items;
  }

  Future sendQuestion() async {
    setState(() => isLoading = true);
    try {
      final headers = await UserAgent.headers();
      final response =
          await http.post('$apiLocal/ticket/', headers: headers, body: {
        "title": judul.text,
        "priority": prioritas.toString(),
        "comment": pesan.text,
      });
      if (response.statusCode == 200) {
        judul.clear();
        pesan.clear();
        setState(() => isLoading = false);
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Pertanyaan Anda berhasil dikirim'),
          duration: Duration(seconds: 1),
          backgroundColor: Colors.blue,
        ));
      } else {
        setState(() => isLoading = false);
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Ada kesalahan'),
            duration: Duration(seconds: 1),
            backgroundColor: Colors.red));
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Proses mengalami kendala, harap coba kembali'),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage();
    dropDownItem = getDropDownMenuItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: SingleChildScrollView(
            padding: EdgeInsets.all(8),
            scrollDirection: Axis.vertical,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Theme(
                data: ThemeData(
                    primaryColor: Colors.grey, accentColor: Colors.red[800]),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //Judul
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          maxLines: null,
                          controller: judul,
                          maxLength: 100,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Harus diisi';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            labelText: "Judul",
                            hintText: "Judul",
                            errorMaxLines: 5,
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.grey)),
                          ),
                        ),
                      ),

                      //Kategori
                      Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 0),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(3)),
                            height: 58,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: DropdownButtonHideUnderline(
                                        child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: DropdownButton(
                                    isDense: true,
                                    isExpanded: true,
                                    value: prioritas,
                                    items: dropDownItem,
                                    hint: Text("Pilih Prioritas"),
                                    onChanged: (selected) {
                                      setState(() {
                                        prioritas = selected;
                                      });
                                    },
                                  ),
                                )))
                              ],
                            ),
                          ),
                          Container(
                            color: Colors.white,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 4, vertical: 2),
                            margin: EdgeInsets.fromLTRB(6, 0, 30, 0),
                            child: Text("Prioritas",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Colors.grey)),
                          ),
                        ],
                      ),

                      //Pesan
                      Container(
                        margin: EdgeInsets.only(top: 28),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          maxLines: 3,
                          controller: pesan,
                          maxLength: 255,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Harus diisi';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            labelText: "Pesan",
                            hintText: "Pesan",
                            errorMaxLines: 5,
                            alignLabelWithHint: true,
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.grey)),
                          ),
                        ),
                      ),

                      Center(
                        child: Container(
                            width: 200,
                            margin: EdgeInsets.only(top: 30),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              onPressed: prioritas != null
                                  ? () {
                                      if (formKey.currentState.validate()) {
                                        sendQuestion();
                                      }
                                    }
                                  : null,
                              color: Color(0xFFBF2D30),
                              disabledColor: Colors.grey,
                              child: Text(
                                isLoading ? 'Mengirim...' : 'Kirim Pertanyaan',
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
