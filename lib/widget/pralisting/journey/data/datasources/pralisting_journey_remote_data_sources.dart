import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/journey/data/models/pralisting_journey_model.dart';
import 'package:meta/meta.dart';

abstract class PralistingJourneyRemoteDataSources {
  Future<PralistingJourneyModel> getPralistingJourney({@required String uuid});
}

class PralistingJourneyRemoteDataSourcesImpl
    implements PralistingJourneyRemoteDataSources {
  final RestClient client;

  PralistingJourneyRemoteDataSourcesImpl({@required this.client});

  @override
  Future<PralistingJourneyModel> getPralistingJourney({String uuid}) async {
    try {
      final result = await client.getExternalUrl(
        url: "$apiPralisting/pralisting-detail-submissions/$uuid",
      );
      if (result.statusCode == 200) {
        return PralistingJourneyModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
