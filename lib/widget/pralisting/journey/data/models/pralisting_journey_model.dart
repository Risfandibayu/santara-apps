// To parse this JSON data, do
//
//     final pralistingJourneyModel = pralistingJourneyModelFromJson(jsonString);

import 'dart:convert';

PralistingJourneyModel pralistingJourneyModelFromJson(String str) =>
    PralistingJourneyModel.fromJson(json.decode(str));

String pralistingJourneyModelToJson(PralistingJourneyModel data) =>
    json.encode(data.toJson());

class PralistingJourneyModel {
  PralistingJourneyModel({
    this.journey,
  });

  List<Journey> journey;

  factory PralistingJourneyModel.fromJson(Map<String, dynamic> json) =>
      PralistingJourneyModel(
        journey: json["journey"] == null
            ? null
            : List<Journey>.from(
                json["journey"].map((x) => Journey.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "journey": journey == null
            ? null
            : List<dynamic>.from(journey.map((x) => x.toJson())),
      };
}

class Journey {
  Journey({
    this.stepId,
    this.text,
    this.statusFilling,
    this.statusRejection,
    this.detail,
  });

  int stepId;
  String text;
  bool statusFilling;
  bool statusRejection;
  List<Detail> detail;

  factory Journey.fromJson(Map<String, dynamic> json) => Journey(
        stepId: json["step_id"] == null ? null : json["step_id"],
        text: json["text"] == null ? null : json["text"],
        statusFilling:
            json["status_filling"] == null ? null : json["status_filling"],
        statusRejection:
            json["status_rejection"] == null ? null : json["status_rejection"],
        detail: json["detail"] == null
            ? null
            : List<Detail>.from(json["detail"].map((x) => Detail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "step_id": stepId == null ? null : stepId,
        "text": text == null ? null : text,
        "status_filling": statusFilling == null ? null : statusFilling,
        "status_rejection": statusRejection == null ? null : statusRejection,
        "detail": detail == null
            ? null
            : List<dynamic>.from(detail.map((x) => x.toJson())),
      };
}

class Detail {
  Detail({
    this.fieldId,
    this.status,
    this.error,
  });

  String fieldId;
  int status;
  String error;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        fieldId: json["field_id"] == null ? null : json["field_id"],
        status: json["status"] == null ? null : json["status"],
        error: json["error"] == null ? null : json["error"],
      );

  Map<String, dynamic> toJson() => {
        "field_id": fieldId == null ? null : fieldId,
        "status": status == null ? null : status,
        "error": error == null ? null : error,
      };
}
