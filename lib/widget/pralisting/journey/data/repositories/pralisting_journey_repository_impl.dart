import 'package:meta/meta.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/journey/data/datasources/pralisting_journey_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/journey/data/models/pralisting_journey_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/journey/domain/repositories/pralisting_journey_repository.dart';

class PralistingJourneyRepositoryImpl implements PralistingJourneyRepository {
  final NetworkInfo networkInfo;
  final PralistingJourneyRemoteDataSources remoteDataSource;

  PralistingJourneyRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, PralistingJourneyModel>> getPralistingJourney({
    String uuid,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPralistingJourney(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
