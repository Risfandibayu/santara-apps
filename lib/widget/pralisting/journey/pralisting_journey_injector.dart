import 'package:santaraapp/widget/pralisting/journey/data/datasources/pralisting_journey_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/journey/data/repositories/pralisting_journey_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/journey/domain/repositories/pralisting_journey_repository.dart';
import 'package:santaraapp/widget/pralisting/journey/domain/usecases/pralisting_journey_usecases.dart';

import '../../../injector_container.dart';
import 'presentation/blocs/pralisting_journey_bloc.dart';

void initPralistingJourneyInjector() {
  sl.registerFactory(
    () => PralistingJourneyBloc(getPralistingJourney: sl()),
  );

  sl.registerLazySingleton<PralistingJourneyRepository>(
    () => PralistingJourneyRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<PralistingJourneyRemoteDataSources>(
    () => PralistingJourneyRemoteDataSourcesImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetPralistingJourney(sl()));
}
