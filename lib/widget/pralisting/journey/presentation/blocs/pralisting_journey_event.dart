import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PralistingJourneyEvent extends Equatable {}

class LoadPralistingJourney extends PralistingJourneyEvent {
  final String uuid;
  LoadPralistingJourney({@required this.uuid});

  @override
  List<Object> get props => [uuid];
}
