import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/journey/data/models/pralisting_journey_model.dart';

abstract class PralistingJourneyState extends Equatable {}

class PralistingJourneyUninitialized extends PralistingJourneyState {
  @override
  List<Object> get props => [];
}

class PralistingJourneyLoading extends PralistingJourneyState {
  @override
  List<Object> get props => [];
}

class PralistingJourneyError extends PralistingJourneyState {
  final Failure failure;
  PralistingJourneyError({@required this.failure});

  @override
  List<Object> get props => [failure];
}

class PralistingJourneyLoaded extends PralistingJourneyState {
  final PralistingJourneyModel data;
  PralistingJourneyLoaded({@required this.data});

  @override
  List<Object> get props => [data];
}
