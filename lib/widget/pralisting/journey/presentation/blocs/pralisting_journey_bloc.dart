import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/journey/domain/usecases/pralisting_journey_usecases.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_event.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_state.dart';

class PralistingJourneyBloc
    extends Bloc<PralistingJourneyEvent, PralistingJourneyState> {
  PralistingJourneyBloc({
    @required GetPralistingJourney getPralistingJourney,
  })  : assert(
          getPralistingJourney != null,
        ),
        _getPralistingJourney = getPralistingJourney,
        super(PralistingJourneyUninitialized());

  final GetPralistingJourney _getPralistingJourney;

  @override
  Stream<PralistingJourneyState> mapEventToState(
    PralistingJourneyEvent event,
  ) async* {
    if (event is LoadPralistingJourney) {
      yield* _mapGetPralistingJourneyToState(event, state);
    }
  }

  Stream<PralistingJourneyState> _mapGetPralistingJourneyToState(
    LoadPralistingJourney event,
    PralistingJourneyState state,
  ) async* {
    try {
      yield PralistingJourneyLoading();
      final result = await _getPralistingJourney(event.uuid);
      yield* result.fold(
        (failure) async* {
          printFailure(failure);
          yield PralistingJourneyError(failure: failure);
        },
        (list) async* {
          yield PralistingJourneyLoaded(data: list);
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield PralistingJourneyError(
        failure: Failure(
          type: FailureType.localError,
          message: e,
        ),
      );
    }
  }
}
