import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_bloc.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_event.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_state.dart';
import 'package:santaraapp/widget/widget/components/listing/SantaraStepper.dart';

import '../../../../../injector_container.dart';

class PralistingJourneyPage extends StatefulWidget {
  final String uuid;
  PralistingJourneyPage({@required this.uuid});

  @override
  _PralistingJourneyPageState createState() => _PralistingJourneyPageState();
}

class _PralistingJourneyPageState extends State<PralistingJourneyPage> {
  PralistingJourneyBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<PralistingJourneyBloc>();
    bloc..add(LoadPralistingJourney(uuid: widget.uuid));
    print(widget.uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Form",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        centerTitle: true,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.all(Sizes.s20),
        child: BlocProvider<PralistingJourneyBloc>(
          create: (_) => bloc,
          child: BlocBuilder<PralistingJourneyBloc, PralistingJourneyState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is PralistingJourneyLoading) {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (state is PralistingJourneyError) {
                return Center(
                  child: Text("${state.failure.message}"),
                );
              } else if (state is PralistingJourneyLoaded) {
                return state.data.journey != null &&
                        state.data.journey.length > 0
                    ? ListView.builder(
                        itemCount: state.data.journey.length,
                        itemBuilder: (context, index) {
                          var journey = state.data.journey[index];
                          return SantaraStepper(
                            title: "${journey.text}",
                            isActive: journey.statusFilling,
                            isEnd: index == state.data.journey.length - 1,
                            enableSeparator: true,
                          );
                        },
                      )
                    : Container(
                        child: Text("Empty"),
                      );
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }
}
