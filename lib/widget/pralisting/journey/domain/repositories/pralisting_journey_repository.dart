import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/journey/data/models/pralisting_journey_model.dart';
import 'package:meta/meta.dart';

abstract class PralistingJourneyRepository {
  Future<Either<Failure, PralistingJourneyModel>> getPralistingJourney({
    @required String uuid,
  });
}
