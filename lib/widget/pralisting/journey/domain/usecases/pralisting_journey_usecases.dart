import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/journey/data/models/pralisting_journey_model.dart';
import 'package:santaraapp/widget/pralisting/journey/domain/repositories/pralisting_journey_repository.dart';

class GetPralistingJourney implements UseCase<PralistingJourneyModel, String> {
  final PralistingJourneyRepository repository;
  GetPralistingJourney(this.repository);

  @override
  Future<Either<Failure, PralistingJourneyModel>> call(String uuid) async {
    return await repository.getPralistingJourney(uuid: uuid);
  }
}