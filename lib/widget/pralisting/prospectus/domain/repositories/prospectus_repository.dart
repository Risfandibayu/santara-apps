import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';

abstract class ProspectusRepository {
  Future<Either<Failure, dynamic>> getProspectus({
    @required String uuid,
    @required int step,
  });
}
