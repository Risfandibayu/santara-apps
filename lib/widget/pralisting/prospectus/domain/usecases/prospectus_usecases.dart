import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/prospectus/domain/repositories/prospectus_repository.dart';

class GetProspectus implements UseCase<dynamic, dynamic> {
  final ProspectusRepository repository;
  GetProspectus(this.repository);

  @override
  Future<Either<Failure, dynamic>> call(dynamic params) async {
    return await repository.getProspectus(
      uuid: params['uuid'],
      step: params['step'],
    );
  }
}
