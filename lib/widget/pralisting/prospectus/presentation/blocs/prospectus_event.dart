import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ProspectusEvent extends Equatable {}

class LoadProspectus extends ProspectusEvent {
  final String uuid;
  final int step;
  LoadProspectus({@required this.uuid, @required this.step});

  @override
  List<Object> get props => [uuid, step];
}
