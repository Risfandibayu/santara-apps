import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';

abstract class ProspectusState extends Equatable {}

class ProspectusUninitialized extends ProspectusState {
  @override
  List<Object> get props => [];
}

class ProspectusLoading extends ProspectusState {
  @override
  List<Object> get props => [];
}

class ProspectusError extends ProspectusState {
  final Failure failure;
  ProspectusError({@required this.failure});

  @override
  List<Object> get props => [failure];
}

class ProspectusLoaded extends ProspectusState {
  final dynamic data;
  ProspectusLoaded({@required this.data});

  @override
  List<Object> get props => [data];
}
