import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/prospectus/domain/usecases/prospectus_usecases.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:meta/meta.dart';

class ProspectusBloc extends Bloc<ProspectusEvent, ProspectusState> {
  ProspectusBloc({
    @required GetProspectus getProspectus,
  })  : assert(
          getProspectus != null,
        ),
        _getProspectus = getProspectus,
        super(ProspectusUninitialized());

  final GetProspectus _getProspectus;

  @override
  Stream<ProspectusState> mapEventToState(
    ProspectusEvent event,
  ) async* {
    if (event is LoadProspectus) {
      print(">> HELLO");
      yield* _mapGetProspectusToState(event, state);
    }
  }

  Stream<ProspectusState> _mapGetProspectusToState(
    LoadProspectus event,
    ProspectusState state,
  ) async* {
    try {
      yield ProspectusLoading();
      final params = {
        "uuid": "${event.uuid}",
        "step": event.step,
      };
      final result = await _getProspectus(params);
      yield* result.fold(
        (failure) async* {
          printFailure(failure);
          yield ProspectusError(failure: failure);
        },
        (data) async* {
          yield ProspectusLoaded(data: data);
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield ProspectusError(
        failure: Failure(
          type: FailureType.localError,
          message: e,
        ),
      );
    }
  }
}
