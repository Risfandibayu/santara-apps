import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/models/opener_model.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

import '../../../../../injector_container.dart';

class OpenerTab extends StatefulWidget {
  final String uuid;
  final int step;
  OpenerTab({
    @required this.uuid,
    @required this.step,
  });

  @override
  _OpenerTabState createState() => _OpenerTabState();
}

class _OpenerTabState extends State<OpenerTab> {
  ProspectusBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<ProspectusBloc>();
    bloc..add(LoadProspectus(uuid: widget.uuid, step: widget.step));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<ProspectusBloc, ProspectusState>(
          builder: (context, state) {
        if (state is ProspectusLoading) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ProspectusLoaded) {
          OpenerModel data = OpenerModel.fromJson(state.data);
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "${data.trademark}",
                        style: TextStyle(
                          fontSize: FontSize.s24,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      // SizedBox(height: Sizes.s10),
                      Text(
                        "${data.companyName}",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      SantaraCachedImage(
                        image: "${data.logo}",
                        height: Sizes.s300,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Prospektus",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      SantaraCachedImage(
                        image: "${data.picture}",
                        height: Sizes.s300,
                      ),
                      SizedBox(height: Sizes.s50),
                      Text(
                        "Tanya Jawab Seputar\nPenawaran Saham",
                        style: TextStyle(
                            fontSize: FontSize.s24,
                            fontWeight: FontWeight.w800,
                            height: 1.2),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: Sizes.s10),
                      Text(
                        "${data.companyName}",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      InkWell(
                        onTap: () {},
                        child: Container(
                          height: Sizes.s45,
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: Color(0xff039BE5),
                            borderRadius: BorderRadius.circular(Sizes.s5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                "assets/icon/telegram.png",
                                width: Sizes.s20,
                              ),
                              SizedBox(width: Sizes.s20),
                              Text(
                                "Join Grup Telegram",
                                style: TextStyle(
                                  fontSize: FontSize.s14,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                data?.isPandhega == 1
                    ? Image.asset(
                        "assets/santara/pandhega.png",
                        width: double.maxFinite,
                      )
                    : Container(),
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  color: Colors.black,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Disclaimer",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      Text(
                        """Pembelian  saham  bisnis  merupakan  aktivitas  berisiko  tinggi. Anda  berinvestasi  pada  bisnis  yang  mungkin  saja  mengalami kenaikan dan penurunan kinerja bahkan mengalami kegagalan.

Harap  menggunakan  pertimbangan  ekstra  dalam  membuat keputusan untuk membeli saham. Ada kemungkinan Anda tidak bisa menjual kembali saham bisnis dengan cepat. Penyelenggara tidak menjamin likuidasi saham pada saat secondary market.

Lakukan diversifikasi investasi,  hanya gunakan dana yang siap Anda lepaskan (afford to loose) dan atau disimpan dalam jangka panjang.

Santara  tidak  memaksa  Pemodal  untuk  membeli  saham  UKM sebagai  investasi.  Semua  keputusan  pembelian  merupakan keputusan independen oleh Pemodal.

Harap membaca kembali setiap klausul pada syarat & ketentuan sebagai Pemodal. Dengan memberikan persetujuan, maka Anda tunduk pada setiap isi klausula tersebut.

Santara  bertindak  sebagai  penyelenggara  urun  dana  yang mempertemukan  Pemodal  dan  Penerbit,  bukan  sebagai  pihak yang  menjalankan  bisnis  (Penerbit).  Otoritas  Jasa  Keuangan bertindak  sebagai  regulator  dan  pemberi  izin,  bukan  sebagai penjamin investasi.

Semua data yang tersaji di dalam prospektus ini diperoleh dari Penerbit  dan  data  sekunder  faktual  penunjang.  Keputusan pembelian  saham,  sepenuhnya  merupakan  hak  dan  tanggung jawab  Pemodal  (Investor).  Dengan  membeli  saham  di  Santara berarti  Anda  sudah  menyetujui  seluruh  syarat  dan  ketentuan serta  memahami  semua  risiko  investasi  termasuk  risiko kehilangan sebagian atau seluruh modal.""",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: FontSize.s12,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        } else if (state is ProspectusError) {
          return Center(
            child: Text("${state.failure.message}"),
          );
        } else {
          return Container(
            child: Text("Unknown State"),
          );
        }
      }),
    );
  }
}
