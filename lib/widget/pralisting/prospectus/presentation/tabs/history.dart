import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/models/business_history_model.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

import '../../../../../injector_container.dart';

class HistoryTab extends StatefulWidget {
  final String uuid;
  final int step;
  HistoryTab({
    @required this.uuid,
    @required this.step,
  });

  @override
  _HistoryTabState createState() => _HistoryTabState();
}

class _HistoryTabState extends State<HistoryTab> {
  ProspectusBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<ProspectusBloc>();
    bloc..add(LoadProspectus(uuid: widget.uuid, step: widget.step));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<ProspectusBloc, ProspectusState>(
          builder: (context, state) {
        if (state is ProspectusLoading) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ProspectusLoaded) {
          BusinessHistoryModel data = BusinessHistoryModel.fromJson(state.data);
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "DESKRIPSI DAN SEJARAH USAHA",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      SantaraCachedImage(
                        image: "${data.picture}",
                        height: Sizes.s300,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Highlight Industri",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "${data.highlightIndustry}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Sejarah Berdiri dan Latar Belakang",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "${data.businessHistory}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Deskripsi dan Pengembangan Usaha",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "${data.businessDescription}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Competitive Advantage Usaha",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "${data.advantage}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Rencana Ekspansi Bisnis Kedepan",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "${data.expansionPlan}",
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else if (state is ProspectusError) {
          return Center(
            child: Text("${state.failure.message}"),
          );
        } else {
          return Container(
            child: Text("Unknown State"),
          );
        }
      }),
    );
  }
}
