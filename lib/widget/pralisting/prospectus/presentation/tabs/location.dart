import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/models/business_location_model.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/widgets/prospectus_image_carousel.dart';
import '../../../../../injector_container.dart';

class LocationTab extends StatefulWidget {
  final String uuid;
  final int step;

  LocationTab({
    @required this.uuid,
    @required this.step,
  });

  @override
  _LocationTabState createState() => _LocationTabState();
}

class _LocationTabState extends State<LocationTab> {
  ProspectusBloc bloc;

  _buttonTitle({@required String title, @required Color color}) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(Sizes.s5)),
      child: Container(
        color: color,
        padding: EdgeInsets.fromLTRB(
          Sizes.s25,
          Sizes.s8,
          Sizes.s25,
          Sizes.s8,
        ),
        child: Text(
          "$title",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  _locationTile({@required String name, @required String address}) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            "assets/icon/location.png",
            width: Sizes.s25,
          ),
          SizedBox(width: Sizes.s15),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: Sizes.s2),
                Text(
                  "$name",
                  style: TextStyle(
                    fontSize: FontSize.s18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: Sizes.s10),
                Text(
                  "Alamat",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "$address",
                  style: TextStyle(
                    fontSize: FontSize.s12,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildExistingBusinessLocation(BusinessLocationModel data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buttonTitle(
          title: "Eksisting",
          color: Color(0xffD23737),
        ),
        SizedBox(height: Sizes.s20),
        data.oldLocations != null && data.oldLocations.length > 0
            ? ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: data.oldLocations.length,
                itemBuilder: (context, index) {
                  var existing = data.oldLocations[index];
                  return _locationTile(
                    name: existing?.name ?? "",
                    address: existing?.address ?? "",
                  );
                },
              )
            : Container(),
        SizedBox(height: Sizes.s10),
        Text(
          "Foto Lokasi Usaha",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: FontSize.s18,
          ),
        ),
        SizedBox(height: Sizes.s10),
        ProspectusImageCarousel(
          pictures: data.oldLocationPictures,
        ),
        SizedBox(height: Sizes.s50),
      ],
    );
  }

  Widget _buildNewBusinessLocation(BusinessLocationModel data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buttonTitle(
          title: "Akan Dibuka",
          color: Color(0xff374FC7),
        ),
        SizedBox(height: Sizes.s20),
        data.newLocations != null && data.newLocations.length > 0
            ? ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: data.newLocations.length,
                itemBuilder: (context, index) {
                  var existing = data.newLocations[index];
                  return _locationTile(
                    name: existing?.name ?? "",
                    address: existing?.address ?? "",
                  );
                },
              )
            : Container(),
        SizedBox(height: Sizes.s10),
        Text(
          "Foto Lokasi Usaha Baru",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: FontSize.s18,
          ),
        ),
        SizedBox(height: Sizes.s10),
        ProspectusImageCarousel(
          pictures: data.newLocationPictures,
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = sl<ProspectusBloc>();
    bloc..add(LoadProspectus(uuid: widget.uuid, step: widget.step));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<ProspectusBloc, ProspectusState>(
        builder: (context, state) {
          if (state is ProspectusLoading) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          } else if (state is ProspectusLoaded) {
            BusinessLocationModel data =
                BusinessLocationModel.fromJson(state.data);
            return SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(Sizes.s20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "LOKASI",
                          style: TextStyle(
                            fontSize: FontSize.s18,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        SizedBox(height: Sizes.s20),
                        _buildExistingBusinessLocation(data),
                        _buildNewBusinessLocation(data),
                      ],
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
