import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/detail_emiten/YoutubePlayerUI.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/models/business_team_model.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/widgets/prospectus_image_carousel.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import '../../../../../injector_container.dart';

class ProfileTeamTab extends StatefulWidget {
  final String uuid;
  final int step;
  ProfileTeamTab({
    @required this.uuid,
    @required this.step,
  });

  @override
  _ProfileTeamTabState createState() => _ProfileTeamTabState();
}

class _ProfileTeamTabState extends State<ProfileTeamTab> {
  ProspectusBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<ProspectusBloc>();
    bloc..add(LoadProspectus(uuid: widget.uuid, step: widget.step));
  }

  teamProfile({
    @required String name,
    @required String position,
    @required String desc,
    @required String image,
  }) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: Sizes.s30),
        ClipRRect(
          borderRadius: BorderRadius.circular(Sizes.s5),
          child: SantaraCachedImage(
            width: Sizes.s150,
            height: Sizes.s200,
            image: "$image",
          ),
        ),
        SizedBox(height: Sizes.s10),
        Text(
          "$name",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: FontSize.s18,
          ),
        ),
        Text(
          "$position",
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: FontSize.s18,
          ),
        ),
        SizedBox(height: Sizes.s10),
        Text(
          "$desc",
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: FontSize.s12,
          ),
          textAlign: TextAlign.justify,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<ProspectusBloc, ProspectusState>(
          builder: (context, state) {
        if (state is ProspectusLoading) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ProspectusLoaded) {
          BusinessTeamModel data = BusinessTeamModel.fromJson(state.data);
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "PROFIL TIM",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: FontSize.s18,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      data.emitenTeams != null && data.emitenTeams.length > 0
                          ? ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: data.emitenTeams.length,
                              itemBuilder: (context, index) {
                                var team = data.emitenTeams[index];
                                return teamProfile(
                                  name: team.name,
                                  position: team.position,
                                  desc: team.experience,
                                  image: team.photoUrl,
                                );
                              },
                            )
                          : Container(),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: Sizes.s40),
                      Text(
                        "JUMLAH PEKERJA",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: FontSize.s18,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      data.emitenPositions != null &&
                              data.emitenPositions.length > 0
                          ? ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: data.emitenPositions.length,
                              itemBuilder: (context, index) {
                                var position = data.emitenPositions[index];
                                return Container(
                                  margin: EdgeInsets.only(top: Sizes.s10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          "${position.name}",
                                          style: TextStyle(
                                            fontSize: FontSize.s14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "${position.employee}",
                                        style: TextStyle(
                                          fontSize: FontSize.s14,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            )
                          : Container()
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: Sizes.s40),
                      Text(
                        "Foto Tim",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: FontSize.s18,
                        ),
                      ),
                      SizedBox(height: Sizes.s15),
                      ProspectusImageCarousel(
                        pictures: [
                          "https://cdn.jpegmini.com/user/images/slider_puffin_before_mobile.jpg",
                          "https://images.ctfassets.net/hrltx12pl8hq/5GaLeZJlLyOiQC4gOA0qUM/a0398c237e9744ade8b072f99349e07a/shutterstock_152461202_thumb.jpg?fit=fill&w=480&h=270",
                          "https://thumbs.dreamstime.com/b/monarch-orange-butterfly-bright-summer-flowers-background-blue-foliage-fairy-garden-macro-artistic-image-monarch-167030287.jpg",
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        } else if (state is ProspectusError) {
          return Center(
            child: Text("${state.failure.message}"),
          );
        } else {
          return Container(
            child: Text("Unknown State"),
          );
        }
      }),
    );
  }
}
