import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/detail_emiten/YoutubePlayerUI.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/models/business_profile_model.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_event.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_state.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import '../../../../../injector_container.dart';

class ProfileSocialTab extends StatefulWidget {
  final String uuid;
  final int step;
  ProfileSocialTab({
    @required this.uuid,
    @required this.step,
  });

  @override
  _ProfileSocialTabState createState() => _ProfileSocialTabState();
}

class _ProfileSocialTabState extends State<ProfileSocialTab> {
  ProspectusBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<ProspectusBloc>();
    bloc..add(LoadProspectus(uuid: widget.uuid, step: widget.step));
  }

  socialProfile({@required String title, @required String image}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: Sizes.s30),
        Text(
          "$title",
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: FontSize.s14,
          ),
        ),
        SizedBox(height: Sizes.s10),
        ClipRRect(
          borderRadius: BorderRadius.circular(Sizes.s5),
          child: SantaraCachedImage(
            height: Sizes.s300,
            image: "$image",
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<ProspectusBloc, ProspectusState>(
          builder: (context, state) {
        if (state is ProspectusLoading) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ProspectusLoaded) {
          BusinessProfileModel data = BusinessProfileModel.fromJson(state.data);
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(Sizes.s20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "PROFIL DAN MEDIA SOSIAL",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      InkWell(
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => YoutubePlayerUI(
                              url: data.video,
                            ),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(Sizes.s5),
                          child: Container(
                            height: Sizes.s200,
                            width: double.maxFinite,
                            child: Stack(
                              children: [
                                SantaraCachedImage(
                                  image: data.video == null ||
                                          data.video.isEmpty
                                      ? ""
                                      : YoutubeThumbnailGenerator.getThumbnail(
                                          "${data.video}",
                                          ThumbnailQuality.medium,
                                        ),
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color(0xffD23737),
                                    ),
                                    height: Sizes.s45,
                                    width: Sizes.s45,
                                    child: Icon(
                                      Icons.play_arrow,
                                      color: Colors.white,
                                      size: FontSize.s25,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Center(
                        child: Column(
                          children: [
                            Text(
                              "Video Profil",
                              style: TextStyle(
                                fontSize: FontSize.s18,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            SizedBox(height: Sizes.s10),
                            Text(
                              "${data.companyName}",
                              style: TextStyle(
                                fontSize: FontSize.s14,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Media Sosial",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: FontSize.s18,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      socialProfile(
                        title: "Instagram",
                        image: "${data.instagram}",
                      ),
                      socialProfile(
                        title: "Facebook",
                        image: "${data.facebook}",
                      ),
                      socialProfile(
                        title: "Website",
                        image: "${data.website}",
                      ),
                      socialProfile(
                        title: "Review",
                        image: "${data.review}",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else if (state is ProspectusError) {
          return Center(
            child: Text("${state.failure.message}"),
          );
        } else {
          return Container(
            child: Text("Unknown State"),
          );
        }
      }),
    );
  }
}
