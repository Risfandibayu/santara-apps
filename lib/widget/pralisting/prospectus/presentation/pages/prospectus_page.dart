import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/tabs/history.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/tabs/location.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/tabs/opener.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/tabs/profile_social.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/tabs/profile_team.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinancialStatements.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraPralistingTabs.dart';

class ProspectusPage extends StatefulWidget {
  final String uuid;
  ProspectusPage({@required this.uuid});

  @override
  _ProspectusPageState createState() => _ProspectusPageState();
}

class _ProspectusPageState extends State<ProspectusPage>
    with SingleTickerProviderStateMixin {
  // inisialisasi nama tabs
  final List<Widget> tabs = [
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Pembuka",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Deskrispi dan Sejarah",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Lokasi",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Profil dan Media Sosial",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Profil Tim",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Finansial",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Penawaran",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Rencana Penggunaan Dana",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Risiko & Mitigasi",
      ),
    ),
    Tab(
      child: IconTabBar(
        isNotified: false,
        title: "Rekam Jejak Perusahaan",
      ),
    ),
  ];
  TabController _tabController; // controller tabs
  int _tabIndex = 0; // current index

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_handleTabChanges);
    super.initState();
  }

  _handleTabChanges() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Prospektus",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: FontSize.s18,
          ),
        ),
        centerTitle: true,
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        bottom: TabBar(
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          controller: _tabController,
          isScrollable: true,
          tabs: tabs,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        // physics: NeverScrollableScrollPhysics(),
        children: [
          OpenerTab(uuid: widget.uuid, step: 1),
          HistoryTab(uuid: widget.uuid, step: 2),
          LocationTab(uuid: widget.uuid, step: 3),
          ProfileSocialTab(uuid: widget.uuid, step: 4),
          ProfileTeamTab(uuid: widget.uuid, step: 5),
          Icon(Icons.directions_car, size: 350),
          Icon(Icons.directions_car, size: 350),
          Icon(Icons.flight, size: 350),
          Icon(Icons.directions_transit, size: 350),
          Icon(Icons.directions_car, size: 350),
        ],
      ),
    );
  }
}
