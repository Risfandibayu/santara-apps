import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class ProspectusImageCarousel extends StatefulWidget {
  final List<String> pictures;
  ProspectusImageCarousel({@required this.pictures});
  @override
  _ProspectusImageCarouselState createState() =>
      _ProspectusImageCarouselState();
}

class _ProspectusImageCarouselState extends State<ProspectusImageCarousel> {
  int _currentIndex = 0;
  CarouselController buttonCarouselController = CarouselController();

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.pictures != null && widget.pictures.length > 0) {
      return Column(
        children: [
          Container(
            height: Sizes.s250,
            color: Color(0xff2E2E2E),
            child: Center(
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: CarouselSlider(
                      carouselController: buttonCarouselController,
                      options: CarouselOptions(
                        height: Sizes.s200,
                        autoPlay: false,
                        viewportFraction: 1.0,
                        enlargeCenterPage: false,
                        pauseAutoPlayOnTouch: true,
                        aspectRatio: 0.0,
                        onPageChanged: (index, reason) {
                          setState(() {
                            _currentIndex = index;
                          });
                        },
                      ),
                      items: widget.pictures.map((card) {
                        return Builder(builder: (BuildContext context) {
                          return Container(
                            height: Sizes.s200,
                            width: double.maxFinite,
                            color: Colors.pinkAccent,
                            child: SantaraCachedImage(image: card),
                          );
                        });
                      }).toList(),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: <Color>[
                            Colors.black.withOpacity(.6),
                            Colors.black.withOpacity(0.0),
                          ],
                        ),
                      ),
                      width: Sizes.s40,
                      height: Sizes.s200,
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          buttonCarouselController.previousPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.linear,
                          );
                        },
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerRight,
                          end: Alignment.centerLeft,
                          colors: <Color>[
                            Colors.black.withOpacity(.6),
                            Colors.black.withOpacity(0.0),
                          ],
                        ),
                      ),
                      width: Sizes.s40,
                      height: Sizes.s200,
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          buttonCarouselController.nextPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.linear,
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(
              widget.pictures,
              (index, url) {
                return Container(
                  width: Sizes.s8,
                  height: Sizes.s8,
                  margin: EdgeInsets.symmetric(
                    vertical: Sizes.s8,
                    horizontal: Sizes.s2,
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _currentIndex == index
                        ? Color(0xffBF0000)
                        : Color(0xffC4C4C4),
                  ),
                );
              },
            ),
          ),
        ],
      );
    } else {
      return SantaraCachedImage(
        image: "",
        height: Sizes.s300,
      );
    }
  }
}
