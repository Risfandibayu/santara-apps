import 'package:santaraapp/widget/pralisting/prospectus/data/datasources/prospectus_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/repositories/prospectus_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/prospectus/domain/repositories/prospectus_repository.dart';
import 'package:santaraapp/widget/pralisting/prospectus/domain/usecases/prospectus_usecases.dart';
import 'package:santaraapp/widget/pralisting/prospectus/presentation/blocs/prospectus_bloc.dart';

import '../../../injector_container.dart';

void initProspectusInjector() {
  // Bloc
  sl.registerFactory(
    () => ProspectusBloc(getProspectus: sl()),
  );

  //! Global Usecase
  sl.registerLazySingleton(() => GetProspectus(sl()));

  //! Global Repository
  sl.registerLazySingleton<ProspectusRepository>(
    () => ProspectusRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<ProspectusRemoteDataSource>(
    () => ProspectusRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
