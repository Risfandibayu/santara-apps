import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/core/utils/helpers/rest_helper.dart';
import 'package:santaraapp/utils/api.dart';

abstract class ProspectusRemoteDataSource {
  Future<dynamic> getProspektus({@required String uuid, @required int step});
}

class ProspectusRemoteDataSourceImpl implements ProspectusRemoteDataSource {
  final RestClient client;

  ProspectusRemoteDataSourceImpl({@required this.client});
  @override
  Future getProspektus({String uuid, int step}) async {
    try {
      final result = await client.getExternalUrl(
        url: "$apiPralisting/prospektus/$uuid?step=$step",
      );
      if (result.statusCode == 200) {
        return result.data['data'];
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
