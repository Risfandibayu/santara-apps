// To parse this JSON data, do
//
//     final businessLocationModel = businessLocationModelFromJson(jsonString);

import 'dart:convert';

BusinessLocationModel businessLocationModelFromJson(String str) =>
    BusinessLocationModel.fromJson(json.decode(str));

String businessLocationModelToJson(BusinessLocationModel data) =>
    json.encode(data.toJson());

class BusinessLocationModel {
  BusinessLocationModel({
    this.oldLocations,
    this.oldLocationPictures,
    this.newLocations,
    this.newLocationPictures,
  });

  List<Location> oldLocations;
  List<String> oldLocationPictures;
  List<Location> newLocations;
  List<String> newLocationPictures;

  factory BusinessLocationModel.fromJson(Map<String, dynamic> json) =>
      BusinessLocationModel(
        oldLocations: json["old_locations"] == null
            ? null
            : List<Location>.from(
                json["old_locations"].map((x) => Location.fromJson(x))),
        oldLocationPictures: json["old_location_pictures"] == null
            ? null
            : List<String>.from(json["old_location_pictures"].map((x) => x)),
        newLocations: json["new_locations"] == null
            ? null
            : List<Location>.from(
                json["new_locations"].map((x) => Location.fromJson(x))),
        newLocationPictures: json["new_location_pictures"] == null
            ? null
            : List<String>.from(json["new_location_pictures"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "old_locations": oldLocations == null
            ? null
            : List<dynamic>.from(oldLocations.map((x) => x.toJson())),
        "old_location_pictures": oldLocationPictures == null
            ? null
            : List<dynamic>.from(oldLocationPictures.map((x) => x)),
        "new_locations": newLocations == null
            ? null
            : List<dynamic>.from(newLocations.map((x) => x.toJson())),
        "new_location_pictures": newLocationPictures == null
            ? null
            : List<dynamic>.from(newLocationPictures.map((x) => x)),
      };
}

class Location {
  Location({
    this.name,
    this.address,
  });

  String name;
  String address;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "address": address == null ? null : address,
      };
}
