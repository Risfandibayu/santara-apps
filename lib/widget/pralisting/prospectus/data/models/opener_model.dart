// To parse this JSON data, do
//
//     final openerModel = openerModelFromJson(jsonString);

import 'dart:convert';

OpenerModel openerModelFromJson(String str) =>
    OpenerModel.fromJson(json.decode(str));

String openerModelToJson(OpenerModel data) => json.encode(data.toJson());

class OpenerModel {
  OpenerModel({
    this.companyName,
    this.trademark,
    this.logo,
    this.picture,
    this.telegram,
    this.isPandhega,
  });

  String companyName;
  String trademark;
  String logo;
  String picture;
  String telegram;
  int isPandhega;

  factory OpenerModel.fromJson(Map<String, dynamic> json) => OpenerModel(
        companyName: json["company_name"] == null ? null : json["company_name"],
        trademark: json["trademark"] == null ? null : json["trademark"],
        logo: json["logo"] == null ? null : json["logo"],
        picture: json["picture"] == null ? null : json["picture"],
        telegram: json["telegram"] == null ? null : json["telegram"],
        isPandhega: json["is_pandhega"] == null ? null : json["is_pandhega"],
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName == null ? null : companyName,
        "trademark": trademark == null ? null : trademark,
        "logo": logo == null ? null : logo,
        "picture": picture == null ? null : picture,
        "telegram": telegram == null ? null : telegram,
        "is_pandhega": isPandhega == null ? null : isPandhega,
      };
}
