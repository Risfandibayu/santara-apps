// To parse this JSON data, do
//
//     final businessTeamModel = businessTeamModelFromJson(jsonString);

import 'dart:convert';

BusinessTeamModel businessTeamModelFromJson(String str) =>
    BusinessTeamModel.fromJson(json.decode(str));

String businessTeamModelToJson(BusinessTeamModel data) =>
    json.encode(data.toJson());

class BusinessTeamModel {
  BusinessTeamModel({
    this.emitenTeams,
    this.employee,
    this.emitenPositions,
  });

  List<EmitenTeam> emitenTeams;
  int employee;
  List<EmitenPosition> emitenPositions;

  factory BusinessTeamModel.fromJson(Map<String, dynamic> json) =>
      BusinessTeamModel(
        emitenTeams: json["emiten_teams"] == null
            ? null
            : List<EmitenTeam>.from(
                json["emiten_teams"].map((x) => EmitenTeam.fromJson(x))),
        employee: json["employee"] == null ? null : json["employee"],
        emitenPositions: json["emiten_positions"] == null
            ? null
            : List<EmitenPosition>.from(json["emiten_positions"]
                .map((x) => EmitenPosition.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "emiten_teams": emitenTeams == null
            ? null
            : List<dynamic>.from(emitenTeams.map((x) => x.toJson())),
        "employee": employee == null ? null : employee,
        "emiten_positions": emitenPositions == null
            ? null
            : List<dynamic>.from(emitenPositions.map((x) => x.toJson())),
      };
}

class EmitenPosition {
  EmitenPosition({
    this.name,
    this.employee,
  });

  String name;
  int employee;

  factory EmitenPosition.fromJson(Map<String, dynamic> json) => EmitenPosition(
        name: json["name"] == null ? null : json["name"],
        employee: json["employee"] == null ? null : json["employee"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "employee": employee == null ? null : employee,
      };
}

class EmitenTeam {
  EmitenTeam({
    this.name,
    this.position,
    this.experience,
    this.photo,
    this.photoUrl,
  });

  String name;
  String position;
  String experience;
  String photo;
  String photoUrl;

  factory EmitenTeam.fromJson(Map<String, dynamic> json) => EmitenTeam(
        name: json["name"] == null ? null : json["name"],
        position: json["position"] == null ? null : json["position"],
        experience: json["experience"] == null ? null : json["experience"],
        photo: json["photo"] == null ? null : json["photo"],
        photoUrl: json["photo_url"] == null ? null : json["photo_url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "position": position == null ? null : position,
        "experience": experience == null ? null : experience,
        "photo": photo == null ? null : photo,
        "photo_url": photoUrl == null ? null : photoUrl,
      };
}
