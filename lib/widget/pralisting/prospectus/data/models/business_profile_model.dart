// To parse this JSON data, do
//
//     final businessProfileModel = businessProfileModelFromJson(jsonString);

import 'dart:convert';

BusinessProfileModel businessProfileModelFromJson(String str) =>
    BusinessProfileModel.fromJson(json.decode(str));

String businessProfileModelToJson(BusinessProfileModel data) =>
    json.encode(data.toJson());

class BusinessProfileModel {
  BusinessProfileModel({
    this.companyName,
    this.video,
    this.instagram,
    this.linkInstagram,
    this.facebook,
    this.linkFacebook,
    this.website,
    this.linkWebsite,
    this.review,
  });

  String companyName;
  String video;
  String instagram;
  String linkInstagram;
  String facebook;
  String linkFacebook;
  String website;
  String linkWebsite;
  String review;

  factory BusinessProfileModel.fromJson(Map<String, dynamic> json) =>
      BusinessProfileModel(
        companyName: json["company_name"] == null ? null : json["company_name"],
        video: json["video"] == null ? null : json["video"],
        instagram: json["instagram"] == null ? null : json["instagram"],
        linkInstagram:
            json["link_instagram"] == null ? null : json["link_instagram"],
        facebook: json["facebook"] == null ? null : json["facebook"],
        linkFacebook:
            json["link_facebook"] == null ? null : json["link_facebook"],
        website: json["website"] == null ? null : json["website"],
        linkWebsite: json["link_website"] == null ? null : json["link_website"],
        review: json["review"] == null ? null : json["review"],
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName == null ? null : companyName,
        "video": video == null ? null : video,
        "instagram": instagram == null ? null : instagram,
        "link_instagram": linkInstagram == null ? null : linkInstagram,
        "facebook": facebook == null ? null : facebook,
        "link_facebook": linkFacebook == null ? null : linkFacebook,
        "website": website == null ? null : website,
        "link_website": linkWebsite == null ? null : linkWebsite,
        "review": review == null ? null : review,
      };
}
