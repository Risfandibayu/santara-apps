// To parse this JSON data, do
//
//     final businessHistoryModel = businessHistoryModelFromJson(jsonString);

import 'dart:convert';

BusinessHistoryModel businessHistoryModelFromJson(String str) =>
    BusinessHistoryModel.fromJson(json.decode(str));

String businessHistoryModelToJson(BusinessHistoryModel data) =>
    json.encode(data.toJson());

class BusinessHistoryModel {
  BusinessHistoryModel({
    this.highlightIndustry,
    this.businessDescription,
    this.businessHistory,
    this.advantage,
    this.picture,
    this.expansionPlan,
  });

  String highlightIndustry;
  String businessDescription;
  String businessHistory;
  String advantage;
  String picture;
  String expansionPlan;

  factory BusinessHistoryModel.fromJson(Map<String, dynamic> json) =>
      BusinessHistoryModel(
        highlightIndustry: json["highlight_industry"] == null
            ? null
            : json["highlight_industry"],
        businessDescription: json["business_description"] == null
            ? null
            : json["business_description"],
        businessHistory:
            json["business_history"] == null ? null : json["business_history"],
        advantage: json["advantage"] == null ? null : json["advantage"],
        picture: json["picture"] == null ? null : json["picture"],
        expansionPlan:
            json["expansion_plan"] == null ? null : json["expansion_plan"],
      );

  Map<String, dynamic> toJson() => {
        "highlight_industry":
            highlightIndustry == null ? null : highlightIndustry,
        "business_description":
            businessDescription == null ? null : businessDescription,
        "business_history": businessHistory == null ? null : businessHistory,
        "advantage": advantage == null ? null : advantage,
        "picture": picture == null ? null : picture,
        "expansion_plan": expansionPlan == null ? null : expansionPlan,
      };
}
