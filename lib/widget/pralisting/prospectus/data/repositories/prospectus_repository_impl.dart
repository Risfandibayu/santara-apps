import 'package:santaraapp/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/prospectus/data/datasources/prospectus_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/prospectus/domain/repositories/prospectus_repository.dart';
import 'package:meta/meta.dart';

class ProspectusRepositoryImpl implements ProspectusRepository {
  final NetworkInfo networkInfo;
  final ProspectusRemoteDataSource remoteDataSource;

  ProspectusRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, dynamic>> getProspectus(
      {String uuid, int step}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getProspektus(
          uuid: uuid,
          step: step,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
