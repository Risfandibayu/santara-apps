import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pralisting_step_status_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pre_screening_question_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/domain/repositories/pre_screening_repository.dart';

class GetPreScreeningQuestions
    implements UseCase<List<PreScreeningQuestionModel>, NoParams> {
  final PreScreeningRepository repository;
  GetPreScreeningQuestions(this.repository);

  @override
  Future<Either<Failure, List<PreScreeningQuestionModel>>> call(
      NoParams params) async {
    return await repository.getPreScreeningQuestions();
  }
}

class GetPralistingStepStatus
    implements UseCase<PralistingStepStatusModel, NoParams> {
  final PreScreeningRepository repository;
  GetPralistingStepStatus(this.repository);

  @override
  Future<Either<Failure, PralistingStepStatusModel>> call(
      NoParams params) async {
    return await repository.getPralistingStepStatus();
  }
}

class PostPreScreeningAnswers implements UseCase<String, dynamic> {
  final PreScreeningRepository repository;
  PostPreScreeningAnswers(this.repository);

  @override
  Future<Either<Failure, String>> call(dynamic params) async {
    return await repository.postPreScreeningAnswers(body: params);
  }
}
