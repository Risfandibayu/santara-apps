import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pralisting_step_status_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pre_screening_question_model.dart';
import 'package:meta/meta.dart';

abstract class PreScreeningRepository {
  Future<Either<Failure, List<PreScreeningQuestionModel>>>
      getPreScreeningQuestions();
  Future<Either<Failure, PralistingStepStatusModel>> getPralistingStepStatus();
  Future<Either<Failure, String>> postPreScreeningAnswers({
    @required dynamic body,
  });
}
