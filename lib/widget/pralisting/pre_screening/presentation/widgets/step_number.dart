import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class StepNumberTile extends StatelessWidget {
  final String step;
  final String title;
  StepNumberTile({
    @required this.step,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s18),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: Sizes.s20,
            height: Sizes.s20,
            decoration: BoxDecoration(
              color: Color(0xffBF2D30),
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text(
                "$step",
                style: TextStyle(
                  fontSize: FontSize.s10,
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          SizedBox(width: Sizes.s10),
          Expanded(
            flex: 1,
            child: Container(
              child: Text(
                "$title",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
