import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';

class RejectedMessagePreScreening extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Sizes.s30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            "assets/icon/forbidden.png",
            height: Sizes.s150,
          ),
          SizedBox(height: Sizes.s20),
          Text(
            "Pengajuan Ditolak",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: FontSize.s14,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: Sizes.s20),
          Text(
            "Mohon maaf, bisnis yang Anda ajukan belum memenuhi persyaratan pendanaan.",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: Sizes.s20),
          SantaraMainButton(
            key: Key('back_to_home_btn'),
            child: Text(
              "Kembali ke Home",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
            onTap: () => Navigator.pushReplacementNamed(context, '/index'),
          )
        ],
      ),
    );
  }
}
