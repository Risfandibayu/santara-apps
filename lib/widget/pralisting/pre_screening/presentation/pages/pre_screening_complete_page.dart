import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/widgets/step_number.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/pages/business_prospective_info_page.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';

class PreScreeningCompletePage extends StatelessWidget {
  final String uuid;
  PreScreeningCompletePage({@required this.uuid});
  final List<dynamic> _stepList = [
    {
      "step": "1",
      "title": "Kartu Identitas Owner atau Pengurus",
    },
    {
      "step": "2",
      "title":
          "Dokumen Perusahaan (Izin Usaha, NIB/SKU, Akta Pendirian, Akta Perubahan)",
    },
    {
      "step": "3",
      "title": "Dokumen Legalitas Tempat Usaha",
    },
    {
      "step": "4",
      "title": "Laporan Neraca dan Laba-Rugi",
    },
    {
      "step": "5",
      "title": "Lampiran Validasi POS atau Rekening Koran 6 bulan terakhir",
    },
    {
      "step": "6",
      "title": "Lampiran Laba Rugi Bulanan (tahun terakhir)",
    },
    {
      "step": "7",
      "title": "Rencana Anggaran Biaya",
    },
    {
      "step": "8",
      "title": "Proyeksi Keuangan 5 Tahun Kedepan",
    },
    {
      "step": "9",
      "title": "Foto & Video Tentang Usaha Anda",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.all(Sizes.s20),
        child: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(Sizes.s16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(Sizes.s5),
                    color: Color(0xffFFF1DB),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Form Pendaftaran Bisnis Santara",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: FontSize.s14,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      Text(
                        "Pengisian form berkisar 20 hingga 30 menit",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: FontSize.s12,
                        ),
                      ),
                      SizedBox(height: Sizes.s5),
                      Text(
                        "Seluruh data yang di submit hanya untuk kepentingan review dan trade checking. Keamanan informasi dijamin oleh Santara.",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: FontSize.s12,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: Sizes.s25),
                Text(
                  "Persiapkan Dokumen-Dokumen Berikut ",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s14,
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _stepList.length,
                  itemBuilder: (context, index) {
                    return StepNumberTile(
                      step: _stepList[index]["step"],
                      title: _stepList[index]["title"],
                    );
                  },
                ),
                SizedBox(height: Sizes.s20),
                SantaraMainButton(
                  key: Key('next_step_pre_screening'),
                  child: Text(
                    "Selanjutnya",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                  onTap: () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BusinessProspectiveInfoPage(
                        uuid: "$uuid",
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
