import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pralisting_step_status_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/blocs/pre_screening_bloc.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/pages/pre_screening_complete_page.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/widgets/rejected_message.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/pages/business_dev_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/pages/business_info_1_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/pages/business_info_2_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/financial_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/pages/management_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/media_page.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/pages/share_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/pages/terms_conditions_page.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import '../../../../../injector_container.dart';

class PreScreeningPage extends StatefulWidget {
  @override
  _PreScreeningPageState createState() => _PreScreeningPageState();
}

class _PreScreeningPageState extends State<PreScreeningPage> {
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;
  PreScreeningFormBloc bloc;

  final scrollDirection = Axis.vertical;
  Widget _buildRadioButton({
    @required int index,
    @required String title,
    @required SelectFieldBloc bloc,
  }) {
    return fieldWrapper.wrap(
      index: index,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
            child: Text(
              "$title",
              style: TextStyle(
                color: Colors.black,
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            child: RadioButtonGroupFieldBlocBuilder(
              selectFieldBloc: bloc,
              itemBuilder: (context, value) => value,
              padding: EdgeInsets.zero,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding:
                    EdgeInsets.only(left: Sizes.s5, right: Sizes.s5),
                errorMaxLines: 5,
                isDense: true,
                isCollapsed: true,
              ),
            ),
          ),
          SizedBox(height: Sizes.s15),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = sl<PreScreeningFormBloc>();
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: PralistingAppbar(
          title: "Pre-Screening",
          showSteps: false,
          uuid: "",
        ),
      ),
      body: BlocProvider(
        create: (_) => bloc,
        child: Builder(
          builder: (context) {
            // ignore: close_sinks
            final bloc = context.bloc<PreScreeningFormBloc>();
            return FormBlocListener<PreScreeningFormBloc, FormResult,
                FormResult>(
              onLoaded: (context, state) {
                PralistingHelper.handleAutoScroll(
                  fieldWrapper: fieldWrapper,
                  state: state,
                );
              },
              onSubmitting: (context, state) {
                PopupHelper.showLoading(context);
              },
              onSuccess: (context, state) {
                final form = state.successResponse;
                var nextPage;
                if (form.status == FormStatus.submit_success) {
                  Navigator.pop(context);
                  form.message.isNotEmpty
                      ? ToastHelper.showSuccessToast(context, form.message)
                      : null;
                  if (form.data != null) {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PreScreeningCompletePage(
                          uuid: form.data,
                        ),
                      ),
                    );
                  }
                }

                if (form.status == FormStatus.get_success) {
                  form.message.isNotEmpty
                      ? ToastHelper.showSuccessToast(context, form.message)
                      : null;
                  final data = form.data;
                  int lastStep = data.lastStep;
                  switch (lastStep) {
                    case 1:
                      nextPage = PreScreeningCompletePage(
                        uuid: data.uuid,
                      );
                      break;
                    case 2:
                      nextPage = BusinessInfoOnePage(
                        uuid: data.uuid,
                      );
                      break;
                    case 3:
                      nextPage = BusinsessInfoTwoPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 4:
                      nextPage = ManagementInfoPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 5:
                      nextPage = FinancialInfoPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 6:
                      nextPage = BusinessDevPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 7:
                      nextPage = BusinessDevPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 8:
                      nextPage = ShareInfoPage(
                        uuid: data.uuid,
                      );
                      break;
                    case 9:
                      nextPage = MediaPage(
                        uuid: data.uuid,
                      );
                      break;
                    default:
                      nextPage = TermsConditionsPage(
                        uuid: data.uuid,
                      );
                      break;
                  }
                }
                if (nextPage != null) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => nextPage,
                    ),
                  );
                }
              },
              onFailure: (context, state) {
                final form = state.failureResponse;
                Navigator.pop(context);
                form.message.isNotEmpty
                    ? ToastHelper.showFailureToast(context, form.message)
                    : null;
                if (form.message.isEmpty) {
                  showModalBottomSheet(
                    isDismissible: false,
                    isScrollControlled: false,
                    enableDrag: false,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(5.0),
                      ),
                    ),
                    context: context,
                    builder: (context) {
                      return RejectedMessagePreScreening();
                    },
                  );
                }
              },
              onSubmissionFailed: (context, state) {
                PralistingHelper.handleAutoScroll(
                  fieldWrapper: fieldWrapper,
                  state: state,
                );
              },
              child: BlocBuilder<PreScreeningFormBloc, FormBlocState>(
                builder: (context, state) {
                  if (state is FormBlocLoading) {
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                  } else {
                    return BlocBuilder<ListFieldBloc<SelectFieldBloc>,
                        ListFieldBlocState<SelectFieldBloc>>(
                      bloc: bloc.preScreeningQuestions,
                      builder: (context, state) {
                        if (state.fieldBlocs != null &&
                            state.fieldBlocs.length > 0) {
                          return Container(
                            color: Colors.white,
                            width: double.maxFinite,
                            height: double.maxFinite,
                            child: SingleChildScrollView(
                              scrollDirection: scrollDirection,
                              controller: controller,
                              physics: BouncingScrollPhysics(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(Sizes.s20),
                                    child: Text(
                                      "Silakan mengisi pernyataan dibawah ini sesuai dengan kondisi usaha Anda.",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: FontSize.s14,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: Sizes.s20),
                                  ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    padding: EdgeInsets.zero,
                                    itemCount: state.fieldBlocs.length,
                                    itemBuilder: (context, index) {
                                      var teks = state
                                          .fieldBlocs[index].state.extraData;
                                      return _buildRadioButton(
                                        index: index,
                                        title: "$teks",
                                        bloc: state.fieldBlocs[index],
                                      );
                                    },
                                  ),
                                  SizedBox(height: Sizes.s10),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      left: Sizes.s20,
                                      right: Sizes.s20,
                                      bottom: Sizes.s20,
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: SantaraMainButton(
                                        key: Key('submit_pre_screening'),
                                        onTap: () {
                                          print(
                                            ">> Submit ${bloc.state.canSubmit}",
                                          );
                                          bloc.submit();
                                        },
                                        child: Text(
                                          "Selanjutnya",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontSize: FontSize.s14,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    );
                  }
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
