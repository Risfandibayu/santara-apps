import 'dart:convert';

import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/domain/usecases/pre_screening_usecases.dart';
import 'package:meta/meta.dart';

class PreScreeningFormBloc extends FormBloc<FormResult, FormResult> {
  final GetPreScreeningQuestions getPreScreeningQuestions;
  final PostPreScreeningAnswers postPreScreeningAnswers;
  final GetPralistingStepStatus getPralistingStepStatus;

  final preScreeningQuestions = ListFieldBloc<SelectFieldBloc>(name: 'answers');

  PreScreeningFormBloc({
    @required this.getPreScreeningQuestions,
    @required this.postPreScreeningAnswers,
    @required this.getPralistingStepStatus,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        preScreeningQuestions,
      ],
    );

    distinct(
      (previous, current) =>
          previous.toJson().toString() != current.toString().toString(),
    ).listen((state) {
      print(state.toJson());
    });
  }

  getPralistingStep() async {
    final result = await getPralistingStepStatus(NoParams());
    if (result.isRight()) {
      emitSuccess(
        canSubmitAgain: false,
        successResponse: FormResult(
          status: FormStatus.get_success,
          message: "",
          data: result.getOrElse(null),
        ),
      );
    } else {
      await initQuestions();
    }
  }

  initQuestions() async {
    try {
      final result = await getPreScreeningQuestions(NoParams());
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && data.length > 0) {
          data.forEach((element) {
            preScreeningQuestions.addFieldBloc(
              SelectFieldBloc(
                name: "${element.id}",
                extraData: element.text,
                items: ['Ya', 'Tidak'],
                validators: [PralistingValidationHelper.required],
              ),
            );
          });
          emitLoaded();
        } else {
          emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_error,
              message: "Daftar pertanyaan tidak ada!",
            ),
          );
        }
      } else {
        result.leftMap((l) {
          printFailure(l);
          emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_error,
              message: "${l.message}",
            ),
          );
        });
      }
    } catch (e, stack) {
      print(">> Here u are");
      santaraLog(e, stack);
      emitFailure(failureResponse: e);
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }

  @override
  void onLoading() async {
    super.onLoading();
    getPralistingStep();
  }

  postData() async {
    try {
      emitSubmitting();
      List<dynamic> payloads = [];
      // Parse Json Data
      preScreeningQuestions.state.fieldBlocs.forEach((element) {
        var data = {
          "id": int.parse(element.state.name),
          "answer": element.state.value == "Ya" ? 1 : 0,
        };
        payloads.add(data);
      });
      var body = {"answers": payloads};
      final result = await postPreScreeningAnswers(body);
      if (result.isRight()) {
        var response = json.decode(result.getOrElse(null));
        emitSuccess(
          canSubmitAgain: true,
          successResponse: FormResult(
            status: FormStatus.submit_success,
            message: response["meta"]["message"],
            data: response["data"]["uuid"],
          ),
        );
      } else {
        result.leftMap((l) {
          if (l.apiStatus != 400) {
            emitFailure(
              failureResponse: FormResult(
                status: FormStatus.submit_error,
                message: "${l.message}",
              ),
            );
          } else {
            emitFailure(
              failureResponse: FormResult(
                status: FormStatus.submit_error,
                message: "",
              ),
            );
          }
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "$e",
        ),
      );
    }
  }

  @override
  void onSubmitting() async {
    if (state.isValid()) {
      await postData();
    }
  }
}
