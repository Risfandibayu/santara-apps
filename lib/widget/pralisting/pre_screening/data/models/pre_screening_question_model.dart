// To parse this JSON data, do
//
//     final preScreeningQuestionModel = preScreeningQuestionModelFromJson(jsonString);

import 'dart:convert';

PreScreeningQuestionModel preScreeningQuestionModelFromJson(String str) =>
    PreScreeningQuestionModel.fromJson(json.decode(str));

String preScreeningQuestionModelToJson(PreScreeningQuestionModel data) =>
    json.encode(data.toJson());

class PreScreeningQuestionModel {
  PreScreeningQuestionModel({
    this.id,
    this.text,
  });

  int id;
  String text;

  factory PreScreeningQuestionModel.fromJson(Map<String, dynamic> json) =>
      PreScreeningQuestionModel(
        id: json["id"] == null ? null : json["id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": text == null ? null : text,
      };
}
