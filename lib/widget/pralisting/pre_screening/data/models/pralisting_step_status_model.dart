// To parse this JSON data, do
//
//     final pralistingStepStatusModel = pralistingStepStatusModelFromJson(jsonString);

import 'dart:convert';

PralistingStepStatusModel pralistingStepStatusModelFromJson(String str) =>
    PralistingStepStatusModel.fromJson(json.decode(str));

String pralistingStepStatusModelToJson(PralistingStepStatusModel data) =>
    json.encode(data.toJson());

class PralistingStepStatusModel {
  PralistingStepStatusModel({
    this.uuid,
    this.lastStep,
  });

  String uuid;
  int lastStep;

  factory PralistingStepStatusModel.fromJson(Map<String, dynamic> json) =>
      PralistingStepStatusModel(
        uuid: json["uuid"] == null ? null : json["uuid"],
        lastStep: json["last_step"] == null ? null : json["last_step"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid == null ? null : uuid,
        "last_step": lastStep == null ? null : lastStep,
      };
}
