import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pralisting_step_status_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pre_screening_question_model.dart';
import 'package:meta/meta.dart';

abstract class PreScreeningRemoteDataSource {
  Future<List<PreScreeningQuestionModel>> getPreScreeningQuestions();
  Future<PralistingStepStatusModel> getPralistingStepStatus();
  Future<String> postPreScreeningAnswers({@required dynamic body});
}

class PreScreeningRemoteDataSourceImpl implements PreScreeningRemoteDataSource {
  final RestClient client;
  PreScreeningRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<PreScreeningQuestionModel>> getPreScreeningQuestions() async {
    try {
      final result = await client.getExternalUrl(
          url: "$apiPralisting/list-questions?page=1&limit=200");
      if (result.statusCode == 200) {
        List<PreScreeningQuestionModel> questions =
            List<PreScreeningQuestionModel>();
        result.data["data"].forEach((val) {
          questions.add(PreScreeningQuestionModel.fromJson(val));
        });
        return questions;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingStepStatusModel> getPralistingStepStatus() async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/is-pralisting");
      if (result.statusCode == 200) {
        return PralistingStepStatusModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<String> postPreScreeningAnswers({dynamic body}) async {
    try {
      print(body);
      final result = await client.postExternalUrl(
        url: "$apiPralisting/answer",
        body: body,
      );
      if (result.statusCode == 200) {
        return json.encode(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
