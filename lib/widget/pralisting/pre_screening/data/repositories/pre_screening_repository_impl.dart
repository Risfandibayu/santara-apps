import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/datasources/pre_screening_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pralisting_step_status_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/models/pre_screening_question_model.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/domain/repositories/pre_screening_repository.dart';
import 'package:meta/meta.dart';

class PreScreeningRepositoryImpl implements PreScreeningRepository {
  final NetworkInfo networkInfo;
  final PreScreeningRemoteDataSource remoteDataSource;

  PreScreeningRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<PreScreeningQuestionModel>>>
      getPreScreeningQuestions() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPreScreeningQuestions();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingStepStatusModel>>
      getPralistingStepStatus() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPralistingStepStatus();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, String>> postPreScreeningAnswers(
      {@required dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.postPreScreeningAnswers(body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
