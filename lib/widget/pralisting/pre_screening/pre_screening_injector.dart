import 'package:santaraapp/widget/pralisting/pre_screening/data/datasources/pre_screening_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/data/repositories/pre_screening_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/domain/repositories/pre_screening_repository.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/domain/usecases/pre_screening_usecases.dart';

import '../../../injector_container.dart';
import 'presentation/blocs/pre_screening_bloc.dart';

void initPreScreeningInjector() {
  sl.registerFactory(
    () => PreScreeningFormBloc(
      getPreScreeningQuestions: sl(),
      getPralistingStepStatus: sl(),
      postPreScreeningAnswers: sl(),
    ),
  );

  sl.registerLazySingleton<PreScreeningRepository>(
    () => PreScreeningRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<PreScreeningRemoteDataSource>(
    () => PreScreeningRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetPreScreeningQuestions(sl()));
  sl.registerLazySingleton(() => PostPreScreeningAnswers(sl()));
  sl.registerLazySingleton(() => GetPralistingStepStatus(sl()));
}
