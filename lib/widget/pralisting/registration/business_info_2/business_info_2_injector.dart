import '../../../../injector_container.dart';
import 'data/datasources/business_info_2_remote_data_source.dart';
import 'data/repositories/business_info_2_repository_impl.dart';
import 'domain/repositories/business_info_2_repository.dart';
import 'domain/usecases/business_info_2_usecases.dart';
import 'presentation/blocs/business_info_2_bloc.dart';

void initBusinessInfoTwoInjector() {
  // Business Info 2
  sl.registerFactory(
    () => BusinessInfoTwoBloc(
      getBanks: sl(),
      updateBusinessInfoTwo: sl(),
      getListBusinessOptions: sl(),
      getBusinessInfoTwo: sl(),
    ),
  );

  // Business Info 2
  sl.registerLazySingleton(() => GetBanks(sl()));
  sl.registerLazySingleton(() => UpdateBusinessInfoTwo(sl()));
  sl.registerLazySingleton(() => GetBusinessInfoTwo(sl()));

  // Business Info 2
  sl.registerLazySingleton<BusinessInfoTwoRepository>(
    () => BusinessInfoTwoRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessInfoTwoRemoteDataSource>(
    () => BusinessInfoTwoRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
