import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/models/business_info_2_model.dart';

abstract class BusinessInfoTwoRepository {
  Future<Either<Failure, List<BankInvestor>>> getBanks();
  Future<Either<Failure, PralistingMetaModel>> updateBusinessInfoTwo({
    @required dynamic body,
    @required String uuid,
  });
  Future<Either<Failure, BusinessInfoTwoModel>> getBusinessInfoTwo({
    @required String uuid,
  });
}
