import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/models/business_info_2_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/domain/repositories/business_info_2_repository.dart';

class GetBanks implements UseCase<List<BankInvestor>, NoParams> {
  final BusinessInfoTwoRepository repository;
  GetBanks(this.repository);

  @override
  Future<Either<Failure, List<BankInvestor>>> call(NoParams noParams) async {
    return await repository.getBanks();
  }
}

class UpdateBusinessInfoTwo implements UseCase<PralistingMetaModel, dynamic> {
  final BusinessInfoTwoRepository repository;
  UpdateBusinessInfoTwo(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateBusinessInfoTwo(
      body: params['body'],
      uuid: params['uuid'],
    );
  }
}

class GetBusinessInfoTwo implements UseCase<BusinessInfoTwoModel, String> {
  final BusinessInfoTwoRepository repository;
  GetBusinessInfoTwo(this.repository);

  @override
  Future<Either<Failure, BusinessInfoTwoModel>> call(String uuid) async {
    return await repository.getBusinessInfoTwo(uuid: uuid);
  }
}
