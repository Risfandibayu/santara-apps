import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/datasources/business_info_2_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/models/business_info_2_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/domain/repositories/business_info_2_repository.dart';
import 'package:meta/meta.dart';

class BusinessInfoTwoRepositoryImpl implements BusinessInfoTwoRepository {
  final NetworkInfo networkInfo;
  final BusinessInfoTwoRemoteDataSource remoteDataSource;

  BusinessInfoTwoRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, List<BankInvestor>>> getBanks() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBanks();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateBusinessInfoTwo(
      {dynamic body, String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateBusinessInfoTwo(
          body: body,
          uuid: uuid,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, BusinessInfoTwoModel>> getBusinessInfoTwo(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBusinessInfoTwo(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
