// To parse this JSON data, do
//
//     final businessInfoTwoModel = businessInfoTwoModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

BusinessInfoTwoModel businessInfoTwoModelFromJson(String str) =>
    BusinessInfoTwoModel.fromJson(json.decode(str));

String businessInfoTwoModelToJson(BusinessInfoTwoModel data) =>
    json.encode(data.toJson());

class BusinessInfoTwoModel {
  BusinessInfoTwoModel({
    this.businessEntity,
    this.businessDocuments,
    this.businessStartDate,
    this.branchCompany,
    this.owningProof,
    this.businessLocationStatus,
    this.hasBankLoan,
    this.emitenBankLoans,
    this.paymentAspectLabel,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  String businessEntity;
  List<BusinessDocument> businessDocuments;
  String businessStartDate;
  int branchCompany;
  String owningProof;
  String businessLocationStatus;
  int hasBankLoan;
  List<EmitenBankLoan> emitenBankLoans;
  List<PaymentAspectLabel> paymentAspectLabel;

  Submission submission;
  bool isComplete;
  bool isClean;
  factory BusinessInfoTwoModel.fromJson(Map<String, dynamic> json) =>
      BusinessInfoTwoModel(
        businessEntity:
            json["business_entity"] == null ? null : json["business_entity"],
        businessDocuments: json["business_documents"] == null
            ? null
            : List<BusinessDocument>.from(json["business_documents"]
                .map((x) => BusinessDocument.fromJson(x))),
        businessStartDate: json["business_start_date"] == null
            ? null
            : json["business_start_date"],
        branchCompany:
            json["branch_company"] == null ? null : json["branch_company"],
        owningProof: json["owning_proof"] == null ? null : json["owning_proof"],
        businessLocationStatus: json["business_location_status"] == null
            ? null
            : json["business_location_status"],
        hasBankLoan:
            json["has_bank_loan"] == null ? null : json["has_bank_loan"],
        emitenBankLoans: json["emiten_bank_loans"] == null
            ? null
            : List<EmitenBankLoan>.from(json["emiten_bank_loans"]
                .map((x) => EmitenBankLoan.fromJson(x))),
        paymentAspectLabel: json["payment_aspect_label"] == null
            ? null
            : List<PaymentAspectLabel>.from(json["payment_aspect_label"]
                .map((x) => PaymentAspectLabel.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "business_entity": businessEntity == null ? null : businessEntity,
        "business_documents": businessDocuments == null
            ? null
            : List<dynamic>.from(businessDocuments.map((x) => x.toJson())),
        "business_start_date":
            businessStartDate == null ? null : businessStartDate,
        "branch_company": branchCompany == null ? null : branchCompany,
        "owning_proof": owningProof == null ? null : owningProof,
        "business_location_status":
            businessLocationStatus == null ? null : businessLocationStatus,
        "has_bank_loan": hasBankLoan == null ? null : hasBankLoan,
        "emiten_bank_loans": emitenBankLoans == null
            ? null
            : List<dynamic>.from(emitenBankLoans.map((x) => x.toJson())),
        "payment_aspect_label": paymentAspectLabel == null
            ? null
            : List<dynamic>.from(paymentAspectLabel.map((x) => x.toJson())),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class BusinessDocument {
  BusinessDocument({
    this.type,
    this.files,
  });

  String type;
  List<FileElement> files;

  factory BusinessDocument.fromJson(Map<String, dynamic> json) =>
      BusinessDocument(
        type: json["type"] == null ? null : json["type"],
        files: json["files"] == null
            ? null
            : List<FileElement>.from(
                json["files"].map((x) => FileElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "files": files == null
            ? null
            : List<dynamic>.from(files.map((x) => x.toJson())),
      };
}

class FileElement {
  FileElement({
    this.emitenDocument,
    this.emitenDocumentUrl,
  });

  String emitenDocument;
  String emitenDocumentUrl;

  factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        emitenDocument:
            json["emiten_document"] == null ? null : json["emiten_document"],
        emitenDocumentUrl: json["emiten_document_url"] == null
            ? null
            : json["emiten_document_url"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_document": emitenDocument == null ? null : emitenDocument,
        "emiten_document_url":
            emitenDocumentUrl == null ? null : emitenDocumentUrl,
      };
}

class EmitenBankLoan {
  EmitenBankLoan({
    this.bankInvestorsId,
    this.bankName,
    this.amount,
    this.tenor,
    this.remaining,
  });

  int bankInvestorsId;
  String bankName;
  int amount;
  int tenor;
  int remaining;

  factory EmitenBankLoan.fromJson(Map<String, dynamic> json) => EmitenBankLoan(
        bankInvestorsId: json["bank_investors_id"] == null
            ? null
            : json["bank_investors_id"],
        bankName: json["bank_name"] == null ? null : json["bank_name"],
        amount: json["amount"] == null ? null : json["amount"],
        tenor: json["tenor"] == null ? null : json["tenor"],
        remaining: json["remaining"] == null ? null : json["remaining"],
      );

  Map<String, dynamic> toJson() => {
        "bank_investors_id": bankInvestorsId == null ? null : bankInvestorsId,
        "bank_name": bankName == null ? null : bankName,
        "amount": amount == null ? null : amount,
        "tenor": tenor == null ? null : tenor,
        "remaining": remaining == null ? null : remaining,
      };
}

class PaymentAspectLabel {
  PaymentAspectLabel({
    this.id,
    this.text,
  });

  int id;
  String text;

  factory PaymentAspectLabel.fromJson(Map<String, dynamic> json) =>
      PaymentAspectLabel(
        id: json["id"] == null ? null : json["id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": text == null ? null : text,
      };
}
