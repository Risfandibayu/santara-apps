import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/models/Bank.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/models/business_info_2_model.dart';

abstract class BusinessInfoTwoRemoteDataSource {
  Future<List<BankInvestor>> getBanks();
  Future<PralistingMetaModel> updateBusinessInfoTwo({
    @required dynamic body,
    @required String uuid,
  });
  Future<BusinessInfoTwoModel> getBusinessInfoTwo({@required String uuid});
}

class BusinessInfoTwoRemoteDataSourceImpl
    implements BusinessInfoTwoRemoteDataSource {
  final RestClient client;
  BusinessInfoTwoRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<BankInvestor>> getBanks() async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiLocal/banks/bank-investors");
      if (result.statusCode == 200) {
        List<BankInvestor> banks = List<BankInvestor>();
        result.data.forEach((val) {
          banks.add(BankInvestor.fromJson(val));
        });
        return banks;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateBusinessInfoTwo(
      {dynamic body, String uuid}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/step3/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<BusinessInfoTwoModel> getBusinessInfoTwo({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step3/$uuid");
      if (result.statusCode == 200) {
        return BusinessInfoTwoModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
