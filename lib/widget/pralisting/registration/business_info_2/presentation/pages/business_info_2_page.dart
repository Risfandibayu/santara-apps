import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_branch_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_certificate_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_entity_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_established_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_loan_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_location_status_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_payment_aspect_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/pages/management_info_page.dart';

import '../../../../../../injector_container.dart';

class BusinsessInfoTwoPage extends StatefulWidget {
  final String uuid;
  BusinsessInfoTwoPage({@required this.uuid});
  @override
  _BusinsessInfoTwoPageState createState() => _BusinsessInfoTwoPageState();
}

class _BusinsessInfoTwoPageState extends State<BusinsessInfoTwoPage> {
  BusinessInfoTwoBloc businessInfoTwoBloc;
  final _controller = ScrollController();

  @override
  void initState() {
    super.initState();
    businessInfoTwoBloc = sl<BusinessInfoTwoBloc>();
    businessInfoTwoBloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = businessInfoTwoBloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Informasi Usaha (2)",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "3 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider<BusinessInfoTwoBloc>(
          create: (_) => businessInfoTwoBloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<BusinessInfoTwoBloc>();
              return FormBlocListener<BusinessInfoTwoBloc, FormResult,
                  FormResult>(
                onLoaded: (context, state) async {
                  // setState(() {});
                  await Future.delayed(Duration(milliseconds: 500));
                  // _controller.jumpTo(0);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                  // print(">> submitting!");
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ManagementInfoPage(
                          uuid: "${widget.uuid}",
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  } else {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<BusinessInfoTwoBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        color: Colors.white,
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: SingleChildScrollView(
                          controller: _controller,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BusinessEntityField(bloc: bloc),
                              BusinessEstablishedField(bloc: bloc),
                              BusinessBranchField(bloc: bloc),
                              BusinessLocationStatusField(bloc: bloc),
                              BusinessCertificateField(bloc: bloc),
                              BusinessLoanField(bloc: bloc),
                              BusinessPaymentAspectField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
