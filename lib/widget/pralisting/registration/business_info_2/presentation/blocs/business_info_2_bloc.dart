import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/data/models/submission_detail_model.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/usecases/business_info_1_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/data/models/business_info_2_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/domain/usecases/business_info_2_usecases.dart';

class BusinessInfoTwoBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetBanks getBanks;
  final UpdateBusinessInfoTwo updateBusinessInfoTwo;
  final GetListBusinessOptions getListBusinessOptions;
  final GetBusinessInfoTwo getBusinessInfoTwo;

  List<SearchData> banks = [];
  static List<FileUploadModel> certs = [];
  final numberFormat = NumberFormat("###,###,###", "en_us");

  final businessEntity = SelectFieldBloc(
    name: "business_entity",
    validators: [PralistingValidationHelper.required],
    items: ['Perseorangan', 'CV', 'PT'],
  );

  // Individual Fields
  var ownerIdCard = TextFieldBloc(
    name: "owner_id_card",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  var ownerTaxID = TextFieldBloc(
    name: "owner_tax_id",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  var ownerNibID = TextFieldBloc(
    name: "owner_nib_id",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  // CV
  final deedOfIncorporationCv = TextFieldBloc(
    name: "deed_of_incorporation_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final deedOfChangeCv = TextFieldBloc(
    name: "deed_of_change_cv",
    validators: [],
    extraData: certs,
  );

  final nibIdCv = TextFieldBloc(
    name: "nib_id_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final businessPermitCv = TextFieldBloc(
    name: "business_permit_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final taxIdCv = TextFieldBloc(
    name: "tax_id_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final adminIdCardCv = TextFieldBloc(
    name: "admin_id_card_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final adminTaxIdCv = TextFieldBloc(
    name: "admin_tax_id_cv",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  // PT
  final deedOfIncorporationPt = TextFieldBloc(
    name: "deed_of_incorporation_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final deedOfChangePt = TextFieldBloc(
    name: "deed_of_change_pt",
    validators: [],
    extraData: certs,
  );

  final nibIdPt = TextFieldBloc(
    name: "nib_id_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final businessPermitPt = TextFieldBloc(
    name: "business_permit_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final taxIdPt = TextFieldBloc(
    name: "tax_id_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final adminIdCardPt = TextFieldBloc(
    name: "admin_id_card_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );

  final adminTaxIdPt = TextFieldBloc(
    name: "admin_tax_id_pt",
    validators: [PralistingValidationHelper.required],
    extraData: certs,
  );
  // End

  final businessStartDate = InputFieldBloc<DateTime, Object>(
    name: "business_start_date",
    validators: [PralistingValidationHelper.required],
    toJson: (value) => value.toString().replaceAll(" 00:00:00.000", ""),
  );

  final branchCompany = TextFieldBloc(
    name: "branch_company",
    validators: [PralistingValidationHelper.required],
  );

  final businessLocationStatus = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_location_status',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
    items: rawBusinessLocationStatus,
  );

  final businessCertificate = TextFieldBloc(
    name: "business_certificate",
    validators: [PralistingValidationHelper.required],
  );

  final hasBankLoan = SelectFieldBloc<String, String>(
    name: 'business_bank_loan',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
    items: [
      "Ya, Memiliki Pinjaman Bank",
      "Tidak, Tidak ada pinjaman bank",
    ],
  );

  final businessLoans = ListFieldBloc<LoanFieldBloc>(name: 'emiten_bank_loans');

  final paymentAspect = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_payment_aspect',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  //
  List<FieldBloc> individualFields;

  List<FieldBloc> cvFields;

  List<FieldBloc> companyFields;

  BusinessInfoTwoBloc({
    @required this.getBanks,
    @required this.updateBusinessInfoTwo,
    @required this.getListBusinessOptions,
    @required this.getBusinessInfoTwo,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        businessEntity,
        //
        businessStartDate,
        branchCompany,
        businessLocationStatus,
        businessCertificate,
        hasBankLoan,
        businessLoans,
        paymentAspect,
      ],
    );

    individualFields = [
      // Individual
      ownerIdCard,
      ownerTaxID,
      ownerNibID,
    ];

    cvFields = [
      // CV
      deedOfIncorporationCv,
      deedOfChangeCv,
      nibIdCv,
      businessPermitCv,
      taxIdCv,
      adminIdCardCv,
      adminTaxIdCv,
    ];

    companyFields = [
      // PT
      deedOfIncorporationPt,
      deedOfChangePt,
      nibIdPt,
      businessPermitPt,
      taxIdPt,
      adminIdCardPt,
      adminTaxIdPt,
    ];

    hasBankLoan.onValueChanges(onData: (prev, next) {
      if (next != prev) {
        if (next.value.contains("Ya")) {
          addBusinessLoan();
        } else {
          for (var i = 0; i < businessLoans.state.fieldBlocs.length; i++) {
            businessLoans.removeFieldBlocAt(i);
          }
        }
      }
    });
  }

  clearFieldsFile(List<dynamic> fields) {
    fields.forEach((field) {
      // var f = field as SingleFieldBloc;
      field.updateExtraData(null);
      field.updateValue(null);
      field.clear();
    });
  }

  void addIndividual() {
    var removedFields = cvFields + companyFields;
    clearFieldsFile(removedFields);
    removeFieldBlocs(fieldBlocs: removedFields);
    addFieldBlocs(fieldBlocs: individualFields);
  }

  void addCv() {
    var removedFields = individualFields + companyFields;
    clearFieldsFile(removedFields);
    removeFieldBlocs(fieldBlocs: removedFields);
    addFieldBlocs(fieldBlocs: cvFields);
  }

  void addCompany() {
    var removedFields = individualFields + cvFields;
    clearFieldsFile(removedFields);
    removeFieldBlocs(fieldBlocs: removedFields);
    addFieldBlocs(fieldBlocs: companyFields);
  }

  void addBusinessLoan() {
    businessLoans.addFieldBloc(LoanFieldBloc(
      name: 'businessRisk',
      bankName: SelectFieldBloc(
        name: 'bank',
        items: banks,
        validators: [PralistingValidationHelper.required],
      ),
      loanAmount: TextFieldBloc(
        name: 'amount',
        validators: [PralistingValidationHelper.required],
      ),
      remainingPeriod: TextFieldBloc(
        name: 'remaining_period',
        validators: [PralistingValidationHelper.required],
      ),
      tenor: TextFieldBloc(
        name: 'tenor',
        validators: [PralistingValidationHelper.required],
      ),
    ));
  }

  void removeBusinessLoan(int index) {
    businessLoans.removeFieldBlocAt(index);
  }

  initializeBanks() async {
    final bankFailure = FormResult(
      status: FormStatus.get_field_data_error,
      message: "Tidak dapat menerima data bank",
    );
    try {
      final result = await getBanks(NoParams());
      if (result.isRight()) {
        final regencies = result.getOrElse(null);

        List<SearchData> _datas = [];
        regencies.forEach((element) {
          _datas.add(SearchData(
            id: '${element.id}',
            uuid: '${element.code}',
            name: '${element.bank}',
          ));
        });
        banks = _datas;
      } else {
        emitFailure(failureResponse: bankFailure);
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      emitFailure(failureResponse: bankFailure);
    }
  }

  updateFileValue({
    @required dynamic field,
    @required List<FileElement> files,
  }) {
    List<FileUploadModel> uploaded = [];
    files.forEach((f) {
      uploaded.add(FileUploadModel(
        url: f.emitenDocumentUrl,
        filename: f.emitenDocument,
      ));
    });
    if (field != null && uploaded != null && uploaded.length > 0) {
      field.updateExtraData(uploaded);
    }
  }

  getFieldByEntity({
    @required String entity,
    dynamic perseorangan,
    dynamic cv,
    dynamic pt,
  }) {
    switch (entity) {
      case "Perseorangan":
        return perseorangan;
        break;
      case "CV":
        return cv;
        break;
      case "PT":
        return pt;
        break;
      default:
        return null;
        break;
    }
  }

  setDocuments({
    @required String entity,
    @required List<BusinessDocument> businessDocuments,
  }) {
    if (businessDocuments != null && businessDocuments.length > 0) {
      businessDocuments.forEach((d) {
        switch (d.type) {
          case "ktp_owner":
            updateFileValue(
              field: ownerIdCard,
              files: d.files,
            );
            break;
          case "npwp":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                perseorangan: ownerTaxID,
                cv: taxIdCv,
                pt: taxIdPt,
              ),
              files: d.files,
            );
            break;
          case "nib":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                perseorangan: ownerNibID,
                cv: nibIdCv,
                pt: nibIdPt,
              ),
              files: d.files,
            );
            break;
          case "akta_pendirian":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                cv: deedOfIncorporationCv,
                pt: deedOfIncorporationPt,
              ),
              files: d.files,
            );
            break;
          case "akta_perubahan":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                cv: deedOfChangeCv,
                pt: deedOfChangePt,
              ),
              files: d.files,
            );
            break;
          case "izin_usaha":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                cv: businessPermitCv,
                pt: businessPermitPt,
              ),
              files: d.files,
            );
            break;
          case "ktp_pengurus":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                cv: adminIdCardCv,
                pt: adminIdCardPt,
              ),
              files: d.files,
            );
            break;
          case "npwp_pengurus":
            updateFileValue(
              field: getFieldByEntity(
                entity: entity,
                cv: adminTaxIdCv,
                pt: adminTaxIdPt,
              ),
              files: d.files,
            );
            break;
          default:
            break;
        }
      });
    }
  }

  updateFileValueTwo({
    @required dynamic field,
    @required List<dynamic> files,
  }) {
    List<FileUploadModel> uploaded = [];
    files.forEach((f) {
      uploaded.add(FileUploadModel(
        url: f.url,
        filename: f.filename,
      ));
    });
    if (field != null && uploaded != null && uploaded.length > 0) {
      field.updateExtraData(uploaded);
    }
  }

  addErrorToField({
    @required String entity,
    @required String fieldId,
    @required String message,
  }) {
    switch (fieldId) {
      case "ktp_owner":
        ownerIdCard.addFieldError(message);
        break;
      case "npwp":
        getFieldByEntity(
          entity: entity,
          perseorangan: ownerTaxID,
          cv: taxIdCv,
          pt: taxIdPt,
        ).addFieldError(message);
        break;
      case "nib":
        getFieldByEntity(
          entity: entity,
          perseorangan: ownerNibID,
          cv: nibIdCv,
          pt: nibIdPt,
        ).addFieldError(message);
        break;
      case "akta_pendirian":
        getFieldByEntity(
          entity: entity,
          cv: deedOfIncorporationCv,
          pt: deedOfIncorporationPt,
        ).addFieldError(message);
        break;
      case "akta_perubahan":
        getFieldByEntity(
          entity: entity,
          cv: deedOfChangeCv,
          pt: deedOfChangePt,
        ).addFieldError(message);
        break;
      case "izin_usaha":
        getFieldByEntity(
          entity: entity,
          cv: businessPermitCv,
          pt: businessPermitPt,
        ).addFieldError(message);
        break;
      case "ktp_pengurus":
        getFieldByEntity(
          entity: entity,
          cv: adminIdCardCv,
          pt: adminIdCardPt,
        ).addFieldError(message);
        break;
      case "npwp_pengurus":
        getFieldByEntity(
          entity: entity,
          cv: adminTaxIdCv,
          pt: adminTaxIdPt,
        ).addFieldError(message);
        break;
      default:
        break;
    }
  }

  // handle error message businessEntity
  handleBusinessEntityError(BusinessInfoTwoModel data) {
    data.submission.submissionDetail.forEach((element) {
      if (element.fieldId.contains(data.businessEntity)) {
        String fieldId = element.fieldId.split(";").last;
        addErrorToField(
          entity: data.businessEntity,
          fieldId: fieldId,
          message: element.error,
        );
      }
    });
  }

  // handle error pinjaman bank
  handleBusinessLoansError(BusinessInfoTwoModel data) {
    data.submission.submissionDetail.forEach((element) {
      if (element.fieldId.contains("emiten_bank_loans")) {
        print(">> Loans : ${businessLoans.state.fieldBlocs.length}");
        businessLoans.state.fieldBlocs.forEach((field) {
          print(">> Field Error : ${element.error}");
          field.error = element.error;
        });
      }
    });
  }

  getBusinessInfoTwoData() async {
    try {
      final result = await getBusinessInfoTwo("$uuid");
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && !data.isClean) {
          handleBusinessEntityError(data);
          String entity = data.businessEntity;
          businessEntity.updateValue(entity);
          setDocuments(
            entity: entity,
            businessDocuments: data.businessDocuments,
          );
          businessStartDate.updateValue(data.businessStartDate != null &&
                  data.businessStartDate.isNotEmpty
              ? DateTime.parse(data.businessStartDate)
              : null);
          branchCompany.updateValue("${data.branchCompany}");
          if (data.businessLocationStatus != null &&
              data.businessLocationStatus.isNotEmpty) {
            var itemBusinessLocation = businessLocationStatus.state.items
                .firstWhere((d) =>
                    d.uuid.toLowerCase() ==
                    "${data.businessLocationStatus}".toLowerCase());
            businessLocationStatus.updateValue(itemBusinessLocation);
          }
          hasBankLoan.updateValue(
            hasBankLoan.state.items[data.hasBankLoan == 0 ? 1 : 0],
          );
          if (data.hasBankLoan == 1) {
            data.emitenBankLoans.asMap().forEach(
              (index, element) async {
                index > 0 ? addBusinessLoan() : null;
                await Future.delayed(Duration(milliseconds: 100));
                // ignore: close_sinks
                var loanField = businessLoans.state.fieldBlocs[index];
                loanField.bankName.updateValue(loanField.bankName.state.items
                    .firstWhere((e) => e.id == "${element.bankInvestorsId}"));
                loanField.loanAmount.updateValue(
                    PralistingHelper.parseToDecimal(element.amount));
                loanField.remainingPeriod.updateValue("${element.remaining}");
                loanField.tenor.updateValue("${element.tenor}");
              },
            );
          }
          if (data.paymentAspectLabel != null &&
              data.paymentAspectLabel.length > 0) {
            var paymentAspectId = "${data.paymentAspectLabel[0].id}";
            paymentAspect.updateValue(
              paymentAspect.state.items.firstWhere(
                (e) => e.id == paymentAspectId,
              ),
            );
          }

          List<FileUploadModel> certs = [];
          if (data.owningProof.isNotEmpty) {
            businessCertificate.updateExtraData(null);
            certs.add(FileUploadModel(
              filename: "${data.owningProof}",
              url: "${data.owningProof}",
            ));
          }
          businessCertificate.updateExtraData(certs);
          emitLoaded();
          PralistingHelper.handleErrorFields(
            state: state,
            submission: data.submission,
          );
          handleBusinessLoansError(data);
        } else {
          businessCertificate.updateExtraData(certs);
        }
        emitLoaded();
      } else {
        businessCertificate.updateExtraData(certs);
        // await Future.delayed(Duration(milliseconds: 1000));
        result.leftMap((l) {
          emitLoaded();
          printFailure(l);
        });
      }
    } catch (e, stack) {
      businessCertificate.updateExtraData(certs);
      santaraLog(e, stack);
    }
    emitLoaded();
  }

  @override
  Future<void> close() {
    return super.close();
  }

  getLoanPayments() async {
    await PralistingHelper.getBusinessOptions(
      group: 'loan_payment',
      bloc: paymentAspect,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  @override
  void onLoading() async {
    super.onLoading();
    await initializeBanks();
    await getLoanPayments();
    await getBusinessInfoTwoData();
    previousState = state.toJson();
  }

  Map<String, dynamic> setDocument({
    @required String type,
    @required String filename,
  }) {
    return {"type": "$type", "emiten_document": "$filename"};
  }

  Map<String, dynamic> setMultipleDocuments({
    @required String type,
    @required dynamic data,
  }) {
    return {
      "type": "$type",
      "emiten_document": PralistingHelper.getUploadedFileName(data)
    };
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  postData(Map<String, dynamic> body, {bool isNext}) async {
    try {
      emitSubmitting();
      List<Map<String, dynamic>> rawDocs = [];
      List<Map<String, dynamic>> rawLoans = [];
      if (body["business_entity"] == "Perseorangan") {
        // print(">> KTP : ${ownerIdCard.value}");
        // ktp owner
        rawDocs.add(setMultipleDocuments(
          type: "ktp_owner",
          data: ownerIdCard,
        ));

        // npwp owner
        rawDocs.add(setMultipleDocuments(
          type: "npwp",
          data: ownerTaxID,
        ));

        // nib/sku owner
        rawDocs.add(setMultipleDocuments(
          type: "nib",
          data: ownerNibID,
        ));
      } else {
        String entity = body["business_entity"];
        // CV / PT
        rawDocs.add(setMultipleDocuments(
          type: "akta_pendirian",
          data: entity == "CV" ? deedOfIncorporationCv : deedOfIncorporationPt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "akta_perubahan",
          data: entity == "CV" ? deedOfChangeCv : deedOfChangePt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "nib",
          data: entity == "CV" ? nibIdCv : nibIdPt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "izin_usaha",
          data: entity == "CV" ? businessPermitCv : businessPermitPt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "npwp",
          data: entity == "CV" ? taxIdCv : taxIdPt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "ktp_pengurus",
          data: entity == "CV" ? adminIdCardCv : adminIdCardPt,
        ));

        rawDocs.add(setMultipleDocuments(
          type: "npwp_pengurus",
          data: entity == "CV" ? adminTaxIdCv : adminTaxIdPt,
        ));
      }

      body["business_documents"] = rawDocs;
      body["has_bank_loan"] =
          body["business_bank_loan"].toString().contains("Tidak") ? 0 : 1;
      body["owning_proof"] =
          PralistingHelper.getSingleFile(businessCertificate);
      body["business_location_status"] =
          businessLocationStatus.value.uuid.toLowerCase();
      // banks / loans
      var loans = businessLoans.state.fieldBlocs;
      if (loans != null && loans.length > 0) {
        loans.forEach((l) {
          var rawLoan = {
            "bank_investors_id": int.parse(l.bankName.value.id),
            "amount": PralistingHelper.removeComma(l.loanAmount.value),
            "tenor": l.tenor.valueToInt,
            "remaining": l.remainingPeriod.valueToInt,
          };
          rawLoans.add(rawLoan);
        });
      }
      body["emiten_bank_loans"] = rawLoans;
      body["payment_aspect_label"] = int.parse(paymentAspect.value.id);
      body["branch_company"] = branchCompany.valueToInt;
      var data = {"body": body, "uuid": "$uuid"};
      // logger.i(data);
      final result = await updateBusinessInfoTwo(data);
      if (result.isRight()) {
        var message = result.getOrElse(null).meta.message;
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: message,
        );
        emitSuccess(
          successResponse: success,
          canSubmitAgain: true,
        );
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      santaraLog(e, stack);
      emitFailure(failureResponse: failure);
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}

class LoanFieldBloc extends GroupFieldBloc {
  final SelectFieldBloc bankName;
  final TextFieldBloc loanAmount;
  final TextFieldBloc tenor;
  final TextFieldBloc remainingPeriod;
  String errorMessage;

  LoanFieldBloc({
    @required this.bankName,
    @required this.loanAmount,
    @required this.tenor,
    @required this.remainingPeriod,
    String name,
  }) : super([bankName, loanAmount, tenor, remainingPeriod], name: name);

  String get error => errorMessage;

  set error(String error) => errorMessage = error;
}

var rawBusinessLocationStatus = [
  SearchData(
    id: "1",
    uuid: "sewa",
    name: "Sewa",
  ),
  SearchData(
    id: "2",
    uuid: "milik_pribadi",
    name: "Milik Pribadi",
  ),
  SearchData(
    id: "3",
    uuid: "milik_bu",
    name: "Milik Badan Usaha",
  ),
];
