import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessEstablishedField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessEstablishedField({@required this.bloc});
  final DateFormat format = DateFormat("dd LLLL yyyy", "id");

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Tanggal Mulai Usaha",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: FontSize.s14,
            ),
          ),
          SizedBox(height: Sizes.s10),
          BlocBuilder<InputFieldBloc, InputFieldBlocState>(
            bloc: bloc.businessStartDate,
            builder: (context, state) {
              DateTime date = bloc.businessStartDate.state?.value != null
                  ? bloc.businessStartDate.state?.value
                  : null;
              return TextField(
                readOnly: true,
                onTap: () async {
                  await DatePicker.showDatePicker(
                    context,
                    showTitleActions: true,
                    minTime: DateTime(1900, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      if (date != null) {
                        bloc.businessStartDate.updateValue(date);
                      }
                    },
                    currentTime: date != null ? date : DateTime.now(),
                    locale: LocaleType.id,
                  );
                },
                decoration: InputDecoration(
                  border: PralistingInputDecoration.outlineInputBorder,
                  contentPadding: EdgeInsets.all(Sizes.s15),
                  isDense: true,
                  errorMaxLines: 5,
                  labelText: "",
                  labelStyle: PralistingInputDecoration.labelTextStyle,
                  hintText: date != null ? format.format(date) : "",
                  hintStyle: PralistingInputDecoration.hintTextStyle(
                    bloc.businessStartDate.state.value != null ? true : false,
                  ),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  suffixIcon: Icon(Icons.date_range),
                  errorText: !bloc.businessStartDate.state.isInitial
                      ? bloc.businessStartDate.state.hasError
                          ? bloc.businessStartDate.state.error
                          : null
                      : null,
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
