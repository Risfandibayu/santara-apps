import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessLocationStatusField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessLocationStatusField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.businessLocationStatus.state.extraData;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SantaraDropDownField(
                onTap: bloc.businessLocationStatus.state.items != null &&
                        bloc.businessLocationStatus.state.items.length > 0
                    ? () async {
                        final result = await PralistingHelper.showModal(
                          context,
                          title: "",
                          hintText: "",
                          lists: bloc.businessLocationStatus.state.items,
                          selected: bloc.businessLocationStatus.value,
                          showSearch: false,
                        );
                        bloc.businessLocationStatus.updateValue(result);
                      }
                    : null,
                onReload: () {},
                labelText: "Status Lokasi Usaha",
                hintText: "Pilih Status Lokasi Usaha",
                state: fieldState,
                value: bloc.businessLocationStatus.value?.name,
                errorText: !bloc.businessLocationStatus.state.isInitial
                    ? bloc.businessLocationStatus.state.hasError
                        ? bloc.businessLocationStatus.state.error
                        : null
                    : null,
              ),
              SizedBox(height: Sizes.s5),
              Text(
                "*Lokasi usaha utama.",
                style: TextStyle(
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
