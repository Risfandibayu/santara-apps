import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessCertificateField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessCertificateField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder<TextFieldBloc, TextFieldBlocState>(
        bloc: bloc.businessCertificate,
        builder: (context, state) {
          // print(">> State : ${bloc.businessCertificate.state.extraData}");
          return state.extraData != null
              ? FileUploadSubmission(
                  label: "Upload Sertifikat Kepemilikan/Perjanjian Sewa",
                  hintText: "",
                  allowedTypes: ['pdf', 'jpg', 'jpeg', 'png'],
                  fieldBloc: bloc.businessCertificate,
                  maxFiles: 1,
                  minFiles: 1,
                  name: 'image',
                  path: '$apiFileUpload/upload',
                  location: "pralisting/business_documents/",
                  type: 'pdf',
                  annotation: Text(
                    "*Dokumen dengan format PDF, JPG, JPEG, PNG",
                    style: TextStyle(
                      fontSize: FontSize.s10,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff707070),
                    ),
                  ),
                )
              : Container();
        },
      ),
    );
  }
}
