import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/widgets/business_loan_card.dart';

class BusinessLoanField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessLoanField({@required this.bloc});

  Widget _loanWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<LoanFieldBloc>,
            ListFieldBlocState<LoanFieldBloc>>(
          bloc: bloc.businessLoans,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return BusinessLoanCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: () => bloc.removeBusinessLoan(i),
                  );
                },
              );
            }
            return Container();
          },
        ),
        FlatButton.icon(
          padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
          onPressed: () => bloc.addBusinessLoan(),
          icon: Text(
            "Tambah Form Pinjaman",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
              color: Color(0xff218196),
              decoration: TextDecoration.underline,
            ),
          ),
          label: Icon(
            Icons.add_circle,
            size: FontSize.s20,
            color: Color(0xff218196),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
        bloc: bloc.hasBankLoan,
        builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Memiliki Pinjaman Bank?",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: FontSize.s14,
                ),
              ),
              SizedBox(height: Sizes.s10),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: state.items.length,
                itemBuilder: (context, index) {
                  String item = state.items[index];
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Transform.translate(
                        offset: Offset(-Sizes.s25, 0),
                        child: RadioListTile<String>(
                          dense: true,
                          title: Text(
                            '$item',
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          value: item,
                          groupValue: state.value,
                          onChanged: (String value) {
                            bloc.hasBankLoan.updateValue(value);
                          },
                        ),
                      ),
                      state.value != null && state.value == item
                          ? state.value.toString().contains("Ya")
                              ? _loanWidget()
                              : Container()
                          : Container(),
                    ],
                  );
                },
              ),
              !state.isInitial
                  ? state.hasError
                      ? Container(
                        margin: EdgeInsets.only(left: Sizes.s15),
                        child: Text(
                            "${state.error}",
                            style: TextStyle(
                              color: Colors.red[700],
                              fontSize: FontSize.s12,
                            ),
                          ),
                      )
                      : Container()
                  : Container()
            ],
          );
        },
      ),
    );
  }
}
