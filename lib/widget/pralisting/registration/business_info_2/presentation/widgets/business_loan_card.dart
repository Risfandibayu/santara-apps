import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/core/widgets/text/error_text.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';

class BusinessLoanCard extends StatelessWidget {
  final int index;
  final LoanFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const BusinessLoanCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Pinjaman #${index + 1}',
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
              ),
            ),
            index > 0
                ? FlatButton.icon(
                    padding: EdgeInsets.zero,
                    onPressed: onRemove,
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: FontSize.s20,
                    ),
                    label: Text(
                      "Hapus",
                      style: TextStyle(
                        color: Colors.red,
                        decoration: TextDecoration.underline,
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        BlocBuilder(
          bloc: fieldBloc.bankName,
          builder: (context, state) {
            var businessTypeState = fieldBloc.bankName.state.extraData;
            return SantaraDropDownField(
              onTap: fieldBloc.bankName.state.items != null &&
                      fieldBloc.bankName.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "Pilih Bank",
                        hintText: "Cari Nama Bank",
                        lists: fieldBloc.bankName.state.items,
                        selected: fieldBloc.bankName.value,
                      );
                      fieldBloc.bankName.updateValue(result);
                    }
                  : null,
              onReload: () => null,
              labelText: "Nama Bank",
              hintText: "",
              state: businessTypeState,
              value: fieldBloc.bankName.value?.name,
              errorText: !fieldBloc.bankName.state.isInitial
                  ? fieldBloc.bankName.state.hasError
                      ? fieldBloc.bankName.state.error
                      : null
                  : null,
            );
          },
        ),
        SizedBox(height: Sizes.s10),
        BalanceDecimalInputField(
          bloc: fieldBloc.loanAmount,
          label: "Jumlah Pinjaman",
          hintText: "1,000,000,000",
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.tenor,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            labelText: 'Tenor (Bulan)',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '63',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(3),
            FilteringTextInputFormatter.digitsOnly,
          ],
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.remainingPeriod,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            labelText: 'Sisa Jangka Waktu (Bulan)',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '36',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(3),
            FilteringTextInputFormatter.digitsOnly,
          ],
        ),
        ErrorText(message: fieldBloc.error),
      ],
    );
  }
}
