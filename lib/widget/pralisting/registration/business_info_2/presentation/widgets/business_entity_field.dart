import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessEntityField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessEntityField({@required this.bloc});

  final List<String> _individualDocRequirements = [
    "KTP Owner",
    "NPWP Owner",
    "NIB/SKU Owner"
  ];

  final List<String> _companiesDocRequirements = [
    "Akta Pendirian dan SK Kemenkumham",
    "Akta Perubahan dan SK Kemenkumham (opsional)",
    "NIB PT",
    "Izin Usaha PT",
    "NPWP PT",
    "KTP Pengurus dan Pemegang Saham PT (2 File)",
    "NPWP Pengurus dan Pemegang Saham PT (2 File)",
  ];

  Widget _fileTypeAnotation(String type) {
    return Text(
      "*Jenis file $type",
      style: TextStyle(
        fontSize: FontSize.s10,
        fontWeight: FontWeight.w300,
        color: Color(0xff707070),
      ),
    );
  }

  Widget _itemBullet(String text) {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s10),
      child: Row(
        children: [
          Icon(
            Icons.fiber_manual_record,
            color: Color(0xffBF2D30),
            size: FontSize.s12,
          ),
          SizedBox(width: Sizes.s5),
          Text(
            "$text",
            style: TextStyle(
              color: Colors.black,
              fontSize: FontSize.s12,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildIndividualField({@required BusinessInfoTwoBloc bloc}) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10, bottom: Sizes.s10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Dokumen yang dibutuhkan",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _individualDocRequirements.length,
            itemBuilder: (context, index) {
              String item = _individualDocRequirements[index];
              return _itemBullet("$item");
            },
          ),
          Text(
            "*File maksimal 10 MB",
            style: TextStyle(
              color: Colors.black,
              fontSize: FontSize.s12,
            ),
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "Unggah KTP Owner",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf', 'png', 'jpg'],
            fieldBloc: bloc.ownerIdCard,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            // name: 'owner_id_card',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF, PNG, JPG)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "NPWP Owner",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf', 'png', 'jpg'],
            fieldBloc: bloc.ownerTaxID,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            // name: 'owner_tax_id',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF, PNG, JPG)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "NIB/SKU Owner",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf'],
            fieldBloc: bloc.ownerNibID,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            // name: 'owner_nib_id',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF)"),
            maxFileSize: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildCompaniesField({
    @required BusinessInfoTwoBloc bloc,
    @required String prefix,
  }) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10, bottom: Sizes.s10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Dokumen yang dibutuhkan",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _companiesDocRequirements.length,
            itemBuilder: (context, index) {
              String item = _companiesDocRequirements[index];
              return _itemBullet("$item");
            },
          ),
          Text(
            "*File maksimal 10 MB",
            style: TextStyle(
              color: Colors.black,
              fontSize: FontSize.s12,
            ),
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "Akta Pendirian dan SK Kemenkumham (2 File)",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf'],
            fieldBloc: prefix == "CV"
                ? bloc.deedOfIncorporationCv
                : bloc.deedOfIncorporationPt,
            maxFiles: 2,
            minFiles: 2,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "Akta Perubahan dan SK Kemenkumham (2 File)",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf'],
            fieldBloc:
                prefix == "CV" ? bloc.deedOfChangeCv : bloc.deedOfChangePt,
            maxFiles: 2,
            minFiles: 2,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF) (Opsional)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "NIB $prefix",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf'],
            fieldBloc: prefix == "CV" ? bloc.nibIdCv : bloc.nibIdPt,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "Izin Usaha $prefix",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf'],
            fieldBloc:
                prefix == "CV" ? bloc.businessPermitCv : bloc.businessPermitPt,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "NPWP $prefix",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf', 'png', 'jpg'],
            fieldBloc: prefix == "CV" ? bloc.taxIdCv : bloc.taxIdPt,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF, PNG, JPG)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "KTP Pengurus $prefix (2 Pengurus)",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf', 'png', 'jpg'],
            fieldBloc: prefix == "CV" ? bloc.adminIdCardCv : bloc.adminIdCardPt,
            maxFiles: 2,
            minFiles: 2,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF, PNG, JPG)"),
            maxFileSize: 10,
          ),
          SizedBox(height: Sizes.s20),
          FileUploadSubmission(
            label: "NPWP Pengurus $prefix (2 Pengurus)",
            hintText: "Klik unggah dan pilih file",
            allowedTypes: ['pdf', 'png', 'jpg'],
            fieldBloc: prefix == "CV" ? bloc.adminTaxIdCv : bloc.adminTaxIdPt,
            maxFiles: 2,
            minFiles: 2,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: 'pdf',
            annotation: _fileTypeAnotation("(PDF, PNG, JPG)"),
            maxFileSize: 10,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Dokumen Legalitas",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: FontSize.s14,
              ),
            ),
            subtitle: Text(
              "Pilih Bentuk Badan Usaha",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s12,
              ),
            ),
          ),
          BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
            bloc: bloc.businessEntity,
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: state.items.length,
                    itemBuilder: (context, index) {
                      String item = state.items[index];
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Transform.translate(
                            offset: Offset(-Sizes.s25, 0),
                            child: RadioListTile<String>(
                              dense: true,
                              title: Text(
                                '$item',
                                style: TextStyle(
                                  fontSize: FontSize.s12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              value: item,
                              groupValue: state.value,
                              onChanged: (String value) {
                                switch (value) {
                                  case 'Perseorangan':
                                    bloc.addIndividual();
                                    break;
                                  case 'CV':
                                    bloc.addCv();
                                    break;
                                  case 'PT':
                                    bloc.addCompany();
                                    break;
                                  default:
                                }
                                bloc.businessEntity.updateValue(value);
                              },
                            ),
                          ),
                          state.value != null && state.value == item
                              ? state.value == "Perseorangan"
                                  ? _buildIndividualField(bloc: bloc)
                                  : _buildCompaniesField(
                                      bloc: bloc,
                                      prefix: state.value,
                                    )
                              : Container(),
                        ],
                      );
                    },
                  ),
                  !state.isInitial
                      ? state.hasError
                          ? Container(
                              margin: EdgeInsets.only(left: Sizes.s15),
                              child: Text(
                                "${state.error}",
                                style: TextStyle(
                                  color: Colors.red[700],
                                  fontSize: FontSize.s12,
                                ),
                              ),
                            )
                          : Container()
                      : Container()
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
