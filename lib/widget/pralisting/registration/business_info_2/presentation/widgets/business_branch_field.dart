import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessBranchField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessBranchField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc.branchCompany,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: "Jumlah Cabang",
          hintText: "3",
          hintStyle: PralistingInputDecoration.hintTextStyle(
            false,
          ),
          labelStyle: PralistingInputDecoration.labelTextStyle,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        inputFormatters: [
          LengthLimitingTextInputFormatter(2),
          FilteringTextInputFormatter.digitsOnly,
        ],
        keyboardType: TextInputType.number,
      ),
    );
  }
}
