import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/blocs/business_info_2_bloc.dart';

class BusinessPaymentAspectField extends StatelessWidget {
  final BusinessInfoTwoBloc bloc;
  BusinessPaymentAspectField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.paymentAspect.state.extraData;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  "Aspek Pembayaran",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: FontSize.s14,
                  ),
                ),
                subtitle: Text(
                  "Pilih salah satu pernyataan yang paling sesuai dengan bisnis Anda",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s12,
                  ),
                ),
              ),
              SizedBox(height: Sizes.s20),
              SantaraDropDownField(
                onTap: bloc.paymentAspect.state.items != null &&
                        bloc.paymentAspect.state.items.length > 0
                    ? () async {
                        final result = await PralistingHelper.showModal(
                          context,
                          title: "",
                          hintText: "",
                          lists: bloc.paymentAspect.state.items,
                          selected: bloc.paymentAspect.value,
                          showSearch: false,
                        );
                        bloc.paymentAspect.updateValue(result);
                      }
                    : null,
                onReload: () {},
                labelText: "Aspek Pembayaran Terkait Pinjaman Bank",
                hintText:
                    "Bagaimana tingkat kelancaran pembayaran pinjaman bank?",
                state: fieldState,
                value: bloc.paymentAspect.value?.name,
                errorText: !bloc.paymentAspect.state.isInitial
                    ? bloc.paymentAspect.state.hasError
                        ? bloc.paymentAspect.state.error
                        : null
                    : null,
              ),
            ],
          );
        },
      ),
    );
  }
}
