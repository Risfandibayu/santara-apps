// To parse this JSON data, do
//
//     final businessProspectiveInfoModel = businessProspectiveInfoModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

BusinessProspectiveInfoModel businessProspectiveInfoModelFromJson(String str) =>
    BusinessProspectiveInfoModel.fromJson(json.decode(str));

String businessProspectiveInfoModelToJson(BusinessProspectiveInfoModel data) =>
    json.encode(data.toJson());

class BusinessProspectiveInfoModel {
  BusinessProspectiveInfoModel({
    this.companyName,
    this.trademark,
    this.ownerName,
    this.categoryId,
    this.subCategoryId,
    this.regencyId,
    this.address,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  String companyName;
  String trademark;
  String ownerName;
  YId categoryId;
  YId subCategoryId;
  YId regencyId;
  String address;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory BusinessProspectiveInfoModel.fromJson(Map<String, dynamic> json) =>
      BusinessProspectiveInfoModel(
        companyName: json["company_name"] == null ? null : json["company_name"],
        trademark: json["trademark"] == null ? null : json["trademark"],
        ownerName: json["owner_name"] == null ? null : json["owner_name"],
        categoryId: json["category_id"] == null
            ? null
            : YId.fromJson(json["category_id"]),
        subCategoryId: json["sub_category_id"] == null
            ? null
            : YId.fromJson(json["sub_category_id"]),
        regencyId: json["regency_id"] == null
            ? null
            : YId.fromJson(json["regency_id"]),
        address: json["address"] == null ? null : json["address"],
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName == null ? null : companyName,
        "trademark": trademark == null ? null : trademark,
        "owner_name": ownerName == null ? null : ownerName,
        "category_id": categoryId == null ? null : categoryId.toJson(),
        "sub_category_id":
            subCategoryId == null ? null : subCategoryId.toJson(),
        "regency_id": regencyId == null ? null : regencyId.toJson(),
        "address": address == null ? null : address,
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class YId {
  YId({
    this.id,
    this.text,
  });

  int id;
  String text;

  factory YId.fromJson(Map<String, dynamic> json) => YId(
        id: json["id"] == null ? null : json["id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": text == null ? null : text,
      };
}
