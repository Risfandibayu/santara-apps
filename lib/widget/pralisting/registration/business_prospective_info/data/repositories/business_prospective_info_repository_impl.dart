import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/data/datasources/business_prospective_info_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/data/models/business_prospective_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/domain/repositories/business_prospective_info_repository.dart';
import 'package:meta/meta.dart';

class BusinessProspectiveInfoRepositoryImpl
    implements BusinessProspectiveInfoRepository {
  final NetworkInfo networkInfo;
  final BusinessProspectiveInfoRemoteDataSource remoteDataSource;

  BusinessProspectiveInfoRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<SearchData>>> getBusinessCategories() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getCategories();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, List<SearchData>>> getBusinessSubcategories(
      {String id}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getSubcategories(id: id);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, List<Regency>>> getRegencies() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getRegencies();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, String>> postBusinessProspectiveInfo(
      {@required dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.postBusinessProspective(body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, BusinessProspectiveInfoModel>>
      getBusinessProspectiveInfo({@required String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBusinessProspective(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, String>> updateBusinessProspectiveInfo({
    @required dynamic body,
    @required String uuid,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateBusinessProspective(
            body: body, uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
