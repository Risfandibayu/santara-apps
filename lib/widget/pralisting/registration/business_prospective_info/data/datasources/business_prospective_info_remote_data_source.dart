import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/data/models/business_prospective_info_model.dart';

abstract class BusinessProspectiveInfoRemoteDataSource {
  Future<List<SearchData>> getCategories();
  Future<List<SearchData>> getSubcategories({@required String id});
  Future<List<Regency>> getRegencies();
  Future<String> postBusinessProspective({@required dynamic body});
  Future<BusinessProspectiveInfoModel> getBusinessProspective({
    @required String uuid,
  });
  Future<String> updateBusinessProspective({
    @required dynamic body,
    @required String uuid,
  });
}

class BusinessProspectiveInfoRemoteDataSourceImpl
    implements BusinessProspectiveInfoRemoteDataSource {
  final RestClient client;
  BusinessProspectiveInfoRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<SearchData>> getCategories() async {
    try {
      final result = await client.getExternalUrl(
          url: "$apiPralisting/list-categories?page=1&limit=200");
      if (result.statusCode == 200) {
        // print(">> Categories Length : ${result.data}");
        List<SearchData> categories = List<SearchData>();
        result.data["data"].forEach((val) {
          categories.add(SearchData.fromJson(val));
        });
        return categories;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<List<SearchData>> getSubcategories({String id}) async {
    try {
      final result = await client.getExternalUrl(
          url:
              "$apiPralisting/list-sub-categories?page=1&limit=200&category_id=$id");
      if (result.statusCode == 200) {
        // print(">> Subcategories Length : ${result.data}");
        List<SearchData> subcategories = List<SearchData>();
        if (result.data["data"] != null) {
          result.data["data"].forEach((val) {
            subcategories.add(SearchData.fromJson(val));
          });
        }
        return subcategories;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<List<Regency>> getRegencies() async {
    try {
      final result = await client.getExternalUrl(url: "$apiLocal/regencies/");
      if (result.statusCode == 200) {
        List<Regency> regencies = List<Regency>();
        result.data.forEach((val) {
          regencies.add(Regency.fromJson(val));
        });
        return regencies;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<String> postBusinessProspective({@required dynamic body}) async {
    try {
      final result =
          await client.putExternalUrl(url: "$apiPralisting/step1", body: body);
      if (result.statusCode == 200) {
        return result.data["meta"]["message"];
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<BusinessProspectiveInfoModel> getBusinessProspective(
      {String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step1/$uuid");
      if (result.statusCode == 200) {
        return BusinessProspectiveInfoModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<String> updateBusinessProspective({body, String uuid}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/step1/$uuid", body: body);
      if (result.statusCode == 200) {
        return result.data["meta"]["message"];
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
