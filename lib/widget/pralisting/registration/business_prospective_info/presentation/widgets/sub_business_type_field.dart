import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';

class SubBusinessTypeField extends StatelessWidget {
  final KycFieldWrapper fieldWrapper;
  final BusinessProspectiveInfoBloc bloc;

  SubBusinessTypeField({
    @required this.fieldWrapper,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: fieldWrapper.wrap(
        index: 5,
        child: BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
          bloc: bloc.subBusinessType,
          builder: (context, state) {
            var subBusinessTypeState = bloc.subBusinessType.state.extraData;
            return SantaraDropDownField(
              onTap: bloc.subBusinessType.state.items != null &&
                      bloc.subBusinessType.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "Sub Jenis Usaha",
                        hintText: "Cari Sub Jenis Usaha",
                        lists: bloc.subBusinessType.state.items,
                        selected: bloc.subBusinessType.value,
                      );
                      bloc.subBusinessType.updateValue(result);
                    }
                  : () => ToastHelper.showFailureToast(
                        context,
                        "Sub jenis usaha tidak ada",
                      ),
              onReload: () =>
                  bloc.initializeSubBusinessType(bloc.businessType.value.id),
              labelText: "Sub Jenis Usaha",
              hintText: "Pilih Sub Jenis Usaha",
              state: subBusinessTypeState,
              value: bloc.subBusinessType.value?.name,
              errorText: !bloc.subBusinessType.state.isInitial
                  ? bloc.subBusinessType.state.hasError
                      ? bloc.subBusinessType.state.error
                      : null
                  : null,
            );
          },
        ),
      ),
    );
  }
}
