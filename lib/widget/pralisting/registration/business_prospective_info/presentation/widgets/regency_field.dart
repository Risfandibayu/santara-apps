import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';

class RegencyField extends StatelessWidget {
  final BusinessProspectiveInfoBloc bloc;
  final KycFieldWrapper fieldWrapper;

  RegencyField({
    @required this.fieldWrapper,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: fieldWrapper.wrap(
        index: 6,
        child: BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
          bloc: bloc.businessRegency,
          builder: (context, state) {
            var regencyState = bloc.businessRegency.state.extraData;
            return SantaraDropDownField(
              onTap: bloc.businessRegency.state.items != null &&
                      bloc.businessRegency.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "Kota Lokasi Usaha",
                        hintText: "Pilih Kota Lokasi Usaha",
                        lists: bloc.businessRegency.state.items,
                        selected: bloc.businessRegency.value,
                      );
                      bloc.businessRegency.updateValue(result);
                    }
                  : null,
              onReload: () => bloc.initializeRegencies(),
              labelText: "Kota Lokasi Usaha",
              hintText: "",
              state: regencyState,
              value: bloc.businessRegency.value?.name,
              errorText: !bloc.businessRegency.state.isInitial
                  ? bloc.businessRegency.state.hasError
                      ? bloc.businessRegency.state.error
                      : null
                  : null,
            );
          },
        ),
      ),
    );
  }
}
