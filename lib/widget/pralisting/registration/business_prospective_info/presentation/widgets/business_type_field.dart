import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';

class BusinessTypeField extends StatelessWidget {
  final KycFieldWrapper fieldWrapper;
  final BusinessProspectiveInfoBloc bloc;
  BusinessTypeField({
    @required this.fieldWrapper,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: fieldWrapper.wrap(
        index: 4,
        child: BlocBuilder<SelectFieldBloc, SelectFieldBlocState>(
          bloc: bloc.businessType,
          builder: (context, state) {
            var businessTypeState = bloc.businessType.state.extraData;
            return SantaraDropDownField(
              onTap: bloc.businessType.state.items != null &&
                      bloc.businessType.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "Jenis Usaha",
                        hintText: "Cari Jenis Usaha",
                        lists: bloc.businessType.state.items,
                        selected: bloc.businessType.value,
                      );
                      bloc.businessType.updateValue(result);
                    }
                  : null,
              onReload: () => bloc.initializeBusinessType(),
              labelText: "Jenis Usaha",
              hintText: "Pilih Jenis Usaha",
              state: businessTypeState,
              value: bloc.businessType.value?.name,
              errorText: !bloc.businessType.state.isInitial
                  ? bloc.businessType.state.hasError
                      ? bloc.businessType.state.error
                      : null
                  : null,
            );
          },
        ),
      ),
    );
  }
}
