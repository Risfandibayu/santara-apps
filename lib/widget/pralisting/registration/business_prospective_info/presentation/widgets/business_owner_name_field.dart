import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';

class BusinessOwnerNameField extends StatelessWidget {
  final KycFieldWrapper fieldWrapper;
  final BusinessProspectiveInfoBloc bloc;
  BusinessOwnerNameField({
    @required this.fieldWrapper,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: fieldWrapper.wrap(
        index: 3,
        child: TextFieldBlocBuilder(
          textFieldBloc: bloc.ownerName,
          decoration: InputDecoration(
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            labelText: "Nama Pemilik Usaha",
            hintText: "Nama Lengkap",
            labelStyle: PralistingInputDecoration.labelTextStyle,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            floatingLabelBehavior: FloatingLabelBehavior.always,
          ),
        ),
      ),
    );
  }
}
