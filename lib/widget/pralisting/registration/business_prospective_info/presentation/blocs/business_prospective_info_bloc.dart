import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/domain/usecases/business_prospective_info_usecase.dart';
import 'package:meta/meta.dart';

class BusinessProspectiveInfoBloc extends FormBloc<FormResult, FormResult> {
  // UUID
  String uuid;
  Map previousState;
  final GetCategories getCategories;
  final GetSubCategories getSubCategories;
  final GetRegencies getRegencies;
  final PostBusinessProspective postBusinessProspective;
  final GetBusinessProspectiveInfo getBusinessProspectiveInfo;
  final UpdateBusinessProspective updateBusinessProspective;

  final companyName = TextFieldBloc(name: 'company_name', validators: [
    PralistingValidationHelper.required,
  ]);

  final trademarkName = TextFieldBloc(
    name: 'trademark',
    validators: [],
  );

  final ownerName = TextFieldBloc(
    name: 'owner_name',
    validators: [PralistingValidationHelper.required],
  );

  final businessType = SelectFieldBloc<SearchData, dynamic>(
    name: 'category_id',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final subBusinessType = SelectFieldBloc<SearchData, dynamic>(
    name: 'sub_category_id',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessRegency = SelectFieldBloc<SearchData, dynamic>(
    name: 'regency_id',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessAddress = TextFieldBloc(
    name: 'address',
    validators: [PralistingValidationHelper.required],
  );

  BusinessProspectiveInfoBloc({
    @required this.getCategories,
    @required this.getSubCategories,
    @required this.getRegencies,
    @required this.postBusinessProspective,
    @required this.getBusinessProspectiveInfo,
    @required this.updateBusinessProspective,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        companyName,
        trademarkName,
        ownerName,
        businessType,
        subBusinessType,
        businessRegency,
        businessAddress,
      ],
    );
    // add validation
    ownerName.addAsyncValidators([PralistingValidationHelper.ownerName]);
    trademarkName.addAsyncValidators([PralistingValidationHelper.trademark]);
    companyName.addValidators([PralistingValidationHelper.companyName]);
    businessAddress.addAsyncValidators([PralistingValidationHelper.address]);
  }

  _handleOnchangeBusinessType() async {
    businessType.onValueChanges(
      onData: (previous, current) async* {
        if (previous.value != current.value) {
          subBusinessType.clear();
          subBusinessType.state.items.clear();
          await initializeSubBusinessType(current.value.id);
        } else {
          print(">> No Value Has Changed");
        }
      },
    );
  }

  initializeBusinessType() async {
    try {
      businessType.updateExtraData(FieldState.loading);
      // if (businessType.state.formBloc != null) {
      //   businessType.state.formBloc.emitLoading();
      // }
      final result = await getCategories(NoParams());
      if (result.isRight()) {
        final categories = result.getOrElse(null);
        businessType.updateItems(categories);
        await Future.delayed(Duration(milliseconds: 100));
        businessType.updateExtraData(FieldState.loaded);
        // businessType.state.formBloc.emitSuccess(canSubmitAgain: true);
        if (categories.length < 1) {
          emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_field_data_error,
              message: "Data jenis usaha kosong!",
            ),
          );
        }
      } else {
        businessType.updateExtraData(FieldState.error);
        // businessType.state.formBloc.emitFailure();
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      businessType.updateExtraData(FieldState.error);
      // businessType.state.formBloc.emitFailure();
    }
  }

  initializeSubBusinessType(String id) async {
    // print(">> HELLOW");
    try {
      subBusinessType.updateExtraData(FieldState.loading);
      // if (subBusinessType.state.formBloc != null) {
      //   subBusinessType.state.formBloc.emitLoading();
      // }
      final result = await getSubCategories(id);
      if (result.isRight()) {
        final subcategories = result.getOrElse(null);
        subBusinessType.updateItems(subcategories);
        subBusinessType.updateExtraData(FieldState.loaded);
        await Future.delayed(Duration(milliseconds: 100));
        // subBusinessType.state.formBloc.emitSuccess(canSubmitAgain: true);
        if (subcategories.length < 1) {
          emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_field_data_error,
              message: "Data sub jenis usaha kosong!",
            ),
          );
        }
      } else {
        subBusinessType.updateItems([]);
        subBusinessType.updateExtraData(FieldState.error);
        // subBusinessType.state.formBloc.emitFailure();
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      subBusinessType.updateExtraData(FieldState.error);
      // subBusinessType.state.formBloc.emitFailure();
    }
  }

  initializeRegencies() async {
    try {
      businessRegency.updateExtraData(FieldState.loading);
      // if (businessRegency.state.formBloc != null) {
      //   businessRegency.state.formBloc.emitLoading();
      // }
      final result = await getRegencies(NoParams());
      if (result.isRight()) {
        final regencies = result.getOrElse(null);

        List<SearchData> _datas = [];
        regencies.forEach((element) {
          _datas.add(SearchData(
            id: '${element.id}',
            uuid: '${element.uuid}',
            name: '${element.name}',
          ));
        });
        businessRegency.updateItems(_datas);
        businessRegency.updateExtraData(FieldState.loaded);
        // businessRegency.state.formBloc.emitSuccess(canSubmitAgain: true);
        if (_datas.length < 1) {
          emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_field_data_error,
              message: "Data kota lokasi usaha kosong!",
            ),
          );
        }
      } else {
        businessRegency.updateExtraData(FieldState.error);
        // businessRegency.state.formBloc.emitFailure();
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      businessRegency.updateExtraData(FieldState.error);
      // businessRegency.state.formBloc.emitFailure();
    }
  }

  postData(Map<String, dynamic> body, String uuid, {bool isNext = true}) async {
    try {
      emitSubmitting();
      body['category_id'] = int.parse("${body['category_id'].id}");
      body['sub_category_id'] = int.parse("${body['sub_category_id'].id}");
      body['regency_id'] = int.parse("${body['regency_id'].id}");
      var data = {
        "body": body,
        "uuid": "$uuid",
      };
      // logger.i(data);
      final result = await updateBusinessProspective(data);
      // final result = await postBusinessProspective(body);
      if (result.isRight()) {
        result.map((r) {
          emitSuccess(
            successResponse: FormResult(
              status: isNext
                  ? FormStatus.submit_success
                  : FormStatus.submit_success_close_page,
              message: r,
            ),
            canSubmitAgain: true,
          );
        });
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(status: FormStatus.submit_error, message: e);
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  getBusinessProspectiveInfoData(String uuid) async {
    try {
      final result = await getBusinessProspectiveInfo('$uuid');
      await initializeRegencies();
      await initializeBusinessType();
      if (result.isRight()) {
        result.map((data) async {
          companyName.updateValue(data.companyName);
          trademarkName.updateValue(data.trademark);
          ownerName.updateValue(data.ownerName);
          businessAddress.updateValue(data.address);

          if (data.regencyId.id != null) {
            businessRegency.updateValue(businessRegency.state.items
                .firstWhere((element) => element.id == "${data.regencyId.id}"));
          }

          if (data.categoryId.id != null) {
            var item = businessType.state.items
                .firstWhere((element) => element.id == "${data.categoryId.id}");
            businessType.updateValue(item);
          }

          await Future.delayed(Duration(milliseconds: 500));

          if (data.subCategoryId.id != null) {
            await initializeSubBusinessType("${data.categoryId.id}");
            if (subBusinessType.state.extraData == FieldState.loaded) {
              var subBusinessData = subBusinessType.state.items
                  .firstWhere((e) => e.id == "${data.subCategoryId.id}");
              subBusinessType.updateValue(subBusinessData);
              previousState = state.toJson();
            }
          } else {
            previousState = state.toJson();
          }
          // logger.i(data.submission.toJson());
          PralistingHelper.handleErrorFields(
            state: state,
            submission: data.submission,
          );
          emitLoaded();
          // await Future.delayed(Duration(milliseconds: 1000));
        });
      } else {
        result.leftMap((l) async {
          final failure = FormResult(
            status: FormStatus.get_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          previousState = state.toJson();
          emitLoaded();
          printFailure(l);
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      previousState = state.toJson();
      emitLoaded();
    }
    // if(subBusinessType.state.items
  }

  void submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, uuid, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getBusinessProspectiveInfoData(uuid);
    _handleOnchangeBusinessType();
    emitLoaded();
  }

  @override
  void onSubmitting() async {}
}
