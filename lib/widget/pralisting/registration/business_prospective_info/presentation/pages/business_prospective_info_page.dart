import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/pages/business_info_1_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/business_address_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/business_owner_name_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/business_type_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/company_name_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/regency_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/sub_business_type_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/widgets/trademark_field.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import '../../../../../../injector_container.dart';

class BusinessProspectiveInfoPage extends StatefulWidget {
  final String uuid;
  BusinessProspectiveInfoPage({@required this.uuid});

  @override
  _BusinessProspectiveInfoPageState createState() =>
      _BusinessProspectiveInfoPageState();
}

class _BusinessProspectiveInfoPageState
    extends State<BusinessProspectiveInfoPage> {
  BusinessProspectiveInfoBloc bloc;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;
  final scrollDirection = Axis.vertical;

  @override
  void initState() {
    super.initState();
    bloc = sl<BusinessProspectiveInfoBloc>();
    bloc.uuid = widget.uuid;
    // bloc.add(event)
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Identitas Calon Penerbit",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "1 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            color: Colors.white,
            padding: EdgeInsets.all(Sizes.s20),
            child: BlocProvider<BusinessProspectiveInfoBloc>(
              create: (_) => bloc,
              child: Builder(
                builder: (context) {
                  // ignore: close_sinks
                  final bloc = context.bloc<BusinessProspectiveInfoBloc>();
                  return FormBlocListener<BusinessProspectiveInfoBloc,
                      FormResult, FormResult>(
                    onLoaded: (context, state) {},
                    onLoading: (context, state) {},
                    onSubmitting: (context, state) {
                      PopupHelper.showLoading(context);
                    },
                    onSuccess: (context, state) {
                      final form = state.successResponse;
                      if (form.status == FormStatus.submit_success) {
                        Navigator.pop(context);
                        ToastHelper.showSuccessToast(context, form.message);
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => BusinessInfoOnePage(
                              uuid: widget.uuid,
                            ),
                          ),
                        );
                      } else if (form.status ==
                          FormStatus.submit_success_close_page) {
                        PralistingHelper.handleOnSavePralisting(
                          context,
                          message: form.message,
                        );
                      }
                    },
                    onFailure: (context, state) {
                      final form = state.failureResponse;
                      if (form.status == FormStatus.submit_error) {
                        Navigator.pop(context);
                        ToastHelper.showFailureToast(context, form.message);
                      }

                      if (form.status == FormStatus.get_field_data_error ||
                          form.status == FormStatus.validation_error) {
                        ToastHelper.showFailureToast(context, form.message);
                      }
                    },
                    onSubmissionFailed: (context, state) {
                      PralistingHelper.showSubmissionFailed(context);
                    },
                    child:
                        BlocBuilder<BusinessProspectiveInfoBloc, FormBlocState>(
                      builder: (context, state) {
                        if (state is FormBlocLoading) {
                          return Center(
                            child: CupertinoActivityIndicator(),
                          );
                        } else {
                          return SingleChildScrollView(
                            scrollDirection: scrollDirection,
                            controller: controller,
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CompanyNameField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                TrademarkField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                BusinessOwnerNameField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                BusinessTypeField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                SubBusinessTypeField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                RegencyField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                BusinessAddressField(
                                  fieldWrapper: fieldWrapper,
                                  bloc: bloc,
                                ),
                                PralistingSubmitButton(
                                  nextKey: 'pralisting_submit_button',
                                  saveKey: 'pralisting_save_button',
                                  onTapNext: () =>
                                      bloc.submitForm(isNext: true),
                                  onTapSave: () =>
                                      bloc.submitForm(isNext: false),
                                ),
                              ],
                            ),
                          );
                        }
                      },
                    ),
                  );
                },
              ),
            )),
      ),
    );
  }
}
