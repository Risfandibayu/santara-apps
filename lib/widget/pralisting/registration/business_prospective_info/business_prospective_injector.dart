import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/blocs/business_prospective_info_bloc.dart';
import '../../../../injector_container.dart';
import 'data/datasources/business_prospective_info_remote_data_source.dart';
import 'data/repositories/business_prospective_info_repository_impl.dart';
import 'domain/repositories/business_prospective_info_repository.dart';
import 'domain/usecases/business_prospective_info_usecase.dart';

void initBusinessProspective() {
  sl.registerFactory(
    () => BusinessProspectiveInfoBloc(
      getCategories: sl(),
      getSubCategories: sl(),
      getRegencies: sl(),
      postBusinessProspective: sl(),
      getBusinessProspectiveInfo: sl(),
      updateBusinessProspective: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessProspectiveInfoRepository>(
    () => BusinessProspectiveInfoRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessProspectiveInfoRemoteDataSource>(
    () => BusinessProspectiveInfoRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetCategories(sl()));
  sl.registerLazySingleton(() => GetSubCategories(sl()));
  sl.registerLazySingleton(() => GetRegencies(sl()));
  sl.registerLazySingleton(() => PostBusinessProspective(sl()));
  sl.registerLazySingleton(() => UpdateBusinessProspective(sl()));
  sl.registerLazySingleton(() => GetBusinessProspectiveInfo(sl()));
}
