import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/data/models/business_prospective_info_model.dart';

abstract class BusinessProspectiveInfoRepository {
  Future<Either<Failure, List<SearchData>>> getBusinessCategories();
  Future<Either<Failure, List<SearchData>>> getBusinessSubcategories(
      {@required String id});
  Future<Either<Failure, List<Regency>>> getRegencies();
  Future<Either<Failure, String>> postBusinessProspectiveInfo({
    @required dynamic body,
  });
  Future<Either<Failure, String>> updateBusinessProspectiveInfo({
    @required dynamic body,
    @required String uuid,
  });
  Future<Either<Failure, BusinessProspectiveInfoModel>>
      getBusinessProspectiveInfo({
    @required String uuid,
  });
}
