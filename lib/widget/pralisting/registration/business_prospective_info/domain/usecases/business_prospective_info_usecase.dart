import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/data/models/business_prospective_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/domain/repositories/business_prospective_info_repository.dart';

class GetCategories implements UseCase<List<SearchData>, NoParams> {
  final BusinessProspectiveInfoRepository repository;
  GetCategories(this.repository);

  @override
  Future<Either<Failure, List<SearchData>>> call(NoParams noParams) async {
    return await repository.getBusinessCategories();
  }
}

class GetSubCategories implements UseCase<List<SearchData>, String> {
  final BusinessProspectiveInfoRepository repository;
  GetSubCategories(this.repository);

  @override
  Future<Either<Failure, List<SearchData>>> call(String id) async {
    return await repository.getBusinessSubcategories(id: id);
  }
}

class GetRegencies implements UseCase<List<Regency>, NoParams> {
  final BusinessProspectiveInfoRepository repository;
  GetRegencies(this.repository);

  @override
  Future<Either<Failure, List<Regency>>> call(NoParams params) async {
    return await repository.getRegencies();
  }
}

class PostBusinessProspective implements UseCase<String, dynamic> {
  final BusinessProspectiveInfoRepository repository;
  PostBusinessProspective(this.repository);

  @override
  Future<Either<Failure, String>> call(dynamic body) async {
    return await repository.postBusinessProspectiveInfo(body: body);
  }
}

class UpdateBusinessProspective implements UseCase<String, dynamic> {
  final BusinessProspectiveInfoRepository repository;
  UpdateBusinessProspective(this.repository);

  @override
  Future<Either<Failure, String>> call(dynamic params) async {
    print(params);
    return await repository.updateBusinessProspectiveInfo(
      body: params["body"],
      uuid: params["uuid"],
    );
  }
}

class GetBusinessProspectiveInfo
    implements UseCase<BusinessProspectiveInfoModel, String> {
  final BusinessProspectiveInfoRepository repository;
  GetBusinessProspectiveInfo(this.repository);

  @override
  Future<Either<Failure, BusinessProspectiveInfoModel>> call(
      String uuid) async {
    return await repository.getBusinessProspectiveInfo(uuid: uuid);
  }
}
