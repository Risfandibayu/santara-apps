import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/datasources/share_info_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/models/share_info_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/domain/repositories/share_info_repository.dart';
import 'package:meta/meta.dart';

class ShareInfoRepositoryImpl implements ShareInfoRepository {
  final NetworkInfo networkInfo;
  final ShareInfoRemoteDataSources remoteDataSource;

  ShareInfoRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, ShareInfoModel>> getShareInfo({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getShareInfo(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateShareInfo({
    String uuid,
    dynamic body,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateShareInfo(
          uuid: uuid,
          body: body,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
