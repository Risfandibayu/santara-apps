// To parse this JSON data, do
//
//     final shareInfoModel = shareInfoModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

ShareInfoModel shareInfoModelFromJson(String str) =>
    ShareInfoModel.fromJson(json.decode(str));

String shareInfoModelToJson(ShareInfoModel data) => json.encode(data.toJson());

class ShareInfoModel {
  ShareInfoModel({
    this.shareAmount,
    this.price,
    this.period,
    this.dividendPercentage,
    this.skNumberKemenkumham,
    this.earlyInvestors,
    this.locations,
    this.newLocations,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  double shareAmount;
  int price;
  String period;
  double dividendPercentage;
  String skNumberKemenkumham;
  List<EarlyInvestor> earlyInvestors;
  List<Location> locations;
  List<Location> newLocations;
  Submission submission;
  bool isComplete;
  bool isClean;

  static removeComma(String value) {
    if (value != null) {
      return value.replaceAll(",", "");
    }
  }

  factory ShareInfoModel.fromJson(Map<String, dynamic> json) => ShareInfoModel(
        shareAmount: json["share_amount"] == null
            ? null
            : double.parse(json["share_amount"].toString()),
        price: json["price"] == null
            ? null
            : int.parse(removeComma(json["price"].toString())),
        period: json["period"] == null ? null : json["period"],
        dividendPercentage: json["dividend_percentage"] == null
            ? null
            : double.parse(json["dividend_percentage"].toString()),
        skNumberKemenkumham: json["sk_number_kemenkumham"] == null
            ? null
            : json["sk_number_kemenkumham"],
        earlyInvestors: json["early_investors"] == null
            ? null
            : List<EarlyInvestor>.from(
                json["early_investors"].map((x) => EarlyInvestor.fromJson(x))),
        locations: json["locations"] == null
            ? null
            : List<Location>.from(
                json["locations"].map((x) => Location.fromJson(x))),
        newLocations: json["new_locations"] == null
            ? null
            : List<Location>.from(
                json["new_locations"].map((x) => Location.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "share_amount": shareAmount == null ? null : shareAmount,
        "price": price == null ? null : price,
        "period": period == null ? null : period,
        "dividend_percentage":
            dividendPercentage == null ? null : dividendPercentage,
        "sk_number_kemenkumham":
            skNumberKemenkumham == null ? null : skNumberKemenkumham,
        "early_investors": earlyInvestors == null
            ? null
            : List<dynamic>.from(earlyInvestors.map((x) => x.toJson())),
        "locations": locations == null
            ? null
            : List<dynamic>.from(locations.map((x) => x.toJson())),
        "new_locations": newLocations == null
            ? null
            : List<dynamic>.from(newLocations.map((x) => x.toJson())),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class EarlyInvestor {
  EarlyInvestor({
    this.name,
    this.percentage,
  });

  String name;
  double percentage;

  factory EarlyInvestor.fromJson(Map<String, dynamic> json) => EarlyInvestor(
        name: json["name"] == null ? null : json["name"],
        percentage: json["percentage"] == null
            ? null
            : double.parse(json["percentage"].toString().isNotEmpty
                ? json["percentage"].toString().replaceAll(",", ".")
                : "0"),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "percentage": percentage == null ? null : percentage,
      };
}

class Location {
  Location({
    this.name,
    this.address,
  });

  String name;
  String address;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "address": address == null ? null : address,
      };
}
