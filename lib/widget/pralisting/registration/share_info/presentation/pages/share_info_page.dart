import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/media_page.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/devidend_percentage_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/devidend_period_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/existing_location_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/new_location_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/share_owner_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/share_percentage_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/share_price_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/sk_number_field.dart';

import '../../../../../../injector_container.dart';

class ShareInfoPage extends StatefulWidget {
  final String uuid;
  ShareInfoPage({@required this.uuid});

  @override
  _ShareInfoPageState createState() => _ShareInfoPageState();
}

class _ShareInfoPageState extends State<ShareInfoPage> {
  ShareInfoBloc bloc;
  final _controller = ScrollController();

  @override
  void initState() {
    super.initState();
    bloc = sl<ShareInfoBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Informasi Penawaran Saham",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "8 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<ShareInfoBloc>();
              return FormBlocListener<ShareInfoBloc, FormResult, FormResult>(
                onLoaded: (context, state) async {
                  await Future.delayed(Duration(milliseconds: 500));
                  // _controller.jumpTo(0);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MediaPage(
                          uuid: "${widget.uuid}",
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  } else {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<ShareInfoBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        color: Colors.white,
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: SingleChildScrollView(
                          controller: _controller,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SharePercentageField(bloc: bloc),
                              SharePriceField(bloc: bloc),
                              DevidendPeriodField(bloc: bloc),
                              DevidendSharePercentageField(bloc: bloc),
                              SkNumberField(bloc: bloc),
                              ShareOwnersField(bloc: bloc),
                              ExistingLocationField(bloc: bloc),
                              NewLocationField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
