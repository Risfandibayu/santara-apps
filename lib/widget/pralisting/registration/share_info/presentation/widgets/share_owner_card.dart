import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/message_bottom_sheet/message_bottom_sheet.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class ShareOwnerCard extends StatelessWidget {
  final int index;
  final ShareOwnerFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const ShareOwnerCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: TextFieldBlocBuilder(
              textFieldBloc: fieldBloc.shareOwnerName,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'Nama Pemegang Saham',
                floatingLabelBehavior: FloatingLabelBehavior.always,
                hintText: '',
                border: PralistingInputDecoration.outlineInputBorder,
                contentPadding: EdgeInsets.all(Sizes.s15),
                isDense: true,
                errorMaxLines: 5,
                hintStyle: PralistingInputDecoration.hintTextStyle(false),
                labelStyle: PralistingInputDecoration.labelTextStyle,
              ),
            ),
          ),
          SizedBox(width: Sizes.s10),
          Expanded(
            flex: 1,
            child: TextFieldBlocBuilder(
              textFieldBloc: fieldBloc.shareOwnerPercentage,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Persentase Saham',
                floatingLabelBehavior: FloatingLabelBehavior.always,
                hintText: '',
                border: PralistingInputDecoration.outlineInputBorder,
                contentPadding: EdgeInsets.all(Sizes.s15),
                isDense: true,
                errorMaxLines: 5,
                hintStyle: PralistingInputDecoration.hintTextStyle(false),
                labelStyle: PralistingInputDecoration.labelTextStyle,
                suffix: InkWell(
                  child: Icon(
                    Icons.info,
                    color: Colors.grey[300],
                    size: Sizes.s20,
                  ),
                  onTap: () {
                    MessageBottomSheet.show(
                      context,
                      title: "Persentase Saham",
                      message:
                          "Sesuai lampiran Keputusan Menteri Hukum dan HAM Republik Indonesia perusahaan yang didaftarkan.",
                    );
                  },
                ),
                // suffix: PralistingInfoButton(
                //   title: "Persentase Saham",
                //   message:
                //       "Sesuai lampiran Keputusan Menteri Hukum dan HAM Republik Indonesia perusahaan yang didaftarkan.",
                // ),
              ),
            ),
          ),
          index > 1
              ? InkWell(
                  onTap: index > 1 ? onRemove : null,
                  child: Container(
                    padding: EdgeInsets.only(left: Sizes.s15),
                    child: Icon(
                      Icons.delete,
                      color: index > 1 ? Colors.red : Colors.transparent,
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
