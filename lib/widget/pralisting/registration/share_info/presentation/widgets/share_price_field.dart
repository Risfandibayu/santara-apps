import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class SharePriceField extends StatelessWidget {
  final ShareInfoBloc bloc;
  SharePriceField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return BalanceDecimalInputField(
      bloc: bloc.sharePrice,
      label: "Harga Saham Per Lembar",
      hintText: "100",
    );
  }
}
