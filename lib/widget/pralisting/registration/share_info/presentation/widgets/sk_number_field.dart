import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class SkNumberField extends StatelessWidget {
  final ShareInfoBloc bloc;
  SkNumberField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc.skNumber,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: "No Dokumen SK Kemenkunham (Opsional)",
          hintText: "",
          hintStyle: PralistingInputDecoration.hintTextStyle(
            false,
          ),
          labelStyle: PralistingInputDecoration.labelTextStyle,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }
}
