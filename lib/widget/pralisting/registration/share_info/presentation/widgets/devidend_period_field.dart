import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class DevidendPeriodField extends StatelessWidget {
  final ShareInfoBloc bloc;
  DevidendPeriodField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.devidendPeriod.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.devidendPeriod.state.items != null &&
                    bloc.devidendPeriod.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.devidendPeriod.state.items,
                      selected: bloc.devidendPeriod.value,
                      showSearch: false,
                    );
                    bloc.devidendPeriod.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Periode Pembagian Dividen",
            hintText: "",
            state: fieldState,
            value: bloc.devidendPeriod.value?.name,
            errorText: !bloc.devidendPeriod.state.isInitial
                ? bloc.devidendPeriod.state.hasError
                    ? bloc.devidendPeriod.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
