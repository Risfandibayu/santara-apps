import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/existing_location_card.dart';

class ExistingLocationField extends StatelessWidget {
  final ShareInfoBloc bloc;
  ExistingLocationField({@required this.bloc});

  Widget _existingLocationsWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<BusinessLocationFieldBloc>,
            ListFieldBlocState<BusinessLocationFieldBloc>>(
          bloc: bloc.businessExistingLocations,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return ExistingLocationCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: () => bloc.removeExistingLocation(i),
                  );
                },
              );
            }
            return Container();
          },
        ),
        bloc.businessExistingLocations.state.fieldBlocs.isNotEmpty &&
                bloc.businessExistingLocations.state.fieldBlocs.length < 10
            ? FlatButton.icon(
                padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                onPressed: () => bloc.addNewExistingLocation(),
                icon: Text(
                  "Tambahkan Usaha Eksisting",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff218196),
                    decoration: TextDecoration.underline,
                  ),
                ),
                label: Icon(
                  Icons.add_circle,
                  size: FontSize.s20,
                  color: Color(0xff218196),
                ),
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: _existingLocationsWidget(),
    );
  }
}
