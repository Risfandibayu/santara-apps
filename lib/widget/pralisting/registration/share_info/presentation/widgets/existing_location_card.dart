import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class ExistingLocationCard extends StatelessWidget {
  final int index;
  final BusinessLocationFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const ExistingLocationCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s30, bottom: Sizes.s20),
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Lokasi Usaha Eksisting #${index + 1}',
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
              ),
            ),
            index > 0
                ? FlatButton.icon(
                    padding: EdgeInsets.zero,
                    onPressed: onRemove,
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: FontSize.s20,
                    ),
                    label: Text(
                      "Hapus",
                      style: TextStyle(
                        color: Colors.red,
                        decoration: TextDecoration.underline,
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.businessExistingName,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Nama Usaha Eksisting',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.businessExistingAddress,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Alamat Usaha Eksisting',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        SizedBox(height: Sizes.s10),
      ],
    );
  }
}
