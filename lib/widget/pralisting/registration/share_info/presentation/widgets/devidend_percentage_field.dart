import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';

class DevidendSharePercentageField extends StatelessWidget {
  final ShareInfoBloc bloc;
  DevidendSharePercentageField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc.devidendSharePercentage,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: "Jumlah Dividen Share (% dari laba bersih)",
          hintText: "49",
          hintStyle: PralistingInputDecoration.hintTextStyle(
            false,
          ),
          labelStyle: PralistingInputDecoration.labelTextStyle,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        keyboardType: TextInputType.number,
      ),
    );
  }
}
