import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/widgets/share_owner_card.dart';

class ShareOwnersField extends StatelessWidget {
  final ShareInfoBloc bloc;
  ShareOwnersField({@required this.bloc});

  Widget _shareOwnersWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<ShareOwnerFieldBloc>,
            ListFieldBlocState<ShareOwnerFieldBloc>>(
          bloc: bloc.shareOwners,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return ShareOwnerCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: () => bloc.removeShareOwner(i),
                  );
                },
              );
            }
            return Container();
          },
        ),
        bloc.shareOwners.state.fieldBlocs.isNotEmpty &&
                bloc.shareOwners.state.fieldBlocs.length < 10
            ? FlatButton.icon(
                padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                onPressed: () => bloc.addNewShareOwner(),
                icon: Text(
                  "Tambahkan Pemegang Saham",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff218196),
                    decoration: TextDecoration.underline,
                  ),
                ),
                label: Icon(
                  Icons.add_circle,
                  size: FontSize.s20,
                  color: Color(0xff218196),
                ),
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Susunan Pemegang Saham Awal (Opsional)",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: FontSize.s14,
              ),
            ),
          ),
          _shareOwnersWidget()
        ],
      ),
    );
  }
}
