import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/models/share_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/domain/usecases/share_info_usecases.dart';

class ShareInfoBloc extends FormBloc<FormResult, FormResult> {
  final GetShareInfo getShareInfo;
  final UpdateShareInfo updateShareInfo;
  String uuid;
  Map previousState;

  final shareTotalPercentage = TextFieldBloc(
    name: 'share_amount',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.stockTotalPercentage,
    ],
  );

  final sharePrice = TextFieldBloc(name: 'price', validators: [
    PralistingValidationHelper.required,
  ]);

  final devidendPeriod = SelectFieldBloc<SearchData, dynamic>(
    name: 'period',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
    items: periodValue,
  );

  final devidendSharePercentage = TextFieldBloc(
    name: 'dividend_percentage',
    validators: [PralistingValidationHelper.required],
  );

  final skNumber = TextFieldBloc(
    name: 'sk_number_kemenkumham',
    validators: [],
    initialValue: "AHU-",
  );

  final shareOwners =
      ListFieldBloc<ShareOwnerFieldBloc>(name: 'early_investors');

  final businessExistingLocations = ListFieldBloc<BusinessLocationFieldBloc>(
    name: 'locations',
  );

  final businessNewLocations = ListFieldBloc<BusinessNewAddressFieldBloc>(
    name: 'new_locations',
  );

  ShareInfoBloc({
    @required this.getShareInfo,
    @required this.updateShareInfo,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      shareTotalPercentage,
      sharePrice,
      devidendPeriod,
      devidendSharePercentage,
      skNumber,
      shareOwners,
      businessExistingLocations,
      businessNewLocations,
    ]);
  }

  void addNewShareOwner({bool mandatory = false}) {
    shareOwners.addFieldBloc(
      ShareOwnerFieldBloc(
        name: 'early_investors',
        shareOwnerName: TextFieldBloc(
          name: 'name',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
        shareOwnerPercentage: TextFieldBloc(
          name: 'percentage',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
      ),
    );
  }

  void removeShareOwner(int index) {
    shareOwners.removeFieldBlocAt(index);
  }

  void addNewExistingLocation({bool mandatory = false}) {
    businessExistingLocations.addFieldBloc(
      BusinessLocationFieldBloc(
        name: 'locations',
        businessExistingAddress: TextFieldBloc(
          name: 'address',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
        businessExistingName: TextFieldBloc(
          name: 'name',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
      ),
    );
  }

  void removeExistingLocation(int index) {
    businessExistingLocations.removeFieldBlocAt(index);
  }

  void addNewBusinessLocation({bool mandatory = false}) {
    businessNewLocations.addFieldBloc(
      BusinessNewAddressFieldBloc(
        name: 'new_locations',
        businessNewAddress: TextFieldBloc(
          name: 'address',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
        businessNewName: TextFieldBloc(
          name: 'name',
          validators: mandatory ? [PralistingValidationHelper.required] : [],
        ),
      ),
    );
  }

  void removeNewBusinessLocation(int index) {
    businessNewLocations.removeFieldBlocAt(index);
  }

  getData() async {
    try {
      var result = await getShareInfo(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && !data.isClean) {
          shareTotalPercentage
              .updateValue("${data.shareAmount}".replaceAll(".", ","));
          sharePrice
              .updateValue("${PralistingHelper.parseToDecimal(data.price)}");
          devidendPeriod.updateValue(devidendPeriod.state.items
              .firstWhere((e) => e.uuid == "${data.period}"));
          devidendSharePercentage
              .updateValue("${data.dividendPercentage}".replaceAll(".", ","));
          skNumber.updateValue("${data.skNumberKemenkumham}");
          // Susunan Pemegang Saham Awal
          if (data.earlyInvestors != null && data.earlyInvestors.length > 0) {
            data.earlyInvestors.asMap().forEach((key, value) async {
              addNewShareOwner(mandatory: false);
              await Future.delayed(Duration(milliseconds: 100));
              shareOwners.state.fieldBlocs[key].shareOwnerName
                  .updateValue(value.name);
              shareOwners.state.fieldBlocs[key].shareOwnerPercentage
                  .updateValue(value.percentage != null && value.percentage > 0
                      ? "${value.percentage}".replaceAll(".", ",")
                      : "");
            });
          }

          if (data.locations != null && data.locations.length > 0) {
            data.locations.asMap().forEach((key, value) async {
              addNewExistingLocation(mandatory: key > 1 ? false : true);
              await Future.delayed(Duration(milliseconds: 100));
              businessExistingLocations
                  .state.fieldBlocs[key].businessExistingName
                  .updateValue(value.name);
              businessExistingLocations
                  .state.fieldBlocs[key].businessExistingAddress
                  .updateValue(value.address);
            });
          }

          if (data.newLocations != null && data.newLocations.length > 0) {
            data.newLocations.asMap().forEach((key, value) async {
              addNewBusinessLocation(mandatory: key > 1 ? false : true);
              await Future.delayed(Duration(milliseconds: 100));
              businessNewLocations.state.fieldBlocs[key].businessNewAddress
                  .updateValue(value.address);
              businessNewLocations.state.fieldBlocs[key].businessNewName
                  .updateValue(value.name);
            });
          }
        } else {
          initFields();
        }
        emitLoaded();
      } else {
        result.leftMap((l) {
          initFields();
          emitLoaded();
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      initFields();
      emitLoaded();
    }
  }

  initFields() {
    devidendPeriod.updateValue(periodValue[0]);
    addNewShareOwner(mandatory: false);
    addNewShareOwner(mandatory: false);
    //
    addNewExistingLocation(mandatory: true);
    //
    addNewBusinessLocation(mandatory: true);
  }

  // ngecek apakah susunan pemegang saham awal valid
  // jika tidak maka set early investors jadi false
  bool parseEarlyInvestors(dynamic body) {
    try {
      var investors = List<EarlyInvestor>.from(
        body["early_investors"].map((x) => EarlyInvestor.fromJson(x)),
      );
      if (investors.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  postData(Map<String, dynamic> body, {bool isNext}) async {
    try {
      emitSubmitting();
      body['share_amount'] = body['share_amount'].replaceAll(",", ".");
      body['dividend_percentage'] =
          body['dividend_percentage'].replaceAll(",", ".");
      // jika susunan pemegang saham awal kosong
      if (!parseEarlyInvestors(body)) {
        body["early_investors"] = [];
      }
      body['period'] = body['period'].uuid;
      // logger.i(body);
      var shareInfo = ShareInfoModel.fromJson(body);
      var payload = {"uuid": uuid, "body": shareInfo};
      var result = await updateShareInfo(payload);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: data.meta.message,
        );
        emitSuccess(canSubmitAgain: true, successResponse: success);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}

class ShareOwnerFieldBloc extends GroupFieldBloc {
  final TextFieldBloc shareOwnerName;
  final TextFieldBloc shareOwnerPercentage;

  ShareOwnerFieldBloc({
    @required this.shareOwnerName,
    @required this.shareOwnerPercentage,
    String name,
  }) : super([shareOwnerName, shareOwnerPercentage], name: name);
}

class BusinessLocationFieldBloc extends GroupFieldBloc {
  final TextFieldBloc businessExistingName;
  final TextFieldBloc businessExistingAddress;

  BusinessLocationFieldBloc({
    @required this.businessExistingName,
    @required this.businessExistingAddress,
    String name,
  }) : super([businessExistingName, businessExistingAddress], name: name);
}

class BusinessNewAddressFieldBloc extends GroupFieldBloc {
  final TextFieldBloc businessNewName;
  final TextFieldBloc businessNewAddress;

  BusinessNewAddressFieldBloc({
    @required this.businessNewName,
    @required this.businessNewAddress,
    String name,
  }) : super([businessNewName, businessNewAddress], name: name);
}

var periodValue = [
  SearchData(
    id: "1",
    uuid: "6",
    name: "6 Bulan",
  ),
  SearchData(
    id: "2",
    uuid: "12",
    name: "12 Bulan",
  ),
];
