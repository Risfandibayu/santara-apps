import 'package:santaraapp/widget/pralisting/registration/share_info/data/datasources/share_info_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/repositories/share_info_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/domain/repositories/share_info_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/domain/usecases/share_info_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/blocs/share_info_bloc.dart';
import '../../../../injector_container.dart';

void initShareInfoInjector() {
  sl.registerFactory(
    () => ShareInfoBloc(
      getShareInfo: sl(),
      updateShareInfo: sl(),
    ),
  );

  sl.registerLazySingleton<ShareInfoRepository>(
    () => ShareInfoRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<ShareInfoRemoteDataSources>(
    () => ShareInfoRemoteDataSourcesImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetShareInfo(sl()));
  sl.registerLazySingleton(() => UpdateShareInfo(sl()));
}
