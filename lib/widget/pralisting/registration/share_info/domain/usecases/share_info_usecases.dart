import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/models/share_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/domain/repositories/share_info_repository.dart';

class GetShareInfo implements UseCase<ShareInfoModel, String> {
  final ShareInfoRepository repository;
  GetShareInfo(this.repository);

  @override
  Future<Either<Failure, ShareInfoModel>> call(String uuid) async {
    return await repository.getShareInfo(uuid: uuid);
  }
}

class UpdateShareInfo implements UseCase<PralistingMetaModel, dynamic> {
  final ShareInfoRepository repository;
  UpdateShareInfo(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateShareInfo(
      body: params['body'],
      uuid: params['uuid'],
    );
  }
}
