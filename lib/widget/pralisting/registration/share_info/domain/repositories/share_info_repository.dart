import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/data/models/share_info_model.dart';
import 'package:meta/meta.dart';

abstract class ShareInfoRepository {
  Future<Either<Failure, ShareInfoModel>> getShareInfo({@required String uuid});

  Future<Either<Failure, PralistingMetaModel>> updateShareInfo({
    @required String uuid,
    @required dynamic body,
  });
}
