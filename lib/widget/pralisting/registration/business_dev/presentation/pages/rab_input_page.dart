import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

import '../../../../../../injector_container.dart';
import '../../../../../../utils/sizes.dart';
import '../../../../../widget/components/main/SantaraButtons.dart';
import '../blocs/rab_input_bloc.dart';
import '../widgets/component_plan_table.dart';
import '../widgets/total_field.dart';

class RabInputPage extends StatefulWidget {
  final String uuid;
  RabInputPage({@required this.uuid});
  @override
  _RabInputPageState createState() => _RabInputPageState();
}

class _RabInputPageState extends State<RabInputPage> {
  RabInputBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<RabInputBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Input Rencana Anggaran Biaya",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<RabInputBloc>();
              return FormBlocListener<RabInputBloc, FormResult, FormResult>(
                onLoaded: (context, state) {},
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pop(context, true);
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<RabInputBloc, FormBlocState>(
                    builder: (context, state) {
                  return state is FormBlocLoading
                      ? Center(
                          child: CupertinoActivityIndicator(),
                        )
                      : Container(
                          width: double.maxFinite,
                          height: double.maxFinite,
                          color: Colors.white,
                          child: SingleChildScrollView(
                            padding: EdgeInsets.all(Sizes.s20),
                            physics: BouncingScrollPhysics(),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BlocBuilder<ListFieldBloc<RabTableFieldBloc>,
                                    ListFieldBlocState<RabTableFieldBloc>>(
                                  bloc: bloc.rabTable,
                                  builder: (context, state) {
                                    if (state.fieldBlocs.isNotEmpty) {
                                      return ListView.builder(
                                        shrinkWrap: true,
                                        physics: const ClampingScrollPhysics(),
                                        itemCount: state.fieldBlocs.length,
                                        itemBuilder: (context, i) {
                                          return ComponentPlanTable(
                                            bloc: bloc,
                                            componentPlanIndex: i,
                                            rabTableFieldBloc:
                                                state.fieldBlocs[i],
                                          );
                                        },
                                      );
                                    }
                                    return Container();
                                  },
                                ),
                                TotalField(
                                    rabTable: bloc.rabTable.value,
                                    title: "Total",
                                    subtotalPrice: bloc.totalPrice),
                                Container(
                                  height: Sizes.s45,
                                  width: double.maxFinite,
                                  margin: EdgeInsets.only(top: Sizes.s40),
                                  child: SantaraMainButton(
                                    key: Key('add_new_table_button'),
                                    title: "Tambah Tabel Baru",
                                    color: Color(0xFF666EE8),
                                    onPressed: () {
                                      bloc.addRabTable(3);
                                    },
                                  ),
                                ),
                                Container(
                                  height: Sizes.s45,
                                  width: double.maxFinite,
                                  margin: EdgeInsets.only(top: Sizes.s40),
                                  child: SantaraMainButton(
                                    key: Key('submit_button'),
                                    title: "Simpan",
                                    onPressed: () {
                                      if (bloc.state.canSubmit) {
                                        if (bloc.state.isValid()) {
                                          bloc.submit();
                                        } else {
                                          bloc.submit();
                                          ToastHelper.showFailureToast(
                                            context,
                                            "Maaf, tidak dapat melanjutkan, mohon cek kembali form anda!",
                                          );
                                        }
                                      } else {
                                        ToastHelper.showFailureToast(
                                          context,
                                          "Maaf, tidak dapat melanjutkan, mohon cek kembali form anda!",
                                        );
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                }),
              );
            },
          ),
        ),
      ),
    );
  }
}
