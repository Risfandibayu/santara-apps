import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/pages/rab_input_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/widgets/rab_input_button.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/pages/financial_projections_page.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../../../../../core/widgets/pralisting_appbar/pralisting_appbar.dart';
import '../../../../../../core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import '../../../../../../helpers/PralistingHelper.dart';
import '../../../../../../injector_container.dart';
import '../../../../../../utils/sizes.dart';
import '../../../../../widget/components/kyc/KycFieldWrapper.dart';
import '../blocs/business_dev_bloc.dart';
import '../widgets/default_text_field.dart';
import '../widgets/project_desc_field.dart';

class BusinessDevPage extends StatefulWidget {
  final String uuid;
  BusinessDevPage({@required this.uuid});

  @override
  _BusinessDevPageState createState() => _BusinessDevPageState();
}

class _BusinessDevPageState extends State<BusinessDevPage> {
  BusinessDevBloc bloc;
  AutoScrollController controller;
  KycFieldWrapper fieldWrapper;
  final scrollDirection = Axis.vertical;

  @override
  void initState() {
    super.initState();
    bloc = sl<BusinessDevBloc>();
    bloc.uuid = widget.uuid;
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  void updateValueRab() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RabInputPage(uuid: widget.uuid),
      ),
    );
    if (result != null && result) {
      setState(() {
        bloc.inputRab.updateValue(jsonEncode(result));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Pengembangan Usaha",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "6 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            color: Colors.white,
            child: BlocProvider<BusinessDevBloc>(
              create: (_) => bloc,
              child: Builder(
                builder: (context) {
                  // ignore: close_sinks
                  final bloc = context.bloc<BusinessDevBloc>();
                  return FormBlocListener<BusinessDevBloc, FormResult,
                      FormResult>(
                    onLoaded: (context, state) {
                      PralistingHelper.handleAutoScroll(
                        fieldWrapper: fieldWrapper,
                        state: state,
                      );
                    },
                    onSubmitting: (context, state) {
                      PopupHelper.showLoading(context);
                    },
                    onSuccess: (context, state) {
                      final form = state.successResponse;
                      if (form.status == FormStatus.submit_success) {
                        Navigator.pop(context);
                        ToastHelper.showSuccessToast(context, form.message);
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => FinancialProjectionsPage(
                              uuid: "${widget.uuid}",
                            ),
                          ),
                        );
                      } else if (form.status ==
                          FormStatus.submit_success_close_page) {
                        PralistingHelper.handleOnSavePralisting(
                          context,
                          message: form.message,
                        );
                      } else if (form.status == FormStatus.delete_success) {
                        Navigator.pop(context);
                        ToastHelper.showSuccessToast(context, form.message);
                      }
                    },
                    onFailure: (context, state) {
                      final form = state.failureResponse;
                      if (form.status == FormStatus.submit_error ||
                          form.status == FormStatus.delete_error) {
                        Navigator.pop(context);
                        ToastHelper.showFailureToast(context, form.message);
                      } else {
                        ToastHelper.showFailureToast(context, form.message);
                      }
                    },
                    onSubmissionFailed: (context, state) {
                      PralistingHelper.showSubmissionFailed(context);
                      PralistingHelper.handleAutoScroll(
                        fieldWrapper: fieldWrapper,
                        state: state,
                      );
                    },
                    child: BlocBuilder<BusinessDevBloc, FormBlocState>(
                      builder: (context, state) {
                        return state is FormBlocLoading
                            ? Center(
                                child: CupertinoActivityIndicator(),
                              )
                            : SingleChildScrollView(
                                padding: EdgeInsets.all(Sizes.s20),
                                scrollDirection: scrollDirection,
                                controller: controller,
                                physics: BouncingScrollPhysics(),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ProjectDescField(bloc: bloc),
                                    SizedBox(height: Sizes.s10),
                                    DefaultTextField(
                                        textFieldBloc: bloc.location,
                                        labelText: 'Lokasi Project Baru',
                                        keyboardType: TextInputType.text),
                                    SizedBox(height: Sizes.s10),
                                    BalanceDecimalInputField(
                                      bloc: bloc.fundNeeds,
                                      label: 'Kebutuhan Dana',
                                    ),
                                    SizedBox(height: Sizes.s10),
                                    BalanceDecimalInputField(
                                      bloc: bloc.minFund,
                                      label: 'Kebutuhan Dana Minimum',
                                    ),
                                    RichText(
                                      textAlign: TextAlign.justify,
                                      text: TextSpan(
                                        style: TextStyle(
                                          color: Color(0xffBF0000),
                                          fontFamily: 'Nunito',
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(text: "Disclamimer : "),
                                          TextSpan(
                                            text:
                                                "Kekurangan dana atas total project cost akan ditanggung oleh manajemen internal",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: Sizes.s10),
                                    DefaultTextField(
                                      labelText:
                                          'Estimasi Jangka Waktu Pengerjaan (bulan)',
                                      textFieldBloc: bloc.estimation,
                                      keyboardType: TextInputType.number,
                                    ),
                                    SizedBox(height: Sizes.s10),
                                    Text("Rencana Anggaran Biaya (RAB)",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    RichText(
                                      textAlign: TextAlign.justify,
                                      text: TextSpan(
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'Nunito',
                                          fontSize: FontSize.s12,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  "Silahkan input Rencana Anggaran Biaya (RAB) Anda dengan menekan tombol Input RAB dibawah ini. "),
                                          TextSpan(
                                            text:
                                                "*Laporan Keuangan yang disajikan minimal standar ETAP",
                                            style: TextStyle(
                                                color: Color(0xffBF0000)),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: Sizes.s10),
                                    RabInputButton(bloc: bloc),
                                    SizedBox(height: Sizes.s10),
                                    PralistingSubmitButton(
                                      nextKey: 'pralisting_submit_button',
                                      saveKey: 'pralisting_save_button',
                                      onTapNext: () =>
                                          bloc.submitForm(isNext: true),
                                      onTapSave: () =>
                                          bloc.submitForm(isNext: false),
                                    ),
                                  ],
                                ),
                              );
                      },
                    ),
                  );
                },
              ),
            )),
      ),
    );
  }
}
