import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/domain/usecase/business_dev_usecases.dart';

class RabInputBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetRabInput getRabInput;
  final UpdateRabInput updateRabInput;

  final rabTable = ListFieldBloc<RabTableFieldBloc>(
    name: 'plans',
    fieldBlocs: [],
  );

  final totalPrice = TextFieldBloc(
    name: 'total_price',
    validators: [],
  );

  RabInputBloc({
    @required this.getRabInput,
    @required this.updateRabInput,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        rabTable,
        totalPrice,
      ],
    );
  }

  List<ComponentColumnFieldBloc> initialComponentColumn(int length) {
    var c = List<ComponentColumnFieldBloc>();
    for (var i = 0; i < length; i++) {
      c.add(ComponentColumnFieldBloc(
        name: 'rows',
        description: TextFieldBloc(name: 'desc'),
        unit: TextFieldBloc(name: 'unit'),
        price: TextFieldBloc(name: 'price'),
        amount: TextFieldBloc(name: 'amount'),
        totalPrice: TextFieldBloc(name: 'totalPrice'),
      ));
    }
    return c;
  }

  void addRabTable(int length) async {
    rabTable.addFieldBloc(RabTableFieldBloc(
      name: 'plans',
      componentColumn: ListFieldBloc<ComponentColumnFieldBloc>(
        name: 'rows',
        fieldBlocs: initialComponentColumn(length),
      ),
      componentName: TextFieldBloc(name: 'name'),
      subtotalPrice: TextFieldBloc(name: 'subtotal_price'),
      isDeleteRow: BooleanFieldBloc(name: 'is_delete_row'),
    ));
  }

  void removeRabTable(int index) async {
    rabTable.removeFieldBlocAt(index);
  }

  void addComponentColumn(int index) {
    rabTable.state.fieldBlocs[index].componentColumn
        .addFieldBloc(ComponentColumnFieldBloc(
      name: 'rows',
      description: TextFieldBloc(name: 'desc'),
      unit: TextFieldBloc(name: 'unit'),
      price: TextFieldBloc(name: 'price'),
      amount: TextFieldBloc(name: 'amount'),
      totalPrice: TextFieldBloc(name: 'totalPrice'),
    ));
  }

  void removeComponentColumn(int index, List<int> numberToRemove) {
    for (var i = 0; i < numberToRemove.length; i++) {
      rabTable.state.fieldBlocs[index].componentColumn
          .removeFieldBlocAt(numberToRemove[i]);
    }
    if (rabTable.state.fieldBlocs.length == 0) {
      rabTable.state.fieldBlocs[index].componentColumn
          .addFieldBloc(ComponentColumnFieldBloc(
        name: 'rows',
        description: TextFieldBloc(name: 'desc'),
        unit: TextFieldBloc(name: 'unit'),
        price: TextFieldBloc(name: 'price'),
        amount: TextFieldBloc(name: 'amount'),
        totalPrice: TextFieldBloc(name: 'totalPrice'),
      ));
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  static int parseToInt(String value) {
    try {
      return int.parse(value.replaceAll(",", ""));
    } catch (e) {
      return 0;
    }
  }

  postData(Map<String, dynamic> body) async {
    try {
      body["plans"].forEach((elem) {
        elem["rows"].forEach((value) {
          value["price"] = parseToInt(value["price"]);
          value["amount"] = parseToInt(value["amount"]);
          value["totalPrice"] = parseToInt(value["totalPrice"]);
        });
      });
      var payload = {"uuid": uuid, "body": body};
      // logger.i(payload);
      var result = await updateRabInput(payload);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: FormStatus.submit_success,
          message: data?.meta?.message,
        );
        emitSuccess(canSubmitAgain: true, successResponse: success);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l?.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  getData() async {
    try {
      var result = await getRabInput(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && data.plans != null && data.plans.length > 0) {
          data.plans.asMap().forEach((index, elem) async {
            addRabTable(0);
            await Future.delayed(Duration(milliseconds: 100));
            rabTable.state.fieldBlocs[index].componentName
                .updateValue(elem.name);
            elem.rows.asMap().forEach((i, row) async {
              addComponentColumn(index);
              await Future.delayed(Duration(milliseconds: 100));
              // ignore: close_sinks
              var rowState = rabTable
                  .state.fieldBlocs[index].componentColumn.state.fieldBlocs[i];
              rowState.description.updateValue(row.desc);
              rowState.price.updateValue("${row.price}");
              rowState.unit.updateValue("${row.unit}");
              rowState.totalPrice.updateValue("${row.price * row.amount}");
              rowState.amount.updateValue("${row.amount}");
            });
          });
        } else {
          addRabTable(3);
        }
      } else {
        addRabTable(3);
        result.leftMap((l) {
          printFailure(l);
        });
      }
    } catch (e, stack) {
      addRabTable(3);
      santaraLog(e, stack);
    }
    emitLoaded();
  }

  @override
  void onSubmitting() async {
    var body = state.toJson();
    await postData(body);
  }
}

class RabTableFieldBloc extends GroupFieldBloc {
  final TextFieldBloc componentName;
  final ListFieldBloc<ComponentColumnFieldBloc> componentColumn;
  final TextFieldBloc subtotalPrice;
  final BooleanFieldBloc isDeleteRow;

  RabTableFieldBloc({
    @required this.componentName,
    @required this.componentColumn,
    @required this.subtotalPrice,
    @required this.isDeleteRow,
    String name,
  }) : super([componentName, componentColumn, subtotalPrice, isDeleteRow],
            name: name);
}

class ComponentColumnFieldBloc extends GroupFieldBloc {
  final TextFieldBloc description;
  final TextFieldBloc unit;
  final TextFieldBloc price;
  final TextFieldBloc amount;
  final TextFieldBloc totalPrice;

  ComponentColumnFieldBloc({
    @required this.description,
    @required this.unit,
    @required this.price,
    @required this.amount,
    @required this.totalPrice,
    String name,
  }) : super([description, unit, price, amount, totalPrice], name: name);
}
