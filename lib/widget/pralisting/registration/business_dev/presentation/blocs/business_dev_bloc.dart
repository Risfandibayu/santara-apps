import 'package:form_bloc/form_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/domain/usecase/business_dev_usecases.dart';
import 'package:meta/meta.dart';

class BusinessDevBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetBusinessDev getBusinessDev;
  final UpdateBusinessDev updateBusinessDev;
  final DeleteRab deleteRab;

  final numberFormat = NumberFormat("###,###,###", "en_us");

  final description = TextFieldBloc(
    name: 'new_desc',
    validators: [PralistingValidationHelper.required],
  );

  final location = TextFieldBloc(
    name: 'new_location',
    validators: [PralistingValidationHelper.required],
  );

  final fundNeeds = TextFieldBloc(
    name: 'new_fund',
    validators: [PralistingValidationHelper.required],
  );

  final minFund = TextFieldBloc(
    name: 'new_min_fund',
    validators: [PralistingValidationHelper.required],
  );

  final estimation = TextFieldBloc(
    name: 'new_period',
    validators: [PralistingValidationHelper.required],
  );

  final inputRab = TextFieldBloc(
    name: 'input_rab',
    validators: [PralistingValidationHelper.required],
  );

  BusinessDevBloc({
    @required this.getBusinessDev,
    @required this.updateBusinessDev,
    @required this.deleteRab,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        description,
        location,
        fundNeeds,
        minFund,
        estimation,
        inputRab
      ],
    );
  }

  static int parseToInt(String value) {
    try {
      return int.parse(value.replaceAll(",", ""));
    } catch (e) {
      return 0;
    }
  }

  String parseToDecimal(int value) {
    try {
      return numberFormat.format(value);
    } catch (e) {
      return "";
    }
  }

  getData() async {
    try {
      var result = await getBusinessDev(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && !data.isClean) {
          description.updateValue(data.newDesc);
          location.updateValue(data.newLocation);
          fundNeeds.updateValue(parseToDecimal(data.newFund));
          minFund.updateValue(parseToDecimal(data.newMinFund));
          estimation.updateValue(parseToDecimal(data.newPeriod));
          if (data.plans != null && data.plans.length > 0) {
            inputRab.updateValue("true");
          }
        }
      } else {
        result.leftMap((l) {
          printFailure(l);
        });
      }
      emitLoaded();
    } catch (e, stack) {
      santaraLog(e, stack);
      emitLoaded();
    }
  }

  postData(Map<String, dynamic> body, {bool isNext}) async {
    try {
      emitSubmitting();
      body["new_fund"] = parseToInt(body["new_fund"]);
      body["new_min_fund"] = parseToInt(body["new_min_fund"]);
      body["new_period"] = parseToInt(body["new_period"]);
      var payload = {"uuid": uuid, "body": body};
      var result = await updateBusinessDev(payload);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: data.meta.message,
        );
        emitSuccess(canSubmitAgain: true, successResponse: success);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  removeRab() async {
    try {
      emitSubmitting();
      var result = await deleteRab(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: FormStatus.delete_success,
          message: data.meta.message,
        );
        inputRab.updateValue("");
        emitSuccess(canSubmitAgain: true, successResponse: success);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.delete_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.delete_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  @override
  void onSubmitting() async {}
}
