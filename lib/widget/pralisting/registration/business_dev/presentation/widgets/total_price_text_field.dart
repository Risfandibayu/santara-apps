import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/blocs/rab_input_bloc.dart';

class TotalPriceTextField extends StatelessWidget {
  final ComponentColumnFieldBloc bloc;
  final TextFieldBloc textFieldBloc;
  final String hintText;
  final bool isEnabled;
  final TextInputType keyboardType;
  final List<TextInputFormatter> inputFormatters;

  const TotalPriceTextField(
      {Key key,
      this.bloc,
      this.textFieldBloc,
      this.hintText,
      this.isEnabled: true,
      this.inputFormatters,
      this.keyboardType})
      : super(key: key);

  void onCountTotalPrice() {
    try {
      final price = num.parse(bloc.price.state.value.replaceAll(",", ""));
      final amount = num.parse(bloc.amount.state.value.replaceAll(",", ""));
      final total = price * amount;
      textFieldBloc.updateValue(NumberFormatter.convertNumber(total));
    } catch (_, __) {
      textFieldBloc.updateValue("0");
    }
  }

  @override
  Widget build(BuildContext context) {
    onCountTotalPrice();
    return Theme(
      data: ThemeData(disabledColor: Colors.black),
      child: TextFieldBlocBuilder(
        textFieldBloc: textFieldBloc,
        keyboardType: keyboardType,
        minLines: 1,
        maxLines: 5,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 12, color: Colors.black),
        isEnabled: isEnabled,
        inputFormatters: inputFormatters,
        decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(fontSize: 12, color: Color(0xFFB8B8B8)),
            border: InputBorder.none),
      ),
    );
  }
}
