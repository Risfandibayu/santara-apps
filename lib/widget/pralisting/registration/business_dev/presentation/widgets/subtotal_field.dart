import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/CurrencyFormat.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';

import '../blocs/rab_input_bloc.dart';
import 'intable_textfield.dart';

class SubTotalField extends StatelessWidget {
  final List<ComponentColumnFieldBloc> bloc;
  final String title;
  final TextFieldBloc subtotalPrice;

  SubTotalField({Key key, this.bloc, this.title, this.subtotalPrice})
      : super(key: key);

  void onCountSubTotal() {
    num subTotal = 0;
    for (var i = 0; i < bloc.length; i++) {
      num totalPrice = 0;
      try {
        totalPrice =
            num.parse(bloc[i].totalPrice.state.value.replaceAll(",", ""));
      } catch (_, __) {
        totalPrice = 0;
      }
      subTotal += totalPrice;
    }
    subtotalPrice.updateValue(NumberFormatter.convertNumber(subTotal));
  }

  @override
  Widget build(BuildContext context) {
    onCountSubTotal();
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.only(right: 20),
      height: 56,
      decoration: BoxDecoration(
          border: Border.all(
            width: 1.5,
            color: Colors.black,
          ),
          borderRadius: BorderRadius.circular(6)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              height: 56,
              width: MediaQuery.of(context).size.width / 3,
              decoration: BoxDecoration(
                  color: Color(0xFFEDEDED),
                  border: Border.all(
                    width: 1,
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.circular(6)),
              child: Center(child: Text(title))),
          Flexible(
            child: Theme(
              data: ThemeData(disabledColor: Colors.black),
              child: InTableTextField(
                isEnabled: false,
                textFieldBloc: subtotalPrice,
                hintText: '',
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyFormat(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
