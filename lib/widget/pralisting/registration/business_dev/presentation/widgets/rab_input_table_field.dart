import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/CurrencyFormat.dart';

import '../blocs/rab_input_bloc.dart';
import 'intable_textfield.dart';
import 'table_header.dart';
import 'total_price_text_field.dart';

class RabInputTableField extends StatelessWidget {
  final RabTableFieldBloc bloc;
  final List<int> numberToRemove;

  const RabInputTableField({Key key, this.bloc, this.numberToRemove})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ListFieldBloc<ComponentColumnFieldBloc>,
            ListFieldBlocState<ComponentColumnFieldBloc>>(
        bloc: bloc.componentColumn,
        builder: (context, state) {
          if (state.fieldBlocs.isNotEmpty) {
            return SingleChildScrollView(
              padding: EdgeInsets.symmetric(vertical: 20),
              scrollDirection: Axis.horizontal,
              child: Table(
                  columnWidths: {
                    0: FixedColumnWidth(200),
                    1: FixedColumnWidth(60),
                    2: FixedColumnWidth(100),
                    3: FixedColumnWidth(75),
                    4: FixedColumnWidth(125)
                  },
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  border: TableBorder.all(
                      color: Colors.black,
                      style: BorderStyle.solid,
                      width: 1.5),
                  children: state.fieldBlocs
                      .asMap()
                      .map((i, e) {
                        if (i == 0) {
                          return MapEntry(
                              i,
                              TableRow(
                                children: [
                                  TableHeader(
                                    "Keterangan",
                                    InTableTextFieldWithCheckBox(
                                      bloc: bloc,
                                      index: i,
                                      numberToRemove: numberToRemove,
                                      textFieldBloc: e.description,
                                      hintText: "Input Pekerjaan",
                                      keyboardType: TextInputType.multiline,
                                    ),
                                  ),
                                  TableHeader(
                                    "Satuan",
                                    InTableTextField(
                                      textFieldBloc: e.unit,
                                      hintText: "Input",
                                      keyboardType: TextInputType.text,
                                    ),
                                  ),
                                  TableHeader(
                                    "Harga (Rp)",
                                    InTableTextField(
                                      textFieldBloc: e.price,
                                      hintText: "Input",
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                        CurrencyFormat(),
                                      ],
                                    ),
                                  ),
                                  TableHeader(
                                    "Jumlah",
                                    InTableTextField(
                                      textFieldBloc: e.amount,
                                      hintText: "Input",
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                        CurrencyFormat(),
                                      ],
                                    ),
                                  ),
                                  TableHeader(
                                    "Total (Rp)",
                                    TotalPriceTextField(
                                      bloc: e,
                                      textFieldBloc: e.totalPrice,
                                      hintText: "Input",
                                      isEnabled: false,
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                        CurrencyFormat(),
                                      ],
                                    ),
                                  ),
                                ],
                              ));
                        } else {
                          return MapEntry(
                              i,
                              TableRow(
                                children: [
                                  InTableTextFieldWithCheckBox(
                                    bloc: bloc,
                                    index: i,
                                    numberToRemove: numberToRemove,
                                    textFieldBloc: e.description,
                                    hintText: "Input Pekerjaan",
                                    keyboardType: TextInputType.multiline,
                                  ),
                                  InTableTextField(
                                    textFieldBloc: e.unit,
                                    hintText: "Input",
                                    keyboardType: TextInputType.text,
                                  ),
                                  InTableTextField(
                                    textFieldBloc: e.price,
                                    hintText: "Input",
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly,
                                      CurrencyFormat(),
                                    ],
                                  ),
                                  InTableTextField(
                                    textFieldBloc: e.amount,
                                    hintText: "Input",
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly,
                                      CurrencyFormat(),
                                    ],
                                  ),
                                  TotalPriceTextField(
                                    bloc: e,
                                    textFieldBloc: e.totalPrice,
                                    hintText: "Input",
                                    isEnabled: false,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly,
                                      CurrencyFormat(),
                                    ],
                                  )
                                ],
                              ));
                        }
                      })
                      .values
                      .toList()),
            );
          }
          return Container();
        });
  }
}
