import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/blocs/business_dev_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/pages/rab_input_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/bottom_sheet_confirmation.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/input_done_button.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class RabInputButton extends StatelessWidget {
  final BusinessDevBloc bloc;
  RabInputButton({@required this.bloc});

  updateRab(context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RabInputPage(uuid: bloc.uuid),
      ),
    );
    if (result != null && result) {
      bloc.inputRab.updateValue("$result");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        bloc.inputRab.state.value.isNotEmpty
            ? InputDoneButton(
                onEdit: () {
                  FinancialBottomSheetConfirmation.show(
                    context,
                    title: "Edit Data",
                    description:
                        "Data yang sudah diunggah akan terhapus jika Anda edit data. Yakin untuk edit data?",
                    actionButton: SantaraMainButton(
                      title: "Edit",
                      onPressed: () async {
                        Navigator.pop(context);
                        final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RabInputPage(
                              uuid: bloc.uuid,
                            ),
                          ),
                        );
                        if (result != null && result) {
                          bloc.inputRab.updateValue("$result");
                        }
                      },
                    ),
                  );
                },
                onDelete: () {
                  FinancialBottomSheetConfirmation.show(
                    context,
                    title: "Yakin RAB?",
                    description: "Data yang dihapus tidak dapat dikembalikan.",
                    actionButton: SantaraMainButton(
                      title: "Hapus",
                      onPressed: () {
                        bloc.removeRab();
                        Navigator.pop(context);
                      },
                    ),
                  );
                },
              )
            : FlatButton(
                color: Color(0xff6870E1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(Sizes.s5),
                ),
                onPressed: () => updateRab(context),
                child: Container(
                  height: Sizes.s45,
                  child: Center(
                    child: Text(
                      "Input RAB",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ),
        SizedBox(height: Sizes.s10),
        bloc.inputRab.state.hasError && !bloc.inputRab.state.isInitial
            ? Text(
                "${bloc.inputRab.state.error}",
                style: TextStyle(
                  color: Colors.red,
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w500,
                ),
              )
            : Container(),
      ],
    );
  }
}
