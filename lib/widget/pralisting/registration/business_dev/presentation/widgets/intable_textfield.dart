import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

import '../blocs/rab_input_bloc.dart';

class InTableTextField extends StatelessWidget {
  final TextFieldBloc textFieldBloc;
  final String hintText;
  final bool isEnabled;
  final TextInputType keyboardType;
  final List<TextInputFormatter> inputFormatters;

  const InTableTextField({
    Key key,
    this.textFieldBloc,
    this.hintText,
    this.isEnabled: true,
    this.keyboardType,
    this.inputFormatters,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldBlocBuilder(
      textFieldBloc: textFieldBloc,
      keyboardType: keyboardType,
      minLines: 1,
      maxLines: 5,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 12),
      isEnabled: isEnabled,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(fontSize: 12, color: Color(0xFFB8B8B8)),
        border: InputBorder.none,
      ),
    );
  }
}

class InTableTextFieldWithCheckBox extends StatefulWidget {
  final RabTableFieldBloc bloc;
  final TextFieldBloc textFieldBloc;
  final int index;
  final String hintText;
  final bool isEnabled;
  final List<int> numberToRemove;
  final TextInputType keyboardType;

  const InTableTextFieldWithCheckBox(
      {Key key,
      this.bloc,
      this.index,
      this.textFieldBloc,
      this.hintText,
      this.isEnabled: true,
      this.numberToRemove,
      this.keyboardType})
      : super(key: key);

  @override
  _InTableTextFieldWithCheckBoxState createState() =>
      _InTableTextFieldWithCheckBoxState();
}

class _InTableTextFieldWithCheckBoxState
    extends State<InTableTextFieldWithCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Row(children: [
      widget.bloc.isDeleteRow.state.value
          ? Checkbox(
              value: widget.numberToRemove.contains(widget.index),
              onChanged: (v) {
                setState(() {
                  if (v) {
                    widget.numberToRemove.add(widget.index);
                  } else {
                    widget.numberToRemove.remove(widget.index);
                  }
                });
              })
          : Container(),
      Flexible(
        child: TextFieldBlocBuilder(
          textFieldBloc: widget.textFieldBloc,
          keyboardType: widget.keyboardType,
          minLines: 1,
          maxLines: 5,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12),
          isEnabled: widget.isEnabled,
          decoration: InputDecoration(
              hintText: widget.hintText,
              hintStyle: TextStyle(fontSize: 12, color: Color(0xFFB8B8B8)),
              border: InputBorder.none),
        ),
      )
    ]);
  }
}
