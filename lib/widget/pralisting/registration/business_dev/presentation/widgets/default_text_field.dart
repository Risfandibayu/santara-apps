import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../utils/pralisting_input_decoration.dart';
import '../../../../../../utils/sizes.dart';

class DefaultTextField extends StatelessWidget {
  final TextFieldBloc<Object> textFieldBloc;
  final String labelText;
  final TextInputType keyboardType;

  const DefaultTextField(
      {Key key,
      @required this.textFieldBloc,
      @required this.labelText,
      @required this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldBlocBuilder(
      textFieldBloc: textFieldBloc,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        labelText: labelText,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: '',
        border: PralistingInputDecoration.outlineInputBorder,
        contentPadding: EdgeInsets.all(Sizes.s15),
        isDense: true,
        errorMaxLines: 5,
        hintStyle: PralistingInputDecoration.hintTextStyle(false),
        labelStyle: PralistingInputDecoration.labelTextStyle,
      ),
    );
  }
}
