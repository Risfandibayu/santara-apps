import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/CurrencyFormat.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';

import '../../../../../../utils/sizes.dart';
import '../blocs/rab_input_bloc.dart';
import 'intable_textfield.dart';

class TotalField extends StatelessWidget {
  final List<RabTableFieldBloc> rabTable;
  final String title;
  final TextFieldBloc subtotalPrice;

  TotalField({Key key, this.rabTable, this.title, this.subtotalPrice})
      : super(key: key);

  void onCountTotal() {
    num total = 0;
    for (var i = 0; i < rabTable.length; i++) {
      num subtotalPrice = 0;
      try {
        subtotalPrice = num.parse(
            rabTable[i].subtotalPrice.state.value.replaceAll(",", ""));
      } catch (_, __) {
        subtotalPrice = 0;
      }
      total += subtotalPrice;
    }
    subtotalPrice.updateValue(NumberFormatter.convertNumber(total));
  }

  @override
  Widget build(BuildContext context) {
    onCountTotal();
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s20, top: Sizes.s10),
      padding: EdgeInsets.only(right: Sizes.s20),
      height: 56,
      decoration: BoxDecoration(
          border: Border.all(
            width: 1.5,
            color: Colors.black,
          ),
          borderRadius: BorderRadius.circular(6)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              height: 56,
              width: MediaQuery.of(context).size.width / 3,
              decoration: BoxDecoration(
                  color: Color(0xFFEDEDED),
                  border: Border.all(
                    width: 1,
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.circular(6)),
              child: Center(child: Text(title))),
          Flexible(
            child: Theme(
              data: ThemeData(disabledColor: Colors.black),
              child: InTableTextField(
                isEnabled: false,
                textFieldBloc: subtotalPrice,
                hintText: '',
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyFormat(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
