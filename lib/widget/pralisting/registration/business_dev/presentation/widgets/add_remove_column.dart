import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

import '../../../../../../utils/sizes.dart';
import '../../../../../widget/components/market/field_pasar_sekunder.dart';
import '../blocs/rab_input_bloc.dart';

class AddRemoveColumn extends StatefulWidget {
  final RabInputBloc bloc;
  final int index;
  final RabTableFieldBloc rabTableFieldBloc;

  AddRemoveColumn({
    Key key,
    this.bloc,
    this.index,
    this.rabTableFieldBloc,
  }) : super(key: key);

  @override
  _AddRemoveColumnState createState() => _AddRemoveColumnState();
}

class _AddRemoveColumnState extends State<AddRemoveColumn> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ListFieldBloc<ComponentColumnFieldBloc>,
        ListFieldBlocState<ComponentColumnFieldBloc>>(
      bloc: widget.rabTableFieldBloc.componentColumn,
      builder: (context, state) {
        if (state.fieldBlocs.isNotEmpty) {
          return Padding(
            padding: EdgeInsets.only(bottom: Sizes.s25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SantaraMainButtonWithIcon(
                    key: Key('add_component_column'),
                    height: Sizes.s35,
                    width: Sizes.s130,
                    padding: EdgeInsets.symmetric(horizontal: Sizes.s8),
                    icon: Icon(Icons.add, color: Color(0xFFD23737)),
                    name: "Tambah Baris",
                    textColor: Color(0xFFD23737),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        border: Border.all(width: 1, color: Color(0xFFD23737))),
                    onTap: () => widget.bloc.addComponentColumn(widget.index)),
                SantaraMainButtonWithIcon(
                    key: Key('remove_component_column'),
                    height: Sizes.s35,
                    width: Sizes.s130,
                    padding: EdgeInsets.symmetric(horizontal: Sizes.s8),
                    icon: Icon(Icons.delete_outline, color: Colors.white),
                    name: "Hapus Baris",
                    textColor: Colors.white,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Color(0xFFFF4343)),
                    onTap: () {
                      setState(() => widget.rabTableFieldBloc.isDeleteRow
                          .updateValue(true));
                    }),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}
