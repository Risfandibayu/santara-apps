import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../core/widgets/pralisting_buttons/pralisting_info_button.dart';
import '../../../../../../utils/pralisting_input_decoration.dart';
import '../../../../../../utils/sizes.dart';
import '../blocs/business_dev_bloc.dart';

class ProjectDescField extends StatelessWidget {
  final BusinessDevBloc bloc;
  ProjectDescField({@required this.bloc});
  final String hint =
      "Alasan dan latar belakang pekerjaan project baru, Konsep project baru, produk yang dijual, potensi pasar project dan lokasi baru";
  final String title = "Deskripsi Project Baru";
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc.description,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: "$title",
          labelStyle: PralistingInputDecoration.labelTextStyle,
          hintText: hint,
          hintStyle: PralistingInputDecoration.hintTextStyle(false),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: PralistingInfoButton(
            title: title,
            message: hint,
          ),
        ),
        maxLines: 4,
      ),
    );
  }
}
