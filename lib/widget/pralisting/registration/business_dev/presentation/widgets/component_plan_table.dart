import 'package:flutter/material.dart';

import '../../../../../../utils/sizes.dart';
import '../../../../../widget/components/main/SantaraButtons.dart';
import '../blocs/rab_input_bloc.dart';
import 'add_remove_column.dart';
import 'default_text_field.dart';
import 'rab_input_table_field.dart';
import 'subtotal_field.dart';

class ComponentPlanTable extends StatefulWidget {
  final RabInputBloc bloc;
  final int componentPlanIndex;
  final RabTableFieldBloc rabTableFieldBloc;

  const ComponentPlanTable(
      {Key key, this.bloc, this.componentPlanIndex, this.rabTableFieldBloc})
      : super(key: key);

  @override
  _ComponentPlanTableState createState() => _ComponentPlanTableState();
}

class _ComponentPlanTableState extends State<ComponentPlanTable> {
  @override
  Widget build(BuildContext context) {
    final List<int> numberToRemove = [];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Komponen Rencana ${widget.componentPlanIndex + 1}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                widget.componentPlanIndex == 0
                    ? Container()
                    : GestureDetector(
                        onTap: () {
                          widget.bloc.removeRabTable(widget.componentPlanIndex);
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.delete,
                              size: 20,
                              color: Color(0xFFFF4343),
                            ),
                            Text(
                              "Hapus Komponen",
                              style: TextStyle(
                                fontSize: 12,
                                color: Color(0xFFFF4343),
                              ),
                            ),
                          ],
                        ),
                      )
              ],
            )),
        DefaultTextField(
            textFieldBloc: widget.rabTableFieldBloc.componentName,
            labelText: "Nama Komponen",
            keyboardType: TextInputType.text),
        RabInputTableField(
            bloc: widget.rabTableFieldBloc, numberToRemove: numberToRemove),
        SubTotalField(
            bloc: widget.bloc.rabTable.value[widget.componentPlanIndex]
                .componentColumn.state.fieldBlocs,
            subtotalPrice: widget.rabTableFieldBloc.subtotalPrice,
            title: "Sub Total"),
        widget.rabTableFieldBloc.isDeleteRow.state.value
            ? Container()
            : AddRemoveColumn(
                bloc: widget.bloc,
                index: widget.componentPlanIndex,
                rabTableFieldBloc: widget.rabTableFieldBloc),
        widget.rabTableFieldBloc.isDeleteRow.state.value
            ? Container(
                height: Sizes.s45,
                width: double.maxFinite,
                margin: EdgeInsets.only(bottom: Sizes.s20),
                child: SantaraMainButton(
                  key: Key('remove_row_button'),
                  color: Color(0xFFFF4343),
                  title: "Konfirmasi Hapus Baris",
                  onPressed: () {
                    widget.bloc.removeComponentColumn(
                        widget.componentPlanIndex, numberToRemove);
                    numberToRemove.clear();
                    setState(() => widget.rabTableFieldBloc.isDeleteRow
                        .updateValue(false));
                  },
                ),
              )
            : Container()
      ],
    );
  }
}
