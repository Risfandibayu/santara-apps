import 'package:flutter/material.dart';

class TableHeader extends StatelessWidget {
  final String title;
  final Widget child;

  const TableHeader(this.title, this.child);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            constraints: BoxConstraints(minHeight: 40),
            decoration: BoxDecoration(
              color: Color(0xFFE5E5E5),
            ),
            padding: EdgeInsets.all(8),
            child: Center(
                child: Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ))),
        Container(height: 1.5, color: Colors.black),
        Container(child: child),
      ],
    );
  }
}
