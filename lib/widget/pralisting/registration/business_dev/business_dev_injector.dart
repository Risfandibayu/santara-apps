import 'package:santaraapp/widget/pralisting/registration/business_dev/data/datasources/business_dev_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/repositories/business_dev_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/domain/repositories/business_dev_repository.dart';
import '../../../../injector_container.dart';
import 'domain/usecase/business_dev_usecases.dart';
import 'presentation/blocs/business_dev_bloc.dart';
import 'presentation/blocs/rab_input_bloc.dart';

void initBusinessDevInjector() {
  sl.registerFactory(
    () => RabInputBloc(
      getRabInput: sl(),
      updateRabInput: sl(),
    ),
  );

  sl.registerFactory(
    () => BusinessDevBloc(
      getBusinessDev: sl(),
      updateBusinessDev: sl(),
      deleteRab: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessDevRepository>(
    () => BusinessDevRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessDevRemoteDataSource>(
    () => BusinessDevRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetRabInput(sl()));
  sl.registerLazySingleton(() => UpdateRabInput(sl()));
  sl.registerLazySingleton(() => GetBusinessDev(sl()));
  sl.registerLazySingleton(() => UpdateBusinessDev(sl()));
  sl.registerLazySingleton(() => DeleteRab(sl()));
}
