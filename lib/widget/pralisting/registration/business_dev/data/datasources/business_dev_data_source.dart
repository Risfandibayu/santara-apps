import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/business_dev_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/rab_data_model.dart';
import 'package:meta/meta.dart';

abstract class BusinessDevRemoteDataSource {
  Future<RabDataModel> getInputRab({@required String uuid});
  Future<PralistingMetaModel> updateInputRab({
    @required String uuid,
    @required dynamic body,
  });
  Future<BusinessDevModel> getBusinessDev({@required String uuid});
  Future<PralistingMetaModel> updateBusinessDev({
    @required String uuid,
    @required dynamic body,
  });
  Future<PralistingMetaModel> deleteRab({
    @required String uuid,
  });
}

class BusinessDevRemoteDataSourceImpl implements BusinessDevRemoteDataSource {
  final RestClient client;
  BusinessDevRemoteDataSourceImpl({@required this.client});

  @override
  Future<RabDataModel> getInputRab({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step6-1/$uuid");
      if (result.statusCode == 200) {
        return RabDataModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateInputRab({
    String uuid,
    dynamic body,
  }) async {
    try {
      final result = await client.putExternalUrl(
        url: "$apiPralisting/step6-1/$uuid",
        body: body,
      );
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<BusinessDevModel> getBusinessDev({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step6/$uuid");
      if (result.statusCode == 200) {
        return BusinessDevModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateBusinessDev(
      {String uuid, dynamic body}) async {
    try {
      final result = await client.putExternalUrl(
        url: "$apiPralisting/step6/$uuid",
        body: body,
      );
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> deleteRab({String uuid}) async {
    try {
      final result = await client.deleteExternalUrl(
        url: "$apiPralisting/step6-1/$uuid",
      );
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
