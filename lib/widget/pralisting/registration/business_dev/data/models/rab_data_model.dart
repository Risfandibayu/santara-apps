// To parse this JSON data, do
//
//     final rabDataModel = rabDataModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

RabDataModel rabDataModelFromJson(String str) =>
    RabDataModel.fromJson(json.decode(str));

String rabDataModelToJson(RabDataModel data) => json.encode(data.toJson());

class RabDataModel {
  RabDataModel({
    this.plans,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  List<Plan> plans;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory RabDataModel.fromJson(Map<String, dynamic> json) => RabDataModel(
        plans: json["plans"] == null
            ? null
            : List<Plan>.from(json["plans"].map((x) => Plan.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "plans": plans == null
            ? null
            : List<dynamic>.from(plans.map((x) => x.toJson())),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class Plan {
  Plan({
    this.name,
    this.rows,
  });

  String name;
  List<Row> rows;

  factory Plan.fromJson(Map<String, dynamic> json) => Plan(
        name: json["name"] == null ? null : json["name"],
        rows: json["rows"] == null
            ? null
            : List<Row>.from(json["rows"].map((x) => Row.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "rows": rows == null
            ? null
            : List<dynamic>.from(rows.map((x) => x.toJson())),
      };
}

class Row {
  Row({
    this.desc,
    this.unit,
    this.price,
    this.amount,
  });

  String desc;
  String unit;
  int price;
  int amount;

  factory Row.fromJson(Map<String, dynamic> json) => Row(
        desc: json["desc"] == null ? null : json["desc"],
        unit: json["unit"] == null ? null : json["unit"],
        price: json["price"] == null ? null : json["price"],
        amount: json["amount"] == null ? null : json["amount"],
      );

  Map<String, dynamic> toJson() => {
        "desc": desc == null ? null : desc,
        "unit": unit == null ? null : unit,
        "price": price == null ? null : price,
        "amount": amount == null ? null : amount,
      };
}
