// To parse this JSON data, do
//
//     final businessDevModel = businessDevModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

import 'rab_data_model.dart';

BusinessDevModel businessDevModelFromJson(String str) =>
    BusinessDevModel.fromJson(json.decode(str));

String businessDevModelToJson(BusinessDevModel data) =>
    json.encode(data.toJson());

class BusinessDevModel {
  BusinessDevModel({
    this.newDesc,
    this.newLocation,
    this.newFund,
    this.newMinFund,
    this.newPeriod,
    this.plans,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  String newDesc;
  String newLocation;
  int newFund;
  int newMinFund;
  int newPeriod;
  List<Plan> plans;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory BusinessDevModel.fromJson(Map<String, dynamic> json) =>
      BusinessDevModel(
        newDesc: json["new_desc"] == null ? null : json["new_desc"],
        newLocation: json["new_location"] == null ? null : json["new_location"],
        newFund: json["new_fund"] == null ? null : json["new_fund"],
        newMinFund: json["new_min_fund"] == null ? null : json["new_min_fund"],
        newPeriod: json["new_period"] == null ? null : json["new_period"],
        plans: json["plans"] == null
            ? null
            : List<Plan>.from(json["plans"].map((x) => Plan.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "new_desc": newDesc == null ? null : newDesc,
        "new_location": newLocation == null ? null : newLocation,
        "new_fund": newFund == null ? null : newFund,
        "new_min_fund": newMinFund == null ? null : newMinFund,
        "new_period": newPeriod == null ? null : newPeriod,
        "plans": plans == null
            ? null
            : List<dynamic>.from(plans.map((x) => x.toJson())),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}
