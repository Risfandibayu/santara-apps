import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/datasources/business_dev_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/business_dev_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/rab_data_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/domain/repositories/business_dev_repository.dart';
import 'package:meta/meta.dart';

class BusinessDevRepositoryImpl implements BusinessDevRepository {
  final NetworkInfo networkInfo;
  final BusinessDevRemoteDataSource remoteDataSource;

  BusinessDevRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, RabDataModel>> getInputRab({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getInputRab(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateInputRab(
      {String uuid, dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateInputRab(
          uuid: uuid,
          body: body,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, BusinessDevModel>> getBusinessDev(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBusinessDev(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateBusinessDev(
      {String uuid, dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateBusinessDev(
          uuid: uuid,
          body: body,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> deleteRab({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.deleteRab(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
