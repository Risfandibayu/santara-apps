import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/business_dev_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/rab_data_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/domain/repositories/business_dev_repository.dart';

class GetRabInput implements UseCase<RabDataModel, String> {
  final BusinessDevRepository repository;
  GetRabInput(this.repository);

  @override
  Future<Either<Failure, RabDataModel>> call(String uuid) async {
    return await repository.getInputRab(uuid: uuid);
  }
}

class UpdateRabInput implements UseCase<PralistingMetaModel, dynamic> {
  final BusinessDevRepository repository;
  UpdateRabInput(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateInputRab(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class GetBusinessDev implements UseCase<BusinessDevModel, String> {
  final BusinessDevRepository repository;
  GetBusinessDev(this.repository);

  @override
  Future<Either<Failure, BusinessDevModel>> call(String uuid) async {
    return await repository.getBusinessDev(uuid: uuid);
  }
}

class UpdateBusinessDev implements UseCase<PralistingMetaModel, dynamic> {
  final BusinessDevRepository repository;
  UpdateBusinessDev(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateBusinessDev(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class DeleteRab implements UseCase<PralistingMetaModel, String> {
  final BusinessDevRepository repository;
  DeleteRab(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(String uuid) async {
    return await repository.deleteRab(uuid: uuid);
  }
}
