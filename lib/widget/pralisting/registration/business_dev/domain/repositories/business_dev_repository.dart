import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/business_dev_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/data/models/rab_data_model.dart';

abstract class BusinessDevRepository {
  Future<Either<Failure, PralistingMetaModel>> updateInputRab({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, RabDataModel>> getInputRab({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> updateBusinessDev({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, BusinessDevModel>> getBusinessDev({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> deleteRab({
    @required String uuid,
  });
}
