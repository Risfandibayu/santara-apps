import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/pages/business_info_1_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/pages/business_info_2_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/pages/business_prospective_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/financial_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/pages/financial_projections_page.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/pages/management_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/media_page.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/pages/share_info_page.dart';

class RegistrationStepsPage extends StatefulWidget {
  @override
  _RegistrationStepsPageState createState() => _RegistrationStepsPageState();
}

class _RegistrationStepsPageState extends State<RegistrationStepsPage> {
  Widget _statusTile({
    @required String title,
    @required bool isError,
    @required Function onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: Sizes.s20,
              right: Sizes.s20,
              top: Sizes.s5,
              bottom: Sizes.s5,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "$title",
                      style: TextStyle(
                        fontSize: FontSize.s16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    isError
                        ? Row(
                            children: [
                              Icon(
                                Icons.info,
                                color: Color(0xffFF4343),
                                size: Sizes.s20,
                              ),
                              SizedBox(width: Sizes.s5),
                              Text(
                                "Ditolak",
                                style: TextStyle(
                                  fontSize: FontSize.s14,
                                  color: Color(0xffFF4343),
                                ),
                              )
                            ],
                          )
                        : Container(),
                  ],
                ),
                isError
                    ? Text(
                        "Perbaiki >",
                        style: TextStyle(
                          fontSize: FontSize.s16,
                          color: Color(0xff218196),
                        ),
                      )
                    : Container(width: 1),
              ],
            ),
          ),
          Divider(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Pengajuan Bisnis",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
            Text(
              "Sop Ayam Pak Min",
              style: TextStyle(
                fontSize: FontSize.s20,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        elevation: 1,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _statusTile(
                title: "Informasi Calon Penerbit",
                isError: true,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BusinessProspectiveInfoPage(
                      uuid: "123",
                    ),
                  ),
                ),
              ),
              _statusTile(
                title: "Informasi Usaha (1)",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BusinessInfoOnePage(
                      uuid: "123",
                    ),
                  ),
                ),
              ),
              _statusTile(
                title: "Informasi Usaha (2)",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BusinsessInfoTwoPage(uuid: ""),
                  ),
                ),
              ),
              _statusTile(
                title: "Informasi Manajemen",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ManagementInfoPage(uuid: ""),
                  ),
                ),
              ),
              _statusTile(
                title: "Informasi Finansial",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FinancialInfoPage(uuid: ""),
                  ),
                ),
              ),
              _statusTile(
                title: "Pengembangan Usaha",
                isError: false,
                onTap: null,
              ),
              _statusTile(
                title: "Proyeksi Keuangan",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FinancialProjectionsPage(uuid: ""),
                  ),
                ),
              ),
              _statusTile(
                title: "Informasi Penawaran Saham",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ShareInfoPage(uuid: ""),
                  ),
                ),
              ),
              _statusTile(
                title: "Media",
                isError: false,
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MediaPage(uuid: ""),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
