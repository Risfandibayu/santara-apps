import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/models/business_info_1_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/repositories/business_info_1_repository.dart';

class GetListBusinessOptions
    implements UseCase<List<SearchData>, Map<String, dynamic>> {
  final BusinessInfoOneRepository repository;
  GetListBusinessOptions(this.repository);

  @override
  Future<Either<Failure, List<SearchData>>> call(
      Map<String, dynamic> params) async {
    return await repository.getListBusinessOptions(
      page: params['page'],
      limit: params['limit'],
      group: params['group'],
      showAll: params['show_all'],
    );
  }
}

class GetListBusinessRisks
    implements UseCase<List<SearchData>, Map<String, dynamic>> {
  final BusinessInfoOneRepository repository;
  GetListBusinessRisks(this.repository);

  @override
  Future<Either<Failure, List<SearchData>>> call(
      Map<String, dynamic> params) async {
    return await repository.getListBusinessRisks(
      page: params['page'],
      limit: params['limit'],
    );
  }
}

class UpdateBusinessInfoOne implements UseCase<String, Map<String, dynamic>> {
  final BusinessInfoOneRepository repository;
  UpdateBusinessInfoOne(this.repository);

  @override
  Future<Either<Failure, String>> call(Map<String, dynamic> params) async {
    return await repository.updateBusinessInfoOne(
      body: params['body'],
      uuid: params['uuid'],
    );
  }
}

class GetBusinessInfoOne implements UseCase<BusinessInfoOneModel, String> {
  final BusinessInfoOneRepository repository;
  GetBusinessInfoOne(this.repository);

  @override
  Future<Either<Failure, BusinessInfoOneModel>> call(String uuid) async {
    return await repository.getBusinessInfoOne(uuid: uuid);
  }
}
