import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/models/business_info_1_model.dart';

abstract class BusinessInfoOneRepository {
  // Future<Either<Failure, String>> postBusinessInfoOne(dynamic body);
  Future<Either<Failure, String>> updateBusinessInfoOne({
    @required dynamic body,
    @required String uuid,
  });

  Future<Either<Failure, List<SearchData>>> getListBusinessOptions({
    @required int page,
    @required int limit,
    @required String group,
    @required String showAll,
  });

  Future<Either<Failure, List<SearchData>>> getListBusinessRisks({
    @required int page,
    @required int limit,
  });

  Future<Either<Failure, BusinessInfoOneModel>> getBusinessInfoOne({
    @required String uuid,
  });
}
