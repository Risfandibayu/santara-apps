import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/models/business_info_1_model.dart';

abstract class BusinessInfoOneRemoteDataSource {
  Future<List<SearchData>> getListBusinessOptions({
    @required int page,
    @required int limit,
    @required String group,
    @required String showAll,
  });

  Future<List<SearchData>> getListBusinessRisks({
    @required int page,
    @required int limit,
  });

  Future<String> updateBusinessInfoOne({
    @required dynamic body,
    @required String uuid,
  });

  Future<BusinessInfoOneModel> getBusinessInfoOne({@required String uuid});
}

class BusinessInfoOneRemoteDataSourceImpl
    implements BusinessInfoOneRemoteDataSource {
  final RestClient client;
  BusinessInfoOneRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<SearchData>> getListBusinessOptions(
      {int page, int limit, String group, String showAll}) async {
    try {
      final result = await client.getExternalUrl(
          url:
              "$apiPralisting/list-business-options?page=$page&limit=$limit&group=$group&show-all=$showAll");
      if (result.statusCode == 200) {
        List<SearchData> data = List<SearchData>();
        if (result.data['data'] != null && result.data['data'].length > 0) {
          result.data['data'].forEach((val) {
            data.add(SearchData.fromJson(val));
          });
        }
        return data;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<List<SearchData>> getListBusinessRisks({int page, int limit}) async {
    try {
      final result = await client.getExternalUrl(
          url: "$apiPralisting/list-business-risks?page=$page&limit=$limit");
      if (result.statusCode == 200) {
        List<SearchData> data = List<SearchData>();
        result.data['data'].forEach((val) {
          data.add(SearchData.fromJson(val));
        });
        return data;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<String> updateBusinessInfoOne({dynamic body, String uuid}) async {
    try {
      final result = await client.putExternalUrl(
        url: "$apiPralisting/step2/$uuid",
        body: body,
      );
      if (result.statusCode == 200) {
        return result.data["meta"]["message"];
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<BusinessInfoOneModel> getBusinessInfoOne({String uuid}) async {
    try {
      final result = await client.getExternalUrl(
        url: "$apiPralisting/step2/$uuid",
      );
      if (result.statusCode == 200) {
        return BusinessInfoOneModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
