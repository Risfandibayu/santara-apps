import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/datasources/business_info_1_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/models/business_info_1_model.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/repositories/business_info_1_repository.dart';
import 'package:meta/meta.dart';

class BusinessInfoOneRepositoryImpl implements BusinessInfoOneRepository {
  final NetworkInfo networkInfo;
  final BusinessInfoOneRemoteDataSource remoteDataSource;
  BusinessInfoOneRepositoryImpl(
      {@required this.networkInfo, @required this.remoteDataSource});
  @override
  Future<Either<Failure, List<SearchData>>> getListBusinessOptions(
      {int page, int limit, String group, String showAll}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getListBusinessOptions(
          page: page,
          limit: limit,
          group: group,
          showAll: showAll,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, List<SearchData>>> getListBusinessRisks(
      {int page, int limit}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getListBusinessRisks(
          page: page,
          limit: limit,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, String>> updateBusinessInfoOne(
      {dynamic body, String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateBusinessInfoOne(
          body: body,
          uuid: uuid,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, BusinessInfoOneModel>> getBusinessInfoOne({
    String uuid,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBusinessInfoOne(
          uuid: uuid,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
