// To parse this JSON data, do
//
//     final businessInfoOneModel = businessInfoOneModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

BusinessInfoOneModel businessInfoOneModelFromJson(String str) =>
    BusinessInfoOneModel.fromJson(json.decode(str));

String businessInfoOneModelToJson(BusinessInfoOneModel data) =>
    json.encode(data.toJson());

class BusinessInfoOneModel {
  BusinessInfoOneModel({
    this.businessHistory,
    this.businessDescription,
    this.advantage,
    this.expansionPlan,
    this.highlightIndustry,
    this.businessRisks,
    this.emitensSuppliers,
    this.emitenBuyers,
    this.emitenCoopContracts,
    this.businessOptionsAnswer,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  String businessHistory;
  String businessDescription;
  String advantage;
  String expansionPlan;
  String highlightIndustry;
  List<BusinessRisk> businessRisks;
  List<Emiten> emitensSuppliers;
  List<Emiten> emitenBuyers;
  List<EmitenCoopContract> emitenCoopContracts;
  List<BusinessOptionsAnswer> businessOptionsAnswer;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory BusinessInfoOneModel.fromJson(Map<String, dynamic> json) =>
      BusinessInfoOneModel(
        businessHistory:
            json["business_history"] == null ? null : json["business_history"],
        businessDescription: json["business_description"] == null
            ? null
            : json["business_description"],
        advantage: json["advantage"] == null ? null : json["advantage"],
        expansionPlan:
            json["expansion_plan"] == null ? null : json["expansion_plan"],
        highlightIndustry: json["highlight_industry"] == null
            ? null
            : json["highlight_industry"],
        businessRisks: json["business_risks"] == null
            ? null
            : List<BusinessRisk>.from(
                json["business_risks"].map((x) => BusinessRisk.fromJson(x))),
        emitensSuppliers: json["emitens_suppliers"] == null
            ? null
            : List<Emiten>.from(
                json["emitens_suppliers"].map((x) => Emiten.fromJson(x))),
        emitenBuyers: json["emiten_buyers"] == null
            ? null
            : List<Emiten>.from(
                json["emiten_buyers"].map((x) => Emiten.fromJson(x))),
        emitenCoopContracts: json["emiten_coop_contracts"] == null
            ? null
            : List<EmitenCoopContract>.from(json["emiten_coop_contracts"]
                .map((x) => EmitenCoopContract.fromJson(x))),
        businessOptionsAnswer: json["business_options_answer"] == null
            ? null
            : List<BusinessOptionsAnswer>.from(json["business_options_answer"]
                .map((x) => BusinessOptionsAnswer.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "business_history": businessHistory == null ? null : businessHistory,
        "business_description":
            businessDescription == null ? null : businessDescription,
        "advantage": advantage == null ? null : advantage,
        "expansion_plan": expansionPlan == null ? null : expansionPlan,
        "highlight_industry":
            highlightIndustry == null ? null : highlightIndustry,
        "business_risks": businessRisks == null
            ? null
            : List<dynamic>.from(businessRisks.map((x) => x.toJson())),
        "emitens_suppliers": emitensSuppliers == null
            ? null
            : List<dynamic>.from(emitensSuppliers.map((x) => x.toJson())),
        "emiten_buyers": emitenBuyers == null
            ? null
            : List<dynamic>.from(emitenBuyers.map((x) => x.toJson())),
        "emiten_coop_contracts": emitenCoopContracts == null
            ? null
            : List<dynamic>.from(emitenCoopContracts.map((x) => x.toJson())),
        "business_options_answer": businessOptionsAnswer == null
            ? null
            : List<dynamic>.from(businessOptionsAnswer.map((x) => x.toJson())),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class BusinessOptionsAnswer {
  BusinessOptionsAnswer({
    this.businessAspectOptionsId,
    this.text,
  });

  int businessAspectOptionsId;
  String text;

  factory BusinessOptionsAnswer.fromJson(Map<String, dynamic> json) =>
      BusinessOptionsAnswer(
        businessAspectOptionsId: json["business_aspect_options_id"] == null
            ? null
            : json["business_aspect_options_id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "business_aspect_options_id":
            businessAspectOptionsId == null ? null : businessAspectOptionsId,
        "text": text == null ? null : text,
      };
}

class BusinessRisk {
  BusinessRisk({
    this.businessRisksId,
    this.name,
    this.desc,
  });

  int businessRisksId;
  String name;
  String desc;

  factory BusinessRisk.fromJson(Map<String, dynamic> json) => BusinessRisk(
        businessRisksId: json["business_risks_id"] == null
            ? null
            : json["business_risks_id"],
        name: json["name"] == null ? null : json["name"],
        desc: json["desc"] == null ? null : json["desc"],
      );

  Map<String, dynamic> toJson() => {
        "business_risks_id": businessRisksId == null ? null : businessRisksId,
        "name": name == null ? null : name,
        "desc": desc == null ? null : desc,
      };
}

class Emiten {
  Emiten({
    this.name,
    this.phone,
    this.address,
  });

  String name;
  String phone;
  String address;

  factory Emiten.fromJson(Map<String, dynamic> json) => Emiten(
        name: json["name"] == null ? null : json["name"],
        phone: json["phone"] == null ? null : json["phone"],
        address: json["address"] == null ? null : json["address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "phone": phone == null ? null : phone,
        "address": address == null ? null : address,
      };
}

class EmitenCoopContract {
  EmitenCoopContract({
    this.filename,
    this.url,
  });

  String filename;
  String url;

  factory EmitenCoopContract.fromJson(Map<String, dynamic> json) =>
      EmitenCoopContract(
        filename: json["filename"] == null ? null : json["filename"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "filename": filename == null ? null : filename,
        "url": url == null ? null : url,
      };
}
