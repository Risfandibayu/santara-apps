import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/datasources/business_info_1_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/repositories/business_info_1_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/repositories/business_info_1_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';
import '../../../../injector_container.dart';
import 'domain/usecases/business_info_1_usecases.dart';

void initBusinessInfoOneInjector() {
  sl.registerFactory(
    () => BusinessInfoOneBloc(
      getListBusinessOptions: sl(),
      getListBusinessRisks: sl(),
      updateBusinessInfoOne: sl(),
      getBusinessInfoOne: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessInfoOneRepository>(
    () => BusinessInfoOneRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<BusinessInfoOneRemoteDataSource>(
    () => BusinessInfoOneRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetListBusinessOptions(sl()));
  sl.registerLazySingleton(() => GetListBusinessRisks(sl()));
  sl.registerLazySingleton(() => UpdateBusinessInfoOne(sl()));
  sl.registerLazySingleton(() => GetBusinessInfoOne(sl()));
}
