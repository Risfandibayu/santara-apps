import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessContractsField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessContractsField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
      child: BlocBuilder<TextFieldBloc, TextFieldBlocState>(
        bloc: bloc.employmentContract,
        builder: (context, state) {
          // print(">> Bloc State : ${bloc.state}");
          // print(">> State Extra Data : ${state.extraData}");
          return state.extraData != null
              ? FileUploadSubmission(
                  label: "Kontrak Kerja (opsional)",
                  annotation: Text(
                    "*Unggah dokumen jika usaha memiliki kontrak kerja dengan pihak lain\n*Jenis file (PNG, JPG, PDF), Maksimal 3 file",
                    style: TextStyle(
                      fontSize: FontSize.s10,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff707070),
                    ),
                  ),
                  hintText: "",
                  allowedTypes: ['pdf', 'jpg', 'png'],
                  fieldBloc: bloc.employmentContract,
                  maxFiles: 3,
                  minFiles: 1,
                  name: 'image',
                  path: '$apiFileUpload/upload',
                  location: "pralisting/business_documents/",
                  type: "pdf",
                )
              : Container();
        },
      ),
    );
  }
}
