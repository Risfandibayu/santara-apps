import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessGovernmentSupportField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessGovernmentSupportField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.businessGovernmentSupport.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.businessGovernmentSupport.state.items != null &&
                    bloc.businessGovernmentSupport.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.businessGovernmentSupport.state.items,
                      selected: bloc.businessGovernmentSupport.value,
                      showSearch: false,
                    );
                    bloc.businessGovernmentSupport.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Dukungan Pemerintah",
            hintText: "Tingkat dukungan pemerintah terhadap bisnis Anda?",
            state: fieldState,
            value: bloc.businessGovernmentSupport.value?.name,
            errorText: !bloc.businessGovernmentSupport.state.isInitial
                ? bloc.businessGovernmentSupport.state.hasError
                    ? bloc.businessGovernmentSupport.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
