import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

import 'business_risk_card.dart';

class BusinessRiskField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessRiskField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 1,
            color: Colors.grey[300],
            width: double.maxFinite,
            margin: EdgeInsets.only(bottom: Sizes.s20),
          ),
          Row(
            children: [
              Text(
                "Risiko-Risiko Usaha Dan Mitigasinya",
                style: TextStyle(
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w600,
                ),
              ),
              PralistingInfoButton(
                title: "Risiko-Risiko Usaha Dan Mitigasinya",
                message:
                    "Menjelaskan risiko usaha dan mitigasinya, seperti : Risiko Kelangsungan Usaha, Risiko Persaingan Usaha, Risiko Ekspansi Usaha, dll.",
              )
              // InkWell(
              //   onTap: () {},
              //   child: Container(
              //     width: Sizes.s30,
              //     height: Sizes.s30,
              //     child: Icon(
              //       Icons.info,
              //       size: FontSize.s20,
              //       color: Color(0xffdddddd),
              //     ),
              //   ),
              // ),
            ],
          ),
          SizedBox(height: Sizes.s10),
          BlocBuilder<ListFieldBloc<BusinessRiskFieldBloc>,
              ListFieldBlocState<BusinessRiskFieldBloc>>(
            bloc: bloc.businessRisks,
            builder: (context, state) {
              if (state.fieldBlocs.isNotEmpty) {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: state.fieldBlocs.length,
                  itemBuilder: (context, i) {
                    return BusinessRiskCard(
                      businessRiskIndex: i,
                      businessRiskField: state.fieldBlocs[i],
                      onRemoveBusinessRisk: () => bloc.removeBusinessRisk(i),
                      businessRiskList: bloc.risks,
                    );
                  },
                );
              }
              return Container();
            },
          ),
          bloc.businessRisks.state.fieldBlocs.isNotEmpty &&
                  bloc.businessRisks.state.fieldBlocs.length < 8
              ? FlatButton.icon(
                  padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                  onPressed: () => bloc.addBusinessRisk(),
                  icon: Text(
                    "Tambah Risiko Usaha",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff218196),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  label: Icon(
                    Icons.add_circle,
                    size: FontSize.s20,
                    color: Color(0xff218196),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
