import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/text/error_text.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessPartnerCard extends StatelessWidget {
  final int index;
  final BusinessPartnerFieldBloc fieldBloc;
  final VoidCallback onRemove;
  final String fieldTitle;
  final bool showDelete;

  const BusinessPartnerCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
    @required this.fieldTitle,
    @required this.showDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              '$fieldTitle #${index + 1}',
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
              ),
            ),
            index > 0
                ? showDelete
                    ? FlatButton.icon(
                        padding: EdgeInsets.zero,
                        onPressed: showDelete ? onRemove : null,
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: FontSize.s20,
                        ),
                        label: Text(
                          "Hapus",
                          style: TextStyle(
                            color: Colors.red,
                            decoration: TextDecoration.underline,
                            fontSize: FontSize.s12,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    : Container()
                : Container(),
          ],
        ),
        showDelete
            ? Container()
            : index > 0
                ? SizedBox(height: Sizes.s15)
                : Container(),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        // SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.fullName,
          decoration: InputDecoration(
            labelText: 'Nama Lengkap',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'Nama Lengkap',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(50),
          ],
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.phone,
          decoration: InputDecoration(
            labelText: 'No HP',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'No HP',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          keyboardType: TextInputType.number,
          inputFormatters: [
            LengthLimitingTextInputFormatter(13),
            FilteringTextInputFormatter.digitsOnly,
          ],
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.address,
          decoration: InputDecoration(
            labelText: 'Alamat',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'Alamat',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(255),
          ],
        ),
        ErrorText(message: fieldBloc.error)
      ],
    );
  }
}
