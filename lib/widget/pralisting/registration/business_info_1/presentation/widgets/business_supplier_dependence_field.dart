import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessSupplierDependenceField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessSupplierDependenceField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.businessSupplierDependence.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.businessSupplierDependence.state.items != null &&
                    bloc.businessSupplierDependence.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.businessSupplierDependence.state.items,
                      selected: bloc.businessSupplierDependence.value,
                      showSearch: false,
                    );
                    bloc.businessSupplierDependence.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Ketergantungan Pada Supplier",
            hintText: "Tingkat ketergantungan terhadap supplier?",
            state: fieldState,
            value: bloc.businessSupplierDependence.value?.name,
            errorText: !bloc.businessSupplierDependence.state.isInitial
                ? bloc.businessSupplierDependence.state.hasError
                    ? bloc.businessSupplierDependence.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
