import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessAspectField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessAspectField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            "Aspek Lingkungan Usaha",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Text(
            "Pilih salah satu pernyataan yang paling sesuai dengan bisnis Anda",
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        SizedBox(height: Sizes.s20),
        BlocBuilder(
          bloc: bloc,
          builder: (context, state) {
            var aspectState = bloc.businessAspect.state.extraData;
            return SantaraDropDownField(
              onTap: bloc.businessAspect.state.items != null &&
                      bloc.businessAspect.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "",
                        hintText: "",
                        lists: bloc.businessAspect.state.items,
                        selected: bloc.businessAspect.value,
                        showSearch: false,
                      );
                      bloc.businessAspect.updateValue(result);
                    }
                  : null,
              onReload: () {},
              labelText: "Aspek Prospek Usaha",
              hintText: "Tingkat Pertumbuhan Prospek Usaha Anda?",
              state: aspectState,
              value: bloc.businessAspect.value?.name,
              errorText: !bloc.businessAspect.state.isInitial
                  ? bloc.businessAspect.state.hasError
                      ? bloc.businessAspect.state.error
                      : null
                  : null,
            );
          },
        ),
      ],
    );
  }
}
