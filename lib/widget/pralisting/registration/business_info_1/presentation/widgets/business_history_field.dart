import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessHistoryField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessHistoryField({@required this.bloc});

  final String label = "Sejarah Usaha & Latar Belakang Usaha";
  final String description =
      "Menjelaskan sejarah usaha, kapan usaha berdiri, latar belakang dan alasan membangun usaha tersebut";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldBlocBuilder(
            textFieldBloc: bloc.businessHistory,
            decoration: InputDecoration(
              border: PralistingInputDecoration.outlineInputBorder,
              contentPadding: EdgeInsets.all(Sizes.s15),
              isDense: true,
              errorMaxLines: 5,
              labelText: label,
              hintText: "($description)".replaceAll(".", ""),
              hintStyle: PralistingInputDecoration.hintTextStyle(false),
              labelStyle: PralistingInputDecoration.labelTextStyle,
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: PralistingInfoButton(
                title: label,
                message: description,
              ),
            ),
            maxLines: 4,
          ),
          SizedBox(height: Sizes.s5),
        ],
      ),
    );
  }
}
