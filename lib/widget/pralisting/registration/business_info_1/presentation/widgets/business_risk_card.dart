import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/text/error_text.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessRiskCard extends StatelessWidget {
  final int businessRiskIndex;
  final BusinessRiskFieldBloc businessRiskField;
  final VoidCallback onRemoveBusinessRisk;
  final List<SearchData> businessRiskList;

  const BusinessRiskCard({
    Key key,
    @required this.businessRiskIndex,
    @required this.businessRiskField,
    @required this.onRemoveBusinessRisk,
    @required this.businessRiskList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // businessRiskField.businessRiskType.updateItems(businessRiskList);
    return Column(
      children: <Widget>[
        businessRiskIndex > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Risiko #${businessRiskIndex + 1}',
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w700,
              ),
            ),
            businessRiskIndex > 0
                ? FlatButton.icon(
                    padding: EdgeInsets.zero,
                    onPressed: onRemoveBusinessRisk,
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: FontSize.s20,
                    ),
                    label: Text(
                      "Hapus",
                      style: TextStyle(
                        color: Colors.red,
                        decoration: TextDecoration.underline,
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        businessRiskIndex < 1 ? SizedBox(height: Sizes.s15) : Container(),
        DropdownFieldBlocBuilder(
          selectFieldBloc: businessRiskField.businessRiskType,
          decoration: InputDecoration(
            labelText: 'Jenis Risiko Usaha',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
          itemBuilder: (context, value) => value?.name ?? '',
          showEmptyItem: false,
          animateWhenCanShow: false,
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: businessRiskField.description,
          maxLines: 4,
          decoration: InputDecoration(
            labelText: 'Deskripsi Risiko dan Mitigasinya',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText:
                '(Menjelaskan risiko usaha dan mitigasinya, seperti : Risiko Kelangsungan Usaha, Risiko Persaingan Usaha, Risiko Ekspansi Usaha dll)',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        ErrorText(message: businessRiskField.error)
      ],
    );
  }
}
