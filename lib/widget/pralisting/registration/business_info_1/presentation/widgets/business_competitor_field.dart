import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessCompetitorField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessCompetitorField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.businessCompetitor.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.businessCompetitor.state.items != null &&
                    bloc.businessCompetitor.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.businessCompetitor.state.items,
                      selected: bloc.businessCompetitor.value,
                      showSearch: false,
                    );
                    bloc.businessCompetitor.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Perusahaan Pesaing",
            hintText: "Adakah perusahaan pesaing yang sejenis?",
            state: fieldState,
            value: bloc.businessCompetitor.value?.name,
            errorText: !bloc.businessCompetitor.state.isInitial
                ? bloc.businessCompetitor.state.hasError
                    ? bloc.businessCompetitor.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
