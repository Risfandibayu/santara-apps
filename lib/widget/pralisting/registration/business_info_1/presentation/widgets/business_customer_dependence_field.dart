import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessCustomerDependenceField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessCustomerDependenceField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.businessCustomerDependence.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.businessCustomerDependence.state.items != null &&
                    bloc.businessCustomerDependence.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.businessCustomerDependence.state.items,
                      selected: bloc.businessCustomerDependence.value,
                      showSearch: false,
                    );
                    bloc.businessCustomerDependence.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Ketergantungan Pada Pelanggan",
            hintText: "Apakah bisnis Anda sudah memiliki pelanggan?",
            state: fieldState,
            value: bloc.businessCustomerDependence.value?.name,
            errorText: !bloc.businessCustomerDependence.state.isInitial
                ? bloc.businessCustomerDependence.state.hasError
                    ? bloc.businessCustomerDependence.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
