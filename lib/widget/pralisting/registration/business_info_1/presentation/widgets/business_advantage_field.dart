import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessAdvantageField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessAdvantageField({@required this.bloc});

  final String label = "Kelebihan / Competitive Advantage Usaha";
  final String description =
      "Menjelaskan apa saja keunggulan usaha/bisnis yang dijalani dibandingkan dengan kompetitor atau bisnis lain.";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc.advantage,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: label,
          labelStyle: PralistingInputDecoration.labelTextStyle,
          hintText: "($description)".replaceAll(".", ""),
          hintStyle: PralistingInputDecoration.hintTextStyle(false),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: PralistingInfoButton(
            title: label,
            message: description,
          ),
        ),
        maxLines: 4,
      ),
    );
  }
}
