import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessSocialPoliticField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessSocialPoliticField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var businessSocialPoliticState =
              bloc.businessSocialPolitic.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.businessSocialPolitic.state.items != null &&
                    bloc.businessSocialPolitic.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.businessSocialPolitic.state.items,
                      selected: bloc.businessSocialPolitic.value,
                      showSearch: false,
                    );
                    bloc.businessSocialPolitic.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Pengaruh Kondisi Sosial Politik",
            hintText: "Pengaruh kondisi sosial dan politik terhadap bisnis Anda?",
            state: businessSocialPoliticState,
            value: bloc.businessSocialPolitic.value?.name,
            errorText: !bloc.businessSocialPolitic.state.isInitial
                ? bloc.businessSocialPolitic.state.hasError
                    ? bloc.businessSocialPolitic.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
