import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

class BusinessSubstitutionField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessSubstitutionField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: Sizes.s20),
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            "Profil Usaha",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Text(
            "Pilih salah satu pernyataan yang paling sesuai dengan bisnis Anda",
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        SizedBox(height: Sizes.s20),
        BlocBuilder(
          bloc: bloc,
          builder: (context, state) {
            var fieldState = bloc.businessSubstitution.state.extraData;
            return SantaraDropDownField(
              onTap: bloc.businessSubstitution.state.items != null &&
                      bloc.businessSubstitution.state.items.length > 0
                  ? () async {
                      final result = await PralistingHelper.showModal(
                        context,
                        title: "",
                        hintText: "",
                        lists: bloc.businessSubstitution.state.items,
                        selected: bloc.businessSubstitution.value,
                        showSearch: false,
                      );
                      bloc.businessSubstitution.updateValue(result);
                    }
                  : null,
              onReload: () {},
              labelText: "Produk / Jasa Subtitusi",
              hintText: "Apakah produk/jasa Anda memiliki subtitusi?",
              state: fieldState,
              value: bloc.businessSubstitution.value?.name,
              errorText: !bloc.businessSubstitution.state.isInitial
                  ? bloc.businessSubstitution.state.hasError
                      ? bloc.businessSubstitution.state.error
                      : null
                  : null,
            );
          },
        ),
      ],
    );
  }
}
