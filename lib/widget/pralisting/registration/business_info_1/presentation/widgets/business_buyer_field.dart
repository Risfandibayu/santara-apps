import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';

import 'business_partner_card.dart';

class BusinessBuyerField extends StatelessWidget {
  final BusinessInfoOneBloc bloc;
  BusinessBuyerField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 1,
            color: Colors.grey[300],
            width: double.maxFinite,
            margin: EdgeInsets.only(bottom: Sizes.s20),
          ),
          Text(
            "3 Buyer Terbesar (opsional)",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Nunito',
                fontSize: FontSize.s12,
              ),
              children: <TextSpan>[
                TextSpan(text: "Note : "),
                TextSpan(
                  text:
                      "Data yang di submit hanya untuk kepentingan review dan trade checking.",
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: Sizes.s20),
          BlocBuilder<ListFieldBloc<BusinessPartnerFieldBloc>,
              ListFieldBlocState<BusinessPartnerFieldBloc>>(
            bloc: bloc.businessBuyer,
            builder: (context, state) {
              if (state.fieldBlocs.isNotEmpty) {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: state.fieldBlocs.length,
                  itemBuilder: (context, i) {
                    return BusinessPartnerCard(
                      fieldTitle: "Buyer",
                      index: i,
                      fieldBloc: state.fieldBlocs[i],
                      onRemove: () => bloc.removeBusinessBuyer(i),
                      showDelete: true,
                    );
                  },
                );
              }
              return Container();
            },
          ),
          bloc.businessBuyer.state.fieldBlocs.length >= 3
              ? Container()
              : FlatButton.icon(
                  padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                  onPressed: () => bloc.addBusinessBuyer(),
                  icon: Text(
                    "Tambah Buyer",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff218196),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  label: Icon(
                    Icons.add_circle,
                    size: FontSize.s20,
                    color: Color(0xff218196),
                  ),
                ),
        ],
      ),
    );
  }
}
