import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/file_data_model.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/usecases/business_info_1_usecases.dart';

class BusinessInfoOneBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetListBusinessOptions getListBusinessOptions;
  final GetListBusinessRisks getListBusinessRisks;
  final UpdateBusinessInfoOne updateBusinessInfoOne;
  final GetBusinessInfoOne getBusinessInfoOne;

  List<SearchData> risks = [];
  List<FileUploadModel> dummies = [];

  final businessHistory = TextFieldBloc(
    name: 'business_history',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.max5000,
    ],
  );

  final businessDescription = TextFieldBloc(
    name: 'business_description',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.max5000,
    ],
  );

  final advantage = TextFieldBloc(
    name: 'advantage',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.max5000,
    ],
  );

  final expansionPlan = TextFieldBloc(
    name: 'expansion_plan',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.max5000,
    ],
  );

  final highlightIndustry = TextFieldBloc(
    name: 'highlight_industry',
    validators: [
      PralistingValidationHelper.required,
      PralistingValidationHelper.max6000,
    ],
  );

  final businessRisks = ListFieldBloc<BusinessRiskFieldBloc>(
    name: 'business_risks',
  );

  final businessSupplier =
      ListFieldBloc<BusinessPartnerFieldBloc>(name: 'emitens_suppliers');

  final businessBuyer =
      ListFieldBloc<BusinessPartnerFieldBloc>(name: 'emiten_buyers');

  // TODO : File Upload
  final employmentContract = TextFieldBloc(
    name: 'emiten_coop_contracts',
    validators: [],
    // extraData: dummies,
  );

  final businessAspect = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_aspect',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessSocialPolitic = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_social_politic',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessGovernmentSupport = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_government_support',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessSubstitution = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_substitution',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessCustomerDependence = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_customer_dependence',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessSupplierDependence = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_supplier_dependence',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final businessCompetitor = SelectFieldBloc<SearchData, dynamic>(
    name: 'business_competitor',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  BusinessInfoOneBloc({
    @required this.getListBusinessOptions,
    @required this.getListBusinessRisks,
    @required this.updateBusinessInfoOne,
    @required this.getBusinessInfoOne,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        businessHistory,
        businessDescription,
        advantage,
        expansionPlan,
        highlightIndustry,
        businessRisks,
        businessSupplier,
        businessBuyer,
        employmentContract,
        businessAspect,
        businessSocialPolitic,
        businessGovernmentSupport,
        businessSubstitution,
        businessCustomerDependence,
        businessSupplierDependence,
        businessCompetitor,
        //
      ],
    );
  }

  void addBusinessRisk() {
    businessRisks.addFieldBloc(
      BusinessRiskFieldBloc(
        name: 'businessRisk',
        description: TextFieldBloc(
          name: 'description',
          validators: [
            PralistingValidationHelper.required,
            PralistingValidationHelper.max5000
          ],
        ),
        businessRiskType: SelectFieldBloc(
          name: 'business_risk_type',
          items: risks,
          validators: [PralistingValidationHelper.required],
        ),
      ),
    );
  }

  void removeBusinessRisk(int index) {
    try {
      var currentSelectedItem =
          businessRisks.state?.fieldBlocs[index]?.businessRiskType?.value;
      if (currentSelectedItem != null) {
        risks.add(currentSelectedItem);
      }
      updateItems(index, risks);
      businessRisks.removeFieldBlocAt(index);
    } catch (e, stack) {
      santaraLog(e, stack);
      businessRisks.removeFieldBlocAt(index);
    }
  }

  void addBusinessSupplier() {
    businessSupplier.addFieldBloc(
      BusinessPartnerFieldBloc(
        name: 'emitens_suppliers',
        fullName: TextFieldBloc(
          name: 'name',
          validators: [
            PralistingValidationHelper.required,
            PralistingValidationHelper.fullName
          ],
        ),
        address: TextFieldBloc(
          name: 'address',
          validators: [PralistingValidationHelper.required],
        ),
        phone: TextFieldBloc(
          name: 'phone',
          validators: [PralistingValidationHelper.required],
        ),
      ),
    );
  }

  void removeBusinessSupplier(int index) {
    businessSupplier.removeFieldBlocAt(index);
  }

  void addBusinessBuyer() {
    businessBuyer.addFieldBloc(
      BusinessPartnerFieldBloc(
        name: 'emiten_buyers',
        fullName: TextFieldBloc(
          name: 'name',
          validators: [
            PralistingValidationHelper.fullName,
          ],
        ),
        address: TextFieldBloc(name: 'address'),
        phone: TextFieldBloc(name: 'phone'),
      ),
    );
  }

  void removeBusinessBuyer(int index) {
    businessBuyer.removeFieldBlocAt(index);
  }

  @override
  Future<void> close() {
    return super.close();
  }

  getBusinessRisks() async {
    try {
      final params = {
        'page': 1,
        'limit': 100,
      };
      final result = await getListBusinessRisks(params);

      if (result.isRight()) {
        final listRisks = result.getOrElse(null);
        risks.addAll(listRisks);
      } else {
        print('error get risks');
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  updateSelectField({@required SelectFieldBloc bloc, @required int id}) {
    var item = bloc.state.items.where((i) => i.id == id.toString()).first;
    bloc.updateValue(item);
  }

  initFields() {
    employmentContract.updateExtraData(dummies);
    addBusinessRisk();
    for (var i = 0; i < 3; i++) {
      addBusinessSupplier();
    }
    addBusinessBuyer();
  }

  getBusinessInfoOneData(String uuid) async {
    try {
      await getProspects();
      await getSocpols();
      await getGovs();
      await getSubs();
      await getBuyerDeps();
      await getSupplierDeps();
      await getCompetitors();
      final result = await getBusinessInfoOne("$uuid");

      if (result.isRight()) {
        final data = result.getOrElse(null);
        if (data != null && !data.isClean) {
          businessHistory.updateValue(data.businessHistory);
          businessDescription.updateValue(data.businessDescription);
          advantage.updateValue(data.advantage);
          expansionPlan.updateValue(data.expansionPlan);
          highlightIndustry.updateValue(data.highlightIndustry);
          // update business risks
          if (data.businessRisks != null && data.businessRisks.length > 0) {
            for (var i = 0; i < data.businessRisks.length; i++) {
              addBusinessRisk();
            }
            // delay untuk memastikan fields telah ditambahkan ke ui
            await Future.delayed(Duration(milliseconds: 500));
            int index = 0;
            var riskFields = businessRisks.state.fieldBlocs;
            riskFields.forEach((element) {
              element.description
                  .updateValue("${data.businessRisks[index].desc}");
              var item = element.businessRiskType.state.items
                  .where((i) =>
                      i.id ==
                      data.businessRisks[index].businessRisksId.toString())
                  .first;
              element.description.updateValue(data.businessRisks[index].desc);
              element.businessRiskType.updateValue(item);
              index++;
            });
          } else {
            addBusinessRisk();
          }

          // update business suppliers
          if (data.emitensSuppliers != null &&
              data.emitensSuppliers.length > 0) {
            for (var i = 0; i < data.emitensSuppliers.length; i++) {
              addBusinessSupplier();
            }
            // delay untuk memastikan fields telah ditambahkan ke ui
            await Future.delayed(Duration(milliseconds: 500));
            int index = 0;
            var supplierFields = businessSupplier.state.fieldBlocs;
            supplierFields.forEach((element) {
              var supplier = data.emitensSuppliers[index];
              element.fullName.updateValue("${supplier.name}");
              element.address.updateValue("${supplier.address}");
              element.phone.updateValue("${supplier.phone}");
              index++;
            });
          } else {
            for (var i = 0; i < 3; i++) {
              addBusinessSupplier();
            }
          }

          // update business buyers
          if (data.emitenBuyers != null && data.emitenBuyers.length > 0) {
            for (var i = 0; i < data.emitenBuyers.length; i++) {
              addBusinessBuyer();
            }
            // delay untuk memastikan fields telah ditambahkan ke ui
            await Future.delayed(Duration(milliseconds: 500));
            int index = 0;
            var buyerFields = businessBuyer.state.fieldBlocs;
            buyerFields.forEach((element) {
              var buyer = data.emitenBuyers[index];
              element.fullName.updateValue("${buyer.name}");
              element.address.updateValue("${buyer.address}");
              element.phone.updateValue("${buyer.phone}");
              index++;
            });
          } else {
            addBusinessBuyer();
          }
          // Lampiran validasi POS
          List<FileUploadModel> contracts = [];
          if (data.emitenCoopContracts != null) {
            employmentContract.updateExtraData(null);
            data.emitenCoopContracts.forEach((e) {
              contracts.add(FileUploadModel(
                filename: "${e.filename}",
                url: "${e.url}",
              ));
            });
          }

          if (data.businessOptionsAnswer != null &&
              data.businessOptionsAnswer.length > 0) {
            for (var i = 0; i < data.businessOptionsAnswer.length; i++) {
              switch (i) {
                case 0:
                  updateSelectField(
                    bloc: businessAspect,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 1:
                  updateSelectField(
                    bloc: businessSocialPolitic,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 2:
                  updateSelectField(
                    bloc: businessGovernmentSupport,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 3:
                  updateSelectField(
                    bloc: businessSubstitution,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 4:
                  updateSelectField(
                    bloc: businessCustomerDependence,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 5:
                  updateSelectField(
                    bloc: businessSupplierDependence,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                case 6:
                  updateSelectField(
                    bloc: businessCompetitor,
                    id: data.businessOptionsAnswer[i].businessAspectOptionsId,
                  );
                  break;
                default:
              }
            }
          }
          employmentContract.updateExtraData(contracts);
          await Future.delayed(Duration(milliseconds: 500));
          PralistingHelper.handleErrorFields(
            state: state,
            submission: data.submission,
          );
          // emitLoaded();
        } else {
          initFields();
          // emitLoaded();
        }
      } else {
        result.leftMap((l) async {
          initFields();
          // emitLoaded();
          printFailure(l);
        });
      }
    } catch (e, stack) {
      initFields();
      emitLoaded();
      santaraLog(e, stack);
    }
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        var body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  getProspects() async {
    await PralistingHelper.getBusinessOptions(
      group: 'prospect',
      bloc: businessAspect,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getSocpols() async {
    await PralistingHelper.getBusinessOptions(
      group: 'socpol',
      bloc: businessSocialPolitic,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getGovs() async {
    await PralistingHelper.getBusinessOptions(
      group: 'gov',
      bloc: businessGovernmentSupport,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getSubs() async {
    await PralistingHelper.getBusinessOptions(
      group: 'sub',
      bloc: businessSubstitution,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getBuyerDeps() async {
    await PralistingHelper.getBusinessOptions(
      group: 'buyer_dep',
      bloc: businessCustomerDependence,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getSupplierDeps() async {
    await PralistingHelper.getBusinessOptions(
      group: 'supplier_dep',
      bloc: businessSupplierDependence,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  getCompetitors() async {
    await PralistingHelper.getBusinessOptions(
      group: 'competitor',
      bloc: businessCompetitor,
      getListBusinessOptions: getListBusinessOptions,
    );
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getBusinessRisks();
    await getBusinessInfoOneData("$uuid");
    await Future.delayed(Duration(milliseconds: 100));
    businessRisks.listen((data) {
      data.fieldBlocs.asMap().forEach((index, element) {
        element.businessRiskType.onValueChanges(onData: (prev, next) {
          if (prev.value == null) {
            risks.remove(next.value);
          }

          if (prev.value != null && prev != next) {
            risks.remove(prev.value);
          }
          updateItems(index, risks);
        });
      });
    });
    emitLoaded();
    previousState = state.toJson();
  }

  updateItems(int indexVal, List<SearchData> items) {
    businessRisks.state.fieldBlocs.asMap().forEach((index, element) {
      if (index != indexVal) {
        if (indexVal < index) {
          var currentItem = element.businessRiskType.state?.value;
          if (currentItem != null) {
            items.add(currentItem);
          }
          element.businessRiskType.updateItems(items);
        }
      }
    });
  }

  postData(Map<String, dynamic> body, {bool isNext = true}) async {
    try {
      emitSubmitting();
      var listRisks = [];
      var businessOptions = [];
      body["business_risks"].forEach((element) {
        var data = {
          "business_risks_id": int.parse(
              element["business_risk_type"].id != null
                  ? element["business_risk_type"].id
                  : "0"),
          "desc": element["description"]
        };
        listRisks.add(data);
      });
      body["business_risks"] = listRisks;
      body["emiten_coop_contracts"] =
          PralistingHelper.getUploadedFileName(employmentContract);
      businessOptions.add(body["business_aspect"].id);
      businessOptions.add(body["business_social_politic"].id);
      businessOptions.add(body["business_government_support"].id);
      businessOptions.add(body["business_substitution"].id);
      businessOptions.add(body["business_customer_dependence"].id);
      businessOptions.add(body["business_supplier_dependence"].id);
      businessOptions.add(body["business_competitor"].id);
      body["business_options_answer"] =
          businessOptions.map((e) => int.parse(e)).toList();
      // logger.i(body);
      var data = {"body": body, "uuid": "$uuid"};
      final result = await updateBusinessInfoOne(data);
      if (result.isRight()) {
        var message = result.getOrElse(null);
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}

class BusinessRiskFieldBloc extends GroupFieldBloc {
  final SelectFieldBloc businessRiskType;
  final TextFieldBloc description;
  String errorMessage;

  BusinessRiskFieldBloc({
    @required this.businessRiskType,
    @required this.description,
    String name,
  }) : super([businessRiskType, description], name: name);

  String get error => errorMessage;

  set error(String error) => errorMessage = error;
}

class BusinessPartnerFieldBloc extends GroupFieldBloc {
  final TextFieldBloc fullName;
  final TextFieldBloc phone;
  final TextFieldBloc address;
  String errorMessage;

  BusinessPartnerFieldBloc({
    @required this.fullName,
    @required this.phone,
    @required this.address,
    String name,
  }) : super([fullName, phone, address], name: name);

  String get error => errorMessage;

  set error(String error) => errorMessage = error;
}
