import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/blocs/business_info_1_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_advantage_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_aspect_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_buyer_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_competitor_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_contracts_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_customer_dependence_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_description_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_expansion_plan_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_government_support_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_history_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_risk_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_social_politic_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_substitution_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_supplier_dependence_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/business_supplier_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/widgets/highlight_industy_field.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/pages/business_info_2_page.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

import '../../../../../../injector_container.dart';

class BusinessInfoOnePage extends StatefulWidget {
  final String uuid;
  BusinessInfoOnePage({@required this.uuid});

  @override
  _BusinessInfoOnePageState createState() => _BusinessInfoOnePageState();
}

class _BusinessInfoOnePageState extends State<BusinessInfoOnePage> {
  BusinessInfoOneBloc bloc;
  final _controller = ScrollController();

  @override
  void initState() {
    super.initState();
    bloc = sl<BusinessInfoOneBloc>();
    bloc.uuid = widget.uuid;
    PralistingHelper.initAutoScroll(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Informasi Usaha (1)",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "2 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider<BusinessInfoOneBloc>(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<BusinessInfoOneBloc>();
              return FormBlocListener<BusinessInfoOneBloc, FormResult,
                  FormResult>(
                onLoaded: (context, state) {
                  setState(() {});
                  // PralistingHelper.controller.jumpTo(0);
                  PralistingHelper.handleScrollValidation(state: state);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                  print(">> submitting!");
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BusinsessInfoTwoPage(
                          uuid: widget.uuid,
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  }

                  if (form.status == FormStatus.validation_error) {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                  PralistingHelper.handleScrollValidation(state: state);
                },
                child: BlocBuilder<BusinessInfoOneBloc, FormBlocState>(
                  bloc: bloc,
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        color: Colors.white,
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: SingleChildScrollView(
                          scrollDirection: PralistingHelper.scrollDirection,
                          controller: PralistingHelper.controller,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BusinessHistoryField(bloc: bloc),
                              BusinessDescriptionField(bloc: bloc),
                              BusinessAdvantageField(bloc: bloc),
                              BusinessExpansionPlanField(bloc: bloc),
                              HighlightIndustryField(bloc: bloc),
                              BusinessRiskField(bloc: bloc),
                              BusinessSupplierField(bloc: bloc),
                              BusinessBuyerField(bloc: bloc),
                              BusinessContractsField(bloc: bloc),
                              BusinessAspectField(bloc: bloc),
                              BusinessSocialPoliticField(bloc: bloc),
                              BusinessGovernmentSupportField(bloc: bloc),
                              BusinessSubstitutionField(bloc: bloc),
                              BusinessCustomerDependenceField(bloc: bloc),
                              BusinessSupplierDependenceField(bloc: bloc),
                              BusinessCompetitorField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
