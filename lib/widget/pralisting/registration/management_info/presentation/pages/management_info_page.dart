import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/financial_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/competence_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/employee_details_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/id_card_file_upload/id_card_file_upload.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/inovation_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/management_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/number_of_employees_field.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/reputation_field.dart';

import '../../../../../../injector_container.dart';

class ManagementInfoPage extends StatefulWidget {
  final String uuid;
  ManagementInfoPage({@required this.uuid});
  @override
  _ManagementInfoPageState createState() => _ManagementInfoPageState();
}

class _ManagementInfoPageState extends State<ManagementInfoPage> {
  ManagementInfoBloc bloc;
  final _controller = ScrollController();

  Widget _buildManagementAspectTitle() {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s40),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          "Aspek Manajemen",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          "Pilih salah satu pernyataan yang paling sesuai dengan bisnis Anda",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = sl<ManagementInfoBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Informasi Manajemen",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "4 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<ManagementInfoBloc>();
              return FormBlocListener<ManagementInfoBloc, FormResult,
                  FormResult>(
                onLoaded: (context, state) async {
                  await Future.delayed(Duration(milliseconds: 500));
                  // _controller.jumpTo(0);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => FinancialInfoPage(
                          uuid: "${widget.uuid}",
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  } else {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<ManagementInfoBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.white,
                        child: SingleChildScrollView(
                          controller: _controller,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ManagementField(bloc: bloc),
                              NumberOfEmployeesField(bloc: bloc),
                              EmployeeDetailsField(bloc: bloc),
                              _buildManagementAspectTitle(),
                              CompetenceField(bloc: bloc),
                              ReputationField(bloc: bloc),
                              InovationField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
