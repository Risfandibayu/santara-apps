import 'dart:io';
import 'package:meta/meta.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/usecases/business_info_1_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/id_card_file_upload_model.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/management_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/domain/usecases/management_info_usecases.dart';

class ManagementInfoBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetListBusinessOptions getListBusinessOptions;
  final GetManagementInfo getManagementInfo;
  final UpdateManagementInfo updateManagementInfo;

  final managements = ListFieldBloc<ManagementFieldBloc>(name: 'managements');

  final numberOfEmployees = TextFieldBloc(
    name: 'number_of_employees',
    validators: [PralistingValidationHelper.required],
  );

  final employeeDetails =
      ListFieldBloc<EmployeeDetailsFieldBloc>(name: 'employee_details');

  final competence = SelectFieldBloc<SearchData, dynamic>(
    name: 'competence',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final reputation = SelectFieldBloc<SearchData, dynamic>(
    name: 'reputation',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final inovation = SelectFieldBloc<SearchData, dynamic>(
    name: 'inovation',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  ManagementInfoBloc({
    @required this.getListBusinessOptions,
    @required this.getManagementInfo,
    @required this.updateManagementInfo,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      managements,
      numberOfEmployees,
      employeeDetails,
      competence,
      reputation,
      inovation,
    ]);

    managements.state.fieldBlocs.asMap().forEach((index, e) async {
      e.photo.onValueChanges(onData: (prev, next) {});
    });
  }

  void addManagement() {
    managements.addFieldBloc(
      ManagementFieldBloc(
        name: 'management',
        fullName: TextFieldBloc(
          name: 'full_name',
          validators: [
            PralistingValidationHelper.required,
            PralistingValidationHelper.fullName,
          ],
        ),
        position: TextFieldBloc(
          name: 'position',
          validators: [
            PralistingValidationHelper.required,
            PralistingValidationHelper.position,
          ],
        ),
        experience: TextFieldBloc(
          name: 'experience',
          validators: [
            PralistingValidationHelper.required,
          ],
        ),
        photo: InputFieldBloc<dynamic, Object>(
          name: 'photo',
          validators: [
            PralistingValidationHelper.required,
          ],
        ),
      ),
    );
  }

  void removeManagement(int index) {
    managements.removeFieldBlocAt(index);
  }

  void addEmployeeDetails() {
    employeeDetails.addFieldBloc(
      EmployeeDetailsFieldBloc(
        name: 'employee_details',
        position: TextFieldBloc(
          name: 'position_level',
          validators: [PralistingValidationHelper.required],
        ),
        total: TextFieldBloc(
          name: 'number_of_employees',
          validators: [PralistingValidationHelper.required],
        ),
      ),
    );
  }

  void removeEmployeeDetails(int index) {
    employeeDetails.removeFieldBlocAt(index);
  }

  getCompetencies() async {
    await PralistingHelper.getBusinessOptions(
      group: 'competency',
      bloc: competence,
      getListBusinessOptions: getListBusinessOptions,
      buffer: true,
    );
  }

  getReputations() async {
    await PralistingHelper.getBusinessOptions(
      group: 'reputation',
      bloc: reputation,
      getListBusinessOptions: getListBusinessOptions,
      buffer: true,
    );
  }

  getInovations() async {
    await PralistingHelper.getBusinessOptions(
      group: 'innovation',
      bloc: inovation,
      getListBusinessOptions: getListBusinessOptions,
      buffer: true,
    );
  }

  getData() async {
    try {
      await getCompetencies();
      await getReputations();
      await getInovations();
      var res = await getManagementInfo(uuid);
      if (res.isRight()) {
        var data = res.getOrElse(null);
        numberOfEmployees.updateValue("${data.employee}");
        // Kompetensi
        if (data.experienceCompetencyQuisioner.id != null) {
          competence.updateValue(competence.state.items.firstWhere(
              (e) => e.id == "${data.experienceCompetencyQuisioner.id}"));
        }
        if (data.reputationQuisioner.id != null) {
          reputation.updateValue(reputation.state.items
              .firstWhere((e) => e.id == "${data.reputationQuisioner.id}"));
        }
        if (data.innovationQuisioner.id != null) {
          inovation.updateValue(inovation.state.items
              .firstWhere((e) => e.id == "${data.innovationQuisioner.id}"));
        }
        // emiten teams
        if (data.emitenTeams != null && data.emitenTeams.length > 0) {
          data.emitenTeams.asMap().forEach((index, e) async {
            addManagement();
            await Future.delayed(Duration(milliseconds: 100));
            var photo = IdCardFileUploadModel(
              state: IdCardFileUploadState.success,
              url: e.photoUrl,
              fileName: e.photo,
            );
            managements.state.fieldBlocs[index].fullName.updateValue(e.name);
            managements.state.fieldBlocs[index].experience
                .updateValue(e.experience);
            managements.state.fieldBlocs[index].position
                .updateValue(e.position);
            managements.state.fieldBlocs[index].photo.updateExtraData(photo);
            managements.state.fieldBlocs[index].photo.updateValue("OK");
          });
        } else {
          addManagement();
          addManagement();
        }

        // emiten positions
        if (data.emitenPositions != null && data.emitenPositions.length > 0) {
          data.emitenPositions.asMap().forEach((index, e) async {
            addEmployeeDetails();
            await Future.delayed(Duration(milliseconds: 100));
            employeeDetails.state.fieldBlocs[index].position
                .updateValue(e.name);
            employeeDetails.state.fieldBlocs[index].total
                .updateValue("${e.employee}");
          });
        } else {
          addEmployeeDetails();
          addEmployeeDetails();
        }
      } else {
        initFields();
      }
      await Future.delayed(Duration(milliseconds: 100));
      emitLoaded();
    } catch (e, stack) {
      initFields();
      santaraLog(e, stack);
      await Future.delayed(Duration(milliseconds: 100));
      emitLoaded();
    }
  }

  initFields() {
    addManagement();
    addManagement();
    addEmployeeDetails();
    addEmployeeDetails();
  }

  postData({bool isNext}) async {
    emitSubmitting();
    try {
      List<EmitenTeam> teams = [];
      List<EmitenPosition> positions = [];
      // Managements
      var rawManagements = managements.state.fieldBlocs;
      if (rawManagements != null && rawManagements.length > 0) {
        rawManagements.forEach((l) {
          IdCardFileUploadModel data = l.photo.state.extraData;
          EmitenTeam team = EmitenTeam(
            name: l.fullName.value,
            position: l.position.value,
            experience: l.experience.value,
            photo: data.fileName,
          );
          teams.add(team);
        });
      }
      // Positions
      var rawPositions = employeeDetails.state.fieldBlocs;
      if (rawPositions != null && rawPositions.length > 0) {
        rawPositions.forEach((l) {
          EmitenPosition position = EmitenPosition(
            name: l.position.value,
            employee: l.total.valueToInt,
          );
          positions.add(position);
        });
      }
      var rawBody = ManagementInfoModel(
        emitenTeams: teams,
        employee: numberOfEmployees.valueToInt,
        emitenPositions: positions,
        experienceCompetencyQuisioner: Quisioner(
          id: int.parse(competence.value.id),
          text: competence.value.name,
        ),
        reputationQuisioner: Quisioner(
          id: int.parse(reputation.value.id),
          text: reputation.value.name,
        ),
        innovationQuisioner: Quisioner(
          id: int.parse(inovation.value.id),
          text: inovation.value.name,
        ),
      );
      var body = rawBody.toJson();
      body["experience_competency_quisioner"] =
          rawBody.experienceCompetencyQuisioner.id;
      body["reputation_quisioner"] = rawBody.reputationQuisioner.id;
      body["innovation_quisioner"] = rawBody.innovationQuisioner.id;
      var data = {"body": body, "uuid": uuid};
      // logger.i(data);
      final result = await updateManagementInfo(data);
      if (result.isRight()) {
        result.map((r) {
          final success = FormResult(
            status: isNext
                ? FormStatus.submit_success
                : FormStatus.submit_success_close_page,
            message: r.meta.message,
          );
          emitSuccess(canSubmitAgain: true, successResponse: success);
        });
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        await postData(isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  @override
  void onSubmitting() async {}
}

class ManagementFieldBloc extends GroupFieldBloc {
  final TextFieldBloc fullName;
  final TextFieldBloc position;
  final TextFieldBloc experience;
  final InputFieldBloc photo;

  ManagementFieldBloc({
    @required this.fullName,
    @required this.position,
    @required this.experience,
    @required this.photo,
    String name,
  }) : super([fullName, position, experience, photo], name: name);
}

class EmployeeDetailsFieldBloc extends GroupFieldBloc {
  final TextFieldBloc position;
  final TextFieldBloc total;

  EmployeeDetailsFieldBloc({
    @required this.position,
    @required this.total,
    String name,
  }) : super([position, total], name: name);
}
