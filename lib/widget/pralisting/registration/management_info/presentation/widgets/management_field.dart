import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/management_card.dart';

class ManagementField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  ManagementField({@required this.bloc});

  Widget _managementsWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<ManagementFieldBloc>,
            ListFieldBlocState<ManagementFieldBloc>>(
          bloc: bloc.managements,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return ManagementCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: () => bloc.removeManagement(i),
                  );
                },
              );
            }
            return Container();
          },
        ),
        bloc.managements.state.fieldBlocs.isNotEmpty &&
                bloc.managements.state.fieldBlocs.length < 5
            ? FlatButton.icon(
                padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                onPressed: () => bloc.addManagement(),
                icon: Text(
                  "Tambah Data Pengurus",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff218196),
                    decoration: TextDecoration.underline,
                  ),
                ),
                label: Icon(
                  Icons.add_circle,
                  size: FontSize.s20,
                  color: Color(0xff218196),
                ),
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(bottom: Sizes.s30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Profil Tim",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: FontSize.s14,
              ),
            ),
            subtitle: Text(
              "Masukan Nama Pengurus dan Tim (minimal 2 pengurus)",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s12,
              ),
            ),
          ),
          SizedBox(height: Sizes.s10),
          _managementsWidget()
        ],
      ),
    );
  }
}
