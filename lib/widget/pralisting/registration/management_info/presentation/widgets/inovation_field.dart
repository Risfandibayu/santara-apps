import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';

class InovationField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  InovationField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.inovation.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.inovation.state.items != null &&
                    bloc.inovation.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.inovation.state.items,
                      selected: bloc.inovation.value,
                      showSearch: false,
                    );
                    bloc.inovation.updateValue(result);
                  }
                : () => ToastHelper.showFailureToast(
                      context,
                      "Data inovasi kosong",
                    ),
            onReload: () async => await bloc.getInovations(),
            labelText: "Inovasi",
            hintText: "Bagaimana Penerapan Inovasi Teknologi di Usaha Anda?",
            state: fieldState,
            value: bloc.inovation.value?.name,
            errorText: !bloc.inovation.state.isInitial
                ? bloc.inovation.state.hasError
                    ? bloc.inovation.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
