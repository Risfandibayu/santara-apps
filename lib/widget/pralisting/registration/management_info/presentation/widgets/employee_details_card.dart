import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';

class EmployeeDetailsCard extends StatelessWidget {
  final int index;
  final EmployeeDetailsFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const EmployeeDetailsCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: TextFieldBlocBuilder(
              textFieldBloc: fieldBloc.position,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'Level Jabatan',
                floatingLabelBehavior: FloatingLabelBehavior.always,
                hintText: '',
                border: PralistingInputDecoration.outlineInputBorder,
                contentPadding: EdgeInsets.all(Sizes.s15),
                isDense: true,
                errorMaxLines: 5,
                hintStyle: PralistingInputDecoration.hintTextStyle(false),
                labelStyle: PralistingInputDecoration.labelTextStyle,
              ),
            ),
          ),
          SizedBox(width: Sizes.s10),
          Expanded(
            flex: 1,
            child: TextFieldBlocBuilder(
              textFieldBloc: fieldBloc.total,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Jumlah Karyawan',
                floatingLabelBehavior: FloatingLabelBehavior.always,
                hintText: '',
                border: PralistingInputDecoration.outlineInputBorder,
                contentPadding: EdgeInsets.all(Sizes.s15),
                isDense: true,
                errorMaxLines: 5,
                hintStyle: PralistingInputDecoration.hintTextStyle(false),
                labelStyle: PralistingInputDecoration.labelTextStyle,
                suffixIcon: PralistingInfoButton(
                  title: "Jumlah Karyawan",
                  message:
                      "Isi Level Jabatan (Senior Manager, Manager, Supervisor, Admin, Staf produksi, dll) dan Jumlah Karyawan Pada Level Jabatan Tersebut.",
                ),
              ),
              inputFormatters: [
                LengthLimitingTextInputFormatter(3),
                FilteringTextInputFormatter.digitsOnly,
              ],
            ),
          ),
          index > 1
              ? InkWell(
                  onTap: index > 1 ? onRemove : null,
                  child: Container(
                    padding: EdgeInsets.only(left: Sizes.s15),
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
