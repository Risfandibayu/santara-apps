import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';

class NumberOfEmployeesField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  NumberOfEmployeesField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Karyawan",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          TextFieldBlocBuilder(
            textFieldBloc: bloc.numberOfEmployees,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: 'Jumlah Karyawan',
              floatingLabelBehavior: FloatingLabelBehavior.always,
              hintText: '10',
              border: PralistingInputDecoration.outlineInputBorder,
              contentPadding: EdgeInsets.all(Sizes.s15),
              isDense: true,
              errorMaxLines: 5,
              hintStyle: PralistingInputDecoration.hintTextStyle(false),
              labelStyle: PralistingInputDecoration.labelTextStyle,
            ),
            inputFormatters: [
              LengthLimitingTextInputFormatter(4),
              FilteringTextInputFormatter.digitsOnly,
            ],
          ),
        ],
      ),
    );
  }
}
