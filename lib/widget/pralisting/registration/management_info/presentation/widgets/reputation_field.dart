import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';

class ReputationField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  ReputationField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.reputation.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.reputation.state.items != null &&
                    bloc.reputation.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.reputation.state.items,
                      selected: bloc.reputation.value,
                      showSearch: false,
                    );
                    bloc.reputation.updateValue(result);
                  }
                : () => ToastHelper.showFailureToast(
                      context,
                      "Data reputasi kosong",
                    ),
            onReload: () async => await bloc.getReputations(),
            labelText: "Reputasi",
            hintText: "Reputasi perusahaan Anda seperti apa?",
            state: fieldState,
            value: bloc.reputation.value?.name,
            errorText: !bloc.reputation.state.isInitial
                ? bloc.reputation.state.hasError
                    ? bloc.reputation.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
