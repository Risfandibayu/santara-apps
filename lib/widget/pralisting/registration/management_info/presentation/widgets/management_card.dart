import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';

import 'id_card_file_upload/id_card_file_upload.dart';

class ManagementCard extends StatelessWidget {
  final int index;
  final ManagementFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const ManagementCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s30, bottom: Sizes.s20),
              )
            : Container(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Pengurus #${index + 1}',
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: Sizes.s5),
                index == 0
                    ? Text(
                        'Pengurus dengan posisi setara owner atau CEO',
                        style: TextStyle(
                          fontSize: FontSize.s12,
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    : Container(),
              ],
            ),
            index > 1
                ? FlatButton.icon(
                    padding: EdgeInsets.zero,
                    onPressed: onRemove,
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: FontSize.s20,
                    ),
                    label: Text(
                      "Hapus",
                      style: TextStyle(
                        color: Colors.red,
                        decoration: TextDecoration.underline,
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.fullName,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Nama Lengkap',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.position,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Jabatan',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        SizedBox(height: Sizes.s10),
        TextFieldBlocBuilder(
          textFieldBloc: fieldBloc.experience,
          keyboardType: TextInputType.multiline,
          maxLines: 4,
          decoration: InputDecoration(
            labelText: 'Pengalaman Pendidikan Pekerjaan dan Ketrampilan',
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: '',
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            labelStyle: PralistingInputDecoration.labelTextStyle,
          ),
        ),
        SizedBox(height: Sizes.s10),
        Text(
          "Unggah Foto Pribadi",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: FontSize.s12,
          ),
        ),
        SizedBox(height: Sizes.s10),
        IdCardFileUpload(
          bloc: fieldBloc.photo,
          maxFileSize: 10,
        )
      ],
    );
  }
}
