import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

class CompetenceField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  CompetenceField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10),
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.competence.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.competence.state.items != null &&
                    bloc.competence.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.competence.state.items,
                      selected: bloc.competence.value,
                      showSearch: false,
                    );
                    bloc.competence.updateValue(result);
                  }
                : () => ToastHelper.showFailureToast(
                      context,
                      "Data pengalaman/kompetensi kosong",
                    ),
            onReload: () async => await bloc.getCompetencies(),
            labelText: "Pengalaman/Kompetensi",
            hintText: "Tingkat Pengalaman Anda Menjalankan Bisnis?",
            state: fieldState,
            value: bloc.competence.value?.name,
            errorText: !bloc.competence.state.isInitial
                ? bloc.competence.state.hasError
                    ? bloc.competence.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
