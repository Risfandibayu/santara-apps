import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/blocs/management_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/widgets/employee_details_card.dart';

class EmployeeDetailsField extends StatelessWidget {
  final ManagementInfoBloc bloc;
  EmployeeDetailsField({@required this.bloc});

  Widget _employeeDetailsWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<EmployeeDetailsFieldBloc>,
            ListFieldBlocState<EmployeeDetailsFieldBloc>>(
          bloc: bloc.employeeDetails,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return EmployeeDetailsCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: () => bloc.removeEmployeeDetails(i),
                  );
                },
              );
            }
            return Container();
          },
        ),
        bloc.employeeDetails.state.fieldBlocs.isNotEmpty &&
                bloc.employeeDetails.state.fieldBlocs.length < 10
            ? FlatButton.icon(
                padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
                onPressed: () => bloc.addEmployeeDetails(),
                icon: Text(
                  "Tambah Level Jabatan",
                  style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff218196),
                    decoration: TextDecoration.underline,
                  ),
                ),
                label: Icon(
                  Icons.add_circle,
                  size: FontSize.s20,
                  color: Color(0xff218196),
                ),
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Rincian Karyawan",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: FontSize.s14,
              ),
            ),
          ),
          _employeeDetailsWidget()
        ],
      ),
    );
  }
}
