import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/id_card_file_upload_model.dart';
import 'package:http_parser/http_parser.dart';

class IdCardFileUploadBloc extends FormBloc<String, String> {
  final InputFieldBloc fileUpload;
  Dio dio = Dio();

  IdCardFileUploadBloc({@required this.fileUpload}) {
    addFieldBlocs(fieldBlocs: [fileUpload]);
    initImageUpload();
  }

  void initImageUpload() {
    if (fileUpload.state.extraData != null &&
        fileUpload.state.extraData.length > 0) {
      fileUpload.state.extraData.asMap().forEach((index, element) {
        final data = IdCardFileUploadModel(
          state: IdCardFileUploadState.success,
          fileName: element.filename,
          url: element.url,
        );
        fileUpload.updateExtraData(data);
      });
      fileUpload.updateValue("OK");
    }

    if (fileUpload.state.extraData == null) {
      fileUpload.updateExtraData(IdCardFileUploadModel(
        state: IdCardFileUploadState.uninitialized,
        url: "",
        fileName: "",
      ));
    }
  }

  Future<void> _upload() async {
    print(">> Prog 1");
    if (fileUpload.state.value != null) {
      try {
        MultipartFile file = await MultipartFile.fromFile(
          fileUpload.state.value.path,
          contentType: MediaType("image", "jpg"),
        );
        // logger.i(">> File Type : ${file.contentType}");
        FormData formData = FormData.fromMap({
          "image": file,
          "type": "images",
          "location": "pralisting/business_documents/"
        }); // TODO : Change Avatar Key
        // update state
        fileUpload.updateExtraData(
          IdCardFileUploadModel(
            state: IdCardFileUploadState.uploading,
            url: "",
            fileName: fileUpload.state.value.path.split("/").last,
          ),
        );
        print(">> Prog 2");
        // upload progress
        final response = await dio.post(
          // "http://maskmine.hol.es/image_upload/upload_test.php",
          "$apiFileUpload/upload",
          data: formData,
          options: Options(headers: await UserAgent.headers()),
        );

        if (response.statusCode == 200) {
          fileUpload.updateExtraData(
            IdCardFileUploadModel(
              state: IdCardFileUploadState.success,
              url: response.data["data"]["url"],
              fileName: response.data["data"]["filename"],
            ),
          );
          // logger.i(response.data);
          // logger.i(fileUpload.state.extraData);
          // logger.i(response.data);
          emitSuccess(
            successResponse: "Berhasil mengunggah file!",
            canSubmitAgain: true,
          );
        }
      } on DioError catch (e, stack) {
        // print(e.response.data);
        santaraLog(e, stack);

        fileUpload.updateExtraData(
          IdCardFileUploadModel(
            state: IdCardFileUploadState.error,
            url: "",
            fileName: fileUpload.state.value.path.split("/").last,
          ),
        );
        emitFailure(failureResponse: e.response.statusMessage);
      } catch (e, stack) {
        santaraLog(e, stack);

        fileUpload.updateExtraData(
          IdCardFileUploadModel(
            state: IdCardFileUploadState.success,
            url: "",
            fileName: fileUpload.state.value.path.split("/").last,
          ),
        );
        emitFailure(failureResponse: "$e");
      }
    } else {
      logger.i(">> Please pick a file");
    }
  }

  @override
  void onSubmitting() async {
    await _upload();
  }
}
