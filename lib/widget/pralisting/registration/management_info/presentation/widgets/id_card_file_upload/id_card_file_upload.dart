import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/id_card_file_upload_model.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'id_card_file_upload_bloc.dart';

class IdCardFileUpload extends StatelessWidget {
  final InputFieldBloc bloc;
  final int maxFileSize;
  IdCardFileUpload({
    @required this.bloc,
    this.maxFileSize = 10, // mb
  });

  Widget _buildImagePicker(
    BuildContext context, {
    @required IdCardFileUploadBloc bloc,
  }) {
    return BlocBuilder<IdCardFileUploadBloc, FormBlocState>(
      bloc: bloc,
      builder: (context, state) {
        bool canUpload =
            state != null && state is FormBlocSubmitting ? false : true;
        IdCardFileUploadModel stateData = bloc.fileUpload.state?.extraData;
        return InkWell(
          onTap: canUpload
              ? () async {
                  try {
                    bool show = false; // show loading dialog
                    FilePickerResult result =
                        await FilePicker.platform.pickFiles(
                      allowMultiple: false,
                      type: FileType.custom,
                      allowedExtensions: ['jpg', 'jpeg', 'png'],
                      onFileLoading: (status) {
                        // file picker caching file to storage
                        if (status == FilePickerStatus.picking) {
                          show = true;
                          PopupHelper.showLoading(context);
                        }

                        if (status == FilePickerStatus.done) {
                          if (show) {
                            Navigator.pop(context);
                          }
                        }
                      },
                    );

                    if (result != null) {
                      File _file = File(result.paths[0]);
                      if (_file.lengthSync() > (maxFileSize * 1000000)) {
                        ToastHelper.showBasicToast(
                          context,
                          "Ukuran file maksimal adalah $maxFileSize MB",
                        );
                      } else {
                        bloc.fileUpload.updateValue(File(result.paths[0]));
                        bloc.submit();
                      }
                    }
                  } catch (e, stack) {
                    santaraLog(e, stack);
                  }
                }
              : null,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              state is FormBlocSuccess ||
                      state is FormBlocLoaded &&
                          stateData?.url != null &&
                          stateData.url.isNotEmpty
                  ? SantaraCachedImage(
                      image: stateData.url,
                      width: Sizes.s80,
                      height: Sizes.s100,
                    )
                  : bloc.fileUpload.state.value != null &&
                          bloc.fileUpload.state.value.runtimeType == File
                      ? Image.file(
                          bloc.fileUpload.state.value,
                          width: Sizes.s80,
                          height: Sizes.s100,
                        )
                      : Icon(
                          Icons.camera_alt_outlined,
                          color: Color(0xFFB8B8B8),
                          size: Sizes.s40,
                        ),
              SizedBox(
                height: Sizes.s10,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(
                  Sizes.s20,
                  Sizes.s5,
                  Sizes.s20,
                  Sizes.s5,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: state is FormBlocSubmitting
                      ? Colors.grey
                      : Color(0xff6870E1),
                ),
                child: stateData != null && stateData.state != null
                    ? Text(
                        state is FormBlocSubmitting
                            ? "Mengunggah.."
                            : state is FormBlocFailure
                                ? "Unggah Kembali"
                                : state is FormBlocSuccess
                                    ? "Ganti Foto"
                                    : "Unggah Foto",
                        style: TextStyle(
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      )
                    : Text("Unggah Foto"),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => IdCardFileUploadBloc(fileUpload: bloc),
      child: Builder(
        builder: (context) {
          // ignore: close_sinks
          final bloc = context.bloc<IdCardFileUploadBloc>();
          return FormBlocListener<IdCardFileUploadBloc, String, String>(
            onLoaded: (context, state) {},
            onSubmitting: (context, state) {},
            onSuccess: (context, state) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("Berhasil mengunggah File"),
                  duration: Duration(seconds: 2),
                ),
              );
            },
            onFailure: (context, state) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("Gagal mengunggah File"),
                  duration: Duration(seconds: 2),
                ),
              );
            },
            onSubmissionFailed: (context, state) {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                DottedBorder(
                  color: Color(0xFFB8B8B8),
                  strokeWidth: 2,
                  radius: Radius.circular(Sizes.s25),
                  dashPattern: [5, 5],
                  child: Container(
                    padding: EdgeInsets.only(top: Sizes.s10, bottom: Sizes.s10),
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                      color: Color(0xffF4F4F4),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(width: Sizes.s10),
                        //
                        _buildImagePicker(context, bloc: bloc),
                        SizedBox(width: Sizes.s20),
                        Flexible(
                          child: Text(
                            """1. Format jpg, jpeg, png
2. Ukuran file max 10 mb
3. Ukuran foto 280x380 px (optional)""",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: Sizes.s10,
                        ),
                      ],
                    ),
                  ),
                ),
                bloc.fileUpload.state != null &&
                        !bloc.fileUpload.state.isInitial
                    ? bloc.fileUpload.state.hasError
                        ? Container(
                            margin: EdgeInsets.only(
                                top: Sizes.s10, left: Sizes.s20),
                            child: Text(
                              "${bloc.fileUpload.state.error}",
                              style: TextStyle(
                                color: Colors.red[700],
                                fontSize: FontSize.s12,
                              ),
                            ),
                          )
                        : Container()
                    : Container()
              ],
            ),
          );
        },
      ),
    );
  }
}
