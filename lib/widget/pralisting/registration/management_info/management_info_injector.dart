import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/datasources/business_info_1_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/data/repositories/business_info_1_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/repositories/business_info_1_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/datasources/management_info_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/repositories/management_info_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/domain/repositories/management_info_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/domain/usecases/management_info_usecases.dart';
import '../../../../injector_container.dart';
import 'presentation/blocs/management_info_bloc.dart';

void initManagementInfoInjector() {
  sl.registerFactory(
    () => ManagementInfoBloc(
      getListBusinessOptions: sl(),
      getManagementInfo: sl(),
      updateManagementInfo: sl(),
    ),
  );

  sl.registerLazySingleton<ManagementInfoRepository>(
    () => ManagementInfoRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<ManagementInfoRemoteDataSource>(
    () => ManagementInfoRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetManagementInfo(sl()));
  sl.registerLazySingleton(() => UpdateManagementInfo(sl()));
}
