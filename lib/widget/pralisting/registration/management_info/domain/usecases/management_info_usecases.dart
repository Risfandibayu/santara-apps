import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/management_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/domain/repositories/management_info_repository.dart';

class GetManagementInfo implements UseCase<ManagementInfoModel, String> {
  final ManagementInfoRepository repository;
  GetManagementInfo(this.repository);

  @override
  Future<Either<Failure, ManagementInfoModel>> call(String uuid) async {
    return await repository.getManagementInfo(uuid: uuid);
  }
}

class UpdateManagementInfo implements UseCase<PralistingMetaModel, dynamic> {
  final ManagementInfoRepository repository;
  UpdateManagementInfo(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateManagementInfo(
      body: params['body'],
      uuid: params['uuid'],
    );
  }
}
