import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/management_info_model.dart';

abstract class ManagementInfoRepository {
  Future<Either<Failure, ManagementInfoModel>> getManagementInfo(
      {@required String uuid});
  Future<Either<Failure, PralistingMetaModel>> updateManagementInfo(
      {@required dynamic body, @required String uuid});
}
