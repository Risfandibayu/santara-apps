import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/datasources/management_info_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/management_info_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/domain/repositories/management_info_repository.dart';
import 'package:meta/meta.dart';

class ManagementInfoRepositoryImpl implements ManagementInfoRepository {
  final NetworkInfo networkInfo;
  final ManagementInfoRemoteDataSource remoteDataSource;

  ManagementInfoRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, ManagementInfoModel>> getManagementInfo(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getManagementInfo(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateManagementInfo(
      {dynamic body, String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data =
            await remoteDataSource.updateManagementInfo(body: body, uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
