import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/data/models/management_info_model.dart';

abstract class ManagementInfoRemoteDataSource {
  Future<PralistingMetaModel> updateManagementInfo(
      {@required dynamic body, @required String uuid});
  Future<ManagementInfoModel> getManagementInfo({@required String uuid});
}

class ManagementInfoRemoteDataSourceImpl
    implements ManagementInfoRemoteDataSource {
  final RestClient client;
  ManagementInfoRemoteDataSourceImpl({@required this.client});

  @override
  Future<ManagementInfoModel> getManagementInfo({String uuid}) async {
    try {
      final result = await client.getExternalUrl(url: "$apiPralisting/step4/$uuid");
      if (result.statusCode == 200) {
        return ManagementInfoModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateManagementInfo(
      {dynamic body, String uuid}) async {
    try {
      final result =
          await client.putExternalUrl(url: "$apiPralisting/step4/$uuid", body: body);
          // aku ikhwan
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
