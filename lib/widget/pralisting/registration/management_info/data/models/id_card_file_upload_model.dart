import 'package:meta/meta.dart';

enum IdCardFileUploadState { uninitialized, uploading, error, success }

class IdCardFileUploadModel {
  final IdCardFileUploadState state;
  final String url; // if file has uploaded
  final String fileName; // if bloc already has a file

  IdCardFileUploadModel({
    this.state = IdCardFileUploadState.uninitialized,
    this.url = "",
    this.fileName = "",
  });
}
