// To parse this JSON data, do
//
//     final managementInfoModel = managementInfoModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

ManagementInfoModel managementInfoModelFromJson(String str) =>
    ManagementInfoModel.fromJson(json.decode(str));

String managementInfoModelToJson(ManagementInfoModel data) =>
    json.encode(data.toJson());

class ManagementInfoModel {
  ManagementInfoModel({
    this.emitenTeams,
    this.employee,
    this.emitenPositions,
    this.experienceCompetencyQuisioner,
    this.reputationQuisioner,
    this.innovationQuisioner,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  List<EmitenTeam> emitenTeams;
  int employee;
  List<EmitenPosition> emitenPositions;
  Quisioner experienceCompetencyQuisioner;
  Quisioner reputationQuisioner;
  Quisioner innovationQuisioner;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory ManagementInfoModel.fromJson(Map<String, dynamic> json) =>
      ManagementInfoModel(
        emitenTeams: json["emiten_teams"] == null
            ? null
            : List<EmitenTeam>.from(
                json["emiten_teams"].map((x) => EmitenTeam.fromJson(x))),
        employee: json["employee"] == null ? null : json["employee"],
        emitenPositions: json["emiten_positions"] == null
            ? null
            : List<EmitenPosition>.from(json["emiten_positions"]
                .map((x) => EmitenPosition.fromJson(x))),
        experienceCompetencyQuisioner:
            json["experience_competency_quisioner"] == null
                ? null
                : Quisioner.fromJson(json["experience_competency_quisioner"]),
        reputationQuisioner: json["reputation_quisioner"] == null
            ? null
            : Quisioner.fromJson(json["reputation_quisioner"]),
        innovationQuisioner: json["innovation_quisioner"] == null
            ? null
            : Quisioner.fromJson(json["innovation_quisioner"]),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_teams": emitenTeams == null
            ? null
            : List<dynamic>.from(emitenTeams.map((x) => x.toJson())),
        "employee": employee == null ? null : employee,
        "emiten_positions": emitenPositions == null
            ? null
            : List<dynamic>.from(emitenPositions.map((x) => x.toJson())),
        "experience_competency_quisioner": experienceCompetencyQuisioner == null
            ? null
            : experienceCompetencyQuisioner.toJson(),
        "reputation_quisioner":
            reputationQuisioner == null ? null : reputationQuisioner.toJson(),
        "innovation_quisioner":
            innovationQuisioner == null ? null : innovationQuisioner.toJson(),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class EmitenPosition {
  EmitenPosition({
    this.name,
    this.employee,
  });

  String name;
  int employee;

  factory EmitenPosition.fromJson(Map<String, dynamic> json) => EmitenPosition(
        name: json["name"] == null ? null : json["name"],
        employee: json["employee"] == null ? null : json["employee"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "employee": employee == null ? null : employee,
      };
}

class EmitenTeam {
  EmitenTeam({
    this.name,
    this.position,
    this.experience,
    this.photo,
    this.photoUrl,
  });

  String name;
  String position;
  String experience;
  String photo;
  String photoUrl;

  factory EmitenTeam.fromJson(Map<String, dynamic> json) => EmitenTeam(
        name: json["name"] == null ? null : json["name"],
        position: json["position"] == null ? null : json["position"],
        experience: json["experience"] == null ? null : json["experience"],
        photo: json["photo"] == null ? null : json["photo"],
        photoUrl: json["photo_url"] == null ? null : json["photo_url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "position": position == null ? null : position,
        "experience": experience == null ? null : experience,
        "photo": photo == null ? null : photo,
        "photo_url": photoUrl == null ? null : photoUrl,
      };
}

class Quisioner {
  Quisioner({
    this.id,
    this.text,
  });

  int id;
  String text;

  factory Quisioner.fromJson(Map<String, dynamic> json) => Quisioner(
        id: json["id"] == null ? null : json["id"],
        text: json["text"] == null ? null : json["text"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": text == null ? null : text,
      };
}
