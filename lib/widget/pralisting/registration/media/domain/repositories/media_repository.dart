import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/models/media_model.dart';
import 'package:meta/meta.dart';

abstract class MediaRepository {
  Future<Either<Failure, MediaModel>> getMedia({@required String uuid});
  Future<Either<Failure, PralistingMetaModel>> updateMedia({
    @required String uuid,
    @required dynamic body,
  });
}
