import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/models/media_model.dart';
import 'package:santaraapp/widget/pralisting/registration/media/domain/repositories/media_repository.dart';

class GetMedia implements UseCase<MediaModel, String> {
  final MediaRepository repository;
  GetMedia(this.repository);

  @override
  Future<Either<Failure, MediaModel>> call(String uuid) async {
    return await repository.getMedia(uuid: uuid);
  }
}

class UpdateMedia implements UseCase<PralistingMetaModel, dynamic> {
  final MediaRepository repository;
  UpdateMedia(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateMedia(
      body: params['body'],
      uuid: params['uuid'],
    );
  }
}
