import 'package:santaraapp/widget/pralisting/registration/media/data/datasources/media_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/repositories/media_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/media/domain/repositories/media_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';
import '../../../../injector_container.dart';
import 'domain/usecases/media_usecases.dart';

void initMediaInjector() {
  sl.registerFactory(
    () => MediaFormBloc(
      getMedia: sl(),
      updateMedia: sl(),
    ),
  );

  sl.registerLazySingleton<MediaRepository>(
    () => MediaRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<MediaRemoteDataSources>(
    () => MediaRemoteDataSourcesImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetMedia(sl()));
  sl.registerLazySingleton(() => UpdateMedia(sl()));
}
