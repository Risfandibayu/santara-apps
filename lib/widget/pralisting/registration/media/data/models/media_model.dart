// To parse this JSON data, do
//
//     final mediaModel = mediaModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

MediaModel mediaModelFromJson(String str) =>
    MediaModel.fromJson(json.decode(str));

String mediaModelToJson(MediaModel data) => json.encode(data.toJson());

class MediaModel {
  MediaModel({
    this.cover,
    this.oldLocation,
    this.newLocation,
    this.documentation,
    this.team,
    this.logo,
    this.instagram,
    this.linkInstagram,
    this.facebook,
    this.linkFacebook,
    this.website,
    this.linkWebsite,
    this.review,
    this.video,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  List<ImageFile> cover;
  List<ImageFile> oldLocation;
  List<ImageFile> newLocation;
  List<ImageFile> documentation;
  List<ImageFile> team;
  ImageFile logo;
  ImageFile instagram;
  String linkInstagram;
  ImageFile facebook;
  String linkFacebook;
  ImageFile website;
  String linkWebsite;
  ImageFile review;
  String video;

  Submission submission;
  bool isComplete;
  bool isClean;

  factory MediaModel.fromJson(Map<String, dynamic> json) => MediaModel(
        cover: json["cover"] == null
            ? null
            : List<ImageFile>.from(
                json["cover"].map((x) => ImageFile.fromJson(x))),
        oldLocation: json["old_location"] == null
            ? null
            : List<ImageFile>.from(
                json["old_location"].map((x) => ImageFile.fromJson(x))),
        newLocation: json["new_location"] == null
            ? null
            : List<ImageFile>.from(
                json["new_location"].map((x) => ImageFile.fromJson(x))),
        documentation: json["documentation"] == null
            ? null
            : List<ImageFile>.from(
                json["documentation"].map((x) => ImageFile.fromJson(x))),
        team: json["team"] == null
            ? null
            : List<ImageFile>.from(
                json["team"].map((x) => ImageFile.fromJson(x))),
        logo: json["logo"] == null ? null : ImageFile.fromJson(json["logo"]),
        instagram: json["instagram"] == null
            ? null
            : ImageFile.fromJson(json["instagram"]),
        linkInstagram:
            json["link_instagram"] == null ? null : json["link_instagram"],
        facebook: json["facebook"] == null
            ? null
            : ImageFile.fromJson(json["facebook"]),
        linkFacebook:
            json["link_facebook"] == null ? null : json["link_facebook"],
        website: json["website"] == null
            ? null
            : ImageFile.fromJson(json["website"]),
        linkWebsite: json["link_website"] == null ? null : json["link_website"],
        review:
            json["review"] == null ? null : ImageFile.fromJson(json["review"]),
        video: json["video"] == null ? null : json["video"],
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "cover": cover == null
            ? null
            : List<dynamic>.from(cover.map((x) => x.toJson())),
        "old_location": oldLocation == null
            ? null
            : List<dynamic>.from(oldLocation.map((x) => x.toJson())),
        "new_location": newLocation == null
            ? null
            : List<dynamic>.from(newLocation.map((x) => x.toJson())),
        "documentation": documentation == null
            ? null
            : List<dynamic>.from(documentation.map((x) => x.toJson())),
        "team": team == null
            ? null
            : List<dynamic>.from(team.map((x) => x.toJson())),
        "logo": logo == null ? null : logo.toJson(),
        "instagram": instagram == null ? null : instagram.toJson(),
        "link_instagram": linkInstagram == null ? null : linkInstagram,
        "facebook": facebook == null ? null : facebook.toJson(),
        "link_facebook": linkFacebook == null ? null : linkFacebook,
        "website": website == null ? null : website.toJson(),
        "link_website": linkWebsite == null ? null : linkWebsite,
        "review": review == null ? null : review.toJson(),
        "video": video == null ? null : video,
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class ImageFile {
  ImageFile({
    this.filename,
    this.url,
  });

  String filename;
  String url;

  factory ImageFile.fromJson(Map<String, dynamic> json) => ImageFile(
        filename: json["filename"] == null ? null : json["filename"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "filename": filename == null ? null : filename,
        "url": url == null ? null : url,
      };
}
