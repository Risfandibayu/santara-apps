import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/models/media_model.dart';
import 'package:meta/meta.dart';

abstract class MediaRemoteDataSources {
  Future<MediaModel> getMedia({@required String uuid});
  Future<PralistingMetaModel> updateMedia({
    @required String uuid,
    @required dynamic body,
  });
}

class MediaRemoteDataSourcesImpl implements MediaRemoteDataSources {
  final RestClient client;
  MediaRemoteDataSourcesImpl({@required this.client});

  @override
  Future<MediaModel> getMedia({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step9/$uuid");
      if (result.statusCode == 200) {
        return MediaModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateMedia({String uuid, dynamic body}) async {
    try {
      final result = await client.putExternalUrl(
        url: "$apiPralisting/step9/$uuid",
        body: body,
      );
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
