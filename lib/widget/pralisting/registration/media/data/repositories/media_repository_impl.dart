import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/datasources/media_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/models/media_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/media/domain/repositories/media_repository.dart';
import 'package:meta/meta.dart';

class MediaRepositoryImpl implements MediaRepository {
  final NetworkInfo networkInfo;
  final MediaRemoteDataSources remoteDataSource;

  MediaRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, MediaModel>> getMedia({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getMedia(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateMedia({
    String uuid,
    dynamic body,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateMedia(uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
