import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/media/data/models/media_model.dart';
import 'package:santaraapp/widget/pralisting/registration/media/domain/usecases/media_usecases.dart';
import 'package:meta/meta.dart';

class MediaFormBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetMedia getMedia;
  final UpdateMedia updateMedia;

  static List<FileUploadModel> dummies = [];

  final coverPhotos = TextFieldBloc(
    name: 'cover_photos',
    validators: [PralistingValidationHelper.required],
    extraData: dummies,
  );

  final existingLocationPhotos = TextFieldBloc(
    name: 'existing_location_photos',
    validators: [PralistingValidationHelper.required],
    extraData: dummies,
  );

  final newLocationPhotos = TextFieldBloc(
    name: 'new_location_photos',
    extraData: dummies,
  );

  final documentationPhotos = TextFieldBloc(
    name: 'documentation_photos',
    validators: [PralistingValidationHelper.required],
    extraData: dummies,
  );

  final teamPhotos = TextFieldBloc(
    name: 'team_photos',
    validators: [PralistingValidationHelper.required],
    extraData: dummies,
  );

  final logo = TextFieldBloc(
    name: 'logo',
    validators: [PralistingValidationHelper.required],
    extraData: dummies,
  );

  final instagramSs = TextFieldBloc(
    name: 'instagram_ss',
    validators: [],
    extraData: dummies,
  );

  final instagramLink = TextFieldBloc(
    name: 'link_instagram',
    validators: [
      PralistingValidationHelper.validLink,
    ],
  );

  final facebookSs = TextFieldBloc(
    name: 'facebook_ss',
    validators: [],
    extraData: dummies,
  );

  final facebookLink = TextFieldBloc(
    name: 'link_facebook',
    validators: [
      PralistingValidationHelper.validLink,
    ],
  );

  final websiteSs = TextFieldBloc(
    name: 'website_ss',
    validators: [],
    extraData: dummies,
  );

  final websiteLink = TextFieldBloc(
    name: 'link_website',
    validators: [
      PralistingValidationHelper.validLink,
    ],
  );

  final reviewSs = TextFieldBloc(
    name: 'review_ss',
    validators: [],
    extraData: dummies,
  );

  final businessVideo = TextFieldBloc(
    name: 'video',
    validators: [
      PralistingValidationHelper.validYoutubeLink,
    ],
  );

  MediaFormBloc({
    @required this.getMedia,
    @required this.updateMedia,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      coverPhotos,
      existingLocationPhotos,
      newLocationPhotos,
      documentationPhotos,
      teamPhotos,
      logo,
      instagramSs,
      instagramLink,
      facebookSs,
      facebookLink,
      websiteSs,
      websiteLink,
      reviewSs,
      businessVideo,
    ]);
  }

  postData(Map<String, dynamic> body, {bool isNext}) async {
    try {
      // get cover photos
      emitSubmitting();
      body['cover'] = PralistingHelper.getUploadedFileName(coverPhotos);
      body['old_location'] =
          PralistingHelper.getUploadedFileName(existingLocationPhotos);
      body['new_location'] =
          PralistingHelper.getUploadedFileName(newLocationPhotos);
      body['documentation'] =
          PralistingHelper.getUploadedFileName(documentationPhotos);
      body['team'] = PralistingHelper.getUploadedFileName(teamPhotos);
      body['logo'] = PralistingHelper.getSingleFile(logo);
      body['instagram'] = PralistingHelper.getSingleFile(instagramSs);
      body['facebook'] = PralistingHelper.getSingleFile(facebookSs);
      body['website'] = PralistingHelper.getSingleFile(websiteSs);
      body['review'] = PralistingHelper.getSingleFile(reviewSs);
      var payload = {"uuid": uuid, "body": body};
      var result = await updateMedia(payload);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: data.meta.message,
        );
        emitSuccess(canSubmitAgain: true, successResponse: success);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          printFailure(l);
          emitFailure(failureResponse: failure);
        });
      }
      // logger.i(body);
    } catch (e, stack) {
      santaraLog(e, stack);
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  updateFileValue({
    @required dynamic field,
    @required List<ImageFile> files,
  }) {
    if (files != null && files.length > 0) {
      List<FileUploadModel> uploaded = [];
      files.forEach((f) {
        if (f.url != null && f.filename != null) {
          uploaded.add(FileUploadModel(
            url: f.url,
            filename: f.filename,
          ));
        }
      });

      if (field != null && uploaded != null && uploaded.length > 0) {
        field.updateExtraData(uploaded);
      }
    }
  }

  getData() async {
    try {
      final result = await getMedia(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null && !data.isClean) {
          // Cover Photos
          updateFileValue(field: coverPhotos, files: data.cover);
          // Lokasi Usaha Lama
          updateFileValue(
              field: existingLocationPhotos, files: data.oldLocation);
          // Lokasi Usaha Baru
          updateFileValue(field: newLocationPhotos, files: data.newLocation);
          // Dokumentasi Usaha
          updateFileValue(
              field: documentationPhotos, files: data.documentation);
          // Team
          updateFileValue(field: teamPhotos, files: data.team);
          // Logo Perusahaan
          updateFileValue(field: logo, files: [data.logo]);
          // Instagram
          updateFileValue(field: instagramSs, files: [data.instagram]);
          instagramLink.updateValue(data.linkInstagram);
          // Facebook
          updateFileValue(field: facebookSs, files: [data.facebook]);
          facebookLink.updateValue(data.linkFacebook);
          // Website
          updateFileValue(field: websiteSs, files: [data.website]);
          websiteLink.updateValue(data.linkWebsite);
          // Review
          updateFileValue(field: reviewSs, files: [data.review]);
          // Video
          businessVideo.updateValue(data.video);
        }
      } else {
        emitFailure();
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
    emitLoaded();
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  @override
  void onSubmitting() async {}
}
