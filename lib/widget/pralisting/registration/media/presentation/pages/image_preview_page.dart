import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class ImagePreviewPage extends StatelessWidget {
  final String image;
  final String type; // screenshot / image
  ImagePreviewPage({@required this.image, this.type = 'image'});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Stack(
        children: [
          Container(
            width: double.maxFinite,
            height: double.maxFinite,
            color: Colors.black,
            child: type == 'image'
                ? SantaraCachedImage(
                    image: image,
                    width: double.maxFinite,
                    fit: BoxFit.contain,
                  )
                : Image.asset(
                    "assets/santara/$image",
                    width: double.maxFinite,
                    fit: BoxFit.contain,
                  ),
          ),
          type == 'image'
              ? Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: Sizes.s20, bottom: Sizes.s20),
                    child: FlatButton.icon(
                      color: Color(0xffFF4343),
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      icon: Icon(
                        Icons.delete,
                        color: Colors.white,
                        size: Sizes.s20,
                      ),
                      label: Text(
                        "Hapus Foto",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s12,
                        ),
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
