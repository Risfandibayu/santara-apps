import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/core/widgets/image_upload_field.dart/image_upload_field.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/facebook_field.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/instagram_field.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/review_field.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/url_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/video_field.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/website_field.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/pages/terms_conditions_page.dart';

import '../../../../../../injector_container.dart';

class MediaPage extends StatefulWidget {
  final String uuid;
  MediaPage({@required this.uuid});
  @override
  _MediaPageState createState() => _MediaPageState();
}

class _MediaPageState extends State<MediaPage> {
  MediaFormBloc bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = sl<MediaFormBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Media",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "9 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<MediaFormBloc>();
              return FormBlocListener<MediaFormBloc, FormResult, FormResult>(
                onLoaded: (context, state) {},
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsConditionsPage(
                          uuid: widget.uuid,
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  } else {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<MediaFormBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.white,
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: double.maxFinite,
                                padding: EdgeInsets.all(Sizes.s15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(Sizes.s8),
                                  color: Color(0xffEEF1FF),
                                ),
                                child: Text(
                                  "Kualitas dan kelengkapan foto/video yang baik dapat menjadi bahan pertimbangan para investor dalam menentukan pengajuan bisnis yang Anda miliki.",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.s12,
                                  ),
                                ),
                              ),
                              SizedBox(height: Sizes.s40),
                              ImageUploadSubmission(
                                label: "Unggah 2 Foto Cover",
                                description:
                                    "Gunakan foto terbaik yang menggambarkan usaha Anda (Foto produk atau Foto Kegiatan).\nBesar file maksimal 15MB",
                                fieldBloc: bloc.coverPhotos,
                                maxFiles: 2,
                                minFiles: 2,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                                files: [],
                              ),
                              ImageUploadSubmission(
                                label: "Unggah Foto Lokasi Usaha Lama (2 Foto)",
                                description:
                                    "Unggah foto lokasi usaha yang sudah berjalan\n\nBesar file maksimal 15MB",
                                fieldBloc: bloc.existingLocationPhotos,
                                maxFiles: 2,
                                minFiles: 2,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                              ),
                              ImageUploadSubmission(
                                label: "Unggah Foto Lokasi Usaha Baru (2 Foto)",
                                description:
                                    "Unggah foto lokasi usaha baru\n\nBesar file maksimal 15MB",
                                fieldBloc: bloc.newLocationPhotos,
                                maxFiles: 2,
                                minFiles: 2,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                              ),
                              ImageUploadSubmission(
                                label:
                                    "Unggah Foto Dokumentasi Usaha (Minimal 5 Foto)",
                                description:
                                    "Unggah foto dokumentasi usaha Anda\n\nBesar file maksimal 15MB",
                                fieldBloc: bloc.documentationPhotos,
                                maxFiles: 10,
                                minFiles: 5,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                              ),
                              ImageUploadSubmission(
                                label: "Unggah Foto Tim (Minimal 2 Foto)",
                                description:
                                    "Unggah foto tim atau kegiatan tim usaha Anda.\n\nBesar file maksimal 15MB",
                                fieldBloc: bloc.teamPhotos,
                                maxFiles: 5,
                                minFiles: 2,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                              ),
                              ImageUploadSubmission(
                                label: "Unggah Logo Perusahaan",
                                description:
                                    "Unggah logo perusahaan Anda\n\nBesar file maksimal 15MB",
                                fieldBloc: bloc.logo,
                                maxFiles: 1,
                                minFiles: 1,
                                maxFileSize: 15,
                                name: 'image',
                                path: '$apiFileUpload/upload',
                                location: 'pralisting/emitens_pictures/',
                                type: 'images',
                                onTapInfo: null,
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.zero,
                                title: Text(
                                  "Unggah Foto Screenshot Sosial Media & Review (Opsional)",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: FontSize.s14,
                                  ),
                                ),
                                subtitle: Text(
                                  "Pilih Sosial Media",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.s12,
                                  ),
                                ),
                              ),
                              InstagramInputField(bloc: bloc),
                              FacebookInputField(bloc: bloc),
                              WebsiteInputField(bloc: bloc),
                              ReviewInputField(bloc: bloc),
                              VideoInputField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
