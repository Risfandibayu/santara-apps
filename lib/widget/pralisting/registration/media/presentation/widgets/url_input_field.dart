import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/image_preview_page.dart';

class UrlInputField extends StatelessWidget {
  final String label;
  final String urlExample;
  final String hintText;
  final TextFieldBloc bloc;
  UrlInputField({this.label, this.urlExample, this.hintText, @required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldBlocBuilder(
            textFieldBloc: bloc,
            decoration: InputDecoration(
              border: PralistingInputDecoration.outlineInputBorder,
              contentPadding: EdgeInsets.all(Sizes.s15),
              isDense: true,
              errorMaxLines: 5,
              labelText: "$label",
              labelStyle: PralistingInputDecoration.labelTextStyle,
              hintText: hintText,
              hintStyle: PralistingInputDecoration.hintTextStyle(false),
              floatingLabelBehavior: FloatingLabelBehavior.always,
            ),
            maxLines: 1,
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImagePreviewPage(
                    image: urlExample,
                    type: 'screenshot',
                  ),
                ),
              );
            },
            child: Container(
              height: Sizes.s40,
              child: Text(
                "Lihat Contoh Screenshot",
                style: TextStyle(
                  fontSize: FontSize.s12,
                  color: Color(0xff218196),
                  decoration: TextDecoration.underline,
                  decorationStyle: TextDecorationStyle.solid,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
