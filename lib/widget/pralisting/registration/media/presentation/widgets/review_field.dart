import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/image_preview_page.dart';

class ReviewInputField extends StatelessWidget {
  final MediaFormBloc bloc;
  ReviewInputField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: FileUploadSubmission(
              label: "Review",
              allowedTypes: ['jpg', 'jpeg', 'png'],
              fieldBloc: bloc.reviewSs,
              maxFiles: 1,
                              minFiles: 1,
              name: 'image',
              path: '$apiFileUpload/upload',
              location: "pralisting/business_documents/",
              type: "images",
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ImagePreviewPage(
                    image: "review_ss.jpg",
                    type: 'screenshot',
                  ),
                ),
              );
            },
            child: Container(
              height: Sizes.s40,
              child: Text(
                "Lihat Contoh Screenshot",
                style: TextStyle(
                  fontSize: FontSize.s12,
                  color: Color(0xff218196),
                  decoration: TextDecoration.underline,
                  decorationStyle: TextDecorationStyle.solid,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
