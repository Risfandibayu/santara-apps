import 'package:flutter/material.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/url_input_field.dart';

class InstagramInputField extends StatelessWidget {
  final MediaFormBloc bloc;
  InstagramInputField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FileUploadSubmission(
            label: "Instagram",
            allowedTypes: ['jpg', 'jpeg', 'png'],
            fieldBloc: bloc.instagramSs,
            maxFiles: 1,
                              minFiles: 1,
            name: 'image',
            path: 'https://tulabi.com:3701/upload',
            location: "pralisting/business_documents/",
            type: "images",
            files: [],
          ),
          SizedBox(height: Sizes.s10),
          UrlInputField(
            label: "Link Instagram",
            bloc: bloc.instagramLink,
            urlExample: "instagram_ss.jpg",
            hintText: "https://www.instagram.com/santaracoid/",
          ),
        ],
      ),
    );
  }
}
