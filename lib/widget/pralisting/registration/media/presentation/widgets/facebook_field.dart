import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/widgets/url_input_field.dart';

class FacebookInputField extends StatelessWidget {
  final MediaFormBloc bloc;
  FacebookInputField({@required this.bloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FileUploadSubmission(
            label: "Facebook",
            allowedTypes: ['jpg', 'jpeg', 'png'],
            fieldBloc: bloc.facebookSs,
            maxFiles: 1,
                              minFiles: 1,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: "images",
          ),
          SizedBox(height: Sizes.s10),
          UrlInputField(
            label: "Link Facebook",
            bloc: bloc.facebookLink,
            urlExample: "facebook_ss.jpg",
            hintText: "https://www.facebook.com/santaracoid/",
          ),
        ],
      ),
    );
  }
}
