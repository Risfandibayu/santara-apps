import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/blocs/media_bloc.dart';

class VideoInputField extends StatelessWidget {
  final MediaFormBloc bloc;
  VideoInputField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            "Video Tentang Usaha Anda",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: FontSize.s14,
            ),
          ),
          subtitle: Text(
            "Sematkan link video youtube mengenai usaha Anda",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: FontSize.s12,
            ),
          ),
        ),
        TextFieldBlocBuilder(
          textFieldBloc: bloc.businessVideo,
          decoration: InputDecoration(
            border: PralistingInputDecoration.outlineInputBorder,
            contentPadding: EdgeInsets.all(Sizes.s15),
            isDense: true,
            errorMaxLines: 5,
            labelText: "",
            labelStyle: PralistingInputDecoration.labelTextStyle,
            hintText: "https://www.youtube.com/watch?v=2GRtFC8nTag&list=RDDCCJCILiX3o&index=5",
            hintStyle: PralistingInputDecoration.hintTextStyle(false),
            floatingLabelBehavior: FloatingLabelBehavior.always,
          ),
          maxLines: 1,
        ),
      ],
    );
  }
}
