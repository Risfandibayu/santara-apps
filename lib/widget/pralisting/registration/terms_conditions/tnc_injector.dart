import 'package:santaraapp/widget/pralisting/registration/terms_conditions/data/repositories/tnc_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/domain/repositories/tnc_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/domain/usecases/tnc_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_bloc.dart';
import '../../../../injector_container.dart';
import 'data/datasources/tnc_remote_data_sources.dart';

void initTncPralistingInjector() {
  sl.registerFactory(
    () => PralistingTncBloc(tncScore: sl()),
  );

  sl.registerLazySingleton<TncRemoteDataSources>(
    () => TncRemoteDataSourcesImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<TncRepository>(
    () => TncRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton(() => TncScore(sl()));
}
