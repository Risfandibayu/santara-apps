import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class RegisterResultBottomSheet extends StatelessWidget {
  final bool isSuccess;
  final Function onTap;
  RegisterResultBottomSheet({
    this.isSuccess,
    this.onTap,
  });

  static void show(
    BuildContext context, {
    Key key,
    bool isSuccess,
    Function onTap,
  }) =>
      showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(5.0)),
        ),
        context: context,
        builder: (context) {
          return RegisterResultBottomSheet(
            isSuccess: isSuccess,
            onTap: onTap,
          );
        },
      ).then((_) => FocusScope.of(context).requestFocus(FocusNode()));

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Container(
        padding: EdgeInsets.all(Sizes.s20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            isSuccess
                ? Image.asset(
                    "assets/icon/success.png",
                    height: Sizes.s120,
                  )
                : Image.asset(
                    "assets/icon/forbidden.png",
                    height: Sizes.s120,
                  ),
            SizedBox(height: Sizes.s20),
            Text(
              isSuccess ? "Pendaftaran Berhasil!" : "Pendaftaran Gagal!",
              style: TextStyle(
                fontSize: FontSize.s18,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: Sizes.s10),
            Text(
              isSuccess
                  ? "Selamat, bisnis Anda dinyatakan lolos tahap scoring Santara. Selanjutnya Santara akan melakukan review terkait bisnis Anda."
                  : "Maaf, bisnis yang Anda ajukan belum memenuhi standar scoring dari Santara. Silahkan mengajukan kembali dalam waktu 6 (enam) bulan kedepan.",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: Sizes.s40),
            SantaraMainButton(
              title: "Lihat Detail",
              onPressed: onTap,
            )
          ],
        ),
      ),
    );
  }
}
