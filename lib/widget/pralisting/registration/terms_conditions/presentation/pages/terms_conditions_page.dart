import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_page.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_event.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_state.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/widgets/register_result_sheet.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../../injector_container.dart';
import 'information_statement.dart';

class TermsConditionsPage extends StatefulWidget {
  final String uuid;
  TermsConditionsPage({@required this.uuid});
  @override
  _TermsConditionsPageState createState() => _TermsConditionsPageState();
}

class _TermsConditionsPageState extends State<TermsConditionsPage> {
  bool _checked = false;
  PralistingTncBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<PralistingTncBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<PralistingTncBloc>(
        create: (_) => bloc,
        child: BlocListener<PralistingTncBloc, PralistingTncState>(
          listener: (context, state) {
            if (state is PralistingTncLoading) {
              PopupHelper.showLoading(context);
            }

            if (state is PralistingTncError) {
              Navigator.pop(context);
              ToastHelper.showFailureToast(context, state.failure.message);
              if (state.failure.apiStatus == 400) {
                RegisterResultBottomSheet.show(
                  context,
                  isSuccess: false,
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BusinessFiledPage(),
                      ),
                    );
                  },
                );
              }
            }

            if (state is PralistingTncLoaded) {
              Navigator.pop(context);
              ToastHelper.showSuccessToast(context, state.meta.meta.message);
              RegisterResultBottomSheet.show(
                context,
                isSuccess: true,
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BusinessFiledPage(),
                    ),
                  );
                },
              );
            }
          },
          child: BlocBuilder<PralistingTncBloc, PralistingTncState>(
            builder: (context, state) {
              return Container(
                padding: EdgeInsets.all(Sizes.s20),
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.white,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/santara/1.png",
                        height: Sizes.s30,
                      ),
                      SizedBox(height: Sizes.s30),
                      Image.asset(
                        "assets/icon/pra_penawaran.png",
                        height: Sizes.s200,
                      ),
                      SizedBox(height: Sizes.s30),
                      Text(
                        "Pengajuan pendanaan bisnis Anda akan diproses oleh tim Santara minimal 2 minggu setelah anda menyetujui syarat & ketentuan. ",
                        style: TextStyle(
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: Sizes.s20),
                      Text(
                        "Baca",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: FontSize.s14,
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => InformationStatementPage(),
                            ),
                          );
                        },
                        child: Container(
                          height: Sizes.s20,
                          child: Text(
                            "Pernyataan Informasi",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff218196),
                              decoration: TextDecoration.underline,
                              decorationStyle: TextDecorationStyle.solid,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: Sizes.s10),
                      InkWell(
                        onTap: () async {
                          var url =
                              "https://santara.co.id/syarat-ketentuan-penerbit";
                          try {
                            if (await canLaunch(url)) {
                              await launch(url, forceSafariVC: true);
                            } else {
                              ToastHelper.showFailureToast(
                                context,
                                "Tidak dapat membuka url",
                              );
                            }
                          } catch (e, stack) {
                            ToastHelper.showFailureToast(
                              context,
                              "Tidak dapat membuka url",
                            );
                          }
                        },
                        child: Container(
                          height: Sizes.s20,
                          child: Text(
                            "Syarat & Ketentuan Penerbit",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff218196),
                              decoration: TextDecoration.underline,
                              decorationStyle: TextDecorationStyle.solid,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Row(
                        children: [
                          Checkbox(
                            value: _checked,
                            onChanged: (bool val) {
                              setState(() {
                                _checked = _checked ? false : true;
                              });
                            },
                          ),
                          SizedBox(width: Sizes.s5),
                          Flexible(
                            child: RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text:
                                        "Dengan ini saya telah membaca dan menyetujui pernyataan informasi serta syarat & ketentuan yang diberlakukan oleh ",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,

                                      // fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "SANTARA",
                                    style: TextStyle(
                                      fontSize: FontSize.s12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: Sizes.s30),
                      Container(
                        height: Sizes.s45,
                        child: SantaraMainButton(
                          isActive: _checked,
                          title: "Daftarkan Bisnis",
                          onPressed: () {
                            if (_checked) {
                              bloc
                                ..add(
                                  LoadPralistingTnc(
                                    uuid: widget.uuid,
                                    tnc: 1,
                                  ),
                                );
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
