import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class InformationStatementPage extends StatefulWidget {
  @override
  _InformationStatementPageState createState() =>
      _InformationStatementPageState();
}

class _InformationStatementPageState extends State<InformationStatementPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(Sizes.s20),
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/santara/1.png",
                height: Sizes.s30,
              ),
              SizedBox(height: Sizes.s50),
              Container(
                padding: EdgeInsets.all(Sizes.s20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Sizes.s5),
                  border: Border.all(
                    width: 1.0,
                    color: Colors.grey,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        "Pernyataan Informasi",
                        style: TextStyle(
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(height: Sizes.s40),
                    Text(
                      "1. Dengan ini saya menyatakan, bahwa informasi yang saya sampaikan didalam form daftarkan bisnis ini adalah sesuai dengan keadaan yang sebenar-benarnya.",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(height: Sizes.s20),
                    Text(
                      "2. Bahwa saya menerima setiap hasil yang dikeluarkan oleh menu daftarkan bisnis ini dengan penuh kesadaran.",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(height: Sizes.s20),
                    Text(
                      "3. Dengan disetujuinya surat pernyataan ini, maka saya tunduk pada ketentuan internal perihal seleksi calon penerbit yang dijalankan oleh platform Santara",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(height: Sizes.s50),
                  ],
                ),
              ),
              SizedBox(height: Sizes.s20),
              Container(
                height: Sizes.s45,
                child: SantaraMainButton(
                  title: "Tutup",
                  onPressed: () => Navigator.pop(context),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
