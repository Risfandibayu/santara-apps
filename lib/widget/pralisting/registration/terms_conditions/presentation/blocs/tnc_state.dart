import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';

abstract class PralistingTncState extends Equatable {}

class PralistingTncUninitialized extends PralistingTncState {
  @override
  List<Object> get props => [];
}

class PralistingTncLoading extends PralistingTncState {
  @override
  List<Object> get props => [];
}

class PralistingTncError extends PralistingTncState {
  final Failure failure;
  PralistingTncError({@required this.failure});

  @override
  List<Object> get props => [failure];
}

class PralistingTncLoaded extends PralistingTncState {
  final PralistingMetaModel meta;
  PralistingTncLoaded({@required this.meta});

  @override
  List<Object> get props => [meta];
}
