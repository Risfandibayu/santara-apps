import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/domain/usecases/tnc_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_event.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/blocs/tnc_state.dart';
import 'package:meta/meta.dart';

class PralistingTncBloc extends Bloc<PralistingTncEvent, PralistingTncState> {
  PralistingTncBloc({
    @required TncScore tncScore,
  })  : assert(
          tncScore != null,
        ),
        _tncScore = tncScore,
        super(PralistingTncUninitialized());

  final TncScore _tncScore;

  @override
  Stream<PralistingTncState> mapEventToState(
    PralistingTncEvent event,
  ) async* {
    if (event is LoadPralistingTnc) {
      yield* _mapLoadPralistingTncToState(event, state);
    }
  }

  Stream<PralistingTncState> _mapLoadPralistingTncToState(
    LoadPralistingTnc event,
    PralistingTncState state,
  ) async* {
    try {
      yield PralistingTncLoading();
      var params = {
        "uuid": event.uuid,
        "tnc": 1,
      };
      final result = await _tncScore(params);
      yield* result.fold(
        (failure) async* {
          printFailure(failure);
          yield PralistingTncError(failure: failure);
        },
        (tnc) async* {
          yield PralistingTncLoaded(meta: tnc);
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield PralistingTncError(
        failure: Failure(
          type: FailureType.localError,
          message: e,
        ),
      );
    }
  }
}
