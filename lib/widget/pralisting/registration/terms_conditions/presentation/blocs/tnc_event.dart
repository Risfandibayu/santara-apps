import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PralistingTncEvent extends Equatable {}

class LoadPralistingTnc extends PralistingTncEvent {
  final String uuid;
  final int tnc;
  LoadPralistingTnc({@required this.uuid, @required this.tnc});

  @override
  List<Object> get props => [uuid, tnc];
}

