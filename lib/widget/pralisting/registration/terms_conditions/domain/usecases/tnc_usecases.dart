import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/domain/repositories/tnc_repository.dart';

class TncScore implements UseCase<PralistingMetaModel, dynamic> {
  final TncRepository repository;
  TncScore(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.tncScore(uuid: params["uuid"], tnc: params["tnc"]);
  }
}
