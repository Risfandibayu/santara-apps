import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';

abstract class TncRepository {
  Future<Either<Failure, PralistingMetaModel>> tncScore({
    @required String uuid,
    @required int tnc,
  });
}
