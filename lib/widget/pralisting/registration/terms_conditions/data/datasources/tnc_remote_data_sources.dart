import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';

abstract class TncRemoteDataSources {
  Future<PralistingMetaModel> tncScore(
      {@required String uuid, @required int tnc});
}

class TncRemoteDataSourcesImpl implements TncRemoteDataSources {
  final RestClient client;
  TncRemoteDataSourcesImpl({@required this.client});

  @override
  Future<PralistingMetaModel> tncScore({String uuid, int tnc}) async {
    try {
      final result = await client.getExternalUrl(
          url: "$apiPralisting/tnc-score/$uuid?tnc=$tnc");
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
