import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/data/datasources/tnc_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/domain/repositories/tnc_repository.dart';
import 'package:meta/meta.dart';

class TncRepositoryImpl implements TncRepository {
  final NetworkInfo networkInfo;
  final TncRemoteDataSources remoteDataSource;
  TncRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, PralistingMetaModel>> tncScore(
      {String uuid, int tnc}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.tncScore(uuid: uuid, tnc: tnc);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
