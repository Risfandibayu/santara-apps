import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/datasources/financial_info_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/models/financial_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/domain/repositories/financial_info_repository.dart';
import 'package:meta/meta.dart';

class FinancialInfoRepositoryImpl implements FinancialInfoRepository {
  final NetworkInfo networkInfo;
  final FinancialInfoRemoteDataSource remoteDataSource;

  FinancialInfoRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, DateTime>> getCurrentTime() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getCurrentTime();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, FinancialInfoModel>> getFinancialInfo(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getFinancialInfo(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateFinancialInfo({
    String uuid,
    dynamic body,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data =
            await remoteDataSource.updateFinancialInfo(uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> deleteBalanceInput(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.deleteBalanceInput(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> deleteProfitLossInput(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.deleteProfitLossInput(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, EmitenBalanceSheets>> getBalance({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBalance(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateBalance(
      {String uuid, dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data =
            await remoteDataSource.updateBalance(uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, EmitenProfitLoss>> getProfitLoss({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getProfitLoss(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateProfitLoss(
      {String uuid, dynamic body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data =
            await remoteDataSource.updateProfitLoss(uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateReaderNeraca(
      {String uuid, body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data =
            await remoteDataSource.updateReaderNeraca(uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateReaderProfitLoss(
      {String uuid, body}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateReaderProfitLoss(
            uuid: uuid, body: body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
