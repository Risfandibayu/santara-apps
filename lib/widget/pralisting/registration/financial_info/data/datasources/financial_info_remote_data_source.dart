import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/models/financial_info_model.dart';

abstract class FinancialInfoRemoteDataSource {
  Future<DateTime> getCurrentTime();
  Future<FinancialInfoModel> getFinancialInfo({@required String uuid});
  Future<PralistingMetaModel> updateFinancialInfo({
    @required String uuid,
    @required dynamic body,
  });
  Future<PralistingMetaModel> deleteBalanceInput({@required String uuid});
  Future<PralistingMetaModel> deleteProfitLossInput({@required String uuid});
  Future<EmitenBalanceSheets> getBalance({@required String uuid});
  Future<PralistingMetaModel> updateBalance({
    @required String uuid,
    @required dynamic body,
  });
  Future<EmitenProfitLoss> getProfitLoss({@required String uuid});
  Future<PralistingMetaModel> updateProfitLoss({
    @required String uuid,
    @required dynamic body,
  });
  Future<PralistingMetaModel> updateReaderNeraca({
    @required String uuid,
    @required dynamic body,
  });
  Future<PralistingMetaModel> updateReaderProfitLoss({
    @required String uuid,
    @required dynamic body,
  });
}

class FinancialInfoRemoteDataSourceImpl
    implements FinancialInfoRemoteDataSource {
  final RestClient client;
  FinancialInfoRemoteDataSourceImpl({@required this.client});

  @override
  Future<DateTime> getCurrentTime() async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiLocal/traders/ctime");
      if (result.statusCode == 200) {
        DateTime now = DateTime.parse(result.data["time"]);
        return now;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<FinancialInfoModel> getFinancialInfo({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step5/$uuid");
      if (result.statusCode == 200) {
        return FinancialInfoModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateFinancialInfo(
      {String uuid, dynamic body}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/step5/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> deleteBalanceInput({String uuid}) async {
    try {
      final result =
          await client.deleteExternalUrl(url: "$apiPralisting/step5-1/$uuid");
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> deleteProfitLossInput({String uuid}) async {
    try {
      final result =
          await client.deleteExternalUrl(url: "$apiPralisting/step5-2/$uuid");
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<EmitenBalanceSheets> getBalance({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step5-1/$uuid");
      if (result.statusCode == 200) {
        return EmitenBalanceSheets.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateBalance({String uuid, dynamic body}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/step5-1/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<EmitenProfitLoss> getProfitLoss({String uuid}) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step5-2/$uuid");
      if (result.statusCode == 200) {
        return EmitenProfitLoss.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateProfitLoss(
      {String uuid, dynamic body}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/step5-2/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateReaderNeraca({String uuid, body}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/reader-step5-1/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateReaderProfitLoss(
      {String uuid, body}) async {
    try {
      final result = await client.putExternalUrl(
          url: "$apiPralisting/reader-step5-2/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
