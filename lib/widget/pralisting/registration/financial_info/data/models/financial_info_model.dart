// To parse this JSON data, do
//
//     final financialInfoModel = financialInfoModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

FinancialInfoModel financialInfoModelFromJson(String str) =>
    FinancialInfoModel.fromJson(json.decode(str));

String financialInfoModelToJson(FinancialInfoModel data) =>
    json.encode(data.toJson());

class FinancialInfoModel {
  FinancialInfoModel({
    this.financialAspectLabel,
    this.emitenBalanceSheets,
    this.emitenProfitLoss,
    this.posValidationFile,
    this.profitLossMonthly,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  FinancialAspectLabel financialAspectLabel;
  EmitenBalanceSheets emitenBalanceSheets;
  EmitenProfitLoss emitenProfitLoss;
  PosValidationFile posValidationFile;
  PosValidationFile profitLossMonthly;
  Submission submission;
  bool isComplete;
  bool isClean;

  factory FinancialInfoModel.fromJson(Map<String, dynamic> json) =>
      FinancialInfoModel(
        financialAspectLabel: json["financial_aspect_label"] == null
            ? null
            : FinancialAspectLabel.fromJson(json["financial_aspect_label"]),
        emitenBalanceSheets: json["emiten_balance_sheets"] == null
            ? null
            : EmitenBalanceSheets.fromJson(json["emiten_balance_sheets"]),
        emitenProfitLoss: json["emiten_profit_loss"] == null
            ? null
            : EmitenProfitLoss.fromJson(json["emiten_profit_loss"]),
        posValidationFile: json["pos_validation_file"] == null
            ? null
            : PosValidationFile.fromJson(json["pos_validation_file"]),
        profitLossMonthly: json["profit_loss_monthly"] == null
            ? null
            : PosValidationFile.fromJson(json["profit_loss_monthly"]),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "financial_aspect_label":
            financialAspectLabel == null ? null : financialAspectLabel.toJson(),
        "emiten_balance_sheets":
            emitenBalanceSheets == null ? null : emitenBalanceSheets.toJson(),
        "emiten_profit_loss":
            emitenProfitLoss == null ? null : emitenProfitLoss.toJson(),
        "pos_validation_file":
            posValidationFile == null ? null : posValidationFile.toJson(),
        "profit_loss_monthly":
            profitLossMonthly == null ? null : profitLossMonthly.toJson(),
        "submission": submission == null ? null : submission.toJson(),
        "is_complete": isComplete == null ? null : isComplete,
        "is_clean": isClean == null ? null : isClean,
      };
}

class EmitenBalanceSheets {
  EmitenBalanceSheets({
    this.kas,
    this.piutang,
    this.stok,
    this.uangMuka,
    this.biayaMuka,
    this.pajakMuka,
    this.aktivaLancar,
    this.tanah,
    this.bangunan,
    this.peralatan,
    this.kendaraan,
    this.aktivaTetap,
    this.penyusutan,
    this.aktivaLain,
    this.hutang,
    this.hutangPajak,
    this.hutangBankPendek,
    this.biaya,
    this.kewajibanLain,
    this.hutangBankPanjang,
    this.hutangKetigaPanjang,
    this.hutangSaham,
    this.kewajibanPanjangLain,
    this.modal,
    this.labaDitahan,
    this.ekuitasLain,
    this.desc,
  });

  int kas;
  int piutang;
  int stok;
  int uangMuka;
  int biayaMuka;
  int pajakMuka;
  int aktivaLancar;
  int tanah;
  int bangunan;
  int peralatan;
  int kendaraan;
  int aktivaTetap;
  int penyusutan;
  int aktivaLain;
  int hutang;
  int hutangPajak;
  int hutangBankPendek;
  int biaya;
  int kewajibanLain;
  int hutangBankPanjang;
  int hutangKetigaPanjang;
  int hutangSaham;
  int kewajibanPanjangLain;
  int modal;
  int labaDitahan;
  int ekuitasLain;
  String desc;

  static removeComma(String value) {
    if (value != null) {
      return value.replaceAll(",", "");
    }
  }

  factory EmitenBalanceSheets.fromJson(Map<String, dynamic> json) =>
      EmitenBalanceSheets(
        kas: json["kas"] == null
            ? null
            : int.parse(removeComma(json["kas"].toString())),
        piutang: json["piutang"] == null
            ? null
            : int.parse(removeComma(json["piutang"].toString())),
        stok: json["stok"] == null
            ? null
            : int.parse(removeComma(json["stok"].toString())),
        uangMuka: json["uang_muka"] == null
            ? null
            : int.parse(removeComma(json["uang_muka"].toString())),
        biayaMuka: json["biaya_muka"] == null
            ? null
            : int.parse(removeComma(json["biaya_muka"].toString())),
        pajakMuka: json["pajak_muka"] == null
            ? null
            : int.parse(removeComma(json["pajak_muka"].toString())),
        aktivaLancar: json["aktiva_lancar"] == null
            ? null
            : int.parse(removeComma(json["aktiva_lancar"].toString())),
        tanah: json["tanah"] == null
            ? null
            : int.parse(removeComma(json["tanah"].toString())),
        bangunan: json["bangunan"] == null
            ? null
            : int.parse(removeComma(json["bangunan"].toString())),
        peralatan: json["peralatan"] == null
            ? null
            : int.parse(removeComma(json["peralatan"].toString())),
        kendaraan: json["kendaraan"] == null
            ? null
            : int.parse(removeComma(json["kendaraan"].toString())),
        aktivaTetap: json["aktiva_tetap"] == null
            ? null
            : int.parse(removeComma(json["aktiva_tetap"].toString())),
        penyusutan: json["penyusutan"] == null
            ? null
            : int.parse(removeComma(json["penyusutan"].toString())),
        aktivaLain: json["aktiva_lain"] == null
            ? null
            : int.parse(removeComma(json["aktiva_lain"].toString())),
        hutang: json["hutang"] == null
            ? null
            : int.parse(removeComma(json["hutang"].toString())),
        hutangPajak: json["hutang_pajak"] == null
            ? null
            : int.parse(removeComma(json["hutang_pajak"].toString())),
        hutangBankPendek: json["hutang_bank_pendek"] == null
            ? null
            : int.parse(removeComma(json["hutang_bank_pendek"].toString())),
        biaya: json["biaya"] == null
            ? null
            : int.parse(removeComma(json["biaya"].toString())),
        kewajibanLain: json["kewajiban_lain"] == null
            ? null
            : int.parse(removeComma(json["kewajiban_lain"].toString())),
        hutangBankPanjang: json["hutang_bank_panjang"] == null
            ? null
            : int.parse(removeComma(json["hutang_bank_panjang"].toString())),
        hutangKetigaPanjang: json["hutang_ketiga_panjang"] == null
            ? null
            : int.parse(removeComma(json["hutang_ketiga_panjang"].toString())),
        hutangSaham: json["hutang_saham"] == null
            ? null
            : int.parse(removeComma(json["hutang_saham"].toString())),
        kewajibanPanjangLain: json["kewajiban_panjang_lain"] == null
            ? null
            : int.parse(removeComma(json["kewajiban_panjang_lain"].toString())),
        modal: json["modal"] == null
            ? null
            : int.parse(removeComma(json["modal"].toString())),
        labaDitahan: json["laba_ditahan"] == null
            ? null
            : int.parse(removeComma(json["laba_ditahan"].toString())),
        ekuitasLain: json["ekuitas_lain"] == null
            ? null
            : int.parse(removeComma(json["ekuitas_lain"].toString())),
        desc: json["desc"] == null ? null : json["desc"],
      );

  Map<String, dynamic> toJson() => {
        "kas": kas == null ? null : kas,
        "piutang": piutang == null ? null : piutang,
        "stok": stok == null ? null : stok,
        "uang_muka": uangMuka == null ? null : uangMuka,
        "biaya_muka": biayaMuka == null ? null : biayaMuka,
        "pajak_muka": pajakMuka == null ? null : pajakMuka,
        "aktiva_lancar": aktivaLancar == null ? null : aktivaLancar,
        "tanah": tanah == null ? null : tanah,
        "bangunan": bangunan == null ? null : bangunan,
        "peralatan": peralatan == null ? null : peralatan,
        "kendaraan": kendaraan == null ? null : kendaraan,
        "aktiva_tetap": aktivaTetap == null ? null : aktivaTetap,
        "penyusutan": penyusutan == null ? null : penyusutan,
        "aktiva_lain": aktivaLain == null ? null : aktivaLain,
        "hutang": hutang == null ? null : hutang,
        "hutang_pajak": hutangPajak == null ? null : hutangPajak,
        "hutang_bank_pendek":
            hutangBankPendek == null ? null : hutangBankPendek,
        "biaya": biaya == null ? null : biaya,
        "kewajiban_lain": kewajibanLain == null ? null : kewajibanLain,
        "hutang_bank_panjang":
            hutangBankPanjang == null ? null : hutangBankPanjang,
        "hutang_ketiga_panjang":
            hutangKetigaPanjang == null ? null : hutangKetigaPanjang,
        "hutang_saham": hutangSaham == null ? null : hutangSaham,
        "kewajiban_panjang_lain":
            kewajibanPanjangLain == null ? null : kewajibanPanjangLain,
        "modal": modal == null ? null : modal,
        "laba_ditahan": labaDitahan == null ? null : labaDitahan,
        "ekuitas_lain": ekuitasLain == null ? null : ekuitasLain,
        "desc": desc == null ? null : desc,
      };
}

class EmitenProfitLoss {
  EmitenProfitLoss({
    this.profitLoss,
    this.desc,
  });

  List<Map<String, int>> profitLoss;
  String desc;

  factory EmitenProfitLoss.fromJson(Map<String, dynamic> json) =>
      EmitenProfitLoss(
        profitLoss: json["profit_loss"] == null
            ? null
            : List<Map<String, int>>.from(json["profit_loss"].map(
                (x) => Map.from(x).map((k, v) => MapEntry<String, int>(k, v)))),
        desc: json["desc"] == null ? null : json["desc"],
      );

  Map<String, dynamic> toJson() => {
        "profit_loss": profitLoss == null
            ? null
            : List<dynamic>.from(profitLoss.map((x) =>
                Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))),
        "desc": desc == null ? null : desc,
      };
}

class FinancialAspectLabel {
  FinancialAspectLabel({
    this.id,
    this.value,
  });

  int id;
  String value;

  factory FinancialAspectLabel.fromJson(Map<String, dynamic> json) =>
      FinancialAspectLabel(
        id: json["id"] == null ? null : json["id"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "value": value == null ? null : value,
      };
}

class PosValidationFile {
  PosValidationFile({
    this.filename,
    this.url,
  });

  String filename;
  String url;

  factory PosValidationFile.fromJson(Map<String, dynamic> json) =>
      PosValidationFile(
        filename: json["filename"] == null ? null : json["filename"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "filename": filename == null ? null : filename,
        "url": url == null ? null : url,
      };
}
