import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';

class UploadFieldBottomSheet extends StatelessWidget {
  final FinancialInfoBloc bloc;
  final SingleFieldBloc fieldBloc; // file neraca / file laba rugi
  final String title;
  final String subtitle;
  final String downloadBtn;
  final String uploadLabel;
  final String templateUrl;

  UploadFieldBottomSheet({
    @required this.bloc,
    @required this.fieldBloc,
    this.title,
    this.subtitle,
    this.downloadBtn,
    this.uploadLabel,
    @required this.templateUrl,
  });

  static show(
    BuildContext context, {
    Key key,
    FinancialInfoBloc bloc,
    @required SingleFieldBloc fieldBloc,
    String title,
    String subtitle,
    String downloadBtn,
    String uploadLabel,
    String templateUrl,
  }) async =>
      await showModalBottomSheet(
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(5.0)),
        ),
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: UploadFieldBottomSheet(
              title: title,
              subtitle: subtitle,
              downloadBtn: downloadBtn,
              uploadLabel: uploadLabel,
              bloc: bloc,
              fieldBloc: fieldBloc,
              templateUrl: templateUrl,
            ),
          );
        },
      ).then((_) async {
        FocusScope.of(context).requestFocus(FocusNode());
        return fieldBloc.state.extraData != null &&
            fieldBloc.state.extraData.length > 0;
      });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(Sizes.s20, 0, Sizes.s20, Sizes.s20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: Transform.translate(
              offset: Offset(Sizes.s25, 0),
              child: Container(
                // color: Colors.yellow,
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          ),
          Text(
            "$title",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: FontSize.s14,
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "$subtitle",
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w300,
            ),
          ),
          SizedBox(height: Sizes.s10),
          FlatButton.icon(
            padding: EdgeInsets.only(
              top: Sizes.s5,
              bottom: Sizes.s5,
              right: Sizes.s5,
            ),
            onPressed: () async => await bloc.openWebUrl(templateUrl),
            icon: Icon(
              Icons.download_sharp,
              color: Color(0xff218196),
            ),
            label: Text(
              "$downloadBtn",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s12,
                color: Color(0xff218196),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Sizes.s10, bottom: Sizes.s20),
            height: 1,
            color: Colors.grey[300],
          ),
          Text(
            "$uploadLabel",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: FontSize.s14,
            ),
          ),
          SizedBox(height: Sizes.s10),
          FileUploadSubmission(
            label: "",
            hintText: "",
            allowedTypes: ['xlsx', 'xls'],
            fieldBloc: fieldBloc,
            maxFiles: 1,
            minFiles: 1,
            name: 'image',
            path: '$apiFileUpload/upload',
            location: "pralisting/business_documents/",
            type: "excel",
          ),
        ],
      ),
    );
  }
}
