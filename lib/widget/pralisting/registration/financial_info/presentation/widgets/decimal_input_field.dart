import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/helpers/CurrencyFormat.dart';
import 'package:santaraapp/helpers/NumberTextInputFormat.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';

class BalanceDecimalInputField extends StatelessWidget {
  final String label;
  final String hintText;
  final SingleFieldBloc bloc;
  final bool isNegative;

  BalanceDecimalInputField({
    this.label,
    this.hintText = "",
    @required this.bloc,
    this.isNegative = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      child: TextFieldBlocBuilder(
        textFieldBloc: bloc,
        decoration: InputDecoration(
          border: PralistingInputDecoration.outlineInputBorder,
          contentPadding: EdgeInsets.all(Sizes.s15),
          isDense: true,
          errorMaxLines: 5,
          labelText: "$label",
          hintText: "$hintText",
          labelStyle: PralistingInputDecoration.labelTextStyle,
          hintStyle: PralistingInputDecoration.hintTextStyle(false),
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        inputFormatters: isNegative
            ? [
                FilteringTextInputFormatter.allow(RegExp(r'[\d+\-\.]')),
                NumberTextInputFormatter(),
              ]
            : [
                FilteringTextInputFormatter.digitsOnly,
                CurrencyFormat(),
              ],
        keyboardType: TextInputType.number,
        maxLines: 1,
      ),
    );
  }
}
