import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class FinancialBottomSheetConfirmation extends StatelessWidget {
  final String title;
  final String description;
  final Widget actionButton;

  FinancialBottomSheetConfirmation({
    @required this.title,
    @required this.description,
    @required this.actionButton,
  });

  static void show(
    BuildContext context, {
    Key key,
    String title,
    String description,
    Widget actionButton,
  }) =>
      showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(5.0)),
        ),
        context: context,
        builder: (context) {
          return FinancialBottomSheetConfirmation(
            title: title,
            description: description,
            actionButton: actionButton,
          );
        },
      ).then((_) => FocusScope.of(context).requestFocus(FocusNode()));

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Sizes.s20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: Sizes.s20),
          Text(
            "$title",
            style: TextStyle(
              fontSize: FontSize.s20,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "$description",
            style: TextStyle(
              fontSize: FontSize.s20,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: Sizes.s40),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: SantaraOutlineButton(
                  title: "Kembali",
                  onPressed: () => Navigator.pop(context),
                ),
              ),
              SizedBox(width: Sizes.s10),
              Expanded(
                flex: 1,
                child: actionButton,
              ),
            ],
          )
        ],
      ),
    );
  }
}
