import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/balance_input_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/profit_loss_input_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/input_done_button.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/upload_field_bottom_sheet.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

import 'bottom_sheet_confirmation.dart';

class ProfitLossField extends StatelessWidget {
  final FinancialInfoBloc bloc;
  ProfitLossField({@required this.bloc});

  Widget _inputProfitLossLabel() {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s40),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          "Input Laba Rugi",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'Nunito',
              fontSize: FontSize.s12,
            ),
            children: <TextSpan>[
              TextSpan(
                text:
                    "Anda bisa input laba rugi secara langsung atau unggah file .xls sesuai template yang tersedia. ",
              ),
              TextSpan(
                text: "*Laporan Keuangan yang disajikan minimal standar ETAP",
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<InputFieldBloc, InputFieldBlocState>(
      bloc: bloc.lossProfitInput,
      builder: (context, state) {
        return Container(
          // margin: EdgeInsets.only(top: Sizes.s30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _inputProfitLossLabel(),
              SizedBox(height: Sizes.s10),
              state.value != null && state.value == false
                  ? Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: FlatButton(
                            color: Color(0xff6870E1),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(Sizes.s5),
                            ),
                            onPressed: () async {
                              final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ProfitLossInputPage(
                                    uuid: bloc.uuid,
                                  ),
                                ),
                              );

                              if (result != null && result) {
                                bloc.lossProfitInput.updateValue(result);
                                bloc.removeLossProfitFile();
                              }
                            },
                            child: Container(
                              height: Sizes.s45,
                              child: Center(
                                child: Text(
                                  "Input Laba Rugi",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: FontSize.s14,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: Sizes.s15),
                        Expanded(
                          flex: 1,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(Sizes.s5),
                            ),
                            color: Color(0xff6870E1),
                            onPressed: () async {
                              final result = await UploadFieldBottomSheet.show(
                                context,
                                bloc: bloc,
                                fieldBloc: bloc.lossProfitFile,
                                title: "Unggah Laba Rugi",
                                subtitle:
                                    "Dokumen laba rugi yang Anda unggah harus sesuai dengan template yang telah disediakan Santara. Silahkan unduh/ download template di bawah ini.",
                                downloadBtn: "Unduh Template Laba Rugi",
                                uploadLabel: "Unggah Dokumen Laba Rugi",
                                templateUrl:
                                    "https://storage.googleapis.com/asset-santara/santara.co.id/pralisting/static/Template%20Laporan%20Laba%20Rugi.xlsx",
                              );
                              if (result != null && result) {
                                bloc.updateLossProfit();
                                bloc.lossProfitInput.updateValue(result);
                              }
                            },
                            child: Container(
                              height: Sizes.s45,
                              child: Center(
                                child: Text(
                                  "Unggah File .xls",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: FontSize.s14,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  : InputDoneButton(
                      onEdit: () {
                        FinancialBottomSheetConfirmation.show(
                          context,
                          title: "Edit Data",
                          description:
                              "File yang sudah diunggah akan terhapus jika Anda edit data. Yakin untuk edit data?",
                          actionButton: SantaraMainButton(
                            title: "Edit",
                            onPressed: () async {
                              Navigator.pop(context);
                              final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ProfitLossInputPage(
                                    uuid: bloc.uuid,
                                  ),
                                ),
                              );
                              if (result != null && result) {
                                bloc.removeLossProfitFile();
                              }
                            },
                          ),
                        );
                      },
                      onDelete: () {
                        FinancialBottomSheetConfirmation.show(
                          context,
                          title: "Yakin Hapus Laba Rugi?",
                          description:
                              "Data yang dihapus tidak dapat dikembalikan.",
                          actionButton: SantaraMainButton(
                            title: "Hapus",
                            onPressed: () {
                              bloc.removeLossProfit();
                              Navigator.pop(context);
                            },
                          ),
                        );
                      },
                    ),
              !state.isInitial && state.hasError
                  ? Container(
                      margin: EdgeInsets.only(
                        left: Sizes.s15,
                        top: Sizes.s10,
                      ),
                      child: Text(
                        "${state.error}",
                        style: TextStyle(
                          color: Colors.red[700],
                          fontSize: FontSize.s12,
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        );
      },
    );
  }
}
