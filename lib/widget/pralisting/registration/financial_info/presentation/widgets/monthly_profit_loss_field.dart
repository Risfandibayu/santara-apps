import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';

class MonthlyProfitLossField extends StatelessWidget {
  final FinancialInfoBloc bloc;
  MonthlyProfitLossField({@required this.bloc});

  Widget _inputMonthlyProfitLossLabel() {
    return Container(
      // margin: EdgeInsets.only(top: Sizes.s40),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          "Lampiran Laba Rugi bulanan 2 (dua) tahun terakhir",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          "Jika perusahaan belum berusia dua tahun, maka lampirkan laba rugi yang tersedia. Dokumen Laba Rugi Bulanan yang Anda unggah harus sesuai dengan template yang telah disediakan Santara. Silahkan unduh/ download template di bawah ini. ",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _inputMonthlyProfitLossLabel(),
          SizedBox(height: Sizes.s10),
          FlatButton.icon(
            padding: EdgeInsets.only(top: Sizes.s5, bottom: Sizes.s5),
            onPressed: () {
              bloc.openWebUrl(
                "https://storage.googleapis.com/asset-santara/santara.co.id/pralisting/static/Template%20Laba%20Rugi%20Bulanan.xlsx",
              );
            },
            icon: Icon(
              Icons.download_rounded,
              size: FontSize.s20,
              color: Color(0xff218196),
            ),
            label: Text(
              "Unduh Template Laba Rugi Bulanan",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w600,
                color: Color(0xff218196),
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          SizedBox(height: Sizes.s10),
          BlocBuilder(
            bloc: bloc.monthlyProfitLossFile,
            builder: (context, state) {
              // print(">> Monthly Profit State : ${bloc.state}");
              return state.extraData != null
                  ? FileUploadSubmission(
                      label: "",
                      hintText: "",
                      allowedTypes: ['xlsx'],
                      fieldBloc: bloc.monthlyProfitLossFile,
                      maxFiles: 1,
                      minFiles: 1,
                      name: 'image',
                      path: '$apiFileUpload/upload',
                      location: "pralisting/business_documents/",
                      type: "xlsx",
                    )
                  : Container();
            },
          )
        ],
      ),
    );
  }
}
