import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/message_bottom_sheet/message_bottom_sheet.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_info_button.dart';
import 'package:santaraapp/utils/pralisting_input_decoration.dart';
import 'package:santaraapp/utils/sizes.dart';

class NoteInputField extends StatelessWidget {
  final String financialType;
  final TextFieldBloc bloc;
  NoteInputField({
    @required this.financialType,
    @required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Tambahkan Keterangan",
              style: TextStyle(
                fontSize: FontSize.s14,
                fontWeight: FontWeight.w600,
              ),
            ),
            subtitle: Text(
              "Tambahkan keterangan berkaitan dengan data $financialType yang Anda isikan",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          SizedBox(height: Sizes.s20),
          TextFieldBlocBuilder(
            textFieldBloc: bloc,
            decoration: InputDecoration(
              border: PralistingInputDecoration.outlineInputBorder,
              contentPadding: EdgeInsets.all(Sizes.s15),
              isDense: true,
              errorMaxLines: 5,
              labelText: "Keterangan",
              hintText: """Contoh : 
Persedian barang terdiri atas xxx, xxx
Aset tanah merupakan tanah yang berlokasi di xxx""",
              hintStyle: PralistingInputDecoration.hintTextStyle(false),
              labelStyle: PralistingInputDecoration.labelTextStyle,
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: PralistingInfoButton(
                title: "Tambah Keterangan",
                message:
                    """Tambahkan penjelasan atau catatan atas pos-pos pada $financialType. Contoh : Pendapatan th 2020 berasal dari penjualan xxx, xxx dll

Gunakan tombol “Enter” untuk memisahkan antar catatan pos-pos""",
              ),
            ),
            maxLines: 4,
          ),
        ],
      ),
    );
  }
}
