import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/profit_loss_input_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';

class ProfitLossInputCard extends StatelessWidget {
  final int index;
  final ProfitLossInputBloc bloc;
  final AnnualProfitLossBloc fieldBloc;
  final VoidCallback onRemove;

  const ProfitLossInputCard({
    Key key,
    @required this.index,
    @required this.bloc,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
              )
            : Container(),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        Text(
          bloc.annualProfitLoss.state.fieldBlocs[index].year.value == null
              ? "Tahun ${DateFormat('yyyy').format(bloc.currentTime.subtract(Duration(days: 360 * (index + 1))))}"
              : "Tahun ${bloc.annualProfitLoss.state.fieldBlocs[index].year.value}",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        BalanceDecimalInputField(
          label: "Pendapatan",
          bloc: fieldBloc.revenue,
        ),
        BalanceDecimalInputField(
          label: "Biaya Pokok Penjualan (HPP)",
          bloc: fieldBloc.hpp,
        ),
        SizedBox(height: Sizes.s30),
        Text(
          "A) Beban Penjualan",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        BalanceDecimalInputField(
          label: "Marketing",
          bloc: fieldBloc.marketing,
        ),
        BalanceDecimalInputField(
          label: "Bahan Pembantu",
          bloc: fieldBloc.indirectMaterials,
        ),
        BalanceDecimalInputField(
          label: "Biaya Penjualan Lainnya",
          bloc: fieldBloc.otherSellingExpenses,
        ),
        Text(
          "*Seluruh biaya beban penjualan diluar dari field tersedia diatas",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
        SizedBox(height: Sizes.s30),
        Text(
          "B) Beban Umum",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        BalanceDecimalInputField(
          label: "Gaji Karyawan",
          bloc: fieldBloc.employeeSalary,
        ),
        BalanceDecimalInputField(
          label: "Bonus",
          bloc: fieldBloc.bonus,
        ),
        BalanceDecimalInputField(
          label: "Biaya Listrik, Telepon dan Internet",
          bloc: fieldBloc.utilitiesExpenses,
        ),
        BalanceDecimalInputField(
          label: "Biaya Administrasi Kantor",
          bloc: fieldBloc.officeAdministrationFee,
        ),
        BalanceDecimalInputField(
          label: "Biaya Transport dan Akomodasi",
          bloc: fieldBloc.transportationAccommodationCosts,
        ),
        BalanceDecimalInputField(
          label: "Biaya Pemeliharaan dan Perbaikan",
          bloc: fieldBloc.maintenanceCosts,
        ),
        BalanceDecimalInputField(
            label: "Biaya Sewa", bloc: fieldBloc.rentExpense),
        BalanceDecimalInputField(
          label: "Biaya Penyusutan dan Amortisasi",
          bloc: fieldBloc.depreciationAmortization,
        ),
        BalanceDecimalInputField(
          label: "Sumbangan",
          bloc: fieldBloc.donation,
        ),
        BalanceDecimalInputField(
          label: "Biaya Umum dan Administrasi Lain",
          bloc: fieldBloc.generalAdministrationExpense,
        ),
        Text(
          "*Seluruh biaya beban umum diluar dari field tersedia diatas",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s40),
          height: 1,
          color: Colors.grey[300],
          width: double.maxFinite,
        ),
        Text(
          "Pendapatan dan Beban lainnya",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        BalanceDecimalInputField(
          label: "Pendapatan Lain-lain",
          bloc: fieldBloc.otherIncome,
        ),
        BalanceDecimalInputField(
          label: "Beban Lain-lain",
          bloc: fieldBloc.otherExpenses,
        ),
        BalanceDecimalInputField(
          label: "Pajak",
          bloc: fieldBloc.tax,
        ),
        SizedBox(height: Sizes.s20),
      ],
    );
  }
}
