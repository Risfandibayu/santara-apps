import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/dropdown/custom_dropdown_field.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';

class FinancialReportQuisionerField extends StatelessWidget {
  final FinancialInfoBloc bloc;
  FinancialReportQuisionerField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder(
        bloc: bloc,
        builder: (context, state) {
          var fieldState = bloc.financialReportQuisioner.state.extraData;
          return SantaraDropDownField(
            onTap: bloc.financialReportQuisioner.state.items != null &&
                    bloc.financialReportQuisioner.state.items.length > 0
                ? () async {
                    final result = await PralistingHelper.showModal(
                      context,
                      title: "",
                      hintText: "",
                      lists: bloc.financialReportQuisioner.state.items,
                      selected: bloc.financialReportQuisioner.value,
                      showSearch: false,
                    );
                    bloc.financialReportQuisioner.updateValue(result);
                  }
                : null,
            onReload: () {},
            labelText: "Keterandalan dan Kualitas Laporan Keuangan",
            hintText: "Kondisi laporan keuangan bisnis Anda?",
            state: fieldState,
            value: bloc.financialReportQuisioner.value?.name,
            errorText: !bloc.financialReportQuisioner.state.isInitial
                ? bloc.financialReportQuisioner.state.hasError
                    ? bloc.financialReportQuisioner.state.error
                    : null
                : null,
          );
        },
      ),
    );
  }
}
