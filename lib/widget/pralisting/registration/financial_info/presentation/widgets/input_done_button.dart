import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class InputDoneButton extends StatelessWidget {
  final Function onEdit;
  final Function onDelete;

  InputDoneButton({
    @required this.onEdit,
    @required this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xffE1E7FF),
        borderRadius: BorderRadius.circular(8),
      ),
      padding: EdgeInsets.only(
        left: Sizes.s15,
        right: Sizes.s15,
      ),
      child: Row(
        children: [
          Icon(
            Icons.check_circle,
            color: Color(0xff374FC7),
            size: FontSize.s18,
          ),
          SizedBox(width: Sizes.s5),
          Text(
            "Input Selesai",
            style: TextStyle(
              color: Color(0xff374FC7),
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          Spacer(),
          IconButton(
            icon: Icon(
              Icons.edit,
              color: Color(0xff374FC7),
              size: FontSize.s18,
            ),
            onPressed: onEdit,
          ),
          IconButton(
            icon: Icon(
              Icons.delete,
              color: Colors.red[700],
              size: FontSize.s18,
            ),
            onPressed: onDelete,
          ),
        ],
      ),
    );
  }
}
