import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/profit_loss_input_bloc.dart';
import 'profit_loss_input_card.dart';

class ProfitLossInputField extends StatelessWidget {
  final ProfitLossInputBloc bloc;
  ProfitLossInputField({@required this.bloc});

  Widget _profitLossInputWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<AnnualProfitLossBloc>,
            ListFieldBlocState<AnnualProfitLossBloc>>(
          bloc: bloc.annualProfitLoss,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return ProfitLossInputCard(
                    index: i,
                    bloc: bloc,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: null,
                  );
                },
              );
            }
            return Container();
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: _profitLossInputWidget(),
    );
  }
}
