import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/widgets/file_upload_field/file_upload_field.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';

class BankStatementsField extends StatelessWidget {
  final FinancialInfoBloc bloc;
  BankStatementsField({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Lampiran validasi POS atau rekening koran (6 bulan terakhir)",
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          BlocBuilder<TextFieldBloc, TextFieldBlocState>(
              bloc: bloc.bankStatementFile,
              builder: (context, state) {
                return state.extraData != null
                    ? FileUploadSubmission(
                        label: "",
                        hintText: "",
                        allowedTypes: ['pdf', 'jpg', 'png'],
                        fieldBloc: bloc.bankStatementFile,
                        maxFiles: 1,
                        minFiles: 1,
                        name: 'image',
                        path: '$apiFileUpload/upload',
                        location: "pralisting/business_documents/",
                        type: "pdf",
                        annotation: Text(
                          "*Dokumen dengan format PDF, JPG, PNG",
                          style: TextStyle(
                            fontSize: FontSize.s10,
                            fontWeight: FontWeight.w300,
                            color: Color(0xff707070),
                          ),
                        ),
                      )
                    : Container();
              }),
        ],
      ),
    );
  }
}
