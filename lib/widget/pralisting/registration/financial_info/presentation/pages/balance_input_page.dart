import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/balance_input_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/note_input_field.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import '../../../../../../injector_container.dart';

class BalanceInputPage extends StatefulWidget {
  final String uuid;
  BalanceInputPage({@required this.uuid});

  @override
  _BalanceInputPageState createState() => _BalanceInputPageState();
}

class _BalanceInputPageState extends State<BalanceInputPage> {
  BalanceInputBloc bloc;
  @override
  void initState() {
    super.initState();
    bloc = sl<BalanceInputBloc>();
    bloc.uuid = widget.uuid;
    PralistingHelper.initAutoScroll(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Input Neraca",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 1,
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<BalanceInputBloc>();
              return FormBlocListener<BalanceInputBloc, FormResult, FormResult>(
                onLoaded: (context, state) {
                  PralistingHelper.handleScrollValidation(state: state);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pop(context, true);
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.get_error) {
                    ToastHelper.showFailureToast(context, form.message);
                  }

                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                  PralistingHelper.handleScrollValidation(state: state);
                },
                child: BlocBuilder<BalanceInputBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.white,
                        child: SingleChildScrollView(
                          controller: PralistingHelper.controller,
                          scrollDirection: PralistingHelper.scrollDirection,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Aktiva Lancar",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s10),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 1,
                                child: BalanceDecimalInputField(
                                  label: "Kas dan Setara Kas",
                                  bloc: bloc.cashCashEquivalents,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 2,
                                child: BalanceDecimalInputField(
                                  label: "Piutang usaha",
                                  bloc: bloc.accountsReceivable,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 3,
                                child: BalanceDecimalInputField(
                                  label: "Persediaan / Stock",
                                  bloc: bloc.stock,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 4,
                                child: BalanceDecimalInputField(
                                  label: "Uang Muka",
                                  bloc: bloc.downPayment,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 5,
                                child: BalanceDecimalInputField(
                                  label: "Biaya Dibayar Dimuka",
                                  bloc: bloc.prepaidExpenses,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 6,
                                child: BalanceDecimalInputField(
                                  label: "Pajak Dibayar Dimuka",
                                  bloc: bloc.prepaidTaxes,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 7,
                                child: BalanceDecimalInputField(
                                  label: "Aktiva Lancar Lain",
                                  bloc: bloc.otherCurrentAssets,
                                ),
                              ),
                              Text(
                                "*Seluruh biaya aktiva lancar diluar dari field tersedia diatas",
                                style: TextStyle(
                                  fontSize: FontSize.s12,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              SizedBox(height: Sizes.s20),
                              Text(
                                "Aktiva Tetap",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s10),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 8,
                                child: BalanceDecimalInputField(
                                  label: "Tanah",
                                  bloc: bloc.land,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 9,
                                child: BalanceDecimalInputField(
                                  label: "Bangunan",
                                  bloc: bloc.building,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 10,
                                child: BalanceDecimalInputField(
                                  label: "Peralatan",
                                  bloc: bloc.equipment,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 11,
                                child: BalanceDecimalInputField(
                                  label: "Kendaraan",
                                  bloc: bloc.vehicle,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 12,
                                child: BalanceDecimalInputField(
                                  label: "Aktiva Tetap Lainnya",
                                  bloc: bloc.otherFixedAssets,
                                ),
                              ),
                              Text(
                                "*Seluruh biaya aktiva tetap diluar dari field tersedia diatas",
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 13,
                                child: BalanceDecimalInputField(
                                  label: "Akumulasi Penyusutan Aset Tetap",
                                  bloc: bloc.accumulatedDepreciationFixedAssets,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              SizedBox(height: Sizes.s20),
                              // aktiva lainnya
                              Text(
                                "Aktiva Lainnya",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s20),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 14,
                                child: BalanceDecimalInputField(
                                  label:
                                      "*Seluruh biaya diluar dari field tersedia diatas",
                                  bloc: bloc.otherAssets,
                                ),
                              ),
                              Text(
                                "*Seluruh biaya kewajiban lancar diluar dari field tersedia diatas",
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              SizedBox(height: Sizes.s20),
                              // kewajiban lancar
                              Text(
                                "Kewajiban Lancar",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s10),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 15,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Usaha",
                                  bloc: bloc.accountPayable,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 16,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Pajak",
                                  bloc: bloc.taxPayable,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 17,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Bank Jangka Pendek",
                                  bloc: bloc.shortTermBankLoans,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 18,
                                child: BalanceDecimalInputField(
                                  label: "Biaya yang masih harus dibayar",
                                  bloc: bloc.accruedExpense,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 19,
                                child: BalanceDecimalInputField(
                                  label: "Kewajiban Lancar Lainnya",
                                  bloc: bloc.otherCurrentLiabilities,
                                ),
                              ),
                              Text(
                                "*Seluruh biaya kewajiban lancar diluar dari field tersedia diatas",
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              SizedBox(height: Sizes.s20),
                              // kewajiban lancar
                              Text(
                                "Kewajiban Jangka Panjang",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s10),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 20,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Bank Jangka Panjang",
                                  bloc: bloc.longTermBankLoans,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 21,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Pihak Ketiga Jangka Panjang",
                                  bloc: bloc.longTermThirdPartyDebt,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 22,
                                child: BalanceDecimalInputField(
                                  label: "Hutang Kepada Pemegang Saham",
                                  bloc: bloc.debtToShareholders,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 23,
                                child: BalanceDecimalInputField(
                                  label: "Kewajiban Jangka Panjang Lainnya",
                                  bloc: bloc.otherLongTermLiabilities,
                                ),
                              ),
                              Text(
                                "*Seluruh biaya kewajiban lancar diluar dari field tersedia diatas",
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              SizedBox(height: Sizes.s20),
                              // Ekuitas
                              Text(
                                "Ekuitas",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                              SizedBox(height: Sizes.s10),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 24,
                                child: BalanceDecimalInputField(
                                  label: "Modal Disetor",
                                  bloc: bloc.paidInCapital,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 25,
                                child: BalanceDecimalInputField(
                                  label: "Laba Ditahan",
                                  bloc: bloc.retainedEarnings,
                                  isNegative: true,
                                ),
                              ),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 26,
                                child: BalanceDecimalInputField(
                                  label: "Komponen Ekuitas Lain",
                                  bloc: bloc.otherEquityComponents,
                                  isNegative: true,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: Sizes.s10,
                                  bottom: Sizes.s10,
                                ),
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              // SizedBox(height: Sizes.s20),
                              PralistingHelper.fieldWrapper.wrap(
                                index: 27,
                                child: NoteInputField(
                                  bloc: bloc.note,
                                  financialType: "Neraca",
                                ),
                              ),
                              Container(
                                height: Sizes.s45,
                                width: double.maxFinite,
                                margin: EdgeInsets.only(top: Sizes.s40),
                                child: SantaraMainButton(
                                  key: Key('submit_button'),
                                  title: "Simpan",
                                  onPressed: () => bloc.submitForm(),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
