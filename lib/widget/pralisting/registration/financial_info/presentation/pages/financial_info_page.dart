import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/pages/business_dev_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/financial_info_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/balance_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/bank_statements_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/financial_report_quisioner_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/monthly_profit_loss_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/profit_loss_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/pages/financial_projections_page.dart';
import '../../../../../../injector_container.dart';

class FinancialInfoPage extends StatefulWidget {
  final String uuid;
  FinancialInfoPage({@required this.uuid});
  @override
  _FinancialInfoPageState createState() => _FinancialInfoPageState();
}

class _FinancialInfoPageState extends State<FinancialInfoPage> {
  FinancialInfoBloc financialInfoBloc;

  Widget _buildFinancialAspectTitle() {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s20),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          "Aspek Kualitatif Keuangan",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          "Pilih salah satu pernyataan yang paling sesuai dengan bisnis Anda",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    financialInfoBloc = sl<FinancialInfoBloc>();
    financialInfoBloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = financialInfoBloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Informasi Finansial",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "5 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          padding: EdgeInsets.all(Sizes.s20),
          child: BlocProvider<FinancialInfoBloc>(
            create: (_) => financialInfoBloc,
            child: Builder(
              builder: (context) {
                // ignore: close_sinks
                final bloc = context.bloc<FinancialInfoBloc>();
                return FormBlocListener<FinancialInfoBloc, FormResult,
                    FormResult>(
                  onLoaded: (context, state) {
                    setState(() {});
                  },
                  onSubmitting: (context, state) {
                    PopupHelper.showLoading(context);
                  },
                  onSuccess: (context, state) {
                    final form = state.successResponse;

                    if (form.status == FormStatus.submit_success) {
                      Navigator.pop(context);
                      ToastHelper.showSuccessToast(context, form.message);
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BusinessDevPage(
                            uuid: "${widget.uuid}",
                          ),
                        ),
                      );
                    }

                    if (form.status == FormStatus.delete_success) {
                      Navigator.pop(context);
                      ToastHelper.showSuccessToast(context, form.message);
                    }

                    if (form.status == FormStatus.submit_success_close_page) {
                      PralistingHelper.handleOnSavePralisting(
                        context,
                        message: form.message,
                      );
                    }
                  },
                  onFailure: (context, state) {
                    final form = state.failureResponse;
                    if (form.status == FormStatus.submit_error ||
                        form.status == FormStatus.delete_error) {
                      Navigator.pop(context);
                      ToastHelper.showFailureToast(context, form.message);
                    } else {
                      ToastHelper.showFailureToast(context, form.message);
                    }
                  },
                  onSubmissionFailed: (context, state) {
                    PralistingHelper.showSubmissionFailed(context);
                  },
                  child: BlocBuilder<FinancialInfoBloc, FormBlocState>(
                    bloc: bloc,
                    builder: (context, state) {
                      if (state is FormBlocLoading) {
                        return Center(
                          child: CupertinoActivityIndicator(),
                        );
                      } else {
                        return SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildFinancialAspectTitle(),
                              FinancialReportQuisionerField(bloc: bloc),
                              BalanceInputField(bloc: bloc),
                              ProfitLossField(bloc: bloc),
                              BankStatementsField(bloc: bloc),
                              MonthlyProfitLossField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        );
                      }
                    },
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
