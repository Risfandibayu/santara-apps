import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/blocs/profit_loss_input_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/note_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/profit_loss_input_field.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

import '../../../../../../injector_container.dart';

class ProfitLossInputPage extends StatefulWidget {
  final String uuid;
  ProfitLossInputPage({@required this.uuid});
  @override
  _ProfitLossInputPageState createState() => _ProfitLossInputPageState();
}

class _ProfitLossInputPageState extends State<ProfitLossInputPage> {
  ProfitLossInputBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<ProfitLossInputBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Input Laba Rugi",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 1,
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<ProfitLossInputBloc>();
              return FormBlocListener<ProfitLossInputBloc, FormResult,
                  FormResult>(
                onLoaded: (context, state) {},
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pop(context, true);
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<ProfitLossInputBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.white,
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // SizedBox(height: Sizes.s20),
                              ProfitLossInputField(bloc: bloc),
                              NoteInputField(
                                bloc: bloc.note,
                                financialType: "laba rugi",
                              ),
                              Container(
                                height: Sizes.s45,
                                width: double.maxFinite,
                                margin: EdgeInsets.only(top: Sizes.s40),
                                child: SantaraMainButton(
                                  key: Key('submit_button'),
                                  title: "Simpan",
                                  onPressed: () => bloc.submitForm(),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
