import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/models/SearchDataModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/usecases/business_info_1_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/domain/usecases/financial_info_usecases.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:meta/meta.dart';

class FinancialInfoBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetFinancialInfo getFinancialInfo;
  final UpdateFinancialInfo updateFinancialInfo;
  final GetListBusinessOptions getListBusinessOptions;
  final DeleteBalanceInput deleteBalanceInput;
  final DeleteProfitLossInput deleteProfitLossInput;
  final UpdateReaderNeraca updateReaderNeraca;
  final UpdateReaderProfitLoss updateReaderProfitLoss;

  static List<FileUploadModel> dummies = [];

  final financialReportQuisioner = SelectFieldBloc<SearchData, dynamic>(
    name: 'financial_report_quisioner',
    initialValue: null,
    validators: [PralistingValidationHelper.required],
  );

  final balanceInput = InputFieldBloc<bool, Object>(
    name: 'balance_input',
    validators: [PralistingValidationHelper.required],
    initialValue: false,
  );

  final lossProfitInput = InputFieldBloc<bool, Object>(
    name: 'loss_profit_input',
    validators: [PralistingValidationHelper.required],
    initialValue: false,
  );

  final bankStatementFile = TextFieldBloc(
    name: 'bank_statement_file',
    validators: [PralistingValidationHelper.required],
  );

  final monthlyProfitLossFile = TextFieldBloc(
    name: 'monthly_profit_loss_file',
    validators: [PralistingValidationHelper.required],
  );

  // unggah file xls neraca
  final balanceFile = TextFieldBloc(
    name: 'balance_file',
  );

  // unggah file xls laba rugi
  final lossProfitFile = TextFieldBloc(
    name: 'loss_profit_file',
  );

  Future<void> openWebUrl(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: false);
      } else {
        throw 'Cannot launch $url';
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  FinancialInfoBloc({
    @required this.getFinancialInfo,
    @required this.updateFinancialInfo,
    @required this.getListBusinessOptions,
    @required this.deleteBalanceInput,
    @required this.deleteProfitLossInput,
    @required this.updateReaderNeraca,
    @required this.updateReaderProfitLoss,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      financialReportQuisioner,
      balanceInput,
      lossProfitInput,
      bankStatementFile,
      monthlyProfitLossFile,
      balanceFile,
      lossProfitFile,
    ]);
  }

  // Hapus input neraca periode terakhir
  void removeBalanceFile() async {
    balanceFile.updateExtraData(null);
    balanceFile.updateValue(null);
    removeFieldBloc(fieldBloc: balanceFile);
    addFieldBloc(fieldBloc: balanceFile);
  }

  void removeBalance() async {
    emitSubmitting();
    final result = await deleteBalanceInput(uuid);
    if (result.isRight()) {
      var message = result.getOrElse(null).meta.message;
      final success = FormResult(
        status: FormStatus.delete_success,
        message: message,
      );
      emitSuccess(successResponse: success, canSubmitAgain: true);
    } else {
      result.leftMap((l) {
        final failure = FormResult(
          status: FormStatus.delete_error,
          message: l.message,
        );
        emitFailure(failureResponse: failure);
      });
    }
    balanceInput
        .updateValue(false); // false = belum mengisi / mengunggah file xls
    removeBalanceFile();
  }

  // update reader neraca
  void updateBalance() async {
    try {
      emitSubmitting();
      String filename = balanceFile.state?.extraData[0]?.filename ?? "";
      var data = {
        "uuid": "$uuid",
        "body": {"emiten_balance_sheets": "$filename"}
      };
      final result = await updateReaderNeraca(data);
      if (result.isRight()) {
        var message = result.getOrElse(null).meta.message;
        final success = FormResult(
          status: FormStatus.delete_success,
          message: message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.delete_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e) {
      final failure = FormResult(
        status: FormStatus.delete_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  // Hapus input laba rugi
  void removeLossProfitFile() async {
    lossProfitFile.updateExtraData(null);
    lossProfitFile.updateValue(null);
    removeFieldBloc(fieldBloc: lossProfitFile);
    addFieldBloc(fieldBloc: lossProfitFile);
  }

  void removeLossProfit() async {
    emitSubmitting();
    final result = await deleteProfitLossInput(uuid);
    if (result.isRight()) {
      var message = result.getOrElse(null).meta.message;
      final success = FormResult(
        status: FormStatus.delete_success,
        message: message,
      );
      emitSuccess(successResponse: success, canSubmitAgain: true);
    } else {
      result.leftMap((l) {
        final failure = FormResult(
          status: FormStatus.delete_error,
          message: l.message,
        );
        emitFailure(failureResponse: failure);
      });
    }
    lossProfitInput
        .updateValue(false); // false = belum mengisi / mengunggah file xls
    removeLossProfitFile();
  }

  // update reader laba rugi
  void updateLossProfit() async {
    try {
      String filename = lossProfitFile.state?.extraData[0]?.filename ?? "";
      emitSubmitting();
      var data = {
        "uuid": "$uuid",
        "body": {"emiten_profit_loss": "$filename"}
      };
      final result = await updateReaderProfitLoss(data);
      if (result.isRight()) {
        var message = result.getOrElse(null).meta.message;
        final success = FormResult(
          status: FormStatus.delete_success,
          message: message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.delete_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e) {
      final failure = FormResult(
        status: FormStatus.delete_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  postData({bool isNext}) async {
    try {
      emitSubmitting();
      var data = {
        "uuid": uuid,
        "body": {
          "financial_aspect_label":
              int.parse(financialReportQuisioner.value.id),
          "emiten_balance_sheets": balanceFile.state.extraData != null &&
                  balanceFile.state.extraData.length > 0
              ? "${balanceFile.state.extraData[0].filename}"
              : "",
          "emiten_profit_loss": lossProfitFile.state.extraData != null &&
                  lossProfitFile.state.extraData.length > 0
              ? "${lossProfitFile.state.extraData[0].filename}"
              : "",
          "pos_validation_file": bankStatementFile.state.extraData != null &&
                  bankStatementFile.state.extraData.length > 0
              ? "${bankStatementFile.state.extraData[0].filename}"
              : "",
          "profit_loss_monthly":
              monthlyProfitLossFile.state.extraData != null &&
                      monthlyProfitLossFile.state.extraData.length > 0
                  ? "${monthlyProfitLossFile.state.extraData[0].filename}"
                  : "",
        }
      };
      // logger.i(data);
      var result = await updateFinancialInfo(data);

      if (result.isRight()) {
        var message = result.getOrElse(null).meta.message;
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: message,
        );
        emitSuccess(
          canSubmitAgain: true,
          successResponse: success,
        );
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
      santaraLog(e, stack);
    }
  }

  getData() async {
    try {
      var result = await getFinancialInfo(uuid);
      // print(">> Result : $result");
      if (result.isRight()) {
        var data = result.getOrElse(null);

        if (data != null && !data.isClean) {
          if (data.financialAspectLabel.id != null) {
            financialReportQuisioner.updateValue(financialReportQuisioner
                .state.items
                .firstWhere((e) => e.id == "${data.financialAspectLabel.id}"));
          }
          // neraca
          if (data.emitenBalanceSheets != null &&
              data.emitenBalanceSheets.kas != null) {
            balanceInput.updateValue(true);
            balanceInput.updateExtraData(data.emitenBalanceSheets);
          }

          // laba rugi
          if (data.emitenProfitLoss != null &&
              data.emitenProfitLoss.profitLoss != null) {
            lossProfitInput.updateValue(true);
            lossProfitInput.updateExtraData(data.emitenProfitLoss);
          }

          // Lampiran validasi POS
          List<FileUploadModel> bankStatementFiles = [];
          if (data.posValidationFile != null) {
            bankStatementFile.updateExtraData(null);
            bankStatementFiles.add(FileUploadModel(
              filename: "${data.posValidationFile.filename}",
              url: "${data.posValidationFile.url}",
            ));
            bankStatementFiles.toSet().toList();
          }

          // Lampiran Laba Rugi
          List<FileUploadModel> profitLossMonthlyFiles = [];
          if (data.profitLossMonthly != null) {
            monthlyProfitLossFile.updateExtraData(null);
            profitLossMonthlyFiles.add(FileUploadModel(
              filename: "${data.profitLossMonthly.filename}",
              url: "${data.profitLossMonthly.url}",
            ));
            profitLossMonthlyFiles.toSet().toList();
          }

          bankStatementFile.updateExtraData(bankStatementFiles);
          monthlyProfitLossFile.updateExtraData(profitLossMonthlyFiles);
          await Future.delayed(Duration(milliseconds: 100));
          emitLoaded();
        } else {
          initEmptyFile();
          emitLoaded();
        }
      } else {
        initEmptyFile();
        result.leftMap((l) {
          printFailure(l);
        });
        emitLoaded();
      }
    } catch (e, stack) {
      initEmptyFile();
      santaraLog(e, stack);
      emitLoaded();
    }
  }

  initEmptyFile() {
    List<FileUploadModel> dummies = [];
    bankStatementFile.updateExtraData(dummies);
    monthlyProfitLossFile.updateExtraData(dummies);
  }

  openTemplate(String urlValue) async {
    try {
      var url = "${urlValue ?? ''}";
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Cannot launch $url';
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        await postData(isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await PralistingHelper.getBusinessOptions(
      group: 'financial_report',
      bloc: financialReportQuisioner,
      getListBusinessOptions: getListBusinessOptions,
    );
    await getData();
    emitLoaded();
    previousState = state.toJson();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}
