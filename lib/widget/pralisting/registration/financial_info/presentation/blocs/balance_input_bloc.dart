import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/models/financial_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/domain/usecases/financial_info_usecases.dart';

class BalanceInputBloc extends FormBloc<FormResult, FormResult> {
  final GetBalance getBalance;
  final UpdateBalance updateBalance;
  Map previousState;
  String uuid;
  final numberFormat = NumberFormat("###,###,###", "en_us");

  final cashCashEquivalents = TextFieldBloc<int>(
    name: 'kas',
    validators: [PralistingValidationHelper.required],
  );

  final accountsReceivable = TextFieldBloc(
    name: 'piutang',
    validators: [PralistingValidationHelper.required],
  );

  final stock = TextFieldBloc(
    name: 'stok',
    validators: [PralistingValidationHelper.required],
  );

  final downPayment = TextFieldBloc(
    name: 'uang_muka',
    validators: [PralistingValidationHelper.required],
  );

  final prepaidExpenses = TextFieldBloc(
    name: 'biaya_muka',
    validators: [PralistingValidationHelper.required],
  );

  final prepaidTaxes = TextFieldBloc(
    name: 'pajak_muka',
    validators: [PralistingValidationHelper.required],
  );

  final otherCurrentAssets = TextFieldBloc(
    name: 'aktiva_lancar',
    validators: [PralistingValidationHelper.required],
  );

  // aktiva tetap
  final land = TextFieldBloc(
    name: 'tanah',
    validators: [PralistingValidationHelper.required],
  );

  final building = TextFieldBloc(
    name: 'bangunan',
    validators: [PralistingValidationHelper.required],
  );

  final equipment = TextFieldBloc(
    name: 'peralatan',
    validators: [PralistingValidationHelper.required],
  );

  final vehicle = TextFieldBloc(
    name: 'kendaraan',
    validators: [PralistingValidationHelper.required],
  );

  final otherFixedAssets = TextFieldBloc(
    name: 'aktiva_tetap',
    validators: [PralistingValidationHelper.required],
  );

  final accumulatedDepreciationFixedAssets = TextFieldBloc(
    name: 'penyusutan',
    validators: [PralistingValidationHelper.required],
  );

  // aktiva lainnya
  final otherAssets = TextFieldBloc(
    name: 'aktiva_lain',
    validators: [PralistingValidationHelper.required],
  );

  // kewajiban lancar
  final accountPayable = TextFieldBloc(
    name: 'hutang',
    validators: [PralistingValidationHelper.required],
  );

  final taxPayable = TextFieldBloc(
    name: 'hutang_pajak',
    validators: [PralistingValidationHelper.required],
  );

  final shortTermBankLoans = TextFieldBloc(
    name: 'hutang_bank_pendek',
    validators: [PralistingValidationHelper.required],
  );

  final accruedExpense = TextFieldBloc(
    name: 'biaya',
    validators: [PralistingValidationHelper.required],
  );

  final otherCurrentLiabilities = TextFieldBloc(
    name: 'kewajiban_lain',
    validators: [PralistingValidationHelper.required],
  );

  // kewajiban jangka panjang
  final longTermBankLoans = TextFieldBloc(
    name: 'hutang_bank_panjang',
    validators: [PralistingValidationHelper.required],
  );

  final longTermThirdPartyDebt = TextFieldBloc(
    name: 'hutang_ketiga_panjang',
    validators: [PralistingValidationHelper.required],
  );

  final debtToShareholders = TextFieldBloc(
    name: 'hutang_saham',
    validators: [PralistingValidationHelper.required],
  );

  final otherLongTermLiabilities = TextFieldBloc(
    name: 'kewajiban_panjang_lain',
    validators: [PralistingValidationHelper.required],
  );

  // ekuitas
  final paidInCapital = TextFieldBloc(
    name: 'modal',
    validators: [PralistingValidationHelper.required],
  );

  final retainedEarnings = TextFieldBloc(
    name: 'laba_ditahan',
    validators: [PralistingValidationHelper.required],
  );

  final otherEquityComponents = TextFieldBloc(
    name: 'ekuitas_lain',
    validators: [PralistingValidationHelper.required],
  );

  // keterangan
  final note = TextFieldBloc(
    name: 'desc',
    validators: [PralistingValidationHelper.required],
  );

  BalanceInputBloc({
    @required this.getBalance,
    @required this.updateBalance,
  }) : super(isLoading: true) {
    addFieldBlocs(
      fieldBlocs: [
        // aktiva lancar
        cashCashEquivalents,
        accountsReceivable,
        stock,
        downPayment,
        prepaidExpenses,
        prepaidTaxes,
        otherCurrentAssets,
        // aktiva tetap
        land,
        building,
        equipment,
        vehicle,
        otherFixedAssets,
        accumulatedDepreciationFixedAssets,
        // aktiva lainnya
        otherAssets,
        // kewajiban lancar
        accountPayable,
        taxPayable,
        shortTermBankLoans,
        accruedExpense,
        otherCurrentLiabilities,
        // kewajiban jangka panjang
        longTermBankLoans,
        longTermThirdPartyDebt,
        debtToShareholders,
        otherLongTermLiabilities,
        // ekuitas
        paidInCapital,
        retainedEarnings,
        otherEquityComponents,
        // tambahkan keterangan
        note,
      ],
    );
  }
  getData() async {
    try {
      var result = await getBalance(uuid);
      if (result.isRight()) {
        var emitenBalanceSheets = result.getOrElse(null);
        if (emitenBalanceSheets != null) {
          cashCashEquivalents.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.kas));
          accountsReceivable.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.piutang));
          stock.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.stok));
          downPayment.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.uangMuka));
          prepaidExpenses.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.biayaMuka));
          prepaidTaxes.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.pajakMuka));
          //
          otherCurrentAssets.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.aktivaLancar));
          land.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.tanah));
          building.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.bangunan));
          equipment.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.peralatan));
          vehicle.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.kendaraan));
          //
          otherFixedAssets.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.aktivaTetap));
          accumulatedDepreciationFixedAssets.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.penyusutan));

          otherAssets.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.aktivaLain));
          accountPayable.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.hutang));
          taxPayable.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.hutangPajak));
          shortTermBankLoans.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.hutangBankPendek));
          accruedExpense.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.biaya));
          otherCurrentLiabilities.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.kewajibanLain));
          longTermBankLoans.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.hutangBankPanjang));
          longTermThirdPartyDebt.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.hutangKetigaPanjang));
          debtToShareholders.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.hutangSaham));
          otherLongTermLiabilities.updateValue(PralistingHelper.parseToDecimal(
              emitenBalanceSheets.kewajibanPanjangLain));
          paidInCapital.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.modal));
          retainedEarnings.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.labaDitahan));
          otherEquityComponents.updateValue(
              PralistingHelper.parseToDecimal(emitenBalanceSheets.ekuitasLain));
          note.updateValue(emitenBalanceSheets.desc);
        }
        emitLoaded();
      } else {
        result.leftMap((l) {
          printFailure(l);
          emitLoaded();
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      emitLoaded();
    }
  }

  postData(Map<String, dynamic> body) async {
    try {
      emitSubmitting();
      var parsed = EmitenBalanceSheets.fromJson(body);
      var data = {"uuid": uuid, "body": parsed.toJson()};
      // logger.i(data);
      var result = await updateBalance(data);
      print(result);
      if (result.isRight()) {
        var meta = result.getOrElse(null).meta;
        final success = FormResult(
          status: FormStatus.submit_success,
          message: meta.message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e, stack) {
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      santaraLog(e, stack);
      emitFailure(failureResponse: failure);
    }
  }

  submitForm() async {
    submit();
    if (state.isValid()) {
      var body = state.toJson();
      await postData(body);
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}
