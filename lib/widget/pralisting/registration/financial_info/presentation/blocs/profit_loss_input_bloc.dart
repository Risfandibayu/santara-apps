import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/domain/usecases/financial_info_usecases.dart';

class ProfitLossInputBloc extends FormBloc<FormResult, FormResult> {
  final GetCurrentTime getCurrentTime;
  final GetProfitLoss getProfitLoss;
  final UpdateProfitLoss updateProfitLoss;
  Map previousState;
  String uuid;
  DateTime currentTime = DateTime.now();
  final numberFormat = NumberFormat("###,###,###", "en_us");
  //
  final annualProfitLoss =
      ListFieldBloc<AnnualProfitLossBloc>(name: 'profit_loss');
  final note = TextFieldBloc(
    name: 'desc',
    validators: [PralistingValidationHelper.required],
  );

  ProfitLossInputBloc({
    @required this.getCurrentTime,
    @required this.getProfitLoss,
    @required this.updateProfitLoss,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      annualProfitLoss,
      note,
    ]);
  }

  getCurrentTimeApi() async {
    try {
      final result = await getCurrentTime(NoParams());
      print(result);
      if (result.isRight()) {
        final time = result.getOrElse(null);
        currentTime = time;
        if (annualProfitLoss.state.fieldBlocs != null &&
            annualProfitLoss.state.fieldBlocs.length > 0) {
          annualProfitLoss.state.fieldBlocs
              .asMap()
              .forEach((index, elem) async {
            elem.year.updateValue(
              DateFormat('yyyy').format(
                currentTime.subtract(
                  Duration(
                    days: 360 * (index + 1),
                  ),
                ),
              ),
            );
            // await Future.delayed(Duration(milliseconds: 100));
            // print(">> Year : ${elem.year.value}");
          });
        }
      } else {
        // logger.i(result);
        // logger.i(">> FAILED GET TIME");
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  String parseToDecimal(int value) {
    try {
      return numberFormat.format(value);
    } catch (e) {
      return "";
    }
  }

  getData() async {
    try {
      var result = await getProfitLoss(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null &&
            data.profitLoss != null &&
            data.profitLoss.length > 0) {
          data.profitLoss.asMap().forEach((index, e) async {
            addAnnualProfitLoss();
            await Future.delayed(Duration(milliseconds: 100));
            var fields = annualProfitLoss.state.fieldBlocs;
            var elem = fields[index];
            // fields.forEach((elem) {
            var res = data.profitLoss[index];
            elem.year.updateValue("${res['year']}");
            elem.revenue.updateValue(parseToDecimal(res['pendapatan']));
            elem.hpp.updateValue(parseToDecimal(res['hpp']));
            elem.marketing.updateValue(parseToDecimal(res['marketing']));
            elem.indirectMaterials
                .updateValue(parseToDecimal(res['bahan_pembantu']));
            elem.otherSellingExpenses
                .updateValue(parseToDecimal(res['biaya_penjualan_lainnya']));
            elem.employeeSalary
                .updateValue(parseToDecimal(res['gaji_karyawan']));
            elem.bonus.updateValue(parseToDecimal(res['bonus']));
            elem.utilitiesExpenses.updateValue(
                parseToDecimal(res['biaya_listrik_telepon_internet']));
            elem.officeAdministrationFee
                .updateValue(parseToDecimal(res['biaya_administrasi_kantor']));
            elem.transportationAccommodationCosts
                .updateValue(parseToDecimal(res['biaya_transport_akomodasi']));
            elem.maintenanceCosts.updateValue(
                parseToDecimal(res['biaya_pemeliharaan_perbaikan']));
            elem.rentExpense.updateValue(parseToDecimal(res['biaya_sewa']));
            elem.depreciationAmortization.updateValue(
                parseToDecimal(res['biaya_penyusutan_amortisasi']));
            elem.donation.updateValue(parseToDecimal(res['sumbangan']));
            elem.generalAdministrationExpense
                .updateValue(parseToDecimal(res['pendapatan_lain_lain']));
            elem.otherIncome
                .updateValue(parseToDecimal(res['pendapatan_lain_lain']));
            elem.otherExpenses
                .updateValue(parseToDecimal(res['beban_lain_lain']));
            elem.tax.updateValue(parseToDecimal(res['pajak']));
            // });
          });
          note.updateValue(data.desc);
        } else {
          addAnnualProfitLoss();
          addAnnualProfitLoss();
        }
        emitLoaded();
      } else {
        result.leftMap((l) {
          addAnnualProfitLoss();
          addAnnualProfitLoss();
          printFailure(l);
        });
      }
      emitLoaded();
    } catch (e, stack) {
      santaraLog(e, stack);
      emitLoaded();
    }
  }

  postData(Map<String, dynamic> body) async {
    try {
      emitSubmitting();
      // parsing body
      body['profit_loss'].forEach((value) {
        value['year'] = parseToInt(value['year']);
        value['pendapatan'] = parseToInt(value['pendapatan']);
        value['hpp'] = parseToInt(value['hpp']);
        value['marketing'] = parseToInt(value['marketing']);
        value['bahan_pembantu'] = parseToInt(value['bahan_pembantu']);
        value['biaya_penjualan_lainnya'] =
            parseToInt(value['biaya_penjualan_lainnya']);
        value['gaji_karyawan'] = parseToInt(value['gaji_karyawan']);
        value['bonus'] = parseToInt(value['bonus']);
        value['biaya_listrik_telepon_internet'] =
            parseToInt(value['biaya_listrik_telepon_internet']);
        value['biaya_administrasi_kantor'] =
            parseToInt(value['biaya_administrasi_kantor']);
        value['biaya_transport_akomodasi'] =
            parseToInt(value['biaya_transport_akomodasi']);
        value['biaya_pemeliharaan_perbaikan'] =
            parseToInt(value['biaya_pemeliharaan_perbaikan']);
        value['biaya_sewa'] = parseToInt(value['biaya_sewa']);
        value['biaya_penyusutan_amortisasi'] =
            parseToInt(value['biaya_penyusutan_amortisasi']);
        value['sumbangan'] = parseToInt(value['sumbangan']);
        value['biaya_umum_administrasi'] =
            parseToInt(value['biaya_umum_administrasi']);
        value['pendapatan_lain_lain'] =
            parseToInt(value['pendapatan_lain_lain']);
        value['beban_lain_lain'] = parseToInt(value['beban_lain_lain']);
        value['pajak'] = parseToInt(value['pajak']);
      });
      // end
      var payload = {"uuid": uuid, "body": body};
      // logger.i(payload);
      var result = await updateProfitLoss(payload);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        var message = data.meta.message;
        final success = FormResult(
          status: FormStatus.submit_success,
          message: message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
          printFailure(l);
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  submitForm() async {
    submit();
    if (state.isValid()) {
      var body = state.toJson();
      await postData(body);
    }
  }

  void addAnnualProfitLoss() {
    annualProfitLoss.addFieldBloc(
      AnnualProfitLossBloc(
        name: 'profit_loss',
        year: TextFieldBloc(
          name: 'year',
        ),
        revenue: TextFieldBloc(
          name: 'pendapatan',
          validators: [PralistingValidationHelper.required],
        ),
        hpp: TextFieldBloc(
          name: 'hpp',
          validators: [PralistingValidationHelper.required],
        ),
        marketing: TextFieldBloc(
          name: 'marketing',
          validators: [PralistingValidationHelper.required],
        ),
        indirectMaterials: TextFieldBloc(
          name: 'bahan_pembantu',
          validators: [PralistingValidationHelper.required],
        ),
        otherSellingExpenses: TextFieldBloc(
          name: 'biaya_penjualan_lainnya',
          validators: [PralistingValidationHelper.required],
        ),
        employeeSalary: TextFieldBloc(
          name: 'gaji_karyawan',
          validators: [PralistingValidationHelper.required],
        ),
        bonus: TextFieldBloc(
          name: 'bonus',
          validators: [PralistingValidationHelper.required],
        ),
        utilitiesExpenses: TextFieldBloc(
          name: 'biaya_listrik_telepon_internet',
          validators: [PralistingValidationHelper.required],
        ),
        officeAdministrationFee: TextFieldBloc(
          name: 'biaya_administrasi_kantor',
          validators: [PralistingValidationHelper.required],
        ),
        transportationAccommodationCosts: TextFieldBloc(
          name: 'biaya_transport_akomodasi',
          validators: [PralistingValidationHelper.required],
        ),
        maintenanceCosts: TextFieldBloc(
          name: 'biaya_pemeliharaan_perbaikan',
          validators: [PralistingValidationHelper.required],
        ),
        rentExpense: TextFieldBloc(
          name: 'biaya_sewa',
          validators: [PralistingValidationHelper.required],
        ),
        depreciationAmortization: TextFieldBloc(
          name: 'biaya_penyusutan_amortisasi',
          validators: [PralistingValidationHelper.required],
        ),
        donation: TextFieldBloc(
          name: 'sumbangan',
          validators: [PralistingValidationHelper.required],
        ),
        generalAdministrationExpense: TextFieldBloc(
          name: 'biaya_umum_administrasi',
          validators: [PralistingValidationHelper.required],
        ),
        otherIncome: TextFieldBloc(
          name: 'pendapatan_lain_lain',
          validators: [PralistingValidationHelper.required],
        ),
        otherExpenses: TextFieldBloc(
          name: 'beban_lain_lain',
          validators: [PralistingValidationHelper.required],
        ),
        tax: TextFieldBloc(
          name: 'pajak',
          validators: [PralistingValidationHelper.required],
        ),
      ),
    );
  }

  void removeFinancialField(int index) {
    annualProfitLoss.removeFieldBlocAt(index);
  }

  static int parseToInt(String value) {
    try {
      return int.parse(value.replaceAll(",", ""));
    } catch (e) {
      return 0;
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    await getCurrentTimeApi();
    previousState = state.toJson();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}

class AnnualProfitLossBloc extends GroupFieldBloc {
  // tahun
  final TextFieldBloc year;
  final TextFieldBloc revenue;
  final TextFieldBloc hpp;
  // beban penjualan
  final TextFieldBloc marketing;
  final TextFieldBloc indirectMaterials;
  final TextFieldBloc otherSellingExpenses;
  // beban umum
  final TextFieldBloc employeeSalary;
  final TextFieldBloc bonus;
  final TextFieldBloc utilitiesExpenses;
  final TextFieldBloc officeAdministrationFee;
  final TextFieldBloc transportationAccommodationCosts;
  final TextFieldBloc maintenanceCosts;
  final TextFieldBloc rentExpense;
  final TextFieldBloc depreciationAmortization;
  final TextFieldBloc donation;
  final TextFieldBloc generalAdministrationExpense;
  final TextFieldBloc otherIncome;
  final TextFieldBloc otherExpenses;
  final TextFieldBloc tax;

  AnnualProfitLossBloc({
    @required this.year,
    @required this.revenue,
    @required this.hpp,
    @required this.marketing,
    @required this.indirectMaterials,
    @required this.otherSellingExpenses,
    @required this.employeeSalary,
    @required this.bonus,
    @required this.utilitiesExpenses,
    @required this.officeAdministrationFee,
    @required this.transportationAccommodationCosts,
    @required this.maintenanceCosts,
    @required this.rentExpense,
    @required this.depreciationAmortization,
    @required this.donation,
    @required this.generalAdministrationExpense,
    @required this.otherIncome,
    @required this.otherExpenses,
    @required this.tax,
    String name,
  }) : super([
          year,
          revenue,
          hpp,
          marketing,
          indirectMaterials,
          otherSellingExpenses,
          employeeSalary,
          bonus,
          utilitiesExpenses,
          officeAdministrationFee,
          transportationAccommodationCosts,
          maintenanceCosts,
          rentExpense,
          depreciationAmortization,
          donation,
          generalAdministrationExpense,
          otherIncome,
          otherExpenses,
          tax,
        ], name: name);
}
