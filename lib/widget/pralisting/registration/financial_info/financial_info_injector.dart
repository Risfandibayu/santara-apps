import '../../../../injector_container.dart';
import 'data/datasources/financial_info_remote_data_source.dart';
import 'data/repositories/financial_info_repository_impl.dart';
import 'domain/repositories/financial_info_repository.dart';
import 'domain/usecases/financial_info_usecases.dart';
import 'presentation/blocs/balance_input_bloc.dart';
import 'presentation/blocs/financial_info_bloc.dart';
import 'presentation/blocs/profit_loss_input_bloc.dart';

void initFinancialInfoInjector() {
  // Financial Info
  sl.registerFactory(
    () => FinancialInfoBloc(
      getFinancialInfo: sl(),
      updateFinancialInfo: sl(),
      getListBusinessOptions: sl(),
      deleteBalanceInput: sl(),
      deleteProfitLossInput: sl(),
      updateReaderNeraca: sl(),
      updateReaderProfitLoss: sl(),
    ),
  );

  sl.registerFactory(
    () => BalanceInputBloc(
      getBalance: sl(),
      updateBalance: sl(),
    ),
  );

  sl.registerFactory(
    () => ProfitLossInputBloc(
      getCurrentTime: sl(),
      getProfitLoss: sl(),
      updateProfitLoss: sl(),
    ),
  );

  // Financial Info
  sl.registerLazySingleton(() => GetCurrentTime(sl()));
  sl.registerLazySingleton(() => GetFinancialInfo(sl()));
  sl.registerLazySingleton(() => UpdateFinancialInfo(sl()));
  sl.registerLazySingleton(() => DeleteBalanceInput(sl()));
  sl.registerLazySingleton(() => DeleteProfitLossInput(sl()));
  sl.registerLazySingleton(() => GetBalance(sl()));
  sl.registerLazySingleton(() => UpdateBalance(sl()));
  sl.registerLazySingleton(() => GetProfitLoss(sl()));
  sl.registerLazySingleton(() => UpdateProfitLoss(sl()));
  sl.registerLazySingleton(() => UpdateReaderNeraca(sl()));
  sl.registerLazySingleton(() => UpdateReaderProfitLoss(sl()));

  sl.registerLazySingleton<FinancialInfoRepository>(
    () => FinancialInfoRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<FinancialInfoRemoteDataSource>(
    () => FinancialInfoRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
