import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/models/financial_info_model.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/domain/repositories/financial_info_repository.dart';

class GetCurrentTime implements UseCase<DateTime, NoParams> {
  final FinancialInfoRepository repository;
  GetCurrentTime(this.repository);

  @override
  Future<Either<Failure, DateTime>> call(NoParams noParams) async {
    return await repository.getCurrentTime();
  }
}

class UpdateFinancialInfo implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialInfoRepository repository;
  UpdateFinancialInfo(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateFinancialInfo(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class GetFinancialInfo implements UseCase<FinancialInfoModel, String> {
  final FinancialInfoRepository repository;
  GetFinancialInfo(this.repository);

  @override
  Future<Either<Failure, FinancialInfoModel>> call(String uuid) async {
    return await repository.getFinancialInfo(uuid: uuid);
  }
}

class DeleteBalanceInput implements UseCase<PralistingMetaModel, String> {
  final FinancialInfoRepository repository;
  DeleteBalanceInput(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(String uuid) async {
    return await repository.deleteBalanceInput(uuid: uuid);
  }
}

class DeleteProfitLossInput implements UseCase<PralistingMetaModel, String> {
  final FinancialInfoRepository repository;
  DeleteProfitLossInput(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(String uuid) async {
    return await repository.deleteProfitLossInput(uuid: uuid);
  }
}

class GetBalance implements UseCase<EmitenBalanceSheets, String> {
  final FinancialInfoRepository repository;
  GetBalance(this.repository);

  @override
  Future<Either<Failure, EmitenBalanceSheets>> call(String uuid) async {
    return await repository.getBalance(uuid: uuid);
  }
}

class UpdateBalance implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialInfoRepository repository;
  UpdateBalance(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateBalance(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class GetProfitLoss implements UseCase<EmitenProfitLoss, String> {
  final FinancialInfoRepository repository;
  GetProfitLoss(this.repository);

  @override
  Future<Either<Failure, EmitenProfitLoss>> call(String uuid) async {
    return await repository.getProfitLoss(uuid: uuid);
  }
}

class UpdateProfitLoss implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialInfoRepository repository;
  UpdateProfitLoss(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateProfitLoss(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class UpdateReaderProfitLoss implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialInfoRepository repository;
  UpdateReaderProfitLoss(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateReaderProfitLoss(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}

class UpdateReaderNeraca implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialInfoRepository repository;
  UpdateReaderNeraca(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateReaderNeraca(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}
