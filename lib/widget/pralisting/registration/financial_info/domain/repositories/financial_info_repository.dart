import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/data/models/financial_info_model.dart';

abstract class FinancialInfoRepository {
  Future<Either<Failure, DateTime>> getCurrentTime();
  Future<Either<Failure, PralistingMetaModel>> updateFinancialInfo({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, FinancialInfoModel>> getFinancialInfo({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> deleteBalanceInput({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> deleteProfitLossInput({
    @required String uuid,
  });
  Future<Either<Failure, EmitenBalanceSheets>> getBalance({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> updateBalance({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, EmitenProfitLoss>> getProfitLoss({
    @required String uuid,
  });
  Future<Either<Failure, PralistingMetaModel>> updateProfitLoss({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, PralistingMetaModel>> updateReaderNeraca({
    @required String uuid,
    @required dynamic body,
  });
  Future<Either<Failure, PralistingMetaModel>> updateReaderProfitLoss({
    @required String uuid,
    @required dynamic body,
  });
}
