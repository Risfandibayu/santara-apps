import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/models/financial_projections_model.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/domain/repositories/financial_projections_repository.dart';

class GetFinancialProjections
    implements UseCase<FinancialProjectionsModel, String> {
  final FinancialProjectionsRepository repository;
  GetFinancialProjections(this.repository);

  @override
  Future<Either<Failure, FinancialProjectionsModel>> call(String uuid) async {
    return await repository.getFinancialProjections(uuid: uuid);
  }
}

class UpdateFinancialProjections
    implements UseCase<PralistingMetaModel, dynamic> {
  final FinancialProjectionsRepository repository;
  UpdateFinancialProjections(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic params) async {
    return await repository.updateFinancialProjections(
      uuid: params['uuid'],
      body: params['body'],
    );
  }
}
