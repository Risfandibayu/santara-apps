import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/models/financial_projections_model.dart';
import 'package:meta/meta.dart';

abstract class FinancialProjectionsRepository {
  Future<Either<Failure, FinancialProjectionsModel>> getFinancialProjections({
    @required String uuid,
  });

  Future<Either<Failure, PralistingMetaModel>>
      updateFinancialProjections({
    @required String uuid,
    @required dynamic body,
  });
}
