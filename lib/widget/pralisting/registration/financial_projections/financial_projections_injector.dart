import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/datasources/financial_projections_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/repositories/financial_projections_repository_impl.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/domain/repositories/financial_projections_repository.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/domain/usecases/financial_projections_usecases.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/blocs/financial_projections_bloc.dart';
import '../../../../injector_container.dart';

void initFinancialProjectionsInjector() {
  sl.registerFactory(
    () => FinancialProjectionsBloc(
      getFinancialProjections: sl(),
      updateFinancialProjections: sl(),
    ),
  );

  sl.registerLazySingleton<FinancialProjectionsRepository>(
    () => FinancialProjectionsRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<FinancialProjectionsRemoteDataSource>(
    () => FinancialProjectionsRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton(() => GetFinancialProjections(sl()));
  sl.registerLazySingleton(() => UpdateFinancialProjections(sl()));
}
