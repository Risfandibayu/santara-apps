import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/widgets/decimal_input_field.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/blocs/financial_projections_bloc.dart';

class FinancialProjectionsCard extends StatelessWidget {
  final int index;
  final FinancialProjectionsFieldBloc fieldBloc;
  final VoidCallback onRemove;

  const FinancialProjectionsCard({
    Key key,
    @required this.index,
    @required this.fieldBloc,
    @required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        index > 0
            ? Container(
                height: 1,
                width: double.maxFinite,
                color: Colors.grey[300],
                margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s10),
              )
            : Container(),
        index < 1 ? SizedBox(height: Sizes.s15) : Container(),
        Text(
          "Tahun #${index + 1}",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        BalanceDecimalInputField(
          label: "Penjualan",
          bloc: fieldBloc.sales,
        ),
        BalanceDecimalInputField(
          label: "HPP",
          bloc: fieldBloc.hpp,
        ),
        BalanceDecimalInputField(
          label: "SGA",
          bloc: fieldBloc.sga,
        ),
        BalanceDecimalInputField(
          label: "Laba Bersih",
          bloc: fieldBloc.netProfit,
        ),
        SizedBox(height: Sizes.s20),
      ],
    );
  }
}
