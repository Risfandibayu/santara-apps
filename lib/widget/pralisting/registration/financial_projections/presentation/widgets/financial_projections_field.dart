import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/blocs/financial_projections_bloc.dart';

import 'financial_projections_card.dart';

class FinancialProjectionsField extends StatelessWidget {
  final FinancialProjectionsBloc bloc;
  FinancialProjectionsField({@required this.bloc});

  Widget _financialProjectionsInputWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<ListFieldBloc<FinancialProjectionsFieldBloc>,
            ListFieldBlocState<FinancialProjectionsFieldBloc>>(
          bloc: bloc.financialProjections,
          builder: (context, state) {
            if (state.fieldBlocs.isNotEmpty) {
              return ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: state.fieldBlocs.length,
                itemBuilder: (context, i) {
                  return FinancialProjectionsCard(
                    index: i,
                    fieldBloc: state.fieldBlocs[i],
                    onRemove: null,
                  );
                },
              );
            }
            return Container();
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s30),
      child: _financialProjectionsInputWidget(),
    );
  }
}
