import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/widgets/pralisting_appbar/pralisting_appbar.dart';
import 'package:santaraapp/core/widgets/pralisting_buttons/pralisting_submit_button.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/blocs/financial_projections_bloc.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/widgets/financial_projections_field.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/pages/share_info_page.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

import '../../../../../../injector_container.dart';

class FinancialProjectionsPage extends StatefulWidget {
  final String uuid;
  FinancialProjectionsPage({@required this.uuid});
  @override
  _FinancialProjectionsPageState createState() =>
      _FinancialProjectionsPageState();
}

class _FinancialProjectionsPageState extends State<FinancialProjectionsPage> {
  FinancialProjectionsBloc bloc;
  final _controller = ScrollController();

  Widget _buildFinancialProjectionsTitle() {
    return Container(
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          "Proyeksi Keuangan",
          style: TextStyle(
            fontSize: FontSize.s14,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          "Laba rugi 5 tahun kedepan (Proyeksi usaha eksisiting ditambah memperhitungkan penambahan modal dari investor untuk project baru)",
          style: TextStyle(
            fontSize: FontSize.s12,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = sl<FinancialProjectionsBloc>();
    bloc.uuid = widget.uuid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool edited = bloc.detectChange();
        if (edited) {
          return edited;
        } else {
          PralistingHelper.handleBackButton(context);
          return false;
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: PralistingAppbar(
            title: "Proyeksi Keuangan",
            stepTitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Color(0xffBF0000),
                  fontFamily: 'Nunito',
                  fontSize: FontSize.s12,
                  fontWeight: FontWeight.w300,
                ),
                children: <TextSpan>[
                  TextSpan(text: "Tahap "),
                  TextSpan(
                    text: "7 dari 9",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            uuid: widget.uuid,
          ),
        ),
        body: BlocProvider(
          create: (_) => bloc,
          child: Builder(
            builder: (context) {
              // ignore: close_sinks
              final bloc = context.bloc<FinancialProjectionsBloc>();
              return FormBlocListener<FinancialProjectionsBloc, FormResult,
                  FormResult>(
                onLoaded: (context, state) async {
                  await Future.delayed(Duration(milliseconds: 500));
                  // _controller.jumpTo(0);
                },
                onSubmitting: (context, state) {
                  PopupHelper.showLoading(context);
                },
                onSuccess: (context, state) {
                  final form = state.successResponse;
                  if (form.status == FormStatus.submit_success) {
                    Navigator.pop(context);
                    ToastHelper.showSuccessToast(context, form.message);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ShareInfoPage(
                          uuid: "${widget.uuid}",
                        ),
                      ),
                    );
                  } else if (form.status ==
                      FormStatus.submit_success_close_page) {
                    PralistingHelper.handleOnSavePralisting(
                      context,
                      message: form.message,
                    );
                  }
                },
                onFailure: (context, state) {
                  final form = state.failureResponse;
                  if (form.status == FormStatus.submit_error) {
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, form.message);
                  } else {
                    ToastHelper.showFailureToast(context, form.message);
                  }
                },
                onSubmissionFailed: (context, state) {
                  PralistingHelper.showSubmissionFailed(context);
                },
                child: BlocBuilder<FinancialProjectionsBloc, FormBlocState>(
                  builder: (context, state) {
                    if (state is FormBlocLoading) {
                      return Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else {
                      return Container(
                        padding: EdgeInsets.all(Sizes.s20),
                        width: double.maxFinite,
                        height: double.maxFinite,
                        color: Colors.white,
                        child: SingleChildScrollView(
                          controller: _controller,
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildFinancialProjectionsTitle(),
                              FinancialProjectionsField(bloc: bloc),
                              PralistingSubmitButton(
                                nextKey: 'pralisting_submit_button',
                                saveKey: 'pralisting_save_button',
                                onTapNext: () => bloc.submitForm(isNext: true),
                                onTapSave: () => bloc.submitForm(isNext: false),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
