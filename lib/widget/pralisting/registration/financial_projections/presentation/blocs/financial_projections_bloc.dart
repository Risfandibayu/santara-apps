import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/helpers/PralistingHelper.dart';
import 'package:santaraapp/helpers/PralistingValidationHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/models/financial_projections_model.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/domain/usecases/financial_projections_usecases.dart';

class FinancialProjectionsBloc extends FormBloc<FormResult, FormResult> {
  String uuid;
  Map previousState;
  final GetFinancialProjections getFinancialProjections;
  final UpdateFinancialProjections updateFinancialProjections;

  final financialProjections = ListFieldBloc<FinancialProjectionsFieldBloc>(
    name: 'financial_projections',
  );

  FinancialProjectionsBloc({
    @required this.getFinancialProjections,
    @required this.updateFinancialProjections,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [financialProjections]);
  }

  void addFinancialProjectionFields(int year) {
    financialProjections.addFieldBloc(
      FinancialProjectionsFieldBloc(
        name: 'financial_projection',
        sales: TextFieldBloc(
          name: 'penjualan',
          validators: [PralistingValidationHelper.required],
        ),
        hpp: TextFieldBloc(
          name: 'hpp',
          validators: [PralistingValidationHelper.required],
        ),
        sga: TextFieldBloc(
          name: 'sga',
          validators: [PralistingValidationHelper.required],
        ),
        netProfit: TextFieldBloc(
          name: 'laba_bersih',
          validators: [PralistingValidationHelper.required],
        ),
        year: TextFieldBloc(
          name: 'year',
          validators: [PralistingValidationHelper.required],
          initialValue: "$year",
        ),
      ),
    );
  }

  void removeFinancialProjectionField(int index) {
    financialProjections.removeFieldBlocAt(index);
  }

  postData(Map<String, dynamic> body, {bool isNext}) async {
    try {
      emitSubmitting();
      var parsed = FinancialProjectionsModel.fromJson(body);
      var data = {"uuid": uuid, "body": parsed.toJson()};
      var result = await updateFinancialProjections(data);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        final success = FormResult(
          status: isNext
              ? FormStatus.submit_success
              : FormStatus.submit_success_close_page,
          message: data.meta.message,
        );
        emitSuccess(successResponse: success, canSubmitAgain: true);
      } else {
        result.leftMap((l) {
          final failure = FormResult(
            status: FormStatus.submit_error,
            message: l.message,
          );
          emitFailure(failureResponse: failure);
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      final failure = FormResult(
        status: FormStatus.submit_error,
        message: e,
      );
      emitFailure(failureResponse: failure);
    }
  }

  getData() async {
    try {
      var result = await getFinancialProjections(uuid);
      if (result.isRight()) {
        var data = result.getOrElse(null);
        if (data != null &&
            !data.isClean &&
            data.financialProjections != null &&
            data.financialProjections.length > 0) {
          data.financialProjections.asMap().forEach((key, value) async {
            addFinancialProjectionFields(value.year);
            await Future.delayed(Duration(milliseconds: 100));
            financialProjections.state.fieldBlocs[key].hpp
                .updateValue(PralistingHelper.parseToDecimal(value.hpp));
            financialProjections.state.fieldBlocs[key].netProfit
                .updateValue(PralistingHelper.parseToDecimal(value.labaBersih));
            financialProjections.state.fieldBlocs[key].sales
                .updateValue(PralistingHelper.parseToDecimal(value.penjualan));
            financialProjections.state.fieldBlocs[key].sga
                .updateValue(PralistingHelper.parseToDecimal(value.sga));
            financialProjections.state.fieldBlocs[key].year
                .updateValue(PralistingHelper.parseToDecimal(value.year));
          });
        } else {
          await initFields();
        }
        emitLoaded();
      } else {
        await initFields();
        result.leftMap((l) {
          printFailure(l);
          emitLoaded();
        });
      }
    } catch (e, stack) {
      await initFields();
      santaraLog(e, stack);
      emitLoaded();
    }
  }

  initFields() async {
    for (var i = 1; i <= 5; i++) {
      addFinancialProjectionFields(i);
    }
    await Future.delayed(Duration(milliseconds: 100));
  }

  submitForm({@required bool isNext}) async {
    try {
      submit();
      if (state.isValid()) {
        Map<String, dynamic> body = state.toJson();
        await postData(body, isNext: isNext);
      } else {
        emitFailure(
          failureResponse: FormResult(
            status: FormStatus.validation_error,
            message: "Tidak dapat melanjutkan, mohon cek kembali form anda!",
          ),
        );
      }
    } catch (e, stack) {
      emitFailure(
        failureResponse: FormResult(
          status: FormStatus.submit_error,
          message: "Terjadi kesalahan!",
        ),
      );
      santaraLog(e, stack);
    }
  }

  @override
  void onLoading() async {
    super.onLoading();
    await getData();
    previousState = state.toJson();
  }

  bool detectChange() {
    Map nextState = state.toJson();
    if (previousState == null) {
      return true;
    } else if (previousState.toString() != nextState.toString()) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void onSubmitting() async {}
}

class FinancialProjectionsFieldBloc extends GroupFieldBloc {
  final TextFieldBloc sales;
  final TextFieldBloc hpp;
  final TextFieldBloc sga;
  final TextFieldBloc netProfit;
  final TextFieldBloc year;

  FinancialProjectionsFieldBloc({
    @required this.sales,
    @required this.hpp,
    @required this.sga,
    @required this.netProfit,
    @required this.year,
    String name,
  }) : super([sales, hpp, sga, netProfit, year], name: name);
}
