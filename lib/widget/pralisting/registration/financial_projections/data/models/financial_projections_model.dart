// To parse this JSON data, do
//
//     final financialProjectionsModel = financialProjectionsModelFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/core/data/models/submission_detail_model.dart';

FinancialProjectionsModel financialProjectionsModelFromJson(String str) =>
    FinancialProjectionsModel.fromJson(json.decode(str));

String financialProjectionsModelToJson(FinancialProjectionsModel data) =>
    json.encode(data.toJson());

class FinancialProjectionsModel {
  FinancialProjectionsModel({
    this.financialProjections,
    this.submission,
    this.isComplete,
    this.isClean,
  });

  List<FinancialProjection> financialProjections;
  bool isComplete;

  Submission submission;

  bool isClean;
  factory FinancialProjectionsModel.fromJson(Map<String, dynamic> json) =>
      FinancialProjectionsModel(
        financialProjections: json["financial_projections"] == null
            ? null
            : List<FinancialProjection>.from(json["financial_projections"]
                .map((x) => FinancialProjection.fromJson(x))),
        submission: json["submission"] == null
            ? null
            : Submission.fromJson(json["submission"]),
        isComplete: json["is_complete"] == null ? null : json["is_complete"],
        isClean: json["is_clean"] == null ? null : json["is_clean"],
      );

  Map<String, dynamic> toJson() => {
        "financial_projections": financialProjections == null
            ? null
            : List<dynamic>.from(financialProjections.map((x) => x.toJson())),
        "is_complete": isComplete == null ? null : isComplete,
        "submission": submission == null ? null : submission.toJson(),
        "is_clean": isClean == null ? null : isClean,
      };
}

class FinancialProjection {
  FinancialProjection({
    this.penjualan,
    this.hpp,
    this.sga,
    this.labaBersih,
    this.year,
  });

  int penjualan;
  int hpp;
  int sga;
  int labaBersih;
  int year;

  static removeComma(String value) {
    if (value != null) {
      return value.replaceAll(",", "");
    }
  }

  factory FinancialProjection.fromJson(Map<String, dynamic> json) =>
      FinancialProjection(
        penjualan: json["penjualan"] == null
            ? null
            : int.parse(removeComma(json["penjualan"].toString())),
        hpp: json["hpp"] == null
            ? null
            : int.parse(removeComma(json["hpp"].toString())),
        sga: json["sga"] == null
            ? null
            : int.parse(removeComma(json["sga"].toString())),
        labaBersih: json["laba_bersih"] == null
            ? null
            : int.parse(removeComma(json["laba_bersih"].toString())),
        year: json["year"] == null
            ? null
            : int.parse(removeComma(json["year"].toString())),
      );

  Map<String, dynamic> toJson() => {
        "penjualan": penjualan == null ? null : penjualan,
        "hpp": hpp == null ? null : hpp,
        "sga": sga == null ? null : sga,
        "laba_bersih": labaBersih == null ? null : labaBersih,
        "year": year == null ? null : year,
      };
}
