import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/datasources/financial_projections_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/models/financial_projections_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/domain/repositories/financial_projections_repository.dart';
import 'package:meta/meta.dart';

class FinancialProjectionsRepositoryImpl
    implements FinancialProjectionsRepository {
  final NetworkInfo networkInfo;
  final FinancialProjectionsRemoteDataSource remoteDataSource;

  FinancialProjectionsRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, FinancialProjectionsModel>> getFinancialProjections({
    String uuid,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getFinancialProjections(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingMetaModel>> updateFinancialProjections({
    String uuid,
    dynamic body,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.updateFinancialProjections(
          uuid: uuid,
          body: body,
        );
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
