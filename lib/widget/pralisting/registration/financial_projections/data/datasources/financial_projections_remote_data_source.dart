import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/data/models/financial_projections_model.dart';
import 'package:meta/meta.dart';

abstract class FinancialProjectionsRemoteDataSource {
  Future<FinancialProjectionsModel> getFinancialProjections({
    @required String uuid,
  });

  Future<PralistingMetaModel> updateFinancialProjections({
    @required String uuid,
    @required dynamic body,
  });
}

class FinancialProjectionsRemoteDataSourceImpl
    implements FinancialProjectionsRemoteDataSource {
  final RestClient client;

  FinancialProjectionsRemoteDataSourceImpl({@required this.client});

  @override
  Future<FinancialProjectionsModel> getFinancialProjections({
    String uuid,
  }) async {
    try {
      final result =
          await client.getExternalUrl(url: "$apiPralisting/step7/$uuid");
      if (result.statusCode == 200) {
        return FinancialProjectionsModel.fromJson(result.data["data"]);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingMetaModel> updateFinancialProjections({
    String uuid,
    dynamic body,
  }) async {
    try {
      final result =
          await client.putExternalUrl(url: "$apiPralisting/step7/$uuid", body: body);
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
