part of 'comment_field_bloc.dart';

@immutable
abstract class PralistingCommentFieldEvent extends Equatable {
  const PralistingCommentFieldEvent();

  @override
  List<Object> get props => [];
}

class CommentFieldChanged extends PralistingCommentFieldEvent {
  const CommentFieldChanged({
    @required this.comment,
    this.userToReply,
    this.uuid,
  });

  final String comment;
  final String userToReply; // membalas ke
  final String uuid; // comment uuid / reply uuid

  @override
  List<Object> get props => [comment, userToReply, uuid];
}

class CancelReply extends PralistingCommentFieldEvent {}

class CommentSubmitted extends PralistingCommentFieldEvent {
  const CommentSubmitted();
}

class SendLikePralisting extends PralistingCommentFieldEvent {
  final String uuid;
  final bool
      isLike; // jika user sudah menyukai pralisting maka valuenya = false
  const SendLikePralisting({this.uuid, this.isLike});
}

class LoadField extends PralistingCommentFieldEvent {
  final bool isLiked;
  const LoadField({this.isLiked});
}

class SendCommentEvent extends PralistingCommentFieldEvent {
  final String uuid;
  final String comment;
  const SendCommentEvent({@required this.uuid, @required this.comment});
}

class SendReplyEvent extends PralistingCommentFieldEvent {
  final String uuid;
  final String comment;
  const SendReplyEvent({@required this.uuid, @required this.comment});
}

