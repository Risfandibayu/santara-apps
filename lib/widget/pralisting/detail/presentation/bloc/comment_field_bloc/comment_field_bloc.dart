import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/pralisting/detail/data/models/comment_field_model.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/usecases/pralisting_detail_usecase.dart';
part 'comment_field_event.dart';
part 'comment_field_state.dart';

class PralistingCommentFieldBloc
    extends Bloc<PralistingCommentFieldEvent, PralistingCommentFieldState> {
  final LikePralisting _likePralisting;
  final GetUserData _getUserData;
  final SendComment _sendComment;
  final SendReply _sendReply;

  PralistingCommentFieldBloc({
    @required LikePralisting likePralisting,
    @required GetUserData getUserData,
    @required SendComment sendComment,
    @required SendReply sendReply,
  })  : assert(likePralisting != null),
        assert(getUserData != null),
        assert(sendComment != null),
        assert(sendReply != null),
        _likePralisting = likePralisting,
        _getUserData = getUserData,
        _sendComment = sendComment,
        _sendReply = sendReply,
        super(PralistingCommentFieldState());

  @override
  Stream<PralistingCommentFieldState> mapEventToState(
      PralistingCommentFieldEvent event) async* {
    if (event is LoadField) {
      final userOrFailure = await _getUserData.call(NoParams());
      yield* userOrFailure.fold(
        (failure) => null,
        (user) async* {
          String picture = "";
          picture = user.trader.traderType == "company"
              ? user.trader.companyPhoto
              : user.trader.photo;
          yield state.copyWith(
            isLiked: event.isLiked,
            user: user,
            userPicture: picture,
          );
        },
      );
    } else if (event is CommentFieldChanged) {
      yield _mapCommentChangedToState(event, state);
    } else if (event is CancelReply) {
      yield _mapCancelReplyToState(event, state);
    } else if (event is SendLikePralisting) {
      await _likePralisting(event.uuid);
      yield _mapSendLikeToState(event, state);
    } else if (event is SendCommentEvent) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      final sendCommentOrFailure = await _sendComment({
        "comment": event.comment,
        "uuid": event.uuid, // uuid emiten
      });
      yield* sendCommentOrFailure.fold(
        (failure) async* {
          yield state.copyWith(
            status: FormzStatus.submissionFailure,
            failure: failure,
          );
        },
        (user) async* {
          // TODO: REMOVE DELAYED
          await Future.delayed(Duration(seconds: 3));
          yield state.copyWith(
            status: FormzStatus.submissionSuccess,
            comment: CommentFieldModel.pure(),
            userToReply: "",
            commentUuid: "",
          );
        },
      );
    } else if (event is SendReplyEvent) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      final sendCommentOrFailure = await _sendReply({
        "comment": event.comment,
        "uuid": event.uuid, // uuid emiten
      });
      yield* sendCommentOrFailure.fold(
        (failure) async* {
          yield state.copyWith(
            status: FormzStatus.submissionFailure,
            failure: failure,
          );
        },
        (user) async* {
          // TODO: REMOVE DELAYED
          await Future.delayed(Duration(seconds: 3));
          yield state.copyWith(
            status: FormzStatus.submissionSuccess,
            comment: CommentFieldModel.pure(),
            userToReply: "",
            commentUuid: "",
          );
        },
      );
    }
  }

  PralistingCommentFieldState _mapCommentChangedToState(
    CommentFieldChanged event,
    PralistingCommentFieldState state,
  ) {
    final comment = CommentFieldModel.dirty(event.comment);
    return state.copyWith(
      comment: comment,
      commentUuid: event.uuid,
      userToReply: event.userToReply,
      status: Formz.validate([state.comment, comment]),
    );
  }

  PralistingCommentFieldState _mapCancelReplyToState(
    CancelReply event,
    PralistingCommentFieldState state,
  ) {
    return state.copyWith(
      commentUuid: "",
      userToReply: "",
    );
  }

  PralistingCommentFieldState _mapSendLikeToState(
    SendLikePralisting event,
    PralistingCommentFieldState state,
  ) {
    return state.copyWith(
      status: FormzStatus.pure,
      isLiked: event.isLike,
    );
  }
}
