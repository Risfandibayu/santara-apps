part of 'comment_field_bloc.dart';

@immutable
class PralistingCommentFieldState extends Equatable {
  const PralistingCommentFieldState({
    this.status = FormzStatus.pure,
    this.comment = const CommentFieldModel.pure(),
    this.userToReply,
    this.commentUuid,
    this.isLiked,
    this.user,
    this.userPicture,
    this.failure,
  });

  final FormzStatus status;
  final CommentFieldModel comment;
  final String userToReply;
  final String commentUuid;
  final bool isLiked;
  final User user;
  final String userPicture;
  final Failure failure;

  PralistingCommentFieldState copyWith({
    FormzStatus status,
    CommentFieldModel comment,
    String userToReply,
    String commentUuid,
    bool isLiked,
    User user,
    String userPicture,
    Failure failure,
  }) {
    return PralistingCommentFieldState(
      status: status ?? this.status,
      comment: comment ?? this.comment,
      userToReply: userToReply ?? this.userToReply,
      commentUuid: commentUuid ?? this.commentUuid,
      isLiked: isLiked ?? this.isLiked,
      user: user ?? this.user,
      userPicture: userPicture ?? this.userPicture,
      failure: failure ?? this.failure,
    );
  }

  @override
  List<Object> get props => [
        status,
        comment,
        userToReply,
        commentUuid,
        isLiked,
        user,
        userPicture,
        failure,
      ];
}
