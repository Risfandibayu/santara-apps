import 'package:meta/meta.dart';
import 'comment_field_bloc/comment_field_bloc.dart';
import 'comments_bloc/pralisting_comments_bloc.dart';
import 'pralisting_bloc/pralisting_bloc.dart';

class PralistingBlocsData {
  final PralistingBloc pralistingBloc;
  final PralistingCommentsBloc commentsBloc;
  final PralistingCommentFieldBloc commentFieldBloc;

  PralistingBlocsData({
    @required this.pralistingBloc,
    @required this.commentsBloc,
    @required this.commentFieldBloc,
  });
}
