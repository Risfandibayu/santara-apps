part of 'pralisting_comments_bloc.dart';

@immutable
abstract class PralistingCommentsEvent extends Equatable {}

class LoadPralistingComments extends PralistingCommentsEvent {
  final String uuid;
  LoadPralistingComments({
    @required this.uuid,
  });

  @override
  List<Object> get props => [uuid];
}

class VoteEmiten extends PralistingCommentsEvent {
  final String uuid;
  final bool vote;

  VoteEmiten({
    @required this.uuid,
    @required this.vote,
  });

  @override
  List<Object> get props => [uuid, vote];
}

class SubmitInvestmentPlan extends PralistingCommentsEvent {
  final String uuid;
  final int amount;
  SubmitInvestmentPlan({
    @required this.uuid,
    @required this.amount,
  });

  @override
  List<Object> get props => [uuid, amount];
}

class CancelInvestmentPlan extends PralistingCommentsEvent {
  final String uuid;
  CancelInvestmentPlan({@required this.uuid});

  @override
  List<Object> get props => [uuid];
}

class DeleteCommentEvent extends PralistingCommentsEvent {
  final String commentUuid;
  final String emitenUuid;
  DeleteCommentEvent({@required this.commentUuid, @required this.emitenUuid});

  @override
  List<Object> get props => [commentUuid, emitenUuid];
}

class SendReportEvent extends PralistingCommentsEvent {
  final String uuid;
  final String comment;

  SendReportEvent({@required this.uuid, @required this.comment});

  @override
  List<Object> get props => [uuid, comment];
}
