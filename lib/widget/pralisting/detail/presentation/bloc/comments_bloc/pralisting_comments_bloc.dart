import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/usecases/pralisting_detail_usecase.dart';
part 'pralisting_comments_event.dart';
part 'pralisting_comments_state.dart';

class PralistingCommentsBloc
    extends Bloc<PralistingCommentsEvent, PralistingCommentsState> {
  PralistingCommentsBloc({
    @required GetPralistingComments getPralistingComments,
    @required DeleteComment deleteComment,
    @required SendReport sendReport,
  })  : assert(getPralistingComments != null),
        assert(deleteComment != null),
        assert(sendReport != null),
        _getPralistingComments = getPralistingComments,
        _deleteComment = deleteComment,
        _sendReport = sendReport,
        super(Empty());

  final GetPralistingComments _getPralistingComments;
  final DeleteComment _deleteComment;
  final SendReport _sendReport;

  @override
  Stream<PralistingCommentsState> mapEventToState(
      PralistingCommentsEvent event) async* {
    if (event is LoadPralistingComments) {
      yield* _mapLoadCommentsToState(event.uuid);
    } else if (event is DeleteCommentEvent) {
      yield* _mapDeleteCommentToState(event);
    } else if (event is SendReportEvent) {
      yield* _mapSendReportToState(event);
    }
  }

  Stream<PralistingCommentsState> _mapLoadCommentsToState(String uuid) async* {
    yield Loading();
    final failureOrPralistingComments = await _getPralistingComments(uuid);
    yield* failureOrPralistingComments.fold(
      (failure) async* {
        printFailure(failure);
        yield Error(message: failure.message);
      },
      (data) async* {
        if (data.comments.length < 1) {
          yield Empty();
        } else {
          // sorting comments
          var _comments = sortComments(data);
          // end sorting
          yield Loaded(commentsRaw: data, comments: _comments);
        }
      },
    );
  }

  Stream<PralistingCommentsState> _mapDeleteCommentToState(
      DeleteCommentEvent event) async* {
    yield Loading();
    final failureOrDeleteComment = await _deleteComment(event.commentUuid);
    yield* failureOrDeleteComment.fold(
      (failure) async* {
        yield Error(message: failure.message);
      },
      (data) async* {
        yield* _mapLoadCommentsToState(event.emitenUuid);
      },
    );
  }

  Stream<PralistingCommentsState> _mapSendReportToState(
      SendReportEvent event) async* {
    var loadedState = state as Loaded;
    yield Loading();
    final sendReportOrFailure = await _sendReport({
      "comment": event.comment,
      "uuid": event.uuid, // uuid emiten
    });
    yield* sendReportOrFailure.fold(
      (failure) async* {
        yield Error(message: failure.message);
      },
      (data) async* {
        yield loadedState;
      },
    );
  }

  CommentModel sortComments(CommentModel data) {
    var _comments = data;
    _comments.comments = data.comments.reversed.toList();
    for (var i = 0; i < _comments.comments.length; i++) {
      _comments.comments[i].commentHistories =
          _comments.comments[i].commentHistories.length > 1
              ? _comments.comments[i].commentHistories.sublist(
                  (_comments.comments[i].commentHistories.length - 2),
                  _comments.comments[i].commentHistories.length)
              : _comments.comments[i].commentHistories;
    }
    return _comments;
  }
}
