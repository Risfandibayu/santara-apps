part of 'pralisting_comments_bloc.dart';

@immutable
abstract class PralistingCommentsState extends Equatable {}

class Empty extends PralistingCommentsState {
  @override
  List<Object> get props => [];
}

class Loading extends PralistingCommentsState {
  @override
  List<Object> get props => [];
}

class Error extends PralistingCommentsState {
  final String message;
  Error({@required this.message});

  @override
  List<Object> get props => [message];
}

class Loaded extends PralistingCommentsState {
  final CommentModel commentsRaw; // unsorted comments
  final CommentModel comments;
  Loaded({@required this.commentsRaw, @required this.comments});

  Loaded copyWith({
    CommentModel commentsRaw,
    CommentModel comments,
  }) {
    return Loaded(
      comments: comments ?? this.comments,
      commentsRaw: commentsRaw ?? this.commentsRaw,
    );
  }

  @override
  List<Object> get props => [commentsRaw, comments];
}
