part of 'pralisting_bloc.dart';

@immutable
abstract class PralistingState extends Equatable {}

class Empty extends PralistingState {
  @override
  List<Object> get props => [];
}

class Loading extends PralistingState {
  @override
  List<Object> get props => [];
}

class Error extends PralistingState {
  final String message;
  Error({@required this.message});

  @override
  List<Object> get props => [message];
}

class Loaded extends PralistingState {
  final PralistingModel data;
  final List<String> pictures;
  final double progress;
  final String currentFunding;
  final bool showInvestmentPlanForm;
  final FormzStatus status;
  final String statusMessage;

  Loaded({
    @required this.data,
    @required this.pictures,
    @required this.progress,
    @required this.currentFunding,
    @required this.showInvestmentPlanForm,
    @required this.status,
    @required this.statusMessage,
  });

  Loaded copyWith({
    PralistingModel data,
    List<String> pictures,
    double progress,
    String currentFunding,
    bool showInvestmentPlanForm,
    FormzStatus status,
    String statusMessage,
  }) {
    return Loaded(
      data: data ?? this.data,
      pictures: pictures ?? this.pictures,
      progress: progress ?? this.progress,
      currentFunding: currentFunding ?? this.currentFunding,
      showInvestmentPlanForm:
          showInvestmentPlanForm ?? this.showInvestmentPlanForm,
      status: status ?? this.status,
      statusMessage: statusMessage ?? this.statusMessage,
    );
  }

  @override
  List<Object> get props => [
        data,
        pictures,
        progress,
        currentFunding,
        showInvestmentPlanForm,
        status,
        statusMessage,
      ];
}
