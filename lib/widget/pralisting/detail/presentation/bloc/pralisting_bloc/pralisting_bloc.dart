import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/entities/pralisting_model.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/usecases/pralisting_detail_usecase.dart';
part 'pralisting_event.dart';
part 'pralisting_state.dart';

class PralistingBloc extends Bloc<PralistingEvent, PralistingState> {
  PralistingBloc({
    @required GetPralistingDetail getPralistingDetail,
    @required PostVotePralisting postVotePralisting,
    @required PostCancelVotePralisting postCancelVotePralisting,
    @required PostInvestmentPlanPralisting postInvestmentPlanPralisting,
  })  : assert(getPralistingDetail != null),
        assert(postVotePralisting != null),
        assert(postCancelVotePralisting != null),
        assert(postInvestmentPlanPralisting != null),
        _getPralistingDetail = getPralistingDetail,
        _postVotePralisting = postVotePralisting,
        _postCancelVotePralisting = postCancelVotePralisting,
        _postInvestmentPlanPralisting = postInvestmentPlanPralisting,
        super(Empty());

  final GetPralistingDetail _getPralistingDetail;
  final PostVotePralisting _postVotePralisting;
  final PostCancelVotePralisting _postCancelVotePralisting;
  final PostInvestmentPlanPralisting _postInvestmentPlanPralisting;

  _nullIntHandler(int value) {
    return value == null ? 0 : value;
  }

  @override
  Stream<PralistingState> mapEventToState(
    PralistingEvent event,
  ) async* {
    if (event is LoadPralistingDetail) {
      try {
        yield Loading();
        final failureOrPralisting = await _getPralistingDetail(event.uuid);

        yield* failureOrPralisting.fold(
          (failure) async* {
            yield Error(message: failure.message);
          },
          (pralisting) async* {
            List<String> pics = [];
            double progress = 0.0;
            int currentFunding = 0;

            // Pictures Handler
            if (pralisting != null) {
              if (pralisting.pictures != null) {
                List<String> pictures = [];
                for (var i = 0; i < pralisting.pictures.length; i++) {
                  pictures.add(pralisting.pictures[i].picture);
                }

                if (pralisting.videoUrl != null &&
                    pralisting.videoUrl.isNotEmpty) {
                  pictures.insert(0, "");
                }

                pics = pictures;
              }

              // Progress Handler
              progress = (_nullIntHandler(pralisting.totalInvestmentPlans) /
                  _nullIntHandler(pralisting.capitalNeeds));
              if (progress > 1) {
                progress = 1;
              }

              if (progress.isNaN) {
                progress = 0;
              }

              // Current Funding Total Handler
              currentFunding =
                  _nullIntHandler(pralisting.totalInvestmentPlans) >
                          _nullIntHandler(pralisting.capitalNeeds)
                      ? _nullIntHandler(pralisting.capitalNeeds)
                      : _nullIntHandler(pralisting.totalInvestmentPlans);

              String currentFundingStr =
                  "${RupiahFormatter.initialValueFormat(currentFunding.toString())} Rencana pendanaan dari ${RupiahFormatter.initialValueFormat(_nullIntHandler(pralisting.capitalNeeds).toString())}";

              yield Loaded(
                data: pralisting,
                pictures: pics,
                progress: progress,
                currentFunding: currentFundingStr,
                showInvestmentPlanForm: false,
                status: FormzStatus.pure,
                statusMessage: null,
              );
              // cancelFundingStatus
            } else {
              yield Error(message: "No data available!");
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield Error(message: e);
      }
    }

    if (event is VoteEmiten) {
      if (state is Loaded) {
        var currentState = state as Loaded;
        yield currentState.copyWith(status: FormzStatus.submissionInProgress);
        PralistingModel data;
        try {
          if (currentState.data.isVote == 1) {
            data = currentState.data.copyWith(
              isVote: 0,
              totalVotes: currentState.data.totalVotes - 1,
            );
          } else {
            data = currentState.data.copyWith(
              isVote: 1,
              totalVotes: currentState.data.totalVotes + 1,
            );
          }

          final voteOrFailure = await _postVotePralisting.call(event.uuid);

          yield* voteOrFailure.fold(
            (failure) async* {
              print(">> Failure");
              yield currentState.copyWith(
                status: FormzStatus.submissionFailure,
                statusMessage: failure.message,
              );
            },
            (vote) async* {
              print(">> Success");
              yield currentState.copyWith(
                data: data,
                status: FormzStatus.submissionSuccess,
                statusMessage: null,
              );
            },
          );
        } catch (e, stack) {
          santaraLog(e, stack);
        }
      }
    }

    if (event is CancelInvestmentPlan) {
      if (state is Loaded) {
        var currentState = state as Loaded;
        yield currentState.copyWith(status: FormzStatus.submissionInProgress);
        PralistingModel data;
        try {
          print(">> Canceling..");
          // if (currentState.data.isVote == 1) {
          data = currentState.data.copyWith(
            isVote: 0,
            totalVotes: currentState.data.totalVotes - 1,
          );

          final cancelOrFailure =
              await _postCancelVotePralisting.call(event.uuid);

          yield* cancelOrFailure.fold(
            (failure) async* {
              print(">> Here u are");
              yield currentState.copyWith(
                status: FormzStatus.submissionFailure,
                statusMessage: failure.message,
              );
            },
            (vote) async* {
              print(">> Here u are 2");
              yield currentState.copyWith(
                data: data,
                status: FormzStatus.submissionSuccess,
                statusMessage: "Pengajuan Berhasil Dibatalkan!",
              );
            },
          );
          // }
        } catch (e, stack) {
          print(">> Here u are 3");
          santaraLog(e, stack);
          yield currentState.copyWith(
            data: data,
            status: FormzStatus.submissionFailure,
            statusMessage: "Terjadi Kesalahan Saat Membatalkan Pengajuan!",
          );
        }
      }
    }

    if (event is SubmitInvestmentPlan) {
      if (state is Loaded) {
        var currentState = state as Loaded;
        yield currentState.copyWith(status: FormzStatus.submissionInProgress);
        PralistingModel data;
        try {
          final postInvestmentOrFailure =
              await _postInvestmentPlanPralisting.call({
            "uuid": event.uuid,
            "amount": event.amount,
          });

          yield* postInvestmentOrFailure.fold(
            (failure) async* {
              yield currentState.copyWith(
                status: FormzStatus.submissionFailure,
                statusMessage: failure.message,
              );
            },
            (result) async* {
              yield currentState.copyWith(
                data: data,
                status: FormzStatus.submissionSuccess,
                statusMessage: "$result",
              );
            },
          );
        } catch (e, stack) {
          santaraLog(e, stack);
          yield currentState.copyWith(
            data: data,
            status: FormzStatus.submissionFailure,
            statusMessage:
                "Terjadi Kesalahan Saat Mengajukan Rencana Investasi!",
          );
        }
      }
    }

    if (event is UpdateLikesCount) {
      if (state is Loaded) {
        var currentState = state as Loaded;
        var currentPralistingData = currentState.data;
        PralistingModel newData = PralistingModel();
        if (event.isLiked) {
          newData = currentPralistingData.copyWith(
            totalLikes: currentPralistingData.totalLikes + 1,
            isLikes: event.isLiked ? 1 : 0,
          );
        } else {
          newData = currentPralistingData.copyWith(
            totalLikes: currentPralistingData.totalLikes - 1,
            isLikes: event.isLiked ? 1 : 0,
          );
        }
        yield currentState.copyWith(data: newData);
      }
    }
  }
}
