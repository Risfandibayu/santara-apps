part of 'pralisting_bloc.dart';

@immutable
abstract class PralistingEvent extends Equatable {}

class LoadPralistingDetail extends PralistingEvent {
  final String uuid;
  LoadPralistingDetail({
    @required this.uuid,
  });

  @override
  List<Object> get props => [uuid];
}

class VoteEmiten extends PralistingEvent {
  final String uuid;
  final bool vote;

  VoteEmiten({
    @required this.uuid,
    @required this.vote,
  });

  @override
  List<Object> get props => [uuid, vote];
}

class SubmitInvestmentPlan extends PralistingEvent {
  final String uuid;
  final int amount;
  SubmitInvestmentPlan({
    @required this.uuid,
    @required this.amount,
  });

  @override
  List<Object> get props => [uuid, amount];
}

class CancelInvestmentPlan extends PralistingEvent {
  final String uuid;
  CancelInvestmentPlan({@required this.uuid});

  @override
  List<Object> get props => [uuid];
}

// Untuk update jumlah orang menyukai
class UpdateLikesCount extends PralistingEvent {
  final bool isLiked;
  UpdateLikesCount({@required this.isLiked});

  @override
  List<Object> get props => [isLiked];
}

// Untuk update jumlah orang mengajukan
class UpdateSubmissionsCount extends PralistingEvent {
  final bool isSubmitted;
  UpdateSubmissionsCount({@required this.isSubmitted});

  @override
  List<Object> get props => [isSubmitted];
}
