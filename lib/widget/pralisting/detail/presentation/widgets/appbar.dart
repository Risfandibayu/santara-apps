import 'dart:io';

import 'package:flutter/material.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBar.dart';

class BuildPralistingAppBar extends StatelessWidget with PreferredSizeWidget {
  final Function onPop;
  BuildPralistingAppBar({@required this.onPop});

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: SantaraAppBar(
        leading: IconButton(
          icon: Icon(
            Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
          ),
          onPressed: onPop,
        ),
        actions: <Widget>[],
      ),
      preferredSize: Size.fromHeight(60),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
