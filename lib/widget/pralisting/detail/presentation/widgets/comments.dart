import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/detail_emiten/CommentEmitenUI.dart';
import 'package:santaraapp/widget/widget/components/listing/CommentSection.dart';
import 'package:santaraapp/widget/widget/components/listing/ListingDialog.dart';
import 'package:santaraapp/widget/widget/components/listing/RepliesSection.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import '../bloc/bloc_params.dart';
import '../bloc/comments_bloc/pralisting_comments_bloc.dart';
import '../bloc/comment_field_bloc/comment_field_bloc.dart';

class PralistingCommentsSection extends StatelessWidget {
  final String uuid;
  final int status;
  final PralistingBlocsData blocs;

  PralistingCommentsSection({
    @required this.uuid,
    @required this.status,
    @required this.blocs,
  });

  bool showMainFeatures() {
    return status < 3 ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return buildContents();
  }

  _handleDeleteComment({
    @required BuildContext context,
    @required String commentUuid,
    @required String emitenUuid,
  }) {
    PopupHelper.popConfirmation(
      context,
      () {
        Navigator.pop(context);
        blocs.commentsBloc
          ..add(
            DeleteCommentEvent(
              commentUuid: commentUuid,
              emitenUuid: emitenUuid,
            ),
          );
      },
      "Hapus Ulasan",
      "Apakah anda yakin ingin menghapus ulasan ini?",
    );
  }

  // handle ketika user mau report ulasan
  void _handleReport({
    @required BuildContext context,
    @required int type,
    @required int indexComment,
    @required int indexReply,
    @required String uuid,
  }) {
    // final provider = Provider.of<DetailEmitenProvider>(context, listen:false);
    String _radioValue = "";
    TextEditingController reasonCtrl = TextEditingController();
    // set up the AlertDialog
    var alert = StatefulBuilder(builder: (context, setState) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(Sizes.s10),
          ),
        ),
        contentPadding: EdgeInsets.zero,
        content: Container(
          padding: EdgeInsets.all(Sizes.s15),
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: Sizes.s40,
                              width: Sizes.s40,
                              child: Icon(
                                Icons.close,
                                size: Sizes.s20,
                              ),
                            ),
                          )),
                      Container(
                        height: Sizes.s5,
                      ),
                      Text(
                        "Bantu Santara Menjaga Komunitas yang Sehat",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: FontSize.s16,
                        ),
                      ),
                      Container(height: Sizes.s5),
                      Text(
                        "Mengapa Anda tidak ingin melihat kiriman ini ?",
                        style: TextStyle(fontSize: FontSize.s14),
                      ),
                      Container(height: Sizes.s5),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini mengganggu atau tidak menarik",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini mengganggu atau tidak menarik",
                              maxLines: 2,
                              style: TextStyle(fontSize: FontSize.s14),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value:
                                  "Menurut saya, ini tidak seharusnya ada di Santara",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Menurut saya, ini tidak seharusnya ada di Santara",
                              maxLines: 2,
                              style: TextStyle(fontSize: FontSize.s14),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini adalah spam",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini adalah spam",
                              maxLines: 2,
                              style: TextStyle(fontSize: FontSize.s14),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: Sizes.s10,
                      ),
                      Text(
                        "Deskripsi",
                        style: TextStyle(fontSize: FontSize.s14),
                      ),
                      Container(
                        height: Sizes.s5,
                      ),
                      TextFormField(
                        controller: reasonCtrl,
                        maxLines: 3,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: Sizes.s10,
                            vertical: Sizes.s10,
                          ),
                          hintText:
                              "Alasan mengapa anda ingin melaporkan ulasan ini..",
                          hintStyle: TextStyle(fontSize: FontSize.s14),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Color(0xffDDDDDD),
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                        ),
                        onSaved: (value) {
                          // print(">> $value");
                        },
                      ),
                      Container(
                        height: Sizes.s10,
                      ),
                      Container(
                        height: Sizes.s40,
                        child: SantaraMainButton(
                          title: "Kirim",
                          onPressed: () async {
                            if (_radioValue.isEmpty) {
                              ToastHelper.showFailureToast(
                                context,
                                "Mohon pilih alasan anda!",
                              );
                            } else {
                              Navigator.pop(context);
                              // await Future.delayed(Duration(milliseconds: 500));
                              blocs.commentsBloc
                                ..add(
                                  SendReportEvent(
                                    uuid: uuid,
                                    comment:
                                        "${_radioValue + ';' + reasonCtrl.text}",
                                  ),
                                );
                              ToastHelper.showBasicToast(
                                context,
                                "Laporan anda telah dikirim dan akan diproses!",
                              );
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // ketika use click (i) ulasan
  void _handleCommentPeng({@required BuildContext context}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return ListingAlertDialog(
              title: "Pedoman Komunitas",
              subtitle:
                  "Semua ulasan yang Anda berikan akan langsung terlihat oleh publik. Pengguna dapat melaporkan spam dan komentar yang tidak pantas kepada pihak Santara. Komentar yang melanggar tata krama/etika atau mengandung SARA dapat dihapus secara sepihak oleh Santara.",
              buttonTitle: "Mengerti",
              showCloseButton: false,
              height: Sizes.s250 + Sizes.s10,
              onClose: () => Navigator.pop(context),
              onTap: () => Navigator.pop(context));
        });
  }

  Widget _buildUserComments(CommentModel comments) {
    int commentsTotal =
        comments.comments != null && comments.comments.length != null
            ? comments.comments.length < 4
                ? comments.comments.length
                : 4
            : 0;
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: commentsTotal,
      itemBuilder: (context, index) {
        int repliesTotal = comments.comments[index].commentHistories.length > 2
            ? 2
            : comments.comments[index].commentHistories.length;
        var comment = comments.comments[index];
        return UserComment(
          showAction: showMainFeatures(),
          maxRadius: Sizes.s25,
          onDelete: () => _handleDeleteComment(
            context: context,
            commentUuid: comment.uuid,
            emitenUuid: uuid,
          ),
          onReport: () => _handleReport(
            context: context,
            type: 1,
            indexComment: index,
            indexReply: 0,
            uuid: comment.uuid,
          ),
          onReply: () {
            blocs.commentFieldBloc
              ..add(
                CommentFieldChanged(
                  uuid: comment.uuid,
                  userToReply: comment.name,
                  comment: blocs.commentFieldBloc.state.comment.value ?? "",
                ),
              );
          },
          data: comment,
          replies: comments.comments[index].commentHistories != null &&
                  comments.comments[index].commentHistories.length > 0
              ? ListView.builder(
                  itemBuilder: (context, indexReply) {
                    var _histories = comments.comments[index].commentHistories;
                    return RepliesSection(
                      showAction: showMainFeatures(),
                      maxRadius: Sizes.s20,
                      data: _histories[indexReply],
                      onDelete: () => _handleDeleteComment(
                        context: context,
                        commentUuid: _histories[indexReply].uuid,
                        emitenUuid: uuid,
                      ),
                      onReport: () => _handleReport(
                        context: context,
                        type: 1,
                        indexComment: index,
                        indexReply: indexReply,
                        uuid: _histories[indexReply].uuid,
                      ),
                      onReply: () {
                        blocs.commentFieldBloc
                          ..add(
                            CommentFieldChanged(
                              uuid: comment.uuid,
                              userToReply: _histories[indexReply].name,
                              comment:
                                  blocs.commentFieldBloc.state.comment.value ??
                                      "",
                            ),
                          );
                      },
                    );
                  },
                  itemCount: repliesTotal,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                )
              : Container(),
        );
      },
    );
  }

  Widget _buildButton(BuildContext context, CommentModel comments) {
    return Container(
      width: double.infinity,
      child: OutlineButton(
        borderSide: BorderSide.none,
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CommentEmitenUI(
                emitenComment: comments,
                showAction: showMainFeatures(),
              ),
            ),
          );
          if (result != null) {
            blocs.commentsBloc..add(LoadPralistingComments(uuid: uuid));
          }
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Lihat Selengkapnya",
              style: TextStyle(
                color: Color(0xff218196),
                fontSize: FontSize.s16,
              ),
            ),
            Container(width: Sizes.s10),
            Icon(
              Icons.expand_more,
              color: Color(0xff218196),
            )
          ],
        ),
      ),
    );
  }

  Widget _noUlasan() {
    return Container(
      margin: EdgeInsets.only(bottom: Sizes.s30),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 5, bottom: 10),
            child: Text(
              "0 Ulasan",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: FontSize.s18,
                  color: Colors.white),
            ),
          ),
          Center(
            child: Text(
              "Belum ada ulasan untuk bisnis ini",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildContents() {
    return Container(
      width: double.maxFinite,
      color: Colors.transparent,
      padding: EdgeInsets.all(Sizes.s20),
      child: BlocListener<PralistingCommentsBloc, PralistingCommentsState>(
        listener: (context, state) {},
        child: BlocBuilder<PralistingCommentsBloc, PralistingCommentsState>(
          builder: (context, state) {
            if (state is Loading) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state is Empty) {
              return _noUlasan();
            } else if (state is Error) {
              return Center(
                child: Text(
                  "${state.message}",
                  style: TextStyle(color: Colors.white),
                ),
              );
            } else if (state is Loaded) {
              return Container(
                margin: EdgeInsets.only(bottom: Sizes.s20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "${state.comments.totalComments} Ulasan",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.s18,
                              color: Colors.white),
                        ),
                        InkWell(
                          onTap: () => _handleCommentPeng(context: context),
                          child: Container(
                            margin: EdgeInsets.only(left: Sizes.s10),
                            height: Sizes.s25,
                            width: Sizes.s25,
                            child: Icon(
                              Icons.info,
                              size: FontSize.s20,
                              color: Color(0xffDADADA),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: Sizes.s15),
                    _buildUserComments(state.comments),
                    _buildButton(context, state.comments),
                  ],
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }
}
