import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:formz/formz.dart';
import '../bloc/pralisting_bloc/pralisting_bloc.dart';
import '../bloc/comments_bloc/pralisting_comments_bloc.dart';
import '../bloc/comment_field_bloc/comment_field_bloc.dart';
import '../bloc/bloc_params.dart';

class PralistingCommentFieldWidget extends StatelessWidget {
  final PralistingBlocsData blocs;
  final String uuid;

  PralistingCommentFieldWidget({
    @required this.blocs,
    @required this.uuid,
  });

  // Controller
  final TextEditingController controller = TextEditingController();

  Widget _buildButtonSendMessage({
    @required PralistingCommentFieldState state,
    @required BuildContext context,
  }) {
    return IconButton(
      icon: state.status.isSubmissionInProgress
          ? CupertinoActivityIndicator()
          : Icon(
              Icons.send,
              color: Color(0xffBF2D30),
            ),
      onPressed: () async {
        if (state.status.isInvalid) {
          print(state.comment.error);
          Scaffold.of(context).hideCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(
                "Ulasan yang anda masukan tidak valid!",
              ),
            ),
          );
        } else {
          if (!state.status.isSubmissionInProgress) {
            if (state.userToReply != null && state.userToReply.isNotEmpty) {
              blocs.commentFieldBloc
                ..add(SendReplyEvent(
                    comment: controller.text, uuid: state.commentUuid));
            } else {
              blocs.commentFieldBloc
                ..add(SendCommentEvent(comment: controller.text, uuid: uuid));
            }
          }
        }
      },
    );
  }

  Widget _buildButtonLikePublisher(
      {@required bool isLiked, @required String uuid}) {
    return IconButton(
      icon: Icon(
        isLiked ? Icons.favorite : Icons.favorite_border,
        color: Color(0xffBF2D30),
      ),
      onPressed: () {
        // Hit Api Like
        blocs.commentFieldBloc
          ..add(SendLikePralisting(isLike: !isLiked, uuid: uuid));
        // Update UI (likes count number)
        blocs.pralistingBloc..add(UpdateLikesCount(isLiked: !isLiked));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PralistingCommentFieldBloc,
        PralistingCommentFieldState>(
      listener: (BuildContext context, state) {
        if (state.status.isSubmissionSuccess) {
          controller.clear();
          blocs.commentsBloc..add(LoadPralistingComments(uuid: uuid));
        }

        if (state.status.isSubmissionFailure) {
          Scaffold.of(context).hideCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(
                state.failure.message,
              ),
            ),
          );
        }
      },
      child:
          BlocBuilder<PralistingCommentFieldBloc, PralistingCommentFieldState>(
        // buildWhen: (previous, current) => previous.status != current.status,
        builder: (context, state) {
          return Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.transparent,
              // decoration: BoxDecoration(

              //   boxShadow: [
              //     BoxShadow(
              //       color: Color.fromRGBO(146, 146, 146, 0.25),
              //       offset: Offset(2, 0),
              //       blurRadius: 5,
              //       spreadRadius: 5,
              //     )
              //   ],
              // ),
              child: Column(
                children: [
                  state.userToReply != null && state.userToReply.isNotEmpty
                      ? Container(
                          padding: EdgeInsets.only(
                            left: Sizes.s15,
                            top: Sizes.s10,
                            right: Sizes.s15,
                          ),
                          height: Sizes.s30,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Text(
                                  "Membalas ke ${state.userToReply}",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: FontSize.s13,
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () =>
                                    blocs.commentFieldBloc..add(CancelReply()),
                                child: Container(
                                  height: Sizes.s25,
                                  width: Sizes.s25,
                                  child: Center(
                                    child: Icon(
                                      Icons.close,
                                      size: Sizes.s18,
                                      color: Colors.grey[600],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container(),
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.only(left: Sizes.s10, right: Sizes.s10),
                    height: Sizes.s60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: Sizes.s35,
                          height: Sizes.s35,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(Sizes.s35),
                            child: CachedNetworkImage(
                              imageUrl: "${state.userPicture}",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(width: Sizes.s10),
                        Container(
                          height: Sizes.s40,
                          width: MediaQuery.of(context).size.width - Sizes.s130,
                          child: TextField(
                            onChanged: (value) {
                              blocs.commentFieldBloc
                                ..add(CommentFieldChanged(comment: value));
                            },
                            enabled: !state.status.isSubmissionInProgress,
                            controller: controller,
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              hintText: "Tulis Ulasan Anda",
                              filled: true,
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: Sizes.s5, horizontal: Sizes.s15),
                              border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.0),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(Sizes.s40),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: Sizes.s10),
                        state.comment.value.length > 0
                            ? _buildButtonSendMessage(
                                state: state,
                                context: context,
                              )
                            : _buildButtonLikePublisher(
                                isLiked: state.isLiked != null
                                    ? state.isLiked
                                    : false,
                                uuid: uuid,
                              )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
