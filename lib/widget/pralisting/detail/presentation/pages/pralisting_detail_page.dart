import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/detail_emiten/YoutubePlayerUI.dart';
import '../bloc/bloc_params.dart';
import '../bloc/comment_field_bloc/comment_field_bloc.dart';
import '../bloc/pralisting_bloc/pralisting_bloc.dart';
import '../widgets/appbar.dart';
import '../widgets/comment_field.dart';
import '../widgets/comments.dart';
import 'package:santaraapp/widget/widget/components/listing/HeroPhotoViewer.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraPralistingTabs.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../injector_container.dart';
import 'package:formz/formz.dart';
import '../bloc/comments_bloc/pralisting_comments_bloc.dart' as commentsBloc;

class PralistingDetailPage extends StatefulWidget {
  final String uuid;
  final int status;
  final int position;

  PralistingDetailPage({
    @required this.uuid,
    @required this.status,
    @required this.position,
  });

  @override
  _PralistingDetailPageState createState() => _PralistingDetailPageState();
}

class _PralistingDetailPageState extends State<PralistingDetailPage> {
  PralistingBloc pralistingBloc;
  PralistingCommentFieldBloc pralistingCommentFieldBloc;
  commentsBloc.PralistingCommentsBloc pralistingCommentsBloc;

  // controller investasi
  TextEditingController invesCtrl = TextEditingController();
  PralistingBlocsData pralistingBlocsData; // Used as params

  bool _blocsLoaded = false;

  Future<void> initializePralistingBlocs() async {
    pralistingBloc = sl<PralistingBloc>();
    pralistingCommentsBloc = sl<commentsBloc.PralistingCommentsBloc>();
    pralistingCommentFieldBloc = sl<PralistingCommentFieldBloc>();
  }

  // Store all blocs to single variable
  _mapBlocsToData() async {
    try {
      await initializePralistingBlocs();
      pralistingBlocsData = PralistingBlocsData(
        pralistingBloc: pralistingBloc,
        commentsBloc: pralistingCommentsBloc,
        commentFieldBloc: pralistingCommentFieldBloc,
      );
      setState(() {
        _blocsLoaded = true;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  @override
  void initState() {
    super.initState();
    _mapBlocsToData();
  }

  Widget _buildCommentField() {
    return BlocBuilder<PralistingBloc, PralistingState>(
      builder: (context, state) {
        if (state is Loaded) {
          var data = state.data;
          pralistingCommentFieldBloc
            ..add(LoadField(isLiked: data.isLikes == 1));
          return PralistingCommentFieldWidget(
            blocs: pralistingBlocsData,
            uuid: state.data.uuid,
          );
        } else {
          return Container();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: BuildPralistingAppBar(
        onPop: () => Navigator.pop(context),
      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider<PralistingBloc>(
            create: (_) => pralistingBloc
              ..add(
                LoadPralistingDetail(uuid: widget.uuid),
              ),
          ),
          BlocProvider<PralistingCommentFieldBloc>(
            create: (_) => pralistingCommentFieldBloc,
          ),
          BlocProvider<commentsBloc.PralistingCommentsBloc>(
            create: (_) => pralistingCommentsBloc
              ..add(
                commentsBloc.LoadPralistingComments(uuid: widget.uuid),
              ),
          ),
        ],
        child: Stack(
          children: [
            Container(
              width: double.maxFinite,
              height: double.maxFinite,
              child: bodyListener(child: buildBody(context)),
              padding: widget.status != null && widget.status < 3
                  // ? EdgeInsets.only(bottom: Sizes.s40)
                  // : EdgeInsets.zero,
                  ? EdgeInsets.zero
                  : EdgeInsets.zero,
            ),
            _blocsLoaded
                ? widget.status != null && widget.status < 3
                    ? Container()
                    : Container()
                : Container(),
          ],
        ),
      ),
    );
  }

  BlocListener bodyListener({Widget child}) {
    return BlocListener<PralistingBloc, PralistingState>(
      bloc: pralistingBloc,
      listener: (context, state) {
        if (state is Loaded) {
          switch (state.status) {
            case FormzStatus.submissionInProgress:
              PopupHelper.showLoading(context);
              break;
            case FormzStatus.submissionFailure:
              Navigator.pop(context);
              ToastHelper.showFailureToast(context, state.statusMessage ?? "");
              break;
            case FormzStatus.submissionSuccess:
              Navigator.pop(context);
              if (state.statusMessage != null) {
                ToastHelper.showSuccessToast(
                  context,
                  state.statusMessage ?? "",
                );
              }
              break;
            default:
              break;
          }
        }
      },
      child: child,
    );
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  int digitParser(String value) {
    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    double _doubleValue = double.parse(_onlyDigits);
    return _doubleValue.toInt();
  }

  String rupiahText(String value) {
    try {
      var _newValue = RupiahToText.convertRupiah(value);
      return _newValue;
    } catch (e) {
      return "-";
    }
  }

  Widget buildShortDesc(Loaded state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(20),
          color: Colors.transparent,
          child: Text(
            "${state.data.businessDescription}",
            textAlign: TextAlign.justify,
            style: TextStyle(color: Color(ColorRev.mainwhite)),
          ),
        ),
        Container(height: 5),
        // Container(
        //   padding: EdgeInsets.all(Sizes.s20),
        //   color: Colors.white,
        //   child: Container(
        //     width: double.infinity,
        //     height: Sizes.s40,
        //     // child: SantaraOutlineButton(
        //     //   title: "Prospektus",
        //     //   onPressed: () => Navigator.push(
        //     //     context,
        //     //     MaterialPageRoute(
        //     //       builder: (context) => ProspectusPage(
        //     //         uuid: state.data.uuid,
        //     //       ),
        //     //     ),
        //     //   ),
        //     // ),
        //     child: _prospektusDownloadBtn(
        //       context,
        //       "Prospektus",
        //       "${state.data.prospektus}",
        //     ),
        //   ),
        // ),
        // state.data.prospektus == null
        //     ? Container()
        //     : Container(
        //         padding: EdgeInsets.all(Sizes.s20),
        //         color: Colors.white,
        //         child: Container(
        //           width: double.infinity,
        //           height: Sizes.s40,
        //           child: SantaraOutlineButton(
        //             title: "Prospektus",
        //             onPressed: () => Navigator.push(
        //               context,
        //               MaterialPageRoute(
        //                 builder: (context) => ProspectusPage(
        //                   uuid: state.data.uuid,
        //                 ),
        //               ),
        //             ),
        //           ),
        // child: _prospektusDownloadBtn(
        //   context,
        //   "Prospektus",
        //   "${state.data.prospektus}",
        // ),
        //         ),
        //       ),
        Container(height: Sizes.s5),
        // Rencana Investasi
        // Container(
        //   color: Colors.tra,
        //   padding: EdgeInsets.all(Sizes.s20),
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Text(
        //         "Apakah anda ingin mengajukan penerbit ini listing di Santara?",
        //         style: TextStyle(
        //             fontWeight: FontWeight.bold, fontSize: FontSize.s14),
        //         textAlign: TextAlign.center,
        //       ),
        //       SizedBox(height: Sizes.s10),
        //       state.data.isVote == 1
        //           ? Column(
        //               mainAxisSize: MainAxisSize.min,
        //               crossAxisAlignment: CrossAxisAlignment.start,
        //               children: <Widget>[
        //                 SantaraOutlineButton(
        //                   title: "Batalkan Pengajuan",
        //                   onPressed: () async {
        //                     pralistingBloc
        //                       ..add(
        //                         VoteEmiten(
        //                           uuid: "${state.data.uuid}",
        //                           vote: false,
        //                         ),
        //                       );
        //                     await Future.delayed(Duration(milliseconds: 1000));
        //                     if (state.data.investmentPlan != null
        //                         ? true
        //                         : false) {
        //                       invesCtrl.clear();
        //                       pralistingBloc
        //                         ..add(
        //                           CancelInvestmentPlan(
        //                             uuid: "${state.data.uuid}",
        //                           ),
        //                         );
        //                       await Future.delayed(Duration(milliseconds: 200));
        //                       pralistingBloc
        //                         ..add(
        //                           LoadPralistingDetail(
        //                             uuid: "${widget.uuid}",
        //                           ),
        //                         );
        //                     }
        //                   },
        //                 ),
        //                 Container(height: Sizes.s20),
        //                 Text(
        //                   "Masukan rencana investasi jika bisnis ini listing di Santara",
        //                   style: TextStyle(
        //                     fontWeight: FontWeight.bold,
        //                     fontSize: FontSize.s14,
        //                   ),
        //                   textAlign: TextAlign.center,
        //                 ),
        //                 Container(height: Sizes.s20),
        //                 Container(
        //                   width: double.infinity,
        //                   child: Row(
        //                     mainAxisSize: MainAxisSize.max,
        //                     crossAxisAlignment: CrossAxisAlignment.center,
        //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                     children: <Widget>[
        //                       Text(
        //                         "Rencana Investasi",
        //                         style: TextStyle(
        //                           fontSize: FontSize.s14,
        //                           fontWeight: FontWeight.w600,
        //                         ),
        //                       ),
        //                       Container(
        //                         width: Sizes.s5,
        //                       ),
        //                       Container(
        //                           width: Sizes.s150,
        //                           // height: _size * 10,
        //                           child: TextFormField(
        //                             enabled: state.data.investmentPlan != null
        //                                 ? false
        //                                 : true,
        //                             controller: invesCtrl,
        //                             maxLines: 1,
        //                             keyboardType: TextInputType.number,
        //                             decoration: InputDecoration(
        //                               contentPadding: EdgeInsets.symmetric(
        //                                 horizontal: Sizes.s5,
        //                                 vertical: Sizes.s5,
        //                               ),
        //                               hintText: state.data.investmentPlan !=
        //                                       null
        //                                   ? RupiahFormatter.initialValueFormat(
        //                                       "${state.data.investmentPlan}")
        //                                   : "Rp 100,000",
        //                               border: OutlineInputBorder(
        //                                 borderSide: BorderSide(
        //                                     width: 1, color: Color(0xffDDDDDD)),
        //                                 borderRadius: BorderRadius.all(
        //                                   Radius.circular(5),
        //                                 ),
        //                               ),
        //                             ),
        //                             inputFormatters: [
        //                               WhitelistingTextInputFormatter.digitsOnly,
        //                               RupiahFormatter(maxDigits: 14),
        //                             ],
        //                             onSaved: (value) {
        //                               String _onlyDigits = value.replaceAll(
        //                                   RegExp('[^0-9]'), "");
        //                               double _doubleValue =
        //                                   double.parse(_onlyDigits);
        //                               print(_doubleValue);
        //                             },
        //                           )),
        //                     ],
        //                   ),
        //                 ),
        //                 Container(height: Sizes.s20),
        //                 SantaraMainButton(
        //                   isActive:
        //                       state.data.investmentPlan != null ? false : true,
        //                   title: "Ajukan Rencana Investasi",
        //                   onPressed: widget.status != null && widget.status < 3
        //                       ? () async {
        //                           _hideKeyboard();
        //                           var _amount = digitParser(
        //                             invesCtrl.text.isEmpty
        //                                 ? "100000"
        //                                 : invesCtrl.text,
        //                           );
        //                           if (_amount < 1) {
        //                             ToastHelper.showFailureToast(
        //                               context,
        //                               "Mohon masukan nilai yang valid!",
        //                             );
        //                           } else if (_amount >
        //                               state.data.capitalNeeds) {
        //                             ToastHelper.showFailureToast(
        //                               context,
        //                               "Nilai lebih besar dari kebutuhan!",
        //                             );
        //                           } else {
        //                             pralistingBloc
        //                               ..add(
        //                                 SubmitInvestmentPlan(
        //                                   uuid: "${state.data.uuid}",
        //                                   amount: _amount,
        //                                 ),
        //                               );
        //                             pralistingBloc
        //                               ..add(
        //                                 LoadPralistingDetail(
        //                                   uuid: "${widget.uuid}",
        //                                 ),
        //                               );
        //                           }
        //                         }
        //                       : null,
        //                 )
        //               ],
        //             )
        //           : SantaraMainButton(
        //               title: "Ajukan Rencana Investasi",
        //               onPressed: widget.status != null && widget.status < 3
        //                   ? () => pralistingBloc
        //                     ..add(
        //                       VoteEmiten(
        //                         uuid: "${state.data.uuid}",
        //                         vote: true,
        //                       ),
        //                     )
        //                   : null,
        //             )
        //     ],
        //   ),
        // ),
        SizedBox(height: Sizes.s5),
        _blocsLoaded
            ? PralistingCommentsSection(
                uuid: widget.uuid,
                status: widget.status,
                blocs: pralistingBlocsData,
              )
            : Text("UNLOADED"),
        _buildCommentField()
      ],
    );
  }

  Widget buildIdentity(Loaded state) {
    return Container(
      color: Colors.transparent,
      child: ListView(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          // ${emitenDetail.category}
          ListTile(
            title: Text("Nama Perusahaan",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.companyName}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Bentuk Legalitas Usaha",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.businessEntity}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Lama Usaha",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.businessLifespan} Bulan",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Jumlah Cabang",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.branchCompany} Cabang",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Jumlah Karyawan",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.employee} Karyawan",
                style: TextStyle(color: Colors.grey)),
          ),
        ],
      ),
    );
  }

  Widget financialInfo(Loaded state) {
    return Container(
      margin: EdgeInsets.only(bottom: 0),
      color: Colors.transparent,
      child: ListView(shrinkWrap: true, physics: NeverScrollableScrollPhysics(),
          // primary: false,
          children: [
            ListTile(
              title: Text("Besar Kebutuhan Dana",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.capitalNeeds}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Rata-rata omset perbulan saat ini",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.monthlyTurnover}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Rata-rata laba per bulan saat ini",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.monthlyProfit}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Rata-rata omzet per bulan tahun sebelumnya",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.monthlyTurnoverPreviousYear}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Rata-rata laba per bulan tahun sebelumnya",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.monthlyProfitPreviousYear}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Total Hutang Bank/Lembaga Pembiayaan",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.totalBankDebt}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Nama Bank/Lembaga Pembiayaan",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                state.data.bankNameFinancing ?? "-",
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Total Modal disetor",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.totalPaidCapital}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              title: Text("Nilai Per-lembar saham",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              subtitle: Text(
                rupiahText("${state.data.price}"),
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
              ),
            ),
          ]),
    );
  }

  Widget nonFinancialInfo(Loaded state) {
    return Container(
      color: Colors.transparent,
      child: ListView(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListTile(
            title: Text("Sistem Pencatatan Keuangan",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.financialRecordingSystem ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Reputasi Pinjaman Bank/Lainnya",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.bankLoanReputation ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Posisi pasar atas Produk dan/Jasa",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.marketPositionForTheProduct ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Stategi kedepan",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.strategyEmiten ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Status Lokasi/Kantor/Tempat Usaha",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.officeStatus ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Tingkat Persaingan",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.levelOfBusinessCompetition ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Kemampuan Manajerial",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.managerialAbility ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile(
            title: Text("Kemampuan Teknis",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            subtitle: Text("${state.data.technicalAbility ?? '-'}",
                style: TextStyle(color: Colors.grey)),
          ),
          ListTile()
        ],
      ),
    );
  }

  Widget locationInfo(Loaded state) {
    return Container(
      color: Colors.transparent,
      // padding: EdgeInsets.only(bottom: Sizes.s30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            onTap: () async {
              String url = '';
              if (Platform.isAndroid) {
                url = "https://maps.google.com/maps?q=${state.data.address}";
              } else if (Platform.isIOS) {
                url = "https://maps.apple.com/?q=${state.data.address}";
              } else {
                url = '';
              }
              if (await canLaunch(url)) {
                await launch(url);
              }
            },
            title: Text(
              "Alamat Lengkap",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: FontSize.s14,
                  color: Colors.white),
            ),
            subtitle: Text(
              "${state.data.address}",
              style: TextStyle(
                color: Color(0xff58A7FF),
                fontSize: FontSize.s12,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          ListTile(
            title: Text(
              "Kota Lokasi Usaha",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: FontSize.s14,
                  color: Colors.white),
            ),
            subtitle: Text(
              "${state.data.regency}",
              style: TextStyle(
                color: Color(0xff58A7FF),
                fontSize: FontSize.s12,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _prospektusDownloadBtn(
    BuildContext context,
    String title,
    String url,
  ) {
    var _prospektusUrl = "$apiLocalImage/uploads/prospektus/$url";
    return InkWell(
      onTap: () async {
        await _downloadProspektus(_prospektusUrl);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Color(0xffDADADA)),
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text("$title", style: TextStyle(fontSize: FontSize.s16)),
            Container(width: Sizes.s10),
            Container(
              width: Sizes.s15,
              height: Sizes.s15,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Color(0xff218196),
                ),
                shape: BoxShape.circle,
              ),
              child: Icon(
                Icons.arrow_downward,
                color: Color(0xff218196),
                size: FontSize.s10,
              ),
            )
          ],
        ),
      ),
    );
  }

  _downloadProspektus(String url) async {
    // var newUrl = "https://docs.google.com/viewer?url=$url&embedded=true&a=bi&pagenumber=12";
    if (await canLaunch(url)) {
      try {
        await launch(url);
      } catch (e) {
        // print(">> Err : $e");
      }
    }
  }

  buildBody(BuildContext context) {
    return BlocBuilder<PralistingBloc, PralistingState>(
      builder: (context, state) {
        if (state is Empty) {
          return Center(
            child: Text("No Data Available"),
          );
        } else if (state is Loading) {
          return Center(
            child: CupertinoActivityIndicator(
              radius: 50,
            ),
          );
        } else if (state is Loaded) {
          var data = state.data;
          double _progress =
              (state.progress * 100) > 100 ? 100 : (state.progress * 100);
          return ListView(
            children: [
              // Image Section
              Container(
                margin: EdgeInsets.only(left: Sizes.s20, top: Sizes.s20),
                height: Sizes.s180,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: state.pictures.length,
                  itemBuilder: (context, index) {
                    String image = state.pictures[index];
                    return index == 0
                        ? data.videoUrl == null || data.videoUrl.isEmpty
                            ? PralistingCoverImage(picture: image)
                            : YoutubeCoverImage(url: data.videoUrl)
                        : PralistingCoverImage(picture: image);
                  },
                ),
              ),
              // Basic Info Section
              Container(
                margin: EdgeInsets.all(Sizes.s20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.position != null
                        ? Text(
                            "#${widget.position} BANYAK DIAJUKAN",
                            style: TextStyle(
                              color: Color(0xffBF2D30),
                              fontWeight: FontWeight.w600,
                              fontSize: FontSize.s14,
                            ),
                          )
                        : Container(),
                    SizedBox(height: Sizes.s10),
                    Container(
                      decoration: BoxDecoration(
                          color: Color(0xff7F1D1D),
                          border: Border.all(
                            color: Color(0xff7F1D1D),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 5),
                        child: Text(
                          "${data.category}",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            // color: Color(ColorRev.maingrey),
                            fontSize: 10,
                          ),
                          overflow: TextOverflow.visible,
                        ),
                      ),
                    ),
                    SizedBox(height: Sizes.s10),
                    Text(
                      "${data.trademark}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: FontSize.s16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${data.companyName}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: FontSize.s12,
                      ),
                    ),
                  ],
                ),
              ),
              // Stats Section
              Container(
                margin: EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Likes & Submission
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        TextIconView(
                          iconData: Icons.person,
                          text: "${data.totalVotes} Orang Peminat",
                        ),
                        Container(
                          width: 1,
                          height: 20,
                          color: Colors.grey,
                        ),
                        TextIconView(
                          iconData: data.isLikes == 1
                              ? Icons.favorite
                              : Icons.favorite_border,
                          text: "${data.totalLikes} Orang Menyukai",
                        ),
                      ],
                    ),
                    SizedBox(height: Sizes.s20),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 1,
                      color: Colors.grey,
                    ),
                    SizedBox(height: Sizes.s20),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              data.isLikes == 1
                                  ? Icon(Icons.favorite,
                                      size: 30,
                                      color: Color(ColorRev.mainwhite))
                                  : Icon(Icons.favorite_border,
                                      size: 30,
                                      color: Color(ColorRev.mainwhite)),
                              SizedBox(height: Sizes.s10),
                              Text(
                                "Suka",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.person_add_alt,
                                  size: 30, color: Color(ColorRev.mainwhite)),
                              SizedBox(height: Sizes.s10),
                              Text(
                                "Minat",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(
                                'assets/icon/send-2.png',
                                width: 60,
                                height: 30,
                              ),
                              SizedBox(height: Sizes.s10),
                              Text(
                                "Bagikan",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: FontSize.s14,
                                ),
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () async {
                              var _prospektusUrl =
                                  "$apiLocalImage/uploads/prospektus/${(state.data.prospektus)}";
                              await _downloadProspektus(_prospektusUrl);
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image.asset(
                                  'assets/icon/icon-prespektif-download.png',
                                  width: 60,
                                  height: 30,
                                ),
                                SizedBox(height: Sizes.s10),
                                Text(
                                  "Prospektus",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: FontSize.s14,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    // Progreess Bar
                    // LinearPercentIndicator(
                    //   alignment: MainAxisAlignment.center,
                    //   animation: true,
                    //   animationDuration: 2000,
                    //   width: MediaQuery.of(context).size.width - Sizes.s40,
                    //   lineHeight: 22,
                    //   percent: num.parse(
                    //     state.progress != null && state.progress > 0
                    //         ? state.progress.toStringAsFixed(1)
                    //         : "0.0",
                    //   ),
                    //   linearStrokeCap: LinearStrokeCap.roundAll,
                    //   progressColor: Color(0xFF0E7E4A),
                    //   center: Text(
                    //     '${_progress.isNaN ? 0 : _progress.toStringAsFixed(2)} %',
                    //     style: TextStyle(
                    //       color: Colors.white,
                    //       fontSize: FontSize.s14,
                    //     ),
                    //   ),
                    // ),
                    SizedBox(height: Sizes.s5),
                    // Progress Text
                    // Text(
                    //   "${state.currentFunding}",
                    //   style: TextStyle(
                    //     fontWeight: FontWeight.w600,
                    //     fontSize: FontSize.s14,
                    //   ),
                    // ),
                  ],
                ),
              ),
              // Pralisting Tabs
              SizedBox(height: Sizes.s10),
              Container(
                height: 0.5,
                width: double.maxFinite,
                color: Colors.grey,
              ),
              SantaraPralistingTabs(
                about: buildIdentity(state),
                shortDesc: buildShortDesc(state),
                financial: financialInfo(state),
                nonFinancial: nonFinancialInfo(state),
                location: locationInfo(state),
              ),
            ],
          );
        } else if (state is Error) {
          return Center(
            child: Text("${state.message}"),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class YoutubeCoverImage extends StatelessWidget {
  final String url;
  YoutubeCoverImage({@required this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: Sizes.s10),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => YoutubePlayerUI(
                url: "$url",
              ),
            ),
          );
        },
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Container(
            width: Sizes.s300,
            child: Stack(
              children: [
                SantaraCachedImage(
                  image: YoutubeThumbnailGenerator.getThumbnail(
                    url,
                    ThumbnailQuality.medium,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[850].withOpacity(0.7),
                      borderRadius: BorderRadius.all(
                        Radius.circular(50.0),
                      ),
                    ),
                    height: Sizes.s50,
                    width: Sizes.s50,
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: Sizes.s30,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PralistingCoverImage extends StatelessWidget {
  final String picture;
  PralistingCoverImage({@required this.picture});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        var header = await UserAgent.headers();
        print(picture);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HeroPhotoViewWrapper(
              tag: "$picture",
              imageProvider: NetworkImage("$picture", headers: header),
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Hero(
          tag: "$picture",
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: SantaraCachedImage(
              width: Sizes.s300,
              image: "$picture",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
