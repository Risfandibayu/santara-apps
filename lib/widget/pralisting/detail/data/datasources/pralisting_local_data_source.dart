import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/models/User.dart';
import 'package:meta/meta.dart';

abstract class PralistingLocalDataSource {
  Future<bool> get isTutorialHasPassed;
  Future<User> get userData;
}

class PralistingLocalDataSourceImpl implements PralistingLocalDataSource {
  final FlutterSecureStorage secureStorage;
  PralistingLocalDataSourceImpl({@required this.secureStorage});
  @override
  Future<bool> get isTutorialHasPassed async => true;

  @override
  Future<User> get userData async =>
      userFromJson(await secureStorage.read(key: 'user'));
}
