import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/entities/pralisting_model.dart';
import 'package:meta/meta.dart';

abstract class PralistingRemoteDataSource {
  Future<PralistingModel> getPralistingDetail({@required String uuid});
  Future<bool> postVotePralisting({@required String uuid});
  Future<bool> postCancelVotePralisting({@required String uuid});
  Future<String> postInvestmentPlan(
      {@required String uuid, @required int amount});
  Future<CommentModel> getPralistingComments({@required String uuid});
  Future<bool> likePralisting({@required String uuid});
  Future<bool> sendComment({@required String uuid, @required String comment});
  Future<bool> sendReply({@required String uuid, @required String comment});
  Future<bool> deleteComment({@required String uuid});
  Future<String> sendReport({@required String uuid, @required String comment});
}

class PralistingRemoteDataSourceImpl implements PralistingRemoteDataSource {
  final RestClient client;
  PralistingRemoteDataSourceImpl({@required this.client});

  dynamic _parseApiRequest({
    @required Response response,
    String errorMessage,
  }) {
    try {
      switch (response.statusCode) {
        case 200:
          return response.data;
          break;
        default:
          throw ServerFailure(message: response.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(message: result.message);
          break;
        case 500:
          throw ServerFailure(message: "$errorMessage");
          break;
        default:
          throw ServerFailure(
              message: "[${result.response.statusCode}] An error has occured");
          break;
      }
    } catch (e, stack) {
      santaraLog(">> _parseApiRequest error : \n" + e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Memuat Data!",
      );
    }
  }

  @override
  Future<PralistingModel> getPralistingDetail({String uuid}) async {
    final result = await client.get(path: '/emitens/pre-listing/$uuid');
    final pralistingDetail = _parseApiRequest(
      response: result,
      errorMessage: "Terjadi kesalahan saat menerima data pralisting!",
    );
    final data = PralistingModel.fromJson(pralistingDetail);
    return data;
  }

  @override
  Future<bool> postVotePralisting({String uuid}) async {
    final result = await client.post(path: '/emitens/pre-listing/vote/$uuid');
    final votePralisting = _parseApiRequest(
      response: result,
      errorMessage: "Terjadi Kesalahan Saat Mengajukan Rencana Investasi!",
    );
    if (votePralisting != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> postCancelVotePralisting({String uuid}) async {
    final result = await client.post(
        path: '/emitens/pre-listing/cancel-investment-plan/$uuid');
    final postCancelPralisting = _parseApiRequest(
      response: result,
      errorMessage: "Terjadi Kesalahan Saat Membatalkan Rencana Investasi!",
    );
    if (postCancelPralisting != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<String> postInvestmentPlan({String uuid, int amount}) async {
    try {
      final result = await client.post(
        path: '/emitens/pre-listing/investment-plan',
        body: {"emiten_uuid": "$uuid", "amount": "$amount"},
      );
      switch (result.statusCode) {
        case 200:
          return result.data["message"] ??
              "Berhasil Mengajukan Rencana Investasi!";
          break;
        case 404:
          throw ServerFailure(message: "Akun anda belum diverifikasi!");
        default:
          throw ServerFailure(message: result.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(message: result.message);
          break;
        case 500:
          throw ServerFailure(
            message: "Terjadi Kesalahan Saat Mengajukan Rencana Investasi!",
          );
          break;
        default:
          throw ServerFailure(
            message: "[${result.response.statusCode}] An error has occured",
          );
          break;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Memuat Data!",
      );
    }
  }

  @override
  Future<CommentModel> getPralistingComments({String uuid}) async {
    final result =
        await client.get(path: '/emitens/pre-listing/comments/$uuid');
    final pralistingComments = _parseApiRequest(
      response: result,
      errorMessage: "Terjadi kesalahan saat mencoba mengambil data!",
    );
    final data = CommentModel.fromJson(pralistingComments);
    return data;
  }

  @override
  Future<bool> likePralisting({String uuid}) async {
    final result = await client.post(path: '/emitens/pre-listing/like/$uuid');
    if (result.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> sendComment({String uuid, String comment}) async {
    try {
      final result = await client.post(
        path: '/emitens/pre-listing/comment/$uuid',
        body: {'uuid': uuid, 'comment': comment},
      );
      switch (result.statusCode) {
        case 200:
          return true;
          break;
        case 404:
          throw ServerFailure(message: "Akun anda belum diverifikasi!");
        default:
          throw ServerFailure(message: result.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(
            message: result.response.data["message"] != null
                ? result.response.data["message"]
                : result.message,
          );
          break;
        case 500:
          throw ServerFailure(
            message: "Terjadi Kesalahan Saat Mengirimkan Komentar!",
          );
          break;
        default:
          throw ServerFailure(
            message: "[${result.response.statusCode}] An error has occured",
          );
          break;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Mengirim Data!",
      );
    }
  }

  @override
  Future<bool> sendReply({String uuid, String comment}) async {
    try {
      final result = await client.post(
        path: '/emitens/pre-listing/reply-comment/$uuid',
        body: {'uuid': uuid, 'comment': comment},
      );
      switch (result.statusCode) {
        case 200:
          return true;
          break;
        case 404:
          throw ServerFailure(message: "Akun anda belum diverifikasi!");
        default:
          throw ServerFailure(message: result.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(
            message: result.response.data["message"] != null
                ? result.response.data["message"]
                : result.message,
          );
          break;
        case 500:
          throw ServerFailure(
            message: "Terjadi Kesalahan Saat Mengirimkan Komentar!",
          );
          break;
        default:
          throw ServerFailure(
            message: "[${result.response.statusCode}] An error has occured",
          );
          break;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Mengirim Data!",
      );
    }
  }

  @override
  Future<bool> deleteComment({String uuid}) async {
    try {
      final result = await client.post(
        path: '/emitens/pre-listing/delete-comment/$uuid',
      );
      switch (result.statusCode) {
        case 200:
          return true;
          break;
        case 404:
          throw ServerFailure(message: "Akun anda belum diverifikasi!");
        default:
          throw ServerFailure(message: result.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(
            message: result.response.data["message"] != null
                ? result.response.data["message"]
                : result.message,
          );
          break;
        case 500:
          throw ServerFailure(
            message: "Terjadi Kesalahan Saat Menghapus Komentar!",
          );
          break;
        default:
          throw ServerFailure(
            message: "[${result.response.statusCode}] An error has occured",
          );
          break;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Menghapus Data!",
      );
    }
  }

  @override
  Future<String> sendReport({String uuid, String comment}) async {
    try {
      final result = await client.post(
          path: '/emitens/pre-listing/report/$uuid',
          body: {"description": "$comment"}).timeout(Duration(seconds: 60));
      switch (result.statusCode) {
        case 200:
          return result.data["message"];
          break;
        case 404:
          throw ServerFailure(message: "Akun anda belum diverifikasi!");
        default:
          throw ServerFailure(message: result.statusMessage);
          break;
      }
    } on DioError catch (result) {
      switch (result.response.statusCode) {
        case 400:
          throw ServerFailure(
            message: result.response.data["message"] != null
                ? result.response.data["message"]
                : result.message,
          );
          break;
        case 500:
          throw ServerFailure(
            message: "Terjadi Kesalahan Saat Menghapus Komentar!",
          );
          break;
        default:
          throw ServerFailure(
            message: "[${result.response.statusCode}] An error has occured",
          );
          break;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      throw LocalFailure(
        message: "Terjadi Kesalahan Saat Mencoba Menghapus Data!",
      );
    }
  }
}
