import 'package:formz/formz.dart';

enum CommentValidationError { empty, invalidFormat }

class CommentFieldModel extends FormzInput<String, CommentValidationError> {
  const CommentFieldModel.pure() : super.pure('');
  const CommentFieldModel.dirty([String value = '']) : super.dirty(value);

  @override
  CommentValidationError validator(String value) {
    if (value?.isNotEmpty == true) {
      if (RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true)
          .hasMatch(value)) {
        return CommentValidationError.invalidFormat;
      } else {
        return null;
      }
    } else {
      return CommentValidationError.empty;
    }
  }
}
