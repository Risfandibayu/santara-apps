import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/detail/data/datasources/pralisting_local_data_source.dart';
import 'package:santaraapp/widget/pralisting/detail/data/datasources/pralisting_remote_data_source.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/entities/pralisting_model.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/repositories/pralisting_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

class PralistingRepositoryImpl implements PralistingRepository {
  final NetworkInfo networkInfo;
  final PralistingRemoteDataSource remoteDataSource;
  final PralistingLocalDataSource localDataSource;

  PralistingRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
    @required this.localDataSource,
  });

  @override
  Future<Either<Failure, PralistingModel>> getPralistingDetail(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getPralistingDetail(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, bool>> postVotePralisting({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.postVotePralisting(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, bool>> postCancelVotePralisting({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await remoteDataSource.postCancelVotePralisting(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, String>> postInvestmentPlan(
      {String uuid, int amount}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.postInvestmentPlan(
            uuid: uuid, amount: amount);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, CommentModel>> getPralistingComments(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getPralistingComments(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, bool>> likePralisting({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.likePralisting(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, User>> getUserData() async {
    try {
      final result = await localDataSource.userData;
      return Right(result);
    } catch (e, stack) {
      santaraLog(e, stack);
      return Left(LocalFailure(message: "An error has occured!"));
    }
  }

  @override
  Future<Either<Failure, bool>> sendComment(
      {String uuid, String comment}) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await remoteDataSource.sendComment(uuid: uuid, comment: comment);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, bool>> sendReply({String uuid, String comment}) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await remoteDataSource.sendReply(uuid: uuid, comment: comment);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, bool>> deleteComment({String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.deleteComment(uuid: uuid);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }

  @override
  Future<Either<Failure, String>> sendReport(
      {String uuid, String comment}) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await remoteDataSource.sendReport(uuid: uuid, comment: comment);
        return Right(result);
      } on Failure catch (e) {
        return Left(ServerFailure(message: e.message));
      } catch (e, stack) {
        santaraLog(e, stack);
        return Left(LocalFailure(message: "An error has occured!"));
      }
    } else {
      return Left(NoConnection());
    }
  }
}
