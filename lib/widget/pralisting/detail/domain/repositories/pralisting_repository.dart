import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/entities/pralisting_model.dart';
import 'package:meta/meta.dart';

abstract class PralistingRepository {
  Future<Either<Failure, PralistingModel>> getPralistingDetail(
      {@required String uuid});
  Future<Either<Failure, bool>> postVotePralisting({@required String uuid});
  Future<Either<Failure, bool>> postCancelVotePralisting(
      {@required String uuid});
  Future<Either<Failure, String>> postInvestmentPlan(
      {@required String uuid, int amount});
  Future<Either<Failure, CommentModel>> getPralistingComments(
      {@required String uuid});
  Future<Either<Failure, bool>> likePralisting({@required String uuid});
  Future<Either<Failure, User>> getUserData();
  Future<Either<Failure, bool>> sendComment(
      {@required String uuid, @required String comment});
  Future<Either<Failure, bool>> sendReply(
      {@required String uuid, @required String comment});
  Future<Either<Failure, bool>> deleteComment({@required String uuid});
  Future<Either<Failure, String>> sendReport({
    @required String uuid,
    @required String comment,
  });
}
