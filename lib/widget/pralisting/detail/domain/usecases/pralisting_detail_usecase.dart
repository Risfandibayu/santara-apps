import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/entities/pralisting_model.dart';
import 'package:santaraapp/widget/pralisting/detail/domain/repositories/pralisting_repository.dart';

class GetPralistingDetail implements UseCase<PralistingModel, String> {
  final PralistingRepository repository;
  GetPralistingDetail(this.repository);

  @override
  Future<Either<Failure, PralistingModel>> call(String uuid) async {
    return await repository.getPralistingDetail(uuid: uuid);
  }
}

class PostVotePralisting implements UseCase<bool, String> {
  final PralistingRepository repository;
  PostVotePralisting(this.repository);

  @override
  Future<Either<Failure, bool>> call(String uuid) async {
    return await repository.postVotePralisting(uuid: uuid);
  }
}

class PostCancelVotePralisting implements UseCase<bool, String> {
  final PralistingRepository repository;
  PostCancelVotePralisting(this.repository);

  @override
  Future<Either<Failure, bool>> call(String uuid) async {
    return await repository.postCancelVotePralisting(uuid: uuid);
  }
}

class PostInvestmentPlanPralisting
    implements UseCase<String, Map<String, dynamic>> {
  final PralistingRepository repository;
  PostInvestmentPlanPralisting(this.repository);

  @override
  Future<Either<Failure, String>> call(Map<String, dynamic> body) async {
    return await repository.postInvestmentPlan(
      uuid: body["uuid"],
      amount: body["amount"],
    );
  }
}

class GetPralistingComments implements UseCase<CommentModel, String> {
  final PralistingRepository repository;
  GetPralistingComments(this.repository);

  @override
  Future<Either<Failure, CommentModel>> call(String uuid) async {
    return await repository.getPralistingComments(uuid: uuid);
  }
}

class LikePralisting implements UseCase<bool, String> {
  final PralistingRepository repository;
  LikePralisting(this.repository);

  @override
  Future<Either<Failure, bool>> call(String uuid) async {
    return await repository.likePralisting(uuid: uuid);
  }
}

class GetUserData implements UseCase<User, NoParams> {
  final PralistingRepository repository;
  GetUserData(this.repository);

  @override
  Future<Either<Failure, User>> call(NoParams noParams) async {
    return await repository.getUserData();
  }
}

class SendComment implements UseCase<bool, Map<String, dynamic>> {
  final PralistingRepository repository;
  SendComment(this.repository);

  @override
  Future<Either<Failure, bool>> call(Map<String, dynamic> body) async {
    return await repository.sendComment(
      uuid: body['uuid'],
      comment: body['comment'],
    );
  }
}

class SendReply implements UseCase<bool, Map<String, dynamic>> {
  final PralistingRepository repository;
  SendReply(this.repository);

  @override
  Future<Either<Failure, bool>> call(Map<String, dynamic> body) async {
    return await repository.sendReply(
      uuid: body['uuid'],
      comment: body['comment'],
    );
  }
}

class SendReport implements UseCase<String, Map<String, dynamic>> {
  final PralistingRepository repository;
  SendReport(this.repository);

  @override
  Future<Either<Failure, String>> call(Map<String, dynamic> body) async {
    return await repository.sendReport(
      uuid: body['uuid'],
      comment: body['comment'],
    );
  }
}

class DeleteComment implements UseCase<bool, String> {
  final PralistingRepository repository;
  DeleteComment(this.repository);

  @override
  Future<Either<Failure, bool>> call(String uuid) async {
    return await repository.deleteComment(uuid: uuid);
  }
}
