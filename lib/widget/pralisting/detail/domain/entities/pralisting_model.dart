import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:santaraapp/utils/api.dart';

part 'pralisting_model.g.dart';

@JsonSerializable()
class PralistingModel extends Equatable {
  @JsonKey(name: 'uuid')
  final String uuid;
  @JsonKey(name: 'company_name')
  final String companyName;
  @JsonKey(name: 'trademark')
  final String trademark;
  @JsonKey(name: 'category_id')
  final int categoryId;
  @JsonKey(name: 'category')
  final String category;
  @JsonKey(name: 'sub_category_id')
  final int subCategoryId;
  @JsonKey(name: 'address')
  final String address;
  @JsonKey(name: 'regency_id')
  final int regencyId;
  @JsonKey(name: 'regency')
  final String regency;
  @JsonKey(name: 'pictures')
  final List<Pictures> pictures;
  @JsonKey(name: 'prospektus')
  final String prospektus;
  @JsonKey(name: 'video_url')
  final String videoUrl;
  @JsonKey(name: 'slug')
  final String slug;
  @JsonKey(name: 'business_description')
  final String businessDescription;
  @JsonKey(name: 'business_entity')
  final String businessEntity;
  @JsonKey(name: 'business_lifespan')
  final int businessLifespan;
  @JsonKey(name: 'branch_company')
  final String branchCompany;
  @JsonKey(name: 'employee')
  final int employee;
  @JsonKey(name: 'capital_needs')
  final int capitalNeeds;
  @JsonKey(name: 'monthly_turnover')
  final int monthlyTurnover;
  @JsonKey(name: 'monthly_profit')
  final int monthlyProfit;
  @JsonKey(name: 'monthly_turnover_previous_year')
  final int monthlyTurnoverPreviousYear;
  @JsonKey(name: 'monthly_profit_previous_year')
  final int monthlyProfitPreviousYear;
  @JsonKey(name: 'total_bank_debt')
  final int totalBankDebt;
  @JsonKey(name: 'price')
  final int price;
  @JsonKey(name: 'bank_name_financing')
  final String bankNameFinancing;
  @JsonKey(name: 'total_paid_capital')
  final int totalPaidCapital;
  @JsonKey(name: 'financial_recording_system')
  final String financialRecordingSystem;
  @JsonKey(name: 'bank_loan_reputation')
  final String bankLoanReputation;
  @JsonKey(name: 'market_position_for_the_product')
  final String marketPositionForTheProduct;
  @JsonKey(name: 'strategy_emiten')
  final String strategyEmiten;
  @JsonKey(name: 'office_status')
  final String officeStatus;
  @JsonKey(name: 'level_of_business_competition')
  final String levelOfBusinessCompetition;
  @JsonKey(name: 'managerial_ability')
  final String managerialAbility;
  @JsonKey(name: 'technical_ability')
  final String technicalAbility;
  @JsonKey(name: 'total_investment_plans')
  final int totalInvestmentPlans;
  @JsonKey(name: 'investment_plan')
  final int investmentPlan;
  @JsonKey(name: 'total_likes')
  final int totalLikes;
  @JsonKey(name: 'total_votes')
  final int totalVotes;
  @JsonKey(name: 'total_comments')
  final int totalComments;
  @JsonKey(name: 'is_likes')
  final int isLikes;
  @JsonKey(name: 'is_vote')
  final int isVote;
  @JsonKey(name: 'is_comment')
  final int isComment;

  const PralistingModel({
    this.uuid,
    this.companyName,
    this.trademark,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.address,
    this.regencyId,
    this.regency,
    this.pictures,
    this.prospektus,
    this.videoUrl,
    this.slug,
    this.businessDescription,
    this.businessEntity,
    this.businessLifespan,
    this.branchCompany,
    this.employee,
    this.capitalNeeds,
    this.monthlyTurnover,
    this.monthlyProfit,
    this.monthlyTurnoverPreviousYear,
    this.monthlyProfitPreviousYear,
    this.totalBankDebt,
    this.price,
    this.bankNameFinancing,
    this.totalPaidCapital,
    this.financialRecordingSystem,
    this.bankLoanReputation,
    this.marketPositionForTheProduct,
    this.strategyEmiten,
    this.officeStatus,
    this.levelOfBusinessCompetition,
    this.managerialAbility,
    this.technicalAbility,
    this.totalInvestmentPlans,
    this.investmentPlan,
    this.totalLikes,
    this.totalVotes,
    this.totalComments,
    this.isLikes,
    this.isVote,
    this.isComment,
  });

  @override
  String toString() {
    return 'PralistingModel(uuid: $uuid, companyName: $companyName, trademark: $trademark, categoryId: $categoryId, category: $category, subCategoryId: $subCategoryId, address: $address, regencyId: $regencyId, regency: $regency, pictures: $pictures, prospektus: $prospektus, videoUrl: $videoUrl, slug: $slug, businessDescription: $businessDescription, businessEntity: $businessEntity, businessLifespan: $businessLifespan, branchCompany: $branchCompany, employee: $employee, capitalNeeds: $capitalNeeds, monthlyTurnover: $monthlyTurnover, monthlyProfit: $monthlyProfit, monthlyTurnoverPreviousYear: $monthlyTurnoverPreviousYear, monthlyProfitPreviousYear: $monthlyProfitPreviousYear, totalBankDebt: $totalBankDebt, price: $price, bankNameFinancing: $bankNameFinancing, totalPaidCapital: $totalPaidCapital, financialRecordingSystem: $financialRecordingSystem, bankLoanReputation: $bankLoanReputation, marketPositionForTheProduct: $marketPositionForTheProduct, strategyEmiten: $strategyEmiten, officeStatus: $officeStatus, levelOfBusinessCompetition: $levelOfBusinessCompetition, managerialAbility: $managerialAbility, technicalAbility: $technicalAbility, totalInvestmentPlans: $totalInvestmentPlans, investmentPlan: $investmentPlan, totalLikes: $totalLikes, totalVotes: $totalVotes, totalComments: $totalComments, isLikes: $isLikes, isVote: $isVote, isComment: $isComment)';
  }

  factory PralistingModel.fromJson(Map<String, dynamic> json) {
    return _$PralistingModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PralistingModelToJson(this);

  PralistingModel copyWith({
    String uuid,
    String companyName,
    String trademark,
    int categoryId,
    String category,
    int subCategoryId,
    String address,
    int regencyId,
    String regency,
    String pictures,
    String prospektus,
    String videoUrl,
    String slug,
    String businessDescription,
    String businessEntity,
    int businessLifespan,
    String branchCompany,
    int employee,
    int capitalNeeds,
    int monthlyTurnover,
    int monthlyProfit,
    int monthlyTurnoverPreviousYear,
    int monthlyProfitPreviousYear,
    int totalBankDebt,
    int price,
    String bankNameFinancing,
    int totalPaidCapital,
    String financialRecordingSystem,
    String bankLoanReputation,
    String marketPositionForTheProduct,
    String strategyEmiten,
    String officeStatus,
    String levelOfBusinessCompetition,
    String managerialAbility,
    String technicalAbility,
    int totalInvestmentPlans,
    int investmentPlan,
    int totalLikes,
    int totalVotes,
    int totalComments,
    int isLikes,
    int isVote,
    int isComment,
  }) {
    return PralistingModel(
      uuid: uuid ?? this.uuid,
      companyName: companyName ?? this.companyName,
      trademark: trademark ?? this.trademark,
      categoryId: categoryId ?? this.categoryId,
      category: category ?? this.category,
      subCategoryId: subCategoryId ?? this.subCategoryId,
      address: address ?? this.address,
      regencyId: regencyId ?? this.regencyId,
      regency: regency ?? this.regency,
      pictures: pictures ?? this.pictures,
      prospektus: prospektus ?? this.prospektus,
      videoUrl: videoUrl ?? this.videoUrl,
      slug: slug ?? this.slug,
      businessDescription: businessDescription ?? this.businessDescription,
      businessEntity: businessEntity ?? this.businessEntity,
      businessLifespan: businessLifespan ?? this.businessLifespan,
      branchCompany: branchCompany ?? this.branchCompany,
      employee: employee ?? this.employee,
      capitalNeeds: capitalNeeds ?? this.capitalNeeds,
      monthlyTurnover: monthlyTurnover ?? this.monthlyTurnover,
      monthlyProfit: monthlyProfit ?? this.monthlyProfit,
      monthlyTurnoverPreviousYear:
          monthlyTurnoverPreviousYear ?? this.monthlyTurnoverPreviousYear,
      monthlyProfitPreviousYear:
          monthlyProfitPreviousYear ?? this.monthlyProfitPreviousYear,
      totalBankDebt: totalBankDebt ?? this.totalBankDebt,
      price: price ?? this.price,
      bankNameFinancing: bankNameFinancing ?? this.bankNameFinancing,
      totalPaidCapital: totalPaidCapital ?? this.totalPaidCapital,
      financialRecordingSystem:
          financialRecordingSystem ?? this.financialRecordingSystem,
      bankLoanReputation: bankLoanReputation ?? this.bankLoanReputation,
      marketPositionForTheProduct:
          marketPositionForTheProduct ?? this.marketPositionForTheProduct,
      strategyEmiten: strategyEmiten ?? this.strategyEmiten,
      officeStatus: officeStatus ?? this.officeStatus,
      levelOfBusinessCompetition:
          levelOfBusinessCompetition ?? this.levelOfBusinessCompetition,
      managerialAbility: managerialAbility ?? this.managerialAbility,
      technicalAbility: technicalAbility ?? this.technicalAbility,
      totalInvestmentPlans: totalInvestmentPlans ?? this.totalInvestmentPlans,
      investmentPlan: investmentPlan ?? this.investmentPlan,
      totalLikes: totalLikes ?? this.totalLikes,
      totalVotes: totalVotes ?? this.totalVotes,
      totalComments: totalComments ?? this.totalComments,
      isLikes: isLikes ?? this.isLikes,
      isVote: isVote ?? this.isVote,
      isComment: isComment ?? this.isComment,
    );
  }

  @override
  List<Object> get props {
    return [
      uuid,
      companyName,
      trademark,
      categoryId,
      category,
      subCategoryId,
      address,
      regencyId,
      regency,
      pictures,
      prospektus,
      videoUrl,
      slug,
      businessDescription,
      businessEntity,
      businessLifespan,
      branchCompany,
      employee,
      capitalNeeds,
      monthlyTurnover,
      monthlyProfit,
      monthlyTurnoverPreviousYear,
      monthlyProfitPreviousYear,
      totalBankDebt,
      price,
      bankNameFinancing,
      totalPaidCapital,
      financialRecordingSystem,
      bankLoanReputation,
      marketPositionForTheProduct,
      strategyEmiten,
      officeStatus,
      levelOfBusinessCompetition,
      managerialAbility,
      technicalAbility,
      totalInvestmentPlans,
      investmentPlan,
      totalLikes,
      totalVotes,
      totalComments,
      isLikes,
      isVote,
      isComment,
    ];
  }
}
