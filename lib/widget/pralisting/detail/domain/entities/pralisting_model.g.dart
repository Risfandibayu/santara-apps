// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pralisting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PralistingModel _$PralistingModelFromJson(Map<String, dynamic> json) {
  return PralistingModel(
    uuid: json['uuid'] as String,
    companyName: json['company_name'] as String,
    trademark: json['trademark'] as String,
    categoryId: json['category_id'] as int,
    category: json['category'] as String,
    subCategoryId: json['sub_category_id'] as int,
    address: json['address'] as String,
    regencyId: json['regency_id'] as int,
    regency: json['regency'] as String,
    pictures:
        List<Pictures>.from(json["pictures"].map((x) => Pictures.fromJson(x))),
    prospektus: json['prospektus'] as String,
    videoUrl: json['video_url'] as String,
    slug: json['slug'] as String,
    businessDescription: json['business_description'] as String,
    businessEntity: json['business_entity'] as String,
    businessLifespan: json['business_lifespan'] as int,
    branchCompany: json['branch_company'] as String,
    employee: json['employee'] as int,
    capitalNeeds: json['capital_needs'] as int,
    monthlyTurnover: json['monthly_turnover'] as int,
    monthlyProfit: json['monthly_profit'] as int,
    monthlyTurnoverPreviousYear: json['monthly_turnover_previous_year'] as int,
    monthlyProfitPreviousYear: json['monthly_profit_previous_year'] as int,
    totalBankDebt: json['total_bank_debt'] as int,
    price: json['price'] as int,
    bankNameFinancing: json['bank_name_financing'] as String,
    totalPaidCapital: json['total_paid_capital'] as int,
    financialRecordingSystem: json['financial_recording_system'] as String,
    bankLoanReputation: json['bank_loan_reputation'] as String,
    marketPositionForTheProduct:
        json['market_position_for_the_product'] as String,
    strategyEmiten: json['strategy_emiten'] as String,
    officeStatus: json['office_status'] as String,
    levelOfBusinessCompetition: json['level_of_business_competition'] as String,
    managerialAbility: json['managerial_ability'] as String,
    technicalAbility: json['technical_ability'] as String,
    totalInvestmentPlans: json['total_investment_plans'] as int,
    investmentPlan: json['investment_plan'] as int,
    totalLikes: json['total_likes'] as int,
    totalVotes: json['total_votes'] as int,
    totalComments: json['total_comments'] as int,
    isLikes: json['is_likes'] as int,
    isVote: json['is_vote'] as int,
    isComment: json['is_comment'] as int,
  );
}

Map<String, dynamic> _$PralistingModelToJson(PralistingModel instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'company_name': instance.companyName,
      'trademark': instance.trademark,
      'category_id': instance.categoryId,
      'category': instance.category,
      'sub_category_id': instance.subCategoryId,
      'address': instance.address,
      'regency_id': instance.regencyId,
      'regency': instance.regency,
      'pictures': instance.pictures,
      'prospektus': instance.prospektus,
      'video_url': instance.videoUrl,
      'slug': instance.slug,
      'business_description': instance.businessDescription,
      'business_entity': instance.businessEntity,
      'business_lifespan': instance.businessLifespan,
      'branch_company': instance.branchCompany,
      'employee': instance.employee,
      'capital_needs': instance.capitalNeeds,
      'monthly_turnover': instance.monthlyTurnover,
      'monthly_profit': instance.monthlyProfit,
      'monthly_turnover_previous_year': instance.monthlyTurnoverPreviousYear,
      'monthly_profit_previous_year': instance.monthlyProfitPreviousYear,
      'total_bank_debt': instance.totalBankDebt,
      'price': instance.price,
      'bank_name_financing': instance.bankNameFinancing,
      'total_paid_capital': instance.totalPaidCapital,
      'financial_recording_system': instance.financialRecordingSystem,
      'bank_loan_reputation': instance.bankLoanReputation,
      'market_position_for_the_product': instance.marketPositionForTheProduct,
      'strategy_emiten': instance.strategyEmiten,
      'office_status': instance.officeStatus,
      'level_of_business_competition': instance.levelOfBusinessCompetition,
      'managerial_ability': instance.managerialAbility,
      'technical_ability': instance.technicalAbility,
      'total_investment_plans': instance.totalInvestmentPlans,
      'investment_plan': instance.investmentPlan,
      'total_likes': instance.totalLikes,
      'total_votes': instance.totalVotes,
      'total_comments': instance.totalComments,
      'is_likes': instance.isLikes,
      'is_vote': instance.isVote,
      'is_comment': instance.isComment,
    };

class Pictures {
  String picture;

  Pictures({this.picture});

  Pictures.fromJson(Map<String, dynamic> json) {
    picture = json['picture'].contains("http")
        ? json['picture']
        : apiLocalImage + "/uploads/emiten_picture/" + json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['picture'] = this.picture;
    return data;
  }
}
