import '../../../injector_container.dart';
import 'data/datasources/pralisting_local_data_source.dart';
import 'data/datasources/pralisting_remote_data_source.dart';
import 'data/repositories/pralisting_repository_impl.dart';
import 'domain/repositories/pralisting_repository.dart';
import 'domain/usecases/pralisting_detail_usecase.dart';
import 'presentation/bloc/pralisting_bloc/pralisting_bloc.dart';
import 'presentation/bloc/comments_bloc/pralisting_comments_bloc.dart';
import 'presentation/bloc/comment_field_bloc/comment_field_bloc.dart';

void initPralistingDetail() {
  sl.registerFactory(
    () => PralistingBloc(
      getPralistingDetail: sl(),
      postVotePralisting: sl(),
      postCancelVotePralisting: sl(),
      postInvestmentPlanPralisting: sl(),
    ),
  );

  sl.registerFactory(
    () => PralistingCommentsBloc(
      getPralistingComments: sl(),
      deleteComment: sl(),
      sendReport: sl(),
    ),
  );

  sl.registerFactory(
    () => PralistingCommentFieldBloc(
      likePralisting: sl(),
      getUserData: sl(),
      sendComment: sl(),
      sendReply: sl(),
    ),
  );

  //! Global Usecase
  sl.registerLazySingleton(() => GetPralistingDetail(sl()));
  sl.registerLazySingleton(() => PostVotePralisting(sl()));
  sl.registerLazySingleton(() => PostCancelVotePralisting(sl()));
  sl.registerLazySingleton(() => PostInvestmentPlanPralisting(sl()));
  sl.registerLazySingleton(() => GetPralistingComments(sl()));
  sl.registerLazySingleton(() => LikePralisting(sl()));
  sl.registerLazySingleton(() => GetUserData(sl()));
  sl.registerLazySingleton(() => SendComment(sl()));
  sl.registerLazySingleton(() => SendReply(sl()));
  sl.registerLazySingleton(() => SendReport(sl()));
  sl.registerLazySingleton(() => DeleteComment(sl()));

  //! Global Repository
  sl.registerLazySingleton<PralistingRepository>(
    () => PralistingRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
      localDataSource: sl(),
    ),
  );

  //! Data Source
  sl.registerLazySingleton<PralistingRemoteDataSource>(
    () => PralistingRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<PralistingLocalDataSource>(
    () => PralistingLocalDataSourceImpl(secureStorage: sl()),
  );
}
