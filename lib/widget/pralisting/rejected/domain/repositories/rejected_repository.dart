import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';

abstract class RejectedRepository {
  Future<Either<Failure, PralistingMetaModel>> getRejectedDetail({
    @required String uuid,
  });
}
