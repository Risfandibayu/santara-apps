import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/rejected/domain/repositories/rejected_repository.dart';

class GetPralistingRejected implements UseCase<PralistingMetaModel, dynamic> {
  final RejectedRepository repository;
  GetPralistingRejected(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(dynamic uuid) async {
    return await repository.getRejectedDetail(uuid: uuid);
  }
}
