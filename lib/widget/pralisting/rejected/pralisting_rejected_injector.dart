import 'package:santaraapp/widget/pralisting/rejected/data/datasources/rejected_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/rejected/domain/repositories/rejected_repository.dart';
import 'package:santaraapp/widget/pralisting/rejected/domain/usecases/rejected_usecases.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_bloc.dart';
import '../../../injector_container.dart';
import 'data/repositories/rejected_repository_impl.dart';

void initPralistingRejected() {
  // Bloc
  sl.registerFactory(
    () => PralistingRejectedBloc(
      getPralistingRejected: sl(),
    ),
  );

  //! Global Usecase
  sl.registerLazySingleton(() => GetPralistingRejected(sl()));

  //! Global Repository
  sl.registerLazySingleton<RejectedRepository>(
    () => RejectedRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<RejectedRemoteDataSources>(
    () => RejectedRemoteDataSourcesImpl(
      client: sl(),
    ),
  );
}
