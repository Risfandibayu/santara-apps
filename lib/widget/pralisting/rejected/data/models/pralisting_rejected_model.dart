// To parse this JSON data, do
//
//     final pralistingRejectedModel = pralistingRejectedModelFromJson(jsonString);

import 'dart:convert';

PralistingRejectedModel pralistingRejectedModelFromJson(String str) =>
    PralistingRejectedModel.fromJson(json.decode(str));

String pralistingRejectedModelToJson(PralistingRejectedModel data) =>
    json.encode(data.toJson());

class PralistingRejectedModel {
  PralistingRejectedModel({
    this.companyName,
    this.trademark,
    this.reject,
  });

  String companyName;
  String trademark;
  String reject;

  factory PralistingRejectedModel.fromJson(Map<String, dynamic> json) =>
      PralistingRejectedModel(
        companyName: json["company_name"] == null ? null : json["company_name"],
        trademark: json["trademark"] == null ? null : json["trademark"],
        reject: json["reject"] == null ? null : json["reject"],
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName == null ? null : companyName,
        "trademark": trademark == null ? null : trademark,
        "reject": reject == null ? null : reject,
      };
}
