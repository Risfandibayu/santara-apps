import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';
import 'package:santaraapp/utils/api.dart';

abstract class RejectedRemoteDataSources {
  Future<PralistingMetaModel> getRejectedDetail({@required String uuid});
}

class RejectedRemoteDataSourcesImpl implements RejectedRemoteDataSources {
  final RestClient client;
  RejectedRemoteDataSourcesImpl({@required this.client});

  @override
  Future<PralistingMetaModel> getRejectedDetail({String uuid}) async {
    try {
      final result = await client.getExternalUrl(
          url: "$apiPralisting/detail-reject/$uuid");
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
