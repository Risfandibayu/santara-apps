import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/rejected/data/datasources/rejected_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/rejected/domain/repositories/rejected_repository.dart';
import 'package:meta/meta.dart';

class RejectedRepositoryImpl implements RejectedRepository {
  final NetworkInfo networkInfo;
  final RejectedRemoteDataSources remoteDataSource;

  RejectedRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, PralistingMetaModel>> getRejectedDetail(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getRejectedDetail(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
