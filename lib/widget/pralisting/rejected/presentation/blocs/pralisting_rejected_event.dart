import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PralistingRejectedEvent extends Equatable {}

class LoadPralistingRejectedDetail extends PralistingRejectedEvent {
  final String uuid;
  LoadPralistingRejectedDetail({@required this.uuid});

  @override
  List<Object> get props => [uuid];
}
