import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/rejected/domain/usecases/rejected_usecases.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_event.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_state.dart';

class PralistingRejectedBloc
    extends Bloc<PralistingRejectedEvent, PralistingRejectedState> {
  PralistingRejectedBloc({
    @required GetPralistingRejected getPralistingRejected,
  })  : assert(
          getPralistingRejected != null,
        ),
        _getPralistingRejected = getPralistingRejected,
        super(PralistingRejectedUninitialized());

  final GetPralistingRejected _getPralistingRejected;

  @override
  Stream<PralistingRejectedState> mapEventToState(
    PralistingRejectedEvent event,
  ) async* {
    if (event is LoadPralistingRejectedDetail) {
      yield* _mapGetPralistingRejectedDetailToState(event, state);
    }
  }

  Stream<PralistingRejectedState> _mapGetPralistingRejectedDetailToState(
    LoadPralistingRejectedDetail event,
    PralistingRejectedState state,
  ) async* {
    try {
      yield PralistingRejectedLoading();
      final result = await _getPralistingRejected(event.uuid);
      yield* result.fold(
        (failure) async* {
          printFailure(failure);
          yield PralistingRejectedError(failure: failure);
        },
        (data) async* {
          yield PralistingRejectedLoaded(data: data);
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield PralistingRejectedError(
        failure: Failure(
          type: FailureType.localError,
          message: e,
        ),
      );
    }
  }
}
