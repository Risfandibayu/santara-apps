import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';

abstract class PralistingRejectedState extends Equatable {}

class PralistingRejectedUninitialized extends PralistingRejectedState {
  @override
  List<Object> get props => [];
}

class PralistingRejectedLoading extends PralistingRejectedState {
  @override
  List<Object> get props => [];
}

class PralistingRejectedLoaded extends PralistingRejectedState {
  final PralistingMetaModel data;

  PralistingRejectedLoaded({@required this.data});

  @override
  List<Object> get props => [data];
}

class PralistingRejectedError extends PralistingRejectedState {
  final Failure failure;
  PralistingRejectedError({@required this.failure});

  @override
  List<Object> get props => [failure];
}
