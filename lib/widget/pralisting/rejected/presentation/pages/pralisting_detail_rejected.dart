import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_bloc.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_event.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/blocs/pralisting_rejected_state.dart';
import '../../../../../injector_container.dart';

class PralistingDetailRejected extends StatefulWidget {
  final PralistingDatum data;
  PralistingDetailRejected({@required this.data});
  @override
  _PralistingDetailRejectedState createState() =>
      _PralistingDetailRejectedState();
}

class _PralistingDetailRejectedState extends State<PralistingDetailRejected> {
  PralistingRejectedBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<PralistingRejectedBloc>();
    bloc..add(LoadPralistingRejectedDetail(uuid: "${widget.data.uuid}"));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text(
                "Pengajuan Bisnis",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  fontSize: FontSize.s12,
                ),
              ),
              Text(
                "${widget.data.trademark}",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: FontSize.s18,
                ),
              ),
            ],
          ),
          centerTitle: true,
          elevation: 1.0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
          color: Colors.white,
          width: double.maxFinite,
          height: double.maxFinite,
          padding: EdgeInsets.all(Sizes.s20),
          child: BlocBuilder<PralistingRejectedBloc, PralistingRejectedState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is PralistingRejectedLoading) {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (state is PralistingRejectedError) {
                return Center(
                  child: Text("${state.failure.message}"),
                );
              } else if (state is PralistingRejectedLoaded) {
                String reason = state.data.data["reject"] ?? "";
                return Column(
                  children: [
                    Text(
                      "Alasan Penolakan",
                      style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(height: Sizes.s15),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(Sizes.s15),
                      height: Sizes.s180,
                      decoration: BoxDecoration(
                        color: Color(0xffF9F9F9),
                        borderRadius: BorderRadius.all(
                          Radius.circular(Sizes.s10),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffB8B8B8),
                        ),
                      ),
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Text(
                            "$reason",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }
}
