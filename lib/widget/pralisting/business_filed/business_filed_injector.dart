import 'package:santaraapp/widget/pralisting/business_filed/data/datasources/business_filed_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/business_filed/domain/repositories/business_filed_repository.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/blocs/business_filed_bloc.dart';

import '../../../injector_container.dart';
import 'data/repositories/business_filed_repository_impl.dart';
import 'domain/usecases/business_filed_usecases.dart';

void initBusinessFiled() {
  // Bloc
  sl.registerFactory(
    () => BusinessFiledBloc(
      deleteBusinessFiled: sl(),
      getBusinessFiled: sl(),
    ),
  );

  //! Global Usecase
  sl.registerLazySingleton(() => GetBusinessFiled(sl()));
  sl.registerLazySingleton(() => DeleteBusinessFiled(sl()));

  //! Global Repository
  sl.registerLazySingleton<BusinessFiledRepository>(
    () => BusinessFiledRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<BusinessFiledRemoteDataSource>(
    () => BusinessFiledRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
