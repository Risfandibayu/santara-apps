import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';

abstract class BusinessFiledRepository {
  Future<Either<Failure, PralistingMetaModel>> deletePralisting({
    @required String uuid,
  });
  Future<Either<Failure, PralistingListModel>> getPralistingList({
    @required String status,
  });
}
