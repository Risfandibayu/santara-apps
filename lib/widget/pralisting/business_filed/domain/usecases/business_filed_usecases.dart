import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:santaraapp/widget/pralisting/business_filed/domain/repositories/business_filed_repository.dart';

class GetBusinessFiled implements UseCase<PralistingListModel, dynamic> {
  final BusinessFiledRepository repository;
  GetBusinessFiled(this.repository);

  @override
  Future<Either<Failure, PralistingListModel>> call(dynamic status) async {
    return await repository.getPralistingList(status: status);
  }
}

class DeleteBusinessFiled implements UseCase<PralistingMetaModel, String> {
  final BusinessFiledRepository repository;
  DeleteBusinessFiled(this.repository);

  @override
  Future<Either<Failure, PralistingMetaModel>> call(String uuid) async {
    return await repository.deletePralisting(uuid: uuid);
  }
}
