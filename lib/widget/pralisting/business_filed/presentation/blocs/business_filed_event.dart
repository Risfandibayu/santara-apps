import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class BusinessFiledEvent extends Equatable {}

class LoadBusinessFiledList extends BusinessFiledEvent {
  final String status;
  LoadBusinessFiledList({@required this.status});

  @override
  List<Object> get props => [status];
}

class DeleteBusinessFiledData extends BusinessFiledEvent {
  final String uuid;
  DeleteBusinessFiledData({@required this.uuid});

  @override
  List<Object> get props => [uuid];
}
