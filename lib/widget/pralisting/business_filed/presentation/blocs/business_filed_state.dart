import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:meta/meta.dart';

abstract class BusinessFiledState extends Equatable {}

class BusinessFiledUninitialized extends BusinessFiledState {
  @override
  List<Object> get props => [];
}

class BusinessFiledLoading extends BusinessFiledState {
  @override
  List<Object> get props => [];
}

class BusinessFiledLoaded extends BusinessFiledState {
  final PralistingListModel pralisting;
  final Failure failure;
  final String success;
  final String selectedFilter;
  final bool isLoading;

  BusinessFiledLoaded({
    @required this.pralisting,
    this.failure,
    this.success,
    this.selectedFilter,
    this.isLoading,
  });

  BusinessFiledLoaded copyWith({
    PralistingListModel pralistingListModel,
    Failure failure,
    String success,
    String selectedFilter,
    bool isLoading,
  }) {
    return BusinessFiledLoaded(
      pralisting: pralisting ?? this.pralisting,
      failure: failure ?? this.failure,
      success: success ?? this.success,
      selectedFilter: selectedFilter ?? this.selectedFilter,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object> get props => [
        pralisting,
        failure,
        selectedFilter,
        success,
        isLoading,
      ];
}

class BusinessFiledError extends BusinessFiledState {
  final Failure failure;
  BusinessFiledError({@required this.failure});

  @override
  List<Object> get props => [failure];
}
