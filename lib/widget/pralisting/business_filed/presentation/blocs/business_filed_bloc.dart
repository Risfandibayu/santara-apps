import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../models/SearchDataModel.dart';
import '../../../../../utils/logger.dart';
import '../../domain/usecases/business_filed_usecases.dart';
import 'business_filed_event.dart';
import 'business_filed_state.dart';

class BusinessFiledBloc extends Bloc<BusinessFiledEvent, BusinessFiledState> {
  BusinessFiledBloc({
    @required GetBusinessFiled getBusinessFiled,
    @required DeleteBusinessFiled deleteBusinessFiled,
  })  : assert(
          getBusinessFiled != null,
          deleteBusinessFiled != null,
        ),
        _getBusinessFiled = getBusinessFiled,
        _deleteBusinessFiled = deleteBusinessFiled,
        super(BusinessFiledUninitialized());

  final GetBusinessFiled _getBusinessFiled;
  final DeleteBusinessFiled _deleteBusinessFiled;
  List<SearchData> filterItems = [
    SearchData(id: "0", uuid: "", name: "Semua"),
    SearchData(id: "1", uuid: "data_updated", name: "Belum Selesai"),
    SearchData(id: "2", uuid: "verifying", name: "Menunggu Verifikasi"),
    SearchData(
        id: "3", uuid: "verifying_by_ceo", name: "Menunggu Verifikasi CEO"),
    SearchData(id: "4", uuid: "passed_scoring", name: "Lolos scoring"),
    SearchData(id: "5", uuid: "failed_scoring", name: "Tidak lolos scoring"),
    SearchData(id: "6", uuid: "rejected", name: "Pengajuan ditolak"),
    SearchData(id: "7", uuid: "revision", name: "Revisi"),
    SearchData(id: "8", uuid: "verified", name: "Terverifikasi"),
  ];

  @override
  Stream<BusinessFiledState> mapEventToState(
    BusinessFiledEvent event,
  ) async* {
    if (event is LoadBusinessFiledList) {
      yield* _mapGetBusinessFiledListToState(event, state);
    } else if (event is DeleteBusinessFiledData) {
      yield* _mapDeleteBusinessFiled(event, state);
    }
  }

  Stream<BusinessFiledState> _mapGetBusinessFiledListToState(
    LoadBusinessFiledList event,
    BusinessFiledState state,
  ) async* {
    try {
      yield BusinessFiledLoading();
      final result = await _getBusinessFiled(event.status);
      yield* result.fold(
        (failure) async* {
          printFailure(failure);
          yield BusinessFiledError(failure: failure);
        },
        (list) async* {
          yield BusinessFiledLoaded(
            pralisting: list,
            isLoading: null,
            success: null,
            failure: null,
            selectedFilter: event.status,
          );
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield BusinessFiledError(
        failure: Failure(
          type: FailureType.localError,
          message: e,
        ),
      );
    }
  }

  Stream<BusinessFiledState> _mapDeleteBusinessFiled(
    DeleteBusinessFiledData event,
    BusinessFiledState state,
  ) async* {
    if (state is BusinessFiledLoaded) {
      // ignore: unnecessary_cast
      yield state.copyWith(isLoading: true);
      try {
        final result = await _deleteBusinessFiled(event.uuid);
        yield* result.fold(
          (failure) async* {
            printFailure(failure);
            yield state.copyWith(
              isLoading: false,
              failure: failure,
            );
          },
          (success) async* {
            yield state.copyWith(
              success: success.meta.message,
              isLoading: false,
            );
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield state.copyWith(
          isLoading: false,
          failure: Failure(
            type: FailureType.localError,
            message: e,
          ),
        );
      }
    }
  }
}
