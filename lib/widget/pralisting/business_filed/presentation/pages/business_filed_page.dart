import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../helpers/PopupHelper.dart';
import '../../../../../helpers/ToastHelper.dart';
import '../../../../../injector_container.dart';
import '../../../../../models/SearchDataModel.dart';
import '../../../../../utils/sizes.dart';
import '../../../../widget/components/main/SantaraDropDown.dart';
import '../../data/models/pralisting_list_model.dart';
import '../blocs/business_filed_bloc.dart';
import '../blocs/business_filed_event.dart';
import '../blocs/business_filed_state.dart';
import '../widgets/business_filed_card.dart';

class BusinessFiledPage extends StatefulWidget {
  @override
  _BusinessFiledPageState createState() => _BusinessFiledPageState();
}

class _BusinessFiledPageState extends State<BusinessFiledPage> {
  BusinessFiledBloc bloc;
  dynamic selectedFilter = "";

  @override
  void initState() {
    super.initState();
    bloc = sl<BusinessFiledBloc>();
    bloc..add(LoadBusinessFiledList(status: ""));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Bisnis Diajukan",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s18,
              ),
            ),
            centerTitle: true,
            elevation: 1.0,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            padding: EdgeInsets.all(Sizes.s20),
            color: Colors.white,
            child: BlocListener<BusinessFiledBloc, BusinessFiledState>(
                listener: (context, state) {
              if (state is BusinessFiledLoaded) {
                if (state.isLoading != null && state.isLoading) {
                  PopupHelper.showLoading(context);
                } else if (state.isLoading != null && !state.isLoading) {
                  Navigator.pop(context);
                }

                if (state.failure != null) {
                  ToastHelper.showFailureToast(context, state.failure.message);
                }

                if (state.success != null) {
                  ToastHelper.showSuccessToast(context, state.success);
                  bloc..add(LoadBusinessFiledList(status: ""));
                }
              }
            }, child: BlocBuilder<BusinessFiledBloc, BusinessFiledState>(
              builder: (context, state) {
                if (state is BusinessFiledLoading) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else if (state is BusinessFiledError) {
                  return Center(
                    child: Text("${state.failure.message}"),
                  );
                } else if (state is BusinessFiledLoaded) {
                  return SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          // width: Sizes.s150,
                          child: SantaraDropDown(
                            icon: Image.asset(
                              "assets/icon/category_filter.png",
                            ),
                            isExpanded: true,
                            hint: Text("Semua"),
                            items: bloc.filterItems.map((SearchData e) {
                              return DropdownMenuItem<String>(
                                child: Text("${e.name}"),
                                value: "${e.uuid}",
                              );
                            }).toList(),
                            value: state.selectedFilter ?? "",
                            onChanged: (selected) {
                              setState(() {
                                selectedFilter = selected;
                              });
                              bloc
                                ..add(
                                  LoadBusinessFiledList(
                                    status: selected,
                                  ),
                                );
                            },
                          ),
                        ),
                        state.pralisting.pralistingData != null &&
                                state.pralisting.pralistingData.length > 0
                            ? ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    state.pralisting.pralistingData.length,
                                itemBuilder: (context, index) {
                                  PralistingDatum data =
                                      state.pralisting.pralistingData[index];
                                  return BusinessFiledCard(
                                    bloc: bloc,
                                    data: data,
                                    callback: () {
                                      bloc
                                        ..add(
                                          LoadBusinessFiledList(
                                            status: selectedFilter,
                                          ),
                                        );
                                    },
                                  );
                                },
                              )
                            : Center(
                                child: Container(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        "assets/icon/search_not_found.png",
                                      ),
                                      Container(height: Sizes.s10),
                                      Text(
                                        "Mohon Maaf",
                                        style: TextStyle(
                                          fontSize: FontSize.s18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Container(height: Sizes.s10),
                                      Text(
                                        "Data pralisting tidak ditemukan",
                                        style: TextStyle(
                                          color: Color(0xff676767),
                                          fontSize: FontSize.s14,
                                        ),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ),
                                ),
                              )
                      ],
                    ),
                  );
                } else {
                  return Container();
                }
              },
            )),
          )),
    );
  }
}
