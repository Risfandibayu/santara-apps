import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_bloc.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_event.dart';
import 'package:santaraapp/widget/pralisting/journey/presentation/blocs/pralisting_journey_state.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/presentation/pages/business_dev_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/presentation/pages/business_info_1_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/presentation/pages/business_info_2_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/presentation/pages/business_prospective_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/presentation/pages/financial_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_projections/presentation/pages/financial_projections_page.dart';
import 'package:santaraapp/widget/pralisting/registration/management_info/presentation/pages/management_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/media/presentation/pages/media_page.dart';
import 'package:santaraapp/widget/pralisting/registration/share_info/presentation/pages/share_info_page.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/presentation/pages/terms_conditions_page.dart';

import '../../../../../injector_container.dart';

class BusinessFiledDetailPage extends StatefulWidget {
  final PralistingDatum data;
  BusinessFiledDetailPage({@required this.data});

  @override
  _BusinessFiledDetailPageState createState() =>
      _BusinessFiledDetailPageState();
}

class _BusinessFiledDetailPageState extends State<BusinessFiledDetailPage> {
  PralistingJourneyBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = sl<PralistingJourneyBloc>();
    bloc..add(LoadPralistingJourney(uuid: widget.data.uuid));
  }

  void navigateToForm(int step) async {
    String uuid = widget.data.uuid;
    var page;
    switch (step) {
      case 1:
        page = BusinessProspectiveInfoPage(uuid: uuid);
        break;
      case 2:
        page = BusinessInfoOnePage(uuid: uuid);
        break;
      case 3:
        page = BusinsessInfoTwoPage(uuid: uuid);
        break;
      case 4:
        page = ManagementInfoPage(uuid: uuid);
        break;
      case 5:
        page = FinancialInfoPage(uuid: uuid);
        break;
      case 6:
        page = BusinessDevPage(uuid: uuid);
        break;
      case 7:
        page = FinancialProjectionsPage(uuid: uuid);
        break;
      case 8:
        page = ShareInfoPage(uuid: uuid);
        break;
      case 9:
        page = MediaPage(uuid: uuid);
        break;
      case 10:
        page = TermsConditionsPage(uuid: uuid);
        break;
      default:
        page = null;
        break;
    }
    if (page != null) {
      final result = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => page));
      bloc..add(LoadPralistingJourney(uuid: widget.data.uuid));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(
              "Pengajuan Bisnis",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: FontSize.s12,
              ),
            ),
            Text(
              "${widget.data.trademark}",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: FontSize.s18,
              ),
            ),
          ],
        ),
        centerTitle: true,
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        color: Colors.white,
        width: double.maxFinite,
        height: double.maxFinite,
        child: BlocProvider<PralistingJourneyBloc>(
          create: (_) => bloc,
          child: BlocBuilder<PralistingJourneyBloc, PralistingJourneyState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is PralistingJourneyLoading) {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              } else if (state is PralistingJourneyError) {
                return Center(
                  child: Text("${state.failure.message}"),
                );
              } else if (state is PralistingJourneyLoaded) {
                return state.data.journey != null &&
                        state.data.journey.length > 0
                    ? ListView.separated(
                        physics: BouncingScrollPhysics(),
                        itemCount: state.data.journey.length,
                        itemBuilder: (context, index) {
                          var journey = state.data.journey[index];
                          bool canEdit = index == 0
                              ? true
                              : state.data.journey[index - 1].detail.length > 0
                                  ? true
                                  : false;
                          return ListTile(
                            onTap: canEdit
                                ? () => navigateToForm(journey.stepId)
                                : null,
                            title: Text(
                              "${journey.text}",
                              style: TextStyle(
                                fontSize: FontSize.s14,
                                fontWeight: FontWeight.w600,
                                color: canEdit ? Colors.black : Colors.grey,
                              ),
                            ),
                            subtitle: journey.statusRejection
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.info,
                                        size: FontSize.s18,
                                        color: Color(0xffFF4343),
                                      ),
                                      SizedBox(width: Sizes.s8),
                                      Text(
                                        "Ditolak",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xffFF4343),
                                        ),
                                      )
                                    ],
                                  )
                                : null,
                            trailing: journey.statusRejection
                                ? FlatButton(
                                    child: Text(
                                      "Perbaiki >",
                                      style: TextStyle(
                                        color: Color(0xff218196),
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.s14,
                                      ),
                                    ),
                                    onPressed: () {},
                                  )
                                : null,
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return Divider(height: 3);
                        },
                      )
                    : Container(
                        child: Text("Empty"),
                      );
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }
}
