import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/blocs/business_filed_bloc.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/blocs/business_filed_event.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_detail_page.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/pralisting/rejected/presentation/pages/pralisting_detail_rejected.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';

class BusinessFiledCard extends StatelessWidget {
  final PralistingDatum data;
  final BusinessFiledBloc bloc;
  final VoidCallback callback; // callback ketika user tap back button

  BusinessFiledCard({
    @required this.data,
    @required this.bloc,
    this.callback,
  });

  Color getColor(PralistingStatus status) {
    switch (status) {
      case PralistingStatus.data_updated:
        return Color(0xffFFF1DB);
        break;
      case PralistingStatus.revision:
        return Color(0xffFFE7E0);
        break;
      case PralistingStatus.rejected:
        return Color(0xffFFE7E0);
        break;
      default:
        return Color(0xffEEF1FF);
        break;
    }
  }

  bool showButtonStatus(PralistingStatus status) {
    switch (status) {
      case PralistingStatus.data_updated:
        return true;
        break;
      case PralistingStatus.revision:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  Widget buttonStatus(PralistingStatus status) {
    String text;
    switch (status) {
      case PralistingStatus.data_updated:
        text = "Lanjutkan";
        break;
      case PralistingStatus.revision:
        text = "Perbaiki Pengajuan";
        break;
      case PralistingStatus.rejected:
        text = "Lihat Alasan Penolakan";
        break;
      default:
        text = null;
        break;
    }
    return text != null
        ? Text(
            "$text >>",
            style: TextStyle(
              fontSize: FontSize.s11,
              fontWeight: FontWeight.w600,
              color: Colors.red[400],
              // decorationStyle: TextDecoration.underline,
              decoration: TextDecoration.underline,
              decorationStyle: TextDecorationStyle.solid,
            ),
          )
        : Container();
  }

  Widget cardStatus({@required PralistingDatum data}) {
    return "${data.statusText}".isEmpty
        ? Container()
        : Container(
            margin: EdgeInsets.fromLTRB(
              Sizes.s10,
              Sizes.s5,
              Sizes.s10,
              Sizes.s10,
            ),
            width: double.maxFinite,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: getColor(
                data.statusPralisiting,
              ),
            ),
            padding: EdgeInsets.all(Sizes.s10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${data.statusText}",
                  style: TextStyle(
                    fontSize: FontSize.s11,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                buttonStatus(data.statusPralisiting),
              ],
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: Sizes.s20),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Color(0xffDADADA)),
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      child: InkWell(
        onTap: () async {
          dynamic page;
          if (data.statusPralisiting == PralistingStatus.rejected) {
            page = PralistingDetailRejected(data: data);
          } else {
            if (data.isEdited) {
              page = BusinessFiledDetailPage(data: data);
            } else {
              page = PralistingDetailPage(
                position: null,
                status: data.statusPralisiting != null ? 3 : 2,
                uuid: data.uuid,
              );
            }
          }
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => page),
          );
          callback();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: Sizes.s30,
              decoration: BoxDecoration(color: Color(0xffF0F0F0)),
              padding: EdgeInsets.only(left: Sizes.s15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Dari : ${data.createdAt}",
                    style: TextStyle(
                      fontSize: FontSize.s10,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Container(
                    child: PopupMenuButton<String>(
                      padding: EdgeInsets.zero,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3.0),
                        side: BorderSide(
                          color: Color(0xffD6D6D6),
                          width: 1,
                        ),
                      ),
                      onSelected: (String result) {
                        if (result == "delete") {
                          PopupHelper.popConfirmation(
                            context,
                            () {
                              Navigator.pop(context);
                              bloc
                                ..add(
                                  DeleteBusinessFiledData(
                                    uuid: data.uuid,
                                  ),
                                );
                            },
                            "Apakah Anda yakin akan membatalkan pengajuan? ",
                            "Pengajuan yang telah dibatalkan akan dihapus dari list pengajuan bisnis Anda",
                            textCancel: "Kembali",
                            textConfirm: "Ya, Batalkan",
                          );
                        }
                      },
                      itemBuilder: (BuildContext context) =>
                          <PopupMenuEntry<String>>[
                        PopupMenuItem<String>(
                          height: Sizes.s10,
                          value: "delete",
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image.asset(
                                "assets/icon/delete.png",
                                width: Sizes.s20,
                                height: Sizes.s20,
                              ),
                              Container(
                                width: Sizes.s10,
                              ),
                              Text("Batalkan Pengajuan")
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(Sizes.s10),
              // height: Sizes.s140,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    child: SantaraCachedImage(
                      width: Sizes.s80,
                      height: Sizes.s80,
                      image: "${data.picture}",
                    ),
                  ),
                  SizedBox(width: Sizes.s20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: Sizes.s230,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${data.category}",
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: FontSize.s12,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff292F8D),
                              ),
                            ),
                            Text(
                              "${data.trademark}",
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: FontSize.s14,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              "${data.companyName}",
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: FontSize.s11,
                                fontWeight: FontWeight.w400,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: Sizes.s5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextIconView(
                              iconData: Icons.person_outline,
                              text: "${data.vote}",
                            ),
                            SizedBox(width: Sizes.s10),
                            TextIconView(
                              iconData: Icons.favorite_border,
                              text: "${data.like}",
                            ),
                            SizedBox(width: Sizes.s10),
                            TextIconView(
                              iconData: Icons.mode_comment_outlined,
                              text: "${data.comment}",
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            cardStatus(data: data)
          ],
        ),
      ),
    );
  }
}
