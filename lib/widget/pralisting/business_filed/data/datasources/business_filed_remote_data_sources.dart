import 'package:dio/dio.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/http/rest_client.dart';
import 'package:santaraapp/helpers/RestHelper.dart';

import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';

abstract class BusinessFiledRemoteDataSource {
  Future<PralistingMetaModel> deletePralisting({@required String uuid});
  Future<PralistingListModel> getPralistingList({@required String status});
}

class BusinessFiledRemoteDataSourceImpl
    implements BusinessFiledRemoteDataSource {
  final RestClient client;

  BusinessFiledRemoteDataSourceImpl({@required this.client});

  @override
  Future<PralistingMetaModel> deletePralisting({String uuid}) async {
    try {
      final result = await client.deleteExternalUrl(
          url: "$apiPralisting/pralisting/$uuid");
      if (result.statusCode == 200) {
        return PralistingMetaModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<PralistingListModel> getPralistingList({String status}) async {
    try {
      final result = await client.getExternalUrl(
        url: "$apiPralisting/list-pralisting?page=1&limit=200&status=$status",
      );

      if (result.statusCode == 200) {
        return PralistingListModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
