import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/data/models/pralisting_meta_model.dart';
import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/http/network_info.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/datasources/business_filed_remote_data_sources.dart';
import 'package:santaraapp/widget/pralisting/business_filed/data/models/pralisting_list_model.dart';
import 'package:santaraapp/widget/pralisting/business_filed/domain/repositories/business_filed_repository.dart';
import 'package:meta/meta.dart';

class BusinessFiledRepositoryImpl implements BusinessFiledRepository {
  final NetworkInfo networkInfo;
  final BusinessFiledRemoteDataSource remoteDataSource;

  BusinessFiledRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, PralistingMetaModel>> deletePralisting(
      {String uuid}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.deletePralisting(uuid: uuid);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, PralistingListModel>> getPralistingList(
      {String status}) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPralistingList(status: status);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
