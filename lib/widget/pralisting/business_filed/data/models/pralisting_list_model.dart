// To parse this JSON data, do
//
//     final pralistingListModel = pralistingListModelFromJson(jsonString);

import 'dart:convert';

import 'package:intl/intl.dart';

enum PralistingStatus {
  data_updated,
  passed_scoring,
  failed_scoring,
  verifying,
  verifying_by_ceo,
  rejected,
  revision,
  pralisting,
  verified,
}

PralistingListModel pralistingListModelFromJson(String str) =>
    PralistingListModel.fromJson(json.decode(str));

String pralistingListModelToJson(PralistingListModel data) =>
    json.encode(data.toJson());

class PralistingListModel {
  PralistingListModel({
    this.meta,
    this.pralistingData,
  });

  Meta meta;
  List<PralistingDatum> pralistingData;

  factory PralistingListModel.fromJson(Map<String, dynamic> json) =>
      PralistingListModel(
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
        pralistingData: json["data"] == null
            ? null
            : List<PralistingDatum>.from(
                json["data"].map((x) => PralistingDatum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta == null ? null : meta.toJson(),
        "data": pralistingData == null
            ? null
            : List<dynamic>.from(pralistingData.map((x) => x.toJson())),
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  int currentPage;
  int lastPage;
  int count;
  int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"] == null ? null : json["current_page"],
        lastPage: json["last_page"] == null ? null : json["last_page"],
        count: json["count"] == null ? null : json["count"],
        recordPerPage:
            json["record_per_page"] == null ? null : json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage == null ? null : currentPage,
        "last_page": lastPage == null ? null : lastPage,
        "count": count == null ? null : count,
        "record_per_page": recordPerPage == null ? null : recordPerPage,
      };
}

class PralistingDatum {
  static final formatter = DateFormat('dd LLL yyyy', 'id');

  PralistingDatum({
    this.uuid,
    this.picture,
    this.category,
    this.subCategory,
    this.trademark,
    this.companyName,
    this.createdAt,
    this.statusPralisiting,
    this.statusText,
    this.comment,
    this.like,
    this.vote,
    this.isEdited,
  });

  String uuid;
  String picture;
  String category;
  String subCategory;
  String trademark;
  String companyName;
  String createdAt;
  PralistingStatus statusPralisiting;
  String statusText;
  int comment;
  int like;
  int vote;
  bool isEdited;

  static PralistingStatus getStatus(String status) {
    status = 'PralistingStatus.$status';
    return PralistingStatus.values.firstWhere(
      (f) => f.toString() == status,
      orElse: () => null,
    );
  }

  factory PralistingDatum.fromJson(Map<String, dynamic> json) =>
      PralistingDatum(
        uuid: json["uuid"] == null ? null : json["uuid"],
        picture: json["picture"] == null ? null : json["picture"],
        category: json["category"] == null ? null : json["category"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        trademark: json["trademark"] == null ? null : json["trademark"],
        companyName: json["company_name"] == null ? null : json["company_name"],
        createdAt: json["created_at"] == null
            ? null
            : json["created_at"].isEmpty
                ? ""
                : formatter
                    .format(DateTime.parse(json["created_at"]))
                    .toString(),
        statusPralisiting: json["status_pralisiting"] == null
            ? null
            : getStatus(json["status_pralisiting"]),
        statusText: json["text_status_pralisiting"] == null
            ? null
            : json["text_status_pralisiting"],
        comment: json["comment"] == null ? null : json["comment"],
        like: json["like"] == null ? null : json["like"],
        vote: json["vote"] == null ? null : json["vote"],
        isEdited: json["is_edited"] == null
            ? false
            : json["is_edited"] == 1
                ? true
                : false,
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid == null ? null : uuid,
        "picture": picture == null ? null : picture,
        "category": category == null ? null : category,
        "sub_category": subCategory == null ? null : subCategory,
        "trademark": trademark == null ? null : trademark,
        "company_name": companyName == null ? null : companyName,
        "created_at": createdAt == null ? null : createdAt,
        "status_pralisiting":
            statusPralisiting == null ? null : statusPralisiting,
        "text_status_pralisiting": statusText == null ? null : statusText,
        "comment": comment == null ? null : comment,
        "like": like == null ? null : like,
        "vote": vote == null ? null : vote,
        "is_edited": isEdited ? 1 : 0,
      };
}
