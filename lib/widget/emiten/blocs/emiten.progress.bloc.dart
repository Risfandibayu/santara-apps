import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/EmitenDividen.dart';
import 'package:santaraapp/models/EmitenJourney.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class EmitenProgressEvent {}

class EmitenProgressState {}

// Event memuat progress emiten (dividen / journey)
class LoadEmitenProgress extends EmitenProgressEvent {
  int type; // 0 = dengan refresh / 1 = tanpa refresh
  String uuid;
  LoadEmitenProgress({this.type, this.uuid});
}

// State ketika data belom dimuat
class EmitenProgressUninitialized extends EmitenProgressState {}

// State ketika data error dimuat
class EmitenProgressError extends EmitenProgressState {
  final String uuid;
  final String error;
  EmitenProgressError({this.uuid, this.error});
}

// State ketika data berhasil dimuat
class EmitenProgressLoaded extends EmitenProgressState {
  List<EmitenJourney> emitenJourney;
  List<EmitenDividen> emitenDividen = [];
  var isOpenOffer = false;

  EmitenProgressLoaded(
      {this.emitenJourney, this.emitenDividen, this.isOpenOffer});
}

class EmitenProgressBloc
    extends Bloc<EmitenProgressEvent, EmitenProgressState> {
  EmitenProgressBloc(EmitenProgressState initialState)
      : super(EmitenProgressUninitialized());
  var isOpenOffer = false;
  List<EmitenJourney> emitenJourney = [];
  List<EmitenDividen> emitenDividen = [];

  Future<List<EmitenJourney>> getEmitenJourney(String uuid) async {
    List<EmitenJourney> emitenJourney;
    final http.Response response =
        await http.get('$apiLocal/journeys/emiten/$uuid');
    if (response.statusCode == 200) {
      emitenJourney = emitenJourneyFromJson(response.body);
    } else {
      emitenJourney = [];
    }
    return emitenJourney;
  }

  Future<List<EmitenDividen>> getEmitenDividen(String uuid) async {
    List<EmitenDividen> emitenDividen;
    final http.Response response =
        await http.get('$apiLocal/journeys/emiten-history/$uuid');
    if (response.statusCode == 200) {
      emitenDividen = emitenDividenFromJson(response.body);
    } else {
      emitenDividen = [];
    }
    return emitenDividen;
  }

  @override
  Stream<EmitenProgressState> mapEventToState(
      EmitenProgressEvent event) async* {
    if (event is LoadEmitenProgress) {
      if (event.type == 0) {
        yield EmitenProgressUninitialized();
      }

      try {
        var journey = await getEmitenJourney(event.uuid);
        var dividen = await getEmitenDividen(event.uuid);

        if (journey.length > 0) {
          for (var item in journey) {
            if (item.title == "Penawaran Saham") {
              isOpenOffer = true;
            }
          }
        }

        yield EmitenProgressLoaded(
          emitenDividen: dividen,
          emitenJourney: journey,
          isOpenOffer: isOpenOffer,
        );
        
      } catch (e) {
        yield EmitenProgressError(
          uuid: event.uuid,
          error: "[001] Emiten Journey Not Found!",
        );
      }
    }
  }
}
