import 'dart:convert';
import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:santaraapp/core/utils/platforms/user_agent.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/models/EmitenDividen.dart';
import 'package:santaraapp/models/EmitenJourney.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/wallet.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/emiten/blocs/emiten.progress.bloc.dart';

// Event Emiten
class EmitenEvent {}

// Event Load Emiten
class LoadEmiten extends EmitenEvent {
  int type; // 0 = dengan refresh / 1 = tanpa refresh
  String uuid;
  LoadEmiten({this.type = 0, this.uuid});
}

// Event ini dipanggil setelah dividen, journey dimuat dari bloc emiten.progress.bloc.dart
class EmitProgress extends EmitenEvent {
  EmitenProgressLoaded data;
  EmitProgress(this.data);
}

// Event untuk memuat data syarat dan ketentuan pembelian saham
class LoadTermsCondition extends EmitenEvent {}

// Emiten State
class EmitenState {}

// State ketika belum dimuat
class EmitenUninitialized extends EmitenState {}

// State ketika user session expired
class EmitenUnauthorized extends EmitenState {}

// State ketika terjadi kesalahan (error)
class EmitenError extends EmitenState {
  final String error;
  EmitenError({this.error});

  EmitenError copyWith({String error}) {
    return EmitenError(error: error ?? this.error);
  }
}

// State ketika emiten berhasil dimuat
class EmitenLoaded extends EmitenState {
  var token; // token user
  var isFinish = false; // jika
  Set<Marker> markers = {};
  int totalHarga;
  int isVerified;
  double sahamDimiliki = 0;
  double maksInvest = 0;
  double sisaInvest = 0;
  int traderType = 0; // 1 for personal; 2 for company
  Emiten emiten;
  DateTime now;
  bool tncLoaded;
  EmitenProgressLoaded progress;
  User userData;

  EmitenLoaded({
    this.token,
    this.isFinish,
    // this.isOpenOffer,
    this.markers,
    this.totalHarga,
    this.isVerified,
    this.sahamDimiliki,
    this.maksInvest,
    this.sisaInvest,
    this.traderType,
    this.emiten,
    // this.emitenJourney,
    // this.emitenDividen,
    this.now,
    this.tncLoaded = false,
    this.progress,
    this.userData,
  });

  EmitenLoaded copyWith({
    var token,
    var isFinish,
    var isOpenOffer,
    Set<Marker> markers,
    int totalHarga,
    int isVerified,
    double sahamDimiliki,
    double maksInvest,
    double sisaInvest,
    int traderType,
    Emiten emiten,
    List<EmitenJourney> emitenJourney,
    List<EmitenDividen> emitenDividen,
    DateTime now,
    bool tncLoaded,
    EmitenProgressLoaded progress,
    User userData,
  }) {
    return EmitenLoaded(
      token: token ?? this.token,
      isFinish: isFinish ?? this.isFinish,
      markers: markers ?? this.markers,
      totalHarga: totalHarga ?? this.totalHarga,
      isVerified: isVerified ?? this.isVerified,
      sahamDimiliki: sahamDimiliki ?? this.sahamDimiliki,
      maksInvest: maksInvest ?? this.maksInvest,
      sisaInvest: sisaInvest ?? this.sisaInvest,
      traderType: traderType ?? this.traderType,
      emiten: emiten ?? this.emiten,
      now: now ?? this.now,
      tncLoaded: tncLoaded ?? this.tncLoaded,
      progress: progress ?? this.progress,
      userData: userData ?? this.userData,
    );
  }
}

class EmitenBloc extends Bloc<EmitenEvent, EmitenState> {
  EmitenBloc(EmitenState initialState) : super(initialState);
  final storage = FlutterSecureStorage();
  var token;
  var isFinish = false;
  var isOpenOffer = false;
  final Set<Marker> markers = {};

  int totalHarga;
  bool loading = false;
  int isVerified;
  int retryGetMaxInvest = 0;
  double sahamDimiliki = 0;
  double maksInvest = 0;
  double sisaInvest = 0;
  int traderType = 0; // 1 for personal; 2 for company
  int tryLooping = 0;

  Future<double> getMaksimumInvest() async {
    double maxInvest = 0;
    try {
      final response = await http.get('$apiLocal/traders/max-invest',
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        maxInvest = data["max_invest"] / 1;
      } else {
        if (retryGetMaxInvest < 3) {
          retryGetMaxInvest = retryGetMaxInvest + 1;
          getMaksimumInvest();
        } else {
          maxInvest = 0;
        }
      }
    } on SocketException catch (_) {
      maxInvest = 0;
    }
    return maxInvest;
  }

  Future<DateTime> getCurrentTime() async {
    DateTime now;
    final http.Response response = await http.get(
      '$apiLocal/traders/ctime',
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      String dataTime = data["time"];
      now = DateTime.parse(dataTime);
    } else {
      now = DateTime.now();
    }
    return now;
  }

  Future<Emiten> getDetailToken(String uuid) async {
    final http.Response response = await http.get(
      '$apiLocal/emitens/$uuid',
      headers: await UserAgent.headers(),
    );
    if (response.statusCode == 200) {
      return Emiten.fromJson(json.decode(response.body));
    } else {
      return null;
    }
  }

  Future<EmitenState> checking() async {
    User data;
    final tokens = await storage.read(key: 'token');
    if (tokens != null) {
      final datas = userFromJson(await storage.read(key: 'user'));
      final uuid = datas.uuid;
      final http.Response response = await http.get(
          '$apiLocal/users/by-uuid/$uuid',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      if (response.statusCode == 200) {
        data = userFromJson(response.body);
        sahamDimiliki = await getSahamDimiliki(token);
        isVerified = data.trader.isVerified;
        if (data.trader.traderType == "company") {
          maksInvest = -0.1;
          sisaInvest = -0.1;
          traderType = 2;
        } else {
          maksInvest = await getMaksimumInvest();
          traderType = 1;
          sisaInvest = maksInvest - sahamDimiliki;
        }
      } else if (response.statusCode == 401) {
        return EmitenUnauthorized();
      } else {
        if (tryLooping < 3) {
          tryLooping++;
          checking();
        }
      }
    }

    return EmitenLoaded(
      token: tokens,
      tncLoaded: true,
      maksInvest: maksInvest,
      sisaInvest: sisaInvest,
      traderType: traderType,
      sahamDimiliki: sahamDimiliki,
      isVerified: isVerified,
      userData: data,
    );
  }

  @override
  Stream<EmitenState> mapEventToState(EmitenEvent event) async* {
    if (event is LoadEmiten) {
      try {
        // Token value
        token = await storage.read(key: 'token');
        // Jika event 0 ( Menampilkan shimmer )
        if (event.type == 0) {
          yield EmitenUninitialized();
        }
        // Get current timestamp
        var currentDate = await getCurrentTime();
        // Get detail emiten
        var detailToken = await getDetailToken(event.uuid);
        // Jika detail emiten tidak kosong
        if (detailToken != null) {
          Emiten emiten = detailToken;
          markers.add(
            Marker(
              markerId: MarkerId(emiten?.uuid ?? event.uuid),
              position: LatLng(emiten.latitude, emiten.longitude),
              icon: BitmapDescriptor.defaultMarker,
              infoWindow: InfoWindow(
                  title: emiten.trademark, snippet: emiten.companyName),
            ),
          );
          if (DateTime.parse(
                emiten.beginPeriod == null ? currentDate : emiten.beginPeriod,
              ).difference(currentDate).inSeconds <=
              0) {
            isFinish = true;
          } else {
            isFinish = false;
          }
        } else {
          logger.i(">> Error");
          yield EmitenError(error: "Emiten tidak ditemukan!");
          return;
        }
        // Emit State Loaded
        yield EmitenLoaded(
          token: token,
          isFinish: isFinish,
          markers: markers,
          totalHarga: totalHarga,
          isVerified: isVerified,
          sahamDimiliki: sahamDimiliki,
          maksInvest: maksInvest,
          sisaInvest: sisaInvest,
          traderType: traderType,
          emiten: detailToken,
          tncLoaded: false,
          now: currentDate,
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield EmitenError(error: e);
      }
    } else if (event is EmitProgress) {
      if (state is EmitenLoaded) {
        var data = state as EmitenLoaded;
        yield data.copyWith(progress: event.data);
      }
    } else if (event is LoadTermsCondition) {
      if (state is EmitenLoaded) {
        var data = state as EmitenLoaded;
        var result = await checking();
        if (result is EmitenLoaded) {
          yield data.copyWith(
            tncLoaded: result.tncLoaded,
            maksInvest: result.maksInvest,
            token: result.token,
            sisaInvest: result.sisaInvest,
            traderType: result.traderType,
            sahamDimiliki: result.sahamDimiliki,
            isVerified: result.isVerified,
            userData: result.userData,
          );
        } else {
          yield result;
        }
      }
    }
  }
}
