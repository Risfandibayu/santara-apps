import 'dart:async';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/emiten/blocs/emiten.bloc.dart';
import 'package:santaraapp/widget/emiten/blocs/emiten.progress.bloc.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/chart.bloc.dart';
import 'package:santaraapp/widget/home/DividenPenerbit.dart';
import 'package:santaraapp/widget/home/VideoProfile.dart';
import 'package:santaraapp/widget/security_token/pin/SecurityPinUI.dart';
import 'package:santaraapp/widget/widget/components/charts/build_chart.dart';
import 'package:santaraapp/widget/widget/components/listing/HeroPhotoViewer.dart';
import 'package:santaraapp/widget/widget/components/listing/SantaraStepper.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBottomSheet.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBusinessWidget.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraEmitenTabs.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraFinancialStatements.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraScaffoldNoState.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'BuyEmitenUI.dart';

class EmitenUI extends StatelessWidget {
  final String uuid;
  EmitenUI({@required this.uuid});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              FinanceChartBloc(ChartUninitialized())..add(LoadChart(uuid)),
        ),
        BlocProvider(
          create: (context) => EmitenBloc(EmitenUninitialized())
            ..add(LoadEmiten(type: 0, uuid: uuid)),
        ),
        BlocProvider(
          create: (context) => EmitenProgressBloc(EmitenProgressUninitialized())
            ..add(LoadEmitenProgress(type: 0, uuid: uuid)),
        ),
      ],
      child: EmitenContent(),
    );
  }
}

class EmitenContent extends StatefulWidget {
  @override
  _EmitenContentState createState() => _EmitenContentState();
}

class _EmitenContentState extends State<EmitenContent> {
  EmitenBloc bloc;
  FinanceChartBloc chartBloc;
  Future _mapFuture = Future.delayed(Duration(milliseconds: 800), () => true);
  Completer<GoogleMapController> _controller = Completer();
  PermissionStatus _status;

  final rupiah = NumberFormat("#,##0");
  List<String> stepList = [
    "Pra Penawaran Saham",
    "Penawaran Saham",
    "Pendanaan Terpenuhi",
    "Penyerahan Dana",
    "Pembagian Dividen",
    ""
  ];
  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<EmitenBloc>(context);
    chartBloc = BlocProvider.of<FinanceChartBloc>(context);
    _askPermission();
    PermissionHandler() // Check location permission has been granted
        .checkPermissionStatus(PermissionGroup
            .locationWhenInUse) //check permission returns a Future
        .then(_updateStatus);
  }

  void _updateStatus(PermissionStatus status) {
    if (status != _status) {
      // check status has changed
      setState(() {
        _status = status; // update
      });
    } else {
      if (status != PermissionStatus.granted) {
        PermissionHandler().requestPermissions(
            [PermissionGroup.locationWhenInUse]).then(_onStatusRequested);
      }
    }
  }

  void _askPermission() {
    PermissionHandler().requestPermissions(
        [PermissionGroup.locationWhenInUse]).then(_onStatusRequested);
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.locationWhenInUse];
    if (status != PermissionStatus.granted) {
      // On iOS if "deny" is pressed, open App Settings
      PermissionHandler().openAppSettings();
    } else {
      _updateStatus(status);
    }
  }

  @override
  void dispose() {
    bloc.close();
    chartBloc.close();
    super.dispose();
  }

  void _settingModalBottomSheet(context, EmitenProgressLoaded data) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
            heightFactor: .5,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Sizes.s15),
                topRight: Radius.circular(Sizes.s15),
              ),
              clipBehavior: Clip.hardEdge,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Sizes.s15),
                    topRight: Radius.circular(Sizes.s15),
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                  Sizes.s30,
                  Sizes.s30,
                  Sizes.s30,
                  Sizes.s20,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: Sizes.s10),
                      Text(
                        "Progres Penerbit",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: FontSize.s18,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      SantaraEmitenStepper(
                        stepNumber: "1",
                        title: "Pra Penawaran Saham",
                        subtitle: "10-12-2019",
                        isActive: true,
                        isNodeActive: true,
                        emitenJourney: data.emitenJourney,
                      ),
                      SantaraEmitenStepper(
                        stepNumber: "2",
                        title: "Penawaran Saham",
                        subtitle: "24-06-2020",
                        isActive: true,
                        isNodeActive: false,
                        emitenJourney: data.emitenJourney,
                      ),
                      SantaraEmitenStepper(
                        stepNumber: "3",
                        title: "Pendanaan Terpenuhi",
                        subtitle: "",
                        isActive: false,
                        isNodeActive: false,
                        emitenJourney: data.emitenJourney,
                      ),
                      SantaraEmitenStepper(
                        stepNumber: "4",
                        title: "Penyerahan Dana",
                        subtitle: "",
                        isActive: false,
                        isNodeActive: false,
                        emitenJourney: data.emitenJourney,
                      ),
                      InkWell(
                        onTap: () => data?.emitenDividen != null &&
                                data.emitenDividen.length > 0
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => DividenPenerbit(
                                    emitenDividen: data.emitenDividen,
                                  ),
                                ),
                              )
                            : Container(),
                        child: SantaraEmitenStepper(
                          stepNumber: "5",
                          title: "Pembagian Dividen",
                          subtitle: "",
                          isActive: false,
                          isNodeActive: false,
                          emitenJourney: data.emitenJourney,
                          isEnd: true,
                        ),
                      ),
                      SizedBox(height: Sizes.s20),
                      Container(
                        margin:
                            EdgeInsets.only(left: Sizes.s5, right: Sizes.s5),
                        child: SantaraOutlineButton(
                          color: Colors.white,
                          title: "Tutup",
                          onPressed: () => Navigator.pop(context),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  get divider {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      height: 1,
      color: Color(ColorRev.maingrey),
      width: double.maxFinite,
    );
  }

  // Tombol prospektus penerbit & profil perusahaan
  Widget _buildButtons({String prospektus, EmitenLoaded data}) {
    return Container(
      // color: Color(ColorRev.mainBlack),
      margin: EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s20),
      height: Sizes.s40,
      width: double.maxFinite,
      decoration: BoxDecoration(
          color: Color(ColorRev.mainBlack),
          borderRadius: BorderRadius.circular(Sizes.s15)),
      child: FlatButton.icon(
        onPressed: () async {
          if (data.token == null) {
            SantaraBottomSheet.show(
              context,
              "Maaf, silakan login terlebih dahulu.",
            );
          } else {
            if (prospektus != null) {
              try {
                // var url = Uri.encodeFull('$prospektus');
                var url = prospektus;
                if (await canLaunch(url)) {
                  await launch(url);
                }
                await FirebaseAnalytics().logEvent(
                  name: 'app_select_download_prospektus',
                  parameters: null,
                );
              } catch (e, stack) {
                santaraLog(e, stack);
                var params = {'prospektus': prospektus};
                await FirebaseAnalytics().logEvent(
                  name: 'app_select_download_prospektus_failed',
                  parameters: params,
                );
                ToastHelper.showBasicToast(
                  context,
                  "Tidak dapat membuka prospektus! $e",
                );
              }
            } else {
              ToastHelper.showBasicToast(
                context,
                "Tidak dapat membuka prospektus!",
              );
            }
          }
        },
        icon: Container(
          width: Sizes.s15,
          height: Sizes.s15,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              width: 1,
              color: Colors.white,
            ),
          ),
          child: Icon(
            Icons.arrow_downward,
            size: Sizes.s8,
            color: Colors.white,
          ),
        ),
        label: Text(
          "Prospektus Penerbit",
          style: TextStyle(
            color: Colors.white,
            fontSize: FontSize.s12,
          ),
        ),
        color: Color(
          0xffBF2D30,
        ),
      ),
    );
  }

  // Peraturan pojk
  Widget _buildPeraturanPojk(EmitenLoaded data) {
    return Column(
      children: [
        Text(
          "Yuk, Miliki Bisnis Ini",
          style: TextStyle(
            fontSize: FontSize.s16,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
        SizedBox(height: Sizes.s10),
        Container(
          width: double.maxFinite,
          padding: EdgeInsets.all(Sizes.s20),
          decoration: BoxDecoration(
            color: Color(0xffFEE0E0),
            borderRadius: BorderRadius.circular(Sizes.s5),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  "PERATURAN NOMOR 57/POJK.04/2020-Pasal 56-Ayat 3",
                  style: TextStyle(
                    color: Color(0xffBF2D30),
                    fontSize: FontSize.s12,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: Sizes.s10,
              ),
              RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Nunito',
                  ),
                  children: <TextSpan>[
                    TextSpan(text: 'Setiap pemodal dengan '),
                    TextSpan(
                        text:
                            'penghasilan lebih dari Rp.500.000.000 (lima ratus juta rupiah) per tahun, ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'maka batas '),
                    TextSpan(
                        text: 'maksimal investasi ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'pemodal tersebut di SANTARA adalah  '),
                    TextSpan(
                        text: '10% ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'dari jumlah pendapatan per tahun.'),
                  ],
                ),
              ),
              Container(height: 8),
              Text(
                "contoh : jika pendapatan pertahun anda adalah Rp. 100.000.000 maka batas maksimal investasi anda di SANTARA adalah Rp. 5.000.000",
                textAlign: TextAlign.justify,
              ),
              Container(height: 8),
              RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Nunito',
                  ),
                  children: <TextSpan>[
                    TextSpan(text: 'Kriteria '),
                    TextSpan(
                        text: 'batasan kepemilikan saham ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'di atas '),
                    TextSpan(
                        text: 'tidak berlaku ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'jika :'),
                  ],
                ),
              ),
              Text(
                "1.\tPemodal merupakan badan hukum\n2.\tPemodal memiliki rekening efek paling sedikit 2 (dua) tahun",
                textAlign: TextAlign.justify,
              ),
              Container(height: 8),
              RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Nunito',
                  ),
                  children: <TextSpan>[
                    TextSpan(text: 'Permohonan '),
                    TextSpan(
                        text: 'penghapusan limit kepemilikan saham ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: 'dapat diajukan melalui '),
                    TextSpan(
                        text: 'email customer support ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '(customer.support@santara.co.id), dengan '),
                    TextSpan(
                        text: 'melampirkan bukti ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(
                        text:
                            'kepemilikan rekening efek atau bukti SK Kemenkumham badan usaha.'),
                  ],
                ),
              )
            ],
          ),
        ),
        SizedBox(height: Sizes.s10),
        data.tncLoaded
            ? InkWell(
                key: Key('viewTermsConditionBtn'),
                onTap: () => PopupHelper.showModalSyaratDanKetentuan(
                  context,
                  data.emiten.minimumInvest,
                  data.emiten.price,
                  data,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Syarat dan Ketentuan pembelian saham ",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    Icon(
                      Icons.info,
                      color: Colors.white,
                      size: FontSize.s18,
                    ),
                  ],
                ),
              )
            : _buildShimmer(Sizes.s20, double.maxFinite)
      ],
    );
  }

  // Build informasi saham
  Widget _buildInfoSaham(EmitenLoaded data) {
    Emiten emiten = data.emiten;
    return Container(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextValueEmiten(
            title: "Saham Tersisa",
            value:
                "${((emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)) / emiten.supply * 100).toStringAsFixed(2)}%",
            isBold: true,
          ),
          TextValueEmiten(
            title: "Dalam Lembar",
            value:
                "${rupiah.format(emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual))} Lembar",
          ),
          TextValueEmiten(
            title: "Dalam Rupiah",
            value:
                "Rp. ${(rupiah.format((emiten.supply - (emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)) * emiten.price))}",
          ),
          SizedBox(height: Sizes.s20),
          TextValueEmiten(
            title: "Saham Terjual",
            value:
                "${((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) / emiten.supply * 100).toStringAsFixed(2)}%",
            isBold: true,
          ),
          TextValueEmiten(
            title: "Dalam Lembar",
            value:
                "${rupiah.format(emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual)} Lembar",
          ),
          TextValueEmiten(
            title: "Dalam Rupiah",
            value:
                "Rp. ${(rupiah.format((emiten.terjual > emiten.supply ? emiten.supply : emiten.terjual) * emiten.price))}",
          ),
          // buttons
          _buildButtons(prospektus: emiten.prospektus, data: data),
          data.progress != null
              ? Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Color(0xffdddddd),
                    ),
                    borderRadius: BorderRadius.circular(Sizes.s5),
                  ),
                  padding: EdgeInsets.all(Sizes.s10),
                  child: !data.isFinish || !data.progress.isOpenOffer
                      ? Column(
                          children: <Widget>[
                            Image.asset(
                              "assets/icon/pra_penawaran.png",
                              width: Sizes.s150,
                            ),
                            Container(height: Sizes.s15),
                            Text(
                              "Pra Penawaran Saham",
                              style: TextStyle(
                                  fontSize: FontSize.s17,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        )
                      : data.progress.emitenDividen.length > 0
                          ? Column(
                              children: <Widget>[
                                Image.asset(
                                  "assets/icon/pembagian_dividen.png",
                                  width: Sizes.s150,
                                ),
                                Container(height: Sizes.s15),
                                Text(
                                  "Pembagian Dividen",
                                  style: TextStyle(
                                      fontSize: FontSize.s17,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )
                              ],
                            )
                          : _buildPeraturanPojk(data),
                )
              : Container()
        ],
      ),
    );
  }

  // Build detail saham
  Widget _buildDetailSaham(EmitenLoaded data) {
    Emiten emiten = data.emiten;
    return Container(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Kode Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
                color: Color(ColorRev.mainwhite),
              ),
            ),
            subtitle: Text(
              "${emiten.codeEmiten}",
              style: TextStyle(
                fontSize: FontSize.s15,
                fontWeight: FontWeight.w700,
                color: Color(ColorRev.maingreen),
              ),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Harga Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
                color: Color(ColorRev.mainwhite),
              ),
            ),
            subtitle: Text(
              "Rp. ${rupiah.format(emiten.price)}",
              style: TextStyle(
                fontSize: FontSize.s15,
                fontWeight: FontWeight.w700,
                color: Color(ColorRev.maingreen),
              ),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Total Saham",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
                color: Color(ColorRev.mainwhite),
              ),
            ),
            subtitle: Text(
              "${rupiah.format(emiten.supply)} Lembar",
              style: TextStyle(
                  fontSize: FontSize.s15,
                  fontWeight: FontWeight.w700,
                  color: Color(ColorRev.maingreen)),
            ),
          ),
          ListTile(
            isThreeLine: true,
            contentPadding: EdgeInsets.zero,
            title: Text(
              "Total Saham (Rp.)",
              style: TextStyle(
                fontSize: FontSize.s12,
                fontWeight: FontWeight.w400,
                color: Color(ColorRev.mainwhite),
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Rp ${rupiah.format(emiten.supply * emiten.price)}",
                  style: TextStyle(
                    fontSize: FontSize.s15,
                    fontWeight: FontWeight.w700,
                    color: Color(ColorRev.maingreen),
                  ),
                ),
                Text(
                  "(${terbilang((emiten.supply * emiten.price).toDouble())})",
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: FontSize.s12,
                    fontStyle: FontStyle.italic,
                    color: Color(ColorRev.mainwhite),
                  ),
                ),
              ],
            ),
          ),
          _buildButtons(prospektus: emiten.prospektus, data: data),
        ],
      ),
    );
  }

  // Build tentang penerbit
  Widget _buildTentangPenerbit(EmitenLoaded data) {
    Emiten emiten = data.emiten;
    String imageUrl = "$apiLocalImage${emiten.photo}";
    return Container(
      padding: EdgeInsets.all(Sizes.s15),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: SantaraCachedImage(
              height: 100,
              width: 100,
              image: imageUrl.replaceAll("trader", "trader-photo"),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "Profil ${emiten.name}",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: FontSize.s15,
              color: Color(ColorRev.mainwhite),
            ),
          ),
          SizedBox(height: Sizes.s10),
          Text(
            "${emiten.bio}",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: FontSize.s12,
              color: Color(ColorRev.mainwhite),
            ),
          ),
          _buildButtons(prospektus: emiten.prospektus, data: data),
        ],
      ),
    );
  }

  // build finansial
  Widget _buildFinansial(EmitenLoaded data) {
    Emiten emiten = data.emiten;
    return BlocBuilder<FinanceChartBloc, ChartState>(
      builder: (context, state) {
        if (state is ChartUninitialized) {
          return Center(
            child: Container(height: 100, child: CupertinoActivityIndicator()),
          );
        } else if (state is ChartError) {
          return Center(
            child: Text("${state.error}",
                style: TextStyle(color: Color(ColorRev.mainwhite))),
          );
        } else if (state is ChartEmpty) {
          return Center(
            child: Container(
              padding: EdgeInsets.all(Sizes.s10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    height: Sizes.s200,
                  ),
                  Text(
                    "Belum Ada Laporan Keuangan",
                    style: TextStyle(
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  SizedBox(height: Sizes.s20),
                  Text(
                    "Penerbit ini belum memiliki laporan keuangan",
                    style: TextStyle(
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.w300,
                      color: Colors.white,
                    ),
                  ),
                  _buildButtons(prospektus: emiten.prospektus, data: data),
                ],
              ),
            ),
          );
        } else if (state is ChartLoaded) {
          return BuildFinancialChart(data: state.data);
        } else {
          return Container();
        }
      },
    );
  }

  // Lokasi penerbit
  Widget _buildBusinessLocation(EmitenLoaded data) {
    try {
      Emiten emiten = data.emiten;
      final Set<Marker> _markers = {};
      _markers.add(
        Marker(
          markerId: MarkerId("${emiten.uuid}"),
          position: LatLng(emiten.latitude, emiten.longitude),
          icon: BitmapDescriptor.defaultMarker,
          infoWindow: InfoWindow(
            title: "${emiten.name}",
            snippet: "${emiten.companyName}",
          ),
        ),
      );
      return FutureBuilder(
        future: _mapFuture,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
              width: double.maxFinite,
              height: Sizes.s250,
            );
          }
          return Padding(
            padding: EdgeInsets.all(Sizes.s15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: double.maxFinite,
                  height: Sizes.s200,
                  child: GoogleMap(
                    mapType: MapType.normal,
                    markers: _markers,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(emiten.latitude, emiten.longitude),
                      zoom: 15,
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  ),
                ),
                SizedBox(height: Sizes.s10),
                Text(
                  "${emiten.address}",
                  style: TextStyle(
                    color: Color(ColorRev.mainwhite),
                  ),
                ),
                _buildButtons(prospektus: emiten.prospektus, data: data),
              ],
            ),
          );
        },
      );
    } catch (err) {
      print('errorMaps >>> $err');
    }
  }

  // Sisa waktu penjualan
  Widget _buildCaption(DateTime now, Emiten emiten) {
    return Wrap(
      children: <Widget>[
        Text(
          "Sisa waktu : ",
          style: TextStyle(
              fontSize: FontSize.s10,
              fontWeight: FontWeight.w600,
              color: Color(ColorRev.mainwhite)),
        ),
        now == null
            ? Text(
                '0 Hari',
                style: TextStyle(
                    fontSize: FontSize.s10,
                    fontWeight: FontWeight.w600,
                    color: Color(ColorRev.mainwhite)),
              )
            : DateTime.parse(emiten.beginPeriod).difference(now).inSeconds > 0
                ? Text(
                    '(Segera Dimulai)',
                    style: TextStyle(
                        fontSize: FontSize.s10,
                        fontWeight: FontWeight.w600,
                        color: Color(ColorRev.mainwhite)),
                  )
                : Countdown(
                    duration: Duration(
                        seconds: DateTime.parse(emiten.endPeriod)
                            .difference(now)
                            .inSeconds),
                    builder: (BuildContext context, Duration remaining) {
                      if (remaining.inSeconds <= 0 ||
                          emiten.terjual >= emiten.supply) {
                        return Text(
                          '0 Hari',
                          style: TextStyle(
                              fontSize: FontSize.s10,
                              fontWeight: FontWeight.w600,
                              color: Color(ColorRev.mainwhite)),
                        );
                      } else if (remaining.inDays > 0) {
                        return Text(
                          "${remaining.inDays} Hari",
                          style: TextStyle(
                              fontSize: FontSize.s10,
                              fontWeight: FontWeight.w600,
                              color: Color(ColorRev.mainwhite)),
                        );
                      } else {
                        String hour = (remaining.inHours).toString();
                        String second = (remaining.inSeconds % 60).toString();
                        String minute = (remaining.inMinutes % 60).toString();
                        if (second.length == 1) {
                          second = "0" + second;
                        }
                        if (minute.length == 1) {
                          minute = "0" + minute;
                        }
                        if (hour.length == 1) {
                          hour = "0" + hour;
                        }
                        return Text(
                          '$hour:$minute:$second',
                          style: TextStyle(
                              fontSize: FontSize.s10,
                              fontWeight: FontWeight.w600,
                              color: Color(ColorRev.mainwhite)),
                        );
                      }
                    },
                  ),
        emiten?.totalInvestor == null
            ? Container()
            : Text(
                " - ${emiten.totalInvestor} Investor",
                style: TextStyle(
                    fontSize: FontSize.s10,
                    fontWeight: FontWeight.w600,
                    color: Color(ColorRev.mainwhite)),
              ),
      ],
    );
  }

  // Convert step title to number
  String stepNumber(String step) {
    switch (step) {
      case "Pra Penawaran Saham":
        return "1";
        break;
      case "Penawaran Saham":
        return "2";
        break;
      case "Pendanaan Terpenuhi":
        return "3";
        break;
      case "Penyerahan Dana":
        return "4";
        break;
      case "Pembagian Dividen":
        return "5";
        break;
      default:
        return "5";
        break;
    }
  }

  // Build button beli
  Widget _buildButtonBeli(EmitenLoaded data) {
    return data.progress != null
        ? data.progress.emitenDividen.length > 0 ||
                !data.isFinish ||
                !data.progress.isOpenOffer
            ? null
            : data.token == null
                ? InkWell(
                    onTap: () => Navigator.pushNamed(context, '/login'),
                    child: Container(
                      child: SantaraBuyStockButton(
                        title: "Maaf, anda harus login",
                        onTap: () {
                          print('maaf anda harus login');
                          Navigator.pushNamed(context, '/login');
                        },
                      ),
                    ),
                  )
                : DateTime.parse(data.emiten.endPeriod.toString())
                            .difference(data.now)
                            .inSeconds <=
                        0
                    ? SantaraBuyStockButton(
                        title: "Maaf, waktu pembelian telah berakhir",
                        onTap: null,
                      )
                    : data.emiten.terjual < data.emiten.supply
                        ? data.tncLoaded
                            ? SantaraBuyStockButton(
                                key: Key('beliSahamBtn'),
                                title: "Beli Saham Sekarang",
                                onTap: () async => await buyEmiten(data),
                              )
                            : FlatButton(
                                onPressed: () {},
                                child: CupertinoActivityIndicator(),
                              )
                        : SantaraBuyStockButton(
                            title: "Maaf, saham sudah soldout",
                            onTap: null,
                          )
        : null;
  }

  buyEmiten(EmitenLoaded data) async {
    // await Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => SecurityPinUI(
    //       title: "Masukan PIN Anda",
    //       type: SecurityType.check,
    //       onSuccess: (pin, finger) async {
    // add_transaction_info
    FirebaseAnalytics().logEvent(
      name: 'app_add_transaction_info',
      parameters: null,
    );
    // Navigator.pop(context);
    // await Future.delayed(Duration(milliseconds: 100));
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BuyEmitenUI(
          data: data,
          // pin: pin,
          // finger: finger,
        ),
      ),
    );
    //       },
    //     ),
    //   ),
    // );
  }

  // Build loading shimmer
  Widget _buildShimmer(double height, double width) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(Sizes.s10),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.white,
        child: Container(
          height: height,
          width: width,
          color: Colors.pinkAccent,
          child: Image.asset(
            'arfa.png',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EmitenBloc, EmitenState>(
      bloc: bloc,
      listener: (context, state) {
        if (state is EmitenUnauthorized) {
          NavigatorHelper.pushToExpiredSession(context);
        }
      },
      child: BlocBuilder<EmitenBloc, EmitenState>(builder: (context, state) {
        if (state is EmitenUninitialized) {
          return BuildScaffoldNoState(
            content: Padding(
              padding: EdgeInsets.all(Sizes.s15),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Progress section
                    _buildShimmer(Sizes.s80, double.maxFinite),
                    // Slider section
                    Container(
                      margin:
                          EdgeInsets.only(top: Sizes.s20, bottom: Sizes.s20),
                      height: Sizes.s200,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        children: [
                          _buildShimmer(Sizes.s80, Sizes.s300),
                          SizedBox(width: Sizes.s15),
                          _buildShimmer(Sizes.s80, Sizes.s300),
                          SizedBox(width: Sizes.s15),
                          _buildShimmer(Sizes.s80, Sizes.s300),
                        ],
                      ),
                    ), // Title section
                    // Title Section
                    _buildShimmer(Sizes.s15, Sizes.s250),
                    SizedBox(height: Sizes.s10),
                    _buildShimmer(Sizes.s15, Sizes.s180),
                    SizedBox(height: Sizes.s10),
                    _buildShimmer(Sizes.s15, Sizes.s180),
                    // Progress penjualan saham
                    SizedBox(height: Sizes.s20),
                    _buildShimmer(Sizes.s100, double.maxFinite),
                    SizedBox(height: Sizes.s20),
                    // Detail emiten
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: _buildShimmer(Sizes.s30, double.maxFinite),
                        ),
                        SizedBox(
                          width: Sizes.s10,
                        ),
                        Expanded(
                          flex: 1,
                          child: _buildShimmer(Sizes.s30, double.maxFinite),
                        ),
                        SizedBox(
                          width: Sizes.s10,
                        ),
                        Expanded(
                          flex: 1,
                          child: _buildShimmer(Sizes.s30, double.maxFinite),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    _buildShimmer(Sizes.s150, double.maxFinite),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    _buildShimmer(Sizes.s40, double.maxFinite),
                    SizedBox(
                      height: Sizes.s20,
                    ),
                    _buildShimmer(Sizes.s150, double.maxFinite),
                  ],
                ),
              ),
            ),
          );
        } else if (state is EmitenLoaded) {
          EmitenLoaded data = state;
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: Navigator.of(context).pop,
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
              shape: Border(
                  bottom:
                      BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
              title: Text(
                "${data.emiten.trademark}",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              iconTheme: IconThemeData(color: Colors.black),
              centerTitle: true,
              backgroundColor: Color(ColorRev.mainBlack),
            ),
            backgroundColor: Color(ColorRev.mainBlack),
            body: Container(
              width: double.maxFinite,
              height: double.maxFinite,
              child: SingleChildScrollView(
                key: Key('emitenListViewKey'),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: SantaraAppBetaTesting(),
                    ),
                    // Timer
                    data.isFinish
                        ? Container()
                        : Container(
                            margin: EdgeInsets.fromLTRB(
                                Sizes.s20, Sizes.s10, Sizes.s20, 0),
                            height: Sizes.s40,
                            width: double.maxFinite,
                            color: Color(0xffBF2D30),
                            child: Center(
                              child: Countdown(
                                duration: Duration(
                                    seconds:
                                        DateTime.parse(data.emiten.beginPeriod)
                                            .difference(data.now)
                                            .inSeconds),
                                onFinish: () {
                                  // refresh
                                  bloc
                                    ..add(LoadEmiten(
                                        type: 1, uuid: data.emiten.uuid));
                                  BlocProvider.of<EmitenProgressBloc>(context)
                                    ..add(LoadEmitenProgress(
                                        type: 1, uuid: data.emiten.uuid));
                                  chartBloc..add(LoadChart(data.emiten.uuid));
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (_) {
                                      return AlertDialog(
                                        content: Text(
                                            "Waktu penawaran saham telah dimulai"),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text("Oke"),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      );
                                    },
                                  );
                                },
                                builder:
                                    (BuildContext context, Duration remaining) {
                                  if (data.isFinish) {
                                    return Container();
                                  } else {
                                    if (remaining.inSeconds <= 0 ||
                                        data.emiten.terjual >=
                                            data.emiten.supply) {
                                      return Text(
                                        "0 Hari",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      );
                                    } else if (remaining.inDays > 0) {
                                      return Text(
                                        "Bisnis Dibuka Dalam ${remaining.inDays} Hari",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      );
                                    } else {
                                      String hour =
                                          (remaining.inHours).toString();
                                      String second =
                                          (remaining.inSeconds % 60).toString();
                                      String minute =
                                          (remaining.inMinutes % 60).toString();
                                      if (second.length == 1) {
                                        second = "0" + second;
                                      }
                                      if (minute.length == 1) {
                                        minute = "0" + minute;
                                      }
                                      if (hour.length == 1) {
                                        hour = "0" + hour;
                                      }
                                      return Text(
                                        "Bisnis Dibuka Dalam $hour:$minute:$second",
                                        style: TextStyle(
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      );
                                    }
                                  }
                                },
                              ),
                            ),
                          ),
                    // Emiten Info
                    BlocListener<EmitenProgressBloc, EmitenProgressState>(
                      listener: (context, state) {
                        if (state is EmitenProgressLoaded) {
                          EmitenProgressLoaded progress = state;
                          bloc..add(EmitProgress(progress));
                          bloc..add(LoadTermsCondition());
                        }
                      },
                      child:
                          BlocBuilder<EmitenProgressBloc, EmitenProgressState>(
                        builder: (context, state) {
                          if (state is EmitenProgressUninitialized) {
                            return Padding(
                              padding: EdgeInsets.all(Sizes.s15),
                              child: _buildShimmer(Sizes.s80, double.maxFinite),
                            );
                          } else if (state is EmitenProgressError) {
                            return Center(
                              child: Text(state.error),
                            );
                          } else if (state is EmitenProgressLoaded) {
                            return ListTile(
                              contentPadding: EdgeInsets.only(
                                  top: Sizes.s10,
                                  left: Sizes.s20,
                                  right: Sizes.s20,
                                  bottom: 0),
                              isThreeLine: true,
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        "Progress ",
                                        style: TextStyle(
                                          color: Color(0xffBF2D30),
                                          fontSize: FontSize.s12,
                                        ),
                                      ),
                                      Text(
                                        "${stepNumber(state.emitenJourney.length == 0 ? 'Pra Penawaran Saham' : state.emitenJourney.last.title)} dari 5",
                                        style: TextStyle(
                                          color: Color(0xffBF2D30),
                                          fontSize: FontSize.s12,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )
                                    ],
                                  ),
                                  state?.emitenJourney?.length == null
                                      ? Container()
                                      : Text(
                                          state.emitenJourney.length > 0
                                              ? "${state.emitenJourney.last.title}"
                                              : "Pra Penawaran Saham",
                                          style: TextStyle(
                                              fontSize: FontSize.s18,
                                              fontWeight: FontWeight.w700,
                                              color: Color(ColorRev.mainwhite)),
                                        ),
                                ],
                              ),
                              subtitle: Text(
                                state.emitenJourney.length == 0
                                    ? "Selanjutnya : Penawaran Saham"
                                    : stepNumber(state
                                                .emitenJourney.last.title) ==
                                            "5"
                                        ? "Selanjutnya : Pembagian Dividen"
                                        : "Selanjutnya : ${stepList[int.parse(stepNumber(state.emitenJourney.last.title))]}",
                                style: TextStyle(
                                    fontSize: FontSize.s12,
                                    fontWeight: FontWeight.w300,
                                    color: Color(ColorRev.mainwhite)),
                              ),
                              trailing: InkWell(
                                onTap: () {
                                  FirebaseAnalytics().logEvent(
                                    name: 'app_select_progres_penerbit',
                                    parameters: null,
                                  );
                                  _settingModalBottomSheet(context, state);
                                },
                                child: Container(
                                    key: Key('viewProgressEmitenBtn'),
                                    width: Sizes.s40,
                                    child: Icon(Icons.error_outline,
                                        color: Color(ColorRev.mainwhite))),
                              ),
                            );
                          } else {
                            return Text("Unknown State!");
                          }
                        },
                      ),
                    ),
                    // Emiten Image & Title
                    Container(
                      padding: EdgeInsets.only(left: Sizes.s20, top: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: Sizes.s200,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                data?.emiten?.videoUrl != null
                                    ? data.emiten.videoUrl.isNotEmpty
                                        ? SantaraFinanceImage(
                                            image: YoutubeThumbnailGenerator
                                                .getThumbnail(
                                              "${data.emiten.videoUrl}",
                                              ThumbnailQuality.high,
                                            ),
                                            isVideo: true,
                                            onTap: () {
                                              FirebaseAnalytics().logEvent(
                                                name: 'app_play_video',
                                                parameters: null,
                                              );
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (_) => VideoProfile(
                                                    videoUrl:
                                                        "${data.emiten.videoUrl}",
                                                  ),
                                                ),
                                              );
                                            })
                                        : Container()
                                    : Container(),
                                // ignore: sdk_version_ui_as_code
                                for (var i in data?.emiten?.pictures)
                                  SantaraFinanceImage(
                                    image: i.picture,
                                    onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            HeroPhotoViewWrapper(
                                          tag: "${i.picture}",
                                          imageProvider:
                                              NetworkImage(i.picture),
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          SizedBox(height: Sizes.s15),
                          Container(
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  border: Border.all(
                                    color: Colors.red,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              child: businessType("${data.emiten.category}")),
                          SizedBox(height: Sizes.s15),
                          Row(
                            children: [
                              businessName("${data.emiten.trademark}"),
                              SizedBox(width: 5),
                              Image.asset('assets/santara/check-verified.png',
                                  scale: 4.4)
                            ],
                          ),
                          businessCompanyName("${data.emiten.companyName}"),
                          SizedBox(height: Sizes.s25),
                        ],
                      ),
                    ),
                    // Progress Penjualan

                    divider,
                    Container(
                      padding: EdgeInsets.all(Sizes.s20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        "Mulai Dari ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: FontSize.s14,
                                            color: Color(ColorRev.mainwhite)),
                                      ),
                                      Text(
                                        "Rp ${rupiah.format(data.emiten.startFrom)}",
                                        style: TextStyle(
                                          color: Color(ColorRev.maingreen),
                                          fontWeight: FontWeight.w700,
                                          fontSize: FontSize.s16,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "Dari Target ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: FontSize.s12,
                                            color: Color(ColorRev.mainwhite)),
                                      ),
                                      Text(
                                        "Rp. ${rupiah.format(data.emiten.price * data.emiten.supply)}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: FontSize.s12,
                                            color: Color(ColorRev.mainwhite)),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Periode Dividen",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: FontSize.s14,
                                        color: Color(ColorRev.mainwhite)),
                                  ),
                                  Text(
                                    "${data.emiten.period}",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: FontSize.s16,
                                        color: Color(ColorRev.mainwhite)),
                                  )
                                ],
                              ),
                            ],
                          ),
                          SantaraBusinessProgressIndicator(
                            margin: EdgeInsets.fromLTRB(
                              Sizes.s8,
                              Sizes.s15,
                              Sizes.s8,
                              Sizes.s5,
                            ),
                            lineHeight: Sizes.s20,
                            percent: data.emiten.terjual / data.emiten.supply,

                            //  num.parse(
                            //     ((data.emiten.terjual > data.emiten.supply
                            //                 ? data.emiten.supply
                            //                 : data.emiten.terjual) /
                            //             data.emiten.supply)
                            //         .toStringAsFixed(1)),
                            // caption: "Sisa Waktu : 50 Hari - 100 Investor",
                            caption: _buildCaption(data.now, data.emiten),
                            text: Text(
                              "${((data.emiten.terjual > data.emiten.supply ? data.emiten.supply : data.emiten.terjual) / data.emiten.supply * 100) > 100.00 ? 100.00 : ((data.emiten.terjual > data.emiten.supply ? data.emiten.supply : data.emiten.terjual) / data.emiten.supply * 100).toStringAsFixed(2)} %",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s12,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    divider,
                    // Detail section
                    SantaraEmitenTabs(
                      stockInfo: _buildInfoSaham(data),
                      stockDetail: _buildDetailSaham(data),
                      about: _buildTentangPenerbit(data),
                      financial: _buildFinansial(data),
                      location: _buildBusinessLocation(data),
                    ),
                    // SizedBox(height: Sizes.s30),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: _buildButtonBeli(data),
          );
        } else if (state is EmitenError) {
          return BuildScaffoldNoState(
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: Sizes.s250,
                child: Image.asset("assets/icon/empty_dividen.png"),
              ),
              Text("${state.error}", style: TextStyle(color: Colors.white)),
              // Container(
              //   height: Sizes.s500,
              //   child: SingleChildScrollView(child: Text("${state.error}")),
              // ),
            ],
          ));
        } else {
          return BuildScaffoldNoState(
            content: Container(),
          );
        }
      }),
    );
  }
}
