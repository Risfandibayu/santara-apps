import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../features/deposit/deposit_history/presentation/bloc/deposit_history_bloc.dart';
import '../../features/payment/presentation/pages/payment_page.dart';
import '../../features/transaction/presentation/bloc/checkout_list/checkout_list_bloc.dart';
import '../../helpers/PopupHelper.dart';
import '../../utils/logger.dart';
import '../../utils/sizes.dart';
import '../widget/components/main/SantaraAppBetaTesting.dart';
import 'blocs/emiten.bloc.dart';

class BuyEmitenUI extends StatefulWidget {
  final EmitenLoaded data;

  // final String pin;
  // final bool finger;
  BuyEmitenUI({@required this.data});

  @override
  _BuyEmitenUIState createState() => _BuyEmitenUIState();
}

class _BuyEmitenUIState extends State<BuyEmitenUI> {
  get divider {
    return Container(
      height: 0.5,
      color: Color(0xfffafafa),
      width: double.maxFinite,
    );
  }

  CheckoutListBloc _checkoutListBloc;
  DepositHistoryBloc _depositHistoryBloc;
  final rupiah = NumberFormat("#,##0");
  final formKey = GlobalKey<FormState>();
  TextEditingController lembarController = MoneyMaskedTextController(
      decimalSeparator: '', thousandSeparator: ',', precision: 0);
  TextEditingController tokenController = TextEditingController();
  bool isUnlimited = false;
  bool isErrorSaham = false;

  void onChangeSaham() {
    if (lembarController.text.isEmpty) {
      tokenController.text = 'Rp ${rupiah.format(0)}';
      setState(() => widget.data.totalHarga = 0);
    } else if (!RegExp(r"^[0-9\,]*$").hasMatch(lembarController.text) ||
        lembarController.text == ',' ||
        lembarController.text.contains(',,')) {
      tokenController.text = 'Rp ${rupiah.format(0)}';
      setState(() => widget.data.totalHarga = 0);
    } else {
      tokenController.text =
          'Rp ${rupiah.format(num.parse(lembarController.text.replaceAll(",", "")) * widget.data.emiten.price).toString()}';
      setState(() => widget.data.totalHarga =
          num.parse(lembarController.text.replaceAll(",", "")) *
              widget.data.emiten.price);
    }
  }

  Widget _form() {
    return Form(
      key: formKey,
      autovalidate: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Text(
                "Masukan lembar saham yang ingin kamu beli ",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s14,
                  fontWeight: FontWeight.w600,
                ),
              ),
              InkWell(
                onTap: () => PopupHelper.showModalSyaratDanKetentuan(
                  context,
                  widget.data.emiten.minimumInvest,
                  widget.data.emiten.price,
                  widget.data,
                ),
                child: Icon(
                  Icons.info,
                  size: FontSize.s18,
                  color: Color(0xffdddddd),
                ),
              )
            ],
          ),
          SizedBox(height: Sizes.s10),
          //lembar saham
          Container(
            decoration: BoxDecoration(color: Colors.white),
            child: TextFormField(
              key: Key('inputLembarSaham'),
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              controller: lembarController,
              readOnly: true,
              enableInteractiveSelection: false,
              validator: (value) {
                if (value.isEmpty) {
                  return '⚠ Masukkan Jumlah Lembar Saham';
                } else if (!RegExp(r"^[0-9\,]*$").hasMatch(value) ||
                    value == ',' ||
                    value.contains(',,')) {
                  return '⚠ Masukkan Input Tanpa Spesial Karakter';
                } else {
                  if (num.parse(value.replaceAll(",", "")) <
                      widget.data.emiten.minimumInvest) {
                    return '⚠ Minimal pembelian saham adalah ${widget.data.emiten.minimumInvest} lembar';
                  } else if (num.parse(value.replaceAll(",", "")) >
                      (widget.data.emiten.supply -
                          widget.data.emiten.terjual)) {
                    return '⚠ Saham yang tersisa adalah ${(widget.data.emiten.supply - widget.data.emiten.terjual.round()) < 0 ? 0 : (widget.data.emiten.supply - widget.data.emiten.terjual.round())} lembar';
                  } else {
                    FocusScope.of(context).unfocus();
                    return null;
                  }
                }
              },
              // onChanged: (value) {
              //   if (value.isEmpty) {
              //     tokenController.text = 'Rp ${rupiah.format(0)}';
              //     setState(() => widget.data.totalHarga = 0);
              //   } else if (!RegExp(r"^[0-9\,]*$").hasMatch(value) ||
              //       value == ',' ||
              //       value.contains(',,')) {
              //     tokenController.text = 'Rp ${rupiah.format(0)}';
              //     setState(() => widget.data.totalHarga = 0);
              //   } else {
              //     tokenController.text =
              //         'Rp ${rupiah.format(num.parse(lembarController.text.replaceAll(",", "")) * widget.data.emiten.price).toString()}';
              //     setState(() => widget.data.totalHarga =
              //         num.parse(value.replaceAll(",", "")) *
              //             widget.data.emiten.price);
              //   }
              // },
              decoration: InputDecoration(
                suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        lembarController.text = (int.parse(
                                    lembarController.text.replaceAll(',', '')) +
                                100)
                            .toString();
                        onChangeSaham();
                      });
                    },
                    icon: Icon(Icons.add)),
                prefixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        if (int.parse(
                                lembarController.text.replaceAll(',', '')) !=
                            0)
                          lembarController.text = (int.parse(lembarController
                                      .text
                                      .replaceAll(',', '')) -
                                  100)
                              .toString();
                        onChangeSaham();
                      });
                    },
                    icon: Icon(Icons.remove)),
                hintText: 'Minimal ${widget.data.emiten.minimumInvest} lembar',
                errorMaxLines: 5,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                ),
              ),
            ),
          ),
          SizedBox(height: Sizes.s20),
          Text(
            "Total harga saham (IDR)",
            style: TextStyle(
              color: Colors.white,
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: Sizes.s10),
          //token count
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  enabled: false,
                  cursorColor: Color(0xFFF4F5FA),
                  controller: tokenController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    filled: true,
                    fillColor: Color(0xffF6F6F6),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 6, 8, 0),
                  child: widget.data.totalHarga == null
                      ? Container()
                      : widget.data.totalHarga <
                              (widget.data.emiten.minimumInvest *
                                  widget.data.emiten.price)
                          ? Text(
                              '⚠ Minimal pembelian adalah Rp ${rupiah.format(widget.data.emiten.minimumInvest * widget.data.emiten.price)}',
                              style: TextStyle(
                                  color: Colors.red[700], fontSize: 12),
                              overflow: TextOverflow.visible,
                            )
                          : widget.data.totalHarga >
                                  ((widget.data.emiten.supply *
                                          widget.data.emiten.price) -
                                      (widget.data.emiten.terjual *
                                          widget.data.emiten.price))
                              ? Text(
                                  '⚠ Maksimal pembelian adalah Rp ${rupiah.format((widget.data.emiten.supply * widget.data.emiten.price) - (widget.data.emiten.terjual * widget.data.emiten.price) < 0 ? 0 : (widget.data.emiten.supply * widget.data.emiten.price) - (widget.data.emiten.terjual * widget.data.emiten.price))}',
                                  style: TextStyle(
                                    color: Colors.red[700],
                                    fontSize: 12,
                                  ),
                                  overflow: TextOverflow.visible,
                                )
                              : Container(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void actionBeli() {
    var picture = widget.data.emiten.pictures != null &&
            widget.data.emiten.pictures.length > 0
        ? widget.data.emiten.pictures[0].picture
        : "";
    PopupHelper.showModalKonfirmasi(
      context,
      widget.data.emiten.companyName,
      widget.data.emiten.codeEmiten,
      widget.data.emiten.price,
      picture,
      widget.data.totalHarga,
      lembarController,
      () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PaymentPage(
                    type: TransactionPaymentType.perdana,
                    codeEmiten: widget.data.emiten.codeEmiten,
                    name: widget.data.emiten.companyName,
                    emitenUuid: widget.data.emiten.uuid,
                    amount: widget.data.totalHarga / widget.data.emiten.price,
                    price: widget.data.emiten.price,
                    total: widget.data.totalHarga.toString(),
                  )),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _checkoutListBloc = BlocProvider.of<CheckoutListBloc>(context);
    _checkoutListBloc.add(LoadCheckoutList());
    _depositHistoryBloc = BlocProvider.of<DepositHistoryBloc>(context);
    _depositHistoryBloc.add(LoadDepositHistory());
    setState(() {
      isUnlimited = widget.data?.userData?.job?.isUnlimitedInvest != null &&
              widget.data.userData.job.isUnlimitedInvest == 1
          ? true
          : false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Beli Saham",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        iconTheme: IconThemeData(color: Colors.white),
        centerTitle: true,
        backgroundColor: Color(ColorRev.mainBlack),
        leading: Container(),
        actions: [
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        // padding: EdgeInsets.all(Sizes.s20),
        color: Colors.black,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SantaraAppBetaTesting(),
              ),
              ListTile(
                contentPadding:
                    EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
                title: Text(
                  "${widget.data.emiten.trademark}",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: FontSize.s20,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                subtitle: Text(
                  "${widget.data.emiten.companyName}",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(height: Sizes.s10),
              divider,
              SizedBox(height: Sizes.s10),
              ListTile(
                isThreeLine: true,
                contentPadding:
                    EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
                title: Text(
                  "Minimum Pembelian Saat Ini ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Rp. ${rupiah.format(widget.data.emiten.price * widget.data.emiten.minimumInvest)}",
                      style: TextStyle(
                        fontSize: FontSize.s24,
                        fontWeight: FontWeight.w800,
                        color: Color(0xff0E7E4A),
                      ),
                    ),
                    Text(
                      "${rupiah.format(widget.data.emiten.minimumInvest)} Lembar",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: FontSize.s14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: Sizes.s10),
              divider,
              SizedBox(height: Sizes.s30),
              Container(
                padding: EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
                child: _form(),
              ),
              SizedBox(height: Sizes.s50),
              Container(
                height: Sizes.s50,
                width: double.maxFinite,
                margin: EdgeInsets.all(Sizes.s20),
                child: FlatButton(
                  key: Key('beliSahamBtn'),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Sizes.s5),
                  ),
                  color: Color(0xff0E7E4A),
                  onPressed: () {
                    int isUnlimitedInvest =
                        widget.data?.userData?.job?.isUnlimitedInvest ?? 0;
                    try {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (formKey.currentState.validate()) {
                        if (widget.data.isVerified == 0) {
                          PopupHelper.showModalUnverified(
                            context,
                            'Akun anda belum diverifikasi oleh admin',
                          );
                        } else if (widget.data.isVerified == 1) {
                          if (((widget.data.totalHarga == null
                                      ? 0
                                      : widget.data.totalHarga) -
                                  widget.data.sisaInvest) >
                              0) {
                            if (widget.data.traderType == 2) {
                              actionBeli();
                            } else if (isUnlimitedInvest == 1) {
                              actionBeli();
                            } else {
                              PopupHelper.showModalErrorSahamLebih(
                                  context, widget.data);
                            }
                          } else {
                            actionBeli();
                          }
                        } else {
                          PopupHelper.showModalUnverified(
                            context,
                            'Terjadi kesalahan, harap hubungi CS Santara',
                          );
                        }
                      }
                    } catch (e, stack) {
                      logger.i("""
                      Is unlimited : ${widget.data?.userData?.job?.isUnlimitedInvest}
                      """);
                      santaraLog(e, stack);
                    }
                  },
                  child: Text(
                    "Beli Saham",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
