import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/home/blocs/testimoni.bloc.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class ListTestimoni extends StatelessWidget {
  double _heightTestimoni(double t) {
    if (t < 320) {
      return 370;
    } else if (t >= 320 && t < 375) {
      return 340;
    } else if (t >= 375 && t < 400) {
      return 270;
    } else if (t >= 400 && t < 450) {
      return 260;
    } else {
      return 220;
    }
  }

  Widget _testimoni(BuildContext context) {
    return BlocBuilder<TestimoniBloc, TestimoniState>(
      builder: (context, state) {
        if (state is TestimoniUninitialized) {
          return Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.white,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/slide/responsive1.jpg'),
                      fit: BoxFit.fill)),
              margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            ),
          );
        } else if (state is TestimoniLoaded) {
          TestimoniLoaded data = state;
          return Container(
              margin: EdgeInsets.only(top: 10),
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 15),
                  enlargeCenterPage: false,
                  height: _heightTestimoni(MediaQuery.of(context).size.width),
                  // aspectRatio: 16 / 14,
                ),
                items: data.testimonial.map((i) {
                  String desc = i.description;
                  String desc1 = i.description;
                  if (desc1.length > 180) {
                    desc = desc1.substring(0, 180);
                  }
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [
                        new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                    padding: EdgeInsets.fromLTRB(12, 16, 12, 8),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: SantaraCachedImage(
                                  image: urlAsset + "/success_story/" + i.image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(width: 16),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(i.title,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                      overflow: TextOverflow.visible),
                                  Text(i.subtitle,
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Color(0xFFD23737)),
                                      overflow: TextOverflow.visible),
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(height: 16),
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(12, 8, 12, 0),
                                child: Text('“',
                                    style: TextStyle(
                                        fontSize: 100,
                                        color: Color(0xFFBF2D30),
                                        fontFamily: 'Gayathri')),
                              ),
                              Flexible(child: Text(desc))
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }).toList(),
              ));
        } else if (state is TestimoniError) {
          return Center(
            child: IconButton(
                icon: Icon(Icons.replay),
                onPressed: () {
                  // print("RELOADED");
                }),
          );
        } else if (state is TestimoniEmpty) {
          return Center(
            child: Text("Belum ada testimoni!"),
          );
        } else {
          return Center(
            child: Text(
              "Unknown state",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(height: 35),
        Text("Testimoni Santara",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Container(height: 8),
        _testimoni(context),
      ],
    );
  }
}
