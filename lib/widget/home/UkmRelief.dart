import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';

class UKMReliefUI extends StatefulWidget {
  @override
  _UKMReliefUIState createState() => _UKMReliefUIState();
}

class _UKMReliefUIState extends State<UKMReliefUI> {
  void showModalSessionTimedOut(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width / 3 + 50,
                  width: MediaQuery.of(context).size.width / 3 + 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          MediaQuery.of(context).size.width / 3),
                      image: DecorationImage(
                          image: AssetImage("assets/icon/forbidden.png"),
                          fit: BoxFit.fitWidth)),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                  child: Center(
                    child: Text('Maaf, silahkan login terlebih dahulu',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 16),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _itemRelief(String img, String title, String desc) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(right: _size * 2),
      padding: EdgeInsets.all(_size * 3.5),
      width: MediaQuery.of(context).size.width * .85,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Color(0xffF0F0F0),
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Image.asset(
            "assets/icon_relief/$img",
            height: _size * 50,
          ),
          // Container(
          //   height: _size * 2,
          // ),
          Text(
            "$title",
            style:
                TextStyle(fontSize: _size * 3.5, fontWeight: FontWeight.bold),
          ),
          // Container(
          //   height: _size * 2,
          // ),
          Text(
            "$desc",
            style: TextStyle(fontSize: _size * 3),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      padding: EdgeInsets.only(left: _size * 5),
      height: _size * 95,
      width: double.infinity,
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "UKM Corona Relief",
            style:
                TextStyle(fontWeight: FontWeight.bold, fontSize: _size * 4.5),
          ),
          Container(
            height: _size * 3.75,
          ),
          Container(
            height: _size * 80,
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                _itemRelief('corona-1.png', "UKM Corona Relief",
                    "Ayo kolaborasi, putarkan kembali roda ekonomi. Dukung bisnis UKM untuk bangkit dengan cara ini."),
                _itemRelief('corona-2.png', "Untuk Anda Pembisnis",
                    "Lanjutkan Perjuangan Anda, Daftarkan Bisnis Anda Untuk Dapatkan Bantuan Dari Ribuan Pemodal di Santara."),
                _itemRelief('corona-3.png', "Untuk Anda Para Pemodal",
                    "Dukung Bisnis Yang Anda Suka Untuk Bangkit dan Melejit, Nikmati Bagi Dividen Dengan Angka Menarik."),
                _itemRelief('corona-4.png', "Penawaran Saham Dibuka",
                    "Bisnis dengan dukungan terbanyak akan segera masuk tahap review untuk listing di papan penawaran utama."),
                // Container(
                //   height: _size * 50,
                //   child: Column(
                //     mainAxisSize: MainAxisSize.min,
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: <Widget>[
                //       Container(
                //         width: _size * 10,
                //         height: _size * 10,
                //         decoration: BoxDecoration(
                //           color: Color(0xffBF2D30),
                //           shape: BoxShape.circle,
                //         ),
                //         child: IconButton(
                //             icon: Icon(
                //               Icons.arrow_forward,
                //               size: _size * 5,
                //               color: Colors.white,
                //             ),
                //             onPressed: () {
                //               Helper.check
                //                   ? Navigator.push(
                //                       context,
                //                       MaterialPageRoute(
                //                           builder: (context) =>
                //                               PreRegistrasiUI()))
                //                   : showModalSessionTimedOut(context);
                //             }),
                //       ),
                //       Container(
                //         height: _size,
                //         width: _size * 30,
                //       ),
                //       Text(
                //         "Coba Sekarang",
                //         style: TextStyle(
                //             color: Color(0xffBF2D30), fontSize: _size * 3.5),
                //       )
                //     ],
                //   ),
                // )
              ],
            ),
          )
        ],
      ),
    );
  }
}
