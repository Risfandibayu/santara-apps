import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/market/home_market.dart';

class Market extends StatefulWidget {
  @override
  _MarketState createState() => _MarketState();
}

class _MarketState extends State<Market> {
  final storage = FlutterSecureStorage();
  bool isMarketOpen = false;

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    setState(() => isMarketOpen = remoteConfig.getBool(isOpenMarket));
  }

  @override
  void initState() {
    super.initState();
    getRemoteConfig();
  }

  @override
  Widget build(BuildContext context) {
    if (isMarketOpen) {
      return Container(
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.fromLTRB(16, 32, 16, 0),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xFFF0F0F0)),
            borderRadius: BorderRadius.circular(6)),
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Image.asset(
              "assets/icon/market_home.png",
              height: 150,
              fit: BoxFit.fitHeight,
            ),
            GestureDetector(
              onTap: () async {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => HomeMarket()));
              },
              child: Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 3),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(width: 1, color: Color(0xFFF0F0F0)),
                    borderRadius: BorderRadius.circular(6)),
                child: Column(
                  children: [
                    Text(
                      "Pasar sekunder telah resmi dibuka.",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    RichText(
                      text: TextSpan(
                          style: TextStyle(
                              fontFamily: "Nunito", color: Colors.black),
                          text: "Silahkan ",
                          children: [
                            TextSpan(
                                text: "tab",
                                style: TextStyle(
                                    color: Color(0xFFBF2D30),
                                    decoration: TextDecoration.underline)),
                            TextSpan(
                              text: " untuk mengunjungi Pasar Sekunder.",
                            )
                          ]),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
