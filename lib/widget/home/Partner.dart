import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:santaraapp/models/Supporter.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/WebView.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class Partner extends StatefulWidget {
  @override
  _PartnerState createState() => _PartnerState();
}

class _PartnerState extends State<Partner> {
  var loading = true;
  var page = PageController(viewportFraction: 1 / 2);
  String wa =
      "https://api.whatsapp.com/send?phone=6281212227765&text=Halo, apakah ada informasi terbaru mengenai Santara?";

  List<Supporter> suppoter = [
    Supporter(
        name: "Komninfo",
        link: "https://santarax.com/welcome/kominfo",
        logo: "assets/partner/Card-Kominfo.png"),
    Supporter(
        name: "Komninfo",
        link: "https://pse.kominfo.go.id/sistem/1613",
        logo: "assets/partner/Card-Kominfo.png"),
    Supporter(
        name: "Komninfo",
        link: "https://santarax.com/welcome/aftech",
        logo: "assets/partner/Card-Kominfo.png"),
  ];

  Future getContact() async {
    final response =
        await http.get('$apiLocal/contacts'); //.timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      wa = data["whatsapp"];
    }
  }

  Future launchWA() async {
    var url = wa;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Cannot launch $url';
    }
  }

  Future getSupporter() async {
    final response = await http.get("$apiLocal/supporters");
    if (response.statusCode == 200) {
      setState(() {
        List<Supporter> s = supporterFromJson(response.body);
        List<Supporter> sNew = [];
        for (var i = 0; i < s.length; i++) {
          if (!s[i].link.toUpperCase().contains("OJK")) {
            sNew.add(s[i]);
          }
        }
        suppoter = sNew;
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getSupporter();
    getContact();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Center(
                child: Text(
              'Equity Crowdfunding Terbesar Pertama Berizin OJK',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            )),
          ),
          Center(
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => WebViewWidget(
                      appBarName: "Santara Website View",
                      url:
                          "https://www.ojk.go.id/id/berita-dan-kegiatan/siaran-pers/Pages/Siaran-Pers-OJK-Keluarkan-Izin-Penyelenggara-Layanan-Urun-Dana-Berbasis-Teknologi-Informasi-(Equity-Crowd-Funding)-.aspx",
                    ),
                  ),
                );
              },
              child: Container(
                height: Sizes.s100,
                width: Sizes.s150,
                margin: EdgeInsets.only(bottom: Sizes.s15),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/santara/ojk.png'),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Center(
                child: Text(
              'Didukung Oleh',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            )),
          ),
          Container(
            color: Colors.white,
            margin: const EdgeInsets.symmetric(vertical: 16),
            child: CarouselSlider(
              options: CarouselOptions(
                height: MediaQuery.of(context).size.width / 5,
                viewportFraction: 0.4,
                enlargeCenterPage: false,
                autoPlay: true,
              ),
              items: suppoter.map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    if (loading) {
                      return Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  image: AssetImage(i.logo), fit: BoxFit.fill)),
                          margin:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                        ),
                      );
                    } else {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => WebViewWidget(
                                appBarName: "Santara Website View",
                                url: i.link,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: SantaraCachedImage(
                            width: MediaQuery.of(context).size.width / 3 - 20,
                            image: "$urlAsset/supporter/${i.logo}",
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      );
                    }
                  },
                );
              }).toList(),
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(top: Sizes.s20),
              child: Column(
                children: <Widget>[
                  Text("Memiliki pertanyaan terkait",
                      style: TextStyle(color: Color(0xFF676767), fontSize: 18)),
                  RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                      style: TextStyle(
                          color: Color(0xFF676767),
                          fontFamily: 'Nunito',
                          fontSize: 18),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Santara ? ',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: ' Hubungi kami')
                      ],
                    ),
                  ),
                  Container(height: 8),
                  FlatButton(
                    onPressed: launchWA,
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width - 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Color(0xFF01AA59)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset("assets/icon/whatsapp.png"),
                          Container(width: 12),
                          Text(
                            "WhatsApp",
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(height: 40),
                ],
              ),
            ),
          ),
          Container(
            color: Color(0xFFB33F5B),
            margin: EdgeInsets.only(top: 30),
            child: Stack(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.height / 10,
                ),
                Container(
                    margin: const EdgeInsets.fromLTRB(15, 0, 15, 40),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 20),
                    decoration: BoxDecoration(
                        // color: Color(0xFFE7E9FF),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(width: 1, color: Colors.white),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Color(0xFFB33F5B).withOpacity(0.5),
                            blurRadius: 5,
                          )
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Center(
                            child: Text(
                          "\"OTORITAS JASA KEUANGAN TIDAK MEMBERIKAN PERNYATAAN MENYETUJUI ATAU TIDAK MENYETUJUI EFEK INI, TIDAK JUGA MENYATAKAN KEBENARAN ATAU KECUKUPAN INFORMASI DALAM LAYANAN URUN DANA INI. SETIAP PERNYATAAN YANG BERTENTANGAN DENGAN HAL TERSEBUT ADALAH PERBUATAN MELANGGAR HUKUM.\”",
                          style: TextStyle(
                              fontSize: 7, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.justify,
                        )),
                        Container(height: 4),
                        Center(
                            child: Text(
                          "\“INFORMASI DALAM LAYANAN URUN DANA INI PENTING DAN PERLU MENDAPAT PERHATIAN SEGERA. APABILA TERDAPAT KERAGUAN PADA TINDAKAN YANG AKAN DIAMBIL, SEBAIKNYA BERKONSULTASI DENGAN PENYELENGGARA.\” dan",
                          style: TextStyle(
                              fontSize: 7, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.justify,
                        )),
                        Container(height: 4),
                        Center(
                            child: Text(
                          "\“PENERBIT DAN PENYELENGGARA, BAIK SENDIRI-SENDIRI MAUPUN BERSAMA-SAMA, BERTANGGUNG JAWAB SEPENUHNYA ATAS KEBENARAN SEMUA INFORMASI YANG TERCANTUM DALAM LAYANAN URUN DANA INI.\"",
                          style: TextStyle(
                              fontSize: 7, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.justify,
                        )),
                      ],
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
