import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class DetailEmiten extends StatefulWidget {
  final nama;
  final owner;
  final photo;
  final bio;
  final createdAt;

  DetailEmiten({this.nama, this.owner, this.photo, this.bio, this.createdAt});
  @override
  State<StatefulWidget> createState() {
    return _DetailEmitenState(
      nama: nama,
      owner: owner,
      photo: photo,
      bio: bio,
      createdAt: createdAt,
    );
  }
}

class _DetailEmitenState extends State<DetailEmiten> {
  final nama;
  final owner;
  final photo;
  final bio;
  final createdAt;

  _DetailEmitenState({
    this.nama,
    this.owner,
    this.photo,
    this.bio,
    this.createdAt,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            MdiIcons.arrowLeft,
            size: 30,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              nama,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            Text(
              'Detail',
              style: TextStyle(fontSize: 11, color: Colors.black),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
            margin: EdgeInsets.only(left: 30, right: 30, top: 30),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: Text(
                    'Profil $owner',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 60,
                  height: MediaQuery.of(context).size.width - 60,
                  child: photo == null
                      ? Image.asset('assets/icon/user.png')
                      : SantaraCachedImage(
                          // placeholder: 'assets/icon/user.png',
                          image: "$apiLocalImage${widget.photo}",
                          fit: BoxFit.cover,
                        ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    bio,
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.justify,
                  ),
                )
              ],
            )),
      ),
    );
  }
}
