import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/models/Testimonial.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class Testimoni extends StatefulWidget {
  @override
  _TestimoniState createState() => _TestimoniState();
}

class _TestimoniState extends State<Testimoni> {
  var loading = true;
  List<Testimonial> testimonial = [
    Testimonial(),
    Testimonial(),
    Testimonial(),
  ];

  Future getTestimoni() async {
    final response = await http.get("$apiLocal/information/success-stories");
    if (response.statusCode == 200) {
      final t = testimonialFromJson(response.body);
      setState(() {
        testimonial = t;
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getTestimoni();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(height: 35),
        Text(
          "Testimoni Santara",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Container(height: 8),
        _testimoni(),
      ],
    );
  }

  double _heightTestimoni(double t) {
    if (t < 320) {
      return 370;
    } else if (t >= 320 && t < 375) {
      return 340;
    } else if (t >= 375 && t < 400) {
      return 270;
    } else if (t >= 400 && t < 450) {
      return 260;
    } else {
      return 220;
    }
  }

  Widget _testimoni() {
    return Container(
        margin: EdgeInsets.only(top: 10),
        child: CarouselSlider(
          options: CarouselOptions(
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 15),
            enlargeCenterPage: false,
            height: _heightTestimoni(MediaQuery.of(context).size.width),
            // aspectRatio: 16 / 14,
          ),
          items: testimonial.map((i) {
            return Builder(
              builder: (BuildContext context) {
                if (loading) {
                  return Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.white,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image: AssetImage('assets/slide/responsive1.jpg'),
                          fit: BoxFit.fill,
                        ),
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    ),
                  );
                } else {
                  String desc = i.description;
                  String desc1 = i.description;
                  if (desc1.length > 180) {
                    desc = desc1.substring(0, 180);
                  }
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      boxShadow: [
                        BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
                      ],
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                    padding: EdgeInsets.fromLTRB(12, 16, 12, 8),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              height: 60,
                              width: 60,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(60),
                                child: SantaraCachedImage(
                                  image: urlAsset + "/success_story/" + i.image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Container(width: 16),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    i.title,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.visible,
                                  ),
                                  Text(
                                    i.subtitle,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xFFD23737),
                                    ),
                                    overflow: TextOverflow.visible,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(height: 16),
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(12, 8, 12, 0),
                                child: Text(
                                  '“',
                                  style: TextStyle(
                                    fontSize: 100,
                                    color: Color(0xFFBF2D30),
                                    fontFamily: 'Gayathri',
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Text(desc),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }
              },
            );
          }).toList(),
        ));
  }
}
