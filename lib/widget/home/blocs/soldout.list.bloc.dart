import 'package:bloc/bloc.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/soldoutmodel.dart';
import 'package:santaraapp/services/api_service.dart';
import 'dart:async';

import 'package:santaraapp/utils/api.dart';

class SoldOutListEvent {}

class SoldOutListState {}

class SoldOutListUninitialized extends SoldOutListState {}

class SoldOutListEmpty extends SoldOutListState {}

class SoldOutListError extends SoldOutListState {
  String error;
  SoldOutListError({this.error});

  SoldOutListError copyWith({String error}) {
    return SoldOutListError(error: error ?? this.error);
  }
}

class SoldOutListLoaded extends SoldOutListState {
  List<SoldoutModel> emiten; // daftar emiten
  DateTime now; // current time

  SoldOutListLoaded({
    this.emiten,
    this.now,
  });

  SoldOutListLoaded copyWith({List<Emiten> emiten, DateTime now}) {
    return SoldOutListLoaded(
      emiten: emiten ?? this.emiten,
      now: now ?? this.now,
    );
  }
}

//soldout

class SoldOutListBloc extends Bloc<SoldOutListEvent, SoldOutListState> {
  final ApiService apiService = ApiService();

  int getTimes = 0;
  // get emitens
  Future<List<SoldoutModel>> getEmitens() async {
    // print(">> Get Emiten");
    try {
      // final http.Response response =
      //     await http.get('$apiLocal/emitens/?type=saham');

      final http.Response response = await http.get(
          '$apiLocal/emitens/emiten?projectValue1=&projectValue2=&category=&search=&sort=&pageSize=10&pageNumber=1&type=saham&jenis=full');

      print(response.body);
      if (response.statusCode == 200) {
        // var emitens = emitenFromJson(response.body);

        var emitens = soldoutModelFromJson(response.body);

        return emitens;
      } else {
        if (getTimes < 3) {
          // print(">> Retrial 1");
          getEmitens();
          getTimes += 1;
        } else {
          return null;
        }
      }
    } catch (e) {
      if (getTimes < 3) {
        // print(">> Retrial 2");
        getEmitens();
        getTimes += 1;
      } else {
        return null;
      }
    }
  }

  SoldOutListBloc(SoldOutListState initialState)
      : super(SoldOutListUninitialized());

  @override
  Stream<SoldOutListState> mapEventToState(SoldOutListEvent event) async* {
    // if (state is SoldOutListUninitialized) {

    // }
    try {
      // print(">> HELLO");
      // yield SoldOutListUninitialized();
      DateTime recentTimeStamp = await apiService.getCurrentTime();
      List<SoldoutModel> emitens = await getEmitens();
      if (emitens != null) {
        if (emitens.length > 0) {
          yield SoldOutListLoaded(
            emiten: emitens,
            now: recentTimeStamp,
          );
        } else {
          yield SoldOutListEmpty();
        }
      } else {
        yield SoldOutListError(
          error: "Tidak dapat memuat daftar bisnis, coba lagi.",
        );
      }
    } catch (e) {
      yield SoldOutListError(
        error: "Tidak dapat memuat daftar bisnis, coba lagi.",
      );
    }
  }
}
