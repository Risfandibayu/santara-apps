import 'package:bloc/bloc.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/services/api_service.dart';
import 'dart:async';

import 'package:santaraapp/models/comingsoonmodel.dart';

class ComingSoonListEvent {}

class ComingSoonListState {}

class ComingSoonListUninitialized extends ComingSoonListState {}

class ComingSoonListEmpty extends ComingSoonListState {}

class ComingSoonListError extends ComingSoonListState {
  String error;
  ComingSoonListError({this.error});

  ComingSoonListError copyWith({String error}) {
    return ComingSoonListError(error: error ?? this.error);
  }
}

class ComingSoonListLoaded extends ComingSoonListState {
  List<ComingSoonModel> emiten; // daftar emiten
  DateTime now; // current time

  ComingSoonListLoaded({
    this.emiten,
    this.now,
  });

  ComingSoonListLoaded copyWith({List<Emiten> emiten, DateTime now}) {
    return ComingSoonListLoaded(
      emiten: emiten ?? this.emiten,
      now: now ?? this.now,
    );
  }
}

//ComingSoon

class ComingSoonListBloc
    extends Bloc<ComingSoonListEvent, ComingSoonListState> {
  final ApiService apiService = ApiService();

  int getTimes = 0;
  // get emitens
  Future<List<ComingSoonModel>> getEmitens() async {
    // print(">> Get Emiten");
    try {
      // final http.Response response =
      //     await http.get('$apiLocal/emitens/?type=saham');

      final http.Response response = await http
          .get('https://fire.santarax.com:3701/v3.7.1/emitens/coming-soon');

      if (response.statusCode == 200) {
        // var emitens = emitenFromJson(response.body);

        var emitens = comingSoonModelFromJson(response.body);

        return emitens;
      } else {
        if (getTimes < 3) {
          // print(">> Retrial 1");
          getEmitens();
          getTimes += 1;
        } else {
          return null;
        }
      }
    } catch (e) {
      if (getTimes < 3) {
        // print(">> Retrial 2");
        getEmitens();
        getTimes += 1;
      } else {
        return null;
      }
    }
  }

  ComingSoonListBloc(ComingSoonListState initialState)
      : super(ComingSoonListUninitialized());

  @override
  Stream<ComingSoonListState> mapEventToState(
      ComingSoonListEvent event) async* {
    // if (state is ComingSoonListUninitialized) {

    // }
    try {
      // print(">> HELLO");
      // yield ComingSoonListUninitialized();
      DateTime recentTimeStamp = await apiService.getCurrentTime();
      List<ComingSoonModel> emitens = await getEmitens();
      if (emitens != null) {
        if (emitens.length > 0) {
          yield ComingSoonListLoaded(
            emiten: emitens,
            now: recentTimeStamp,
          );
        } else {
          yield ComingSoonListEmpty();
        }
      } else {
        yield ComingSoonListError(
          error: "Tidak dapat memuat daftar bisnis, coba lagi.",
        );
      }
    } catch (e) {
      yield ComingSoonListError(
        error: "Tidak dapat memuat daftar bisnis, coba lagi.",
      );
    }
  }
}
