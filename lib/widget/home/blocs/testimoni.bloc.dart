import 'package:bloc/bloc.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/Testimonial.dart';
import 'dart:async';
import 'package:santaraapp/utils/api.dart';

class TestimoniEvent {}

class TestimoniState {}

class TestimoniUninitialized extends TestimoniState {}

class TestimoniEmpty extends TestimoniState {}

class TestimoniError extends TestimoniState {
  String error;
  TestimoniError({this.error});

  TestimoniError copyWith({String error}) {
    return TestimoniError(error: error ?? this.error);
  }
}

class TestimoniLoaded extends TestimoniState {
  List<Testimonial> testimonial; // daftar testimoni

  TestimoniLoaded({
    this.testimonial,
  });

  TestimoniLoaded copyWith({List<Testimonial> testimonial}) {
    return TestimoniLoaded(testimonial: testimonial ?? this.testimonial);
  }
}

class TestimoniBloc extends Bloc<TestimoniEvent, TestimoniState> {
  TestimoniBloc(TestimoniState initialState) : super(TestimoniUninitialized());

  // fetch testimonial
  Future<List<Testimonial>> getTestimoni() async {
    final response = await http.get("$apiLocal/information/success-stories");
    if (response.statusCode == 200) {
      return testimonialFromJson(response.body);
    } else {
      return null;
    }
  }

  @override
  Stream<TestimoniState> mapEventToState(TestimoniEvent event) async* {
    if (state is TestimoniUninitialized) {
      try {
        // print(">> Fetching Daftar Testimonial..");
        List<Testimonial> testimoni = await getTestimoni();
        if (testimoni != null) {
          if (testimoni.length > 0) {
            yield TestimoniLoaded(testimonial: testimoni);
          } else {
            yield TestimoniEmpty();
          }
        } else {
          yield TestimoniError(
            error: "Tidak dapat memuat testimonial, coba lagi.",
          );
        }
      } catch (e) {
        yield TestimoniError(
          error: "Tidak dapat memuat testimonial, coba lagi.",
        );
      }
    }
  }
}
