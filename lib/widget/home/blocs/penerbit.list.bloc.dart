import 'package:bloc/bloc.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/services/api_service.dart';
import 'dart:async';

import 'package:santaraapp/utils/api.dart';

class PenerbitListEvent {}

class PenerbitListState {}

class PenerbitListUninitialized extends PenerbitListState {}

class PenerbitListEmpty extends PenerbitListState {}

class PenerbitListError extends PenerbitListState {
  String error;
  PenerbitListError({this.error});

  PenerbitListError copyWith({String error}) {
    return PenerbitListError(error: error ?? this.error);
  }
}

class PenerbitListLoaded extends PenerbitListState {
  List<Emiten> emiten; // daftar emiten
  DateTime now; // current time

  PenerbitListLoaded({
    this.emiten,
    this.now,
  });

  PenerbitListLoaded copyWith({List<Emiten> emiten, DateTime now}) {
    return PenerbitListLoaded(
      emiten: emiten ?? this.emiten,
      now: now ?? this.now,
    );
  }
}

//soldout

class PenerbitListBloc extends Bloc<PenerbitListEvent, PenerbitListState> {
  final ApiService apiService = ApiService();

  int getTimes = 0;
  // get emitens
  Future<List<Emiten>> getEmitens() async {
    // print(">> Get Emiten");
    try {
      // final http.Response response =
      //     await http.get('$apiLocal/emitens/?type=saham');

      final http.Response response = await http.get(
          '$apiLocal/emitens/emiten?projectValue1=&projectValue2=&category=&search=&sort=&pageSize=10&pageNumber=1&type=saham&jenis=notfull');

      print(response.body);
      if (response.statusCode == 200) {
        var emitens = emitenFromJson(response.body);

        return emitens;
      } else {
        if (getTimes < 3) {
          // print(">> Retrial 1");
          getEmitens();
          getTimes += 1;
        } else {
          return null;
        }
      }
    } catch (e) {
      if (getTimes < 3) {
        // print(">> Retrial 2");
        getEmitens();
        getTimes += 1;
      } else {
        return null;
      }
    }
  }

  PenerbitListBloc(PenerbitListState initialState)
      : super(PenerbitListUninitialized());

  @override
  Stream<PenerbitListState> mapEventToState(PenerbitListEvent event) async* {
    // if (state is PenerbitListUninitialized) {

    // }
    try {
      // print(">> HELLO");
      // yield PenerbitListUninitialized();
      DateTime recentTimeStamp = await apiService.getCurrentTime();
      List<Emiten> emitens = await getEmitens();
      if (emitens != null) {
        if (emitens.length > 0) {
          yield PenerbitListLoaded(
            emiten: emitens,
            now: recentTimeStamp,
          );
        } else {
          yield PenerbitListEmpty();
        }
      } else {
        yield PenerbitListError(
          error: "Tidak dapat memuat daftar bisnis, coba lagi.",
        );
      }
    } catch (e) {
      yield PenerbitListError(
        error: "Tidak dapat memuat daftar bisnis, coba lagi.",
      );
    }
  }
}
