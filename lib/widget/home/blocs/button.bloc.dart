import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/kyc/StatusTraderKyc.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/logger.dart';

enum ButtonStatus { unauthorized, unverified, verified }

class ButtonEvent {}

class LoadButton extends ButtonEvent {}

class ButtonState {}

class ButtonUninitialized extends ButtonState {}

class ButtonLoaded extends ButtonState {
  ButtonStatus verified;

  ButtonLoaded({this.verified = ButtonStatus.unauthorized});

  ButtonLoaded copyWith({bool verified}) {
    return ButtonLoaded(verified: verified ?? this.verified);
  }
}

class ButtonError extends ButtonState {
  String error;
  ButtonError({this.error});

  ButtonError copyWith({String error}) {
    return ButtonError(error: error ?? this.error);
  }
}

class ButtonBloc extends Bloc<ButtonEvent, ButtonState> {
  ButtonBloc(ButtonState initialState) : super(initialState);
  final storage = FlutterSecureStorage();

  Future<http.Response> checking() async {
    var token = await storage.read(key: 'token');
    var uuid = await storage.read(key: 'uuid');
    if (Helper.check) {
      final http.Response response = await http
          .get('$apiLocal/users/by-uuid/$uuid', headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token'
      }).timeout(Duration(seconds: 15));
      return response;
    } else {
      return null;
    }
  }

  @override
  Stream<ButtonState> mapEventToState(ButtonEvent event) async* {
    if (event is LoadButton) {
      // print(">> Load Button");
    }
    if (Helper.check) {
      if (event is LoadButton) {
        try {
          var result = await checking();
          if (result.statusCode == 200) {
            final data = userFromJson(result.body);
            if (data.trader.traderType == "personal") {
              KycPersonalService _api = KycPersonalService();
              var _response = await _api.statusIndividualKyc();
              if (_response != null) {
                var _res = StatusTraderKycHelper.checkingIndividual(
                    StatusKycTrader.fromJson(_response.data));
                if (_res.status == EndStatus.verified) {
                  yield ButtonLoaded(verified: ButtonStatus.verified);
                } else {
                  yield ButtonLoaded(verified: ButtonStatus.unverified);
                }
              }
            } else if (data.trader.traderType == "company") {
              KycPerusahaanService _api = KycPerusahaanService();
              var _response = await _api.statusKycTrader();
              if (_response != null) {
                var _res = StatusTraderKycHelper.checkingCompany(
                    StatusKycTrader.fromJson(_response.data));
                if (_res.status == EndStatus.verified) {
                  yield ButtonLoaded(verified: ButtonStatus.verified);
                } else {
                  yield ButtonLoaded(verified: ButtonStatus.unverified);
                }
              }
            } else if (data.trader == null ||
                data.trader.traderType == null ||
                data.trader.traderType == "") {
              yield ButtonLoaded(verified: ButtonStatus.unverified);
            } else {
              yield ButtonLoaded(verified: ButtonStatus.unverified);
            }
          } else {
            yield ButtonError(error: "Tidak dapat memuat");
          }
        } catch (e, stack) {
          santaraLog(e, stack);
        }
      }
    } else {
      yield ButtonLoaded(verified: ButtonStatus.unauthorized);
    }
  }
}
