import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/KycStatusHelper.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'dart:async';

import 'package:santaraapp/utils/logger.dart';

class PralistingListEvent {}

class PralistingListState {}

class PralistingListUninitialized extends PralistingListState {}

class PralistingListEmpty extends PralistingListState {}

class PralistingListError extends PralistingListState {
  String error;
  PralistingListError({this.error});

  PralistingListError copyWith({String error}) {
    return PralistingListError(error: error ?? this.error);
  }
}

class PralistingListLoaded extends PralistingListState {
  List<DataPralisting> data; // pralisting list
  bool hasKyc;
  String token;
  PralistingListLoaded({
    this.data,
    this.hasKyc,
    this.token,
  });

  PralistingListLoaded copyWith({List<DataPralisting> data, bool hasKyc}) {
    return PralistingListLoaded(
      data: data ?? this.data,
      hasKyc: hasKyc ?? this.hasKyc,
      token: token ?? this.token,
    );
  }
}

class PralistingListBloc
    extends Bloc<PralistingListEvent, PralistingListState> {
  ListingApiService _service = ListingApiService();

  PralistingListBloc(PralistingListState initialState)
      : super(PralistingListUninitialized());

  Future<List<DataPralisting>> getPeekPralisting() async {
    List<DataPralisting> datas = List<DataPralisting>();
    var result = await _service.fetchPrelistingNonKyc();

    if (result.statusCode == 200) {
      final res = json.decode(result.body);
      res.forEach((val) {
        var dummy = DataPralisting.fromJson(val);
        datas.add(dummy);
      });
      // print(">> Datas : ${datas.length}");
      return datas;
    } else if (result.statusCode == 404) {
      return datas;
    } else {
      return null;
    }
  }

  @override
  Stream<PralistingListState> mapEventToState(
      PralistingListEvent event) async* {
    KycStatusHelper kycStatusHelper = KycStatusHelper();
    final storage = FlutterSecureStorage();
    var token = await storage.read(key: 'token');
    // if (state is PralistingListUninitialized) {
    List<DataPralisting> pralisting;
    bool hasKyc = false;
    yield PralistingListUninitialized();
    try {
      if (token != null) {
        await kycStatusHelper.check().then((value) async {
          if (value) {
            pralisting = await getPeekPralisting();
            hasKyc = true;
          } else {
            pralisting = await getPeekPralisting();
          }
        });
      } else {
        pralisting = await getPeekPralisting();
      }
      if (pralisting != null) {
        if (pralisting.length > 0) {
          yield PralistingListLoaded(
            data: pralisting,
            hasKyc: hasKyc,
            token: token,
          );
        } else {
          yield PralistingListEmpty();
        }
      } else {
        yield PralistingListError(
          error: "Tidak dapat memuat pralisting, coba lagi.",
        );
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      // print(e);
      // print(stack);
      yield PralistingListError(
        error: "Tidak dapat memuat pralisting, coba lagi.",
      );
    }
    // }
  }
}
