import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/home/AllComingsoonList.dart';
import 'package:santaraapp/widget/home/AllPenerbitList.dart';
import 'package:santaraapp/widget/home/blocs/pralisting.list.bloc.dart';
import 'package:santaraapp/widget/listing/dashboard/PengajuanBisnisUI.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/listing/BusinessTile.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBottomSheet.dart';
import 'package:shimmer/shimmer.dart';

class ListPralisting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PralistingListBloc>(
      create: (_) => PralistingListBloc(PralistingListUninitialized())
        ..add(PralistingListEvent()),
      child: ListPralistingContent(),
    );
  }
}

class ListPralistingContent extends StatefulWidget {
  @override
  _ListPralistingContentState createState() => _ListPralistingContentState();
}

class _ListPralistingContentState extends State<ListPralistingContent>
    with AutomaticKeepAliveClientMixin {
  PralistingListBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<PralistingListBloc>(context);
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // ignore: close_sinks
    final bloc = BlocProvider.of<PralistingListBloc>(context);
    SizeConfig().init(context);
    return BlocBuilder<PralistingListBloc, PralistingListState>(
        builder: (context, state) {
      if (state is PralistingListError) {
        return Center(
          child: IconButton(
              icon: Icon(Icons.replay),
              onPressed: () {
                bloc.add(PralistingListEvent());
              }),
        );
      } else if (state is PralistingListUninitialized) {
        return ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 1,
            itemBuilder: (context, index) {
              return BusinessTileLoader();
            });
      } else if (state is PralistingListEmpty) {
        return Center(
          child: Text(
            "Data pralisting tidak ada, daftarkan bisnis anda sekarang!",
            style: TextStyle(color: Colors.white),
          ),
        );
      } else if (state is PralistingListLoaded) {
        PralistingListLoaded data = state;
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListTile(
              title: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: [
                        Text(
                          "Coming Soon",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: SizeConfig.safeBlockHorizontal * 4.3,
                              color: Colors.white),
                        ),
                        SizedBox(width: 150),
                        // GestureDetector(
                        //   key: Key('viewAllPraListing'),
                        //   onTap: () async {
                        //     await Navigator.push(context,
                        //         MaterialPageRoute(builder: (context) {
                        //       return AllComngsoonList();
                        //     }));
                        //   },
                        //   child: Text(
                        //     'Lihat Semua',
                        //     style: TextStyle(
                        //         fontFamily: 'Inter',
                        //         color: Color(0xff218196),
                        //         fontSize: 12),
                        //   ),
                        // )
                        data.hasKyc
                            ? InkWell(
                              
                                onTap: () async {
                                  await Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return AllComngsoonList();
                                  }));
                                },
                                child: Container(
                                  height: 35,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "Lihat Semua",
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.safeBlockHorizontal *
                                                  3.5,
                                          color: Color(0xff218196)),
                                    ),
                                  ),
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.6,
                width: MediaQuery.of(context).size.width,
                key: Key('pralistingListKey'),
                // height: 200,

                margin: EdgeInsets.only(top: 2),
                child: ListView.builder(
                    // physics: NeverScrollableScrollPhysics(),
                    // shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    itemCount: data.data.length < 7 ? data.data.length : 6,
                    itemBuilder: (context, index) {
                      return Container(
                        key: Key(data.data[index].trademark),
                        child: BusinessTile2(
                          onTap: () async {
                            if (data.hasKyc) {
                              final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PralistingDetailPage(
                                    status: 2,
                                    uuid: data.data[index].uuid,
                                    // uuid: 18.toString(),
                                    position: null,
                                  ),
                                ),
                              );
                              if (result != null) {
                                bloc.add(PralistingListEvent());
                              }
                            } else {
                              SantaraBottomSheet.show(
                                context,
                                data.token != null
                                    ? "Maaf tidak dapat mengakses halaman ini, akun anda belum terverifikasi."
                                    : "Maaf, silakan login terlebih dahulu.",
                              );
                            }
                          },
                          onDelete: null,
                          data: data.data[index],
                          isPengajuan: false,
                          showPosition: false,
                          status: 1,
                          showEngagement: true,
                        ),
                      );
                    }))
          ],
        );
      } else {
        return Center(child: Text("Unknown state"));
      }
    });
  }

  @override
  bool get wantKeepAlive => true;

  Widget _loadingBuilder() {
    return Container(
        height: 400,
        padding: EdgeInsets.all(4),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        width: 150,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
