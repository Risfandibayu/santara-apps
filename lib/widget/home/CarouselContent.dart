import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/blocs/home/banner_bloc.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class CarouselContent extends StatefulWidget {
  @override
  _CarouselContentState createState() => _CarouselContentState();
}

class _CarouselContentState extends State<CarouselContent> {
  BannerBloc _bannerBloc;

  @override
  void initState() {
    super.initState();
    _bannerBloc = BlocProvider.of<BannerBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _bannerBloc.close();
  }

  Widget _placeholder() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.circular(4),
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 5,
        vertical: 5,
      ),
    );
  }

  Widget _imageWrapper(String image, String url) {
    return InkWell(
      onTap: url.isNotEmpty
          ? () async {
              try {
                await launch(url, forceSafariVC: false);
              } catch (_, __) {
                logger.i("""
                  >> Error : 
                  """ +
                    _);
              }
            }
          : null,
      child: CachedNetworkImage(
        imageUrl: "$image",
        placeholder: (context, url) => _placeholder(),
        errorWidget: (context, data, _) => _placeholder(),
        imageBuilder: (context, image) => Container(
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(4),
            image: DecorationImage(
              image: image,
              fit: BoxFit.fill,
            ),
          ),
          margin: EdgeInsets.symmetric(
            horizontal: 5,
            vertical: 5,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BannerBloc, BannerState>(
      builder: (context, state) {
        if (state is BannerInitial) {
          return Container(
            margin: EdgeInsets.only(top: 10),
            child: Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                ),
                items: state.string
                    .map((i) => Container(
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(4),
                          image: DecorationImage(
                            image: AssetImage(i),
                            fit: BoxFit.fill,
                          ),
                        ),
                        margin:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 5)))
                    .toList(),
              ),
            ),
          );
        }
        if (state is BannerFailure) {
          return Container(
            margin: EdgeInsets.only(top: 10),
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
              ),
              items: state.string
                  .map(
                    (i) => Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(4),
                        image: DecorationImage(
                          image: AssetImage(i),
                          fit: BoxFit.fill,
                        ),
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    ),
                  )
                  .toList(),
            ),
          );
        }
        if (state is BannerSuccess) {
          return Container(
            margin: EdgeInsets.only(top: 10),
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 5),
              ),
              items: state.banner
                  .map(
                    (i) => _imageWrapper(
                      "$urlAsset/header/${i.mobile}",
                      "${i?.link ?? ''}",
                    ),
                  )
                  .toList(),
            ),
          );
        }
        return Container();
      },
    );
  }
}
