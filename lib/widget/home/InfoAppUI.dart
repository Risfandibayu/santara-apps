import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class InfoAppUI extends StatefulWidget {
  @override
  _InfoAppUIState createState() => _InfoAppUIState();
}

class _InfoAppUIState extends State<InfoAppUI> {
  String versionApp = "";
  String versionBusiness = "";

  Future getPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    final response = await http.get("$apiLocal/information/business-version");
    var data = jsonDecode(response.body);
    setState(() {
      versionApp = info.version;
      versionBusiness = data["business_version"];
    });
  }

  @override
  void initState() {
    super.initState();
    getPackageInfo();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        color: Colors.white,
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(),
            Image.asset(
              "assets/santara/2.png",
              height: _size * 35,
            ),
            SizedBox(
              height: _size * 10,
            ),
            Text(
              "Santara App",
              style:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: _size * 6),
            ),
            SizedBox(
              height: _size * 2,
            ),
            Text("Ver. App $versionApp ● Ver. Bisnis $versionBusiness"),
            Spacer(),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Copyright © 2021 ",
                    style: TextStyle(fontSize: _size * 3),
                  ),
                  Image.asset(
                    "assets/santara/1.png",
                    height: _size * 2,
                  ),
                  Text(
                    " , All rights reserved.",
                    style: TextStyle(fontSize: _size * 3),
                  )
                ],
              ),
            ),
            SizedBox(
              height: _size * 10,
            ),
          ],
        ),
      ),
    );
  }
}
