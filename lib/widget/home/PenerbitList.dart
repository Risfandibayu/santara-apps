import 'dart:async';

import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:santaraapp/widget/home/AllPenerbitList.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class PenerbitList extends StatefulWidget {
  @override
  _PenerbitListState createState() => _PenerbitListState();
}

class _PenerbitListState extends State<PenerbitList> {
  int present = 4;
  List<Emiten> emiten;
  var loading = true;
  final rupiah = new NumberFormat("#,###");
  DateTime now;
  Timer timer;
  final apiService = ApiService();

  Future getEmiten() async {
    final http.Response response = await http.get('$apiLocal/emitens/');
    if (response.statusCode == 200) {
      setState(() {
        emiten = emitenFromJson(response.body);
        loading = false;
      });
    } else {
      // print('data tidak ada');
    }
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    if (mounted) {
      getNowTime().then((_) {
        getEmiten();
      });
      // TODO: REMOVE COMMENT ON PERIODIC TIMER
      timer = Timer.periodic(Duration(seconds: 30), (timer) {
        // getEmiten();
      });
      present = 4;
      // timer.cancel();
    }
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Padding(
              padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Pilih Bisnis',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      final result = Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return AllPenerbitList();
                      }));
                      result.then((_) {
                        getNowTime().then((_) {
                          getEmiten();
                        });
                      });
                    },
                    child: Text(
                      'Lihat Semua',
                      style: TextStyle(color: Color(0xFF218196), fontSize: 12),
                    ),
                  )
                ],
              )),
          loading || emiten == null
              ? Container(
                  height: 400,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    itemCount: 4,
                    itemBuilder: (_, i) {
                      return _stillLoading();
                    },
                  ),
                )
              : Container(
                  height: 415,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    itemCount: emiten.length < 6 ? emiten.length : 6,
                    itemBuilder: (_, i) {
                      return _penerbit(emiten[i]);
                    },
                  ),
                )
        ]));
  }

  Widget _stillLoading() {
    return Container(
        height: 400,
        padding: EdgeInsets.all(4),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        width: 150,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget _penerbit(Emiten emiten) {
    var isFinish = true;
    // var data = (emiten.terjual / emiten.supply) / 1;
    var data = (emiten.terjual / emiten.supply) / 1;
    String trademark = emiten.trademark;
    if (emiten.trademark.length > 40) {
      trademark = emiten.trademark.substring(0, 40);
    }
    if (DateTime.parse(emiten.beginPeriod == null ? now : emiten.beginPeriod)
            .difference(now)
            .inSeconds <=
        0) {
      isFinish = true;
    } else {
      isFinish = false;
    }
    return GestureDetector(
        onTap: () {
          final result =
              Navigator.push(context, MaterialPageRoute(builder: (context) {
            return EmitenUI(
              uuid: emiten.uuid,
            );
          }));
          result.then((_) {
            getNowTime().then((_) {
              getEmiten();
            });
          });
        },
        child: Container(
            height: 415,
            width: 250,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(width: 1, color: Color(0xFFE6EAFA)),
              boxShadow: [
                new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //image
                Container(
                    height: 199,
                    child: Stack(
                      children: <Widget>[
                        SantaraCachedImage(
                          height: 200,
                          width: 250,
                          // placeholder: 'assets/Preload.jpeg',
                          image: '${emiten.pictures[0].picture}',
                          fit: BoxFit.cover,
                        ),
                        isFinish
                            ? Container()
                            : Positioned(
                                top: 10,
                                right: 10,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(40),
                                      color: Color(0xFF324E80)),
                                  child: Center(
                                      child: Text("Segera dimulai",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12))),
                                ),
                              )
                      ],
                    )),

                //content
                Container(
                  height: 180,
                  margin: EdgeInsets.fromLTRB(8, 0, 8, 4),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            emiten.category,
                            style: TextStyle(
                              color: Color(0xFF292F8D),
                              fontSize: 10,
                            ),
                            overflow: TextOverflow.visible,
                          ),
                          Text(
                            emiten.trademark == null || emiten.trademark == ""
                                ? "-"
                                : '$trademark',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                          Text(
                            emiten.companyName,
                            style: TextStyle(fontSize: 12),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 4),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                        fontFamily: 'Nunito', fontSize: 11.5),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Mulai dari ',
                                          style:
                                              TextStyle(color: Colors.black)),
                                      TextSpan(
                                          text:
                                              'Rp. ${rupiah.format(emiten.startFrom)}',
                                          style: TextStyle(
                                              color: Color(0xFF0E7E4A)))
                                    ],
                                  ),
                                ),
                              ),
                              isFinish
                                  ? Container()
                                  : Icon(
                                      Icons.timer,
                                      color: Color(0xFFBF2D30),
                                      size: 12,
                                    ),
                              Container(width: 4),
                              isFinish
                                  ? Container()
                                  : Countdown(
                                      duration: Duration(
                                          seconds:
                                              DateTime.parse(emiten.beginPeriod)
                                                  .difference(now)
                                                  .inSeconds),
                                      builder: (BuildContext context,
                                          Duration remaining) {
                                        if (remaining.inSeconds <= 0 ||
                                            emiten.terjual >= emiten.supply) {
                                          return Text('0 Hari',
                                              style: TextStyle(
                                                  color: Color(0xFFBF2D30),
                                                  fontSize: 10));
                                        } else if (remaining.inDays > 0) {
                                          return Text(
                                              "${remaining.inDays} Hari",
                                              style: TextStyle(
                                                  color: Color(0xFFBF2D30),
                                                  fontSize: 10));
                                        } else {
                                          String hour =
                                              (remaining.inHours).toString();
                                          String second =
                                              (remaining.inSeconds % 60)
                                                  .toString();
                                          String minute =
                                              (remaining.inMinutes % 60)
                                                  .toString();
                                          if (second.length == 1) {
                                            second = "0" + second;
                                          }
                                          if (minute.length == 1) {
                                            minute = "0" + minute;
                                          }
                                          if (hour.length == 1) {
                                            hour = "0" + hour;
                                          }
                                          return Text('$hour:$minute:$second',
                                              style: TextStyle(
                                                  color: Color(0xFFBF2D30),
                                                  fontSize: 10));
                                        }
                                      },
                                    )
                            ],
                          )),
                      Container(height: 1, color: Color(0xFFF0F0F0)),
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2),
                            child: LinearPercentIndicator(
                              alignment: MainAxisAlignment.center,
                              padding: EdgeInsets.symmetric(vertical: 0),
                              animation: true,
                              animationDuration: 2000,
                              width: 210,
                              lineHeight: 18,
                              percent: data > 1
                                  ? 1
                                  : num.parse(data.toStringAsFixed(1)),
                              linearStrokeCap: LinearStrokeCap.roundAll,
                              progressColor: Color(0xFF0E7E4A),
                              center: Text(
                                '${(data * 100) < 100.00 ? (data * 100).toStringAsFixed(2) : 100.00} %',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Wrap(
                              children: <Widget>[
                                Text("Sisa waktu : ",
                                    style: TextStyle(
                                        color: Color(0xFF858585),
                                        fontSize: 10)),
                                now == null
                                    ? Text('0 Hari',
                                        style: TextStyle(
                                            color: Color(0xFF858585),
                                            fontSize: 10))
                                    : DateTime.parse(emiten.beginPeriod)
                                                .difference(now)
                                                .inSeconds >
                                            0
                                        ? Text('(Segera Dimulai)',
                                            style: TextStyle(
                                                color: Color(0xFF858585),
                                                fontSize: 10))
                                        : Countdown(
                                            duration: Duration(
                                                seconds: DateTime.parse(
                                                        emiten.endPeriod)
                                                    .difference(now)
                                                    .inSeconds),
                                            builder: (BuildContext context,
                                                Duration remaining) {
                                              if (remaining.inSeconds <= 0 ||
                                                  emiten.terjual >=
                                                      emiten.supply) {
                                                return Text('0 Hari',
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xFF858585),
                                                        fontSize: 10));
                                              } else if (remaining.inDays > 0) {
                                                return Text(
                                                    "${remaining.inDays} Hari",
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xFF858585),
                                                        fontSize: 10));
                                              } else {
                                                String hour =
                                                    (remaining.inHours)
                                                        .toString();
                                                String second =
                                                    (remaining.inSeconds % 60)
                                                        .toString();
                                                String minute =
                                                    (remaining.inMinutes % 60)
                                                        .toString();
                                                if (second.length == 1) {
                                                  second = "0" + second;
                                                }
                                                if (minute.length == 1) {
                                                  minute = "0" + minute;
                                                }
                                                if (hour.length == 1) {
                                                  hour = "0" + hour;
                                                }
                                                return Text(
                                                    '$hour:$minute:$second',
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xFF858585),
                                                        fontSize: 10));
                                              }
                                            },
                                          ),
                                Text(
                                    emiten.totalInvestor == null
                                        ? ""
                                        : " - ${emiten.totalInvestor} Investor",
                                    style: TextStyle(
                                        color: Color(0xFF858585),
                                        fontSize: 10)),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              "Total Pendanaan\nRp ${rupiah.format(emiten.supply * emiten.price)}",
                              style: TextStyle(
                                  fontSize: 11, fontWeight: FontWeight.bold)),
                          Text("Periode Dividen\n${emiten.period}",
                              style: TextStyle(
                                  fontSize: 11, fontWeight: FontWeight.bold)),
                        ],
                      )
                    ],
                  ),
                )
              ],
            )));
  }
}
