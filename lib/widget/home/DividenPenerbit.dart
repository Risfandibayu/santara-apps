import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/models/EmitenDividen.dart';

class DividenPenerbit extends StatefulWidget {
  final List<EmitenDividen> emitenDividen;
  DividenPenerbit({this.emitenDividen});
  @override
  _DividenPenerbitState createState() => _DividenPenerbitState();
}

class _DividenPenerbitState extends State<DividenPenerbit> {
  final rupiah = new NumberFormat("#,##0");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Log Aktifitas Penerbit", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(16),
        itemCount: widget.emitenDividen.length,
        itemBuilder: (_, i) {
          return Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                        child: Text(widget.emitenDividen[i].title,
                            style: TextStyle(fontSize: 14))),
                    Container(width: 20),
                    Flexible(
                        child: Text(widget.emitenDividen[i].dividenDate,
                            style: TextStyle(fontSize: 12),
                            textAlign: TextAlign.right))
                  ],
                ),
                Container(height: 2),
                Text("Rp ${rupiah.format(widget.emitenDividen[i].amount)}",
                    style: TextStyle(fontWeight: FontWeight.bold))
              ],
            ),
          );
        },
      ),
    );
  }
}
