import 'dart:async';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:santaraapp/models/comingsoonmodel.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:santaraapp/widget/home/AllComingsoonList.dart';
import 'package:santaraapp/widget/home/blocs/ComingSoon.list.bloc.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBottomSheet.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

import 'blocs/pralisting.list.bloc.dart';

var totalentitas;

class ComingSoon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Row(
              children: <Widget>[
                SizedBox(height: 20),
                Expanded(
                  child: Text(
                    'Coming Soon',
                    style: TextStyle(
                        fontFamily: 'Inter',
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Color(ColorRev.mainwhite)),
                  ),
                ),
                GestureDetector(
                  key: Key('viewAllBusinessBtn'),
                  onTap: () async {
                    await Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return AllComngsoonList();
                    }));
                  },
                  child: Text(
                    'Lihat Semua',
                    style: TextStyle(
                        fontFamily: 'Inter',
                        color: Color(ColorRev.mainblue),
                        fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          BlocProvider<ComingSoonListBloc>(
            create: (_) => ComingSoonListBloc(ComingSoonListUninitialized())
              ..add(ComingSoonListEvent()),
            child: PenerbitContent(),
          ),
        ],
      ),
    );
  }
}

class PenerbitContent extends StatefulWidget {
  @override
  _PenerbitContentState createState() => _PenerbitContentState();
}

class _PenerbitContentState extends State<PenerbitContent> {
  // ignore: close_sinks
  ComingSoonListBloc bloc;
  final rupiah = new NumberFormat("#,###");
  Timer timer;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ComingSoonListBloc>(context);
    // TODO: REMOVE COMMENT ON PERIODIC TIMER
    timer = Timer.periodic(Duration(seconds: 30), (Timer t) async {
      // bloc.add(ComingSoonListEvent());
    });
  }

  Widget _loadingBuilder() {
    return Container(
        height: 400,
        padding: EdgeInsets.all(4),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        width: 150,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Color(ColorRev.maingrey),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget _penerbit(ComingSoonModel emiten, DateTime now) {
    var isFinish = true;
    // var data = (emiten.terjual / emiten.supply) / 1;
    // var data = (emiten. / emiten.supply) / 1;
    // String trademark = emiten.trademark;
    // if (emiten.trademark.length > 40) {
    //   trademark = emiten.trademark.substring(0, 40);
    // }
    // if (DateTime.parse(emiten.beginPeriod == null ? now : emiten.beginPeriod)
    //         .difference(now)
    //         .inSeconds <=
    //     0) {
    //   isFinish = true;
    // } else {
    //   isFinish = false;
    // }

    var foto = emiten.pictures;
    var splitag = foto.split(",");

    return GestureDetector(
      // key: Key(emiten.trademark),
      onTap: () async {
        // await Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) {
        //       return EmitenUI(
        //         uuid: emiten.uuid,
        //       );
        //     },
        //   ),
        // );

        // await Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => PralistingDetailPage(
        //       status: 2,
        //       uuid: emiten.uuid,
        //       position: null,
        //     ),
        //   ),
        // );
        // if (result != null) {
        //   bloc.add(ComingSoonListEvent());
        // } else {
        //   SantaraBottomSheet.show(
        //     context,
        //     emiten.token != null
        //         ? "Maaf tidak dapat mengakses halaman ini, akun anda belum terverifikasi."
        //         : "Maaf, silakan login terlebih dahulu.",
        //   );
        // }
      },
      child: Container(
        height: 415,
        width: 250,
        padding: EdgeInsets.only(bottom: 8),
        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        decoration: BoxDecoration(
          color: Color(ColorRev.maingrey),
          borderRadius: BorderRadius.circular(10),
          // border: Border.all(width: 1, color: Color(0xFFE6EAFA)),
          // boxShadow: [
          //   new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
          // ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: 199,
                child: Stack(
                  children: <Widget>[
                    SantaraCachedImage(
                      height: 200,
                      width: 250,
                      image:
                          'https://storage.googleapis.com/santara-bucket-prod/pralisting/emitens_pictures/' +
                              '${splitag[0]}',
                      fit: BoxFit.cover,
                    ),
                    isFinish
                        ? Container()
                        : Positioned(
                            top: 10,
                            right: 10,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(40),
                                  color: Color(0xFF324E80)),
                              child: Center(
                                  child: Text("Segera dimulai",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12))),
                            ),
                          )
                  ],
                )),

            //content
            Container(
              height: 180,
              margin: EdgeInsets.fromLTRB(8, 0, 8, 4),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Container(
                        decoration: BoxDecoration(
                            color: Color(0xff7F1D1D),
                            border: Border.all(
                              color: Color(0xff7F1D1D),
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Text(
                            "${emiten.ownerName}",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Inter',
                              fontSize: 10,
                            ),
                            overflow: TextOverflow.visible,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          Text(
                            emiten.trademark == null || emiten.trademark == ""
                                ? "-"
                                : emiten.companyName,
                            style: TextStyle(
                                fontFamily: 'Inter',
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Color(ColorRev.mainwhite)),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                          SizedBox(width: 5),
                          Image.asset('assets/santara/check-verified.png',
                              scale: 4.4)
                        ],
                      ),
                      Text(
                        "${emiten.companyName}",
                        style: TextStyle(
                            fontFamily: 'Inter',
                            fontSize: 12,
                            color: Color(ColorRev.mainwhite)),
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Row(
                        children: [
                          Icon(Icons.person_outline, color: Colors.white),
                          Text(
                            " ${emiten.vote}",
                            style: TextStyle(
                                fontFamily: 'Inter',
                                fontSize: 12,
                                color: Color(ColorRev.mainwhite)),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      ),
                      SizedBox(width: 20),
                      Row(
                        children: [
                          Icon(Icons.favorite_border, color: Colors.white),
                          Text(
                            " ${emiten.likes}",
                            style: TextStyle(
                                fontFamily: 'Inter',
                                fontSize: 12,
                                color: Color(ColorRev.mainwhite)),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      ),
                      SizedBox(width: 20),
                      Row(
                        children: [
                          Icon(Icons.message, color: Colors.white),
                          Text(
                            " ${emiten.cmt}",
                            style: TextStyle(
                                fontFamily: 'Inter',
                                fontSize: 12,
                                color: Color(ColorRev.mainwhite)),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(height: 1, color: Color(0xFFF0F0F0)),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        border: Border.all(
                          color: Color(0xFFF0F0F0),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 5),
                      child: Center(
                        child: Text(
                          "Dukung Bisnis",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Inter',
                            fontSize: 14,
                          ),
                          overflow: TextOverflow.visible,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    bloc.close();
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ComingSoonListBloc, ComingSoonListState>(
        // key: Key('listBisnisKey'),
        builder: (context, state) {
      if (state is ComingSoonListUninitialized) {
        return Container(
          height: 420,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 12),
            itemCount: 4,
            itemBuilder: (_, i) {
              return _loadingBuilder();
            },
          ),
        );
      } else if (state is ComingSoonListLoaded) {
        ComingSoonListLoaded data = state;
        totalentitas = data.emiten.length;

        return Container(
          height: 415,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 12),
            itemCount: 5,
            itemBuilder: (_, i) {
              return _penerbit(data.emiten[i], data.now);
            },
          ),
        );
      } else if (state is ComingSoonListError) {
        return Center(
          child: IconButton(
            icon: Icon(Icons.replay),
            onPressed: () => bloc.add(
              ComingSoonListEvent(),
            ),
          ),
        );
      } else if (state is ComingSoonListEmpty) {
        return Center(
          child: Text("Belum ada data bisnis!"),
        );
      } else {
        return Center(
          child: Text(
            "Unknown state",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        );
      }
    });
  }
}
