import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/KycStatusHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/listing/dashboard/PengajuanBisnisUI.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/listing/BusinessTile.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraBottomSheet.dart';

class PraListingListUI extends StatefulWidget {
  @override
  _PraListingListUIState createState() => _PraListingListUIState();
}

class _PraListingListUIState extends State<PraListingListUI> {
  // Implementasi provider di pralisting

  List<DataPralisting> data = List<DataPralisting>();
  KycStatusHelper kycStatusHelper = KycStatusHelper();
  final randNumb = Random();
  bool isLoaded = false;
  final storage = new FlutterSecureStorage();
  var token;
  var messageError = "";
  bool _hasKyc = false;
  // generating dummy data
  Future getDataEmitenPralisting() async {
    data.clear();
    final response = await http.get(
        '$apiLocal/emitens/pre-listing/paginate?category=&sort=&search&limit=1&offset=3',
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      setState(() {
        var pralisting = pralistingPaginationFromJson(response.body);
        data = pralisting.data;
        isLoaded = true;
      });
    } else if (response.statusCode == 404) {
      setState(() {
        messageError =
            "Belum ada pendaftar pra-listing sekarang. Mari daftarkan bisnismu!";
        isLoaded = true;
      });
    } else {
      setState(() {
        if (response.body.contains("message")) {
          var res = jsonDecode(response.body);
          messageError = res["message"];
        }
        isLoaded = true;
      });
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
  }

  // jika user belom login
  Future getPralistingList() async {
    try {
      data.clear();
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Request Peek Prelisting Success");
          break;
        }
        // jagain
        if (i > 5) {
          break;
        }
        // print(">> Requesting peek prelisting...");
        final response = await http.get('$apiLocal/emitens/peek-pre-listing',
            headers: {"Authorization": "Bearer $token"});
        if (response.statusCode == 200) {
          final res = json.decode(response.body);
          List<DataPralisting> datas = List<DataPralisting>();
          res.forEach((val) {
            var dummy = DataPralisting.fromJson(val);
            datas.add(dummy);
          });
          setState(() {
            data = datas;
            isLoaded = true;
          });
          _done = true;
        } else if (response.statusCode == 404) {
          setState(() {
            messageError =
                "Belum ada pendaftar pra-listing sekarang. Mari daftarkan bisnismu!";
            isLoaded = true;
          });
          _done = true;
        } else {
          setState(() {
            if (response.body.contains("message")) {
              var res = jsonDecode(response.body);
              messageError = res["message"];
            }
            isLoaded = true;
          });
        }
        i++;
      }
    } catch (e) {
      setState(() {
        messageError =
            "Belum ada pendaftar pra-listing sekarang. Mari daftarkan bisnismu!";
        isLoaded = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((_) {
      if (token != null) {
        kycStatusHelper.check().then((value) {
          // print(">> KYC RESULT : $value");
          if (value) {
            setState(() {
              _hasKyc = true;
            });
            getDataEmitenPralisting();
          } else {
            getPralistingList();
          }
        });
      } else {
        getPralistingList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // print(">> Screen size : ");
    // print(SizeConfig.safeBlockHorizontal);
    // if (Helper.check) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 40,
        ),
        ListTile(
          title: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "UKM Corona Relief",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.safeBlockHorizontal * 4.3),
              ),
              _hasKyc
                  ? InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PengajuanBisnisUI()));
                      },
                      child: Container(
                        height: 35,
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "Lihat Semua",
                            style: TextStyle(
                                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                color: Color(0xff218196)),
                          ),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
          subtitle: Text(
            "Berbagai UKM berkualitas siap untuk diajukan",
            style: TextStyle(color: Colors.black),
          ),
        ),
        // data == null || data.length == 0
        messageError.length > 0
            ? Center(
                child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
                child: Text("$messageError", textAlign: TextAlign.center),
              ))
            : Container(
                // height: 200,
                margin: EdgeInsets.only(top: 2),
                child: isLoaded
                    ? ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length < 3 ? data.length : 3,
                        itemBuilder: (context, index) {
                          return BusinessTile2(
                            onTap: () async {
                              if (_hasKyc) {
                                final result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PralistingDetailPage(
                                      status: 2,
                                      uuid: data[index].uuid,
                                      position: null,
                                    ),
                                  ),
                                );
                                if (result != null) {
                                  getDataEmitenPralisting();
                                }
                              } else {
                                SantaraBottomSheet.show(
                                  context,
                                  token != null
                                      ? "Maaf tidak dapat mengakses halaman ini, akun anda belum terverifikasi."
                                      : "Maaf, silakan login terlebih dahulu.",
                                );
                              }
                            },
                            onDelete: null,
                            data: data[index],
                            isPengajuan: false,
                            showPosition: false,
                            status: 1,
                            showEngagement: true,
                          );
                        })
                    : ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: 1,
                        itemBuilder: (context, index) {
                          return BusinessTileLoader();
                        }))
        // Container(
        //     margin: EdgeInsets.only(top: 2),
        //     // color: Colors.indigo,
        //     // height: MediaQuery.of(context).size.height * .45,
        //     width: double.infinity,
        //     child: isLoaded
        //         ? GridView.builder(
        //             physics: NeverScrollableScrollPhysics(),
        //             shrinkWrap: true,
        //             padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0),
        //             itemCount: dummies.length,
        //             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //                 childAspectRatio: SizeConfig.screenWidth /
        //                     (SizeConfig.screenHeight -
        //                         SizeConfig.safeBlockHorizontal * 20),
        //                 crossAxisCount: 2),
        //             itemBuilder: (context, index) {
        //               return GridContentV2(data: dummies[index]);
        //               // return Container(
        //               //   height: MediaQuery.of(context).size.height * .1,
        //               //   child: Text(dummies[index].tagName),
        //               // );
        //             })
        //         : CircularProgressIndicator()),
      ],
    );
    // } else {
    //   return Container();
    // }
  }
}
