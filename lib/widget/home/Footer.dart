import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/facebook');
                  },
                  child: Image.asset('facebook.png', height: 30, width: 30,),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/twitter');
                  },
                  child: Image.asset('twitter.png', height: 30, width: 30,),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/instagram');
                  },
                  child: Image.asset('instagram.png', height: 30, width: 30,),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/youtube');
                  },
                  child: Image.asset('youtube.png', height: 30, width: 30,),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, bottom: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Bantuan', style: TextStyle(fontSize: 17 ,color: Colors.white, fontWeight: FontWeight.bold),),
                    Container(
                      width: 70,
                      child: Divider(color: Colors.red,),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: GestureDetector(
                        child: Text('Berita', style: TextStyle(fontSize: 15, color: Colors.white)),
                        onTap: (){
                          Navigator.pushNamed(context, '/berita');
                        },
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: GestureDetector(
                        child: Text('Tentang Kami', style: TextStyle(fontSize: 15, color: Colors.white)),
                        onTap: (){
                          Navigator.pushNamed(context, '/tentang');
                        },
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: GestureDetector(
                        child: Text('Petunjuk', style: TextStyle(fontSize: 15, color: Colors.white)),
                        onTap: (){
                          Navigator.pushNamed(context, '/petunjuk');
                        },
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: GestureDetector(
                        child: Text('Syarat dan Ketentuan', style: TextStyle(fontSize: 15, color: Colors.white)),
                        onTap: (){
                          Navigator.pushNamed(context, '/syarat-ketentuan');
                        },
                      )
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Kontak Kami', style: TextStyle(fontSize: 17 ,color: Colors.white, fontWeight: FontWeight.bold),),
                    Container(
                      width: 70,
                      child: Divider(color: Colors.red,),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.phone, color: Colors.white, size: 20,),
                          Text('+62 274 2822744', style: TextStyle(fontSize: 15 ,color: Colors.white))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(Icons.mail, color: Colors.white, size: 20,),
                          Text('official@santara.co.id', style: TextStyle(fontSize: 15 ,color: Colors.white))
                        ],
                      ),
                    ),
                    Text('Santara Yogyakarta', style: TextStyle(fontSize: 15 ,color: Colors.white, fontWeight: FontWeight.bold)),
                    Container(
                      width: 200,
                      child: Text('No 35, Jalan Patukan, Ambarketawang, Gamping Lor, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55294', style: TextStyle(fontSize: 10 ,color: Colors.white)),
                    ),
                  ],
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('© 2018 Santara. All rights reserved. Crafted with Love by "Pejuang Santara"', style: TextStyle( fontSize: 9.0, color: Colors.white),
              )
            ],
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text('Semua aktivitas permodalan melibatkan risiko termasuk kemungkinan hilangnya modal awal. SANTARA tidak menjamin bahwa keuntungan yang diharapkan akan terwujudkan. Dengan memodali proyek permodalan di SANTARA, pemodal mengerti dan menerima semua risiko yang terlibat di proyek terpilih. Untuk keterangan lebih lanjut tentang risiko proyek permodalan SANTARA,', textAlign: TextAlign.center, style: TextStyle(fontSize: 8),)
              ],
            ),
          )
        ]
      ),
    );
  }
}