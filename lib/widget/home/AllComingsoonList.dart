import 'dart:async';

import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/models/BidangUsaha.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/models/EmitenComingSoon.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class AllComngsoonList extends StatefulWidget {
  @override
  _AllComngsoonListState createState() => _AllComngsoonListState();
}

class _AllComngsoonListState extends State<AllComngsoonList> {
  var searchKey = TextEditingController();
  int present = 6;
  List<EmitenComingSoon> emiten;
  List<ProjectValue> projectValue;
  ProjectValue vProject;
  final rupiah = new NumberFormat("#,###");
  DateTime now;
  Timer timer;
  String sort = "";
  String min = "";
  String max = "";
  String keySearch = "";
  bool isSearch = false;
  bool isLoading = true;
  List<BidangUsaha> bidangUsaha = [BidangUsaha(category: "Semua")];
  BidangUsaha bu;
  final apiService = ApiService();

  List<Map> sortBy = [
    {"id": "0", "sort": "Urutkan", "value": ""},
    {"id": "1", "sort": "Terbaru", "value": "true"},
    {"id": "2", "sort": "Terlama", "value": "false"},
    {"id": "2", "sort": "Saham Terpenuhi", "value": "terpenuhi"},
    {"id": "2", "sort": "Belum Terpenuhi", "value": "belum terpenuhi"},
  ];
  List<DropdownMenuItem<String>> dropDownSortBy;
  List<DropdownMenuItem<String>> getChoiceSortBy() {
    List<DropdownMenuItem<String>> items = new List();
    sortBy.map((value) {
      items.add(DropdownMenuItem(
        value: value['value'],
        child: Text(value['sort'], overflow: TextOverflow.ellipsis),
      ));
    }).toList();
    return items;
  }

  List<DropdownMenuItem<ProjectValue>> dropDownPrice;
  List<DropdownMenuItem<ProjectValue>> getChoicePrice2() {
    List<DropdownMenuItem<ProjectValue>> items = new List();
    projectValue.map((value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Text(value.name),
      ));
    }).toList();
    return items;
  }

  Future getBidangUsaha() async {
    final http.Response response = await http.get('$apiLocal/categories/');
    if (response.statusCode == 200) {
      setState(() {
        var data = bidangUsahaFromJson(response.body);
        bidangUsaha.addAll(data);
        dropDownCategory = getChoiceCategory();
        bu = dropDownCategory[0].value;
      });
    } else if (response.statusCode == 401) {
      NavigatorHelper.pushToExpiredSession(context);
    }
  }

  List<DropdownMenuItem<BidangUsaha>> dropDownCategory;
  List<DropdownMenuItem<BidangUsaha>> getChoiceCategory() {
    List<DropdownMenuItem<BidangUsaha>> items = new List();
    bidangUsaha.map((value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Text(value.category),
      ));
    }).toList();
    return items;
  }

  Future getEmiten() async {
    var url =
        '$apiLocal/emitens/coming-soon?type=saham&projectValue1=${vProject.value1}&projectValue2=${vProject.value2}&category=${bu.category == 'Semua' ? '' : bu.category}&search=$keySearch&sort=$sort';

    final http.Response response = await http.get(url);

    print("isi url " + url);

    if (response.statusCode == 200) {
      setState(() {
        emiten = emitenComingSoonFromJson(response.body);

        isLoading = false;
      });
    } else if (response.statusCode == 401) {
      NavigatorHelper.pushToExpiredSession(context);
    } else {
      setState(() {
        emiten = [];
        isLoading = false;
      });
    }
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    FirebaseAnalytics()
        .logEvent(name: 'app_view_business_list', parameters: null);
    List<Map> jsonList = price2;
    projectValue = jsonList
        .map((s) => ProjectValue(
            name: s['name'], value1: s['value1'], value2: s["value2"]))
        .toList();
    dropDownPrice = getChoicePrice2();
    vProject = dropDownPrice[0].value;
    dropDownSortBy = getChoiceSortBy();
    getBidangUsaha().then((_) {
      getNowTime().then((_) {
        getEmiten();
      });
    });
    // TODO: REMOVE COMMENT ON PERIODIC TIMER
    timer = Timer.periodic(Duration(seconds: 30), (timer) {
      // getEmiten();
    });
    present = 6;
    if (mounted) {
      timer.cancel();
    }
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(ColorRev.mainBlack),
      appBar: isSearch
          ? AppBar(
              shape: Border(
                  bottom:
                      BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
              titleSpacing: 0,
              automaticallyImplyLeading: false,
              title: Padding(
                padding: const EdgeInsets.only(right: 8),
                child: TextField(
                  controller: searchKey,
                  onSubmitted: (value) {
                    setState(() {
                      isLoading = true;
                      keySearch = value;
                      getEmiten();
                    });
                  },
                  decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      hintText: "Contoh : Ayam",
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(40.0),
                          ),
                          borderSide: new BorderSide(color: Colors.grey)),
                      suffixIcon: IconButton(
                          padding: EdgeInsets.only(right: 16),
                          color: Color(ColorRev.mainBlack),
                          icon: Icon(Icons.search),
                          onPressed: () {
                            setState(() {
                              isLoading = true;
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              keySearch = searchKey.text;
                              getEmiten();
                            });
                          })),
                ),
              ),
              backgroundColor: Color(ColorRev.mainBlack),
              leading: IconButton(
                icon: Icon(Icons.close),
                color: Colors.white,
                onPressed: () {
                  setState(() {
                    if (isSearch) {
                      isSearch = false;
                    } else {
                      isSearch = true;
                    }
                  });
                },
              ),
            )
          : AppBar(
              shape: Border(
                  bottom:
                      BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
              title: Text("Coming Soon",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600)),
              leading: IconButton(
                onPressed: Navigator.of(context).pop,
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
              centerTitle: true,
              backgroundColor: Color(ColorRev.mainBlack),
              iconTheme: IconThemeData(color: Colors.black),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.search, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      if (isSearch) {
                        isSearch = false;
                      } else {
                        isSearch = true;
                      }
                    });
                  },
                )
              ],
            ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(height: 12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: SantaraAppBetaTesting(),
            ),
            Row(
              children: <Widget>[
                Flexible(child: _sortby()),
                Flexible(child: _kategori())
              ],
            ),
            _valueProject(),
            isLoading
                ? Column(
                    children: <Widget>[
                      _stillLoading(),
                      _stillLoading(),
                      _stillLoading()
                    ],
                  )
                : emiten == null || emiten.length == 0
                    ? Container(
                        height: MediaQuery.of(context).size.height - 300,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height / 3,
                                child: Image.asset(
                                    "assets/icon/search_not_found.png")),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16, 12, 16, 4),
                              child: Text(
                                "Tidak ada hasil yang ditemukan",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                    color: Colors.white),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                              child: Text(
                                "Coba kata kunci lain atau hapus filter penelusuran",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                    color: Colors.white),
                              ),
                            ),
                          ],
                        ))
                    : Container(
                        child: ListView(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            children: <Widget>[
                            Container(
                              child: GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio:
                                      MediaQuery.of(context).size.width /
                                          (MediaQuery.of(context).size.height *
                                              1.2),
                                ),
                                physics: ClampingScrollPhysics(),
                                shrinkWrap: true,
                                // padding: EdgeInsets.all(8),
                                itemCount: present >= emiten.length
                                    ? emiten.length
                                    : present,
                                itemBuilder: (_, i) {
                                  print(emiten);
                                  return _penerbit(emiten[i]);
                                },
                              ),
                            ),
                            emiten.length <= 6
                                ? Container()
                                : present >= emiten.length
                                    ? Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 12),
                                        child: FlatButton(
                                            onPressed: () {
                                              setState(() {
                                                present = 6;
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.keyboard_arrow_up,
                                                    color: Color(0xFF218196)),
                                                Container(width: 12),
                                                Text("Lihat lebih sedikit",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFF218196),
                                                        fontWeight:
                                                            FontWeight.w600)),
                                              ],
                                            )),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 12),
                                        child: FlatButton(
                                            onPressed: () {
                                              setState(() {
                                                present = present + 6;
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.keyboard_arrow_down,
                                                    color: Color(0xFF218196)),
                                                Container(width: 12),
                                                Text("Lihat Selengkapnya",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFF218196),
                                                        fontWeight:
                                                            FontWeight.w600)),
                                              ],
                                            )),
                                      )
                          ]))
          ],
        ),
      ),
    );
  }

  Widget _penerbit(EmitenComingSoon emiten) {
    var isFinish = true;
    var foto = emiten.pictures;
    var splitag = foto.split(",");

    print("isi emiten \n" + emiten.toString());

    var height = MediaQuery.of(context).size.width +
        (MediaQuery.of(context).size.width * 1 / 4);
    return GestureDetector(
        onTap: () async {
          FirebaseAnalytics()
              .logEvent(name: 'app_select_business_card', parameters: null);
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PralistingDetailPage(
                status: 2,
                uuid: emiten.uuid,
                // uuid: 18.toString(),
                position: null,
              ),
            ),
          );

          result.then((_) {
            getNowTime().then((_) {
              getEmiten();
            });
          });
        },
        child: Container(
            height: height,
            // width: 250,
            padding: EdgeInsets.only(bottom: 8),
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: Color(ColorRev.maingrey),
              borderRadius: BorderRadius.circular(10),

              // boxShadow: [
              //   new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
              // ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //image
                Container(
                    height: height / 2 - 20,
                    child: Stack(
                      children: <Widget>[
                        SantaraCachedImage(
                          height: height / 2,
                          width: MediaQuery.of(context).size.width,
                          image:
                              'https://storage.googleapis.com/santara-bucket-prod/pralisting/emitens_pictures/' +
                                      '${splitag[0]}' ??
                                  "",
                          fit: BoxFit.cover,
                        ),
                        isFinish
                            ? Container()
                            : Positioned(
                                top: 10,
                                right: 10,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(40),
                                      color: Color(0xFF324E80)),
                                  child: Center(
                                      child: Text("Segera dimulai",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14))),
                                ),
                              )
                      ],
                    )),

                //content
                Container(
                  height: height / 2 - 20,
                  // margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                emiten.companyName ?? "",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Inter',
                                  color: Colors.white,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.visible,
                              ),
                              Image.asset('assets/santara/check-verified.png',
                                  scale: 5.5)
                            ],
                          ),
                          Text(
                            emiten.companyName ?? "",
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'Inter',
                              color: Colors.white,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      ),
                      Row(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Row(
                            children: [
                              Icon(Icons.person_outline, color: Colors.white),
                              Text(
                                emiten.vote.toString() ?? "",
                                style: TextStyle(
                                    fontFamily: 'Inter',
                                    fontSize: 12,
                                    color: Color(ColorRev.mainwhite)),
                                maxLines: 2,
                                overflow: TextOverflow.visible,
                              ),
                            ],
                          ),
                          SizedBox(width: 10),
                          Row(
                            children: [
                              Icon(Icons.favorite_border, color: Colors.white),
                              Text(
                                emiten.likes.toString() ?? "",
                                style: TextStyle(
                                    fontFamily: 'Inter',
                                    fontSize: 12,
                                    color: Color(ColorRev.mainwhite)),
                                maxLines: 1,
                                overflow: TextOverflow.visible,
                              ),
                            ],
                          ),
                          SizedBox(width: 10),
                          Row(
                            children: [
                              Icon(Icons.message, color: Colors.white),
                              Text(
                                emiten.cmt.toString() ?? "",
                                style: TextStyle(
                                    fontFamily: 'Inter',
                                    fontSize: 12,
                                    color: Color(ColorRev.mainwhite)),
                                maxLines: 1,
                                overflow: TextOverflow.visible,
                              ),
                            ],
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        height: 1,
                        color: Color(0xFFF0F0F0),
                        width: MediaQuery.of(context).size.width * 0.7,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        width: MediaQuery.of(context).size.width * 0.7,
                        height: MediaQuery.of(context).size.height * 0.04,
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(
                              color: Color(0xFFF0F0F0),
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Center(
                            child: Text(
                              "Dukung Bisnis",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Inter',
                                fontSize: 14,
                              ),
                              overflow: TextOverflow.visible,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )));
  }

  Widget _kategori() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: EdgeInsets.fromLTRB(8, 20, 16, 10),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFFD0D0D0)),
            borderRadius: BorderRadius.circular(4)),
        child: Row(children: <Widget>[
          Icon(
            Icons.filter_list,
            color: Color(ColorRev.maingrey),
          ),
          Container(width: 10),
          Flexible(
            child: DropdownButtonHideUnderline(
              key: Key('dropdownCategoryBtn'),
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 6),
                child: DropdownButton(
                  value: bu,
                  isExpanded: true,
                  items: dropDownCategory,
                  onChanged: (selected) {
                    setState(() {
                      bu = selected;
                      isLoading = true;
                      getEmiten();
                    });
                  },
                ),
              ),
            ),
          )
        ]));
  }

  Widget _sortby() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: EdgeInsets.fromLTRB(16, 20, 8, 10),
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.white,
            border: Border.all(color: Color(0xFFD0D0D0))),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.sort,
              color: Color(ColorRev.maingrey),
            ),
            Container(width: 10),
            Flexible(
              child: DropdownButtonHideUnderline(
                key: Key('dropdownSortBtn'),
                child: DropdownButton(
                  isExpanded: true,
                  value: sort,
                  items: dropDownSortBy,
                  onChanged: (selected) {
                    setState(() {
                      sort = selected;
                      isLoading = true;
                      getEmiten();
                    });
                  },
                ),
              ),
            )
          ],
        ));
  }

  Widget _valueProject() {
    return Container(
        margin: EdgeInsets.fromLTRB(8, 0, 8, 20),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text("Nilai Proyek",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Color(ColorRev.mainwhite))),
            ),
            Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                  color: Color(ColorRev.mainwhite),
                  border: Border.all(color: Color(0xFFD0D0D0)),
                  borderRadius: BorderRadius.circular(4)),
              child: DropdownButtonHideUnderline(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 6),
                  child: DropdownButton(
                    value: vProject,
                    isExpanded: true,
                    items: dropDownPrice,
                    onChanged: (selected) {
                      setState(() {
                        vProject = selected;
                        isLoading = true;
                        getEmiten();
                      });
                    },
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _stillLoading() {
    var height = MediaQuery.of(context).size.width +
        (MediaQuery.of(context).size.width * 1 / 4);
    return Container(
        height: height,
        padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: height / 2,
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class ProjectValue {
  final String name;
  final String value1;
  final String value2;

  ProjectValue({
    this.name,
    this.value1,
    this.value2,
  });
}

List<Map> price2 = [
  {"name": "Semua", "value1": "", "value2": ""},
  {"name": "< 50 Juta", "value1": "0", "value2": "50000000"},
  {"name": "> 50 - 100 Juta", "value1": "50000001", "value2": "100000000"},
  {"name": "> 100 - 250 Juta", "value1": "100000001", "value2": "250000000"},
  {"name": "> 250 - 500 Juta", "value1": "250000001", "value2": "500000000"},
  {
    "name": "> 500 juta - 1 Milyar",
    "value1": "500000001",
    "value2": "1000000000"
  },
  {
    "name": "> 1 Milyar - 5 Milyar",
    "value1": "1000000001",
    "value2": "5000000000"
  },
  {
    "name": "> 5 Milyar - 10 Milyar",
    "value1": "5000000001",
    "value2": "100000000000"
  },
];
