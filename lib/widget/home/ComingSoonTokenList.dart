import 'package:flutter/material.dart';

class ComingSoonTokenList extends StatefulWidget {
  @override
  _ComingSoonTokenListState createState() => _ComingSoonTokenListState();
}

class _ComingSoonTokenListState extends State<ComingSoonTokenList> {
  List image = [
    AssetImage('assets/ComingsoonToken/Inspira.png'),
  ];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (Helper.check) {
    //   Helper.restartTimer(context, true);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 40.0, left: 5.0, right: 5.0),
        height: MediaQuery.of(context).size.height / 2,
        child: Card(
          child: Container(
            child: Column(
              children: <Widget>[
              Container(
                padding: EdgeInsets.only(top:10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Coming Soon Token', textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold)),
                  ],
                )
              ),
              Container(
                padding: EdgeInsets.only(top: 10),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                height: MediaQuery.of(context).size.height / 2.7,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal, 
                  itemCount: image.length,
                  itemBuilder: (BuildContext context, int index){
                    return Container(
                      width: 220.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: image[index],
                          fit: BoxFit.contain
                        )
                      )
                    );
                  },
                )
              )
            ]
          ),
        )
      )
    );
  }
}