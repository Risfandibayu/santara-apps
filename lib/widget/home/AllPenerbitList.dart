import 'dart:async';

import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/models/BidangUsaha.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class AllPenerbitList extends StatefulWidget {
  @override
  _AllPenerbitListState createState() => _AllPenerbitListState();
}

class _AllPenerbitListState extends State<AllPenerbitList> {
  var searchKey = TextEditingController();
  int present = 6;
  List<Emiten> emiten;
  List<ProjectValue> projectValue;
  ProjectValue vProject;
  final rupiah = new NumberFormat("#,###");
  DateTime now;
  Timer timer;
  String sort = "";
  String min = "";
  String max = "";
  String keySearch = "";
  bool isSearch = false;
  bool isLoading = true;
  List<BidangUsaha> bidangUsaha = [BidangUsaha(category: "Semua")];
  BidangUsaha bu;
  final apiService = ApiService();

  List<Map> sortBy = [
    {"id": "0", "sort": "Urutkan", "value": ""},
    {"id": "1", "sort": "Terbaru", "value": "true"},
    {"id": "2", "sort": "Terlama", "value": "false"},
    {"id": "2", "sort": "Terpenuhi", "value": "terpenuhi"},
    {"id": "2", "sort": "Belum Terpenuhi", "value": "belum terpenuhi"},
  ];
  List<DropdownMenuItem<String>> dropDownSortBy;
  List<DropdownMenuItem<String>> getChoiceSortBy() {
    List<DropdownMenuItem<String>> items = new List();
    sortBy.map((value) {
      items.add(DropdownMenuItem(
        value: value['value'],
        child: Text(value['sort'], overflow: TextOverflow.ellipsis),
      ));
    }).toList();
    return items;
  }

  List<DropdownMenuItem<ProjectValue>> dropDownPrice;
  List<DropdownMenuItem<ProjectValue>> getChoicePrice2() {
    List<DropdownMenuItem<ProjectValue>> items = new List();
    projectValue.map((value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Text(value.name),
      ));
    }).toList();
    return items;
  }

  Future getBidangUsaha() async {
    final http.Response response = await http.get('$apiLocal/categories/');
    if (response.statusCode == 200) {
      setState(() {
        var data = bidangUsahaFromJson(response.body);
        bidangUsaha.addAll(data);
        dropDownCategory = getChoiceCategory();
        bu = dropDownCategory[0].value;
      });
    } else if (response.statusCode == 401) {
      NavigatorHelper.pushToExpiredSession(context);
    }
  }

  List<DropdownMenuItem<BidangUsaha>> dropDownCategory;
  List<DropdownMenuItem<BidangUsaha>> getChoiceCategory() {
    List<DropdownMenuItem<BidangUsaha>> items = new List();
    bidangUsaha.map((value) {
      items.add(DropdownMenuItem(
        value: value,
        child: Text(value.category),
      ));
    }).toList();
    return items;
  }

  Future getEmiten() async {
    var url =
        '$apiLocal/emitens/emiten?type=saham&projectValue1=${vProject.value1}&projectValue2=${vProject.value2}&category=${bu.category == 'Semua' ? '' : bu.category}&search=$keySearch&sort=$sort';
    final http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      setState(() {
        emiten = emitenFromJson(response.body);
        isLoading = false;
      });
    } else if (response.statusCode == 401) {
      NavigatorHelper.pushToExpiredSession(context);
    } else {
      setState(() {
        emiten = [];
        isLoading = false;
      });
    }
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    FirebaseAnalytics()
        .logEvent(name: 'app_view_business_list', parameters: null);
    List<Map> jsonList = price2;
    projectValue = jsonList
        .map((s) => ProjectValue(
            name: s['name'], value1: s['value1'], value2: s["value2"]))
        .toList();
    dropDownPrice = getChoicePrice2();
    vProject = dropDownPrice[0].value;
    dropDownSortBy = getChoiceSortBy();
    getBidangUsaha().then((_) {
      getNowTime().then((_) {
        getEmiten();
      });
    });
    // TODO: REMOVE COMMENT ON PERIODIC TIMER
    timer = Timer.periodic(Duration(seconds: 30), (timer) {
      // getEmiten();
    });
    present = 6;
    if (mounted) {
      timer.cancel();
    }
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(ColorRev.mainBlack),
      appBar: isSearch
          ? AppBar(
              shape: Border(
                  bottom:
                      BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
              titleSpacing: 0,
              automaticallyImplyLeading: false,
              title: Padding(
                padding: const EdgeInsets.only(right: 8),
                child: TextField(
                  controller: searchKey,
                  onSubmitted: (value) {
                    setState(() {
                      isLoading = true;
                      keySearch = value;
                      getEmiten();
                    });
                  },
                  decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      hintText: "Contoh : Ayam",
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(40.0),
                          ),
                          borderSide: new BorderSide(color: Colors.grey)),
                      suffixIcon: IconButton(
                          padding: EdgeInsets.only(right: 16),
                          color: Color(ColorRev.mainBlack),
                          icon: Icon(Icons.search),
                          onPressed: () {
                            setState(() {
                              isLoading = true;
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              keySearch = searchKey.text;
                              getEmiten();
                            });
                          })),
                ),
              ),
              backgroundColor: Color(ColorRev.mainBlack),
              leading: IconButton(
                icon: Icon(Icons.close),
                color: Colors.white,
                onPressed: () {
                  setState(() {
                    if (isSearch) {
                      isSearch = false;
                    } else {
                      isSearch = true;
                    }
                  });
                },
              ),
            )
          : AppBar(
              shape: Border(
                  bottom:
                      BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
              title: Text("Now Playing",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600)),
              leading: IconButton(
                onPressed: Navigator.of(context).pop,
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
              centerTitle: true,
              backgroundColor: Color(ColorRev.mainBlack),
              iconTheme: IconThemeData(color: Colors.black),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.search, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      if (isSearch) {
                        isSearch = false;
                      } else {
                        isSearch = true;
                      }
                    });
                  },
                )
              ],
            ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(height: 12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: SantaraAppBetaTesting(),
            ),
            Row(
              children: <Widget>[
                Flexible(child: _sortby()),
                Flexible(child: _kategori())
              ],
            ),
            _valueProject(),
            isLoading
                ? Column(
                    children: <Widget>[
                      _stillLoading(),
                      _stillLoading(),
                      _stillLoading()
                    ],
                  )
                : emiten == null || emiten.length == 0
                    ? Container(
                        height: MediaQuery.of(context).size.height - 300,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                height: MediaQuery.of(context).size.height / 3,
                                child: Image.asset(
                                    "assets/icon/search_not_found.png")),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16, 12, 16, 4),
                              child: Text(
                                "Tidak ada hasil yang ditemukan",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                    color: Colors.white),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                              child: Text(
                                "Coba kata kunci lain atau hapus filter penelusuran",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                    color: Colors.white),
                              ),
                            ),
                          ],
                        ))
                    : Container(
                        child: ListView(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            children: <Widget>[
                            Container(
                              child: GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: MediaQuery.of(context)
                                          .size
                                          .width /
                                      (MediaQuery.of(context).size.height * 1),
                                ),
                                physics: ClampingScrollPhysics(),
                                shrinkWrap: true,
                                padding: EdgeInsets.all(8),
                                itemCount: present >= emiten.length
                                    ? emiten.length
                                    : present,
                                itemBuilder: (_, i) {
                                  return _penerbit(emiten[i]);
                                },
                              ),
                            ),
                            emiten.length <= 6
                                ? Container()
                                : present >= emiten.length
                                    ? Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 12),
                                        child: FlatButton(
                                            onPressed: () {
                                              setState(() {
                                                present = 6;
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.keyboard_arrow_up,
                                                    color: Color(0xFF218196)),
                                                Container(width: 12),
                                                Text("Lihat lebih sedikit",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFF218196),
                                                        fontWeight:
                                                            FontWeight.w600)),
                                              ],
                                            )),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 12),
                                        child: FlatButton(
                                            onPressed: () {
                                              setState(() {
                                                present = present + 6;
                                              });
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(Icons.keyboard_arrow_down,
                                                    color: Color(0xFF218196)),
                                                Container(width: 12),
                                                Text("Lihat Selengkapnya",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFF218196),
                                                        fontWeight:
                                                            FontWeight.w600)),
                                              ],
                                            )),
                                      )
                          ]))
          ],
        ),
      ),
    );
  }

  Widget _penerbit(Emiten emiten) {
    var isFinish = true;
    var data = (emiten.terjual / emiten.supply) / 1;
    String trademark = emiten.trademark;
    if (emiten.trademark.length > 40) {
      trademark = emiten.trademark.substring(0, 40);
    }
    if (DateTime.parse(emiten.beginPeriod == null ? now : emiten.beginPeriod)
            .difference(now)
            .inSeconds <=
        0) {
      isFinish = true;
    } else {
      isFinish = false;
    }
    var height = MediaQuery.of(context).size.width +
        (MediaQuery.of(context).size.width * 1 / 4);
    return GestureDetector(
      onTap: () {
        FirebaseAnalytics()
            .logEvent(name: 'app_select_business_card', parameters: null);
        final result =
            Navigator.push(context, MaterialPageRoute(builder: (context) {
          return EmitenUI(
            uuid: emiten.uuid,
          );
        }));
        result.then((_) {
          getNowTime().then((_) {
            getEmiten();
          });
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        decoration: BoxDecoration(
          color: Color(0xff404040),
          borderRadius: BorderRadius.circular(10),

          // boxShadow: [
          //   new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 12)
          // ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //image
            Container(
                // height: height / 2 - 20,
                // height: 10,
                child: Stack(
              children: <Widget>[
                SantaraCachedImage(
                  height: height / 3,
                  width: MediaQuery.of(context).size.width,
                  image: '${emiten.pictures[0].picture}',
                  fit: BoxFit.cover,
                ),
                isFinish
                    ? Container()
                    : Positioned(
                        top: 10,
                        right: 10,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              color: Color(0xFF324E80)),
                          child: Center(
                              child: Text("Segera dimulai",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14))),
                        ),
                      )
              ],
            )),

            //content
            Container(
              // height: height / 2 - 20,
              margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(
                              color: Colors.red,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 5),
                        child: Text(
                          emiten.category,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Inter',
                              fontSize: 10),
                          overflow: TextOverflow.visible,
                        ),
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          Text(
                            emiten.trademark == null || emiten.trademark == ""
                                ? "-"
                                : '$trademark',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Inter',
                              color: Colors.white,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                          ),
                          SizedBox(width: 5),
                          Image.asset('assets/santara/check-verified.png',
                              scale: 4.4)
                        ],
                      ),
                      Text(
                        emiten.companyName,
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'Inter',
                          color: Colors.white,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(
                                    fontFamily: 'Inter', fontSize: 12),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Mulai dari ',
                                      style: TextStyle(color: Colors.white)),
                                  TextSpan(
                                      text:
                                          'Rp. ${rupiah.format(emiten.startFrom)}',
                                      style: TextStyle(
                                          color: Color(ColorRev.mainwhite),
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ),
                          isFinish
                              ? Container()
                              : Icon(
                                  Icons.timer,
                                  color: Color(0xFFBF2D30),
                                  size: 12,
                                ),
                          Container(width: 4),
                          isFinish
                              ? Container()
                              : Countdown(
                                  duration: Duration(
                                      seconds:
                                          DateTime.parse(emiten.beginPeriod)
                                              .difference(now)
                                              .inSeconds),
                                  builder: (BuildContext context,
                                      Duration remaining) {
                                    if (remaining.inSeconds <= 0 ||
                                        emiten.terjual >= emiten.supply) {
                                      return Text('0 Hari',
                                          style: TextStyle(
                                              color: Color(ColorRev.mainwhite),
                                              fontSize: 12));
                                    } else if (remaining.inDays > 0) {
                                      return Text("${remaining.inDays} Hari",
                                          style: TextStyle(
                                              color: Color(ColorRev.mainwhite),
                                              fontSize: 14));
                                    } else {
                                      String hour =
                                          (remaining.inHours).toString();
                                      String second =
                                          (remaining.inSeconds % 60).toString();
                                      String minute =
                                          (remaining.inMinutes % 60).toString();
                                      if (second.length == 1) {
                                        second = "0" + second;
                                      }
                                      if (minute.length == 1) {
                                        minute = "0" + minute;
                                      }
                                      if (hour.length == 1) {
                                        hour = "0" + hour;
                                      }
                                      return Text('$hour:$minute:$second',
                                          style: TextStyle(
                                              color: Color(ColorRev.mainwhite),
                                              fontSize: 14));
                                    }
                                  },
                                )
                        ],
                      )),
                  // Container(height: 1, color: Color(0xFFF0F0F0)),
                  SizedBox(height: 10),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2),
                        child: LinearPercentIndicator(
                          alignment: MainAxisAlignment.center,
                          padding: EdgeInsets.symmetric(vertical: 0),
                          animation: true,
                          animationDuration: 2000,
                          width: MediaQuery.of(context).size.width * 0.4,
                          lineHeight: 14,
                          percent:
                              data > 1 ? 1 : num.parse(data.toStringAsFixed(1)),
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          progressColor: Color(ColorRev.mainRed),
                          center: Text(
                            '${(data * 100) < 100.00 ? (data * 100).toStringAsFixed(2) : 100.00} %',
                            style: TextStyle(color: Colors.white, fontSize: 10),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Wrap(
                          children: <Widget>[
                            Text("Sisa waktu : ",
                                style: TextStyle(
                                    color: Color(ColorRev.mainwhite),
                                    fontSize: 12)),
                            now == null
                                ? Text('0 Hari',
                                    style: TextStyle(
                                        color: Color(ColorRev.mainwhite),
                                        fontSize: 12))
                                : DateTime.parse(emiten.beginPeriod)
                                            .difference(now)
                                            .inSeconds >
                                        0
                                    ? Text('(Segera Dimulai)',
                                        style: TextStyle(
                                            color: Color(ColorRev.mainwhite),
                                            fontSize: 12))
                                    : Countdown(
                                        duration: Duration(
                                            seconds:
                                                DateTime.parse(emiten.endPeriod)
                                                    .difference(now)
                                                    .inSeconds),
                                        builder: (BuildContext context,
                                            Duration remaining) {
                                          if (remaining.inSeconds <= 0 ||
                                              emiten.terjual >= emiten.supply) {
                                            return Text('0 Hari',
                                                style: TextStyle(
                                                    color: Color(
                                                        ColorRev.mainwhite),
                                                    fontSize: 12));
                                          } else if (remaining.inDays > 0) {
                                            return Text(
                                                "${remaining.inDays} Hari",
                                                style: TextStyle(
                                                    color: Color(
                                                        ColorRev.mainwhite),
                                                    fontSize: 12));
                                          } else {
                                            String hour =
                                                (remaining.inHours).toString();
                                            String second =
                                                (remaining.inSeconds % 60)
                                                    .toString();
                                            String minute =
                                                (remaining.inMinutes % 60)
                                                    .toString();
                                            if (second.length == 1) {
                                              second = "0" + second;
                                            }
                                            if (minute.length == 1) {
                                              minute = "0" + minute;
                                            }
                                            if (hour.length == 1) {
                                              hour = "0" + hour;
                                            }
                                            return Text('$hour:$minute:$second',
                                                style: TextStyle(
                                                    color: Color(
                                                        ColorRev.mainwhite),
                                                    fontSize: 12));
                                          }
                                        },
                                      ),
                            Text(
                                emiten.totalInvestor == null
                                    ? ""
                                    : " - ${emiten.totalInvestor} Investor",
                                style: TextStyle(
                                    color: Color(ColorRev.mainwhite),
                                    fontSize: 12)),
                          ],
                        ),
                      ),
                    ],
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.start,
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: <Widget>[
                  //     Text(
                  //         "Total Pendanaan\nRp ${rupiah.format(emiten.supply * emiten.price)}",
                  //         style: TextStyle(
                  //             fontSize: 13,
                  //             fontWeight: FontWeight.bold,
                  //             color: Color(ColorRev.mainwhite))),
                  //     Text("Periode Dividen\n${emiten.period}",
                  //         style: TextStyle(
                  //             fontSize: 13,
                  //             fontWeight: FontWeight.bold,
                  //             color: Color(ColorRev.mainwhite))),
                  //   ],
                  // )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _kategori() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: EdgeInsets.fromLTRB(8, 20, 16, 10),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFFD0D0D0)),
            borderRadius: BorderRadius.circular(4)),
        child: Row(children: <Widget>[
          Icon(
            Icons.filter_list,
            color: Color(ColorRev.maingrey),
          ),
          Container(width: 10),
          Flexible(
            child: DropdownButtonHideUnderline(
              key: Key('dropdownCategoryBtn'),
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 6),
                child: DropdownButton(
                  value: bu,
                  isExpanded: true,
                  items: dropDownCategory,
                  onChanged: (selected) {
                    setState(() {
                      bu = selected;
                      isLoading = true;
                      getEmiten();
                    });
                  },
                ),
              ),
            ),
          )
        ]));
  }

  Widget _sortby() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        margin: EdgeInsets.fromLTRB(16, 20, 8, 10),
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.white,
            border: Border.all(color: Color(0xFFD0D0D0))),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.sort,
              color: Color(ColorRev.maingrey),
            ),
            Container(width: 10),
            Flexible(
              child: DropdownButtonHideUnderline(
                key: Key('dropdownSortBtn'),
                child: DropdownButton(
                  isExpanded: true,
                  value: sort,
                  items: dropDownSortBy,
                  onChanged: (selected) {
                    setState(() {
                      sort = selected;
                      isLoading = true;
                      getEmiten();
                    });
                  },
                ),
              ),
            )
          ],
        ));
  }

  Widget _valueProject() {
    return Container(
        margin: EdgeInsets.fromLTRB(8, 0, 8, 20),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text("Nilai Proyek",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Color(ColorRev.mainwhite))),
            ),
            Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                  color: Color(ColorRev.mainwhite),
                  border: Border.all(color: Color(0xFFD0D0D0)),
                  borderRadius: BorderRadius.circular(4)),
              child: DropdownButtonHideUnderline(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 6),
                  child: DropdownButton(
                    value: vProject,
                    isExpanded: true,
                    items: dropDownPrice,
                    onChanged: (selected) {
                      setState(() {
                        vProject = selected;
                        isLoading = true;
                        getEmiten();
                      });
                    },
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _stillLoading() {
    var height = MediaQuery.of(context).size.width +
        (MediaQuery.of(context).size.width * 1 / 4);
    return Container(
        height: height,
        padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.white,
                child: Container(
                  height: height / 2,
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(
                    'arfa.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(height: 16),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 175,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        width: MediaQuery.of(context).size.width / 2,
                        height: MediaQuery.of(context).size.height / 45,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class ProjectValue {
  final String name;
  final String value1;
  final String value2;

  ProjectValue({
    this.name,
    this.value1,
    this.value2,
  });
}

List<Map> price2 = [
  {"name": "Semua", "value1": "", "value2": ""},
  {"name": "< 50 Juta", "value1": "0", "value2": "50000000"},
  {"name": "> 50 - 100 Juta", "value1": "50000001", "value2": "100000000"},
  {"name": "> 100 - 250 Juta", "value1": "100000001", "value2": "250000000"},
  {"name": "> 250 - 500 Juta", "value1": "250000001", "value2": "500000000"},
  {
    "name": "> 500 juta - 1 Milyar",
    "value1": "500000001",
    "value2": "1000000000"
  },
  {
    "name": "> 1 Milyar - 5 Milyar",
    "value1": "1000000001",
    "value2": "5000000000"
  },
  {
    "name": "> 5 Milyar - 10 Milyar",
    "value1": "5000000001",
    "value2": "100000000000"
  },
];
