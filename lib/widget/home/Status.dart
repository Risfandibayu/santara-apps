import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/api.dart';

class Status extends StatefulWidget {
  @override
  _StatusState createState() => _StatusState();
}

class _StatusState extends State<Status> {
  final angka = new NumberFormat("#,##0");
  var data;

  Future getStatus() async {
    final http.Response response = await http.get('$apiLocal/information');
    if (response.statusCode == 200) {
      setState(() {
        data = jsonDecode(response.body);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getStatus();
  }

  @override
  Widget build(BuildContext context) {
    if (data == null) {
      return Container();
    } else {
      return Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [new BoxShadow(color: Color(0xFFE6EAFA), blurRadius: 8)],
          ),
          child: Column(
            children: <Widget>[
              Row(children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Text(data['record_soldout'],
                          style: TextStyle(
                              fontSize: 24,
                              color: Color(0xFF9E1E1E),
                              fontWeight: FontWeight.w600)),
                      Text(
                        'Rekor Sold',
                        style: TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                ),
                Container(width: 1, height: 80, color: Color(0xFFD6D6D6)),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Text(convertNumb(data['emiten'] / 1),
                          style: TextStyle(
                              fontSize: 24,
                              color: Color(0xFF9E1E1E),
                              fontWeight: FontWeight.w600)),
                      Text('Bisnis Terdaftar', style: TextStyle(fontSize: 12))
                    ],
                  ),
                ),
              ]),
              Container(
                height: 1,
                color: Color(0xFFD6D6D6),
                margin: EdgeInsets.symmetric(horizontal: 20),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(convertNumb(data['emiten_registered'] / 1),
                              style: TextStyle(
                                  fontSize: 24,
                                  color: Color(0xFF9E1E1E),
                                  fontWeight: FontWeight.w600)),
                          Text('Bisnis Mendaftar',
                              style: TextStyle(fontSize: 12))
                        ],
                      ),
                    ),
                    Container(width: 1, height: 80, color: Color(0xFFD6D6D6)),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(convertNumb(data['user'] / 1),
                              style: TextStyle(
                                  fontSize: 24,
                                  color: Color(0xFF9E1E1E),
                                  fontWeight: FontWeight.w600)),
                          Text('Member Pemodal', style: TextStyle(fontSize: 12))
                        ],
                      ),
                    ),
                  ]),
            ],
          ));
    }
  }

  String convertNumb(double numb) {
    if (numb / 1000000000000 >= 1) {
      return (numb / 1000000000000).toStringAsFixed(0) + "+ T";
    } else {
      if (numb / 1000000000 >= 1) {
        return (numb / 1000000000).toStringAsFixed(0) + "+ M";
      } else {
        if (numb / 1000000 >= 1) {
          return (numb / 1000000).toStringAsFixed(0) + "+ Jt";
        } else {
          if (numb / 1000 >= 1) {
            return (numb / 1000).toStringAsFixed(0) + "+ Rb";
          } else {
            return numb.toStringAsFixed(0);
          }
        }
      }
    }
  }
}
