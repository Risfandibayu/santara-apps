import 'dart:convert';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shimmer/shimmer.dart';

import '../../features/deposit/main/presentation/pages/main_deposit_page.dart';
import '../../helpers/UserAgent.dart';
import '../../models/Deposit.dart';
import '../../models/PaymentChannel.dart';
import '../../models/TokenHistory.dart';
import '../../models/User.dart';
import '../../pages/Home.dart';
import '../../services/wallet.dart';
import '../../utils/api.dart';
import '../../utils/logger.dart';
import '../widget/components/listing/SantaraNotification.dart';
import '../widget/components/main/SantaraAppBetaTesting.dart';
import '../widget/components/main/SantaraCachedImage.dart';

class PraCheckout extends StatefulWidget {
  final String name;
  final String emitenUuid;
  final String image;
  final double count;
  final int price;
  final int amount;
  PraCheckout(
      {this.name,
      this.emitenUuid,
      this.image,
      this.count,
      this.price,
      this.amount});
  @override
  _PraCheckoutState createState() => _PraCheckoutState();
}

class _PraCheckoutState extends State<PraCheckout> {
  final storage = new FlutterSecureStorage();
  final rupiah = new NumberFormat("#,##0");
  final angka = new NumberFormat("#,##0");
  String token;
  int isVerified;
  int tryLooping = 0;
  int isTrying = 0;
  String chanel = "";
  String bank = "";
  bool loading = false;
  double saldoRupiah;
  int tryLooping1 = 0;
  List<TokenHistory> tokenHistory = [];
  List<Deposit> deposit = [];
  // List<Bank> bankList = [];
  PaymentChannel paymentChannel;

  void _handleRadioValueChange(String value) {
    setState(() {
      chanel = value;
      switch (value) {
        case "VA":
          break;
        case "BANKTRANSFER":
          break;
        case "OVO":
          setState(() => bank = null);
          _next();
          break;
        case "WALLET":
          setState(() => bank = null);
          break;
        case "CC":
          setState(() => bank = null);
          _next();
          break;
        case "ALFAMART":
          setState(() => bank = null);
          break;
        case "INDOMARET":
          setState(() => bank = null);
          break;
      }
    });
  }

  void _next() async {
    try {
      if (isVerified == 0) {
        showModalUnverified('Akun anda belum diverifikasi oleh admin');
      } else if (isVerified == 1) {
        // Future.delayed(Duration(milliseconds: 800), () {
        // });
        await buyToken();
      } else {
        showModalUnverified('Terjadi kesalahan, harap hubungi CS Santara');
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  Future getPaymentChannel() async {
    setState(() => loading = true);
    final http.Response response = await http.get('$apiLocal/payment-channels');
    if (response.statusCode == 200) {
      setState(() {
        loading = false;
        paymentChannel = paymentChannelFromJson(response.body);
      });
    } else {
      setState(() => loading = false);
      paymentChannel = null;
    }
  }

  Future buyToken() async {
    try {
      setState(() {
        loading = true;
      });
      final body = {
        "emiten_uuid": widget.emitenUuid,
        "amount": widget.amount.toString(),
        "channel": chanel,
        "bank": bank == null ? "" : bank,
      };
      logger.i(body);
      final headers = await UserAgent.headers();
      final http.Response response = await http.post(
          '$apiLocal/transactions/buy-token',
          headers: headers,
          body: body);
      if (response.statusCode == 200) {
        FirebaseAnalytics().logEvent(
          name: 'app_begin_checkout',
          parameters: null,
        );
        setState(() {
          loading = false;
        });
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Checkout Saham Telah Berhasil'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home(index: 1)),
                          (Route<dynamic> route) => false);
                      // Navigator.of(context).pushNamed('/token');
                    },
                  ),
                ],
              );
            });
      } else {
        setState(() {
          loading = false;
        });
        final msg = jsonDecode(response.body);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text(msg['message']),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      if (msg["message"] ==
                          "Anda telah mencapai batas limit pembelian saham atau Anda belum menyelesaikan pembayaran sebelumnya") {
                        Navigator.of(context).pop();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (Route<dynamic> route) => false);
                        Navigator.of(context).pushNamed('/token');
                      } else if (msg["message"] ==
                          "Anda telah mencapai batas limit pembelian saham") {
                        Navigator.of(context).pop();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 1)),
                            (Route<dynamic> route) => false);
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                  )
                ],
              );
            });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        loading = false;
      });
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
    saldoRupiah = await getIdrDeposit();
  }

  Future getUserInfo() async {
    final datas = userFromJson(await storage.read(key: 'user'));
    final uuid = datas.uuid;
    final http.Response response = await http.get(
        '$apiLocal/users/by-uuid/$uuid',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      final data = userFromJson(response.body);
      setState(() {
        isVerified = data.trader.isVerified;
        tryLooping = 0;
      });
    } else {
      if (tryLooping < 3) {
        setState(() => tryLooping = tryLooping + 1);
        getLocalStorage();
      }
    }
  }

  Future getAvalableTransaction() async {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/transactions/checkout', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        tokenHistory = tokenHistoryFromJson(response.body);
        tryLooping1 = 0;
      });
    } else {
      if (tryLooping1 < 5) {
        setState(() {
          tryLooping1 = tryLooping1 + 1;
          getAvalableTransaction();
        });
      } else {
        setState(() {
          tokenHistory = [];
        });
      }
    }
  }

  Future getHistoryDeposit() async {
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/deposit/', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        deposit = depositFromJson(response.body);
        isTrying = 0;
      });
    } else {
      setState(() {
        if (isTrying < 3) {
          isTrying = isTrying + 1;
          getHistoryDeposit();
        } else {
          deposit = [];
        }
      });
    }
  }

  void showModalUnverified(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Pembelian gagal dilakukan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 50,
                      width: double.maxFinite,
                      color: Color(0xFFBF2D30),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getPaymentChannel();
    // getBank();
    getLocalStorage().then((_) {
      getHistoryDeposit();
      getUserInfo();
      getAvalableTransaction();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: loading,
      progressIndicator: CircularProgressIndicator(),
      opacity: 0.5,
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/santara/1.png',
            width: 110,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: SantaraAppBetaTesting(),
            ),
            _title(),
            Container(
              height: 4,
              color: Color(0xFFDADADA),
            ),
            _pilihMetodePembayaran()
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return Container(
      color: Color(0xFFF3F3F3),
      padding: EdgeInsets.all(16),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: SantaraCachedImage(
                height: 100,
                width: 100,
                image: widget.image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  widget.name,
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Container(height: 6),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Harga Saham",
                            style: TextStyle(fontSize: 13))),
                    Text("Rp ${rupiah.format(widget.price)}",
                        style: TextStyle(fontSize: 13))
                  ],
                ),
                // Container(height: 4),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Jumlah Investasi",
                            style: TextStyle(fontSize: 13))),
                    Text("${angka.format(widget.count)} Lembar",
                        style: TextStyle(fontSize: 13))
                  ],
                ),
                Container(
                  height: 1,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(vertical: 6),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text("Total",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16))),
                    Text("Rp ${rupiah.format(widget.amount)}",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Color(0xFFBF2D30)))
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _pilihMetodePembayaran() {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Text(
              "Pilih Metode Pembayaran",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            margin: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Color(0xFFC4C4C4)),
            ),
            child: Column(
              children: <Widget>[
                _itemMetode(
                    'methodWallet', "WALLET", false, "Saldo Wallet", ""),
                chanel == "WALLET" ? _wallet() : Container(),
                Container(height: 1, color: Colors.grey[200]),
                paymentChannel == null ||
                        paymentChannel.virtualAccounts.length == 0
                    ? Container()
                    : _itemMetode('methodVA', "VA", false,
                        "Bank Transfer (Virtual Account)", ""),
                chanel == "VA"
                    ? paymentChannel == null ||
                            paymentChannel.virtualAccounts.length == 0
                        ? Container()
                        : _va()
                    : Container(),
                Container(height: 1, color: Colors.grey[200]),
                // _itemMetode("VAS", false, "Bank Transfer (Virtual Account Syariah)", ""),
                // Container(height: 1, color: Colors.grey[200]),
                paymentChannel == null ||
                        paymentChannel.bankTransfer.length == 0
                    ? Container()
                    : _itemMetode('methodBankTransfer', "BANKTRANSFER", false,
                        "Bank Transfer (Konfirmasi Manual)", ""),
                chanel == "BANKTRANSFER"
                    ? paymentChannel == null ||
                            paymentChannel.bankTransfer.length == 0
                        ? Container()
                        : _manualTransfer()
                    : Container(),
                // Container(height: 1, color: Colors.grey[200]),
                // _itemMetode("OVO", true, "assets/icon/ovo.png",
                //     "Verifikasi otomatis, Gratis biaya transfer."),
                // Container(height: 1, color: Colors.grey[200]),
                // _itemMetode("CC", false, "Kartu Kredit", ""),
                // Container(height: 1, color: Colors.grey[200]),
                // _itemMetode("INDOMARET", true, "assets/icon/indomaret.png", ""),
                // chanel == "INDOMARET" ? _convienceStore() : Container(),
                // Container(height: 1, color: Colors.grey[200]),
                // _itemMetode("ALFAMART", true, "assets/icon/alfamart.png", ""),
                // chanel == "ALFAMART" ? _convienceStore() : Container(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemMetode(
      String key, String value, bool isImage, String name, String subName) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Radio(
                key: Key(key),
                groupValue: chanel,
                onChanged: _handleRadioValueChange,
                value: value,
              ),
              isImage
                  ? Image.asset(
                      name,
                      height: 45,
                      width: 120,
                    )
                  : Text(
                      name,
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                    ),
            ],
          ),
          subName == ""
              ? Container()
              : Padding(
                  padding:
                      const EdgeInsets.only(left: 16, bottom: 12, right: 20),
                  child: Text(
                    subName,
                    textAlign: TextAlign.justify,
                  ),
                )
        ],
      ),
    );
  }

  Widget _wallet() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Saldo Rupiah yang Anda miliki di akun Santara"),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            margin: EdgeInsets.symmetric(vertical: 8),
            decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
            child: saldoRupiah == null
                ? Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.black,
                    child: Text("Loading"))
                : Text(
                    "Rp ${rupiah.format(saldoRupiah)}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
          ),
          FlatButton(
            key: Key('pilihPaymentBtn'),
            onPressed: saldoRupiah == null || saldoRupiah < widget.amount
                ? null
                : () => _next(),
            color: Color(0xFF1BC47D),
            disabledColor: Colors.grey,
            child: Container(
              height: 40,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(4)),
              child: Center(
                  child: Text(
                      saldoRupiah == null || saldoRupiah < widget.amount
                          ? "Saldo Tidak Cukup"
                          : "Pilih",
                      style: TextStyle(color: Colors.white))),
            ),
          ),
          Container(height: 20),
        ],
      ),
    );
  }

  Widget _va() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'Nunito',
              ),
              children: <TextSpan>[
                TextSpan(
                  text:
                      'Transfer pembayaran melalui metode virtual account akan terverifikasi secara otomatis',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: ' (tidak perlu mengirim bukti pembayaran)\n'),
              ],
            ),
          ),
          Container(
            height: 60,
            width: double.maxFinite,
            child: SantaraNotification(
              margin: EdgeInsets.all(0),
              message: Text(
                "Transaksi akan dikenakan biaya admin sebesar Rp. 2.000 (Dua Ribu Rupiah)",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                ),
              ),
              backgroundColor: Color(0xffFEE0E0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Text("Pilih Rekening Virtual Account :"),
          ),
          Container(height: 1, color: Colors.grey[200]),
          paymentChannel == null || paymentChannel.virtualAccounts.length == 0
              ? Container(
                  height: 140,
                  child: Center(child: CircularProgressIndicator()))
              : ListView.separated(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: paymentChannel.virtualAccounts.length,
                  separatorBuilder: (context, i) {
                    return Container(height: 1, color: Colors.grey[200]);
                  },
                  itemBuilder: (context, i) {
                    return _listBank(paymentChannel.virtualAccounts[i].bank);
                  },
                ),
          // _listBank("assets/bank/bca.png", "Bank BCA", "BCA"),
          // Container(height: 1, color: Colors.grey[200]),
          // _listBank("assets/bank/bni.png", "Bank BNI", "BNI"),
          // Container(height: 1, color: Colors.grey[200]),
          // _listBank("assets/bank/mandiri.png", "Bank Mandiri", "MANDIRI"),
          // Container(height: 1, color: Colors.grey[200]),
          // _listBank("assets/bank/bri.png", "Bank BRI", "BRI"),
          // Container(height: 1, color: Colors.grey[200]),
          // _listBank("assets/bank/permatabank.png", "Bank Permata", "PERMATA"),
        ],
      ),
    );
  }

  Widget _manualTransfer() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Nunito',
                  fontSize: 11,
                ),
                children: <TextSpan>[
                  TextSpan(
                      text: "Deposit menggunakan metode Bank Transfer akan "),
                  TextSpan(
                    text: "diverifikasi",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(text: " oleh admin "),
                  TextSpan(
                    text: "Maks. 2 x 24 Jam",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(text: " (Hari Kerja)."),
                ],
              ),
            ),
          ),
          // FlatButton(
          //   onPressed: () => _next(),
          //   color: Color(0xFF1BC47D),
          //   child: Container(
          //     height: 40,
          //     decoration: BoxDecoration(borderRadius: BorderRadius.circular(4)),
          //     child: Center(
          //         child: Text("Pilih", style: TextStyle(color: Colors.white))),
          //   ),
          // ),
          // bankList.length == 0
          paymentChannel == null
              ? Container(
                  height: 140,
                  child: Center(child: CircularProgressIndicator()))
              : ListView.builder(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: paymentChannel.bankTransfer.length,
                  itemBuilder: (context, i) {
                    return _listBankManual(paymentChannel.bankTransfer[i]);
                  },
                ),
          Container(height: 20),
        ],
      ),
    );
  }

  Widget _listBankManual(BankTransfer bankData) {
    return GestureDetector(
      onTap: () {
        setState(() => bank = bankData.provider);
        _next();
      },
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 12),
          child: Column(
            children: <Widget>[
              Container(
                height: 1,
                color: Color(0xFFC4C4C4),
              ),
              Container(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                      width: 75,
                      height: 50,
                      margin:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                      child: Image.asset(
                          bankData.provider == "BCA"
                              ? 'assets/bank/bca.png'
                              : bankData.provider == "BRI"
                                  ? 'assets/bank/bri.png'
                                  : 'assets/bank/mandiri.png',
                          fit: BoxFit.fitWidth)),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            bankData.accountName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14),
                            overflow: TextOverflow.visible,
                          ),
                          Text(
                            bankData.accountNumber,
                            style: TextStyle(fontSize: 12),
                            overflow: TextOverflow.visible,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }

  Widget _listBank(String bankValue) {
    return InkWell(
      onTap: () {
        if (bankValue == "BCA") {
          bool isBCA = false;
          int _bca = 0;
          for (var i = 0; i < deposit.length; i++) {
            if (deposit[i].virtualAccount != null &&
                deposit[i].virtualAccount.merchantCode == "12090" &&
                deposit[i].status == 0 &&
                deposit[i].confirmationPhoto == null) {
              setState(() {
                isBCA = true;
                _bca = 1;
              });
            }
          }
          for (var i = 0; i < tokenHistory.length; i++) {
            if (tokenHistory[i].bank == "BCA") {
              setState(() {
                isBCA = true;
                _bca = 2;
              });
            }
          }
          if (isBCA) {
            showPopupBCA(_bca, bankValue);
          } else {
            setState(() => bank = bankValue);
            _next();
          }
        } else {
          setState(() => bank = bankValue);
          _next();
        }
      },
      child: Container(
        height: 75,
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 75,
              width: MediaQuery.of(context).size.width / 5,
              margin: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(bankValue.toUpperCase() == "BCA"
                          ? "assets/bank/bca.png"
                          : bankValue.toUpperCase() == "BNI"
                              ? "assets/bank/bni.png"
                              : bankValue.toUpperCase() == "MANDIRI"
                                  ? "assets/bank/mandiri.png"
                                  : bankValue.toUpperCase() == "BRI"
                                      ? "assets/bank/bri.png"
                                      : "assets/bank/permatabank.png"),
                      fit: BoxFit.fitWidth)),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 10,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      bankValue.toUpperCase() == "BCA"
                          ? "Bank BCA"
                          : bankValue.toUpperCase() == "BNI"
                              ? "Bank BNI"
                              : bankValue.toUpperCase() == "MANDIRI"
                                  ? "Bank Mandiri"
                                  : bankValue.toUpperCase() == "BRI"
                                      ? "Bank BRI"
                                      : "Bank Permata",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                          fontSize: 16),
                      overflow: TextOverflow.visible,
                    ),
                    Text(
                      'Virtual Account',
                      overflow: TextOverflow.visible,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void showPopupBCA(int isDep, String bankValue) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      child: Icon(Icons.close),
                      onTap: () => Navigator.pop(context)),
                ),
                Container(height: 20),
                Text(
                  "Anda masih memiliki pembayaran yang harus diselesaikan",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    "Anda harus menyelesaikan pembayaran melalui virtual account BCA di transaksi sebelumnya terlebih dahulu atau sistem virtual account BCA akan menghapus pembayaran sebelumnya.",
                    style: TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (isDep == 1) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(index: 4)),
                                (Route<dynamic> route) => false);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) =>
                                        MainDepositPage(initialTab: 1)));
                          } else if (isDep == 2) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(index: 1)),
                                (Route<dynamic> route) => false);
                          }
                        },
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(color: Color(0xFFC52D2F))),
                          child: Center(
                              child: Text("Lakukan Pembayaran",
                                  style: TextStyle(
                                      color: Color(0xFFC52D2F), fontSize: 13),
                                  textAlign: TextAlign.center)),
                        ),
                      ),
                    ),
                    Container(width: 10),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() => bank = bankValue);
                          _next();
                        },
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              color: Color(0xFFC52D2F),
                              borderRadius: BorderRadius.circular(4)),
                          child: Center(
                              child: Text("Lanjutkan",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 13),
                                  textAlign: TextAlign.center)),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ));
        });
  }

  // Widget _convienceStore() {
  //   return Container(
  //     margin: EdgeInsets.fromLTRB(25, 0, 25, 10),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: <Widget>[
  //         Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: <Widget>[
  //             Text("Total Bayar",
  //                 style: TextStyle(
  //                     color: Color(0xFF676767), fontWeight: FontWeight.w600)),
  //             Text("Rp ${rupiah.format(widget.amount)}",
  //                 style: TextStyle(
  //                     color: Color(0xFF9E1E1E),
  //                     fontWeight: FontWeight.w600,
  //                     fontSize: 16))
  //           ],
  //         ),
  //         FlatButton(
  //           color: Color(0xFF1BC47D),
  //           child: Text("Bayar"),
  //           textColor: Colors.white,
  //           onPressed: () {},
  //         )
  //       ],
  //     ),
  //   );
  // }
}
