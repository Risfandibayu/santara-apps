import 'dart:async';

import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class TokenList extends StatefulWidget {
  @override
  _TokenListState createState() => _TokenListState();
}

class _TokenListState extends State<TokenList> {
  int present = 4;
  List<Emiten> emiten;
  var loading = true;
  final rupiah = new NumberFormat("#,###");
  DateTime now;
  Timer timer;
  final apiService = ApiService();

  Future getEmiten() async {
    final http.Response response = await http.get('$apiLocal/emitens/');
    if (response.statusCode == 200) {
      setState(() {
        emiten = emitenFromJson(response.body);
        loading = false;
      });
    } else {
      setState(() => loading = false);
      // print('data tidak ada');
    }
  }

  Future getNowTime() async {
    try {
      var result = await apiService.getCurrentTime();
      setState(() {
        now = result;
        loading = false;
      });
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        now = DateTime.now();
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getNowTime().then((_) {
      getEmiten();
    });
    // TODO: REMOVE COMMENT ON PERIODIC TIMER
    timer = Timer.periodic(Duration(seconds: 30), (timer) {
      // getNowTime();
      // getEmiten();
    });
    present = 4;
    if (mounted) {
      timer.cancel();
    }
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
            child: Text(
              'Pilih Bisnis yang Bagus-Bagus, Sebelum Kehabisan',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              textAlign: TextAlign.center,
            ),
          ),
          //list token
          loading
              ? Container(
                  margin: EdgeInsets.only(top: 20),
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 0.55,
                        mainAxisSpacing: 1,
                        crossAxisSpacing: 1),
                    itemCount: 4,
                    shrinkWrap: true,
                    padding: const EdgeInsets.all(8),
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        clipBehavior: Clip.antiAlias,
                        elevation: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //image
                            Shimmer.fromColors(
                              baseColor: Colors.grey[300],
                              highlightColor: Colors.white,
                              child: Container(
                                child: Image.asset(
                                  'arfa.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            //content
                            Container(
                              margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.white,
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.height /
                                              6,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              45,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.white,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      width:
                                          MediaQuery.of(context).size.height /
                                              5,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              45,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.white,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      width:
                                          MediaQuery.of(context).size.height /
                                              5,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              45,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.white,
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      width:
                                          MediaQuery.of(context).size.height /
                                              5,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              45,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ));
                    },
                  ),
                )
              : emiten == null
                  ? Text('Koneksi Bermasalah Silahkan Periksa Kembali')
                  : Column(
                      children: <Widget>[
                        Container(
                            child: GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio:
                                      MediaQuery.of(context).size.width /
                                          MediaQuery.of(context).size.height,
                                  mainAxisSpacing: 1,
                                  crossAxisSpacing: 1),
                          itemCount: present >= emiten.length
                              ? emiten.length
                              : present,
                          shrinkWrap: true,
                          padding: const EdgeInsets.all(8),
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return _itemList(index);
                          },
                        )),
                        present >= emiten.length
                            ? Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
                                child: FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        present = 4;
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.keyboard_arrow_up,
                                            color: Color(0xFF218196)),
                                        Container(width: 12),
                                        Text("Lihat lebih sedikit",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Color(0xFF218196),
                                                fontWeight: FontWeight.w600)),
                                      ],
                                    )),
                              )
                            : Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
                                child: FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        present = present + 4;
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.keyboard_arrow_down,
                                            color: Color(0xFF218196)),
                                        Container(width: 12),
                                        Text("Lihat Selengkapnya",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Color(0xFF218196),
                                                fontWeight: FontWeight.w600)),
                                      ],
                                    )),
                              )
                      ],
                    )
        ],
      ),
    );
  }

  Widget _itemList(int index) {
    var isFinish = true;
    var data = (emiten[index].terjual / emiten[index].supply) * 100 / 100;
    String trademark = emiten[index].trademark;
    if (emiten[index].trademark.length > 40) {
      trademark = emiten[index].trademark.substring(0, 40);
    }
    if (DateTime.parse(emiten[index].beginPeriod == null
                ? now.toString()
                : emiten[index].beginPeriod)
            .difference(now)
            .inSeconds <=
        0) {
      isFinish = true;
    } else {
      isFinish = false;
    }
    return GestureDetector(
        onTap: () {
          final result =
              Navigator.push(context, MaterialPageRoute(builder: (context) {
            return EmitenUI(
              uuid: emiten[index].uuid,
            );
          }));
          result.then((_) {
            getNowTime();
            getEmiten();
          });
        },
        child: Container(
            child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          clipBehavior: Clip.antiAlias,
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              Container(
                  height: MediaQuery.of(context).size.height / 4.25,
                  child: Stack(
                    children: <Widget>[
                      SantaraCachedImage(
                        height: MediaQuery.of(context).size.height / 4.25,
                        width: double.maxFinite,
                        // placeholder: 'assets/Preload.jpeg',
                        image: '${emiten[index].pictures[0].picture}',
                        fit: BoxFit.cover,
                      ),
                      isFinish
                          ? Container()
                          : Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                height: 40,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                      Colors.black.withOpacity(0.4),
                                      Colors.black.withOpacity(0.3),
                                      Colors.black.withOpacity(0.0),
                                    ],
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topCenter)),
                                child: Center(
                                    child: Text("Segera dimulai",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold))),
                              ),
                            )
                    ],
                  )),
              //content
              Container(
                height: MediaQuery.of(context).size.height / 4.7,
                margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      emiten[index].trademark == null ||
                              emiten[index].trademark == ""
                          ? "-"
                          : '$trademark',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.visible,
                    ),
                    Text(
                      emiten[index].companyName == null ||
                              emiten[index].companyName == ""
                          ? "-"
                          : emiten[index].companyName,
                      style: TextStyle(fontSize: 14),
                      maxLines: 2,
                      overflow: TextOverflow.visible,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 2),
                          child: Text(
                            'Mulai dari Rp. ${rupiah.format(emiten[index].supply <= emiten[index].terjual ? 0 : emiten[index].startFrom)}',
                            style: TextStyle(fontSize: 11, color: Colors.grey),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2),
                          child: LinearPercentIndicator(
                            alignment: MainAxisAlignment.center,
                            padding: EdgeInsets.symmetric(vertical: 0),
                            animation: true,
                            animationDuration: 2000,
                            width: MediaQuery.of(context).size.width / 2 - 40,
                            lineHeight: 16,
                            percent: data > 1
                                ? 1
                                : num.parse(data.toStringAsFixed(1)),
                            linearStrokeCap: LinearStrokeCap.roundAll,
                            progressColor: Colors.green,
                            center: Text(
                              '${(data * 100) < 100.00 ? (data * 100).toStringAsFixed(2) : 100.00} %',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 11),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Text('Lama Permodalan',
                                style: TextStyle(fontSize: 11))),
                        Container(width: 8),
                        now == null
                            ? Text('0 Hari',
                                style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.height / 60,
                                    color: Color(0xFFBF2D30)))
                            : Countdown(
                                duration: Duration(
                                    seconds: DateTime.parse(
                                            emiten[index].endPeriod == null
                                                ? now.toString()
                                                : emiten[index].endPeriod)
                                        .difference(now)
                                        .inSeconds),
                                builder:
                                    (BuildContext context, Duration remaining) {
                                  if (remaining.inSeconds <= 0 ||
                                      emiten[index].terjual >=
                                          emiten[index].supply) {
                                    return Text('0 Hari',
                                        style: TextStyle(
                                            fontSize: 11,
                                            color: Color(0xFFBF2D30)));
                                  } else if (remaining.inDays > 0) {
                                    return Text("${remaining.inDays} Hari",
                                        style: TextStyle(
                                            fontSize: 11,
                                            color: Color(0xFFBF2D30)));
                                  } else {
                                    String hour =
                                        (remaining.inHours).toString();
                                    String second =
                                        (remaining.inSeconds % 60).toString();
                                    String minute =
                                        (remaining.inMinutes % 60).toString();
                                    if (second.length == 1) {
                                      second = "0" + second;
                                    }
                                    if (minute.length == 1) {
                                      minute = "0" + minute;
                                    }
                                    if (hour.length == 1) {
                                      hour = "0" + hour;
                                    }
                                    return Text('$hour:$minute:$second',
                                        style: TextStyle(
                                            fontSize: 11,
                                            color: Color(0xFFBF2D30)));
                                  }
                                },
                              ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        )));
  }
}
