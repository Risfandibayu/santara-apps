import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  String mail = "customer.support@santara.co.id";
  String maps =
      "https://www.google.com/maps/place/Santara/@-7.7987767,110.3276352,17z/data=!4m5!3m4!1s0x2e7af949d7abe11d:0x94ba5aae4b549072!8m2!3d-7.7988698!4d110.3275518";
  String wa =
      "https://api.whatsapp.com/send?phone=6281212227765&text=Halo, apakah ada informasi terbaru mengenai Santara?";

  Future getContact() async {
    final response =
        await http.get('$apiLocal/contacts'); //.timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      mail = data["email"];
      wa = data["whatsapp"];
      maps = data["maps"];
    }
  }

  Future launchWA() async {
    var url = wa;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Cannot launch $url';
    }
  }

  Future launchMap() async {
    var url = maps;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Cannot launch $url';
    }
  }

  Future launchEmail() async {
    var url = "mailto:" + mail;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Cannot launch $url';
    }
  }

  @override
  void initState() {
    super.initState();
    getContact();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 40),
            child: Center(
                child: Text(
              'Kontak kami',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            )),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                  onTap: launchMap,
                  child: Container(
                    height: MediaQuery.of(context).size.width / 6,
                    width: MediaQuery.of(context).size.width / 6,
                    child: Image.asset("assets/cp/lokasi.png"),
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: launchWA,
                  child: Container(
                    height: MediaQuery.of(context).size.width / 6,
                    width: MediaQuery.of(context).size.width / 6,
                    child: Image.asset("assets/cp/whatsapp.png"),
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: launchEmail,
                  child: Container(
                    height: MediaQuery.of(context).size.width / 6,
                    width: MediaQuery.of(context).size.width / 6,
                    child: Image.asset("assets/cp/email.png"),
                  ),
                ),
              ),
            ],
          ),
          Container(height: 50)
        ],
      ),
    );
  }
}
