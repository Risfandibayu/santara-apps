import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/pages/pre_screening_page.dart';
import 'package:shimmer/shimmer.dart';

import '../listing/registrasi/PreRegistrasiUI.dart';
import '../widget/components/main/SantaraBottomSheet.dart';
import 'blocs/button.bloc.dart';

class Button extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ButtonBloc>(
      create: (_) => ButtonBloc(ButtonUninitialized())..add(LoadButton()),
      child: ButtonContent(),
    );
  }
}

class ButtonContent extends StatefulWidget {
  @override
  _ButtonContentState createState() => _ButtonContentState();
}

class _ButtonContentState extends State<ButtonContent>
    with AutomaticKeepAliveClientMixin {
  ButtonBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ButtonBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // ignore: close_sinks
    final bloc = BlocProvider.of<ButtonBloc>(context);
    return Container(
      margin: EdgeInsets.fromLTRB(5, 0, 5, 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
            child: Text('Urun Dana Investasi Bisnis UKM',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Color(ColorRev.mainwhite))),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 40),
            child: Text(
              'Anda punya bisnis? Sedang cari pemodal? Daftarkan disini',
              style: TextStyle(fontSize: 12, color: Color(ColorRev.mainwhite)),
              textAlign: TextAlign.center,
            ),
          ),
          BlocBuilder<ButtonBloc, ButtonState>(builder: (context, state) {
            if (state is ButtonUninitialized) {
              return Container(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 30),
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      height: 30,
                      width: MediaQuery.of(context).size.width / 1.2,
                      child: Image.asset(
                        'arfa.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              );
            } else if (state is ButtonLoaded) {
              return Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.height * 0.06,
                child: FlatButton(
                  key: Key('daftarkanButton'),
                  color: Color(0xFFBF2D30),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  onPressed: () {
                    switch (state.verified) {
                      case ButtonStatus.unauthorized:
                        SantaraBottomSheet.show(
                          context,
                          "Maaf, silakan login terlebih dahulu.",
                        );
                        break;
                      case ButtonStatus.unverified:
                        SantaraBottomSheet.show(
                          context,
                          "Maaf, akun anda belum terverifikasi.",
                        );
                        break;
                      case ButtonStatus.verified:
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PreScreeningPage(),
                          ),
                        );
                        break;
                      default:
                    }
                  },
                  child: Text(
                    'Daftarkan Bisnis',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              );
            } else {
              return Container();
            }
          })
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
