import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/widget/widget/components/market/table_pasar_sekunder.dart'
    as table;

import '../../blocs/market/ara_arb_bloc.dart';
import '../../blocs/market/emiten_by_uuid_bloc.dart';
import '../../blocs/market/market_fee_bloc.dart';
import '../../blocs/market/market_fraction_bloc.dart';
import '../../blocs/market/max_invest_bloc.dart';
import '../../blocs/market/min_purchase_bloc.dart';
import '../../blocs/market/pending_transaction_bloc.dart';
import '../../blocs/market/portofolio_by_emiten_code_bloc.dart';
import '../../blocs/market/profile_ballance_bloc.dart';
import '../../blocs/market/submit_transaction.dart';
import '../../blocs/user_bloc.dart';
import '../../core/usecases/unauthorize_usecase.dart';
import '../../helpers/RupiahFormatter.dart';
import '../../helpers/market/popup_helper_market.dart';
import '../../models/User.dart';
import '../../models/market/ara_arb_model.dart';
import '../../models/market/emiten_by_uuid.dart';
import '../../models/market/market_fraction.dart';
import '../../models/market/portofolio_by_emiten_code.dart';
import '../../models/market/profile_ballance.dart';
import '../../services/market_trails/market_trails_services.dart';
import '../widget/components/main/SantaraAppBetaTesting.dart';
import '../widget/components/market/info_price.dart';
import 'submit_trx_success.dart';

enum TransactionType { jual, beli }

class JualBeliView extends StatefulWidget {
  final TransactionType type;
  final int pending;
  final String uuid;
  final String codeEmiten;
  final String trxUuid;
  final int amount;
  final int price;
  final double totalSaham;
  JualBeliView(
      {@required this.type,
      this.pending,
      @required this.uuid,
      this.codeEmiten,
      this.trxUuid,
      this.amount,
      this.price,
      this.totalSaham});
  @override
  _JualBeliViewState createState() => _JualBeliViewState();
}

class _JualBeliViewState extends State<JualBeliView> {
  EmitenByUuidBloc _emitenByUuidBloc;
  AraArbBloc _araArbBloc;
  SubmitTransactionBloc _submitTransactionBloc;
  ProfileBallanceBloc _profileBallanceBloc;
  PendingTransactionBloc _pendingTransactionBloc;
  MarketFractionBloc _fractionBloc;
  MaxInvestBloc _maxInvestBloc;
  PortofolioByEmitenCodeBloc _portofolioByEmitenCodeBloc;
  MinPurchaseBloc _minPurchaseBloc;
  var codeEmiten;

  final _price = new MoneyMaskedTextController(
      decimalSeparator: '',
      thousandSeparator: ',',
      initialValue: 0,
      precision: 0);
  final _amount = new MoneyMaskedTextController(
    decimalSeparator: '',
    thousandSeparator: ',',
    initialValue: 0,
    precision: 0,
  );
  final _total = new MoneyMaskedTextController(
      decimalSeparator: '',
      thousandSeparator: ',',
      initialValue: 0,
      precision: 0);
  String errMsg;
  var fraction = 0;
  var maxUpperPrice = 0;

  User user;
  EmitenByUuid emitenByUuid;
  AraArbModel araArbModel;
  ProfileBallance profileBallance;
  PortofolioByEmitenCode portofolioByEmitenCode;
  int onFocus =
      0; // 0 = tidak fokus, 1 = fokus ke textfield harga, 2 = fokus ke textfield amount
  double feeBuy = 0;
  double feeSell = 0;
  double maxInvest = 0;
  double minPurchase = 0;

  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    await _trailsServices.createTrails(
        event: marketTrailsData["buy_stocks_form_view_success"]["event"],
        note: marketTrailsData["buy_stocks_form_view_success"]["note"]);
  }

  get divider {
    return Container(height: 3, color: Color(0xFFF4F4F4));
  }

  void addCount() {
    FocusScope.of(context).requestFocus(new FocusNode());
    var count = (_amount.text == "0" || _amount.text.isEmpty
            ? 0
            : num.parse(_amount.text.replaceAll(",", "")).toInt()) +
        1;
    var price = _price.text.isEmpty || _price.text == "0"
        ? 0
        : num.parse(_price.text.replaceAll(",", "")).toInt();
    setState(() {
      _amount.value = TextEditingValue(text: count.toString());
      if (count == 0 || price == 0) {
        _total.value = TextEditingValue(text: "0");
      } else {
        _total.value = TextEditingValue(text: (price * count).toString());
      }
    });
  }

  void minusCount() {
    FocusScope.of(context).requestFocus(new FocusNode());
    var count = (num.parse(_amount.text.replaceAll(",", "")) <= 0 ||
                _amount.text.isEmpty
            ? 0
            : num.parse(_amount.text.replaceAll(",", "")).toInt()) -
        1;
    var price = _price.text.isEmpty || _price.text == "0"
        ? 0
        : num.parse(_price.text.replaceAll(",", "")).toInt();
    setState(() {
      if (count <= 0) {
        count = 0;
        _amount.value = TextEditingValue(text: "0");
      } else {
        _amount.value = TextEditingValue(text: count.toString());
      }
      if (count == 0 || price == 0) {
        _total.value = TextEditingValue(text: "0");
      } else {
        _total.value = TextEditingValue(text: (price * count).toString());
      }
    });
  }

  void addPrice(int fraksi) {
    FocusScope.of(context).requestFocus(new FocusNode());
    var count = _amount.text == "0" || _amount.text.isEmpty
        ? 0
        : num.parse(_amount.text.replaceAll(",", "")).toInt();
    var price =
        (_price.text.isEmpty || num.parse(_price.text.replaceAll(",", "")) <= 0
                ? 0
                : num.parse(_price.text.replaceAll(",", "")).toInt()) +
            fraksi;
    setState(() {
      _price.value = TextEditingValue(text: price.toString());
      if (count == 0 || price == 0) {
        _total.value = TextEditingValue(text: "0");
      } else {
        _total.value = TextEditingValue(text: (price * count).toString());
      }
    });
  }

  void minusPrice(int fraksi) {
    FocusScope.of(context).requestFocus(new FocusNode());
    var count = _amount.text == "0" || _amount.text.isEmpty
        ? 0
        : num.parse(_amount.text.replaceAll(",", "")).toInt();
    var price =
        (_price.text.isEmpty || num.parse(_price.text.replaceAll(",", "")) <= 0
                ? 0
                : num.parse(_price.text.replaceAll(",", "")).toInt()) -
            fraksi;
    setState(() {
      if (price <= 0) {
        _price.value = TextEditingValue(text: "0");
      } else {
        _price.value = TextEditingValue(text: price.toString());
      }
      if (count == 0 || price == 0) {
        _total.value = TextEditingValue(text: "0");
      } else {
        _total.value = TextEditingValue(text: (price * count).toString());
      }
    });
  }

  bool validator() {
    // Harga yg dinputkan 0 atau null
    if (_price.text == "0" || _price.text.isEmpty) {
      setState(() => errMsg = "Harga yang anda inputkan tidak boleh kosong");
      return false;
    }

    // Amount yg dinputkan 0 atau null
    else if (_amount.text == "0" || _amount.text.isEmpty) {
      setState(
          () => errMsg = "Jumlah lembar yang anda inputkan tidak boleh kosong");
      return false;
    }

    // Transaksi melebihi batas kepemilikan saham(Khusus pembelian saham)
    else if (widget.type == TransactionType.beli &&
        user.trader.traderType == 'personal' &&
        profileBallance.data.isUnlimitedInvest == 0 &&
        maxInvest <
            profileBallance.data.totalSaham +
                num.parse(_total.text.replaceAll(",", ""))) {
      setState(() => errMsg =
          "Pembelian saham Anda melebihi batas kepemilikan saham. Silahkan mengubah transaksi atau perbaharui informasi pendapatan pertahun Anda untuk melanjutkan.");
      return false;
    }

    // Transaksi tidak mencapai minimal transaksi
    else if (num.parse(_total.text.replaceAll(",", "")) < minPurchase ||
        _total.text.isEmpty) {
      setState(() => errMsg =
          "Jumlah yang Anda inputkan belum memenuhi ketentuan minimal transaksi saham.\nMinimum Transaksi adalah Rp. ${NumberFormatter.convertNumber(minPurchase)}");
      return false;
    }

    // Harga tidak sesuai dengan kelipatan fraksi
    else if (num.parse(_price.text.replaceAll(",", "")) % fraction != 0) {
      setState(() => errMsg =
          "Harga yang anda inputkan tidak sesuai dengan ketentuan kelipatan harga. Kelipatan harga minimum: ${NumberFormatter.convertNumber(fraction)}");
      return false;
    }

    // Harga melebihi jenjang maksimum fraksi.
    else if (num.parse(_price.text.replaceAll(",", "")) > maxUpperPrice) {
      setState(() => errMsg =
          "Harga yang anda inputkan melebihi jenjang maksimum perubahan.");
      return false;
    }

    // Harga yang diinputkan melebihi ARA
    else if (num.parse(_price.text.replaceAll(",", "")) >
        araArbModel.data.ara) {
      setState(() => errMsg =
          "Harga yang Anda inputkan melebihi batas atas harga saham. Harga tidak boleh lebih besar dari Rp ${NumberFormatter.convertNumber(araArbModel.data.ara)}");
      return false;
    }

    // Harga yang diinputkan melebihi ARB
    else if (num.parse(_price.text.replaceAll(",", "")) <
        araArbModel.data.arb) {
      setState(() => errMsg =
          "Harga yang Anda inputkan melebihi batas bawah harga saham. Harga tidak boleh lebih kecil dari Rp ${NumberFormatter.convertNumber(araArbModel.data.arb)}");
      return false;
    }

    // Total transaksi melebihi dana yang tersedia di wallet. (Khusus pembelian saham)
    else if (widget.type == TransactionType.beli &&
        num.parse(_total.text.replaceAll(",", "")) >
            (profileBallance.data.totalSaldo +
                (widget.pending == null ? 0 : widget.pending))) {
      setState(() => errMsg =
          "Saldo Anda tidak mencukupi. Silahkan ubah pembelian atau deposit terlebih dahulu.");
      return false;
    }

    // Transaksi melebihi jumlah lembar saham yang dimiliki. (Khusus penjualan saham)
    else if (widget.type == TransactionType.jual &&
        num.parse(_amount.text.replaceAll(",", "")) >
            portofolioByEmitenCode.data[0].total) {
      setState(() => errMsg =
          "Total lembar saham yang Anda inputkan melebihi total lembar saham yang Anda miliki saat ini. Total lembar saham yang Anda miliki adalah sejumlah ${NumberFormatter.convertNumber(portofolioByEmitenCode.data[0].total)} lembar");
      return false;
    }

    // Semua inputan valid
    else {
      setState(() => errMsg = null);
      return true;
    }
  }

  @override
  void initState() {
    super.initState();
    initMarketTrails();
    UnauthorizeUsecase.check(context);
    _emitenByUuidBloc = BlocProvider.of<EmitenByUuidBloc>(context);
    _submitTransactionBloc = BlocProvider.of<SubmitTransactionBloc>(context);
    _profileBallanceBloc = BlocProvider.of<ProfileBallanceBloc>(context);
    _pendingTransactionBloc = BlocProvider.of<PendingTransactionBloc>(context);
    _fractionBloc = BlocProvider.of<MarketFractionBloc>(context);
    _araArbBloc = BlocProvider.of<AraArbBloc>(context);
    _maxInvestBloc = BlocProvider.of<MaxInvestBloc>(context);
    _minPurchaseBloc = BlocProvider.of<MinPurchaseBloc>(context);
    _portofolioByEmitenCodeBloc =
        BlocProvider.of<PortofolioByEmitenCodeBloc>(context);
    _emitenByUuidBloc.add(EmitenByUuidRequested(uuid: widget.uuid));
    _profileBallanceBloc.add(ProfileBallanceRequested());
    _pendingTransactionBloc.add(PendingTransactionRequested());
    _fractionBloc.add(MarketFractionRequested());
    _araArbBloc.add(AraArbRequested(emitenUuid: widget.uuid));
    _maxInvestBloc.add(MaxInvestRequested());
    _minPurchaseBloc.add(MinPurchaseRequested());
    if (widget.amount != null) {
      _amount.text = widget.amount.toString();
    }
    if (widget.price != null) {
      if (widget.price != 0) {
        _price.text = widget.price.toString();
        _amount.text = (100000 / widget.price).ceil().toString();
        _total.text =
            (widget.price * (100000 / widget.price).ceil()).toString();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SubmitTransactionBloc, SubmitTransactionState>(
        listener: (context, state) {
      if (state is SubmitTransactionSuccess) {
        errMsg = null;
        _profileBallanceBloc.add(ProfileBallanceRequested());
        _pendingTransactionBloc.add(PendingTransactionRequested());
        _portofolioByEmitenCodeBloc
            .add(PortofolioByEmitenCodeRequested(codeEmiten: codeEmiten));
        if (widget.trxUuid == null) {
          PopupHelperMarket.showOrderBeliBerhasil(context,
              widget.type == TransactionType.beli, widget.trxUuid == null);
        } else {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => SubmitTrxSuccess(
                      isBeli: widget.type == TransactionType.beli)));
        }
      }
      if (state is SubmitTransactionFailure) {
        errMsg = state.message;
      }
    }, builder: (context, state) {
      return ModalProgressHUD(
        inAsyncCall: state is SubmitTransactionLoading,
        color: Colors.black,
        opacity: 0.2,
        progressIndicator: CupertinoActivityIndicator(),
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.black),
              title: Text(
                  widget.type == TransactionType.beli
                      ? "Beli Saham"
                      : "Jual Saham",
                  style: TextStyle(color: Colors.black)),
              centerTitle: true,
            ),
            bottomNavigationBar: BottomAppBar(
              child: BlocBuilder<UserBloc, UserState>(
                builder: (context, state) {
                  if (state is UserLoaded) {
                    user = state.user;
                  }
                  return BlocConsumer<EmitenByUuidBloc, EmitenByUuidState>(
                      listener: (context, state) {
                    if (state is EmitenByUuidLoaded) {
                      codeEmiten = state.emitenByUuid.data.emiten.codeEmiten;
                      _portofolioByEmitenCodeBloc.add(
                          PortofolioByEmitenCodeRequested(
                              codeEmiten:
                                  state.emitenByUuid.data.emiten.codeEmiten));
                    }
                  }, builder: (context, state) {
                    if (state is EmitenByUuidLoaded) {
                      emitenByUuid = state.emitenByUuid;
                    }
                    return BlocBuilder<MarketFeeBloc, MarketFeeState>(
                      builder: (context, stateFee) {
                        if (stateFee is MarketFeeLoaded) {
                          feeBuy = stateFee.feeBuy;
                          feeSell = stateFee.feeSell;
                        }
                        return BlocBuilder<AraArbBloc, AraArbState>(
                          builder: (context, stateAraArb) {
                            if (stateAraArb is AraArbLoaded) {
                              araArbModel = stateAraArb.araArb;
                            }
                            return BlocBuilder<MaxInvestBloc, MaxInvestState>(
                              builder: (context, stateMaxInvest) {
                                if (stateMaxInvest is MaxInvestLoaded) {
                                  maxInvest = stateMaxInvest.maxInvest;
                                }
                                return BlocBuilder<MinPurchaseBloc,
                                    MinPurchaseState>(
                                  builder: (context, stateMinPurchase) {
                                    if (stateMinPurchase is MinPurchaseLoaded) {
                                      minPurchase =
                                          stateMinPurchase.minPurchase;
                                    }
                                    return Container(
                                      padding:
                                          EdgeInsets.fromLTRB(12, 8, 12, 8),
                                      height: 55,
                                      child: FlatButton(
                                        key: Key('btn_jualbeli_in_trx_page'),
                                        disabledColor: Colors.grey,
                                        onPressed: _price.text == "0" ||
                                                _price.text.isEmpty ||
                                                _amount.text == "0" ||
                                                _amount.text.isEmpty
                                            ? null
                                            : () async {
                                                await _trailsServices.createTrails(
                                                    event: marketTrailsData[
                                                            "purchase_confirmation_view"]
                                                        ["event"],
                                                    note: marketTrailsData[
                                                            "purchase_confirmation_view"]
                                                        ["note"]);
                                                if (validator()) {
                                                  await _trailsServices.createTrails(
                                                      event: marketTrailsData[
                                                              "purchase_confirmation_view_success"]
                                                          ["event"],
                                                      note: marketTrailsData[
                                                              "purchase_confirmation_view_success"]
                                                          ["note"]);
                                                  PopupHelperMarket.showKonfirmasiTransaksiSaham(
                                                      context: context,
                                                      uuid: widget.trxUuid,
                                                      emitenUuid: emitenByUuid
                                                          .data.emiten.uuid,
                                                      code: emitenByUuid.data
                                                          .emiten.codeEmiten,
                                                      company: emitenByUuid.data
                                                          .emiten.companyName,
                                                      fee: widget.type ==
                                                              TransactionType
                                                                  .beli
                                                          ? feeBuy
                                                          : feeSell,
                                                      amount: num.parse(_amount
                                                          .text
                                                          .replaceAll(",", "")),
                                                      price: num.parse(
                                                          _price.text.replaceAll(",", "")),
                                                      type: widget.type,
                                                      bloc: _submitTransactionBloc);
                                                } else {
                                                  await _trailsServices.createTrails(
                                                      event: marketTrailsData[
                                                              "purchase_confirmation_view_failed"]
                                                          ["event"],
                                                      note: marketTrailsData[
                                                              "purchase_confirmation_view_failed"]
                                                          ["note"]);
                                                }
                                                FocusScope.of(context)
                                                    .unfocus();
                                              },
                                        child: Text(
                                          widget.type == TransactionType.beli
                                              ? "Beli Saham"
                                              : "Jual Saham",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color:
                                            widget.type == TransactionType.beli
                                                ? Color(0xFF0E7E4A)
                                                : Color(0xFFEB5255),
                                      ),
                                    );
                                  },
                                );
                              },
                            );
                          },
                        );
                      },
                    );
                  });
                },
              ),
            ),
            body: GestureDetector(
              onTap: () {
                setState(() => onFocus = 0);
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: BlocBuilder<EmitenByUuidBloc, EmitenByUuidState>(
                  builder: (context, state) {
                if (state is EmitenByUuidLoaded) {
                  EmitenByUuid emitenByUuid = state.emitenByUuid;
                  Query query = FirebaseFirestore.instance.collection(
                      "current_price_${state.emitenByUuid.data.emiten.codeEmiten}");
                  return ListView(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: SantaraAppBetaTesting(),
                      ),
                      // info saham
                      ListTile(
                        title: EmitenByUuid == null
                            ? CupertinoActivityIndicator()
                            : Text(emitenByUuid.data.emiten.codeEmiten),
                        subtitle: EmitenByUuid == null
                            ? CupertinoActivityIndicator()
                            : Text(emitenByUuid.data.emiten.companyName),
                      ),
                      divider,
                      // info saldo / saham
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                        child: Text(
                            widget.type == TransactionType.beli
                                ? "Info Saldo"
                                : "Info Saham",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold)),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                        child: widget.type == TransactionType.beli
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                    Text("Dana Tersedia",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 15)),
                                    BlocBuilder<ProfileBallanceBloc,
                                        ProfileBallanceState>(
                                      builder: (context, stateBallance) {
                                        return BlocBuilder<
                                                PendingTransactionBloc,
                                                PendingTransactionState>(
                                            builder: (context, statePending) {
                                          if (stateBallance
                                                  is ProfileBallanceLoaded &&
                                              statePending
                                                  is PendingTransactionLoaded) {
                                            profileBallance =
                                                stateBallance.profileBallance;
                                            return Text(
                                                "Rp " +
                                                    NumberFormatter.convertNumber(
                                                        stateBallance
                                                                .profileBallance
                                                                .data
                                                                .totalSaldo +
                                                            (widget.pending ==
                                                                    null
                                                                ? 0
                                                                : widget
                                                                    .pending)),
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold));
                                          } else {
                                            return CupertinoActivityIndicator();
                                          }
                                        });
                                      },
                                    )
                                  ])
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Flexible(
                                    flex: 3,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text("Saham yang tersedia",
                                            style:
                                                TextStyle(color: Colors.black)),
                                        Text(
                                          "(Saham yang belum Anda jual dan masih di portofolio)",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Color(0xFF676767)),
                                        )
                                      ],
                                    ),
                                  ),
                                  Flexible(
                                      flex: 2,
                                      child: BlocBuilder<
                                              PortofolioByEmitenCodeBloc,
                                              PortofolioByEmitenCodeState>(
                                          builder: (context, statePortofolio) {
                                        if (statePortofolio
                                            is PortofolioByEmitenCodeLoaded) {
                                          portofolioByEmitenCode =
                                              statePortofolio.portofolio;
                                          return Text(
                                              NumberFormatter.convertNumber(
                                                      statePortofolio.portofolio
                                                              .data[0].total -
                                                          statePortofolio
                                                              .portofolio
                                                              .data[0]
                                                              .pending +
                                                          (widget.pending ==
                                                                  null
                                                              ? 0
                                                              : widget
                                                                  .pending)) +
                                                  " Lembar",
                                              textAlign: TextAlign.right,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold));
                                        } else {
                                          return CupertinoActivityIndicator();
                                        }
                                      }))
                                ],
                              ),
                      ),

                      StreamBuilder<QuerySnapshot>(
                          stream: query.snapshots(),
                          builder: (context, stream) {
                            QuerySnapshot querySnapshot = stream.data;
                            var snapshot;
                            if (stream.connectionState ==
                                ConnectionState.active) {
                              if (querySnapshot.docs.length > 0) {
                                snapshot = querySnapshot.docs[0].data();
                              }
                            }
                            return Column(
                              children: [
                                divider,
                                // form beli
                                _form(),
                                divider,
                                _detailSaham(emitenByUuid, snapshot, stream),
                                Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: InfoPrice(
                                      codeEmiten:
                                          emitenByUuid.data.emiten.codeEmiten,
                                      prev: snapshot == null
                                          ? emitenByUuid.data.emiten.price
                                          : snapshot["prev"],
                                      price: snapshot == null
                                          ? emitenByUuid.data.emiten.price
                                          : snapshot["price"],
                                    )),
                                _table(emitenByUuid.data.emiten.codeEmiten,
                                    snapshot == null ? 0 : snapshot["prev"]),
                              ],
                            );
                          }),
                    ],
                  );
                } else {
                  return Container(
                    child: Center(
                      child: CupertinoActivityIndicator(),
                    ),
                  );
                }
              }),
            )),
      );
    });
  }

  Widget _form() {
    return BlocBuilder<MarketFractionBloc, MarketFractionState>(
      builder: (context, state) {
        MarketFraction marketFraction;
        if (state is MarketFractionLoaded) {
          marketFraction = state.marketFraction;
          for (var i = 0; i < marketFraction.data.length; i++) {
            if (_price.text.isNotEmpty) {
              if (RegExp(r"^[0-9]*$")
                  .hasMatch(_price.text.replaceAll(",", ""))) {
                if (num.parse(_price.text.replaceAll(",", "")) <=
                        marketFraction.data[i].priceUpper &&
                    num.parse(_price.text.replaceAll(",", "")) >=
                        marketFraction.data[i].priceLower &&
                    !marketFraction.data[i].isDeleted) {
                  fraction = marketFraction.data[i].fraction;
                  i = marketFraction.data.length;
                }
              }
            }
          }
          maxUpperPrice =
              marketFraction.data[marketFraction.data.length - 1].priceUpper;
        }
        return Theme(
            data: ThemeData(primaryColor: Color(0xFFBF2D30)),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  // text field harga
                  Row(
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Text(widget.type == TransactionType.beli
                              ? "Beli di Harga"
                              : "Jual di Harga")),
                      Container(width: 12),
                      Expanded(
                          flex: 2,
                          child: Container(
                            height: 58,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border:
                                    Border.all(width: 1, color: Colors.grey)),
                            child: Row(
                              children: <Widget>[
                                onFocus == 1
                                    ? Container(
                                        height: 58,
                                        width: 58,
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF4F4F4),
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: IconButton(
                                            key: Key('min_price_btn'),
                                            icon:
                                                Icon(Icons.keyboard_arrow_down),
                                            onPressed: () =>
                                                minusPrice(fraction)),
                                      )
                                    : Container(width: 8),
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 4.0),
                                    child: TextField(
                                      key: Key('text_field_price'),
                                      controller: _price,
                                      textAlign: TextAlign.right,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          prefix: Text("Rp "),
                                          border: InputBorder.none),
                                      onTap: () => setState(() => onFocus = 1),
                                      onChanged: (value) {
                                        setState(() {
                                          if (_price.text == "0" ||
                                              _price.text.isEmpty) {
                                            _total.text = "0";
                                          } else if (_amount.text == "0" ||
                                              _amount.text.isEmpty) {
                                            _total.text = "0";
                                          } else {
                                            var price = num.parse(_price.text
                                                .replaceAll(",", ""));
                                            var count = num.parse(_amount.text
                                                .replaceAll(",", ""));
                                            _total.text =
                                                (price * count).toString();
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                ),
                                onFocus == 1
                                    ? Container(
                                        height: 58,
                                        width: 58,
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF4F4F4),
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: IconButton(
                                            key: Key('plus_price_btn'),
                                            icon: Icon(Icons.keyboard_arrow_up),
                                            onPressed: () =>
                                                addPrice(fraction)),
                                      )
                                    : Container(width: 8),
                              ],
                            ),
                          ))
                    ],
                  ),
                  Container(height: 8),
                  // text field amount
                  Row(
                    children: <Widget>[
                      Expanded(flex: 1, child: Text("Jumlah")),
                      Container(width: 12),
                      Expanded(
                          flex: 2,
                          child: Container(
                            height: 58,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border:
                                    Border.all(width: 1, color: Colors.grey)),
                            child: Row(
                              children: <Widget>[
                                onFocus == 2
                                    ? Container(
                                        height: 58,
                                        width: 58,
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF4F4F4),
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: IconButton(
                                            key: Key('min_amount_btn'),
                                            icon:
                                                Icon(Icons.keyboard_arrow_down),
                                            onPressed: minusCount),
                                      )
                                    : Container(width: 8),
                                Flexible(
                                    child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 4.0),
                                  child: TextField(
                                    key: Key('text_field_amount'),
                                    controller: _amount,
                                    textAlign: TextAlign.right,
                                    keyboardType: TextInputType.number,
                                    onTap: () => setState(() => onFocus = 2),
                                    onChanged: (value) {
                                      setState(() {
                                        if (_amount.text == "0" ||
                                            _amount.text.isEmpty) {
                                          _total.text = "0";
                                        } else if (_price.text == "0" ||
                                            _price.text.isEmpty) {
                                          _total.text = "0";
                                        } else {
                                          var price = num.parse(
                                              _price.text.replaceAll(",", ""));
                                          var count = num.parse(
                                              _amount.text.replaceAll(",", ""));
                                          _total.text =
                                              (price * count).toString();
                                        }
                                      });
                                    },
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                  ),
                                )),
                                onFocus == 2
                                    ? Container(
                                        height: 58,
                                        width: 58,
                                        decoration: BoxDecoration(
                                          color: Color(0xFFF4F4F4),
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: IconButton(
                                            key: Key('plus_amount_btn'),
                                            icon: Icon(Icons.keyboard_arrow_up),
                                            onPressed: addCount),
                                      )
                                    : Container(width: 8),
                              ],
                            ),
                          ))
                    ],
                  ),
                  Container(height: 8),
                  // text field total
                  Row(
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Text(widget.type == TransactionType.beli
                              ? "Total Beli"
                              : "Total Jual")),
                      Container(width: 12),
                      Expanded(
                          flex: 2,
                          child: TextField(
                            controller: _total,
                            enabled: false,
                            textAlign: TextAlign.right,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                prefix: Text("Rp "),
                                border: OutlineInputBorder()),
                          ))
                    ],
                  ),
                  errMsg == null
                      ? Container()
                      : Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(12),
                          margin: EdgeInsets.symmetric(vertical: 8),
                          decoration: BoxDecoration(color: Color(0xFFFCE9D3)),
                          child: Column(children: [
                            Text(errMsg),
                            errMsg.contains("deposit")
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: GestureDetector(
                                      onTap: () => Navigator.pushNamed(
                                          context, "/deposit"),
                                      child: Text("Deposit sekarang",
                                          style: TextStyle(
                                            color: Color(0xFF666EE8),
                                            decoration:
                                                TextDecoration.underline,
                                          )),
                                    ),
                                  )
                                : Container()
                          ]))
                ],
              ),
            ));
      },
    );
  }

  Widget _detailSaham(EmitenByUuid data, Map<String, dynamic> snapshot,
      AsyncSnapshot<QuerySnapshot> stream) {
    return Padding(
        padding: const EdgeInsets.all(16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(data.data.emiten.codeEmiten,
                    style:
                        TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                Text(
                  data.data.emiten.trademark,
                  style: TextStyle(fontSize: 12),
                ),
                Text(data.data.emiten.companyName,
                    style: TextStyle(color: Color(0xFF676767), fontSize: 14)),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text("Harga Terakhir", style: TextStyle(fontSize: 12)),
                stream.connectionState == ConnectionState.waiting
                    ? Center(child: CupertinoActivityIndicator())
                    : Text(
                        "Rp " +
                            NumberFormatter.convertNumber(snapshot == null
                                ? data.data.emiten.price
                                : snapshot["price"]),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                Row(
                  children: <Widget>[
                    stream.connectionState == ConnectionState.waiting
                        ? Center(child: CupertinoActivityIndicator())
                        : stream.hasError
                            ? Container()
                            : snapshot == null
                                ? Padding(
                                    padding: const EdgeInsets.only(
                                        left: 4, right: 4),
                                    child: Icon(MdiIcons.circleDouble,
                                        color: Colors.orange, size: 16),
                                  )
                                : snapshot["price"] > snapshot["prev"]
                                    ? Icon(Icons.arrow_drop_up,
                                        color: Colors.green)
                                    : snapshot["price"] < snapshot["prev"]
                                        ? Icon(
                                            Icons.arrow_drop_down,
                                            color: Colors.red,
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.only(
                                                left: 4, right: 4),
                                            child: Icon(MdiIcons.circleDouble,
                                                color: Colors.orange, size: 16),
                                          ),
                    Text(
                        stream.connectionState != ConnectionState.waiting
                            ? snapshot == null
                                ? "0.0 %"
                                : "${((snapshot["price"] - snapshot["prev"]) / snapshot["prev"] * 100).toStringAsFixed(1)} %"
                            : "",
                        style: TextStyle(
                            color: snapshot == null
                                ? Colors.orange
                                : snapshot["price"] > snapshot["prev"]
                                    ? Colors.green
                                    : snapshot["price"] < snapshot["prev"]
                                        ? Colors.red
                                        : Colors.orange)),
                  ],
                )
              ],
            ),
          ],
        ));
  }

  Widget _table(String codeEmiten, int prev) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          child: table.TableJualBeli(
            isJual: false,
            prev: prev,
            codeEmiten: codeEmiten,
          ),
        ),
        Container(
          width: 8,
        ),
        Flexible(
          child: table.TableJualBeli(
            isJual: true,
            prev: prev,
            codeEmiten: codeEmiten,
          ),
        ),
      ],
    );
  }
}
