import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/blocs/market/history_market_bloc.dart';
import 'package:santaraapp/blocs/market/history_today_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/models/market/history_market.dart' as history;
import 'package:santaraapp/models/market/history_today.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/market/detail_order_view.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:shimmer/shimmer.dart';

class TransaksiView extends StatefulWidget {
  @override
  _TransaksiViewState createState() => _TransaksiViewState();
}

class _TransaksiViewState extends State<TransaksiView>
    with SingleTickerProviderStateMixin {
  TabController controller;
  HistoryMarketBloc _marketBloc;
  HistoryTodayBloc _todayBloc;
  FlutterSecureStorage storage = FlutterSecureStorage();

  int pageHistory = 1;
  int lastPageHistory = 0;
  ScrollController _scrollHistory;

  _scrollListenerHistory() {
    if (_scrollHistory.offset >= _scrollHistory.position.maxScrollExtent &&
        !_scrollHistory.position.outOfRange) {
      if (lastPageHistory != null && pageHistory < lastPageHistory) {
        pageHistory = pageHistory + 1;
        _marketBloc.add(HistoryMarketNextPageRequested(page: pageHistory));
      }
    }
  }

  Future _checkLogin() async {
    await storage.read(key: 'token').then((value) {
      if (value == null) {
        PopupHelperMarket.needLoginToAccessThisPage(
            context: context, isGoBack: 1);
      }
    });
  }

  void initState() {
    super.initState();
    _checkLogin();
    UnauthorizeUsecase.check(context);
    controller = new TabController(vsync: this, length: 2, initialIndex: 0);
    _scrollHistory = ScrollController();
    _scrollHistory.addListener(_scrollListenerHistory);
    _marketBloc = BlocProvider.of<HistoryMarketBloc>(context);
    _todayBloc = BlocProvider.of<HistoryTodayBloc>(context);
    _marketBloc.add(HistoryMarketRequested());
    _todayBloc.add(HistoryTodayRequested());
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text("Transaksi", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        bottom: TabBar(
            controller: controller,
            labelColor: Color(0xFFBF2D30),
            indicatorColor: Color(0xFFBF2D30),
            unselectedLabelColor: Colors.black,
            tabs: [
              new Tab(key: Key("order_tabbar"), text: "Order"),
              new Tab(key: Key("history_tabbar"), text: "Riwayat Transaksi"),
            ]),
      ),
      backgroundColor: Color(ColorRev.mainBlack),
      body: new TabBarView(
          controller: controller, children: <Widget>[_order(), _histori()]),
    );
  }

  Widget _order() {
    return ListView(padding: EdgeInsets.fromLTRB(12, 16, 12, 16), children: <
        Widget>[
      SantaraAppBetaTesting(),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Expanded(
              flex: 3,
              child: Center(
                child: Text("Kode Saham",
                    style: TextStyle(fontSize: 12, color: Color(0xFFA0A0A0))),
              )),
          Expanded(
              flex: 3,
              child: Center(
                child: Text("Total",
                    style: TextStyle(fontSize: 12, color: Color(0xFFA0A0A0))),
              )),
          Expanded(
              flex: 3,
              child: Center(
                child: Text("Tipe",
                    style: TextStyle(fontSize: 12, color: Color(0xFFA0A0A0))),
              )),
          Expanded(
              flex: 3,
              child: Center(
                child: Text("Status",
                    style: TextStyle(fontSize: 12, color: Color(0xFFA0A0A0))),
              )),
          Expanded(flex: 1, child: Container()),
        ],
      ),
      Container(height: 8),
      BlocBuilder<HistoryTodayBloc, HistoryTodayState>(
        builder: (context, state) {
          HistoryToday data;
          if (state is HistoryTodayLoaded) {
            data = state.historyToday;
          }
          if (state is HistoryTodayLoading) {
            return _shimmer(3);
          } else {
            if (data == null || data.data.length == 0) {
              return Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icon/search_not_found.png"),
                    Text("Data transaksi tidak ditemukan")
                  ],
                ),
              );
            } else {
              return ListView.separated(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: data.data.length,
                  itemBuilder: (_, i) {
                    return GestureDetector(
                      key: Key('item_order_$i'),
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => DetailOrderView(
                                    historyToday: data.data[i],
                                    type: DetailOrderType.order,
                                  ))),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            border:
                                Border.all(width: 1, color: Color(0xFFDADADA))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                                flex: 3,
                                child: Center(
                                    child: Text(data.data[i].codeEmiten))),
                            Expanded(
                                flex: 3,
                                child: Center(
                                    child:
                                        Text("${data.data[i].startAmount}"))),
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  data.data[i].type == "buy" ? "BELI" : "JUAL",
                                  style: TextStyle(
                                      color: data.data[i].type == "buy"
                                          ? Color(0xFF0E7E4A)
                                          : Color(0xFFBF2D30)),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  "${data.data[i].statusOrder}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Icon(
                                Icons.chevron_right,
                                color: Color(0xFFDADADA),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (_, i) {
                    return Container(height: 8);
                  });
            }
          }
        },
      ),
    ]);
  }

  Widget _histori() {
    return BlocBuilder<HistoryMarketBloc, HistoryMarketState>(
      builder: (context, state) {
        if (state is HistoryMarketLoaded) {
          lastPageHistory = state.lastPage;
        }
        final currentState = state;
        List<history.History> data = currentState.props[0];
        return ListView(
          padding: EdgeInsets.fromLTRB(12, 16, 12, 16),
          controller: _scrollHistory,
          children: <Widget>[
            SantaraAppBetaTesting(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                    flex: 3,
                    child: Center(
                      child: Text("Kode Saham",
                          style: TextStyle(
                              fontSize: 12, color: Color(0xFFA0A0A0))),
                    )),
                Expanded(
                    flex: 3,
                    child: Center(
                      child: Text("Total",
                          style: TextStyle(
                              fontSize: 12, color: Color(0xFFA0A0A0))),
                    )),
                Expanded(
                    flex: 3,
                    child: Center(
                      child: Text("Tipe",
                          style: TextStyle(
                              fontSize: 12, color: Color(0xFFA0A0A0))),
                    )),
                Expanded(
                    flex: 3,
                    child: Center(
                      child: Text("Tanggal",
                          style: TextStyle(
                              fontSize: 12, color: Color(0xFFA0A0A0))),
                    )),
                Expanded(flex: 1, child: Container()),
              ],
            ),
            Container(height: 8),
            state is HistoryTodayLoading
                ? _shimmer(3)
                : data == null || data.length == 0
                    ? Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("assets/icon/search_not_found.png"),
                            Text("Data transaksi tidak ditemukan")
                          ],
                        ),
                      )
                    : ListView.separated(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: data.length,
                        itemBuilder: (_, i) {
                          return GestureDetector(
                            key: Key('item_history_'),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => DetailOrderView(
                                          historyMarket: data[i],
                                          type: DetailOrderType.history,
                                        ))),
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  border: Border.all(
                                      width: 1, color: Color(0xFFDADADA))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Expanded(
                                      flex: 3,
                                      child: Center(
                                          child: Text(data[i].codeEmiten))),
                                  Expanded(
                                      flex: 3,
                                      child: Center(
                                          child: Text("${data[i].amount}"))),
                                  Expanded(
                                    flex: 3,
                                    child: Center(
                                      child: Text(
                                        data[i].type == "buy" ? "BELI" : "JUAL",
                                        style: TextStyle(
                                            color: data[i].type == "buy"
                                                ? Color(0xFF0E7E4A)
                                                : Color(0xFFBF2D30)),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Center(
                                      child: Text(
                                        DateFormat("dd MMM yyyy")
                                            .format(data[i].createdAt),
                                        overflow: TextOverflow.visible,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Color(0xFFDADADA),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (_, i) {
                          return Container(height: 8);
                        }),
            state is HistoryMarketPaginationLoading ? _shimmer(2) : Container()
          ],
        );
      },
    );
  }

  Widget _shimmer(int count) {
    return ListView.separated(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: count,
      separatorBuilder: (_, i) {
        return Container(height: 8);
      },
      itemBuilder: (_, i) {
        return Shimmer.fromColors(
            baseColor: Colors.grey[200],
            highlightColor: Colors.grey[400],
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey[400],
                  borderRadius: BorderRadius.circular(4)),
              height: 60,
              width: MediaQuery.of(context).size.width,
            ));
      },
    );
  }
}
