import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

import '../../pages/Home.dart';
import '../widget/components/market/field_pasar_sekunder.dart';
import 'home_market.dart';
import 'transaksi_view.dart';

class SubmitTrxSuccess extends StatelessWidget {
  final bool isBeli;

  SubmitTrxSuccess({Key key, this.isBeli}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(Sizes.s20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            // mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset("assets/icon/success.png",
                  width: MediaQuery.of(context).size.width / 3,
                  height: MediaQuery.of(context).size.width / 3),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: Text(
                    isBeli ? "Order Beli Berhasil" : "Order Jual berhasil",
                    style:
                        TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
              ),
              Text(
                isBeli
                    ? "Order berhasil dan telah masuk ke antrian. Jika match, maka transaksi akan otomatis diproses dan saham akan masuk ke portofolio Anda."
                    : "Order berhasil dan telah masuk ke antrian. Jika match, maka transaksi akan otomatis diproses dan saham akan dikurangi dari portofolio Anda. ",
                style: TextStyle(fontSize: 15),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: SantaraMainButton(
                  key: Key('go_to_transaction_page'),
                  height: 40,
                  child: Text(
                    "Lihat Transaksi",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => Home(index: 4)),
                        (route) => false);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => HomeMarket()));
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => TransaksiView()));
                  },
                ),
              ),
              SantaraSecondaryButton(
                key: Key('go_to_home_market_page'),
                height: 40,
                child: Text(
                  "Home Pasar Sekunder",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xFFBF2D30),
                    fontSize: 14,
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => Home(index: 4)),
                      (route) => false);
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => HomeMarket()));
                },
                decoration: BoxDecoration(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
