import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/portofolio_list_model.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/check_today_widget.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';

class DetailPortofolioView extends StatefulWidget {
  final bool isAvailable;
  final bool isInOrder;
  final Portofolio portofolio;
  DetailPortofolioView({this.isAvailable, this.isInOrder, this.portofolio});
  @override
  _DetailPortofolioViewState createState() => _DetailPortofolioViewState();
}

class _DetailPortofolioViewState extends State<DetailPortofolioView> {
  CheckTodayBloc _checkTodayBloc;
  FlutterSecureStorage storage = FlutterSecureStorage();
  var token;

  Future _checkLogin() async {
    token = await storage.read(key: 'token');
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    _checkLogin();
    _checkTodayBloc = BlocProvider.of<CheckTodayBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${widget.portofolio.codeEmiten}");
    return StreamBuilder<Object>(
        stream: query.snapshots(),
        builder: (context, stream) {
          QuerySnapshot querySnapshot = stream.data;
          var snapshot;
          if (stream.connectionState == ConnectionState.active) {
            if (querySnapshot.docs.length > 0) {
              snapshot = querySnapshot.docs[0].data();
            }
          }
          return CheckTodayWidget(
              checkTodayBloc: _checkTodayBloc,
              builder: (context, state) {
                return ModalProgressHUD(
                  inAsyncCall: state is CheckTodayLoading,
                  color: Colors.black,
                  opacity: 0.2,
                  progressIndicator: CupertinoActivityIndicator(),
                  child: Scaffold(
                    appBar: AppBar(
                      backgroundColor: Colors.white,
                      iconTheme: IconThemeData(color: Colors.black),
                      title: Text("Portofolio",
                          style: TextStyle(color: Colors.black)),
                      centerTitle: true,
                    ),
                    bottomNavigationBar: BottomAppBar(
                      child: Container(
                        padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                        height: 55,
                        child: FlatButton(
                          key: Key('btn_jual_in_detail_portofolio'),
                          onPressed: !widget.isAvailable || widget.isInOrder
                              ? null
                              : () {
                                  if (token == null) {
                                    PopupHelperMarket.needLoginToAccessThisPage(
                                        context: context, isGoBack: 0);
                                  } else {
                                    _checkTodayBloc.add(CheckTodayRequested(
                                        uuid: widget.portofolio.emitenUuid,
                                        type: TransactionType.jual,
                                        codeEmiten:
                                            widget.portofolio.codeEmiten,
                                        price: snapshot == null
                                            ? widget.portofolio.lastPrice
                                                .toInt()
                                            : snapshot["price"],
                                        totalSaham: widget.portofolio.total));
                                  }
                                },
                          child: Text(
                            "Jual",
                            style: TextStyle(color: Colors.white),
                          ),
                          disabledColor: Color(0xFF868686),
                          color: Color(0xFFEB5255),
                        ),
                      ),
                    ),
                    body: ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: SantaraAppBetaTesting(),
                        ),
                        ListTile(
                          title: Text(widget.portofolio.codeEmiten),
                          subtitle: Text(widget.portofolio.companyName),
                        ),
                        _itemPortofolio("Kode", widget.portofolio.codeEmiten),
                        _itemPortofolio(
                            "Market Value",
                            NumberFormatter.convertNumberDecimal(
                                widget.portofolio.marketValue)),
                        _itemPortofolio(
                            "Pot G/L",
                            NumberFormatter.convertNumberDecimal(
                                widget.portofolio.potGl)),
                        _itemPortofolio(
                            "% Pot G/L",
                            NumberFormatter.convertNumberDecimal(
                                    widget.portofolio.potGlPercent) +
                                "%"),
                        _itemPortofolio(
                            "Avg Price",
                            NumberFormatter.convertNumberDecimal(
                                widget.portofolio.avgPrice)),
                        _itemPortofolio(
                            "Jumlah",
                            NumberFormatter.convertNumberDecimal(
                                widget.portofolio.total)),
                        _itemPortofolio(
                            "Last Price",
                            NumberFormatter.convertNumberDecimal(
                                snapshot == null
                                    ? widget.portofolio.lastPrice
                                    : snapshot["price"])),
                        _itemPortofolio(
                            "Avg Value",
                            NumberFormatter.convertNumberDecimal(
                                widget.portofolio.avgValue)),
                        widget.isInOrder
                            ? _itemPortofolio("Status Order", "Open")
                            : Container(),
                        !widget.isAvailable
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(16, 10, 16, 10),
                                child: RichText(
                                  textAlign: TextAlign.justify,
                                  text: TextSpan(
                                      text: 'Catatan : ',
                                      style:
                                          TextStyle(color: Color(0xFFF28A8A)),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text:
                                                'Penerbit ini belum masuk dipasar sekunder. Penerbit di papan utama Santara bisa masuk ke pasar sekunder minimal 1 tahun setelah listing dipapan utama dan memenuhi persyaratan dokumen. ',
                                            style:
                                                TextStyle(color: Colors.black))
                                      ]),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                );
              });
        });
  }

  Widget _itemPortofolio(String title, String value) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title),
          Text(
            value,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
