import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/blocs/market/glossarium_search_bloc.dart';
import 'package:santaraapp/widget/widget/components/market/item_glossarium.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

class GlossariumSearchView extends StatefulWidget {
  @override
  _GlossariumSearchViewState createState() => _GlossariumSearchViewState();
}

class _GlossariumSearchViewState extends State<GlossariumSearchView> {
  final _search = TextEditingController();
  GlossariumSearchBloc _glossariumSearchBloc;

  @override
  void initState() {
    super.initState();
    _glossariumSearchBloc = BlocProvider.of<GlossariumSearchBloc>(context);
    _glossariumSearchBloc.add(GlossariumSearchRequested(keySearch: ""));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: Padding(
          padding: const EdgeInsets.only(right: 8),
          child: TextField(
            textInputAction: TextInputAction.search,
            autofocus: true,
            controller: _search,
            onSubmitted: (value) {
              FocusScope.of(context).requestFocus(FocusNode());
              if (value.isNotEmpty) {
                _glossariumSearchBloc
                    .add(GlossariumSearchRequested(keySearch: value));
              } else {
                ToastHelper.showFailureToast(
                    context, "Kata kunci pencarian tidak boleh kosong");
              }
            },
            decoration: InputDecoration(
                hintText: "Cari Glossarium",
                hintStyle: TextStyle(color: Color(0xff676767)),
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                fillColor: Color(0xffF2F2F2),
                border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                    borderSide: BorderSide(color: Color(0xffDDDDDD))),
                suffixIcon: IconButton(
                    padding: EdgeInsets.only(right: 16),
                    color: Colors.black,
                    icon: Icon(Icons.search),
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (_search.text.isNotEmpty) {
                        _glossariumSearchBloc.add(
                            GlossariumSearchRequested(keySearch: _search.text));
                      } else {
                        ToastHelper.showFailureToast(
                            context, "Kata kunci pencarian tidak boleh kosong");
                      }
                    })),
          ),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.close),
          color: Colors.black,
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: BlocBuilder<GlossariumSearchBloc, GlossariumSearchState>(
        builder: (context, state) {
          if (state is GlossariumSearchLoaded) {
            if (state.glossariumList.data.length == 0) {
              return Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/icon/search_not_found.png",
                      width: MediaQuery.of(context).size.width * 2 / 3,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 12, 16, 4),
                      child: Text(
                        "Tidak ada hasil yang ditemukan",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Text("Coba kata kunci penelusuran lainnya"),
                    ),
                  ],
                ),
              );
            } else {
              return ListView.builder(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  itemCount: state.glossariumList.data.length,
                  itemBuilder: (_, i) {
                    return ItemGlossarium(
                        key: Key('item_glossarium_search_$i'),
                        title: state.glossariumList.data[i].title,
                        content: state.glossariumList.data[i].description);
                  });
            }
          }
          if (state is GlossariumSearchLoading) {
            return Container(
              child: Center(
                child: CupertinoActivityIndicator(),
              ),
            );
          }
          if (state is GlossariumSearchError) {
            return Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  Text("Data glossarium tidak ditemukan")
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
