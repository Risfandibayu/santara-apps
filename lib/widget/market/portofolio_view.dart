import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/blocs/current_time_bloc.dart';
import 'package:santaraapp/blocs/market/pending_transaction_bloc.dart';
import 'package:santaraapp/blocs/market/portofolio_list_bloc.dart';
import 'package:santaraapp/blocs/market/profile_ballance_bloc.dart';
import 'package:santaraapp/blocs/market/summary_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/DateFormatter.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/portofolio_list_model.dart';
import 'package:santaraapp/models/market/summary_model.dart';
import 'package:santaraapp/widget/market/detail_portofolio_view.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/date_range_picker.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:shimmer/shimmer.dart';

class PortofolioView extends StatefulWidget {
  @override
  _PortofolioViewState createState() => _PortofolioViewState();
}

class _PortofolioViewState extends State<PortofolioView>
    with TickerProviderStateMixin {
  PortofolioListBloc _portofolioListBloc;
  ProfileBallanceBloc _profileBallanceBloc;
  PendingTransactionBloc _pendingTransactionBloc;
  SummaryBloc _summaryBloc;
  ScrollController _scroll;
  TabController _portofolioTabs;
  FlutterSecureStorage storage = FlutterSecureStorage();

  int page = 1;
  int lastPage = 0;
  int selectedIndex = 0;
  List<DateTime> initialDate = [];

  Future _checkLogin() async {
    await storage.read(key: 'token').then((value) {
      if (value == null) {
        PopupHelperMarket.needLoginToAccessThisPage(
            context: context, isGoBack: 1);
      }
    });
  }

  _scrollListener() {
    if (_scroll.offset >= _scroll.position.maxScrollExtent &&
        !_scroll.position.outOfRange) {
      if (lastPage != null && page < lastPage) {
        page = page + 1;
        _portofolioListBloc.add(PortofolioListNextPageRequested(page: page));
      }
    }
  }

  Future _pickDateRange(
      DateTime start, DateTime end, DateTime currentDate) async {
    var picked = await showDateRangePickerCustom(
      context: context,
      initialFirstDate: start,
      initialLastDate: end,
      firstDate: new DateTime(2021),
      lastDate: currentDate,
    );
    if (initialDate != null && initialDate.length == 2) {
      _summaryBloc.add(SummaryRequested(
          start: DateFormatter.yyyy_MM_dd(picked[0]),
          end: DateFormatter.yyyy_MM_dd(picked[1])));
    }
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
    UnauthorizeUsecase.check(context);
    _scroll = ScrollController();
    _scroll.addListener(_scrollListener);
    _portofolioTabs =
        new TabController(vsync: this, length: 2, initialIndex: selectedIndex);
    _profileBallanceBloc = BlocProvider.of<ProfileBallanceBloc>(context);
    _pendingTransactionBloc = BlocProvider.of<PendingTransactionBloc>(context);
    _portofolioListBloc = BlocProvider.of<PortofolioListBloc>(context);
    _summaryBloc = BlocProvider.of<SummaryBloc>(context);
    _profileBallanceBloc.add(ProfileBallanceRequested());
    _pendingTransactionBloc.add(PendingTransactionRequested());
    _portofolioListBloc.add(PortofolioListRequested());
  }

  String x = "Select Picker";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text("Portofolio", style: TextStyle(color: Colors.black)),
        centerTitle: true,
      ),
      body: BlocBuilder<CurrentTimeBloc, CurrentTimeState>(
        builder: (context, stateTime) {
          var currentDate =
              stateTime is CurrentTimeLoaded ? stateTime.now : DateTime.now();
          if (stateTime is CurrentTimeLoaded) {
            _summaryBloc.add(SummaryRequested(
                start: DateFormatter.yyyy_MM_dd(stateTime.now),
                end: DateFormatter.yyyy_MM_dd(stateTime.now)));
          }
          return BlocBuilder<SummaryBloc, SummaryState>(
            builder: (context, stateSummary) {
              initialDate = stateSummary is SummaryLoaded
                  ? [
                      DateTime.parse(stateSummary.start),
                      DateTime.parse(stateSummary.end)
                    ]
                  : [
                      DateTime.parse(stateSummary.props[0]),
                      DateTime.parse(stateSummary.props[1])
                    ];
              Summary summary =
                  stateSummary is SummaryLoaded ? stateSummary.summary : null;
              return ListView(
                controller: _scroll,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: SantaraAppBetaTesting(),
                  ),
                  _infoPortofolio(),
                  _tabs(),
                  IndexedStack(
                    children: <Widget>[
                      Visibility(
                        child: _listPortofolio(),
                        maintainState: true,
                        visible: selectedIndex == 0,
                      ),
                      Visibility(
                        child: Container(
                            child: stateSummary is SummaryLoaded
                                ? _infoKas(initialDate, currentDate, summary)
                                : stateSummary is SummaryLoading
                                    ? Container(
                                        height: 100,
                                        child: Center(
                                            child:
                                                CupertinoActivityIndicator()))
                                    : stateSummary is SummaryError
                                        ? Container(
                                            width: double.infinity,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Image.asset(
                                                  "assets/icon/search_not_found.png",
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2,
                                                ),
                                                Text(stateSummary.error)
                                              ],
                                            ),
                                          )
                                        : Container()),
                        maintainState: true,
                        visible: selectedIndex == 1,
                      ),
                    ],
                    index: selectedIndex,
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }

  Widget _tabs() {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(blurRadius: 2, color: Colors.grey[400], offset: Offset(0, 4))
      ]),
      child: TabBar(
          controller: _portofolioTabs,
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          isScrollable: true,
          onTap: (int index) {
            setState(() {
              selectedIndex = index;
              _portofolioTabs.animateTo(index);
            });
          },
          tabs: [
            Container(
                key: Key('tab_portovolio_list'),
                height: 40,
                width: (MediaQuery.of(context).size.width - 64) / 2,
                child: Center(child: Text("Portofolio"))),
            Container(
                key: Key('tab_summary'),
                height: 40,
                width: (MediaQuery.of(context).size.width - 64) / 2,
                child: Center(child: Text("Info Kas"))),
          ]),
    );
  }

  Widget _infoPortofolio() {
    return BlocBuilder<ProfileBallanceBloc, ProfileBallanceState>(
        builder: (context, stateBallance) {
      var asset;
      var availableCost;
      var stockValue;
      if (stateBallance is ProfileBallanceLoaded) {
        asset = stateBallance.profileBallance.data.totalSaldo +
            stateBallance.profileBallance.data.totalSaham;
        availableCost = stateBallance.profileBallance.data.totalSaldo;
        stockValue = stateBallance.profileBallance.data.totalSaham;
      }
      return BlocBuilder<PendingTransactionBloc, PendingTransactionState>(
        builder: (context, statePending) {
          var pendingBuy;
          var pendingSell;
          if (statePending is PendingTransactionLoaded) {
            pendingBuy = statePending.pendingTransaction.data.buy;
            pendingSell = statePending.pendingTransaction.data.sell;
          }
          return Container(
            margin: EdgeInsets.all(16),
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: Color(0xFFDADADA)),
                borderRadius: BorderRadius.circular(6),
                color: Color(0xFFF9FCFF)),
            child: Column(
              children: <Widget>[
                // aset balance
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Text(
                        "Asset Balance",
                        style: TextStyle(fontSize: 12),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: GestureDetector(
                          child: Icon(
                            Icons.info,
                            color: Color(0xFFDADADA),
                            size: 16,
                          ),
                          onTap: () {
                            PopupHelperMarket.showPopupInfoBallance(
                                context,
                                "Asset Balance",
                                "Asset Balance merupakan dana total yang menggabungkan antara dana rupiah Anda dengan nilai saham Anda",
                                asset == null
                                    ? ""
                                    : "Rp ${NumberFormatter.convertNumber(asset)}");
                          },
                        ),
                      )
                    ],
                  ),
                  subtitle: asset == null
                      ? CupertinoActivityIndicator()
                      : Text(
                          "Rp ${NumberFormatter.convertNumber(asset)}",
                          style: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF0E7E4A),
                              fontWeight: FontWeight.bold),
                        ),
                ),
                //
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              "Dana Tersedia",
                              style: TextStyle(fontSize: 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: GestureDetector(
                                child: Icon(
                                  Icons.info,
                                  color: Color(0xFFDADADA),
                                  size: 16,
                                ),
                                onTap: () {
                                  PopupHelperMarket.showPopupInfoBallance(
                                      context,
                                      "Dana Tersedia",
                                      "Jumlah dana rupiah Anda yang belum dialokasikan",
                                      availableCost == null
                                          ? ""
                                          : "Rp ${NumberFormatter.convertNumber(availableCost)}");
                                },
                              ),
                            )
                          ],
                        ),
                        subtitle: availableCost == null
                            ? CupertinoActivityIndicator()
                            : Text(
                                "Rp ${NumberFormatter.convertNumber(availableCost)}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                      ),
                    ),
                    Container(width: 1, height: 40, color: Colors.black),
                    Flexible(
                      child: ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Saham",
                              style: TextStyle(fontSize: 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: GestureDetector(
                                child: Icon(
                                  Icons.info,
                                  color: Color(0xFFDADADA),
                                  size: 16,
                                ),
                                onTap: () {
                                  PopupHelperMarket.showPopupInfoBallance(
                                      context,
                                      "Nilai Saham",
                                      "Nilai valuasi saham Anda, sesuai perkembangan harga dipasar sekunder",
                                      stockValue == null
                                          ? ""
                                          : "Rp ${NumberFormatter.convertNumber(stockValue)}");
                                },
                              ),
                            )
                          ],
                        ),
                        subtitle: stockValue == null
                            ? CupertinoActivityIndicator()
                            : Text(
                                "Rp ${NumberFormatter.convertNumber(stockValue)}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                      ),
                    ),
                  ],
                ),
                //
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              "Beli Pending",
                              style: TextStyle(fontSize: 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: GestureDetector(
                                child: Icon(
                                  Icons.info,
                                  color: Color(0xFFDADADA),
                                  size: 16,
                                ),
                                onTap: () {
                                  PopupHelperMarket.showPopupInfoBallance(
                                      context,
                                      "Beli Pending",
                                      "Jumlah dana pembelian yang telah Anda transaksikan namun masih dalam antrian order",
                                      pendingBuy == null
                                          ? ""
                                          : "Rp ${NumberFormatter.convertNumber(pendingBuy)}");
                                },
                              ),
                            )
                          ],
                        ),
                        subtitle: pendingBuy == null
                            ? CupertinoActivityIndicator()
                            : Text(
                                "Rp ${NumberFormatter.convertNumber(pendingBuy)}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                      ),
                    ),
                    Container(width: 1, height: 40, color: Colors.black),
                    Flexible(
                      child: ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(
                              "Jual Pending",
                              style: TextStyle(fontSize: 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: GestureDetector(
                                child: Icon(
                                  Icons.info,
                                  color: Color(0xFFDADADA),
                                  size: 16,
                                ),
                                onTap: () {
                                  PopupHelperMarket.showPopupInfoBallance(
                                      context,
                                      "Jual Pending",
                                      "Jumlah dana penjualan yang telah Anda transaksikan namun masih dalam antrian order",
                                      pendingSell == null
                                          ? ""
                                          : "Rp ${NumberFormatter.convertNumber(pendingSell)}");
                                },
                              ),
                            )
                          ],
                        ),
                        subtitle: pendingSell == null
                            ? CupertinoActivityIndicator()
                            : Text(
                                "Rp ${NumberFormatter.convertNumber(pendingSell)}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      );
    });
  }

  Widget _listPortofolio() {
    return BlocBuilder<PortofolioListBloc, PortofolioListState>(
      builder: (context, state) {
        List<Portofolio> portofolioList;
        if (state is PortofolioListLoaded) {
          portofolioList = state.portofolioList;
          lastPage = state.lastPage;
          page = state.currentPage;
        }
        if (state is PortofolioListLoading) {
          return ListView.separated(
            physics: ClampingScrollPhysics(),
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            itemCount: 3,
            separatorBuilder: (_, i) {
              return Container(height: 12);
            },
            itemBuilder: (_, i) {
              return Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[400],
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[400],
                        borderRadius: BorderRadius.circular(6)),
                    height: 90,
                    width: MediaQuery.of(context).size.width,
                  ));
            },
          );
        } else {
          if (portofolioList == null || portofolioList.length == 0) {
            return Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(height: 12),
                  Image.asset(
                    "assets/icon/no-result.png",
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text("Portofolio Kosong",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                  Text("Anda belum memiliki saham")
                ],
              ),
            );
          } else {
            return Column(
              children: [
                ListView.separated(
                  padding: EdgeInsets.all(16),
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: portofolioList.length,
                  itemBuilder: (_, i) {
                    return _itemList(
                        Key('item_portofolio_${portofolioList[i].codeEmiten}'),
                        portofolioList[i]);
                  },
                  separatorBuilder: (_, i) {
                    return Container(
                      height: 8,
                    );
                  },
                ),
                page < lastPage
                    ? ListView.separated(
                        physics: ClampingScrollPhysics(),
                        padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                        shrinkWrap: true,
                        itemCount: 2,
                        separatorBuilder: (_, i) {
                          return Container(height: 12);
                        },
                        itemBuilder: (_, i) {
                          return Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[400],
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[400],
                                    borderRadius: BorderRadius.circular(6)),
                                height: 90,
                                width: MediaQuery.of(context).size.width,
                              ));
                        },
                      )
                    : Container(),
              ],
            );
          }
        }
      },
    );
  }

  Widget _itemList(Key key, Portofolio data) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${data.codeEmiten}");
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => DetailPortofolioView(
                    portofolio: data,
                    isAvailable: data.tradeable,
                    isInOrder: data.status == "Edit",
                  ))),
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Color(0xFFDDDDDD))),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(data.codeEmiten),
                      data.tradeable // != "REJECTED"
                          ? Container()
                          : Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: GestureDetector(
                                child: Icon(Icons.info,
                                    color: Color(0xFFF28A8A), size: 14),
                                onTap: () {
                                  PopupHelperMarket.showTooltip(context, null,
                                      "Penerbit ini belum masuk dipasar sekunder. Penerbit di papan utama Santara bisa masuk ke pasar sekunder minimal 1 tahun setelah listing dipapan utama dan memenuhi persyaratan dokumen.");
                                },
                              ),
                            )
                    ],
                  ),
                  Container(height: 4),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Market Value", style: TextStyle(fontSize: 12)),
                          Text(NumberFormatter.convertNumber(data.marketValue),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold))
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Last Price", style: TextStyle(fontSize: 12)),
                          StreamBuilder<Object>(
                              stream: query.snapshots(),
                              builder: (context, stream) {
                                QuerySnapshot querySnapshot = stream.data;
                                var snapshot;
                                if (stream.connectionState ==
                                    ConnectionState.active) {
                                  if (querySnapshot.docs.length > 0) {
                                    snapshot = querySnapshot.docs[0].data();
                                  }
                                }
                                return Text(
                                    NumberFormatter.convertNumber(
                                        snapshot == null
                                            ? data.lastPrice
                                            : snapshot["price"]),
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold));
                              }),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Pot G/L", style: TextStyle(fontSize: 12)),
                          Text(NumberFormatter.convertNumber(data.potGl),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold))
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Icon(
                Icons.chevron_right,
                color: Color(0xFFC6C6C6),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _infoKas(
      List<DateTime> initialDate, DateTime currentDate, Summary summary) {
    String date = initialDate[0] == initialDate[1]
        ? DateFormatter.ddMMMMyyyy(initialDate[0])
        : "${DateFormatter.ddMMMMyyyy(initialDate[0])} - ${DateFormatter.ddMMMMyyyy(initialDate[1])}";
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(children: [
        _dateRangePicker(initialDate, currentDate),
        ListTile(
          contentPadding: EdgeInsets.all(10),
          title: Text("Info Kas",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          subtitle: Text(
              "Menu ini merupakan ringkasan dari info kas (kas masuk dan kas keluar) pengguna pada tanggal $date",
              style: TextStyle(fontSize: 12)),
        ),
        _summaryItem(
            DateFormatter.ddMMMMyyyy(initialDate[0]) ==
                        DateFormatter.ddMMMMyyyy(currentDate) &&
                    DateFormatter.ddMMMMyyyy(initialDate[1]) ==
                        DateFormatter.ddMMMMyyyy(currentDate)
                ? "Deposit Hari Ini"
                : "Total Deposit",
            summary.data.deposit),
        _summaryItem(
            DateFormatter.ddMMMMyyyy(initialDate[0]) ==
                        DateFormatter.ddMMMMyyyy(currentDate) &&
                    DateFormatter.ddMMMMyyyy(initialDate[1]) ==
                        DateFormatter.ddMMMMyyyy(currentDate)
                ? "Penarikan Hari Ini"
                : "Total Penarikan",
            summary.data.withdraw),
        _summaryItem(
            DateFormatter.ddMMMMyyyy(initialDate[0]) ==
                        DateFormatter.ddMMMMyyyy(currentDate) &&
                    DateFormatter.ddMMMMyyyy(initialDate[1]) ==
                        DateFormatter.ddMMMMyyyy(currentDate)
                ? "Transaksi Hari Ini"
                : "Total Transaksi",
            summary.data.trxMarket),
      ]),
    );
  }

  Widget _dateRangePicker(List<DateTime> initialDate, currentDate) {
    return Row(
      children: [
        Expanded(
          child: SantaraMainButtonWithIcon(
            key: Key('start_date_summary'),
            icon: Icon(
              Icons.calendar_today,
              color: Color(0xFF727272),
            ),
            name: DateFormat("dd MMM yyyy").format(initialDate[0]),
            textColor: Color(0xFF727272),
            padding: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(width: 1, color: Color(0xFFDDDDDD))),
            mainAxisAlignment: MainAxisAlignment.start,
            onTap: () async {
              await _pickDateRange(initialDate[0], initialDate[1], currentDate);
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Text("-"),
        ),
        Expanded(
          child: SantaraMainButtonWithIcon(
            key: Key('end_date_summary'),
            icon: Icon(
              Icons.calendar_today,
              color: Color(0xFF727272),
            ),
            name: DateFormat("dd MMM yyyy").format(initialDate[1]),
            textColor: Color(0xFF727272),
            padding: EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(width: 1, color: Color(0xFFDDDDDD))),
            mainAxisAlignment: MainAxisAlignment.start,
            onTap: () async {
              await _pickDateRange(initialDate[0], initialDate[1], currentDate);
            },
          ),
        )
      ],
    );
  }

  Widget _summaryItem(String title, double value) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
            Text(RupiahFormatter.initialValueFormat(value.toString()),
                style: TextStyle(
                    color: value < 0 ? Color(0xFFEB5255) : Color(0xFF0E7E4A)))
          ],
        ));
  }
}
