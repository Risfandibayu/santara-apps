import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';

class CaraKerjaView extends StatefulWidget {
  @override
  _CaraKerjaViewState createState() => _CaraKerjaViewState();
}

class _CaraKerjaViewState extends State<CaraKerjaView> {
  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    await _trailsServices.createTrails(
        event: marketTrailsData["how_it_works_view_success"]["event"],
        note: marketTrailsData["how_it_works_view_success"]["note"]);
  }

  @override
  void initState() {
    super.initState();
    initMarketTrails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text("Cara Kerja", style: TextStyle(color: Colors.black)),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 8),
        children: [
          _caraKerja(
              key: Key('cara_kerja_beli'),
              title: "Pembelian",
              child: _content(
                  content1: "Transaksi Beli",
                  content2:
                      "Transaksi beli Anda diproses saat permintaan yang Anda inputkan sesuai dengan penawaran yang ada di orderbook dan selanjutnya dinyatakan berhasil setelah dana dalam wallet anda telah berkurang sesuai dengan transaksi yang Anda buat dan kepemilikan saham Anda terima.",
                  content3: "Prioritas Transaksi Beli",
                  content4:
                      "Saat terjadi transaksi pembelian saham :\n- Yang akan mendapatkan prioritas untuk diproses lebih dulu adalah transaksi dengan harga terbesar\n- Jika terdapat kasus harga sama, maka transaksi yang diinputkan lebih awal menjadi prioritas.\n- Jika harga dan waktu sama, maka jumlah pembelian yang lebih besar akan mendapatkan prioritas.")),
          _caraKerja(
              key: Key('cara_kerja_jual'),
              title: "Penjualan",
              child: _content(
                  content1: "Transaksi Jual",
                  content2:
                      "Transaksi jual Anda diproses saat penawaran yang Anda inputkan sesuai dengan permintaan yang ada di orderbook dan selanjutnya dinyatakan berhasil setelah dana dalam wallet anda telah bertambah sesuai dengan transaksi yang Anda buat dan kepemilikan saham Anda dialihkan.",
                  content3: "Prioritas Transaksi Jual",
                  content4:
                      "Saat terjadi transaksi penjualan saham :\n- Yang akan mendapatkan prioritas untuk diproses lebih dulu adalah transaksi dengan harga terkecil\n- Jika terdapat kasus harga sama, maka transaksi yang diinputkan lebih awal menjadi prioritas.\n- Jika harga dan waktu sama, maka jumlah penjualan yang lebih besar akan mendapatkan prioritas.")),
        ],
      ),
    );
  }

  Widget _caraKerja({Key key, String title, Widget child}) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      scrollOnExpand: false,
      scrollOnCollapse: true,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(width: 1, color: Color(0xFFDADADA))),
        child: Column(
          children: <Widget>[
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                header: Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      title,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    )),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: child,
                    ),
                    Container(
                      height: 10,
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _content({content1, content2, content3, content4}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          content1,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        Container(height: 4),
        Text(content2),
        Container(height: 16),
        Text(
          content3,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        Container(height: 4),
        Text(content4),
      ],
    );
  }
}
