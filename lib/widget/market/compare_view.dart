import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:linked_scroll_controller/linked_scroll_controller.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/blocs/market/compare_fundamental_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/fundamental_list_model.dart';
import 'package:santaraapp/utils/strings.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/check_today_widget.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/helpers/market/page_scroll_physics_with_two_item.dart';

class CompareView extends StatefulWidget {
  final List<String> data;
  CompareView({this.data});
  @override
  _CompareViewState createState() => _CompareViewState();
}

class _CompareViewState extends State<CompareView> {
  ScrollController _main;
  ScrollController _lastPrice;
  ScrollController _changePrice;
  ScrollController _available;
  ScrollController _ytd;
  ScrollController _gps;
  ScrollController _opm;
  ScrollController _npm;
  ScrollController _marketCap;
  ScrollController _per;
  ScrollController _bvps;
  ScrollController _pbv;
  ScrollController _roa;
  ScrollController _roe;
  ScrollController _roi;
  ScrollController _ebitda;
  ScrollController _debt;
  LinkedScrollControllerGroup _controllers;

  CompareFundamentalBloc _compareBloc;
  CheckTodayBloc _checkTodayBloc;
  FlutterSecureStorage storage = FlutterSecureStorage();
  var token;

  Future _checkLogin() async {
    token = await storage.read(key: 'token');
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
    UnauthorizeUsecase.check(context);
    _compareBloc = BlocProvider.of<CompareFundamentalBloc>(context);
    _checkTodayBloc = BlocProvider.of<CheckTodayBloc>(context);
    _compareBloc.add(CompareFundamentalRequested(
        listCode: widget.data
            .toString()
            .replaceAll("[", "")
            .replaceAll("]", "")
            .replaceAll(" ", "")));
    _controllers = LinkedScrollControllerGroup();
    _main = _controllers.addAndGet();
    _lastPrice = _controllers.addAndGet();
    _changePrice = _controllers.addAndGet();
    _available = _controllers.addAndGet();
    _ytd = _controllers.addAndGet();
    _gps = _controllers.addAndGet();
    _opm = _controllers.addAndGet();
    _npm = _controllers.addAndGet();
    _marketCap = _controllers.addAndGet();
    _per = _controllers.addAndGet();
    _bvps = _controllers.addAndGet();
    _pbv = _controllers.addAndGet();
    _roa = _controllers.addAndGet();
    _roe = _controllers.addAndGet();
    _roi = _controllers.addAndGet();
    _ebitda = _controllers.addAndGet();
    _debt = _controllers.addAndGet();
  }

  @override
  void dispose() {
    super.dispose();
    _main.dispose();
    _lastPrice.dispose();
    _changePrice.dispose();
    _available.dispose();
    _ytd.dispose();
    _gps.dispose();
    _opm.dispose();
    _npm.dispose();
    _marketCap.dispose();
    _per.dispose();
    _bvps.dispose();
    _pbv.dispose();
    _roa.dispose();
    _roe.dispose();
    _roi.dispose();
    _ebitda.dispose();
    _debt.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CheckTodayWidget(
        checkTodayBloc: _checkTodayBloc,
        builder: (context, state) {
          return ModalProgressHUD(
            inAsyncCall: state is CheckTodayLoading,
            color: Colors.black,
            opacity: 0.2,
            progressIndicator: CupertinoActivityIndicator(),
            child: Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(color: Colors.black),
                  title: Text("Tabel Pembanding",
                      style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                ),
                body: BlocBuilder<CompareFundamentalBloc,
                    CompareFundamentalState>(builder: (context, state) {
                  if (state is CompareFundamentalLoaded) {
                    List<Fundamental> data = state.fundamentalList.data;
                    return Column(children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: SantaraAppBetaTesting(),
                      ),
                      //main
                      _itemToCompare(
                          height: 120,
                          controller: _main,
                          itemCount: data.length,
                          itemBuilder: (_, i) {
                            return _titleSaham(data, i);
                          }),
                      Expanded(
                        child: ListView(
                          children: [
                            _mainTitle("Kondisi Saham"),
                            divider,
                            _secondaryTitle("Harga Terakhir (Last Price)",
                                GlossariumMarket.lastPrice),
                            _itemToCompare(
                                height: 35,
                                controller: _lastPrice,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  Query query = FirebaseFirestore.instance
                                      .collection(
                                          "current_price_${data[i].codeEmiten}");
                                  return StreamBuilder<Object>(
                                      stream: query.snapshots(),
                                      builder: (context, stream) {
                                        QuerySnapshot querySnapshot =
                                            stream.data;
                                        var snapshot;
                                        if (stream.connectionState ==
                                            ConnectionState.active) {
                                          if (querySnapshot.docs.length > 0) {
                                            snapshot =
                                                querySnapshot.docs[0].data();
                                          }
                                        }
                                        return _content("Rp " +
                                            NumberFormatter.convertNumber(
                                                snapshot == null
                                                    ? data[i].price
                                                    : snapshot["price"]));
                                      });
                                }),
                            _secondaryTitle(
                                "Perubahan Harga", GlossariumMarket.change),
                            _itemToCompare(
                                height: 35,
                                controller: _changePrice,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  Query query = FirebaseFirestore.instance
                                      .collection(
                                          "current_price_${data[i].codeEmiten}");
                                  return StreamBuilder<Object>(
                                      stream: query.snapshots(),
                                      builder: (context, stream) {
                                        QuerySnapshot querySnapshot =
                                            stream.data;
                                        var snapshot;
                                        if (stream.connectionState ==
                                            ConnectionState.active) {
                                          if (querySnapshot.docs.length > 0) {
                                            snapshot =
                                                querySnapshot.docs[0].data();
                                          }
                                        }
                                        return _content(snapshot == null
                                            ? "Rp 0"
                                            : "Rp ${NumberFormatter.convertNumberDecimal(snapshot['price'] - snapshot['prev'])}");
                                      });
                                }),
                            _secondaryTitle("Saham Tersedia",
                                "Lembar saham yang tersedia dalam orderbook saat ini."),
                            _itemToCompare(
                                height: 35,
                                controller: _available,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  Query orderbook = FirebaseFirestore.instance
                                      .collection(
                                          "orderbooks_${data[i].codeEmiten}")
                                      .where("type", isEqualTo: "sell")
                                      .where("status", isEqualTo: false);
                                  return StreamBuilder<Object>(
                                      stream: orderbook.snapshots(),
                                      builder: (context, stream) {
                                        QuerySnapshot querySnapshot =
                                            stream.data;
                                        var total = 0;
                                        if (stream.connectionState ==
                                            ConnectionState.active) {
                                          for (var i = 0;
                                              i < querySnapshot.docs.length;
                                              i++) {
                                            total += querySnapshot.docs[i]
                                                    .data()["amount"]
                                                    .toInt() *
                                                querySnapshot.docs[i]
                                                    .data()["price"]
                                                    .toInt();
                                          }
                                        }
                                        return _content(NumberFormatter
                                            .convertNumberDecimal(total));
                                      });
                                }),
                            divider,
                            _mainTitle("Fundamental"),
                            divider,
                            // Sales Growth (YtD)
                            _secondaryTitle("Sales Growth (YtD)",
                                GlossariumMarket.salesGrowth),
                            _itemToCompare(
                                height: 35,
                                controller: _ytd,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].sales));
                                }),
                            // GPS
                            _secondaryTitle("GPS", GlossariumMarket.gps),
                            _itemToCompare(
                                height: 35,
                                controller: _gps,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].gps) +
                                          "%");
                                }),
                            // OPM
                            _secondaryTitle("OPM", GlossariumMarket.opm),
                            _itemToCompare(
                                height: 35,
                                controller: _opm,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].opm) +
                                          "%");
                                }),
                            // NPM
                            _secondaryTitle("NPM", GlossariumMarket.npm),
                            _itemToCompare(
                                height: 35,
                                controller: _npm,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].npm) +
                                          "%");
                                }),
                            // Market Cap
                            _secondaryTitle(
                                "Market Cap", GlossariumMarket.marketCap),
                            _itemToCompare(
                                height: 35,
                                controller: _marketCap,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].marketCap));
                                }),
                            // PER
                            _secondaryTitle("PER", GlossariumMarket.per),
                            _itemToCompare(
                                height: 35,
                                controller: _per,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].per));
                                }),
                            // BVPS
                            _secondaryTitle("BVPS", GlossariumMarket.bvps),
                            _itemToCompare(
                                height: 35,
                                controller: _bvps,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].bpvs));
                                }),
                            // PBV
                            _secondaryTitle("PBV", GlossariumMarket.pbv),
                            _itemToCompare(
                                height: 35,
                                controller: _pbv,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].pbv));
                                }),
                            // ROA
                            _secondaryTitle("ROA", GlossariumMarket.roa),
                            _itemToCompare(
                                height: 35,
                                controller: _roa,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].roa) +
                                          "%");
                                }),
                            // ROE
                            _secondaryTitle("ROE", GlossariumMarket.roe),
                            _itemToCompare(
                                height: 35,
                                controller: _roe,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].roe) +
                                          "%");
                                }),
                            // ROI
                            // _secondaryTitle("ROI", GlossariumMarket.roi),
                            // _itemToCompare(
                            //     height: 35,
                            //     controller: _roi,
                            //     itemCount: data.length,
                            //     itemBuilder: (_, i) {
                            //       return _content(
                            //           data[i].roi.toStringAsFixed(2) + "%");
                            //     }),
                            // EV/EBITDA
                            _secondaryTitle(
                                "EV/EBITDA", GlossariumMarket.ebitda),
                            _itemToCompare(
                                height: 35,
                                controller: _ebitda,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                          data[i].ev));
                                }),
                            // Debt to Equity Ratio
                            _secondaryTitle(
                                "Debt to Equity Ratio", GlossariumMarket.der),
                            _itemToCompare(
                                height: 35,
                                controller: _debt,
                                itemCount: data.length,
                                itemBuilder: (_, i) {
                                  return _content(
                                      NumberFormatter.convertNumberDecimal(
                                              data[i].der) +
                                          "%");
                                }),
                          ],
                        ),
                      ),
                    ]);
                  } else {
                    return Container(
                      child: Center(
                        child: CupertinoActivityIndicator(),
                      ),
                    );
                  }
                })),
          );
        });
  }

  get divider {
    return Container(
      height: 1,
      width: MediaQuery.of(context).size.width,
      color: Color(0xFFDADADA),
    );
  }

  Widget _titleSaham(List<Fundamental> data, int i) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${data[i].codeEmiten}");
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: Color(0xFFF6FAFF),
          border: Border.symmetric(
              vertical: BorderSide(width: 1, color: Color(0xFFDADADA)),
              horizontal: BorderSide(width: 0.5, color: Color(0xFFDADADA)))),
      width: MediaQuery.of(context).size.width / 2,
      child: StreamBuilder<Object>(
          stream: query.snapshots(),
          builder: (context, stream) {
            QuerySnapshot querySnapshot = stream.data;
            var snapshot;
            if (stream.connectionState == ConnectionState.active) {
              if (querySnapshot.docs.length > 0) {
                snapshot = querySnapshot.docs[0].data();
              }
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(data[i].codeEmiten,
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        stream.connectionState == ConnectionState.waiting
                            ? Center(child: CupertinoActivityIndicator())
                            : stream.hasError
                                ? Container()
                                : snapshot == null
                                    ? Padding(
                                        padding: const EdgeInsets.only(
                                            left: 4, right: 4),
                                        child: Icon(MdiIcons.circleDouble,
                                            color: Colors.orange, size: 16),
                                      )
                                    : snapshot["price"] > snapshot["prev"]
                                        ? Icon(Icons.arrow_drop_up,
                                            color: Colors.green)
                                        : snapshot["price"] < snapshot["prev"]
                                            ? Icon(
                                                Icons.arrow_drop_down,
                                                color: Colors.red,
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 4, right: 4),
                                                child: Icon(
                                                    MdiIcons.circleDouble,
                                                    color: Colors.orange,
                                                    size: 16),
                                              ),
                        Text(
                            stream.connectionState == ConnectionState.active
                                ? snapshot == null
                                    ? "0.0 %"
                                    : "${((snapshot["price"] - snapshot["prev"]) / snapshot["prev"] * 100).toStringAsFixed(1)} %"
                                : "",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: snapshot == null
                                    ? Colors.orange
                                    : snapshot["price"] > snapshot["prev"]
                                        ? Colors.green
                                        : snapshot["price"] < snapshot["prev"]
                                            ? Colors.red
                                            : Colors.orange)),
                      ],
                    )),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4),
                  child: Text(
                    data[i].companyName,
                    style: TextStyle(fontSize: 10, color: Color(0xFF676767)),
                  ),
                ),
                SantaraMainButton(
                  key: Key('beli_btn_in_compare_view'),
                  child: Text(
                    "Beli",
                    style: TextStyle(color: Colors.white),
                  ),
                  onTap: () {
                    if (token == null) {
                      PopupHelperMarket.needLoginToAccessThisPage(
                          context: context, isGoBack: 0);
                    } else {
                      _checkTodayBloc.add(CheckTodayRequested(
                          price: snapshot == null
                              ? data[i].price
                              : snapshot["price"],
                          type: TransactionType.beli,
                          uuid: data[i].emitenUuid));
                    }
                  },
                  height: 36,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Color((0xFF0E7E4A))),
                ),
              ],
            );
          }),
    );
  }

  Widget _itemToCompare(
      {double height,
      ScrollController controller,
      int itemCount,
      Function(BuildContext, int) itemBuilder}) {
    return Container(
      height: height,
      child: ListView.builder(
          physics: PageScrollPhysicsWithTwoItem(),
          addSemanticIndexes: true,
          shrinkWrap: true,
          controller: controller,
          scrollDirection: Axis.horizontal,
          itemCount: itemCount,
          itemBuilder: itemBuilder),
    );
  }

  Widget _mainTitle(String title) {
    return Container(
      height: 50,
      child: Center(
          child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold),
      )),
    );
  }

  Widget _secondaryTitle(String title, String desc) {
    return Container(
      height: 35,
      color: Color(0xFFF7FAFF),
      child: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title),
          Container(width: 4),
          GestureDetector(
            onTap: () {
              PopupHelperMarket.showTooltip(context, null, desc);
            },
            child: Icon(
              Icons.info,
              size: 14,
              color: Color(0xFFDADADA),
            ),
          )
        ],
      )),
    );
  }

  Widget _content(String value) {
    return Container(
      height: 35,
      width: MediaQuery.of(context).size.width / 2,
      child: Center(
          child: Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold),
      )),
    );
  }
}
