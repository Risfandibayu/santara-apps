import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/blocs/market/emiten_list_all_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:santaraapp/widget/market/compare_view.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/check_today_widget.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/widget/widget/components/market/info_price.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/market/table_pasar_sekunder.dart'
    as table;
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:shimmer/shimmer.dart';

class OrderbookView extends StatefulWidget {
  @override
  _OrderbookViewState createState() => _OrderbookViewState();
}

class _OrderbookViewState extends State<OrderbookView> {
  final _search = TextEditingController();
  bool _bandingkan = false;
  bool isSearching = false;
  List<String> _sahamBandingkan = [];
  EmitenListAllBloc _listAllBloc;
  CheckTodayBloc _checkTodayBloc;
  ScrollController _scroll;
  int page = 1;
  int lastPage = 0;

  FlutterSecureStorage storage = FlutterSecureStorage();
  var token;

  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    await _trailsServices.createTrails(
        event: marketTrailsData["orderbook_view_success"]["event"],
        note: marketTrailsData["orderbook_view_success"]["note"]);
  }

  Future _checkLogin() async {
    token = await storage.read(key: 'token');
  }

  _scrollListener() {
    if (_scroll.offset >= _scroll.position.maxScrollExtent &&
        !_scroll.position.outOfRange) {
      if (lastPage != null && page < lastPage) {
        page = page + 1;
        _listAllBloc.add(EmitenListAllPagination(page: page, key: ""));
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
    UnauthorizeUsecase.check(context);
    initMarketTrails();
    _listAllBloc = BlocProvider.of<EmitenListAllBloc>(context);
    _checkTodayBloc = BlocProvider.of<CheckTodayBloc>(context);
    _scroll = ScrollController();
    _scroll.addListener(_scrollListener);
    _listAllBloc.add(EmitenListAllRequested(group: "", sort: 0, key: ""));
  }

  @override
  Widget build(BuildContext context) {
    return CheckTodayWidget(
        checkTodayBloc: _checkTodayBloc,
        builder: (context, state) {
          return ModalProgressHUD(
              inAsyncCall: state is CheckTodayLoading,
              color: Colors.black,
              opacity: 0.2,
              progressIndicator: CupertinoActivityIndicator(),
              child: Scaffold(
                appBar: isSearching
                    ? AppBar(
                        titleSpacing: 0,
                        automaticallyImplyLeading: false,
                        title: Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: TextField(
                            textInputAction: TextInputAction.search,
                            controller: _search,
                            onSubmitted: (value) {
                              if (value.isNotEmpty) {
                                _listAllBloc.add(EmitenListAllRequested(
                                    group: "", sort: 0, key: value));
                              } else {
                                ToastHelper.showFailureToast(context,
                                    "Kata kunci pencarian tidak boleh kosong");
                              }
                            },
                            onChanged: (value) {
                              if (value == "") {
                                if (state is EmitenListAllLoaded) {
                                } else {
                                  _listAllBloc.add(EmitenListAllRequested(
                                      group: "", sort: 0, key: ""));
                                }
                              }
                            },
                            decoration: InputDecoration(
                                hintText: "Cari Nama Bisnis",
                                hintStyle: TextStyle(color: Color(0xff676767)),
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 20),
                                fillColor: Color(0xffF2F2F2),
                                border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(40.0),
                                    ),
                                    borderSide:
                                        BorderSide(color: Color(0xffDDDDDD))),
                                suffixIcon: IconButton(
                                    padding: EdgeInsets.only(right: 16),
                                    color: Colors.black,
                                    icon: Icon(Icons.search),
                                    onPressed: () {
                                      if (_search.text.isNotEmpty) {
                                        _listAllBloc.add(EmitenListAllRequested(
                                            group: "",
                                            sort: 0,
                                            key: _search.text));
                                      } else {
                                        if (state is EmitenListAllLoaded) {
                                        } else {
                                          _listAllBloc.add(
                                              EmitenListAllRequested(
                                                  group: "", sort: 0, key: ""));
                                        }
                                      }
                                    })),
                          ),
                        ),
                        backgroundColor: Colors.white,
                        leading: IconButton(
                          icon: Icon(Icons.close),
                          color: Colors.black,
                          onPressed: () {
                            _search.clear();
                            if (state is EmitenListAllLoaded) {
                            } else {
                              _listAllBloc.add(EmitenListAllRequested(
                                  group: "", sort: 0, key: ""));
                            }
                            setState(() => isSearching = false);
                          },
                        ))
                    : AppBar(
                        backgroundColor: Colors.white,
                        iconTheme: IconThemeData(color: Colors.black),
                        title: Text("Orderbook",
                            style: TextStyle(color: Colors.black)),
                        centerTitle: true,
                        actions: [
                          IconButton(
                              icon: Icon(Icons.search),
                              onPressed: () {
                                setState(() => isSearching = true);
                              })
                        ],
                      ),
                body: ListView(
                  controller: _scroll,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: SantaraAppBetaTesting(),
                    ),
                    _bandingkan
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(16, 20, 16, 0),
                            child: Column(children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child:
                                    Text("Pilih penerbit untuk dibandingkan"),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  SantaraSecondaryButton(
                                    key: Key('hide_menu_bandingkan'),
                                    height: 40,
                                    width: 40,
                                    child: Icon(
                                      Icons.close,
                                      color: Color(0xFFD23737),
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(4),
                                        border: Border.all(
                                            width: 1,
                                            color: Color(0xFFD23737))),
                                    onTap: () {
                                      setState(() {
                                        _bandingkan = false;
                                        _sahamBandingkan.clear();
                                      });
                                    },
                                  ),
                                  Container(width: 16),
                                  SantaraMainButton(
                                    key: Key('go_to_compare_page'),
                                    height: 40,
                                    width:
                                        MediaQuery.of(context).size.width / 3,
                                    child: Text("Lanjutkan",
                                        style: TextStyle(color: Colors.white)),
                                    onTap: () {
                                      if (_sahamBandingkan.length < 2) {
                                        ToastHelper.showFailureToast(
                                            context, "Minimal memilih 2 saham");
                                      } else {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) => CompareView(
                                                    data: _sahamBandingkan)));
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ]),
                          )
                        : Padding(
                            padding: const EdgeInsets.fromLTRB(16, 20, 16, 0),
                            child: Center(
                              child: SantaraSecondaryButton(
                                key: Key('show_menu_bandingkan'),
                                height: 40,
                                child: Text("Bandingkan Saham",
                                    style: TextStyle(color: Color(0xFFD23737))),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    border: Border.all(
                                        width: 1, color: Color(0xFFD23737))),
                                onTap: () {
                                  setState(() => _bandingkan = true);
                                },
                              ),
                            ),
                          ),
                    BlocConsumer<EmitenListAllBloc, EmitenListAllState>(
                      listener: (context, state) {
                        if (state is EmitenListAllLoaded) {
                          lastPage = state.lastPage;
                        }
                      },
                      builder: (context, state) {
                        final currentState = state;
                        List<Emiten> data = currentState.props[2];
                        if (state is EmitenListAllLoading) {
                          return ListView.separated(
                            physics: ClampingScrollPhysics(),
                            padding: EdgeInsets.all(16),
                            shrinkWrap: true,
                            itemCount: 3,
                            separatorBuilder: (_, i) {
                              return Container(height: 8);
                            },
                            itemBuilder: (_, i) {
                              return Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[400],
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey[400],
                                        borderRadius: BorderRadius.circular(6)),
                                    height: 180,
                                    width: MediaQuery.of(context).size.width,
                                  ));
                            },
                          );
                        } else {
                          return ListView.separated(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              padding: EdgeInsets.all(16),
                              itemBuilder: (_, i) {
                                return _itemOrderbook(data[i]);
                              },
                              separatorBuilder: (_, i) {
                                return Container(height: 8);
                              },
                              itemCount: data.length);
                        }
                      },
                    ),
                    BlocBuilder<EmitenListAllBloc, EmitenListAllState>(
                        builder: (context, state) {
                      if (state is EmitenListAllPaginationLoading) {
                        return ListView.separated(
                          physics: ClampingScrollPhysics(),
                          padding: EdgeInsets.all(16),
                          shrinkWrap: true,
                          itemCount: 1,
                          separatorBuilder: (_, i) {
                            return Container(height: 8);
                          },
                          itemBuilder: (_, i) {
                            return Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[400],
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[400],
                                      borderRadius: BorderRadius.circular(6)),
                                  height: 180,
                                  width: MediaQuery.of(context).size.width,
                                ));
                          },
                        );
                      } else {
                        return Container();
                      }
                    })
                  ],
                ),
              ));
        });
  }

  Widget _itemOrderbook(Emiten data) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${data.codeEmiten}");
    return StreamBuilder<QuerySnapshot>(
        stream: query.snapshots(),
        builder: (context, stream) {
          QuerySnapshot querySnapshot = stream.data;
          var snapshot;
          if (stream.connectionState == ConnectionState.active) {
            if (querySnapshot.docs.length > 0) {
              snapshot = querySnapshot.docs[0].data();
            }
          }
          return ExpandableNotifier(
              child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(width: 1, color: Color(0xFFC8C8C8))),
            child: ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: false,
              child: ExpandablePanel(
                theme: const ExpandableThemeData(
                    useInkWell: true,
                    hasIcon: false,
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToCollapse: true,
                    tapBodyToExpand: true),
                header: Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(data.codeEmiten,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold)),
                          Text(
                            data.trademark,
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(data.companyName,
                              style: TextStyle(
                                  color: Color(0xFF676767), fontSize: 14)),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text("Harga Terakhir",
                              style: TextStyle(fontSize: 12)),
                          stream.connectionState == ConnectionState.waiting
                              ? Center(child: CupertinoActivityIndicator())
                              : Text(
                                  "Rp " +
                                      NumberFormatter.convertNumber(
                                          snapshot == null
                                              ? data.currentPrice
                                              : snapshot["price"]),
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                          Row(
                            children: <Widget>[
                              stream.connectionState == ConnectionState.waiting
                                  ? Center(child: CupertinoActivityIndicator())
                                  : stream.hasError
                                      ? Container()
                                      : snapshot == null
                                          ? Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 4, right: 4),
                                              child: Icon(MdiIcons.circleDouble,
                                                  color: Colors.orange,
                                                  size: 16),
                                            )
                                          : snapshot["price"] > snapshot["prev"]
                                              ? Icon(Icons.arrow_drop_up,
                                                  color: Colors.green)
                                              : snapshot["price"] <
                                                      snapshot["prev"]
                                                  ? Icon(
                                                      Icons.arrow_drop_down,
                                                      color: Colors.red,
                                                    )
                                                  : Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 4,
                                                              right: 4),
                                                      child: Icon(
                                                          MdiIcons.circleDouble,
                                                          color: Colors.orange,
                                                          size: 16),
                                                    ),
                              Text(
                                  stream.connectionState !=
                                          ConnectionState.waiting
                                      ? snapshot == null
                                          ? "0.0 %"
                                          : "${((snapshot["price"] - snapshot["prev"]) / snapshot["prev"] * 100).toStringAsFixed(1)} %"
                                      : "",
                                  style: TextStyle(
                                      color: snapshot == null
                                          ? Colors.orange
                                          : snapshot["price"] > snapshot["prev"]
                                              ? Colors.green
                                              : snapshot["price"] <
                                                      snapshot["prev"]
                                                  ? Colors.red
                                                  : Colors.orange)),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                collapsed: Column(children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: table.TableJualBeliCollapse(
                          isJual: false,
                          prev: snapshot == null ? 0 : snapshot["prev"],
                          codeEmiten: data.codeEmiten,
                        ),
                      ),
                      Container(
                        width: 8,
                      ),
                      Flexible(
                        child: table.TableJualBeliCollapse(
                          isJual: true,
                          prev: snapshot == null ? 0 : snapshot["prev"],
                          codeEmiten: data.codeEmiten,
                        ),
                      ),
                    ],
                  ),
                  Container(height: 6),
                  Text(
                    "Lihat Selengkapnya",
                    style: TextStyle(
                      color: Color(0xFF666EE8),
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.all(10),
                      child: _bandingkan
                          ? _sahamBandingkan.contains(data.codeEmiten)
                              ? SantaraSecondaryButton(
                                  key: Key('btn_batal_bandingkan'),
                                  child: Text("Batalkan",
                                      style:
                                          TextStyle(color: Color(0xFF666EE8))),
                                  height: 40,
                                  onTap: () {
                                    setState(() {
                                      _sahamBandingkan.remove(data.codeEmiten);
                                    });
                                  },
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1, color: Color(0xFF666EE8)),
                                      borderRadius: BorderRadius.circular(4)),
                                )
                              : SantaraMainButton(
                                  key: Key('btn_pilih_bandingkan'),
                                  child: Text("Pilih",
                                      style: TextStyle(color: Colors.white)),
                                  height: 40,
                                  onTap: () {
                                    setState(() {
                                      if (_sahamBandingkan.length >= 3) {
                                        ToastHelper.showFailureToast(context,
                                            "Maksimal memilih 3 saham");
                                      } else {
                                        _sahamBandingkan.add(data.codeEmiten);
                                      }
                                    });
                                  },
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      color: Color(0xFF666EE8)),
                                )
                          : _buttonJualBeli(data, snapshot))
                ]),
                expanded: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: InfoPrice(
                          codeEmiten: data.codeEmiten,
                          price: snapshot == null
                              ? data.currentPrice
                              : snapshot["price"],
                          prev: snapshot == null
                              ? data.prevPrice
                              : snapshot["prev"]),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: table.TableJualBeli(
                              isJual: false,
                              prev: snapshot == null ? 0 : snapshot["prev"],
                              codeEmiten: data.codeEmiten),
                        ),
                        Container(
                          width: 8,
                        ),
                        Flexible(
                          child: table.TableJualBeli(
                              isJual: true,
                              prev: snapshot == null ? 0 : snapshot["prev"],
                              codeEmiten: data.codeEmiten),
                        ),
                      ],
                    ),
                    Icon(
                      Icons.keyboard_arrow_up,
                      color: Color(0xFF666EE8),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(10),
                        child: _bandingkan
                            ? _sahamBandingkan.contains(data.codeEmiten)
                                ? SantaraSecondaryButton(
                                    key: Key('btn_batal_bandingkan_expand'),
                                    child: Text("Batalkan",
                                        style: TextStyle(
                                            color: Color(0xFF666EE8))),
                                    height: 40,
                                    onTap: () {
                                      setState(() {
                                        _sahamBandingkan
                                            .remove(data.codeEmiten);
                                      });
                                    },
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1, color: Color(0xFF666EE8)),
                                        borderRadius: BorderRadius.circular(4)),
                                  )
                                : SantaraMainButton(
                                    key: Key('btn_pilih_bandingkan_expand'),
                                    child: Text("Pilih",
                                        style: TextStyle(color: Colors.white)),
                                    height: 40,
                                    onTap: () {
                                      setState(() {
                                        if (_sahamBandingkan.length >= 3) {
                                          ToastHelper.showFailureToast(context,
                                              "Maksimal memilih 3 saham");
                                        } else {
                                          _sahamBandingkan.add(data.codeEmiten);
                                        }
                                      });
                                    },
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(4),
                                        color: Color(0xFF666EE8)),
                                  )
                            : _buttonJualBeli(data, snapshot))
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                      theme: const ExpandableThemeData(crossFadePoint: 0),
                    ),
                  );
                },
              ),
            ),
          ));
        });
  }

  Widget _buttonJualBeli(Emiten data, dynamic snapshot) {
    return Row(
      children: [
        Expanded(
          child: SantaraMainButton(
            key: Key('btn_beli_in_orderbook_page'),
            child: Text("Beli Saham", style: TextStyle(color: Colors.white)),
            height: 40,
            onTap: () {
              if (token == null) {
                PopupHelperMarket.needLoginToAccessThisPage(
                    context: context, isGoBack: 0);
              } else {
                _checkTodayBloc.add(CheckTodayRequested(
                    type: TransactionType.beli,
                    uuid: data.uuid,
                    price: snapshot == null
                        ? data.currentPrice
                        : snapshot["price"]));
              }
            },
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Color(0xFF0E7E4A)),
          ),
        ),
        Container(width: 8),
        Expanded(
          child: SantaraMainButton(
            key: Key('btn_jual_in_orderbook_page'),
            child: Text("Jual Saham", style: TextStyle(color: Colors.white)),
            height: 40,
            onTap: () {
              if (data.isEmitenOwner) {
                if (token == null) {
                  PopupHelperMarket.needLoginToAccessThisPage(
                      context: context, isGoBack: 0);
                } else {
                  _checkTodayBloc.add(CheckTodayRequested(
                      type: TransactionType.jual,
                      uuid: data.uuid,
                      price: snapshot == null ? 0 : snapshot["price"]));
                }
              }
            },
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color:
                    data.isEmitenOwner ? Color(0xFFEB5255) : Color(0xFF8E8E8E)),
          ),
        ),
      ],
    );
  }
}
