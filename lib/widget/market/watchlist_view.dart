import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/blocs/market/emiten_list_all_bloc.dart';
import 'package:santaraapp/blocs/market/watchlist_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart' as el;
import 'package:santaraapp/models/market/watchlist_model.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/widget/widget/components/market/item_list_pasar_sekunder.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class WatchlistView extends StatefulWidget {
  @override
  _WatchlistViewState createState() => _WatchlistViewState();
}

class _WatchlistViewState extends State<WatchlistView> {
  WatchlistBloc _watchlistBloc;
  EmitenListAllBloc _listAllBloc;
  final _search = TextEditingController();
  bool isSearching = false;
  int page = 1;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  FlutterSecureStorage storage = FlutterSecureStorage();

  Future _checkLogin() async {
    await storage.read(key: 'token').then((value) {
      if (value == null) {
        PopupHelperMarket.needLoginToAccessThisPage(
            context: context, isGoBack: 1);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
    UnauthorizeUsecase.check(context);
    _watchlistBloc = BlocProvider.of<WatchlistBloc>(context);
    _listAllBloc = BlocProvider.of<EmitenListAllBloc>(context);
    _listAllBloc.add(EmitenListAllRequested(group: "", sort: 0, key: ""));
    _watchlistBloc.add(WatchlistRequested());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WatchlistBloc, WatchlistState>(
        listener: (context, state) {
      if (state is WatchlistAddSuccess) {
        ToastHelper.showSnackBarWithAction(
            context: context,
            key: _scaffoldKey,
            icon: Icon(
              Icons.favorite,
              color: Color(0xFFFF6464),
            ),
            message: state.msg,
            backgroundColor: Colors.black,
            duration: Duration(seconds: 3));
      }
      if (state is WatchlistDeleteSuccess) {
        ToastHelper.showSnackBarWithAction(
            context: context,
            key: _scaffoldKey,
            icon: Icon(
              Icons.favorite_border,
              color: Color(0xFFFF6464),
            ),
            message: state.msg,
            backgroundColor: Colors.black,
            duration: Duration(seconds: 3));
      }
      if (state is WatchlistActionFailure) {
        ToastHelper.showFailureToast(context, state.msg);
      }
    }, builder: (context, state) {
      WatchlistModel data;
      if (state is WatchlistLoaded) {
        data = state.watchlistModel;
      } else if (state is WatchlistSearchLoaded) {
        data = state.watchlistModel;
      } else if (state is WatchlistLoading) {
        data = state.watchlistModel;
      } else if (state is WatchlistAddSuccess) {
        data = state.watchlistModel;
      } else if (state is WatchlistDeleteSuccess) {
        data = state.watchlistModel;
      } else if (state is WatchlistActionLoading) {
        data = state.watchlistModel;
      } else if (state is SearchWatchlistError) {
        data = state.watchlistModel;
      }
      return Stack(children: [
        Scaffold(
            key: _scaffoldKey,
            appBar: isSearching
                ? AppBar(
                    titleSpacing: 0,
                    automaticallyImplyLeading: false,
                    title: Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: TextField(
                        textInputAction: TextInputAction.search,
                        controller: _search,
                        onSubmitted: (value) {
                          if (value.isNotEmpty) {
                            _watchlistBloc
                                .add(SearchWatchlistRequested(key: value));
                          } else {
                            if (state is WatchlistLoaded) {
                            } else {
                              _watchlistBloc.add(WatchlistRequested());
                            }
                          }
                        },
                        onChanged: (value) {
                          if (value == "") {
                            if (state is WatchlistLoaded) {
                            } else {
                              _watchlistBloc.add(WatchlistRequested());
                            }
                          }
                        },
                        decoration: InputDecoration(
                            hintText: "Cari Nama Bisnis",
                            hintStyle: TextStyle(color: Color(0xff676767)),
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 20),
                            fillColor: Color(0xffF2F2F2),
                            border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(40.0),
                                ),
                                borderSide:
                                    BorderSide(color: Color(0xffDDDDDD))),
                            suffixIcon: IconButton(
                                padding: EdgeInsets.only(right: 16),
                                color: Colors.black,
                                icon: Icon(Icons.search),
                                onPressed: () {
                                  if (_search.text.isNotEmpty) {
                                    _watchlistBloc.add(SearchWatchlistRequested(
                                        key: _search.text));
                                  } else {
                                    if (state is WatchlistLoaded) {
                                    } else {
                                      _watchlistBloc.add(WatchlistRequested());
                                    }
                                  }
                                })),
                      ),
                    ),
                    backgroundColor: Colors.white,
                    leading: IconButton(
                      icon: Icon(Icons.close),
                      color: Colors.black,
                      onPressed: () {
                        _search.clear();
                        if (state is WatchlistLoaded) {
                        } else {
                          _watchlistBloc.add(WatchlistRequested());
                        }
                        setState(() => isSearching = false);
                      },
                    ))
                : AppBar(
                    backgroundColor: Colors.white,
                    iconTheme: IconThemeData(color: Colors.black),
                    title: Text("Watchlist",
                        style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    actions: [
                      IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () {
                            setState(() => isSearching = true);
                          })
                    ],
                  ),
            body: SingleChildScrollView(
              padding: EdgeInsets.all(12),
              child: BlocConsumer<EmitenListAllBloc, EmitenListAllState>(
                listener: (context, state) {
                  if (state is EmitenListAllLoaded) {
                    if (page < state.lastPage) {
                      page += 1;
                      _listAllBloc.add(EmitenListAllPagination(page: page));
                    }
                  }
                },
                builder: (context, state2) {
                  List<el.Emiten> emiten;
                  if (state2 is EmitenListAllLoaded) {
                    emiten = state2.emitenList;
                  }
                  if (state2 is EmitenListSearchLoaded) {
                    emiten = state2.emitenList;
                  }
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      SantaraAppBetaTesting(),
                      GestureDetector(
                        onTap: () => _addBottomSheetWatchlist(emiten),
                        child: Container(
                            height: 36,
                            width: MediaQuery.of(context).size.width / 3,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(
                                    width: 1, color: Color(0xFFBF2D30))),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.add,
                                  size: 14,
                                  color: Color(0xFFBF2D30),
                                ),
                                Container(width: 6),
                                Text(
                                  "Tambah",
                                  style: TextStyle(color: Color(0xFFBF2D30)),
                                ),
                              ],
                            ))),
                      ),
                      state is! SearchWatchlistError
                          ? data == null || data.data.length == 0
                              ? _emptyWatchlist(
                                  "Belum Ada Watchlist",
                                  "Tambahkankan bisnis yang ingin Anda ikuti disini.",
                                  emiten)
                              : _watchlist(data)
                          : _emptyWatchlist(
                              "Saham tidak ditemukan",
                              "Ganti kata kunci pencarian atau\nTambahkankan bisnis yang ingin Anda ikuti disini.",
                              emiten)
                    ],
                  );
                },
              ),
            )),
        state is WatchlistLoading || state is WatchlistActionLoading
            ? Container(
                color: Colors.black.withOpacity(0.2),
                child: Center(child: CupertinoActivityIndicator()))
            : Container()
      ]);
    });
  }

  void _addBottomSheetWatchlist(List<el.Emiten> dataSaham) {
    el.Emiten selectedValue;
    List<DropdownMenuItem> data = dataSaham
        .map((e) => DropdownMenuItem(
            value: e,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  e.trademark,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                ),
                Text(
                  e.companyName,
                  style: TextStyle(fontSize: 14),
                ),
              ],
            )))
        .toList();
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Container(
            padding: EdgeInsets.all(16),
            height: 160,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      border: Border.all(width: 1)),
                  child: Center(
                    child: SearchableDropdown.single(
                      items: data,
                      value: selectedValue,
                      hint: "Cari Perusahaan",
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                      underline: Container(),
                      searchFn: (String keyword, items) {
                        List<int> ret = List<int>();
                        if (keyword != null &&
                            items != null &&
                            keyword.isNotEmpty) {
                          keyword.split(" ").forEach((k) {
                            int i = 0;
                            items.forEach((item) {
                              if (k.isNotEmpty &&
                                  ((item.value.code
                                          .toString()
                                          .toLowerCase()
                                          .contains(k.toLowerCase()) ||
                                      item.value.company
                                          .toString()
                                          .toLowerCase()
                                          .contains(k.toLowerCase()) ||
                                      item.value.name
                                          .toString()
                                          .toLowerCase()
                                          .contains(k.toLowerCase())))) {
                                ret.add(i);
                              }
                              i++;
                            });
                          });
                        }
                        if (keyword.isEmpty) {
                          ret = Iterable<int>.generate(items.length).toList();
                        }
                        return (ret);
                      },
                      dialogBox: true,
                      isExpanded: true,
                      icon: Icon(Icons.search),
                      displayClearIcon: false,
                    ),
                  ),
                ),
                SantaraMainButton(
                  key: Key('btn_add_to_watchlist'),
                  height: 40,
                  child: Text(
                    "Tambahkan ke Watchlist",
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    _watchlistBloc
                        .add(AddWatchlistRequested(id: selectedValue.id));
                    // setState(() => dataSaham.add(selectedValue));
                  },
                )
              ],
            ),
          );
        });
  }

  Widget _emptyWatchlist(String str1, String str2, List<el.Emiten> dataSaham) {
    return Container(
      height: MediaQuery.of(context).size.height - 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/icon_market/no_result.png"),
          Container(height: 20),
          Text(
            str1,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
          ),
          Container(height: 20),
          Text(str2, textAlign: TextAlign.center),
          Container(height: 20),
          GestureDetector(
            onTap: () => _addBottomSheetWatchlist(dataSaham),
            child: Container(
                height: 40,
                width: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                  color: Color(0xFFBF2D30),
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.add,
                      size: 14,
                      color: Colors.white,
                    ),
                    Container(width: 6),
                    Text(
                      "Tambah",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ))),
          )
        ],
      ),
    );
  }

  Widget _watchlist(data) {
    return ListView.separated(
      physics: ClampingScrollPhysics(),
      padding: EdgeInsets.symmetric(vertical: 12),
      shrinkWrap: true,
      itemCount: data.data.length,
      separatorBuilder: (_, i) {
        return Container(height: 12);
      },
      itemBuilder: (_, i) {
        return SahamWatchlist(
          key: Key('item_watchlist_$i'),
          watchlist: data.data[i],
        );
      },
    );
  }
}
