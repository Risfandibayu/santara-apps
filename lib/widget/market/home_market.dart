import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/blocs/current_time_bloc.dart';
import 'package:santaraapp/blocs/market/close_period_bloc.dart';
import 'package:santaraapp/blocs/market/emiten_list_bloc.dart';
import 'package:santaraapp/blocs/market/glossarium_bloc.dart';
import 'package:santaraapp/blocs/user_bloc.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/models/market/onboarding_tutorial_model.dart';
import 'package:santaraapp/pages/Notification.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:santaraapp/widget/market/cara_kerja_view.dart';
import 'package:santaraapp/widget/market/glossarium_view.dart';
import 'package:santaraapp/widget/market/list_saham_view.dart';
import 'package:santaraapp/widget/market/onboarding_tutorial_view.dart';
import 'package:santaraapp/widget/market/orderbook_view.dart';
import 'package:santaraapp/widget/market/portofolio_view.dart';
import 'package:santaraapp/widget/market/transaksi_view.dart';
import 'package:santaraapp/widget/market/watchlist_view.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/widget/widget/components/market/item_list_pasar_sekunder.dart';
import 'package:shimmer/shimmer.dart';
import 'package:top_sheet/top_sheet.dart';

class HomeMarket extends StatefulWidget {
  @override
  _HomeMarketState createState() => _HomeMarketState();
}

class _HomeMarketState extends State<HomeMarket> {
  User user;
  UserBloc _userBloc;
  EmitenListBloc _emitenListBloc;
  ClosePeriodBloc _periodBloc;
  GlossariumBloc _glossariumBloc;
  CurrentTimeBloc _currentTimeBloc;

  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    await _trailsServices.createTrails(
        event: marketTrailsData["home_view_success"]["event"],
        note: marketTrailsData["home_view_success"]["note"]);
  }

  @override
  void initState() {
    super.initState();
    initMarketTrails();
    _userBloc = BlocProvider.of<UserBloc>(context);
    _emitenListBloc = BlocProvider.of<EmitenListBloc>(context);
    _periodBloc = BlocProvider.of<ClosePeriodBloc>(context);
    _glossariumBloc = BlocProvider.of<GlossariumBloc>(context);
    _currentTimeBloc = BlocProvider.of<CurrentTimeBloc>(context);
    _emitenListBloc.add(EmitenListRequested());
    _userBloc.add(UserRequested());
    _periodBloc.add(ClosePeriodRequested());
    _glossariumBloc.add(GlossariumRequested());
    _currentTimeBloc.add(CurrentTimeRequested());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserLoaded) {
        user = state.user;
      }
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text("Pasar Sekunder", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.notifications_none),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => Notif()));
                }),
            IconButton(
                icon: Image.asset("assets/icon_market/icon_up_drawer.png"),
                onPressed: () {
                  TopSheet.show(
                    context: context,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        AppBar(
                          backgroundColor: Colors.white,
                          iconTheme: IconThemeData(color: Colors.black),
                          title: Text("Pasar Sekunder",
                              style: TextStyle(color: Colors.black)),
                          centerTitle: true,
                          leading: IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () => Navigator.pop(context)),
                          actions: <Widget>[
                            IconButton(
                                icon: Image.asset(
                                  "assets/icon_market/icon_up_drawer.png",
                                  color: Color(0xFFBF2D30),
                                ),
                                onPressed: () => Navigator.pop(context))
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(
                                    color: Color(0xFFC4C4C4), width: 1)),
                            child: ListTile(
                              leading: user == null || user.trader.photo == null
                                  ? Container(
                                      height: 60,
                                      width: 60,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/icon/user.png'),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    )
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(50),
                                      child: SantaraCachedImage(
                                        height: 60,
                                        width: 60,
                                        image: GetAuthenticatedFile.convertUrl(
                                          image: user.trader.photo,
                                          type: PathType.traderPhoto,
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                              title: state is UserLoading
                                  ? CupertinoActivityIndicator()
                                  : Text(user == null
                                      ? "Guest"
                                      : user.trader.name),
                              subtitle: state is UserLoading
                                  ? CupertinoActivityIndicator()
                                  : Text(user == null
                                      ? "guest@mail.com"
                                      : user.email),
                            )),
                        ListTile(
                          title: Text('Home'),
                          onTap: () => Navigator.pop(context),
                        ),
                        ListTile(
                            title: Text('List Saham'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => ListSahamView()));
                            }),
                        ListTile(
                            title: Text('Watchlist'),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => WatchlistView()));
                            }),
                        ListTile(
                          title: Text('Portofolio'),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => PortofolioView()));
                          },
                        ),
                        ListTile(
                          title: Text('Transaksi'),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => TransaksiView()));
                          },
                        ),
                        ListTile(
                          title: Text('Orderbook'),
                          onTap: () async {
                            await _trailsServices.createTrails(
                                event: marketTrailsData["orderbook_view"]
                                    ["event"],
                                note: marketTrailsData["orderbook_view"]
                                    ["note"]);
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => OrderbookView()));
                          },
                        ),
                      ],
                    ),
                    direction: TopSheetDirection.TOP,
                  );
                })
          ],
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            _countdownCloseMarket(),
            SantaraAppBetaTesting(),
            _mainMenu(),
            _tutorial(),
            _listSaham(),
          ],
        ),
      );
    });
  }

  Widget _countdownCloseMarket() {
    return BlocBuilder<ClosePeriodBloc, ClosePeriodState>(
      builder: (context, stateClosePeriod) {
        if (stateClosePeriod is ClosePeriodLoaded) {
          // DateTime now = state.now;
          // DateTime closePeriod = stateClosePeriod.closePeriod;
          return BlocBuilder<CurrentTimeBloc, CurrentTimeState>(
            builder: (context, stateCurrentTime) {
              DateTime now = DateTime.now();
              if (stateCurrentTime is CurrentTimeLoaded) {
                now = stateCurrentTime.now;
              }
              return Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xFFF6911A)),
                    borderRadius: BorderRadius.circular(6),
                    color: Color(0xFFFDE9CD)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Image.asset("assets/icon_notif/timeout.png",
                          width: 30, height: 30),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14.0),
                      child: stateClosePeriod.closePeriod
                                      .difference(now)
                                      .inSeconds <
                                  1 ||
                              stateClosePeriod.openPeriod
                                      .difference(now)
                                      .inSeconds >
                                  1
                          ? Wrap(
                              children: [
                                Text("Pasar Sekunder "),
                                Text("ditutup",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFFD23737))),
                              ],
                            )
                          : Wrap(
                              children: [
                                Text("Pasar Sekunder "),
                                Text("ditutup",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFFD23737))),
                                Text(stateClosePeriod.closePeriod
                                            .difference(now)
                                            .inDays >
                                        3
                                    ? " pada "
                                    : " dalam "),
                                stateClosePeriod.closePeriod
                                            .difference(now)
                                            .inDays >
                                        3
                                    ? Text(DateFormat('dd MMMM yyyy', 'id')
                                        .format(stateClosePeriod.closePeriod))
                                    : Countdown(
                                        duration: Duration(
                                            seconds: stateClosePeriod
                                                .closePeriod
                                                .difference(now)
                                                .inSeconds),
                                        builder: (BuildContext context,
                                            Duration remaining) {
                                          if (remaining.inDays > 0) {
                                            return Text(
                                                "${remaining.inDays} hari");
                                          } else {
                                            String hour =
                                                (remaining.inHours).toString();
                                            String second =
                                                (remaining.inSeconds % 60)
                                                    .toString();
                                            String minute =
                                                (remaining.inMinutes % 60)
                                                    .toString();
                                            if (second.length == 1) {
                                              second = "0" + second;
                                            }
                                            if (minute.length == 1) {
                                              minute = "0" + minute;
                                            }
                                            if (hour.length == 1) {
                                              hour = "0" + hour;
                                            }
                                            return Text(
                                                '$hour:$minute:$second');
                                          }
                                        },
                                      )
                              ],
                            ),
                    )),
                    IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () => _periodBloc.add(CloseNotifRequested()))
                  ],
                ),
              );
            },
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget _mainMenu() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(0),
            title: Text("Selamat Datang di Pasar Sekunder!",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            subtitle: Text("Apa yang ingin Anda lakukan?"),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: SantaraMainButtonWithIcon(
                  key: Key('beli_btn_in_home_market'),
                  icon: Image.asset("assets/icon_market/beli.png",
                      color: Colors.white),
                  height: 40,
                  name: "Beli Saham",
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => ListSahamView())),
                ),
              ),
              Container(width: 10),
              Expanded(
                child: SantaraMainButtonWithIcon(
                  key: Key('jual_btn_in_home_market'),
                  icon: Image.asset("assets/icon_market/jual.png",
                      color: Colors.white),
                  height: 40,
                  name: "Jual Saham",
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => PortofolioView())),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _tutorial() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Tutorial Pasar Sekunder",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Container(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: <Widget>[
                TutorialButton(
                  key: Key('tutorial_beli'),
                  icon: Image.asset("assets/icon_tutorial_market/cara_beli.png",
                      height: MediaQuery.of(context).size.width / 10,
                      width: MediaQuery.of(context).size.width / 10),
                  name: "Cara Pembelian",
                  onTap: () async {
                    await _trailsServices.createTrails(
                        event: marketTrailsData["how_to_buy_view"]["event"],
                        note: marketTrailsData["how_to_buy_view"]["note"]);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => OnboardingTutorialModelView(
                                appbarName: "Cara Pembelian",
                                data: tutorialBeli)));
                  },
                ),
                TutorialButton(
                  key: Key('tutorial_jual'),
                  icon: Image.asset("assets/icon_tutorial_market/cara_jual.png",
                      height: MediaQuery.of(context).size.width / 10,
                      width: MediaQuery.of(context).size.width / 10),
                  name: "Cara Penjualan",
                  onTap: () async {
                    await _trailsServices.createTrails(
                        event: marketTrailsData["how_to_sell_view"]["event"],
                        note: marketTrailsData["how_to_sell_view"]["note"]);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => OnboardingTutorialModelView(
                                appbarName: "Cara Penjualan",
                                data: tutorialJual)));
                  },
                ),
                TutorialButton(
                  key: Key('tutorial_cara_kerja'),
                  icon: Image.asset(
                      "assets/icon_tutorial_market/cara_kerja.png",
                      height: MediaQuery.of(context).size.width / 10,
                      width: MediaQuery.of(context).size.width / 10),
                  name: "Cara Kerja",
                  onTap: () async {
                    await _trailsServices.createTrails(
                        event: marketTrailsData["how_it_works_view"]["event"],
                        note: marketTrailsData["how_it_works_view"]["note"]);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => CaraKerjaView()));
                  },
                ),
                TutorialButton(
                  key: Key('tutorial_glossarium'),
                  icon: Image.asset("assets/icon_tutorial_market/glossary.png",
                      height: MediaQuery.of(context).size.width / 10,
                      width: MediaQuery.of(context).size.width / 10),
                  name: "Glosarium",
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => GlossariumView()));
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _listSaham() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("List Saham",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          BlocBuilder<EmitenListBloc, EmitenListState>(
            builder: (context, state) {
              if (state is EmitenListLoaded) {
                var data = state.emitenList.data;
                if (data.length == 0) {
                  return Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset("assets/icon/search_not_found.png"),
                        Text("Data saham tidak ditemukan")
                      ],
                    ),
                  );
                } else {
                  return ListView.separated(
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.symmetric(vertical: 12),
                    shrinkWrap: true,
                    itemCount: data.length > 4 ? 4 : data.length,
                    separatorBuilder: (_, i) {
                      return Container(height: 12);
                    },
                    itemBuilder: (_, i) {
                      return ListSahamPasarSekunder(
                          key: Key('item_saham_$i'), emiten: data[i]);
                    },
                  );
                }
              } else if (state is EmitenListLoading) {
                return ListView.separated(
                  physics: ClampingScrollPhysics(),
                  padding: EdgeInsets.symmetric(vertical: 12),
                  shrinkWrap: true,
                  itemCount: 3,
                  separatorBuilder: (_, i) {
                    return Container(height: 12);
                  },
                  itemBuilder: (_, i) {
                    return Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[400],
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey[400],
                              borderRadius: BorderRadius.circular(6)),
                          height: 80,
                          width: MediaQuery.of(context).size.width,
                        ));
                  },
                );
              } else {
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset("assets/icon/search_not_found.png"),
                      Text("Data saham tidak ditemukan")
                    ],
                  ),
                );
              }
            },
          ),
          GestureDetector(
            onTap: () => Navigator.push(
                context, MaterialPageRoute(builder: (_) => ListSahamView())),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Lihat Semua",
                  style: TextStyle(color: Color(0xFF218196)),
                ),
                Icon(Icons.chevron_right, color: Color(0xFF218196))
              ],
            ),
          )
        ],
      ),
    );
  }
}
