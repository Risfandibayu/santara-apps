import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/blocs/market/emiten_list_all_bloc.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraDropDown.dart';
import 'package:santaraapp/widget/widget/components/market/item_list_pasar_sekunder.dart';
import 'package:shimmer/shimmer.dart';

class ListSahamView extends StatefulWidget {
  @override
  _ListSahamViewState createState() => _ListSahamViewState();
}

class _ListSahamViewState extends State<ListSahamView> {
  final _search = TextEditingController();
  ScrollController _scroll;
  EmitenListAllBloc _listAllBloc;
  bool isSearching = false;
  int page = 1;
  int lastPage = 0;
  MarketTrailsServices _trailsServices = MarketTrailsServices();

  final group = [
    {"value": "", "title": "Semua"},
    {"value": "gainer", "title": "Top Gainer"},
    {"value": "loser", "title": "Top Loser"},
    {"value": "pick", "title": "Top Pick"},
  ];

  final price = [
    {"value": 0, "title": "Price"},
    {"value": 1, "title": "Tertinggi"},
    {"value": 2, "title": "Terendah"},
  ];

  _scrollListener() {
    if (_scroll.offset >= _scroll.position.maxScrollExtent &&
        !_scroll.position.outOfRange) {
      if (lastPage != null && page < lastPage) {
        page = page + 1;
        _listAllBloc
            .add(EmitenListAllPagination(page: page, key: _search.text));
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _listAllBloc = BlocProvider.of<EmitenListAllBloc>(context);
    _listAllBloc.add(EmitenListAllRequested(group: "", sort: 0, key: ""));
    _scroll = ScrollController();
    _scroll.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<EmitenListAllBloc, EmitenListAllState>(
        listener: (context, state) async {
      if (state is EmitenListAllLoaded) {
        page = state.currentPage;
        lastPage = state.lastPage;
        if (state.sort == 1) {
          await _trailsServices.createTrails(
              event: marketTrailsData["price_sorting_dsc_view_success"]
                  ["event"],
              note: marketTrailsData["price_sorting_dsc_view_success"]["note"]);
        } else if (state.sort == 2) {
          await _trailsServices.createTrails(
              event: marketTrailsData["price_sorting_asc_view_success"]
                  ["event"],
              note: marketTrailsData["price_sorting_asc_view_success"]["note"]);
        }
      } else if (state is EmitenListAllError) {
        if (state.sort == 1) {
          await _trailsServices.createTrails(
              event: marketTrailsData["price_sorting_dsc_view_failed"]["event"],
              note: marketTrailsData["price_sorting_dsc_view_failed"]["note"]);
        } else if (state.sort == 2) {
          await _trailsServices.createTrails(
              event: marketTrailsData["price_sorting_asc_view_failed"]["event"],
              note: marketTrailsData["price_sorting_asc_view_failed"]["note"]);
        }
      }
    }, builder: (context, state) {
      final currentState = state;
      List<Emiten> emitenList = currentState.props[2];
      return Scaffold(
        appBar: isSearching
            ? AppBar(
                titleSpacing: 0,
                automaticallyImplyLeading: false,
                title: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: TextField(
                    textInputAction: TextInputAction.search,
                    controller: _search,
                    onSubmitted: (value) {
                      if (value.isNotEmpty) {
                        _listAllBloc.add(EmitenListAllRequested(
                            group: currentState.props[0], sort: 0, key: value));
                      } else {
                        if (state is EmitenListAllLoaded) {
                        } else {
                          _listAllBloc.add(EmitenListAllRequested(
                              group: "", sort: 0, key: ""));
                        }
                      }
                    },
                    onChanged: (value) {
                      if (value == "") {
                        if (state is EmitenListAllLoaded) {
                        } else {
                          _listAllBloc.add(EmitenListAllRequested(
                              group: "", sort: 0, key: ""));
                        }
                      }
                    },
                    decoration: InputDecoration(
                        hintText: "Cari Nama Bisnis",
                        hintStyle: TextStyle(color: Color(0xff676767)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        fillColor: Color(0xffF2F2F2),
                        border: OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(40.0),
                            ),
                            borderSide: BorderSide(color: Color(0xffDDDDDD))),
                        suffixIcon: IconButton(
                            padding: EdgeInsets.only(right: 16),
                            color: Colors.black,
                            icon: Icon(Icons.search),
                            onPressed: () {
                              if (_search.text.isNotEmpty) {
                                _listAllBloc.add(EmitenListAllRequested(
                                    group: currentState.props[0],
                                    sort: 0,
                                    key: _search.text));
                              } else {
                                if (state is EmitenListAllLoaded) {
                                } else {
                                  _listAllBloc.add(EmitenListAllRequested(
                                      group: "", sort: 0, key: ""));
                                }
                              }
                            })),
                  ),
                ),
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: Icon(Icons.close),
                  color: Colors.black,
                  onPressed: () {
                    _search.clear();
                    if (state is EmitenListAllLoaded) {
                    } else {
                      _listAllBloc.add(
                          EmitenListAllRequested(group: "", sort: 0, key: ""));
                    }
                    setState(() => isSearching = false);
                  },
                ))
            : AppBar(
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.black),
                title:
                    Text("List Saham", style: TextStyle(color: Colors.black)),
                centerTitle: true,
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        setState(() => isSearching = true);
                      })
                ],
              ),
        body: ListView(
            padding: EdgeInsets.all(16),
            controller: _scroll,
            children: <Widget>[
              ListView(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  children: <Widget>[
                    SantaraAppBetaTesting(),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: SantaraDropDown(
                            isExpanded: true,
                            hint: Text("Semua"),
                            items: group.map((e) {
                              return DropdownMenuItem(
                                  value: e["value"],
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          bottom: e.length > 10
                                              ? MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  2
                                              : 0),
                                      child: Text(e["title"])));
                            }).toList(),
                            value: state.props[0],
                            onChanged: (selected) {
                              if (state is EmitenListAllLoaded) {
                                _listAllBloc.add(EmitenListAllRequested(
                                    group: selected,
                                    sort: 0,
                                    key: _search.text));
                              }
                            },
                          ),
                        ),
                        Container(width: 10),
                        Flexible(
                          flex: 1,
                          child: SantaraDropDown(
                            isExpanded: true,
                            hint: Text("Price"),
                            items: price.map((e) {
                              return DropdownMenuItem(
                                  value: e["value"],
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          bottom: e.length > 10
                                              ? MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  2
                                              : 0),
                                      child: Text(e["title"])));
                            }).toList(),
                            value: state.props[1],
                            onChanged: (selected) async {
                              if (selected == 1) {
                                await _trailsServices.createTrails(
                                    event: marketTrailsData[
                                        "price_sorting_dsc_view"]["event"],
                                    note: marketTrailsData[
                                        "price_sorting_dsc_view"]["note"]);
                              } else if (selected == 2) {
                                await _trailsServices.createTrails(
                                    event: marketTrailsData[
                                        "price_sorting_asc_view"]["event"],
                                    note: marketTrailsData[
                                        "price_sorting_asc_view"]["note"]);
                              }
                              if (state is EmitenListAllLoaded) {
                                if (selected == 2) {
                                  state.emitenList.sort((a, b) =>
                                      a.currentPriceData.currentPrice.compareTo(
                                          b.currentPriceData.currentPrice));
                                } else if (selected == 1) {
                                  state.emitenList.sort((a, b) =>
                                      b.currentPriceData.currentPrice.compareTo(
                                          a.currentPriceData.currentPrice));
                                }
                                _listAllBloc.add(EmitenListAllSort(
                                    group: state.group,
                                    sort: selected,
                                    emitenList: state.emitenList,
                                    key: _search.text));
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    state is EmitenListAllLoading
                        ? ListView.separated(
                            physics: ClampingScrollPhysics(),
                            padding: EdgeInsets.symmetric(vertical: 12),
                            shrinkWrap: true,
                            itemCount: 3,
                            separatorBuilder: (_, i) {
                              return Container(height: 12);
                            },
                            itemBuilder: (_, i) {
                              return Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[400],
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey[400],
                                        borderRadius: BorderRadius.circular(6)),
                                    height: 80,
                                    width: MediaQuery.of(context).size.width,
                                  ));
                            },
                          )
                        : emitenList.length == 0
                            ? Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                        "assets/icon/search_not_found.png"),
                                    Text("Data saham tidak ditemukan")
                                  ],
                                ),
                              )
                            : ListView.separated(
                                physics: ClampingScrollPhysics(),
                                padding: EdgeInsets.symmetric(vertical: 12),
                                shrinkWrap: true,
                                itemCount: emitenList.length,
                                separatorBuilder: (_, i) {
                                  return Container(height: 12);
                                },
                                itemBuilder: (_, i) {
                                  return ListSahamPasarSekunder(
                                      key: Key('item_saham_$i'),
                                      emiten: emitenList[i]);
                                },
                              )
                  ]),
              state is EmitenListAllPaginationLoading
                  ? ListView.separated(
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.symmetric(vertical: 12),
                      shrinkWrap: true,
                      itemCount: 1,
                      separatorBuilder: (_, i) {
                        return Container(height: 12);
                      },
                      itemBuilder: (_, i) {
                        return Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[400],
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[400],
                                  borderRadius: BorderRadius.circular(6)),
                              height: 80,
                              width: MediaQuery.of(context).size.width,
                            ));
                      },
                    )
                  : Container()
            ]),
      );
    });
  }
}
