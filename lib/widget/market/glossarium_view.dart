import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/blocs/market/glossarium_bloc.dart';
import 'package:santaraapp/models/market/glossarium_list.dart';
import 'package:santaraapp/widget/market/glossarium_search_view.dart';
import 'package:santaraapp/widget/widget/components/market/item_glossarium.dart';

class GlossariumView extends StatefulWidget {
  @override
  _GlossariumViewState createState() => _GlossariumViewState();
}

class _GlossariumViewState extends State<GlossariumView> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GlossariumBloc, GlossariumState>(
        builder: (context, state) {
      if (state is GlossariumLoaded) {
        List<String> initial = state.initial;
        GlossariumList data = state.glossariumList;
        return DefaultTabController(
          length: initial.length,
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.black),
              title: Text("Glosarium", style: TextStyle(color: Colors.black)),
              actions: [
                IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => GlossariumSearchView())))
              ],
              bottom: PreferredSize(
                  child: TabBar(
                      isScrollable: true,
                      labelColor: Color(0xFFBF2D30),
                      unselectedLabelColor: Colors.black,
                      indicatorColor: Color(0xFFBF2D30),
                      tabs: initial
                          .map(
                            (e) => Tab(
                              child: Text(e),
                            ),
                          )
                          .toList()),
                  preferredSize: Size.fromHeight(50.0)),
            ),
            body: TabBarView(
                children:
                    initial.map((e) => _glossariumList(e, data)).toList()),
          ),
        );
      } else if (state is GlossariumError) {
        return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.black),
              title: Text("Glosarium", style: TextStyle(color: Colors.black)),
              actions: [
                IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => GlossariumSearchView())))
              ],
            ),
            body: Container(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  Text("Data glossarium tidak ditemukan")
                ],
              ),
            ));
      } else {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            title: Text("Glosarium", style: TextStyle(color: Colors.black)),
            actions: [
              IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => GlossariumSearchView())))
            ],
          ),
          body: Container(
            child: Center(
              child: CupertinoActivityIndicator(),
            ),
          ),
        );
      }
    });
  }

  Widget _glossariumList(String initial, GlossariumList data) {
    return ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 10),
        itemCount: data.data.length,
        itemBuilder: (_, i) {
          if (initial == "#") {
            // tampilkan semua glossarium
            return ItemGlossarium(
                key: Key('item_glossarium_$i'),
                title: data.data[i].title,
                content: data.data[i].description);
          } else {
            // tampilkan glossarium jika initial glosarium sama dengan judul tab
            // jika tidak, tampilkan widget container() **kosong
            if (initial == data.data[i].title[0]) {
              return ItemGlossarium(
                  key: Key('item_glossarium_$i'),
                  title: data.data[i].title,
                  content: data.data[i].description);
            } else {
              return Container();
            }
          }
        });
  }
}
