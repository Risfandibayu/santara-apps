import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:santaraapp/models/market/onboarding_tutorial_model.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';

class OnboardingTutorialModelView extends StatefulWidget {
  final String appbarName;
  final List<OnboardingTutorialModel> data;

  OnboardingTutorialModelView({this.appbarName, this.data})
      : assert(appbarName != null, data != null);

  @override
  _OnboardingTutorialModelViewState createState() =>
      _OnboardingTutorialModelViewState();
}

class _OnboardingTutorialModelViewState
    extends State<OnboardingTutorialModelView> {
  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    if (widget.appbarName == "Cara Pembelian") {
      await _trailsServices.createTrails(
          event: marketTrailsData["how_to_buy_view_success"]["event"],
          note: marketTrailsData["how_to_buy_view_success"]["note"]);
    } else {
      await _trailsServices.createTrails(
          event: marketTrailsData["how_to_sell_view_success"]["event"],
          note: marketTrailsData["how_to_sell_view_success"]["note"]);
    }
  }

  @override
  void initState() {
    super.initState();
    initMarketTrails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(widget.appbarName, style: TextStyle(color: Colors.black)),
          centerTitle: true,
        ),
        body: Swiper(
          itemBuilder: (BuildContext context, int index) {
            return _itemTutorial(context, widget.data[index], index);
          },
          autoplay: false,
          itemCount: widget.data.length,
          loop: false,
          pagination: SwiperPagination(
              margin: EdgeInsets.only(bottom: 20),
              builder: DotSwiperPaginationBuilder(
                  activeColor: Color(0xFFBF2D30),
                  color: Color(0xFFDDDDDD),
                  size: 6,
                  activeSize: 6)),
        ));
  }

  Widget _itemTutorial(
      BuildContext context, OnboardingTutorialModel data, int index) {
    return Stack(children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              child: Image.asset(
                data.image,
                fit: BoxFit.fill,
              )),
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(data.title,
                style: TextStyle(
                    color: Color(0xFFBF2D30), fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: Text(data.subtitle,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Text(
              data.desc,
            ),
          ),
        ],
      ),
      index == 0
          ? Positioned(
              bottom: 15,
              right: 20,
              child: Row(
                children: <Widget>[
                  Text(
                    "Geser",
                    style: TextStyle(color: Color(0xFFBF2D30)),
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Color(0xFFBF2D30),
                  )
                ],
              ))
          : Container()
    ]);
  }
}
