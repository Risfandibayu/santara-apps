import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/blocs/market/history_today_bloc.dart';
import 'package:santaraapp/blocs/market/market_fee_bloc.dart';
import 'package:santaraapp/blocs/market/submit_transaction.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/history_market.dart' as history;
import 'package:santaraapp/models/market/history_today.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/check_today_widget.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';

enum DetailOrderType { order, history }

class DetailOrderView extends StatefulWidget {
  final DetailOrderType type;
  final History historyToday;
  final history.History historyMarket;
  DetailOrderView({this.type, this.historyToday, this.historyMarket});
  @override
  _DetailOrderViewState createState() => _DetailOrderViewState();
}

class _DetailOrderViewState extends State<DetailOrderView> {
  SubmitTransactionBloc _submitTransactionBloc;
  CheckTodayBloc _checkTodayBloc;
  HistoryTodayBloc _todayBloc;
  FlutterSecureStorage storage = FlutterSecureStorage();
  var token;
  var fee;
  var total;

  Future _checkLogin() async {
    token = await storage.read(key: 'token');
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    if (widget.type == DetailOrderType.history) {
      fee = (widget.historyMarket.price * widget.historyMarket.amount) *
          (widget.historyMarket.fee / 100);
      total = widget.historyMarket.price * widget.historyMarket.amount;
    } else {
      total = widget.historyToday.price * widget.historyToday.startAmount;
    }
    _checkLogin();
    _checkTodayBloc = BlocProvider.of<CheckTodayBloc>(context);
    _todayBloc = BlocProvider.of<HistoryTodayBloc>(context);
    _submitTransactionBloc = BlocProvider.of<SubmitTransactionBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SubmitTransactionBloc, SubmitTransactionState>(
        listener: (context, state) {
      if (state is DeleteTransactionSuccess) {
        ToastHelper.showSuccessToast(context, state.message);
        // go back and refresh list transaction
        Navigator.pop(context);
        _todayBloc.add(HistoryTodayRequested());
      }
      if (state is SubmitTransactionFailure) {
        ToastHelper.showFailureToast(context, state.message);
        // go back and refresh list transaction
        Navigator.pop(context);
        _todayBloc.add(HistoryTodayRequested());
      }
    }, builder: (context, state) {
      return CheckTodayWidget(
          checkTodayBloc: _checkTodayBloc,
          builder: (context, state) {
            return ModalProgressHUD(
                inAsyncCall: state is SubmitTransactionLoading ||
                    state is CheckTodayLoading,
                color: Colors.black,
                opacity: 0.2,
                progressIndicator: CupertinoActivityIndicator(),
                child: Scaffold(
                    appBar: AppBar(
                      backgroundColor: Colors.white,
                      iconTheme: IconThemeData(color: Colors.black),
                      title: Text(
                          widget.type == DetailOrderType.order
                              ? "Detail Order"
                              : "Riwayat Transaksi",
                          style: TextStyle(color: Colors.white)),
                      centerTitle: true,
                    ),
                    bottomNavigationBar: widget.type == DetailOrderType.order &&
                            widget.historyToday.statusOrder.toLowerCase() ==
                                "open"
                        ? BottomAppBar(
                            child: Container(
                                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                                height: 55,
                                child: Row(
                                  children: <Widget>[
                                    BlocBuilder<MarketFeeBloc, MarketFeeState>(
                                      builder: (context, stateFee) {
                                        int pending = 0;
                                        if (stateFee is MarketFeeLoaded) {
                                          var fee =
                                              (total * stateFee.feeBuy).ceil();
                                          pending = widget.type ==
                                                  DetailOrderType.order
                                              ? widget.historyToday.type ==
                                                      "buy"
                                                  ? total + fee
                                                  : widget
                                                      .historyToday.startAmount
                                              : 0;
                                        }
                                        return Expanded(
                                          child: SantaraMainButtonWithIcon(
                                            key:
                                                Key('ubah_btn_in_detail_order'),
                                            icon: Icon(Icons.edit,
                                                color: Colors.white),
                                            name: "Ubah",
                                            height: 40,
                                            onTap: () {
                                              if (token == null) {
                                                PopupHelperMarket
                                                    .needLoginToAccessThisPage(
                                                        context: context,
                                                        isGoBack: 0);
                                              } else {
                                                _checkTodayBloc
                                                    .add(CheckTodayRequested(
                                                  uuid: widget
                                                      .historyToday.emitenUuid,
                                                  codeEmiten: widget.type ==
                                                          DetailOrderType.order
                                                      ? widget.historyToday
                                                          .codeEmiten
                                                      : widget.historyMarket
                                                          .codeEmiten,
                                                  trxUuid:
                                                      widget.historyToday.docId,
                                                  type: widget.type ==
                                                          DetailOrderType.order
                                                      ? widget.historyToday
                                                                  .type ==
                                                              "buy"
                                                          ? TransactionType.beli
                                                          : TransactionType.jual
                                                      : widget.historyMarket
                                                                  .type ==
                                                              "buy"
                                                          ? TransactionType.beli
                                                          : TransactionType
                                                              .jual,
                                                  amount: widget.type ==
                                                          DetailOrderType.order
                                                      ? widget
                                                          .historyToday.amount
                                                      : widget
                                                          .historyMarket.amount,
                                                  price: widget.type ==
                                                          DetailOrderType.order
                                                      ? widget
                                                          .historyToday.price
                                                      : widget
                                                          .historyMarket.price,
                                                  pending: pending,
                                                ));
                                              }
                                            },
                                            decoration: BoxDecoration(
                                                color: Color(0xFF666EE8),
                                                borderRadius:
                                                    BorderRadius.circular(4)),
                                          ),
                                        );
                                      },
                                    ),
                                    Container(width: 8),
                                    Expanded(
                                      child: SantaraMainButtonWithIcon(
                                        key: Key(
                                            'hapus_trx_btn_in_detail_order'),
                                        icon: Icon(Icons.delete,
                                            color: Colors.white),
                                        name: "Hapus",
                                        height: 40,
                                        onTap: () {
                                          PopupHelperMarket.showHapusTransaksi(
                                              context,
                                              widget.historyToday.docId,
                                              widget.historyToday.codeEmiten,
                                              widget.historyToday.type,
                                              _submitTransactionBloc);
                                        },
                                        decoration: BoxDecoration(
                                            color: Color(0xFFEB5255),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                      ),
                                    )
                                  ],
                                )),
                          )
                        : Container(
                            height: 0,
                          ),
                    body: ListView(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: SantaraAppBetaTesting(),
                      ),
                      widget.type == DetailOrderType.order
                          ? ListTile(
                              title: Text(widget.historyToday.codeEmiten),
                              subtitle: Text(widget.historyToday.companyName),
                            )
                          : Container(),
                      _itemDetailOrder(
                        widget.type == DetailOrderType.order
                            ? "Order ID"
                            : "Kode Saham",
                        Text(
                          widget.type == DetailOrderType.order
                              ? widget.historyToday.orderId
                              : widget.historyMarket.codeEmiten,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      _itemDetailOrder(
                        "Tipe",
                        Text(
                          widget.type == DetailOrderType.order
                              ? widget.historyToday.type == "buy"
                                  ? "BELI"
                                  : "JUAL"
                              : widget.historyMarket.type == "buy"
                                  ? "BELI"
                                  : "JUAL",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      _itemDetailOrder(
                        "Jumlah",
                        Text(
                          NumberFormatter.convertNumber(
                                  widget.type == DetailOrderType.order
                                      ? widget.historyToday.startAmount
                                      : widget.historyMarket.amount) +
                              " Lembar",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      // widget.type == DetailOrderType.order
                      //     ? _itemDetailOrder(
                      //         "Tersedia",
                      //         Text(
                      //             NumberFormatter.convertNumber(
                      //                     widget.type == DetailOrderType.order
                      //                         ? widget.historyToday.amount
                      //                         : widget.historyMarket.amount) +
                      //                 " Lembar",
                      //             style:
                      //                 TextStyle(fontWeight: FontWeight.bold)),
                      //       )
                      //     : Container(),
                      _itemDetailOrder(
                        "Harga",
                        Text(
                          "Rp " +
                              NumberFormatter.convertNumber(
                                  widget.type == DetailOrderType.order
                                      ? widget.historyToday.price
                                      : widget.historyMarket.price),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      widget.type == DetailOrderType.order
                          ? Container()
                          : _itemDetailOrder(
                              "Fee",
                              Text(
                                "Rp " + NumberFormatter.convertNumber(fee),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                      _itemDetailOrder(
                        "Total",
                        Text(
                          "Rp " +
                              NumberFormatter.convertNumber(
                                  widget.type == DetailOrderType.order
                                      ? total
                                      : widget.historyMarket.type == "buy"
                                          ? total + fee
                                          : total - fee),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      widget.type == DetailOrderType.order
                          ? _itemDetailOrder(
                              "Status",
                              Text(
                                widget.historyToday.statusOrder,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          : _itemDetailOrder(
                              "Tanggal",
                              Text(
                                DateFormat('dd MMM yyyy')
                                    .format(widget.historyMarket.createdAt),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                    ])));
          });
    });
  }

  Widget _itemDetailOrder(String title, Widget content) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title),
          content,
        ],
      ),
    );
  }
}
