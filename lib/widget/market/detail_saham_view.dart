import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/blocs/current_time_bloc.dart';
import 'package:santaraapp/blocs/market/check_today_bloc.dart';
import 'package:santaraapp/blocs/market/emiten_profile_bloc.dart';
import 'package:santaraapp/blocs/market/data_chart_bloc.dart';
import 'package:santaraapp/blocs/market/financial_bloc.dart';
import 'package:santaraapp/blocs/market/financial_chart_bloc.dart';
import 'package:santaraapp/blocs/market/fundamental_bloc.dart';
import 'package:santaraapp/blocs/market/watchlist_bloc.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/market/expand_collapse_helper/expand_collapse.dart';
import 'package:santaraapp/models/market/emiten_profile_model.dart';
import 'package:santaraapp/models/market/fundamental_list_model.dart';
import 'package:santaraapp/models/market/watchlist_model.dart';
import 'package:santaraapp/services/market_trails/market_trails_services.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/utils/strings.dart';
import 'package:santaraapp/widget/financial_statements/pages/blocs/chart.bloc.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/widget/components/charts/build_chart.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/market/check_today_widget.dart';
import 'package:santaraapp/widget/widget/components/market/info_price.dart';
import 'package:santaraapp/helpers/market/popup_helper_market.dart';
import 'package:santaraapp/widget/widget/components/market/syncfusion_chart.dart';
import 'package:santaraapp/widget/widget/components/market/table_pasar_sekunder.dart'
    as table;
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailSahamView extends StatefulWidget {
  final int id;
  final String codeEmiten;
  const DetailSahamView({this.id, this.codeEmiten});
  @override
  _DetailSahamViewState createState() => _DetailSahamViewState();
}

class _DetailSahamViewState extends State<DetailSahamView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController _chartTab;
  TabController _emitenTab;

  EmitenProfileBloc _emitenProfileBloc;
  FinancialBloc _financialBloc;
  FinancialChartBloc _chartBloc;
  FundamentalBloc _fundamentalBloc;
  DataChartBloc _dataChartBloc;
  WatchlistBloc _watchlistBloc;
  CheckTodayBloc _checkTodayBloc;
  FinanceChartBloc _financialChartBloc;
  CurrentTimeBloc _currentTimeBloc;

  FlutterSecureStorage storage = FlutterSecureStorage();
  var token;
  DateTime now = DateTime.now();
  EmitenProfileModel emitenProfile;
  String uuid;
  String companyName = "";

  MarketTrailsServices _trailsServices = MarketTrailsServices();

  Future initMarketTrails() async {
    await _trailsServices.createTrails(
        event: marketTrailsData["line_chart_view"]["event"],
        note: marketTrailsData["line_chart_view"]["note"]);
  }

  Future _checkLogin() async {
    token = await storage.read(key: 'token');
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
    _currentTimeBloc = BlocProvider.of<CurrentTimeBloc>(context);
    _currentTimeBloc.add(CurrentTimeRequested());
    _emitenProfileBloc = BlocProvider.of<EmitenProfileBloc>(context);
    _financialBloc = BlocProvider.of<FinancialBloc>(context);
    _chartBloc = BlocProvider.of<FinancialChartBloc>(context);
    _fundamentalBloc = BlocProvider.of<FundamentalBloc>(context);
    _dataChartBloc = BlocProvider.of<DataChartBloc>(context);
    _watchlistBloc = BlocProvider.of<WatchlistBloc>(context);
    _checkTodayBloc = BlocProvider.of<CheckTodayBloc>(context);
    _financialChartBloc = BlocProvider.of<FinanceChartBloc>(context);
    _emitenProfileBloc.add(EmitenProfileRequested(id: widget.id));
    _chartBloc.add(FinancialChartRequested(codeEmiten: widget.codeEmiten));
    _fundamentalBloc.add(FundamentalRequested(codeEmiten: widget.codeEmiten));
    _dataChartBloc.add(DataChartRequested(codeEmiten: widget.codeEmiten));
    _watchlistBloc.add(WatchlistRequested());
    _chartTab = new TabController(vsync: this, length: 3, initialIndex: 0);
    _emitenTab = new TabController(vsync: this, length: 4, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WatchlistBloc, WatchlistState>(
      listener: (context, state) {
        if (state is WatchlistAddSuccess) {
          ToastHelper.showSnackBarWithAction(
              context: context,
              key: _scaffoldKey,
              icon: Icon(Icons.favorite, color: Color(0xFFFF6464)),
              message: state.msg,
              backgroundColor: Colors.black,
              duration: Duration(seconds: 3));
        }
        if (state is WatchlistDeleteSuccess) {
          ToastHelper.showSnackBarWithAction(
              context: context,
              key: _scaffoldKey,
              icon: Icon(Icons.favorite_border, color: Colors.white),
              message: state.msg,
              backgroundColor: Colors.black,
              duration: Duration(seconds: 3));
        }
        if (state is WatchlistActionFailure) {
          ToastHelper.showFailureToast(context, state.msg);
        }
      },
      builder: (context, state) {
        List<Watchlist> data = [];
        if (state is WatchlistLoaded) {
          data = state.watchlistModel.data;
        } else if (state is WatchlistLoading) {
          data = state.watchlistModel.data;
        } else if (state is WatchlistAddSuccess) {
          data = state.watchlistModel.data;
        } else if (state is WatchlistDeleteSuccess) {
          data = state.watchlistModel.data;
        } else if (state is WatchlistActionLoading) {
          data = state.watchlistModel.data;
        }
        Query query = FirebaseFirestore.instance
            .collection("current_price_${widget.codeEmiten}");
        return CheckTodayWidget(
            checkTodayBloc: _checkTodayBloc,
            builder: (context, stateCheck) {
              return ModalProgressHUD(
                inAsyncCall: state is WatchlistLoading ||
                    state is WatchlistActionLoading ||
                    stateCheck is CheckTodayLoading,
                color: Colors.black,
                opacity: 0.2,
                progressIndicator: CupertinoActivityIndicator(),
                child: StreamBuilder<Object>(
                    stream: query.snapshots(),
                    builder: (context, stream) {
                      QuerySnapshot querySnapshot = stream.data;
                      var snapshot;
                      if (stream.connectionState == ConnectionState.active) {
                        if (querySnapshot.docs.length > 0) {
                          snapshot = querySnapshot.docs[0].data();
                        }
                      }
                      return Scaffold(
                        key: _scaffoldKey,
                        appBar: AppBar(
                          centerTitle: true,
                          backgroundColor: Colors.white,
                          iconTheme: IconThemeData(color: Colors.black),
                          title: Column(
                            children: <Widget>[
                              Text(widget.codeEmiten,
                                  style: TextStyle(color: Colors.black)),
                              BlocBuilder<EmitenProfileBloc,
                                      EmitenProfileState>(
                                  builder: (context, state) {
                                if (state is EmitenProfileLoaded) {
                                  _financialBloc.add(FinancialRequested(
                                      uuid: state.emitenProfile.data.uuid));
                                  return Text(
                                      state.emitenProfile.data.companyName,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.black));
                                } else {
                                  return CupertinoActivityIndicator(radius: 5);
                                }
                              })
                            ],
                          ),
                        ),
                        bottomNavigationBar:
                            BlocConsumer<EmitenProfileBloc, EmitenProfileState>(
                                listener: (context, state) {
                          if (state is EmitenProfileLoaded) {
                            uuid = state.emitenProfile.data.uuid;
                            _financialChartBloc.add(LoadChart(uuid));
                          }
                        }, builder: (context, state) {
                          if (state is EmitenProfileLoaded) {
                            companyName = state.emitenProfile.data.companyName;
                            var emitenProfile = state.emitenProfile;
                            uuid = state.emitenProfile.data.uuid;
                            return BottomAppBar(
                                child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  padding: EdgeInsets.fromLTRB(12, 8, 12, 4),
                                  height: 55,
                                  width: double.infinity,
                                  child: FlatButton(
                                    key: Key('btn_beli_in_detail_saham'),
                                    onPressed: () {
                                      if (token == null) {
                                        PopupHelperMarket
                                            .needLoginToAccessThisPage(
                                                context: context, isGoBack: 0);
                                      } else {
                                        _checkTodayBloc.add(CheckTodayRequested(
                                            uuid: uuid,
                                            codeEmiten: widget.codeEmiten,
                                            type: TransactionType.beli,
                                            price: snapshot == null
                                                ? state.emitenProfile.data.price
                                                : snapshot["price"]));
                                      }
                                    },
                                    child: Text(
                                      "Beli Saham",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Color(0xFF0E7E4A),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(12, 4, 12, 8),
                                  height: 55,
                                  width: double.infinity,
                                  child: FlatButton(
                                    disabledColor: Color(0xFF8E8E8E),
                                    key: Key('btn_beli_in_detail_saham'),
                                    onPressed: emitenProfile.data.isEmitenOwner
                                        ? () {
                                            if (token == null) {
                                              PopupHelperMarket
                                                  .needLoginToAccessThisPage(
                                                      context: context,
                                                      isGoBack: 0);
                                            } else {
                                              _checkTodayBloc.add(
                                                  CheckTodayRequested(
                                                      uuid: uuid,
                                                      codeEmiten:
                                                          widget.codeEmiten,
                                                      type:
                                                          TransactionType.jual,
                                                      price: snapshot == null
                                                          ? 0
                                                          : snapshot["price"]));
                                            }
                                          }
                                        : null,
                                    child: Text(
                                      "Jual Saham",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Color(0xFFEB5255),
                                  ),
                                ),
                              ],
                            ));
                          } else {
                            return Container();
                          }
                        }),
                        body:
                            BlocBuilder<EmitenProfileBloc, EmitenProfileState>(
                                builder: (context, state) {
                          if (state is EmitenProfileLoaded) {
                            var emitenProfile = state.emitenProfile;
                            return NestedScrollView(
                              physics: ClampingScrollPhysics(),
                              headerSliverBuilder: (context, value) {
                                return [
                                  SliverToBoxAdapter(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8),
                                          child: SantaraAppBetaTesting(),
                                        ),
                                        _infoLastPrice(emitenProfile, data),
                                        _tabChart()
                                      ],
                                    ),
                                  ),
                                  SliverToBoxAdapter(
                                    child: _sahamTabs(),
                                  )
                                ];
                              },
                              body: Container(
                                child: TabBarView(
                                    controller: _emitenTab,
                                    children: [
                                      _profile(),
                                      _orderBook(emitenProfile),
                                      _financial(),
                                      _fundamental()
                                    ]),
                              ),
                            );
                          } else {
                            return Container();
                          }
                        }),
                      );
                    }),
              );
            });
      },
    );
  }

  Widget _infoLastPrice(
      EmitenProfileModel emitenProfile, List<Watchlist> data) {
    List choices = [
      // {
      //   "id": "0",
      //   "name": "Share",
      //   "icon": Icon(
      //     Icons.share,
      //     size: 20,
      //   )
      // },
      {
        "id": "1",
        "name": "Tambahkan ke Watchlist",
        "icon": Icon(Icons.favorite_border)
      }
    ];
    int watchlistId;
    bool isFavorite = false;
    if (data != null) {
      for (var i = 0; i < data.length; i++) {
        if (emitenProfile.data.id == data[i].emitenId) {
          isFavorite = true;
          watchlistId = data[i].id;
          i = data.length;
        } else {
          isFavorite = false;
        }
      }
    }
    Query query = FirebaseFirestore.instance
        .collection("current_price_${emitenProfile.data.codeEmiten}");
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Last Price (Informasi harga terakhir)",
                  style: TextStyle(fontSize: 12)),
              StreamBuilder<Object>(
                  stream: query.snapshots(),
                  builder: (context, stream) {
                    QuerySnapshot querySnapshot = stream.data;
                    var data;
                    if (stream.connectionState == ConnectionState.active) {
                      if (querySnapshot.docs.length > 0) {
                        data = querySnapshot.docs[0].data();
                      }
                    }
                    return Row(
                      children: <Widget>[
                        stream.connectionState == ConnectionState.waiting
                            ? Center(child: CupertinoActivityIndicator())
                            : Text(
                                "Rp ${NumberFormatter.convertNumber(data == null ? emitenProfile.data.price : data["price"])}",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                        Container(width: 8),
                        stream.connectionState == ConnectionState.waiting
                            ? Center(child: CupertinoActivityIndicator())
                            : stream.hasError
                                ? Container()
                                : data == null
                                    ? Padding(
                                        padding: const EdgeInsets.only(
                                            left: 4, right: 4),
                                        child: Icon(MdiIcons.circleDouble,
                                            color: Colors.orange, size: 16),
                                      )
                                    : data["price"] > data["prev"]
                                        ? Icon(Icons.arrow_drop_up,
                                            color: Colors.green)
                                        : data["price"] < data["prev"]
                                            ? Icon(
                                                Icons.arrow_drop_down,
                                                color: Colors.red,
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 4, right: 4),
                                                child: Icon(
                                                    MdiIcons.circleDouble,
                                                    color: Colors.orange,
                                                    size: 16),
                                              ),
                        Text(
                            stream.connectionState == ConnectionState.active
                                ? data == null
                                    ? "0.0 %"
                                    : "${((data["price"] - data["prev"]) / data["prev"] * 100).toStringAsFixed(1)} %"
                                : "",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: data == null
                                    ? Colors.orange
                                    : data["price"] > data["prev"]
                                        ? Colors.green
                                        : data["price"] < data["prev"]
                                            ? Colors.red
                                            : Colors.orange)),
                      ],
                    );
                  })
            ],
          ),
          PopupMenuButton(
            elevation: 3.2,
            onSelected: (value) {
              if (value["id"] == "1") {
                if (isFavorite) {
                  _watchlistBloc.add(DeleteWatchlistRequested(id: watchlistId));
                } else {
                  _watchlistBloc
                      .add(AddWatchlistRequested(id: emitenProfile.data.id));
                }
              }
              // } else if (value["id"] == "0") {
              //   Share.share(
              //       'Code Emiten : ${emitenProfile.data.codeEmiten}\nEmiten Company Name : ${emitenProfile.data.companyName}\nEmiten Trademark : ${emitenProfile.data.trademark}');
              // }
            },
            itemBuilder: (BuildContext context) {
              return choices.map((choice) {
                return PopupMenuItem(
                    value: choice,
                    child: ListTile(
                      contentPadding: EdgeInsets.all(0),
                      leading: choice["id"] == "1"
                          ? isFavorite
                              ? Icon(Icons.favorite,
                                  size: 20, color: Color(0xFFFF6464))
                              : Icon(
                                  Icons.favorite_border,
                                  size: 20,
                                )
                          : choice["icon"],
                      title: Text(
                        choice["id"] == "1"
                            ? isFavorite
                                ? "Hapus dari Watchlist"
                                : "Tambahkan ke Watchlist"
                            : choice["name"],
                        style: TextStyle(fontSize: 14),
                      ),
                    ));
              }).toList();
            },
          )
        ],
      ),
    );
  }

  Widget _tabChart() {
    double fontSize = MediaQuery.of(context).size.width / 37.5 > 15
        ? 15
        : MediaQuery.of(context).size.width / 37.5;
    TextStyle option =
        TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold);
    TextStyle selected = TextStyle(
        fontSize: fontSize,
        color: Color(0xFFBF2D30),
        fontWeight: FontWeight.bold);
    return Container(
      padding: EdgeInsets.all(12),
      height: MediaQuery.of(context).size.width / 2 + 95,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
              height: MediaQuery.of(context).size.width / 2,
              child: TabBarView(controller: _chartTab, children: [
                // line
                Container(
                    margin: EdgeInsets.only(top: 4),
                    child: SyncfusionChart(
                      codeEmiten: widget.codeEmiten,
                      type: ChartTypeSyncfusion.line,
                    )),
                // candle
                Container(
                    margin: EdgeInsets.only(top: 4),
                    child: SyncfusionChart(
                      codeEmiten: widget.codeEmiten,
                      type: ChartTypeSyncfusion.candle,
                    )),
                // ohlc
                Container(
                    margin: EdgeInsets.only(top: 4),
                    child: SyncfusionChart(
                      codeEmiten: widget.codeEmiten,
                      type: ChartTypeSyncfusion.ohlcv,
                    )),
              ])),

          /// chart period
          BlocBuilder<CurrentTimeBloc, CurrentTimeState>(
            builder: (context, state) {
              DateTime weekAgo;
              DateTime monthAgo;
              DateTime yearAgo;
              if (state is CurrentTimeLoaded) {
                now = state.now.add(Duration(days: 1));
                weekAgo = now.subtract(Duration(days: 7));
                monthAgo = now.subtract(Duration(days: 30));
                yearAgo = now.subtract(Duration(days: 365));
                return BlocBuilder<DataChartBloc, DataChartState>(
                  builder: (context, state) {
                    var time;
                    if (state is DataChartLoaded) {
                      time = state.time;
                    }
                    if (state is DataChartDailyLoaded) {
                      time = state.time;
                    }
                    return Container(
                      padding: EdgeInsets.only(bottom: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                            onTap: () => _dataChartBloc.add(DataChartRequested(
                                codeEmiten: widget.codeEmiten)),
                            child: Text("1HARI",
                                style: time == 'daily' ? selected : option),
                          ),
                          GestureDetector(
                              onTap: () => _dataChartBloc.add(DataChartWeekly(
                                  codeEmiten: widget.codeEmiten,
                                  start: weekAgo,
                                  end: now)),
                              child: Text("1MGG",
                                  style: time == 'weekly' ? selected : option)),
                          GestureDetector(
                            onTap: () => _dataChartBloc.add(DataChartMonthly(
                                codeEmiten: widget.codeEmiten,
                                start: monthAgo,
                                end: now)),
                            child: Text("1BLN",
                                style: time == 'monthly' ? selected : option),
                          ),
                          GestureDetector(
                            onTap: () => _dataChartBloc.add(DataChartAnnual(
                                codeEmiten: widget.codeEmiten,
                                start: yearAgo,
                                end: now)),
                            child: Text("1THN",
                                style: time == 'annual' ? selected : option),
                          ),
                          GestureDetector(
                            onTap: () => _dataChartBloc.add(
                                DataChartAll(codeEmiten: widget.codeEmiten)),
                            child: Text("SEMUA",
                                style: time == 'all' ? selected : option),
                          ),
                        ],
                      ),
                    );
                  },
                );
              } else {
                return Container();
              }
            },
          ),

          /// chart type
          Container(
            // width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                border: Border.all(color: Color(0xFFB8B8B8), width: 1),
                borderRadius: BorderRadius.circular(4)),
            child: TabBar(
              controller: _chartTab,
              indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(
                      color: Color(0xFFf7faff),
                      width: 47.0,
                      style: BorderStyle.solid),
                  insets: EdgeInsets.fromLTRB(0, 0, 0, 0)),
              labelColor: Colors.black,
              unselectedLabelColor: Color(0xFF9A9A9A),
              isScrollable: true,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorWeight: 1,
              onTap: (value) async {
                switch (value) {
                  case 0:
                    await _trailsServices.createTrails(
                        event: marketTrailsData["line_chart_view"]["event"],
                        note: marketTrailsData["line_chart_view"]["note"]);
                    break;
                  case 1:
                    await _trailsServices.createTrails(
                        event: marketTrailsData["candlestick_view"]["event"],
                        note: marketTrailsData["candlestick_view"]["note"]);
                    break;
                  case 2:
                    await _trailsServices.createTrails(
                        event: marketTrailsData["ohlc_bar_view"]["event"],
                        note: marketTrailsData["ohlc_bar_view"]["note"]);
                    break;
                  default:
                    break;
                }
              },
              tabs: [
                new Tab(
                  key: Key('line_chart_tabbar'),
                  child: Container(
                    width: (MediaQuery.of(context).size.width - 120) / 3,
                    child: Center(child: Text("Line Chart")),
                  ),
                ),
                new Tab(
                  key: Key('candle_tabbar'),
                  child: Container(
                    width: (MediaQuery.of(context).size.width - 120) / 3,
                    child: Center(child: Text("Candlestick")),
                  ),
                ),
                new Tab(
                  key: Key('ohlc_tabbar'),
                  child: Container(
                    width: (MediaQuery.of(context).size.width - 120) / 3,
                    child: Center(child: Text("Bar OHLC")),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _sahamTabs() {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(blurRadius: 2, color: Colors.grey[400], offset: Offset(0, 1))
      ]),
      child: TabBar(
          controller: _emitenTab,
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          isScrollable: true,
          tabs: [
            Container(
                key: Key('tab_profile_emiten'),
                height: 40,
                child: Center(child: Text("Profil"))),
            Container(
                key: Key('tab_orderbook_emiten'),
                height: 40,
                child: Center(child: Text("Orderbook"))),
            Container(
                key: Key('tab_financial_emiten'),
                height: 40,
                child: Center(child: Text("Finansial"))),
            Container(
                key: Key('tab_fundamental_emiten'),
                height: 40,
                child: Center(child: Text("Fundamental"))),
          ]),
    );
  }

  Widget _profile() {
    return BlocBuilder<EmitenProfileBloc, EmitenProfileState>(
        builder: (context, state) {
      if (state is EmitenProfileLoaded) {
        emitenProfile = state.emitenProfile;
        return Padding(
            padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
            child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowGlow();
                  return true;
                },
                child: ListView(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      // latar belakang
                      _itemTitleProfile("Latar Belakang Perusahaan"),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 12),
                        child: ExpandText(
                          emitenProfile.data.businessDescription,
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.justify,
                          expandArrowStyle: ExpandArrowStyle.text,
                          collapsedHint: "Lihat Selengkapnya ...",
                          expandedHint: "Lihat Lebih Sedikit",
                          hintTextStyle:
                              TextStyle(color: Color(0xFF218196), fontSize: 12),
                        ),
                      ),

                      // info pendanaan perdana
                      _itemTitleProfile("Info Pendanaan Perdana"),
                      _itemInfoPendanaan("Harga Saham Perdana",
                          ": Rp ${NumberFormatter.convertNumber(emitenProfile.data.price)}"),
                      _itemInfoPendanaan("Total Saham",
                          ": ${NumberFormatter.convertNumber(emitenProfile.data.supply)} lembar"),
                      _itemInfoPendanaan("Total Saham (Rp.)",
                          ": Rp ${NumberFormatter.convertNumber(emitenProfile.data.capitalNeeds)}"),
                      Container(height: 12),

                      // info update perusahaan
                      emitenProfile.data.emitenDividen == null
                          ? Container()
                          : _itemTitleProfile("Update Perusahaan "),
                      emitenProfile.data.emitenDividen == null
                          ? Container()
                          : Container(height: 2, color: Color(0xFFF4F4F4)),
                      emitenProfile.data.emitenDividen == null
                          ? Container()
                          : Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Center(
                                          child: Text("Aktifitas Perusahaan",
                                              style: TextStyle(
                                                  fontWeight:
                                                      FontWeight.bold)))),
                                  Expanded(
                                      child: Center(
                                          child: Text("Tanggal",
                                              style: TextStyle(
                                                  fontWeight:
                                                      FontWeight.bold)))),
                                ],
                              ),
                            ),
                      emitenProfile.data.emitenDividen == null
                          ? Container()
                          : Container(height: 2, color: Color(0xFFF4F4F4)),
                      emitenProfile.data.emitenDividen == null
                          ? Container()
                          : ListView.separated(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount:
                                  emitenProfile.data.emitenDividen.length,
                              separatorBuilder: (_, i) {
                                return Container(height: 8);
                              },
                              itemBuilder: (_, i) {
                                return Row(
                                  children: [
                                    Expanded(
                                        child: Center(
                                            child: Text(emitenProfile
                                                .data.emitenDividen[i].title))),
                                    Expanded(
                                        child: Center(
                                            child: Text(
                                                DateFormat('dd MMM yyyy')
                                                    .format(emitenProfile
                                                        .data
                                                        .emitenDividen[i]
                                                        .createdAt)))),
                                  ],
                                );
                              }),

                      // profil tim
                      _itemTitleProfile("Profil Team"),
                      emitenProfile.data.emitenTeam == null
                          ? Container(child: Text("-"))
                          : ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: emitenProfile.data.emitenTeam.length,
                              itemBuilder: (_, i) {
                                return _itemProfile(
                                    emitenProfile.data.emitenTeam[i].position,
                                    emitenProfile.data.emitenTeam[i].name);
                              }),
                      Container(height: 16),

                      // alamat
                      _itemTitleProfile("Alamat Kantor Utama"),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16),
                        child: Text(
                          emitenProfile.data.address,
                          style: TextStyle(fontSize: 12),
                        ),
                      ),

                      // kontak
                      _itemTitleProfile("Kontak"),
                      _itemProfile(
                          "Email",
                          emitenProfile.data.email.valid
                              ? emitenProfile.data.email.string
                              : "-"),
                      _itemProfile(
                          "No. Telp",
                          emitenProfile.data.telephone.valid
                              ? emitenProfile.data.telephone.int64.toString()
                              : "-"),
                      _itemProfile(
                          "Fax",
                          emitenProfile.data.fax.valid
                              ? emitenProfile.data.fax.int64.toString()
                              : "-"),
                      Container(height: 16),

                      // media sosial
                      emitenProfile.data.instagram.valid ||
                              emitenProfile.data.facebook.valid ||
                              emitenProfile.data.twitter.valid ||
                              emitenProfile.data.youtube.valid
                          ? _itemTitleProfile("Media Sosial")
                          : Container(),
                      emitenProfile.data.instagram.valid ||
                              emitenProfile.data.facebook.valid ||
                              emitenProfile.data.twitter.valid ||
                              emitenProfile.data.youtube.valid
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 16),
                              child: Row(
                                children: <Widget>[
                                  emitenProfile.data.instagram.valid
                                      ? _itemSosmed(
                                          emitenProfile.data.instagram.string,
                                          "assets/icon_socmed/ig.png")
                                      : Container(),
                                  emitenProfile.data.facebook.valid
                                      ? _itemSosmed(
                                          emitenProfile.data.facebook.string,
                                          "assets/icon_socmed/fb.png")
                                      : Container(),
                                  emitenProfile.data.twitter.valid
                                      ? _itemSosmed(
                                          emitenProfile.data.twitter.string,
                                          "assets/icon_socmed/tt.png")
                                      : Container(),
                                  emitenProfile.data.youtube.valid
                                      ? _itemSosmed(
                                          emitenProfile.data.youtube.string,
                                          "assets/icon_socmed/yt.png")
                                      : Container(),
                                ],
                              ),
                            )
                          : Container(),

                      Container(height: 16),
                    ])));
      } else {
        return Container();
      }
    });
  }

  Widget _itemSosmed(String url, String image) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: InkWell(
        onTap: () async {
          if (await canLaunch(url)) {
            await launch(url);
          } else {
            throw 'Cannot launch $url';
          }
        },
        child: Image.asset(
          image,
          height: 30,
          width: 30,
        ),
      ),
    );
  }

  Widget _itemTitleProfile(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
      child: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _itemProfile(String title, String value) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        children: <Widget>[
          Expanded(flex: 1, child: Text(title, style: TextStyle(fontSize: 12))),
          Expanded(
              flex: 2,
              child: Text(
                value,
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              )),
        ],
      ),
    );
  }

  Widget _itemInfoPendanaan(String title, String value) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Text(title,
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
          ),
          Expanded(
            flex: 3,
            child: Text(
              value,
              style: TextStyle(fontSize: 12),
            ),
          ),
        ],
      ),
    );
  }

  Widget _orderBook(EmitenProfileModel emitenProfile) {
    Query query = FirebaseFirestore.instance
        .collection("current_price_${emitenProfile.data.codeEmiten}");
    return BlocBuilder<EmitenProfileBloc, EmitenProfileState>(
        builder: (context, state) {
      if (state is EmitenProfileLoaded) {
        var emitenProfile = state.emitenProfile;
        return StreamBuilder<QuerySnapshot>(
            stream: query.snapshots(),
            builder: (context, stream) {
              QuerySnapshot querySnapshot = stream.data;
              var data;
              if (stream.connectionState == ConnectionState.active) {
                if (querySnapshot.docs.length > 0) {
                  data = querySnapshot.docs[0].data();
                }
              }
              return Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                child: NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overscroll) {
                    overscroll.disallowGlow();
                    return true;
                  },
                  child: ListView(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text("Orderbook",
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold)),
                          Tooltip(
                            message: "Info",
                            child: IconButton(
                              icon: Icon(Icons.info, size: 18),
                              onPressed: () {
                                PopupHelperMarket.showTooltip(
                                    context,
                                    "Orderbook",
                                    "Menampilkan list atau daftar harga jual dan harga beli beserta dengan jumlah saham pada masing-masing transaksi yang tersedia di pasar saat ini.");
                              },
                              color: Color(0xFFC8C8C8),
                            ),
                          )
                        ],
                      ),
                      InfoPrice(
                          codeEmiten: emitenProfile.data.codeEmiten,
                          price: data == null
                              ? emitenProfile.data.price
                              : data["price"],
                          prev: data == null
                              ? emitenProfile.data.price
                              : data["prev"]),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: table.TableJualBeli(
                              isJual: false,
                              prev: data == null
                                  ? emitenProfile.data.price
                                  : data["prev"],
                              codeEmiten: emitenProfile.data.codeEmiten,
                            ),
                          ),
                          Container(
                            width: 8,
                          ),
                          Flexible(
                            child: table.TableJualBeli(
                              isJual: true,
                              prev: data == null
                                  ? emitenProfile.data.price
                                  : data["prev"],
                              codeEmiten: emitenProfile.data.codeEmiten,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            });
      } else {
        return Container();
      }
    });
  }

  Widget _financial() {
    return Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
              return true;
            },
            child: ListView(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                children: [
                  _financialInfo(),
                  // _financialChart(),
                  Container(height: 20),
                  _buildFinansial(),
                ])));
  }

  Widget _financialInfo() {
    return BlocBuilder<FinancialBloc, FinancialState>(
      builder: (context, state) {
        if (state is FinancialLoaded) {
          return ListView(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            children: [
              Container(height: 20),
              RichText(
                text: TextSpan(
                  style: TextStyle(fontFamily: 'Nunito'),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Kinerja ${widget.codeEmiten}, ${now.year} ',
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                    TextSpan(
                        text:
                            '(Par Value Rp ${NumberFormatter.convertNumber(emitenProfile.data.price)} /L) ',
                        style: TextStyle(color: Colors.black)),
                  ],
                ),
              ),
              Container(height: 16),
              Row(
                children: [
                  _financialItem(
                      "Net Income",
                      "Profit / pendapatan bersih",
                      "Rp " +
                          NumberFormatter.convertNumberDecimal(
                              state.financial.data.netIncome) +
                          " /L"),
                  Container(width: 10),
                  _financialItem(
                      "Sales Revenue",
                      "Omset / pendapatan kotor",
                      "Rp " +
                          NumberFormatter.convertNumberDecimal(
                              state.financial.data.salesRevenue) +
                          " /L")
                ],
              ),
              Container(height: 10),
              Row(
                children: [
                  _financialItem(
                      "PER",
                      "Harga per lembar saham / EPS ",
                      NumberFormatter.convertNumberDecimal(
                          state.financial.data.per)),
                  Container(width: 10),
                  _financialItem(
                      "EPS",
                      "Profit / total saham dalam pasar (lembar)",
                      "Rp " +
                          NumberFormatter.convertNumberDecimal(
                              state.financial.data.eps) +
                          " /L")
                ],
              ),
              Container(height: 10),
              Row(
                children: [
                  _financialItem(
                      "Dividen Yield",
                      "Profit 1 tahun / Dividen 1 tahun",
                      NumberFormatter.convertNumberDecimal(
                              state.financial.data.dividendYeild) +
                          "% p.a"),
                  Container(width: 10),
                  _financialItem(
                      "DPS",
                      "Dividen / total saham dalam pasar (lembar)",
                      "Rp " +
                          NumberFormatter.convertNumberDecimal(
                              state.financial.data.dps) +
                          " /L")
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text("*Annual",
                      style: TextStyle(fontStyle: FontStyle.italic)),
                ),
              ),
              Container(
                height: 10,
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget _financialItem(String title, String desc, String value) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 12),
        height: 70,
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xFFB8B8B8)),
            borderRadius: BorderRadius.circular(6)),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text(title),
                  Container(width: 8),
                  GestureDetector(
                      onTap: () =>
                          PopupHelperMarket.showTooltip(context, null, desc),
                      child:
                          Icon(Icons.info, size: 16, color: Color(0xFFA8A8A8))),
                ],
              ),
              Text(value,
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFFD23737),
                      fontWeight: FontWeight.bold)),
            ]),
      ),
    );
  }

  // build finansial
  Widget _buildFinansial() {
    // Emiten emiten = data.emiten;
    return BlocBuilder<FinanceChartBloc, ChartState>(
      builder: (context, state) {
        if (state is ChartUninitialized) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is ChartError) {
          return Center(
            child: Text("${state.error}"),
          );
        } else if (state is ChartEmpty) {
          return Center(
            child: Container(
              padding: EdgeInsets.all(Sizes.s10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    "assets/icon/search_not_found.png",
                    height: Sizes.s200,
                  ),
                  Text(
                    "Belum Ada Laporan Keuangan",
                    style: TextStyle(
                      fontSize: FontSize.s14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: Sizes.s20),
                  Text(
                    "Penerbit ini belum memiliki laporan keuangan",
                    style: TextStyle(
                      fontSize: FontSize.s12,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  // _buildButtons(emiten.prospektus),
                ],
              ),
            ),
          );
        } else if (state is ChartLoaded) {
          return BuildFinancialChart(data: state.data);
        } else {
          return Container();
        }
      },
    );
  }

  Widget _fundamental() {
    TextStyle styleTitle = TextStyle(
        fontSize: MediaQuery.of(context).size.width / 32.5 > 15
            ? 15
            : MediaQuery.of(context).size.width / 32.5);
    TextStyle style = TextStyle(
        fontSize: MediaQuery.of(context).size.width / 32.5 > 15
            ? 15
            : MediaQuery.of(context).size.width / 32.5,
        fontWeight: FontWeight.bold);
    Widget _item(String title, TextStyle style, String desc) {
      return SizedBox(
          height: 30,
          // padding: const EdgeInsets.symmetric(vertical: 2),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(title, style: style),
              Container(width: 4),
              GestureDetector(
                onTap: () {
                  PopupHelperMarket.showTooltip(context, null, desc);
                },
                child: Icon(Icons.info,
                    color: Color(0xFFDADADA),
                    size: MediaQuery.of(context).size.width / 32.5 > 18
                        ? 18
                        : MediaQuery.of(context).size.width / 32.5),
              )
            ],
          ));
    }

    Widget _itemValue(String title, TextStyle style) {
      return SizedBox(
          height: 30,
          // padding: const EdgeInsets.symmetric(vertical: 2),
          child: Center(child: Text(title, style: style)));
    }

    return BlocBuilder<FundamentalBloc, FundamentalState>(
      builder: (context, state) {
        if (state is FundamentalLoaded) {
          FundamentalList data = state.fundamental;
          return SingleChildScrollView(
            child: Container(
              height: 432,
              child: ListView.separated(
                  shrinkWrap: true,
                  padding: EdgeInsets.all(8),
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (_, i) {
                    return Container(width: 2, color: Color(0xFFF4F4F4));
                  },
                  itemCount: data.data.length + 1,
                  itemBuilder: (_, i) {
                    if (i == 0) {
                      return Container(
                        width: MediaQuery.of(context).size.width / 3 + 40,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                            SizedBox(
                              height: 50,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Indikator",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                            _item("Sales Growth (YtD)", styleTitle,
                                GlossariumMarket.salesGrowth),
                            _item("GPS", styleTitle, GlossariumMarket.gps),
                            _item("OPM", styleTitle, GlossariumMarket.opm),
                            _item("NPM", styleTitle, GlossariumMarket.npm),
                            _item("Market Cap", styleTitle,
                                GlossariumMarket.marketCap),
                            _item("PER", styleTitle, GlossariumMarket.per),
                            _item("BVPS", styleTitle, GlossariumMarket.bvps),
                            _item("PBV", styleTitle, GlossariumMarket.pbv),
                            _item("ROA", styleTitle, GlossariumMarket.roa),
                            _item("ROE", styleTitle, GlossariumMarket.roe),
                            // _item("ROI", styleTitle, GlossariumMarket.roi),
                            _item("EV/EBITDA", styleTitle,
                                GlossariumMarket.ebitda),
                            _item("Debt to Equity Ratio", styleTitle,
                                GlossariumMarket.der),
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                          ],
                        ),
                      );
                    } else {
                      return Container(
                        width: MediaQuery.of(context).size.width * 2 / 7,
                        child: Column(
                          children: [
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                            SizedBox(
                                height: 50,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Semester " +
                                          data.data[i - 1].period.toString(),
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    Text(
                                      data.data[i - 1].year.toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].sales) +
                                    "%",
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].gps) +
                                    "%",
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].opm) +
                                    "%",
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].npm) +
                                    "%",
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                    data.data[i - 1].marketCap),
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                    data.data[i - 1].per),
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                    data.data[i - 1].bpvs),
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                    data.data[i - 1].pbv),
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].roa) +
                                    "%",
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].roe) +
                                    "%",
                                style),
                            // _itemValue(
                            //     NumberFormatter.convertNumber(
                            //             data.data[i - 1].roi) +
                            //         "%",
                            //     style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                    data.data[i - 1].ev),
                                style),
                            _itemValue(
                                NumberFormatter.convertNumberDecimal(
                                        data.data[i - 1].der) +
                                    "%",
                                style),
                            Container(height: 2, color: Color(0xFFF4F4F4)),
                          ],
                        ),
                      );
                    }
                  }),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
