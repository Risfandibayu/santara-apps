import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/widget/listing/dashboard/IdentitasPenerbitUI.dart';
import 'package:santaraapp/widget/listing/dashboard/InformasiFinansialUI.dart';
import 'package:santaraapp/widget/listing/dashboard/LampiranUI.dart';
import 'package:santaraapp/widget/listing/dashboard/NonFinansialUI.dart';
import 'MediaBisnisUI.dart';

class PengaturanBisnisUI extends StatefulWidget {
  final PralistingEmiten emitenDetail;
  PengaturanBisnisUI({Key key, @required this.emitenDetail}) : super(key: key);
  @override
  _PengaturanBisnisUIState createState() =>
      _PengaturanBisnisUIState(emitenDetail);
}

class _PengaturanBisnisUIState extends State<PengaturanBisnisUI> {
  PralistingEmiten emitenDetail;
  _PengaturanBisnisUIState(this.emitenDetail);
  final ListingApiService apiService = ListingApiService();

  updateEmiten() async {
    // print(">> Update Pralisting Detail...");
    PopupHelper.showLoading(context);
    try {
      await apiService.fetchPralistingDetail(emitenDetail.uuid).then((value) {
        if (value != null && value.statusCode == 200) {
          final res = json.decode(value.body);
          var emiten = PralistingEmiten.fromJson(res);

          setState(() {
            emitenDetail = emiten;
          });
        }
        Navigator.pop(context);
      });
    } catch (e) {
      Navigator.pop(context);
    }
  }

  Widget _pengaturanBuilder(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(5, 40, 5, 20),
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                PengaturanTile(
                    title: "Identitas Calon Penerbit",
                    onTap: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => IdentitasPenerbitUI(
                                    isEdit: true,
                                    emitenDetail: emitenDetail,
                                  )));
                      if (result != null) {
                        updateEmiten();
                      }
                    }),
                PengaturanTile(
                    title: "Informasi Finansial",
                    onTap: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => InformasiFinansialUI(
                                    isEdit: true,
                                    emitenDetail: emitenDetail,
                                  )));
                      if (result != null) {
                        updateEmiten();
                      }
                    }),
                PengaturanTile(
                    title: "Informasi Non Finansial",
                    onTap: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => InformasiNonFinansialUI(
                                    isEdit: true,
                                    emitenDetail: emitenDetail,
                                  )));
                      if (result != null) {
                        updateEmiten();
                      }
                    }),
                PengaturanTile(
                    title: "Lampiran Dokumen",
                    onTap: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LampiranUI(
                                    isEdit: true,
                                    emitenDetail: emitenDetail,
                                  )));
                      if (result != null) {
                        updateEmiten();
                      }
                    }),
                PengaturanTile(
                    title: "Media",
                    onTap: () async {
                      final result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MediaBisnisUI(
                                    isEdit: true,
                                    emitenDetail: emitenDetail,
                                  )));
                      if (result != null) {
                        updateEmiten();
                      }
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, 'refresh');
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
            elevation: 2,
            title: Text("Edit Pengajuan Bisnis",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: _pengaturanBuilder(context)),
    );
  }
}

class PengaturanTile extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  PengaturanTile({@required this.title, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
          onTap: onTap,
          title: Text(
            "$title",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig.safeBlockHorizontal * 3.5),
          ),
          trailing: Icon(Icons.keyboard_arrow_right),
        ),
        Container(
          margin: EdgeInsets.all(5),
          height: 3,
          color: Color(0xffF4F4F4),
          width: double.infinity,
        ),
      ],
    );
  }
}
