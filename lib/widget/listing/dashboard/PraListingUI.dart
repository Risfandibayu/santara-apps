import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/Pralisting.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/listing/dashboard/BisnisDiajukanUI.dart';
import 'package:santaraapp/widget/listing/dashboard/PengajuanBisnisUI.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/pra_listing.dart';
import 'package:santaraapp/widget/listing/detail_emiten/DetailEmitenUI.dart';
import 'package:santaraapp/widget/listing/registrasi/PreRegistrasiUI.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_page.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/presentation/pages/pre_screening_page.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';
import 'package:santaraapp/widget/widget/components/listing/BusinessTile.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBottomSheet.dart';
import 'package:showcaseview/showcaseview.dart';

class PraListingUI extends StatefulWidget {
  @override
  _PraListingUIState createState() => _PraListingUIState();
}

class _PraListingUIState extends State<PraListingUI> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => PralistingProvider()),
      ],
      child: Scaffold(
          body: ShowCaseWidget(
        builder: Builder(builder: (context) => PraListingBody()),
      )),
    );
  }
}

class PraListingBody extends StatefulWidget {
  @override
  _PraListingBodyState createState() => _PraListingBodyState();
}

class _PraListingBodyState extends State<PraListingBody> {
  // Sebenernya udah semua, cuma kurang processing datanya dipindah ke viewmodel pra_listing.dart
  GlobalKey _one = GlobalKey();
  Random random = new Random();
  bool _showCard = true;
  final GlobalKey<ScaffoldState> _drawerKey = new GlobalKey<ScaffoldState>();

  bool _hasKyc = false; // user sudah kyc apa belom sih?

  @override
  void initState() {
    UnauthorizeUsecase.check(context);
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => ShowCaseWidget.of(context).startShowCase([_one]));
    Future.delayed(Duration(milliseconds: 100), () {
      setPralistingData();
    });
  }

  setPralistingData() async {
    final storage = new FlutterSecureStorage();
    var _kyc = await storage.read(key: Constants.statusKyc);
    setState(() => _hasKyc = _kyc == 'true' ? true : false);
    final provider = Provider.of<PralistingProvider>(context, listen: false);

    if (_kyc == 'true') {
      provider.checkHasMadePralisting();
      if (provider.pralistingState == TopPralistingState.loading) {
        await provider.getTopTenPralisting();
      }

      if (provider.randPralistingState == RandPralistingState.none) {
        await provider.getLatestPralisting();
      }
    } else {
      provider.fetchPrelistingNonKyc();
    }
  }

  Widget _appBarBuilder(BuildContext context) {
    final provider = Provider.of<PralistingProvider>(context, listen: false);

    if (!provider.showCaseLoaded) {
      _hasKyc ? provider.getShowCaseStatus() : null;
    }

    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.menu),
        color: Colors.white,
        onPressed: () => _drawerKey.currentState.openDrawer(),
      ),
      elevation: 2,
      title: Text("Pra-Listing",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          )),
      iconTheme: IconThemeData(color: Colors.white),
      backgroundColor: Color(ColorRev.mainBlack),
      centerTitle: true,
      actions: _hasKyc
          ? provider.showDiajukanIcon
              ? <Widget>[
                  provider.showcaseView
                      ? Showcase.withWidget(
                          key: _one,
                          container: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: Container(
                              color: Colors.white,
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Bisnis Diajukan",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14)),
                                  Container(height: 10),
                                  Text(
                                    "Daftar bisnis yang Anda ajukan\nuntuk melakukan pendanaan\ndi Santara. ",
                                    maxLines: 3,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          height: 80,
                          width: 200,
                          child: IconButton(
                              icon: Image.asset(
                                  "assets/icon/pralisting_icon.png"),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => BusinessFiledPage(),
                                  ),
                                );
                              }),
                        )
                      : IconButton(
                          icon: Image.asset("assets/icon/pralisting_icon.png"),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BusinessFiledPage(),
                              ),
                            );
                          }),
                ]
              : null
          : null,
    );
  }

  // Paling banyak diajukan
  Widget _palingBanyak(BuildContext context) {
    final provider = Provider.of<PralistingProvider>(context, listen: true);
    var _size = SizeConfig.safeBlockHorizontal;

    switch (provider.pralistingState) {
      case TopPralistingState.loading:
        return Container(
          height: Sizes.s420,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: _size * 5, bottom: 10, top: 20),
                child: Text(
                  "Paling Banyak Diajukan",
                  style: TextStyle(
                      fontSize: _size * 4.5, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: _size * 5),
                height: Sizes.s350,
                child: ListView.builder(
                    padding: EdgeInsets.all(0),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return GridContentV2Loader();
                    }),
              )
            ],
          ),
        );
        break;
      case TopPralistingState.done:
        return Container(
          margin: EdgeInsets.only(left: _size * 5, bottom: _size * 5),
          height: Sizes.s420,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    left: _size, bottom: Sizes.s10, top: Sizes.s20),
                child: Text(
                  "Paling Banyak Diajukan",
                  style: TextStyle(
                    fontSize: FontSize.s20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: Sizes.s350,
                child: ListView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: provider.topPralisting.data.length != null
                        ? provider.topPralisting.data.length > 10
                            ? 10
                            : provider.topPralisting.data.length
                        : 1,
                    itemBuilder: (context, index) {
                      var _inside = provider.topPralisting.data[index];
                      var _img = _inside.pictures[0].picture;
                      Data dummy = Data(
                        id: index,
                        uuid: _inside.uuid,
                        image: _img,
                        thumbnail: _img,
                        tagName: _inside.category,
                        businessName: _inside.trademark,
                        companyName: _inside.companyName,
                        position: index + 1,
                        submissions: _inside.votes,
                        likes: _inside.likes,
                        isCommented: _inside.isComment == 1 ? true : false,
                        isLiked: _inside.isLikes == 1 ? true : false,
                        isVoted: _inside.isVote == 1 ? true : false,
                        comments: _inside.comments,
                      );
                      return InkWell(
                        onTap: () async {
                          final result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PralistingDetailPage(
                                uuid: _inside.uuid,
                                status: 2,
                                position: index + 1,
                              ),
                            ),
                          );
                          if (result != null) {
                            provider.pralistingState =
                                TopPralistingState.loading;
                            await provider.getTopTenPralisting();
                          }
                        },
                        child: GridContentV2(
                          data: dummy,
                          showPosition: true,
                        ),
                      );
                    }),
              )
            ],
          ),
        );
        break;
      case TopPralistingState.empty:
        return Container();
        break;
      case TopPralistingState.error:
        return Container(
          child: Center(
            child: Container(
              height: SizeConfig.screenHeight / 8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.replay),
                      onPressed: () async {
                        provider.pralistingState = TopPralistingState.loading;
                        await provider.getTopTenPralisting();
                      }),
                  Text("Terjadi Kesalahan"),
                ],
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  Widget _latestPralistingTitle(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(left: _size * 5),
      child: ListTile(
          contentPadding: EdgeInsets.all(0),
          title: Text(
            "Pengajuan Bisnis",
            style:
                TextStyle(fontSize: _size * 4.5, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            "Berbagai UMKM berkualitas siap untuk diajukan",
            style: TextStyle(fontSize: _size * 3.5),
          )),
    );
  }

  Widget _latestPralisting(BuildContext context) {
    final provider = Provider.of<PralistingProvider>(context, listen: false);

    switch (provider.randPralistingState) {
      case RandPralistingState.none:
        return ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return BusinessTileLoader(
              isPengajuan: false,
            );
          },
          itemCount: 10,
        );
        break;
      case RandPralistingState.loading:
        return ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return BusinessTileLoader(
              isPengajuan: false,
            );
          },
          itemCount: 10,
        );
        break;
      case RandPralistingState.empty:
        return Container();
        break;
      case RandPralistingState.error:
        return Container(
          child: Center(
            child: Container(
              height: SizeConfig.screenHeight / 8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.replay),
                      onPressed: () async {
                        provider.randPralistingState =
                            RandPralistingState.loading;
                        await provider.getLatestPralisting();
                      }),
                  Text("Terjadi Kesalahan"),
                ],
              ),
            ),
          ),
        );
        break;
      case RandPralistingState.done:
        return ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: provider.latestPralisting.data.length,
            itemBuilder: (context, index) {
              var _inside = provider.latestPralisting.data[index];
              PralistingEmiten dummy = PralistingEmiten(
                  uuid: "${_inside.uuid}",
                  // category: "${_inside.category}",
                  category: "${_inside.category}",
                  companyName: "${_inside.companyName}",
                  trademark: "${_inside.trademark}",
                  pictures:
                      "${_inside.pictures != null && _inside.pictures.length > 0 ? _inside.pictures[0].picture : ''}",
                  businessDescription: "${_inside.businessDescription}",
                  isLikes: _inside.isLikes ?? 0,
                  isVote: _inside.isVote ?? 0,
                  isComment: _inside.isComment ?? 0,
                  totalComments: _inside.comments ?? 0,
                  totalLikes: _inside.likes ?? 0,
                  totalVotes: _inside.votes ?? 0);
              return BusinessTile(
                onTap: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PralistingDetailPage(
                        uuid: _inside.uuid,
                        status: 2,
                        position: null,
                      ),
                    ),
                  );
                  if (result != null) {
                    provider.randPralistingState = RandPralistingState.loading;
                    await provider.getLatestPralisting();
                  }
                },
                onDelete: null,
                data: dummy,
                isPengajuan: false,
                status: 0,
                showEngagement: true,
              );
            });
        break;
      default:
        return Container();
        break;
    }
  }

  Widget _coronaReliefCard() {
    var _size = SizeConfig.safeBlockHorizontal;
    return Visibility(
      visible: _showCard,
      child: Container(
        margin: EdgeInsets.all(_size * 5),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xffF0F0F0)),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        padding: EdgeInsets.all(_size * 2.75),
        height: _size * 35,
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/icon_relief/corona-1.png",
                  width: _size * 40,
                ),
                Container(
                  width: _size * 3.75,
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "UKM Corona Relief",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: _size * 3.5),
                      ),
                      Container(
                        height: _size * 1.5,
                      ),
                      Text(
                        "Butuh nafas panjang untuk operasional bisnis Anda ? Mari kolaborasi dengan Investor di Santara ",
                        style: TextStyle(fontSize: _size * 2.5),
                      ),
                      Container(
                        height: _size * 1.5,
                      ),
                      Container(
                        height: _size * 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: FlatButton(
                          color: Color(0xffBF2D30),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PreScreeningPage(),
                              ),
                            );
                          },
                          child: Text(
                            "Daftarkan Bisnis",
                            style: TextStyle(
                                fontSize: _size * 2.5, color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Align(
              alignment: Alignment.topLeft,
              child: InkWell(
                  onTap: () {
                    // print("Closed!");
                    setState(() {
                      _showCard = false;
                    });
                  },
                  child: Container(
                    width: _size * 8,
                    height: _size * 8,
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.close,
                        color: Colors.grey,
                        size: _size * 3.5,
                      ),
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget _viewMorePralisting(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Center(
        child: InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PengajuanBisnisUI()));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: _size * 5, top: _size * 2),
        height: 40,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Lihat Semua",
              style: TextStyle(
                  color: Color(0xff218196),
                  fontSize: _size * 3.5,
                  fontWeight: FontWeight.w500),
            ),
            Container(
              width: 10,
            ),
            Icon(
              Icons.keyboard_arrow_right,
              color: Color(0xff218196),
              size: _size * 4.5,
            ),
          ],
        ),
      ),
    ));
  }

  Widget _nonKycPralisting(BuildContext context) {
    final provider = Provider.of<PralistingProvider>(context, listen: true);

    switch (provider.randPralistingState) {
      case RandPralistingState.none:
        return ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return BusinessTileLoader(
              isPengajuan: false,
            );
          },
          itemCount: 10,
        );
        break;
      case RandPralistingState.loading:
        return ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return BusinessTileLoader(
              isPengajuan: false,
            );
          },
          itemCount: 10,
        );
        break;
      case RandPralistingState.empty:
        return Container();
        break;
      case RandPralistingState.error:
        return Container(
          child: Center(
            child: Container(
              height: SizeConfig.screenHeight / 8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.replay),
                      onPressed: () {
                        provider.randPralistingState =
                            RandPralistingState.loading;
                        provider.fetchPrelistingNonKyc();
                      }),
                  Text("Terjadi Kesalahan"),
                ],
              ),
            ),
          ),
        );
        break;
      case RandPralistingState.done:
        return Container(
          margin: EdgeInsets.only(top: 20),
          child: ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: provider.nonKycDatas.length,
              itemBuilder: (context, index) {
                return BusinessTile2(
                  onTap: () {
                    SantaraBottomSheet.show(
                      context,
                      "Maaf tidak dapat mengakses halaman ini, akun anda belum terverifikasi.",
                    );
                  },
                  onDelete: null,
                  data: provider.nonKycDatas[index],
                  isPengajuan: false,
                  showPosition: false,
                  status: 1,
                  showEngagement: true,
                );
              }),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      key: _drawerKey,
      appBar: _appBarBuilder(context),
      drawer: CustomDrawer(),
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(ColorRev.mainBlack),
            child: SingleChildScrollView(
              child: _hasKyc
                  ? Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: SantaraAppBetaTesting(),
                        ),
                        _coronaReliefCard(),
                        _palingBanyak(context),
                        _latestPralistingTitle(context),
                        _latestPralisting(context),
                        _viewMorePralisting(context)
                      ],
                    )
                  : _nonKycPralisting(context),
            ),
          ),
          KycNotificationStatus(
            showCloseButton: false,
          )
        ],
      ),
    );
  }
}
