import 'dart:math';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/pengajuan_bisnis.dart';
import 'package:santaraapp/widget/listing/detail_emiten/DetailEmitenUI.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/listing/BisnisItem.dart';
import 'package:santaraapp/widget/widget/components/listing/BusinessTile.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraDropDown.dart';

class PengajuanBisnisUI extends StatefulWidget {
  @override
  _PengajuanBisnisUIState createState() => _PengajuanBisnisUIState();
}

class _PengajuanBisnisUIState extends State<PengajuanBisnisUI> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => PengajuanBisnisProvider()),
      ],
      child: PengajuanBisnisBody(),
    );
  }
}

class PengajuanBisnisBody extends StatefulWidget {
  @override
  _PengajuanBisnisBodyState createState() => _PengajuanBisnisBodyState();
}

class _PengajuanBisnisBodyState extends State<PengajuanBisnisBody> {
  // Penambahan pagination
  ScrollController _controller;
  Random random = new Random();
  Widget _filterBuilder(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: true);
    if (provider.filterState == FilterState.loading) {
      provider.fetchFilter();
    }
    return provider.filterState == FilterState.loading
        ? KategoriLoader()
        : Container(
            child: Expanded(
              flex: 1,
              child: SantaraDropDown(
                icon: Image.asset(
                  "assets/icon/category_filter.png",
                ),
                isExpanded: true,
                hint: Text("Semua"),
                items: provider.filter.map((Category e) {
                  return DropdownMenuItem(
                      value: e.id,
                      child: Container(
                          margin: EdgeInsets.only(
                              bottom: e.category.length > 10 ? _size * 2 : 0),
                          child: Text(e.category)));
                }).toList(),
                value: provider.filterBy,
                onChanged: (selected) {
                  provider.filterBy = selected;
                },
              ),
            ),
          );
  }

  Widget _sortBuilder(BuildContext context) {
    // Sort ternyata hardcode
    // ganti SortState jadi filterstate ( Biar sama kaya filter oading time nya)
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    if (provider.sortState == SortState.loading) {
      provider.fetchSort();
    }
    return provider.filterState == FilterState.loading
        ? KategoriLoader()
        : Expanded(
            flex: 1,
            child: SantaraDropDown(
              icon: Image.asset(
                "assets/icon/sort_filter.png",
              ),
              isExpanded: true,
              hint: Text("Semua"),
              items: provider.sort.map((e) {
                return DropdownMenuItem(value: e, child: Text(e["name"]));
              }).toList(),
              value: provider.sortBy,
              onChanged: (selected) {
                // print(selected);
                provider.sortBy = selected;
              },
            ),
          );
  }

  Widget _contentBuilder(BuildContext context) {
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    return provider.bisnisState == PengajuanBisnisState.loading
        ? ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 5,
            itemBuilder: (context, index) {
              return BusinessTileLoader();
            })
        : provider.bisnisState == PengajuanBisnisState.error
            ? Center(
                child: Container(
                  height: SizeConfig.screenHeight / 8,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.replay),
                          onPressed: () async {
                            provider.bisnisState = PengajuanBisnisState.loading;
                            await provider.fetchData();
                          }),
                      Text("Terjadi Kesalahan"),
                    ],
                  ),
                ),
              )
            : provider.data.length < 1
                ? Center(
                    child: contentNotFound(context),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return BusinessTile2(
                        onTap: () async {
                          final result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PralistingDetailPage(
                                uuid: provider.data[index].uuid,
                                status: 2,
                                position: null,
                              ),
                            ),
                          );
                          if (result != null) {
                            await provider.clearData();
                            provider.bisnisState = PengajuanBisnisState.loading;
                            await provider.fetchData();
                          }
                        },
                        onDelete: null,
                        data: provider.data[index],
                        isPengajuan: false,
                        showPosition: false,
                        status: 1,
                        showEngagement: true,
                      );
                    },
                    itemCount: provider.data.length);
  }

  Widget _pagination(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      padding: EdgeInsets.all(_size * 2),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(
              "1",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: _size * 3,
            ),
            Text("2"),
            SizedBox(
              width: _size * 3,
            ),
            Text("3"),
            SizedBox(
              width: _size * 3,
            ),
            Text("4"),
            SizedBox(
              width: _size * 3,
            ),
            Text("5"),
            SizedBox(
              width: _size * 3,
            ),
            Container(
              child: Center(
                child: Icon(Icons.keyboard_arrow_right),
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(_size)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey[800].withOpacity(0.5),
                        blurRadius: 3.0,
                        offset: Offset(0.0, 3.75)),
                  ]),
            )
          ],
        ),
      ),
    );
  }

  Widget searchBar(BuildContext context) {
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    return AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: Padding(
          padding: const EdgeInsets.only(right: 8),
          child: TextField(
            textInputAction: TextInputAction.search,
            focusNode: provider.focusNode,
            controller: provider.searchController,
            onSubmitted: (val) {
              provider.keySearch = val;
            },
            decoration: InputDecoration(
                hintText: "Cari Nama Bisnis",
                hintStyle: TextStyle(color: Color(0xff676767)),
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                fillColor: Color(0xffF2F2F2),
                border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(40.0),
                    ),
                    borderSide: BorderSide(color: Color(0xffDDDDDD))),
                suffixIcon: IconButton(
                    padding: EdgeInsets.only(right: 16),
                    color: Colors.black,
                    icon: Icon(Icons.search),
                    onPressed: () {
                      // print("Searching...");
                    })),
          ),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.close),
          color: Colors.black,
          onPressed: () {
            provider.isSearching = false;
          },
        ));
  }

  Widget normalBar(BuildContext context) {
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    return AppBar(
        elevation: 2,
        title: Text("Pengajuan Bisnis",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
            ),
            onPressed: () {
              provider.isSearching = true;
              provider.focusNode.requestFocus();
            },
          )
        ]);
  }

  Widget contentNotFound(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/icon/search_not_found.png"),
          Container(height: 10),
          Text(
            "Mohon Maaf",
            style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          Container(height: 10),
          Text(
            "Data pralisting tidak ditemukan",
            style: TextStyle(
              color: Color(0xff676767),
              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  _scrollListener() {
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        if (provider.page < provider.lastPage) {
          provider.page = provider.page + 1;
          // provider.fetchData();
        }
      });
    }
  }

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
    UnauthorizeUsecase.check(context);
    Future.delayed(Duration(milliseconds: 100), () async {
      final provider =
          Provider.of<PengajuanBisnisProvider>(context, listen: false);
      if (provider.bisnisState == PengajuanBisnisState.loading) {
        await provider.fetchData();
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider =
        Provider.of<PengajuanBisnisProvider>(context, listen: false);
    return Scaffold(
      key: provider.scaffoldKey,
      appBar: provider.isSearching ? searchBar(context) : normalBar(context),
      body: Container(
          height: SizeConfig.screenHeight,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: SingleChildScrollView(
            controller: _controller,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(_size * 5),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _filterBuilder(context),
                      Container(
                        width: _size * 5,
                      ),
                      _sortBuilder(context)
                    ],
                  ),
                ),
                _contentBuilder(context),
                Container(
                  height: _size,
                ),
                provider.paginationState == PaginationState.loading
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: 2,
                        itemBuilder: (context, index) {
                          return BusinessTileLoader();
                        })
                    : Container(),
                //Center(child: _pagination(context)),
                Container(
                  height: _size * 5,
                )
              ],
            ),
          )),
    );
  }
}
