import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/lampiran_bisnis.dart';
import 'package:santaraapp/widget/listing/registrasi/RegistrasiStepsUI.dart';
import 'package:santaraapp/widget/widget/components/listing/ListingDialog.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';

class LampiranUI extends StatefulWidget {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  LampiranUI({@required this.isEdit, @required this.emitenDetail});
  @override
  _LampiranUIState createState() => _LampiranUIState(isEdit, emitenDetail);
}

class _LampiranUIState extends State<LampiranUI> {
  bool isEdit;
  PralistingEmiten emitenDetail;
  _LampiranUIState(this.isEdit, this.emitenDetail);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
          create: (_) => LampiranBisnisProvider(
              isEdit: isEdit, emitenDetail: emitenDetail)),
    ], child: LampiranBody());
  }
}

class LampiranBody extends StatefulWidget {
  @override
  _LampiranBodyState createState() => _LampiranBodyState();
}

class _LampiranBodyState extends State<LampiranBody> {
  // ketika use click (i) ulasan
  void _handleLampiranKnowledge() {
    var _size = SizeConfig.safeBlockHorizontal;
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return ListingAlertDialog(
              title: "Mengapa Prospektus Penting ?",
              subtitle:
                  "Prospektus adalah gabungan antara profil perusahaan dan laporan tahunan yang menjadikannya sebuah dokumen resmi yang digunakan oleh suatu lembaga/ perusahaan untuk memberikan gambaran mengenai saham yang ditawarkannya untuk dijual kepada publik dalam format PDF.Prospektus menjadi salah satu pertimbangan penting investor dalam mengambil keputusan pengajuan bisnis yang Anda ajukan",
              buttonTitle: "Mengerti",
              showCloseButton: true,
              height: _size * 92.5,
              onClose: () => Navigator.pop(context),
              onTap: () => Navigator.pop(context));
        });
  }

  Widget _infoPick(BuildContext context, String message, bool active) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      height: _size * 6,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(
            Icons.info,
            size: _size * 4,
            color: active ? Color(0xff218196) : Colors.grey,
          ),
          Container(
            width: _size,
          ),
          Text(
            message,
            style: TextStyle(
              color: active ? Color(0xff218196) : Colors.grey,
              fontSize: _size * 3,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider = Provider.of<LampiranBisnisProvider>(context, listen: true);
    if (provider.firstFetch) {
      provider.getProspektusFile();
    }
    return WillPopScope(
      onWillPop: () async {
        if (provider.hasMakeChanges) {
          PopupHelper.handleClosePage(context);
          RegistrationHelper.steps.lampiran = false;
        } else {
          Navigator.pop(context, 'refresh');
        }
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 2,
          title: Text("Lampiran",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            provider.isEdit
                ? Container()
                : IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RegistrasiStepsUI(),
                        ),
                      );
                    },
                  )
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            color: Colors.white,
            height: SizeConfig.screenHeight,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SantaraFileField(
                    key: Key('fileProspektus'),
                    title: "Prospektus (PDF)",
                    filename: provider.prospektusFileName,
                    onTap: () {
                      provider.pickProspektusFile();
                    }),
                Container(
                  height: 5,
                ),
                _infoPick(context, "Ukuran file maksimal 25 Mb", false),
                InkWell(
                  onTap: () {
                    _handleLampiranKnowledge();
                  },
                  child:
                      _infoPick(context, "Mengapa Prospektus Penting?", true),
                ),
                provider.prospektusError.isEmpty
                    ? Container()
                    : Container(
                        height: _size * 5,
                        child: Text(
                          "${provider.prospektusError}",
                          style:
                              TextStyle(color: Colors.red, fontSize: _size * 3),
                        ),
                      ),
              ],
            ),
          ),
        ),
        floatingActionButton: Container(
          padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
          child: provider.isEdit
              ? provider.prospektusState == ProspektusState.none ||
                      provider.prospektusState == ProspektusState.error
                  ? SantaraMainButton(
                      key: Key('btnSimpan'),
                      title: "Simpan Perubahan",
                      onPressed: () async {
                        await provider.validatingLampiran().then((value) {
                          if (value) {
                            PopupHelper.handleConfirmation(context, () {
                              Navigator.pop(context);
                              provider.prospektusState =
                                  ProspektusState.updating;
                              provider.simpanPerubahan(context);
                            });
                          }
                        });
                      },
                    )
                  : Container()
              : SantaraMainButton(
                  key: Key('btnSelanjutnya'),
                  title: "Selanjutnya",
                  onPressed: () {
                    provider.nextProccess(context);
                  },
                ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}

// UI YANG LAMA
// SantaraFileField(
//                   title: "Izin Bentuk Badan Usaha (PDF)",
//                   filename: "Bukti Perizinan PT_Maret_2020.PDF",
//                   onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 8.5,
//               ),
//               SantaraFileField(
//                   title: "Dokumen Perizinan (PDF)",
//                   filename: "Akta Perusahaan_Maret_2020.PDF",
//                   onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 3.5,
//               ),
//               SantaraFileField(filename: "SIUP_Maret_2020.PDF", onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 3.5,
//               ),
//               SantaraFileField(filename: "TDP_Maret_2020.PDF", onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 3.5,
//               ),
//               SantaraFileField(filename: "SKDP_Maret_2020.PDF", onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 3.5,
//               ),
//               SantaraFileField(filename: "NPWP_Maret_2020.PDF", onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 7,
//               ),
//               SantaraFileField(
//                   title: "Prospektus (PDF)",
//                   filename: "Prospektus_Sop Ayam Pak Min.PDF",
//                   onTap: null),
//               Container(
//                 height: SizeConfig.safeBlockHorizontal * 10.5,
//               ),
//               SantaraMainButton(
//                 title: "Simpan Perubahan",
//                 onPressed: () {},
//               )
// END
