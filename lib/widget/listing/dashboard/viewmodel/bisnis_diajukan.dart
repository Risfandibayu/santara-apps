import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/DiajukanListModel.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/widget/listing/dashboard/PengaturanBisnisUI.dart';

enum BisnisDiajukanState { loading, loaded, error, empty, unauthorized }
enum KategoriState { loading, loaded }

class StatusParse {
  int id;
  String value;
  String text;
  StatusParse({this.id, this.value, this.text});
}

class BisnisDiajukanProvider with ChangeNotifier {
  List<String> _categories = List<String>();
  StatusParse _selectedStatus;
  List<StatusParse> _status = [
    StatusParse(id: -1, value: "", text: "Semua"),
    StatusParse(
        id: 0, value: "waiting for verification", text: "Menunggu Verifikasi"),
    StatusParse(id: 1, value: "rejected", text: "Tidak lolos screening"),
    // StatusParse(
    //     id: 1, value: "rejected airing", text: "Tidak lolos ditayangkan"),
    // StatusParse(
    //     id: 2, value: "rejected screeening", text: "Tidak lolos screening"),
    StatusParse(id: 3, value: "verified", text: "Lolos Pendanaan"),
  ];

  String _category = ""; // kategori terpilih
  BisnisDiajukanState _state = BisnisDiajukanState.loading;
  KategoriState _kategoriState = KategoriState.loading;
  EmitenDetail _emiten = DataDummy.dummies[0];
  final ListingApiService apiService = ListingApiService();
  List<DiajukanListModel> _data = List<DiajukanListModel>();
  // getter
  List<StatusParse> get status => _status;
  List<String> get categories => _categories;
  String get category => _category;
  List<DiajukanListModel> get data => _data;
  BisnisDiajukanState get state => _state;
  KategoriState get kategoriState => _kategoriState;
  EmitenDetail get emiten => _emiten;
  StatusParse get selected => _selectedStatus;

  Future<void> fetchKategori() async {
    await Future.delayed(Duration(milliseconds: 2000), () {
      _categories = [
        "Semua",
        "Pengajuan Aktif",
        "Pengajuan Diproses",
        "Pengajuan Ditolak",
        "Lolos Pendanaan"
      ];
      _kategoriState = KategoriState.loaded;
      notifyListeners();
    });
  }

  Future<void> hapusPengajuan(BuildContext context, int index) async {
    // print(">> Deleting Emiten Detail..");
    DiajukanListModel _temp = _data[index];
    try {
      Future(() {
        PopupHelper.showLoading(context);
      });
      await apiService.hapusEmiten(_temp.uuid).then((value) {
        if (value.statusCode == 200) {
          _data.removeAt(index);
          notifyListeners();
          Navigator.pop(context);
          ToastHelper.showSuccessToast(
              context, "Berhasil menghapus pralisting!");
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, value.data["message"]);
        }
      });
    } catch (e) {
      // print(">> Catch error");
      // print(e);
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan, tidak dapat menghapus data pralisting!");
    }
  }

  Future<void> repostPralisting(BuildContext context, String uuid) async {
    try {
      PopupHelper.showLoading(context);
      var _resp = await apiService.repostEmiten(uuid);
      var _parsed = json.decode(_resp.body);
      Navigator.pop(context);
      if (_resp.statusCode == 200) {
        state = BisnisDiajukanState.loading;
        fetchBisnis();
        ToastHelper.showSuccessToast(context, _parsed["message"]);
      } else {
        ToastHelper.showFailureToast(context, _parsed["message"]);
      }
    } catch (e) {
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Tidak dapat mengajukan ulang pralisting!");
    }
  }

  Future<void> editPengajuan(BuildContext context, String uuid) async {
    // print(">> Fetch Emiten Detail..");
    try {
      // Future(() {
      PopupHelper.showLoading(context);
      // });
      await apiService.fetchPralistingDetail(uuid).then((value) async {
        if (value != null && value.statusCode == 200) {
          // print(value.uuid);
          final res = json.decode(value.body);
          var emiten = PralistingEmiten.fromJson(res);
          final result = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PengaturanBisnisUI(emitenDetail: emiten)));
          if (result != null) {
            state = BisnisDiajukanState.loading;
            fetchBisnis();
            Navigator.pop(context);
          }
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(
              context, "Tidak dapat mengambil data pralisting!");
        }
      });
    } catch (e) {
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Tidak dapat mengambil data pralisting!");
    }
  }

  Future<void> fetchBisnis({String status = ""}) async {
    // print(">> Fetching Diajukan..");
    try {
      _data.clear();
      var _response = await apiService.fetchBisnisDiajukan(status);
      if (_response != null && _response.statusCode == 200) {
        List<DiajukanListModel> datas = List<DiajukanListModel>();
        final res = json.decode(_response.body);
        res.forEach((val) {
          var dummy = DiajukanListModel.fromJson(val);
          datas.add(dummy);
        });
        _data = datas;
        _state = BisnisDiajukanState.loaded;
      } else if (_response.statusCode == 401) {
        _state = BisnisDiajukanState.unauthorized;
      } else {
        _state = BisnisDiajukanState.empty;
      }
    } catch (e) {
      // print(e);
      _state = BisnisDiajukanState.empty;
    }
    notifyListeners();
  }

  // setter
  set category(String value) {
    _category = value;
    notifyListeners();
  }

  set filter(StatusParse value) {
    _selectedStatus = value;
    notifyListeners();
  }

  set state(BisnisDiajukanState state) {
    _state = state;
    notifyListeners();
  }

  int getIdStatus(String status) {
    var _val = 0;
    _status.forEach((element) {
      if (element.value == status) {
        _val = element.id;
      }
    });
    return _val;
  }
}
