import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:santaraapp/helpers/PermissionHandler.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/services/listing/category_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

enum PengajuanBisnisState { loading, loaded, error, unauthorized }
enum PaginationState { loading, loaded, error }
enum FilterState { loading, loaded }
enum SortState { loading, loaded }

class PengajuanBisnisProvider with ChangeNotifier {
  Random _random = Random();
  final ListingCategoryService categoryService = ListingCategoryService();
  final storage = new FlutterSecureStorage();
  List<Category> _filter = [
    Category(
      id: 0,
      uuid: '',
      category: 'Semua',
    )
  ];
  FilterState _filterState = FilterState.loading;
  List<Map<dynamic, String>> _sort = List();
  SortState _sortState = SortState.loading;
  int _filterBy = 0;
  int _page = 1;
  int _lastPage = 0;
  String _keySearch = "";
  Map<dynamic, String> _sortBy;
  List<DataPralisting> _data = [];
  PengajuanBisnisState _bisnisState = PengajuanBisnisState.loading;
  PaginationState _paginationState = PaginationState.loaded;
  SantaraPermissionHandler _permissionHandler = SantaraPermissionHandler();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  EmitenDetail _emiten = DataDummy.dummies[0];
  final FocusNode focusNode = FocusNode();
  final TextEditingController searchController = TextEditingController();
  bool _isSearching = false;

  // getter
  List<Category> get filter => _filter;
  List<Map<dynamic, String>> get sort => _sort;
  FilterState get filterState => _filterState;
  SortState get sortState => _sortState;
  int get filterBy => _filterBy;
  int get page => _page;
  int get lastPage => _lastPage;
  String get keySearch => _keySearch;
  Map<dynamic, String> get sortBy => _sortBy;
  List<DataPralisting> get data => _data;
  PaginationState get paginationState => _paginationState;
  PengajuanBisnisState get bisnisState => _bisnisState;
  EmitenDetail get emiten => _emiten;
  bool get isSearching => _isSearching;

  set isSearching(bool value) {
    _page = 1;
    _isSearching = value;
    notifyListeners();
  }

  set keySearch(String value) {
    _page = 1;
    _keySearch = value;
    _data.clear();
    _bisnisState = PengajuanBisnisState.loading;
    fetchData();
    notifyListeners();
  }

  set filterBy(int value) {
    _page = 1;
    _filterBy = value;
    _data.clear();
    _bisnisState = PengajuanBisnisState.loading;
    fetchData();
    notifyListeners();
  }

  set bisnisState(PengajuanBisnisState bisnisState) {
    _bisnisState = bisnisState;
    notifyListeners();
  }

  set sortBy(value) {
    _page = 1;
    if (value["name"] == "Lokasi Terdekat") {
      // print(">> Lokasi terdekat!");
      _permissionCheck().then((val) {
        if (val) {
          // print(">> Allowed");
          _sortBy = value;
          _data.clear();
          _bisnisState = PengajuanBisnisState.loading;
          if (_sortBy["name"] == "Lokasi Terdekat") {
            // fetchNearby();
          } else {
            fetchData();
          }
          notifyListeners();
        } else {
          // print(">> Rejected!");
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
              "Mohon izinkan aplikasi untuk mengakses lokasi!",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.redAccent,
          ));
        }
      });
    } else {
      // print(">> Not lokasi terdekat!");
      _sortBy = value;
      _data.clear();
      _bisnisState = PengajuanBisnisState.loading;
      fetchData();
      notifyListeners();
    }
  }

  set page(int value) {
    _page = value;
    _paginationState = PaginationState.loading;
    fetchData();
    notifyListeners();
  }

  // cek location permission
  Future<bool> _permissionCheck() async {
    var _permission = false;
    try {
      await _permissionHandler
          .listenForPermissionStatus(PermissionGroup.location)
          .then((value) async {
        if (value != null) {
          if (value) {
            _permission = value;
          } else {
            await _permissionHandler
                .requestPermission(PermissionGroup.location)
                .then((value) {
              _permission = value;
            });
          }
        } else {
          await _permissionHandler
              .requestPermission(PermissionGroup.location)
              .then((value) {
            _permission = value;
          });
        }
      });
      // print(">> Permission result : $_permission");
      return _permission;
    } catch (e) {
      // print(">> Error while requesting permissions : $e");
      return false;
    }
  }

  Future<void> clearData() async {
    _page = 1;
    _lastPage = 0;
    _data.clear();
    notifyListeners();
  }

  Future<void> fetchData() async {
    String filter = "";
    String sort = "";
    if (filterBy != 0) {
      filter = filterBy.toString();
    }
    if (sortBy != null) {
      sort = sortBy["value"];
    }
    var i = 1;
    bool _done = false;
    while (i <= 5) {
      if (_done) {
        // print(">> Success");
        break;
      }
      if (i > 5) {
        _done = true;
        break;
      }
      await storage.read(key: 'token').then((token) async {
        // print(">> Fetching pengajuan bisnis data... ");
        final response = await http.get(
            '$apiLocal/emitens/pre-listing/paginate?category=$filter&sort=$sort&search=$keySearch&limit=$_page&offset=5',
            headers: {"Authorization": "Bearer $token"});
        if (response.statusCode == 200) {
          var data = pralistingPaginationFromJson(response.body);
          if (_page == 1) {
            _data.clear();
            _data = data.data;
          } else {
            _data.addAll(data.data);
          }
          _lastPage = data.lastPage;
          _paginationState = PaginationState.loaded;
          _bisnisState = PengajuanBisnisState.loaded;
          _done = true;
        } else if (response.statusCode == 401) {
          _data = [];
          _paginationState = PaginationState.loaded;
          _bisnisState = PengajuanBisnisState.unauthorized;
          _done = true;
        } else if (response.statusCode == 404) {
          _data = [];
          _paginationState = PaginationState.loaded;
          _bisnisState = PengajuanBisnisState.loaded;
          _done = true;
        } else {
          _data = [];
          _paginationState = PaginationState.error;
          _bisnisState = PengajuanBisnisState.error;
        }
      });
      i++;
    }
    notifyListeners();
  }

  // fetch data untuk lokasi terdekat
  Future<void> fetchNearby() async {
    // var location = new Location();
    // var longlat = await location.getLocation();
    // var data = {"latitude": longlat.latitude, "longitude": longlat.longitude};
    // // print(">> User Location : ");
    // // print(data);
    // await Future.delayed(Duration(milliseconds: 2000), () {
    //   _data.clear();
    //   // for (var i = 0; i < 10; i++) {
    //   //   Data dummy = Data(
    //   //       id: i,
    //   //       uuid: "xysz$i",
    //   //       thumbnail:
    //   //           "https://www.fifteenspatulas.com/wp-content/uploads/2016/02/Chicken-Noodle-Soup-Fifteen-Spatulas-2-640x427.jpg",
    //   //       image:
    //   //           "https://www.fifteenspatulas.com/wp-content/uploads/2016/02/Chicken-Noodle-Soup-Fifteen-Spatulas-2-640x427.jpg",
    //   //       tagName: "Kuliner",
    //   //       businessName: "Sop Ayam Pak Min",
    //   //       companyName: "PT. Kembar Cipta Boga",
    //   //       submissions: _random.nextInt(100),
    //   //       likes: _random.nextInt(999),
    //   //       comments: _random.nextInt(100),
    //   //       createdAt: "123",
    //   //       updatedAt: "123",
    //   //       position: 1,
    //   //       isLiked: true,
    //   //       isCommented: true,
    //   //       description:
    //   //           "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    //   //       isDeleted: "1",
    //   //       createdBy: "2",
    //   //       updatedBy: "123");
    //   //   _data.add(dummy);
    //   // }
    //   _bisnisState = PengajuanBisnisState.loaded;
    //   notifyListeners();
    // });
  }
  // end

  Future<void> fetchFilter() async {
    // tambahin try catch
    // print(">> Fetching Category..");
    try {
      await categoryService.fetchCategories().then((value) {
        if (value != null) {
          _filter.addAll(value);
        } else {
          ToastHelper.showSnackBar(
              scaffoldKey, "Data kategori tidak ditemukan!", Colors.redAccent);
        }
      });
      _filterState = FilterState.loaded;
      notifyListeners();
    } catch (e) {
      _filterState = FilterState.loaded;
      notifyListeners();
      ToastHelper.showSnackBar(scaffoldKey,
          "Tidak dapat mengambil data kategori!", Colors.redAccent);
      // print(">> An Error occured $e");
    }
  }

  Future<void> fetchSort() async {
    _sort = [
      {"value": "", "name": "Semua"},
      {"value": "true", "name": "Terbaru"},
      {"value": "false", "name": "Terlama"},
      {"value": "vote", "name": "Paling Banyak Diajukan"},
      // {"value": "", "name": "Lokasi Terdekat"},
    ];
    _sortState = SortState.loaded;
    notifyListeners();
  }
}
