import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/models/listing/QuestionModel.dart';
import 'package:santaraapp/services/listing/listing_service.dart';

import '../LampiranUI.dart';

class InformasiNonFinansialProvider with ChangeNotifier {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  InformasiNonFinansialProvider({this.isEdit, this.emitenDetail});

  final ListingApiService listingApiService = ListingApiService();
  bool _isSubmitError = false;
  bool _firstFetch = true;
  bool _hasMakeChanges = false;
  List<QuestionModel> _questionModel = [
    QuestionModel(
        questionId: 1,
        question: "Sistem Pencatatan Keuangan",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1,
              value: 1,
              answer: "Terkomputerisasi/Software akuntansi"),
          AnswerModel(
              answerId: 2, value: 2, answer: "Catatan pembukuan sederhana/POS"),
          AnswerModel(
              answerId: 3, value: 3, answer: "Hanya berupa bukti dokumentasi"),
          AnswerModel(answerId: 4, value: 4, answer: "Tidak ada"),
        ]),
    QuestionModel(
        questionId: 2,
        question: "Reputasi Pinjaman Bank/Lainnya",
        groupValue: -1,
        answers: [
          AnswerModel(answerId: 1, value: 1, answer: "Tidak memiliki pinjaman"),
          AnswerModel(
              answerId: 2, value: 2, answer: "Memiliki pinjaman lancar"),
          AnswerModel(
              answerId: 3, value: 3, answer: "Pernah bermasalah namun lunas"),
          AnswerModel(
              answerId: 4,
              value: 4,
              answer: "Sedang/pernah bermasalah dan belum lunas"),
        ]),
    QuestionModel(
        questionId: 3,
        question: "Posisi Pasar atas Produk/Jasa",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1, value: 1, answer: "Pemimpin pasar lokal/nasional"),
          AnswerModel(
              answerId: 2,
              value: 2,
              answer: "Mampu bersaing di pasar lokal/nasional"),
          AnswerModel(
              answerId: 3,
              value: 3,
              answer: "Berusaha bersaing di pasar lokal/nasional"),
          AnswerModel(
              answerId: 4, value: 4, answer: "Tidak mampu bersaing di pasar"),
        ]),
    QuestionModel(
        questionId: 4,
        question: "Strategi Kedepan",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1,
              value: 1,
              answer:
                  "Punya milestone jangka panjang owner & infrastruktur siap"),
          AnswerModel(
              answerId: 2,
              value: 2,
              answer:
                  "Milestone sedang disusun owner & infrastruktur sedang diperkuat"),
          AnswerModel(
              answerId: 3,
              value: 3,
              answer: "Lebih menekankan strategi jangka pendek agar optimal"),
          AnswerModel(
              answerId: 4,
              value: 4,
              answer: "Strategi case by case/tentavie agar efektif"),
        ]),
    QuestionModel(
        questionId: 5,
        question: "Status Lokasi/kantor/Tempat Usaha",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1, value: 1, answer: "Milik sendiri/sewa > 5 Tahun"),
          AnswerModel(answerId: 2, value: 2, answer: "Sewa > 2 s.d 5 Tahun"),
          AnswerModel(answerId: 3, value: 3, answer: "Sewa < 2 Tahun"),
          AnswerModel(answerId: 4, value: 4, answer: "Sewa Bulanan"),
        ]),
    QuestionModel(
        questionId: 6,
        question: "Tingkat Persaingan",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1, value: 1, answer: "Mampu memenangkan persaingan"),
          AnswerModel(
              answerId: 2,
              value: 2,
              answer: "Mampu bersaing namun bukan pemimpin pasar"),
          AnswerModel(
              answerId: 3,
              value: 3,
              answer: "Berusaha bersaing namun bukan pemimpin pasar"),
          AnswerModel(
              answerId: 4,
              value: 4,
              answer: "Sedang/Pernah bermasalah dan belum lunas"),
        ]),
    QuestionModel(
        questionId: 7,
        question: "Kemampuan Manajerial",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1, value: 1, answer: "Mampu memenangkan persaingan"),
          AnswerModel(
              answerId: 2,
              value: 2,
              answer: "Mampu bersaing namun bukan pemimpin pasar"),
          AnswerModel(
              answerId: 3,
              value: 3,
              answer: "Berusaha bersaing namun bukan pemimpin pasar"),
          AnswerModel(
              answerId: 4, value: 4, answer: "Tidak mampu bersaing di pasar"),
        ]),
    QuestionModel(
        questionId: 8,
        question: "Kemampuan Teknis",
        groupValue: -1,
        answers: [
          AnswerModel(
              answerId: 1,
              value: 1,
              answer: "Owner/Manajemen ahli di bisnis ini"),
          AnswerModel(
              answerId: 2,
              value: 2,
              answer:
                  "Owner/Manajemen baru dibisnis ini namun memiliki pengalaman bisnis yang sejenis"),
          AnswerModel(
              answerId: 3,
              value: 3,
              answer:
                  "Owner/Manajemen belum pernah memiliki keahlian/pengalaman di bisnis ini dan sejenisnya namun telah memiliki pengalaman di sektor lain"),
          AnswerModel(
              answerId: 4,
              value: 4,
              answer:
                  "Owner/Manajemen baru mulai berbisnis/belum ada track record"),
        ]),
  ];

  List<QuestionModel> get questionModel => _questionModel;
  bool get hasMakeChanges => _hasMakeChanges;
  bool get firstFetch => _firstFetch;
  bool get isSubmitError => _isSubmitError;

  void handleSelectAnswer(int index, int value, {bool changes = true}) {
    _questionModel[index].groupValue = value;
    handleHelper(_questionModel[index]);
    _hasMakeChanges = changes;
    notifyListeners();
  }

  handleHelper(QuestionModel question) {
    var _emiten = RegistrationHelper.emiten;
    var answer =
        question.answers.firstWhere((val) => val.value == question.groupValue);
    switch (question.questionId) {
      case 1:
        _emiten.pencatatanKeuangan = answer.answer;
        break;
      case 2:
        _emiten.reputasiPinjaman = answer.answer;
        break;
      case 3:
        _emiten.posisiPasar = answer.answer;
        break;
      case 4:
        _emiten.strategiKedepan = answer.answer;
        break;
      case 5:
        _emiten.statusTempatUsaha = answer.answer;
        break;
      case 6:
        _emiten.tingkatPersaingan = answer.answer;
        break;
      case 7:
        _emiten.kemampuanManajerial = answer.answer;
        break;
      case 8:
        _emiten.kemampuanTeknis = answer.answer;
        break;
      default:
        // print("Not valid");
        break;
    }
  }

  findAnwerValue(int questionIndex, String answer) {
    _questionModel[questionIndex].answers.forEach((val) {
      if (val.answer == answer) {
        handleSelectAnswer(questionIndex, val.value, changes: false);
      }
    });
  }

  Future<void> fetchQuestionEdit() async {
    if (isEdit) {
      findAnwerValue(0, emitenDetail.financialRecordingSystem);
      findAnwerValue(1, emitenDetail.bankLoanReputation);
      findAnwerValue(2, emitenDetail.marketPositionForTheProduct);
      findAnwerValue(3, emitenDetail.strategyEmiten);
      findAnwerValue(4, emitenDetail.officeStatus);
      findAnwerValue(5, emitenDetail.levelOfBusinessCompetition);
      findAnwerValue(6, emitenDetail.managerialAbility);
      findAnwerValue(7, emitenDetail.technicalAbility);
      _firstFetch = false;
    }
  }

  Future<void> nextProccess(BuildContext context, int type) async {
    // type = jenis posting
    // 0 = Update atau perbarui data
    // 1 = Registrasi
    try {
      var _isAnswerEmpty = _questionModel
          .firstWhere((val) => val.groupValue == -1, orElse: () => null);
      if (_isAnswerEmpty != null) {
        _isSubmitError = true;
        notifyListeners();
      } else {
        RegistrationHelper.steps.infoNonFinansial = true;
        if (type == 0) {
          try {
            PopupHelper.handleConfirmation(context, () async {
              Navigator.pop(context);
              PopupHelper.showLoading(context);
              var _response =
                  await listingApiService.updateInformasiNonFinansial(
                      RegistrationHelper.emiten, emitenDetail.uuid);
              if (_response.statusCode == 200) {
                _hasMakeChanges = false;
                notifyListeners();
                Navigator.pop(context);
                ToastHelper.showSuccessToast(
                    context, "Berhasil menyimpan perubahan!");
              } else {
                Navigator.pop(context);
                ToastHelper.showFailureToast(
                    context, _response.data["message"]);
              }
            });
          } catch (e) {
            Navigator.pop(context);
            ToastHelper.showFailureToast(
                context, "Terjadi kesalahan saat memperbarui data!");
          }
        } else if (type == 1) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      LampiranUI(isEdit: false, emitenDetail: null)));
        }
      }
    } catch (e) {
      //   print(e);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat melakukan validasi data!");
    }
  }
}
