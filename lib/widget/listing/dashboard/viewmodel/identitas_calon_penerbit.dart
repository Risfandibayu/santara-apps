import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/services/listing/category_service.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/listing/dashboard/InformasiFinansialUI.dart';

enum FetchCategoryState {
  loading,
  loaded
} // state ketika user request loading category

enum FetchSubcategoryState {
  loading,
  loaded
} // state ketika user request subcategory

enum FetchRegencyState { loading, loaded } // state ketika user request regency

enum DropDownState {
  loading,
  loaded
} // state ketika kondisi edit, set default ke category, subcategory, dan regency

class IdentitasCalonPenerbitProvider with ChangeNotifier {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  IdentitasCalonPenerbitProvider({this.isEdit, this.emitenDetail});
  // first fetch mirip seperti initstate, ketika user buka page identitaspenerbit pertama kali, maka aplikasi akan menjalankan beberapa method yang sudah ditentukan
  final GlobalKey<FormState> calonPenerbitFormKey = GlobalKey<FormState>();
  final identitasScaffold = GlobalKey<ScaffoldState>();
  bool autoValidate = false;
  bool _firstFetch = true;
  final ListingCategoryService categoryService = ListingCategoryService();
  final ListingApiService listingApiService = ListingApiService();

  String _namaPerusahaan;
  String _merekDagang;
  String _kotaLokasiUsaha;
  String _alamatLengkapUsaha;
  String _lamaUsaha;
  String _jumlahCabang;
  String _jumlahKaryawan;
  String _deskripsiUsaha;
  int _category = 0;
  int _subcategory = 0;
  Regency _regency;
  String _bentukBadanUsaha = "";
  DropDownState _dropDownState = DropDownState.loaded;

  // getter
  String get namaPerusahaan => _namaPerusahaan;
  String get merekDagang => _merekDagang;
  String get kotaLokasiUsaha => _kotaLokasiUsaha;
  String get alamatLengkapUsaha => _alamatLengkapUsaha;
  String get lamaUsaha => _lamaUsaha;
  String get jumlahCabang => _jumlahCabang;
  String get jumlahKaryawan => _jumlahKaryawan;
  String get deskripsiUsaha => _deskripsiUsaha;
  int get category => _category;
  int get subcategory => _subcategory;
  Regency get regency => _regency;
  String get bentukBadanUsaha => _bentukBadanUsaha;
  DropDownState get dropDownState => _dropDownState;

  set dropDownState(DropDownState state) {
    _dropDownState = state;
    notifyListeners();
  }

  // setter
  set namaPerusahaan(String value) {
    _namaPerusahaan = value;
    notifyListeners();
  }

  set merekDagang(String value) {
    _merekDagang = value;
    notifyListeners();
  }

  set kotaLokasiUsaha(String value) {
    _kotaLokasiUsaha = value;
    notifyListeners();
  }

  set alamatLengkapUsaha(String value) {
    _alamatLengkapUsaha = value;
    notifyListeners();
  }

  set lamaUsaha(String value) {
    _lamaUsaha = value;
    notifyListeners();
  }

  set jumlahCabang(String value) {
    _jumlahCabang = value;
    notifyListeners();
  }

  set jumlahKaryawan(String value) {
    _jumlahKaryawan = value;
    notifyListeners();
  }

  set deskripsiUsaha(String value) {
    _deskripsiUsaha = value;
    notifyListeners();
  }

  set bentukBadanUsaha(String value) {
    _bentukBadanUsaha = value;
    _isBentukBadanUsahaValid = true;
    _hasMakeChanges = true;
    notifyListeners();
  }

  // define variables
  // state
  FetchCategoryState _categoryState = FetchCategoryState
      .loading; // ketika page identitas calon penerbit dibuka, maka state defaultnya adalah loading ( request data )
  FetchSubcategoryState _subcategoryState = FetchSubcategoryState
      .loaded; // karena subcategory dipilih berdasarkan category state, maka ini defaultnya loaded dengan data default id 0 dan subcategory pilih sub sektor ekonomi
  FetchRegencyState _regencyState =
      FetchRegencyState.loading; // sama seperti category state

  bool _hasMakeChanges = false;
  List<CheckBoxModel> _docsPerizinan = List<CheckBoxModel>();
  List<String> _docPerizinans = [
    "SIUP",
    "TDP",
    "SKDP / SITU",
    "NIB",
    "NPWP Perusahaan",
    "Akta Perusahaan",
    "Belum Ada",
    "Yang Lain",
  ];
  List<Category> _jenisUsaha = [
    Category(
      id: 0,
      uuid: '',
      category: 'Pilih Jenis Usaha',
    )
  ];
  List<Subcategory> _subJenisUsaha = [
    Subcategory(
      id: 0,
      uuid: '',
      subCategory: 'Pilih Sub Sektor Ekonomi',
    )
  ];
  List<Regency> _regencies = [];
  bool _isBentukBadanUsahaValid = true;
  bool _isCategoryValid = true;
  bool _isSubCategoryValid = true;
  bool _isRegencyValid = true;

  // value getter
  List<Category> get jenisUsaha => _jenisUsaha;
  List<Subcategory> get subJenisUsaha => _subJenisUsaha;
  List<Regency> get regencies => _regencies;
  bool get hasMakeChanges => _hasMakeChanges;
  bool get isBentukBadanUsahaValid => _isBentukBadanUsahaValid;
  bool get isCategoryValid => _isCategoryValid;
  bool get isSubCategoryValid => _isSubCategoryValid;
  bool get isRegencyValid => _isRegencyValid;
  FetchCategoryState get categoryState => _categoryState;
  FetchSubcategoryState get subcategoryState => _subcategoryState;
  FetchRegencyState get regencyState => _regencyState;

  List<CheckBoxModel> get docsPerizinan => _docsPerizinan;
  bool get firstFetch => _firstFetch;

  // setter validitas jenis usaha & badan usaha
  set isBentukBadanUsahaValid(bool value) {
    _isBentukBadanUsahaValid = value;
    notifyListeners();
  }

  set firstFetch(bool value) {
    _firstFetch = value;
    notifyListeners();
  }

  // setter untuk state regency
  set regencyState(FetchRegencyState value) {
    _regencyState = value;
    notifyListeners();
  }

  // Setter untuk state kategori
  set categoryState(FetchCategoryState value) {
    _categoryState = value;
    notifyListeners();
  }

  // Setter untuk state subkategori
  set subcategoeryState(FetchSubcategoryState value) {
    _subcategoryState = value;
    notifyListeners();
  }

  set isCategoryValid(bool value) {
    _isCategoryValid = value;
    notifyListeners();
  }

  set isSubCategoryValid(bool value) {
    _isSubCategoryValid = value;
    notifyListeners();
  }

  set isRegencyValid(bool value) {
    _isRegencyValid = value;
    notifyListeners();
  }

  set category(int value) {
    _hasMakeChanges = true;
    _category = value;
    notifyListeners();
  }

  set regency(Regency value) {
    // print(">> val : ${value.name}");
    _hasMakeChanges = true;
    _regency = value;
    notifyListeners();
  }

  set subcategory(int value) {
    _hasMakeChanges = true;
    _subcategory = value;
    notifyListeners();
  }

  set hasMakeChanges(bool value) {
    _hasMakeChanges = value;
    // print(">> HENLO CHANGES : $_hasMakeChanges");
    notifyListeners();
  }

  set updateView(bool val) {
    notifyListeners();
  }

  // validator
  String handleNamaPerusahaan(String value) {
    if (value.length < 3) {
      return "Nama perusahaan minimal 3 karakter";
    } else if (value.length > 100) {
      return "Nama perusahaan maksimal 100 karakter";
    } else if (value.isEmpty) {
      return "Nama perusahaan tidak boleh kosong";
    } else if (value.contains(RegExp(r'[0-9]'), 1)) {
      return "Nama perusahaan tidak boleh menggunakan angka";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlemerekDagang(String value) {
    if (value.length < 3) {
      return "Merek dagang minimal 3 karakter";
    } else if (value.length > 100) {
      return "Merek dagang maksimal 100 karakter";
    } else if (value.isEmpty) {
      return "Merek dagang tidak boleh kosong";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlekotaLokasiUsaha(String value) {
    if (value.length < 3) {
      return "Lokasi usaha minimal 3 karakter";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlealamatLengkapUsaha(String value) {
    if (value.length < 3) {
      return "Alamat lengkap minimal 3 karakter";
    } else if (value.length > 500) {
      return "Alamat lengkap maksimal 500 karakter";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlelamaUsaha(String value) {
    if (value.length < 1) {
      return "Lama usaha tidak boleh kosong!";
    } else if (RegExp(r'^[0-9]+$').hasMatch(value)) {
      if (int.parse(value) < 1) {
        return "Lama usaha harus lebih dari 0 bulan";
      } else {
        _hasMakeChanges = true;
        notifyListeners();
        return null;
      }
    } else {
      return "Mohon masukan angka!";
    }
  }

  String handlejumlahCabang(String value) {
    if (value.length < 1) {
      return "Jumlah cabang minimal 1";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlejumlahKaryawan(String value) {
    if (value.length < 1) {
      return "Jumlah karyawan harus lebih dari 0";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handledeskripsiUsaha(String value) {
    if (value.length < 20) {
      return "Deskripsi usaha minimal 20 karakter";
    } else if (value.length > 499) {
      return "Maksimal jumlah karakter adalah 500 karakter";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  // method

  // method untuk merequest data kategori
  Future<void> fetchCategories(BuildContext context) async {
    // print(">> Fetching Category...");
    try {
      _categoryState = FetchCategoryState
          .loading; // jika melakukan request, state menjadi loading
      await categoryService.fetchCategories().then((value) {
        if (value != null) {
          _jenisUsaha.addAll(value);
        } else {
          ToastHelper.showSnackBar(identitasScaffold,
              "Sub sektor ekonomi tidak ditemukan!", Colors.redAccent);
        }
      });
      // jika request sudah selesai ( error atau tidak error ) maka state menjadi loaded atau selesai memuat
      _categoryState = FetchCategoryState.loaded;
      notifyListeners();
    } catch (e) {
      // jika error :
      // print(">> An error has occured, $e");
      ToastHelper.showSnackBar(identitasScaffold,
          "Terjadi kesalahan saat menerima data!", Colors.redAccent);
      _categoryState = FetchCategoryState.loaded;
      notifyListeners();
    }
  }

  // method untuk merequest data subkategori
  Future<void> fetchSubcategories(BuildContext context, int id) async {
    // print(">> Fetching Subcategory...");
    try {
      _subcategoryState =
          FetchSubcategoryState.loading; // set state menjadi loading
      notifyListeners();
      await categoryService.fetchSubCategories(id).then((value) {
        if (value != null) {
          // jika subkategori ditemukan
          _subJenisUsaha.addAll(value);
        } else {
          // jika subkategori tidak ditemukan
          ToastHelper.showSnackBar(identitasScaffold,
              "Sub Sektor Ekonomi tidak ditemukan!", Colors.redAccent);
        }
      });
      // jika request berakhir, maka set state menjadi loaded
      _subcategoryState = FetchSubcategoryState.loaded;
      notifyListeners();
    } catch (e) {
      // jika request error
      // print(">> Error : $e");
      ToastHelper.showSnackBar(identitasScaffold,
          "Terjadi kesalahan saat menerima data!", Colors.redAccent);
      _subcategoryState = FetchSubcategoryState.loaded;
      notifyListeners();
    }
  }

  Future<void> fetchRegency(BuildContext context) async {
    try {
      // print(">> Fereg");
      _regencyState = FetchRegencyState.loading; // set state ke loading
      await listingApiService.fetchRegencies().then((value) {
        if (value != null) {
          _regencies.addAll(value);
          if (emitenDetail != null) {
            regency = regencies
                .firstWhere((element) => element.id == emitenDetail.regencyId);
            _hasMakeChanges = false;
          }
        } else {
          ToastHelper.showSnackBar(identitasScaffold,
              "Daftar kota tidak ditemukan!", Colors.redAccent);
        }
      });
      _regencyState = FetchRegencyState.loaded;
      notifyListeners();
    } catch (e) {
      // print(">> Regency Error : $e");
      ToastHelper.showSnackBar(
          identitasScaffold, "Daftar kota tidak ditemukan!", Colors.redAccent);
      _regencyState = FetchRegencyState.loaded;
      notifyListeners();
    }
  }

  // reset subcategory
  Future<void> resetSubcategories() async {
    _subcategory = 0;
    _subJenisUsaha.clear();
    _subJenisUsaha.add(Subcategory(
      id: 0,
      uuid: '',
      subCategory: 'Pilih Sub Sektor Ekonomi',
    ));
    notifyListeners();
  }

  Future<void> simpanPerubahan() async {
    try {
      // print(">> OK");
    } catch (e) {
      // print(e);
    }
  }

  Future<void> initializePerizinan() async {
    var i = 0;
    _docPerizinans
        .map((e) => _docsPerizinan
            .add(CheckBoxModel(id: i++, title: e, isChecked: false)))
        .toList();
  }

  Future<void> validateInputs(BuildContext context, int type) async {
    try {
      // type = jenis posting
      // 0 = Update atau perbarui data
      // 1 = Registrasi
      var _errmsg = "Field tidak valid, mohon cek kembali";
      if (_category < 1) {
        ToastHelper.showFailureToast(context, _errmsg);
        _isCategoryValid = false;
        notifyListeners();
      } else if (_subcategory < 1) {
        ToastHelper.showFailureToast(context, _errmsg);
        _isSubCategoryValid = false;
        notifyListeners();
      } else if (_regency.id < 1) {
        ToastHelper.showFailureToast(context, _errmsg);
        _isRegencyValid = false;
        notifyListeners();
      } else if (_bentukBadanUsaha.length < 1) {
        ToastHelper.showFailureToast(context, _errmsg);
        _isBentukBadanUsahaValid = false;
        notifyListeners();
      } else {
        if (calonPenerbitFormKey.currentState.validate()) {
          calonPenerbitFormKey.currentState.save();
          RegistrationHelper.steps.identitasPenerbit = true;
          RegistrationHelper.emiten.companyName = _namaPerusahaan;
          RegistrationHelper.emiten.businessName = _merekDagang;
          RegistrationHelper.emiten.category = _category.toString();
          RegistrationHelper.emiten.subcategory = _subcategory;
          RegistrationHelper.emiten.city = _kotaLokasiUsaha;
          RegistrationHelper.emiten.regency = _regency.id;
          RegistrationHelper.emiten.companyAddress = _alamatLengkapUsaha;
          RegistrationHelper.emiten.businessEntity = _bentukBadanUsaha;
          RegistrationHelper.emiten.established = int.parse(_lamaUsaha);
          RegistrationHelper.emiten.branch = int.parse(_jumlahCabang);
          RegistrationHelper.emiten.employees = int.parse(_jumlahKaryawan);
          RegistrationHelper.emiten.description = _deskripsiUsaha;
          if (type == 1) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => InformasiFinansialUI(
                  isEdit: false,
                  emitenDetail: null,
                ),
              ),
            );
          } else if (type == 0) {
            try {
              // print(">> Updating identitas...");
              PopupHelper.handleConfirmation(context, () async {
                Navigator.pop(context);
                PopupHelper.showLoading(context);
                var _response = await listingApiService.updateIdentitasPenerbit(
                    RegistrationHelper.emiten, emitenDetail.uuid);
                if (_response.statusCode == 200) {
                  Navigator.pop(context);
                  ToastHelper.showSuccessToast(
                    context,
                    "Berhasil menyimpan perubahan!",
                  );
                } else {
                  Navigator.pop(context);
                  ToastHelper.showFailureToast(
                    context,
                    _response.data["message"],
                  );
                }
              });
            } catch (e) {
              Navigator.pop(context);
              ToastHelper.showFailureToast(
                context,
                "Terjadi kesalahan saat memperbarui data!",
              );
            }
          }
        } else {
          ToastHelper.showFailureToast(context, _errmsg);
          autoValidate = true;
          notifyListeners();
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }
}

class CheckBoxModel {
  int id;
  String title;
  bool isChecked;

  CheckBoxModel(
      {@required this.id, @required this.title, @required this.isChecked});
}
