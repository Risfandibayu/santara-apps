import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/models/listing/DiajukanListModel.dart';
import 'package:santaraapp/models/listing/ErrorHandler.dart';
import 'package:santaraapp/models/listing/PralistingListModel.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/services/listing/listing_service.dart';

enum TopPralistingState { loading, done, empty, error, unauthorized }
enum RandPralistingState { loading, done, empty, error, none, unauthorized }

class PralistingProvider with ChangeNotifier {
  // set value
  final _storage = new FlutterSecureStorage();
  ErrorHandler _scError = ErrorHandler(); // catch show case error
  bool _showcaseView = true;
  bool _showCaseLoaded = false;
  PralistingListModel _topPralisting = PralistingListModel();
  PralistingListModel _latestPralisting = PralistingListModel();
  TopPralistingState _pralistingState = TopPralistingState.loading;
  RandPralistingState _randPralistingState = RandPralistingState.none;
  final ListingApiService apiService = ListingApiService();
  bool _showDiajukanIcon = false;
  List<DataPralisting> _nonKycDatas = List<DataPralisting>();
  // value getter
  bool get showDiajukanIcon => _showDiajukanIcon;
  bool get showcaseView => _showcaseView;
  ErrorHandler get scError => _scError;
  bool get showCaseLoaded => _showCaseLoaded;
  PralistingListModel get topPralisting => _topPralisting;
  PralistingListModel get latestPralisting => _latestPralisting;
  TopPralistingState get pralistingState => _pralistingState;
  RandPralistingState get randPralistingState => _randPralistingState;
  List<DataPralisting> get nonKycDatas => _nonKycDatas;
  // setter
  set randPralistingState(RandPralistingState value) {
    _randPralistingState = value;
    notifyListeners();
  }

  set pralistingState(TopPralistingState state) {
    _pralistingState = state;
    notifyListeners();
  }

  set showDiajukanIcon(bool value) {
    _showDiajukanIcon = value;
    notifyListeners();
  }

  Future<void> checkHasMadePralisting() async {
    // var _res = false;
    try {
      await apiService.fetchBisnisDiajukan("").then((value) {
        if (value != null && value.statusCode == 200) {
          List<DiajukanListModel> datas = List<DiajukanListModel>();
          final res = json.decode(value.body);
          res.forEach((val) {
            var dummy = DiajukanListModel.fromJson(val);
            datas.add(dummy);
          });
          // _res = value.length > 0 ? true : false;
          _showDiajukanIcon = datas.length > 0 ? true : false;
        } else {
          // _res = false;
          _showDiajukanIcon = false;
        }
      });
      // return _res;
    } catch (e) {
      // print(e);
      _showDiajukanIcon = false;
      // return false;
    }
    notifyListeners();
  }

  // get top ten pralisting
  Future<void> getTopTenPralisting() async {
    // print(">>Get top 10");
    try {
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Success");
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        var _result = await apiService.fetchTopTen();
        if (_result != null && _result.statusCode == 200) {
          final res = json.decode(_result.body);
          var list = PralistingListModel.fromJson(res);
          if (list.data.length < 1) {
            _pralistingState = TopPralistingState.empty;
          } else {
            _pralistingState = TopPralistingState.done;
          }
          _topPralisting = list;
          _done = true;
        } else if (_result.statusCode == 401) {
          _pralistingState = TopPralistingState.unauthorized;
        } else {
          _pralistingState = TopPralistingState.error;
        }
        i++;
      }
    } catch (e) {
      _pralistingState = TopPralistingState.error;
      // print(e);
    }
    notifyListeners();
  }

  // get 10 pralisting terbaru
  Future<void> getLatestPralisting() async {
    try {
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Success");
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        // print(">> Fetching latest pralisting...");
        var _result = await apiService.fetchRandTen();
        final res = json.decode(_result.body);
        var list = PralistingListModel.fromJson(res);
        if (_result != null && _result.statusCode == 200) {
          _latestPralisting = list;
          _randPralistingState = RandPralistingState.done;
          _done = true;
        } else if (_result.statusCode == 401) {
          _randPralistingState = RandPralistingState.unauthorized;
          _done = true;
        } else if (_result != null && list.data.length < 1) {
          _randPralistingState = RandPralistingState.empty;
          _done = true;
        } else {
          _randPralistingState = RandPralistingState.error;
        }
        i++;
      }
    } catch (e) {
      _randPralistingState = RandPralistingState.error;
      // print(e);
    }
    // print(">> Done, state : ");
    // print(_randPralistingState);
    notifyListeners();
  }

  // get status tampilkan showcase
  Future<void> getShowCaseStatus() async {
    try {
      String value = await _storage.read(key: Constants.pralistingShowcase);
      if (value != null) {
        _showcaseView = false;
        _showCaseLoaded = true;
      } else {
        await _storage.write(key: Constants.pralistingShowcase, value: 'show');
        _showCaseLoaded = true;
      }
    } catch (e) {
      _showCaseLoaded = true;
      _showcaseView = true;
      _scError = ErrorHandler(
          status: 0,
          errorMessage: e.toString(),
          userError: 'Terjadi kesalahan saat menampilkan showcase!');
    }
    notifyListeners();
  }

  Future<void> fetchPrelistingNonKyc() async {
    try {
      // randPralistingState
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        await apiService.fetchPrelistingNonKyc().then((value) {
          if (value.statusCode == 200) {
            final res = json.decode(value.body);
            List<DataPralisting> datas = List<DataPralisting>();
            res.forEach((val) {
              var dummy = DataPralisting.fromJson(val);
              datas.add(dummy);
            });
            if (datas.length > 0) {
              _nonKycDatas.addAll(datas);
              randPralistingState = RandPralistingState.done;
              _done = true;
              // notifyListeners();
            } else {
              randPralistingState = RandPralistingState.empty;
              _done = true;
            }
          } else if (value.statusCode == 404) {
            randPralistingState = RandPralistingState.empty;
            _done = true;
          } else {
            randPralistingState = RandPralistingState.error;
          }
        });
        i++;
      }
    } catch (e) {
      randPralistingState = RandPralistingState.error;
    }
  }

  // debug sections
  // clear sstorage
  Future<void> deleteShowCase() async {
    await _storage.delete(key: Constants.pralistingShowcase);
  }
}
