import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/widget/listing/dashboard/NonFinansialUI.dart';

class InformasiFinansialProvider with ChangeNotifier {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  InformasiFinansialProvider({this.isEdit, this.emitenDetail});

  final GlobalKey<FormState> infoFinansialKey = GlobalKey<FormState>();
  bool autoValidate = false;
  final ListingApiService listingApiService = ListingApiService();

  // variable define
  int _besarKebutuhanDana;
  int _rataOmzetSekarang;
  int _rataLabaSekarang;
  int _rataOmzetSebelum;
  int _rataLabaSebelum;
  int _totalHutangBank;
  String _namaBank;
  int _totalModalDisetor;
  int _nilaiPerLembarSaham;
  bool _hasMakeChanges = false;
  bool _firstFetch = true;

  // getter
  int get besarKebutuhanDana => _besarKebutuhanDana;
  int get rataOmzetSekarang => _rataOmzetSekarang;
  int get rataLabaSekarang => _rataLabaSekarang;
  int get rataOmzetSebelum => _rataOmzetSebelum;
  int get rataLabaSebelum => _rataLabaSebelum;
  int get totalHutangBank => _totalHutangBank;
  String get namaBank => _namaBank;
  int get totalModalDisetor => _totalModalDisetor;
  int get nilaiPerLembarSaham => _nilaiPerLembarSaham;
  bool get hasMakeChanges => _hasMakeChanges;
  bool get firstFetch => _firstFetch;

  int digitParser(String value) {
    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    double _doubleValue = double.parse(_onlyDigits);
    return _doubleValue.toInt();
  }

  // setter
  set besarKebutuhanDana(int val) {
    _besarKebutuhanDana = val;
    notifyListeners();
  }

  set rataOmzetSekarang(int val) {
    _rataOmzetSekarang = val;
    notifyListeners();
  }

  set hasMakeChanges(bool value) {
    _hasMakeChanges = value;
    notifyListeners();
  }

  set rataLabaSekarang(int val) {
    _rataLabaSekarang = val;
    notifyListeners();
  }

  set rataOmzetSebelum(int val) {
    _rataOmzetSebelum = val;
    notifyListeners();
  }

  set rataLabaSebelum(int val) {
    _rataLabaSebelum = val;
    notifyListeners();
  }

  set totalHutangBank(int val) {
    _totalHutangBank = val;
    notifyListeners();
  }

  set namaBank(String val) {
    _namaBank = val;
    notifyListeners();
  }

  set totalModalDisetor(int val) {
    _totalModalDisetor = val;
    notifyListeners();
  }

  set nilaiPerLembarSaham(int val) {
    _nilaiPerLembarSaham = val;
    notifyListeners();
  }

  // handle input changes
  String handlebesarKebutuhanDana(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlerataOmzetSekarang(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlerataOmzetSebelum(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlerataLabaSekarang(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlerataLabaSebelum(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handletotalHutangBank(String value) {
    // int newvalue = int.parse(value);
    // if (newvalue < 1000) {
    //   return "Besar kebutuhan dana harus lebih dari 1000";
    // } else {
    //   _hasMakeChanges = true;
    //   notifyListeners();
    //   return null;
    // }
  }

  String handlenamaBank(String value) {
    if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(value)) {
      return null;
    } else {
      if (value.length < 1) {
        return null;
      } else {
        return "Mohon hilangkan simbol!";
      }
    }
    // int newvalue = int.parse(value);
    // if (newvalue < 1000) {
    //   return "Besar kebutuhan dana harus lebih dari 1000";
    // } else {
    //   _hasMakeChanges = true;
    //   notifyListeners();
    //   return null;
    // }
  }

  String handletotalModalDisetor(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1000) {
      return "Nilai minimal field ini adalah 1000";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String handlenilaiPerLembarSaham(String value) {
    if (value.length < 1) {
      return "Field ini harus diisi!";
    } else if (digitParser(value) < 1) {
      return "Nilai minimal field ini adalah 1";
    } else {
      _hasMakeChanges = true;
      notifyListeners();
      return null;
    }
  }

  String skipValidate(String value) {
    // print(">> Value");
    return null;
  }

  Future<void> nextProgress(BuildContext context, int type) async {
    // type = jenis posting
    // 0 = Update atau perbarui data
    // 1 = Registrasi
    try {
      if (infoFinansialKey.currentState.validate()) {
        infoFinansialKey.currentState.save();
        RegistrationHelper.steps.infoFinansial = true;
        RegistrationHelper.emiten.fundingReq = _besarKebutuhanDana;
        RegistrationHelper.emiten.turnoverAvg = _rataOmzetSekarang;
        RegistrationHelper.emiten.profitAvg = _rataLabaSekarang;
        RegistrationHelper.emiten.lastYearTurnoverAvg = _rataOmzetSebelum;
        RegistrationHelper.emiten.lastYearProfitAvg = _rataLabaSebelum;
        RegistrationHelper.emiten.debtTotal = _totalHutangBank;
        RegistrationHelper.emiten.bankName = _namaBank;
        RegistrationHelper.emiten.paidUpCapital = _totalModalDisetor;
        RegistrationHelper.emiten.valuePerShare = _nilaiPerLembarSaham;
        if (type == 1) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => InformasiNonFinansialUI(
                      isEdit: false, emitenDetail: null)));
        } else if (type == 0) {
          try {
            PopupHelper.handleConfirmation(context, () async {
              Navigator.pop(context);
              PopupHelper.showLoading(context);
              var _response = await listingApiService.updateInformasiFinansial(
                  RegistrationHelper.emiten, emitenDetail.uuid);
              if (_response.statusCode == 200) {
                hasMakeChanges = false;
                Navigator.pop(context);
                ToastHelper.showSuccessToast(
                    context, "Berhasil menyimpan perubahan!");
              } else {
                Navigator.pop(context);
                ToastHelper.showFailureToast(
                    context, _response.data["message"]);
              }
            });
          } catch (e) {
            // print(">> Error : $e");
            Navigator.pop(context);
            ToastHelper.showFailureToast(
                context, "Terjadi kesalahan saat memperbarui data!");
          }
        }
      } else {
        ToastHelper.showFailureToast(
            context, "Field tidak valid, mohon cek kembali");
        autoValidate = true;
        notifyListeners();
      }
    } catch (e) {
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat melakukan validasi!");
      // print(_besarKebutuhanDana);
      // print(">> $e");
    }
  }
}
