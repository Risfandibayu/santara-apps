import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/models/listing/UploadImageModel.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:path/path.dart' as path;
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/listing/registrasi/PernyataanInfoUI.dart';
import 'package:permission_handler/permission_handler.dart';

enum EmitenState { loading, done, error }

class MediaBisnisProvider with ChangeNotifier {
  final bool isEdit;
  PralistingEmiten emitenDetail;
  MediaBisnisProvider({this.isEdit, this.emitenDetail});
  final storage = new secureStorage.FlutterSecureStorage();
  // YOUTUBE
  bool _firstSet = true;
  bool _isYtError = false;
  TextEditingController ytVideo = TextEditingController();
  //
  bool _hasMakeChanges =
      false; // nandain user udah membuat perubahan di form atau belum
  //
  List<Asset> _images = List<Asset>();
  List<UploadDataImage> _imagelist = [];
  ListingApiService apiService = ListingApiService();
  double _width = 0;
  bool _expanded = true;
  bool _isInProgress = false;
  bool _isYtSetted = false;
  Dio dio = Dio();
  EmitenState _emitenState = EmitenState.loading;
  // get image uri
  List<Asset> get images => _images;
  double get width => _width;
  bool get expanded => _expanded;
  List<UploadDataImage> get imagelist => _imagelist;
  bool get isInProgress => _isInProgress;
  bool get isYtError => _isYtError;
  bool get isYtSetted => _isYtSetted;
  bool get hasMakeChanges => _hasMakeChanges;
  bool get firstSet => _firstSet;
  EmitenState get emitenState => _emitenState;

  set setYt(bool val) {
    _isYtSetted = val;
    notifyListeners();
  }

  set hasMakeChanges(bool val) {
    _hasMakeChanges = val;
    notifyListeners();
  }

  Future<void> _showFilesinDir({Directory dir}) async {
    final PermissionHandler _permissionHandler = PermissionHandler();
    var result =
        await _permissionHandler.requestPermissions([PermissionGroup.storage]);
    if (result[PermissionGroup.storage] == PermissionStatus.granted) {
      // permission was granted
      // print(">> OKE");
      // String imgPath =
      //     '/storage/emulated/0/Android/data/com.android.providers.media/albumthumbs/1570277797774';
      // return File(imgPath);
    }
  }

  Future<void> fetchEmitenDetail(BuildContext context, String uuid) async {
    // print(">> Fetch Emiten Detail..");
    try {
      // PopupHelper.showLoading(context);
      await apiService.fetchPralistingDetail(uuid).then((value) {
        if (value != null && value.statusCode == 200) {
          _imagelist.clear();
          final res = json.decode(value.body);
          var emiten = PralistingEmiten.fromJson(res);
          emitenDetail = emiten;
          getImages();
          _emitenState = EmitenState.done;
          // Navigator.pop(context);
        } else {
          _emitenState = EmitenState.error;
          // Navigator.pop(context);
        }
      });
    } catch (e) {
      _emitenState = EmitenState.error;
      Navigator.pop(context);
    }
    notifyListeners();
  }

  Future<void> setYoutubeEdit() async {
    _firstSet = false;
    ytVideo.text = emitenDetail.videoUrl ?? '';
    if (emitenDetail.videoUrl != null || emitenDetail.videoUrl.isNotEmpty) {
      _isYtSetted = true;
    }
    notifyListeners();
  }

  Future<void> getImages() async {
    var i = 0;
    // emitenDetail.pictures.split(",").forEach((val) {});
    emitenDetail.pictures.split(",").forEach((val) {
      UploadDataImage dummy = UploadDataImage(
          url: val,
          isUploading: false,
          isMain: i == 0 ? true : false,
          progress: 0.0);
      _imagelist.add(dummy);
      _imagelist
          .sort((a, b) => b.isMain.toString().compareTo(a.isMain.toString()));
      i++;
    });
    notifyListeners();
  }

  Future<bool> validateYtUrl(String url) async {
    RegExp regExp = new RegExp(
      // r"^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?\$",
      r"(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?",
      caseSensitive: false,
      multiLine: false,
    );
    if (url.length < 1 || url == null) {
      _isYtError = false;
      notifyListeners();
      return true;
    } else if (regExp.hasMatch(url)) {
      _isYtError = false;
      notifyListeners();
      return true;
    } else {
      _isYtError = true;
      notifyListeners();
      return false;
    }
  }

  Future<void> validateForm(BuildContext context) async {
    // validasi form field
    // validasi gambar emiten
    if (RegistrationHelper.emiten.assets.length > 0) {
      // jika gambar emiten tidak kosong lanjut cek video
      await validateYtUrl(ytVideo.text).then((value) {
        // jika video valid lanjut ke next step
        if (value) {
          RegistrationHelper.steps.media = true;
          RegistrationHelper.emiten.youtube = ytVideo.text;
          handleMediaAsset(context);
        } else {
          ToastHelper.showFailureToast(
              context, "Mohon masukan url video yang valid!");
        }
      });
    } else {
      ToastHelper.showFailureToast(
          context, "Mohon pilih gambar terlebih dahulu!");
    }
  }

  // : Ada perbedaan antara api & view,
  // sementara ini jangan dihapus ( untuk handle edit media )
  // start here!

  validateVideoUrl(String url) {
    RegExp regExp = new RegExp(
      // r"^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?\$",
      r"(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?",
      caseSensitive: false,
      multiLine: false,
    );
    if (regExp.hasMatch(url)) {
      return null;
    } else {
      return "Mohon masukan url video youtube yang valid!";
    }
  }

  Future<Response> uploadImage(
      UploadDataImage data, int type, String uuid) async {
    // type
    // 0 = upload gambar baru
    // 1 = update gambar
    var base = '$apiLocal/emitens/update-media/$uuid';
    _isInProgress = true;
    notifyListeners();
    var imgBytes = await data.asset.getByteData();
    List<int> imageData = imgBytes.buffer.asUint8List();
    try {
      final token = await storage.read(key: 'token');
      FormData formData = new FormData.fromMap({
        "pictures": MultipartFile.fromBytes(
          imageData,
          contentType: MediaType('image', 'jpeg'),
          filename: data.asset.name,
        ),
      });

      // Response response = await dio.post("/info", data: formData);
      Response response = await dio.post(
        "$base",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
        data: formData,
        onSendProgress: (int sent, int total) {
          // print("$sent $total");
          if (sent < total) {
            var _prog = ((sent / total) * 250);
            updateUploadingProgress(data, _prog);
          }
        },
      );
      // print(">> Response from $base : ");
      // print(response.data.toString());
      _isInProgress = false;
      notifyListeners();
      return response;
    } catch (e) {
      _isInProgress = false;
      notifyListeners();
      // print(e);
      return null;
    }
  }

  setAsMainImage(EmitenAssets image) {
    RegistrationHelper.emiten.assets.forEach((val) {
      val.isMain = false;
    });
    image.isMain = true;
    notifyListeners();
  }

  // EDIT MEDIA
  editSetGambarUtama(BuildContext context, UploadDataImage image) async {
    _imagelist.forEach((val) {
      val.isMain = false;
    });
    image.isMain = true;
    // print(">> Set as gambar utama...");
    try {
      await apiService
          .setGambarUtama(emitenDetail.uuid, image.url)
          .then((value) {
        if (value != null) {
          if (value.statusCode == 200) {
          } else {
            ToastHelper.showFailureToast(context, value.data["message"]);
          }
        } else {
          ToastHelper.showFailureToast(context,
              "Terjadi kesalahan saat menjadikan sebagai gambar utama!");
        }
      });
    } catch (e) {
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat menjadikan sebagai gambar utama!");
    }
    notifyListeners();
  }

  Future<void> updateUploadingProgress(
      UploadDataImage image, double val) async {
    if (image.progress < 250) {
      image.progress += val;
      notifyListeners();
    }
  }

  // untuk registrasi
  Future<void> deleteImage(UploadDataImage image) async {
    _imagelist.remove(image);
    notifyListeners();
  }

  // untuk edit media
  Future<void> deleteImageEdit(UploadDataImage image) async {
    _imagelist.remove(image);
    try {
      var response = await apiService.hapusGambar(emitenDetail.uuid, image.url);
      // print(">> Data : ${emitenDetail.uuid} : ${image.url}");
      // print(response.data);
    } catch (e, stack) {
      santaraLog(e, stack);
    }
    notifyListeners();
  }

  Future<void> pickImages(BuildContext context) async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10 - _imagelist.length,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (resultList.length > 0) {
      resultList.forEach((value) {
        var dummy = UploadDataImage(
            url: "",
            asset: value,
            isUploading: true,
            isMain: false,
            progress: 0.0);
        final String fileName = path.basename(value.name);
        final delim = fileName.lastIndexOf('.');
        String ext = fileName.substring(delim >= 0 ? delim : 0);
        _imagelist.add(dummy);
      });
      // print(">> Images length : ${_imagelist.length}");
      _imagelist.forEach((val) async {
        if (val.isUploading) {
          await uploadImage(val, 0, '0df54d54-e38a-4c6e-869a-46ed6dbebcde')
              .then((value) {
            if (value != null) {
              if (value.statusCode == 200) {
                val.isUploading = false;
                val.url = value.data["url"];
                notifyListeners();
              }
            }
          });
        }
      });
      resultList.clear();
      _hasMakeChanges = true;
      notifyListeners();
    }
  }

  Future<void> pickImageV2(BuildContext context) async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10 - _imagelist.length,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (resultList.length > 0) {
      PopupHelper.showLoading(context);
      for (var value in resultList) {
        // Ngecek ukuran gambar
        File _fileMeta = File(await value.filePath);
        // jika gambar lebih dari 20 Mb
        if (_fileMeta.lengthSync() > 20000000) {
          // Navigator.pop(context);
          ToastHelper.showFailureToast(context, "Ukuran gambar maksimal 20 MB");
          break;
        }
        var asset = UploadDataImage(
            // id: 0,
            filepath: await value.filePath,
            // type: 'image',
            uploadToServer: true,
            url: '',
            asset: value,
            isUploading: true,
            isMain: false); // yang diupload
        _imagelist.add(asset);
      }
      _hasMakeChanges = true;
      Navigator.pop(context);
      notifyListeners();
    }
  }

  Future<void> pickSingleImage(
      BuildContext context, UploadDataImage img) async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (resultList.length > 0) {
      img.asset = resultList[0];
      img.isUploading = true;
      img.isMain = img.isMain;
      notifyListeners();
      var dummy = UploadDataImage(
          url: "",
          asset: resultList[0],
          isUploading: true,
          isMain: img.isMain,
          progress: 0.0);
      img = dummy;
      _hasMakeChanges = true;
      resultList.clear();
      notifyListeners();
    }
  }

  // END

  // : Baru
  // : Ini untuk handle registrasi baru
  // : Penyesuaian dengan api yg dibuat mas irfan

  Future<void> pickImagesReg(BuildContext context) async {
    // _showFilesinDir();
    // print(">> Picking images..");
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    // count images selected
    var _selectedTotal = RegistrationHelper.emiten.assets != null
        ? RegistrationHelper.emiten.assets.length
        : 0;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10 - _selectedTotal,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
          // 2808983
          // 13712705
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    List<EmitenAssets> _emitenAssets = [];
    if (RegistrationHelper.emiten.assets != null) {
      _emitenAssets = RegistrationHelper.emiten.assets;
    }
    if (resultList.length > 0) {
      PopupHelper.showLoading(context);
      for (var value in resultList) {
        // Ngecek ukuran gambar
        File _fileMeta = File(await value.filePath);
        // jika gambar lebih dari 20 Mb
        if (_fileMeta.lengthSync() > 20000000) {
          // Navigator.pop(context);
          ToastHelper.showFailureToast(context, "Ukuran gambar maksimal 20 MB");
          break;
        }
        // print(">> FILE PATH : " + await value.filePath);
        var asset = EmitenAssets(
            id: 0,
            filepath: await value.filePath,
            type: 'image',
            url: '',
            asset: value,
            isMain: false); // yang diupload
        _emitenAssets.add(asset);
      }
      // resultList.forEach((value) async {
      // });
      RegistrationHelper.emiten.assets = _emitenAssets;
      _hasMakeChanges = true;
      Navigator.pop(context);
      notifyListeners();
    }
  }

// ketika user registrasi
  Future<void> regPickSingleImageV2(
      BuildContext context, EmitenAssets img) async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
        ),
      );
    } on Exception catch (e) {
      // error = e.toString();
      // print(e);
    }

    if (resultList.length > 0) {
      img.asset = resultList[0];
      img.isMain = img.isMain;
      _hasMakeChanges = true;
      resultList.clear();
      notifyListeners();
    }
    // end
  }

  Future<void> regPickSingleImage(
      BuildContext context, UploadDataImage img) async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    UploadDataImage oldEmit = img;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Pilih Gambar Perusahaan",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
          // autoCloseOnSelectionLimit: true,
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (resultList.length > 0) {
      img.asset = resultList[0];
      img.isUploading = true;
      img.isMain = img.isMain;
      img.progress = 666.0;
      notifyListeners();
      var dummy = UploadDataImage(
          url: "",
          asset: resultList[0],
          isUploading: true,
          isMain: img.isMain,
          progress:
              666.0); // jika progress = 666 gak bisa update gambar krn sebelumnya user udah update gambar ( belum sempat bikin parameter baru krn kekejar deadline euy )
      img = dummy;
      _hasMakeChanges = true;
      notifyListeners();
      // begin update photo
      // print(">> Updating gambar...");
      var emit = EmitenAssets(
          id: 0,
          filepath: await resultList[0].filePath,
          type: 'wow',
          url: '',
          asset: resultList[0],
          isMain: false);
      var _resp =
          await apiService.updateGambar(emitenDetail.uuid, oldEmit.url, emit);
      if (_resp.statusCode == 200) {
        // print(">> Taps");
        resultList.clear();
      } else {
        ToastHelper.showFailureToast(
            context, "Terjadi kesalahan saat mengupload gambar!");
        img = oldEmit;
        resultList.clear();
        notifyListeners();
      }
      // end

    }
  }

  Future<void> regDeleteImage(EmitenAssets image) async {
    RegistrationHelper.emiten.assets.remove(image);
    notifyListeners();
  }

  Future<void> simpanPerubahan(BuildContext context) async {
    if (_imagelist.length < 1) {
      ToastHelper.showFailureToast(
          context, "Mohon setidaknya pilih 1 gambar mengenai usaha anda!");
    } else {
      // define variable untuk nampung gambar yang mau diupload
      List<UploadDataImage> _imgUp = List<UploadDataImage>();
      // looping gambar yang mau diupload
      _imagelist.forEach((img) {
        if (img.uploadToServer) {
          // nandain gambar yg akan diupload ( yg ada di dalem _imagelist )
          _imgUp.add(
              img); // jika key uploadToServer gambar bernilai true, maka tampung ke _imgUp
        }
      });
      // print(">> Saving perubahan...");
      try {
        PopupHelper.showLoading(context);
        var _resp = await apiService.saveMediaChanges(
            emitenDetail.uuid, ytVideo.text, _imgUp);
        Navigator.pop(context);
        if (_resp.statusCode == 200) {
          fetchEmitenDetail(context, emitenDetail.uuid);
          ToastHelper.showSuccessToast(context, "Berhasil menyimpan perubahan");
          _hasMakeChanges = false;
          notifyListeners();
        } else {
          final res = json.decode(_resp.body);
          ToastHelper.showFailureToast(
              context,
              res["message"] != null
                  ? res["message"]
                  : "Gagal menyimpan data!");
        }
      } catch (e) {
        // print(e);
        ToastHelper.showFailureToast(
            context, "Terjadi kesalahan, tidak dapat menyimpan!");
      }
    }
  }

  Future<void> handleMediaAsset(BuildContext context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => PernyataanInfoUI()));
  }

  // END

  set setYtError(bool val) {
    _isYtError = val;
    notifyListeners();
  }
}
