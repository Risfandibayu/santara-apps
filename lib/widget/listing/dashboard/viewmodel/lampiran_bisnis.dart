import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/services/listing/listing_service.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/listing/dashboard/MediaBisnisUI.dart';

enum ProspektusState {
  updating,
  none,
  done,
  error
} // state saat upload prospektus baru

class LampiranBisnisProvider with ChangeNotifier {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  LampiranBisnisProvider({this.isEdit, this.emitenDetail});

  // service
  ListingApiService apiService = ListingApiService();
  ProspektusState _prospektusState = ProspektusState.none;

  // setting value
  String _prospektusFileName = "";
  String _prospektusError = "";
  FilePicker filePicker;
  File _prospektus;
  bool _hasMakeChanges = false;
  bool _firstFetch =
      true; // buka page lampiran ui pertama kali? jika iya maka get prospektus file

  // value getter
  String get prospektusFileName => _prospektusFileName;
  String get prospektusError => _prospektusError;

  File get prospektus => _prospektus;
  bool get hasMakeChanges => _hasMakeChanges;
  bool get firstFetch => _firstFetch;
  ProspektusState get prospektusState => _prospektusState;

  // state setter
  set prospektusState(ProspektusState value) {
    _prospektusState = value;
    notifyListeners();
  }

  // value setter
  set prospektusFileName(String value) {
    _prospektusFileName = value;
    notifyListeners();
  }

  set hasMakeChanges(bool value) {
    _hasMakeChanges = value;
    notifyListeners();
  }

  Future<void> getProspektusFile() async {
    if (isEdit) {
      _prospektusFileName = fileNameFromPath(emitenDetail.prospektus);
    } else {
      _prospektusFileName = "Pilih Dokumen";
    }
    _firstFetch = false;
    notifyListeners();
  }

  // cek ekstensi file
  Future<bool> checkFileExtension(File file, String extension) async {
    if (await file.exists()) {
      final String fileName = path.basename(file.path);
      final delim = fileName.lastIndexOf('.');
      String ext = fileName.substring(delim >= 0 ? delim : 0);
      return ext == extension ? true : false;
    } else {
      return false;
    }
  }

  fileNameFromPath(String path) {
    return path.split("/").last;
  }

  Future<bool> validatingLampiran() async {
    if (_prospektus == null) {
      _prospektusError = "Mohon pilih dokumen prospektus terlebih dahulu!";
      _prospektusState = ProspektusState.error;
      notifyListeners();
      return false;
    } else {
      return true;
    }
  }

  Future<void> simpanPerubahan(BuildContext context) async {
    PopupHelper.showLoading(context);
    _prospektusError = "";
    try {
      // print(">> Updating Lampiran...");
      await apiService
          .updateLampiran(_prospektus.path, emitenDetail.uuid)
          .then((value) {
        if (value.statusCode == 200) {
          Navigator.pop(context); // pop loading dialog
          ToastHelper.showSuccessToast(context, value.data["message"]);
          _prospektusState = ProspektusState.done;
          _hasMakeChanges = false;
          notifyListeners();
        } else {
          Navigator.pop(context); // pop loading dialog
          ToastHelper.showFailureToast(context, value.data['message']);
          _prospektusState = ProspektusState.error;
          _hasMakeChanges = false;
          notifyListeners();
        }
      });
    } catch (e) {
      Navigator.pop(context); // pop loading dialog
      _prospektusState = ProspektusState.error;
      notifyListeners();
    }
  }

  Future<void> pickProspektusFile() async {
    try {
      await FilePicker.platform
          .pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
        allowMultiple: false,
      )
          .then((res) async {
        var file = File(res.files[0].path);
        if (file != null) {
          // jika file berukuran lebih dari 5 MB
          if (file.lengthSync() > 5000000 * 5) {
            _prospektusError = "Ukuran file pdf maksimal 25 MB";
          } else {
            // jika tidak
            await checkFileExtension(file, '.pdf').then((value) {
              if (value) {
                _hasMakeChanges = true;
                _prospektus = file;
                _prospektusFileName = fileNameFromPath(file.path);
                _prospektusError = "";
              } else {
                _prospektusError = "File harus berformat pdf!";
              }
            });
          }
        }
      });
      notifyListeners();
    } catch (e, stack) {
      santaraLog(e, stack);
      _prospektusError = e;
      // print(">> Something error has occured $e");
    }
  }

  Future<void> nextProccess(BuildContext context) async {
    if (_prospektus == null) {
      _prospektusError = "Mohon pilih dokumen prospektus terlebih dahulu!";
    } else {
      _prospektusError = "";
      // emitenDetail.lampiran.fileName = fileNameFromPath(_prospektus.path);
      var attachment = EmitenAttachment(
          id: 1,
          fileName: fileNameFromPath(_prospektus.path),
          filePath: _prospektus.path,
          type: 'document');
      RegistrationHelper.steps.lampiran = true;
      RegistrationHelper.emiten.lampiran = attachment;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  MediaBisnisUI(isEdit: false, emitenDetail: null)));
    }
    notifyListeners();
  }
}
