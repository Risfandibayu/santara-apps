import 'package:flutter/material.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/utils/sizes.dart';

class SearchRegency extends StatefulWidget {
  final List<Regency> regencies;
  SearchRegency({@required this.regencies});

  @override
  _SearchRegencyState createState() => _SearchRegencyState();
}

class _SearchRegencyState extends State<SearchRegency> {
  List<Regency> items = List<Regency>();

  @override
  void initState() {
    items.addAll(widget.regencies);
    super.initState();
  }

  void filterSearchResults(String query) {
    List<Regency> dummySearchList = List<Regency>();
    dummySearchList.addAll(widget.regencies);
    if (query.isNotEmpty) {
      List<Regency> dummyListData = List<Regency>();
      dummySearchList.forEach((item) {
        if (item.name.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(widget.regencies);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Kota Lokasi Usaha",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: Sizes.s50,
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  hintText: "Masukan kata kunci",
                ),
                onChanged: (String query) {
                  filterSearchResults(query);
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  Regency regency = items[index];
                  return ListTile(
                    title: Text("${regency.name}"),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.pop(context, regency);
                    },
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
