import 'dart:io';
import 'dart:math';
import 'package:android_intent/android_intent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/BisnisItemHelper.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/bisnis_diajukan.dart';
import 'package:santaraapp/widget/listing/detail_emiten/DetailEmitenUI.dart';
import 'package:santaraapp/widget/listing/registrasi/PreRegistrasiUI.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/listing/BisnisItem.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAlertDialog.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraDropDown.dart';
import 'package:url_launcher/url_launcher.dart';

enum BisnisPopAction { delete }

class BisnisDiajukanUI extends StatefulWidget {
  @override
  _BisnisDiajukanUIState createState() => _BisnisDiajukanUIState();
}

class _BisnisDiajukanUIState extends State<BisnisDiajukanUI> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => BisnisDiajukanProvider()),
      ],
      child: BisnisDiajukanBody(),
    );
  }
}

class BisnisDiajukanBody extends StatefulWidget {
  @override
  _BisnisDiajukanBodyState createState() => _BisnisDiajukanBodyState();
}

class _BisnisDiajukanBodyState extends State<BisnisDiajukanBody> {
  // Untuk popup sudah semua, tinggal benerin aksi tiap user tap bisnis
  // Aksi akan berbeda-beda tiap status pengajuan
  // Selengkapnya bisa lihat di FIGMA bagian Bisnis Diajukan
  Random random = Random();
  final storage = FlutterSecureStorage();
  final DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");

  Future getLocalStorage() async {
    // print(">> Getting values...");
    await storage.read(key: 'token').then((value) {
      // print("token : $value");
    });
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    _fetchBisnis();
  }

  _fetchBisnis() async {
    await Future.delayed(Duration(milliseconds: 100), () async {
      final provider =
          Provider.of<BisnisDiajukanProvider>(context, listen: false);
      if (provider.state == BisnisDiajukanState.loading) {
        await provider.fetchBisnis(status: "");
      }
    });
  }

  // jika data kosong
  Widget _submissionNotAvailable(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(30),
      height: SizeConfig.screenHeight - 150,
      width: SizeConfig.screenWidth,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/icon/no-result.png",
              height: SizeConfig.screenHeight / 4,
              width: SizeConfig.screenHeight / 4,
            ),
            Container(height: 10),
            Text(
              "Belum Ada Riwayat Pengajuan",
              style: TextStyle(
                  fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Container(height: 10),
            Text(
              "Anda punya bisnis ? Sedang cari pemodal ?\nDaftar disini",
              style: TextStyle(
                color: Color(0xff676767),
                fontSize: SizeConfig.safeBlockHorizontal * 3.5,
              ),
              textAlign: TextAlign.center,
            ),
            Container(height: 15),
            SantaraMainButton(
              title: "Daftarkan Bisnis",
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PreRegistrasiUI()));
              },
            )
          ],
        ),
      ),
    );
  }

  // jika user klik hapus
  void _deleteConfirmation(BuildContext context, int index) {
    final provider =
        Provider.of<BisnisDiajukanProvider>(context, listen: false);
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        title: "Hapus Pengajuan Bisnis ?",
        description: "Apakah Anda yakin ingin menghapus pengajuan bisnis ini ?",
        onYakin: () {
          Navigator.pop(context);
          provider.hapusPengajuan(context, index);
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // jika user klik ajukan kembali
  void _repostConfirmation(BuildContext context, String uuid) {
    final provider =
        Provider.of<BisnisDiajukanProvider>(context, listen: false);
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        title: "Ajukan Kembali Bisnis ?",
        description: "Apakah Anda yakin ingin mengajukan kembali bisnis ini ?",
        onYakin: () async {
          Navigator.pop(context);
          await provider.repostPralisting(context, uuid);
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // widget untuk filter
  Widget _filterBuilder(BuildContext context) {
    final provider = Provider.of<BisnisDiajukanProvider>(context, listen: true);
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: SizeConfig.safeBlockHorizontal * 5),
      child: SantaraDropDown(
        icon: Image.asset(
          "assets/icon/category_filter.png",
        ),
        isExpanded: true,
        hint: Text("Semua"),
        items: provider.status.map((StatusParse e) {
          return DropdownMenuItem(value: e, child: Text(e.text));
        }).toList(),
        value:
            provider.selected == null ? provider.status[0] : provider.selected,
        onChanged: (selected) {
          provider.filter = selected;
          provider.state = BisnisDiajukanState.loading;
          provider.fetchBisnis(status: selected.value);
        },
      ),
    );
  }

  // ditect open edit page
  void _openEditEmiten(String uuid) async {
    final provider =
        Provider.of<BisnisDiajukanProvider>(context, listen: false);
    await provider.editPengajuan(context, uuid);
  }

  // open detail page
  void openDetailPage(int status, String uuid) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PralistingDetailPage(
          uuid: uuid,
          status: status,
          position: null,
        ),
      ),
    );
  }

  // open email
  void openEmailApp() {
    if (Platform.isAndroid) {
      AndroidIntent intent = AndroidIntent(
        action: 'android.intent.action.MAIN',
        category: 'android.intent.category.APP_EMAIL',
      );
      intent.launch().catchError((e) {
        // print(e);
      });
    } else if (Platform.isIOS) {
      launch("message://").catchError((e) {
        // print(e);
      });
    }
  }

  // jika user tap card
  void _handleCardTap(String statusString, String uuid) {
    switch (statusString) {
      case "waiting for verification":
        openDetailPage(3, uuid);
        break;
      case "rejected":
        openDetailPage(3, uuid);
        break;
      // case "rejected airing":
      //   openDetailPage(3, uuid);
      //   break;
      // case "rejected screeening":
      //   openDetailPage(3, uuid);
      //   break;
      case "verified":
        openDetailPage(2, uuid);
        break;
      default:
        openDetailPage(2, uuid);
    }
  }

  Widget _contentBuilder(BuildContext context) {
    final provider =
        Provider.of<BisnisDiajukanProvider>(context, listen: false);
    return provider.state == BisnisDiajukanState.loading
        ? ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 5,
            itemBuilder: (context, index) {
              return BisnisItemLoader();
            })
        : provider.state == BisnisDiajukanState.empty
            ? _submissionNotAvailable(context)
            : provider.state == BisnisDiajukanState.error
                ? Container()
                : provider.state == BisnisDiajukanState.loaded
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: provider.data.length,
                        itemBuilder: (context, index) {
                          var _inside = provider.data[index];
                          return BisnisItem(
                              tanggal: dateFormat.format(_inside.createdAt),
                              onRepost: () => BisnisItemHelper.showRepost(
                                      _inside.status)
                                  ? _repostConfirmation(context, _inside.uuid)
                                  : null,
                              onCheckMail: () =>
                                  BisnisItemHelper.checkMail(_inside.status)
                                      ? openEmailApp()
                                      : null,
                              onEdit: () =>
                                  BisnisItemHelper.showEdit(_inside.status)
                                      ? _openEditEmiten(_inside.uuid)
                                      : null,
                              onDelete: () =>
                                  BisnisItemHelper.showDelete(_inside.status)
                                      ? _deleteConfirmation(context, index)
                                      : null,
                              onTap: () =>
                                  _handleCardTap(_inside.status, _inside.uuid),
                              data: provider.data[index],
                              statusString: _inside.status);
                        })
                    : Container();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Scaffold(
      appBar: AppBar(
        title: Text("Bisnis Diajukan",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(_size * 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SantaraAppBetaTesting(),
                _filterBuilder(context),
                _contentBuilder(context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
