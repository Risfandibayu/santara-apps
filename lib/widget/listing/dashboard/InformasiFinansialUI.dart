import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/informasi_finansial.dart';
import 'package:santaraapp/widget/listing/registrasi/RegistrasiStepsUI.dart';
import 'package:santaraapp/widget/widget/components/listing/ListingDialog.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';

class InformasiFinansialUI extends StatefulWidget {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  InformasiFinansialUI({@required this.isEdit, @required this.emitenDetail});
  @override
  _InformasiFinansialUIState createState() =>
      _InformasiFinansialUIState(isEdit, emitenDetail);
}

class _InformasiFinansialUIState extends State<InformasiFinansialUI> {
  bool isEdit;
  PralistingEmiten emitenDetail;
  _InformasiFinansialUIState(this.isEdit, this.emitenDetail);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
          create: (_) => InformasiFinansialProvider(
              isEdit: isEdit, emitenDetail: emitenDetail)),
    ], child: InformasiFinansialBody());
  }
}

class InformasiFinansialBody extends StatefulWidget {
  @override
  _InformasiFinansialBodyState createState() => _InformasiFinansialBodyState();
}

class _InformasiFinansialBodyState extends State<InformasiFinansialBody> {
  int digitParser(String value) {
    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    double _doubleValue = double.parse(_onlyDigits);
    return _doubleValue.toInt();
  }

  // ketika user click (i) modal disetor
  void _handleModalDisetorKnowledge() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return ListingAlertDialog(
              title: "",
              subtitle:
                  "Modal yang sudah dimasukkan pemegang saham sebagai pelunasan pembayaran saham yang diambilnya sebagai modal yang ditempatkan dari modal dasar perseroan",
              buttonTitle: "Mengerti",
              showCloseButton: false,
              height: SizeConfig.screenHeight / 4,
              onClose: () => Navigator.pop(context),
              onTap: () => Navigator.pop(context));
        });
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final provider =
        Provider.of<InformasiFinansialProvider>(context, listen: true);
    var _size = SizeConfig.safeBlockHorizontal;
    if (provider.firstFetch) {
      // provider.fetchEditData();
      // print(">> Here i am");
    }
    return WillPopScope(
      onWillPop: () async {
        if (provider.hasMakeChanges) {
          PopupHelper.handleClosePage(context);
          RegistrationHelper.steps.infoFinansial = false;
        } else {
          Navigator.pop(context, 'refresh');
        }
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
            elevation: 2,
            title: Text("Informasi Finansial",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              provider.isEdit
                  ? Container()
                  : IconButton(
                      icon: Icon(Icons.more_horiz),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegistrasiStepsUI()));
                      })
            ],
          ),
          body: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(_size * 5),
                color: Colors.white,
                child: Form(
                  key: provider.infoFinansialKey,
                  autovalidate: provider.autoValidate,
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SantaraCashInput(
                          key: Key('txtKebutuhanDana'),
                          // enabled: !provider.isEdit,
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.capitalNeeds == null
                                      ? "0"
                                      : provider.emitenDetail.capitalNeeds
                                          .toString()),
                          maxDigits: 14,
                          hintText: "Rp500.000.000,00",
                          labelText: "Besar Kebutuhan Dana",
                          validator: provider.handlebesarKebutuhanDana,
                          onSaved: (val) {
                            provider.besarKebutuhanDana =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtRataOmzetSaatIni'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.monthlyTurnover == null
                                      ? "0"
                                      : provider.emitenDetail.monthlyTurnover
                                          .toString()),
                          maxDigits: 14,
                          hintText: "Rp50.000.000,00",
                          labelText: "Rata-rata omzet per-bulan saat ini",
                          validator: provider.handlerataOmzetSekarang,
                          onSaved: (val) {
                            provider.rataOmzetSekarang =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtRataLabaSaatIni'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.monthlyProfit == null
                                      ? "0"
                                      : provider.emitenDetail.monthlyProfit
                                          .toString()),
                          maxDigits: 14,
                          hintText: "Rp30.000.000,00",
                          labelText: "Rata-rata laba per-bulan saat ini",
                          validator: provider.handlerataLabaSekarang,
                          onSaved: (val) {
                            provider.rataLabaSekarang =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtRataOmzetTahunSebelumnya'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(provider
                                          .emitenDetail
                                          .monthlyTurnoverPreviousYear ==
                                      null
                                  ? "0"
                                  : provider
                                      .emitenDetail.monthlyTurnoverPreviousYear
                                      .toString()),
                          maxDigits: 14,
                          hintText: "Rp25.000.000,00",
                          labelText:
                              "Rata-rata omzet per bulan tahun sebelumnya",
                          validator: provider.handlerataOmzetSebelum,
                          onSaved: (val) {
                            provider.rataOmzetSebelum =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtRataLabaTahunSebelumnya'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(provider
                                          .emitenDetail
                                          .monthlyProfitPreviousYear ==
                                      null
                                  ? "0"
                                  : provider
                                      .emitenDetail.monthlyProfitPreviousYear
                                      .toString()),
                          maxDigits: 14,
                          hintText: "Rp20.000.000,00",
                          labelText:
                              "Rata-rata laba per bulan tahun sebelumnya",
                          validator: provider.handlerataLabaSebelum,
                          onSaved: (val) {
                            provider.rataLabaSebelum =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtTotalHutangBank'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.totalBankDebt == null
                                      ? "0"
                                      : provider.emitenDetail.totalBankDebt
                                          .toString()),
                          maxDigits: 14,
                          hintText: "Rp0",
                          labelText: "Total hutang bank/lembaga pembiayaan",
                          onSaved: (val) {
                            if (val == null || val.isEmpty) {
                              val = "0";
                              provider.totalHutangBank = 0;
                            } else {
                              provider.totalHutangBank =
                                  provider.digitParser(val);
                            }
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraTextField(
                          key: Key('txtNamaBank'),
                          initialValue: !provider.isEdit
                              ? ""
                              : provider.emitenDetail.bankNameFinancing,
                          hintText: "*Kosongkan jika tidak ada",
                          labelText: "Nama bank/lembaga pembiayaan",
                          inputType: TextInputType.text,
                          validator: provider.handlenamaBank,
                          onSaved: (val) {
                            if (val == null || val.isEmpty) {
                              provider.namaBank = "";
                            } else {
                              provider.namaBank = val;
                            }
                          },
                        ),
                        SizedBox(
                          height: _size * 7,
                        ),
                        SantaraCashInput(
                          key: Key('txtModalDisetor'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.totalPaidCapital == null
                                      ? "0"
                                      : provider.emitenDetail.totalPaidCapital
                                          .toString()),
                          maxDigits: 14,
                          hintText: "Rp100.000.000,00",
                          labelText: "Total modal disetor",
                          suffixIcon: IconButton(
                              icon: Icon(Icons.info),
                              onPressed: _handleModalDisetorKnowledge),
                          validator:
                              RegistrationHelper.emiten.businessEntity == "PT"
                                  ? provider.handletotalModalDisetor
                                  : null,
                          onSaved: (val) {
                            if (val == null || val.isEmpty) {
                              val = "0";
                            }
                            provider.totalModalDisetor =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        SantaraCashInput(
                          key: Key('txtNilaiPerLembarSaham'),
                          initialValue: !provider.isEdit
                              ? ""
                              : RupiahFormatter.initialValueFormat(
                                  provider.emitenDetail.price == null
                                      ? "0"
                                      : provider.emitenDetail.price.toString()),
                          maxDigits: 14,
                          hintText: "Rp200.000,00",
                          labelText: "Nilai per -lembar saham",
                          validator:
                              RegistrationHelper.emiten.businessEntity == "PT"
                                  ? provider.handlenilaiPerLembarSaham
                                  : null,
                          onSaved: (val) {
                            if (val == null || val.isEmpty) {
                              val = "0";
                            }
                            provider.nilaiPerLembarSaham =
                                provider.digitParser(val);
                          },
                        ),
                        SizedBox(
                          height: _size * 5,
                        ),
                        provider.isEdit
                            ? SantaraMainButton(
                                key: Key('btnSimpan'),
                                title: "Simpan Perubahan",
                                onPressed: () async {
                                  _hideKeyboard();
                                  await provider
                                      .nextProgress(context, 0)
                                      .then((value) {
                                    setState(() {
                                      provider.hasMakeChanges = false;
                                    });
                                  });
                                },
                              )
                            : SantaraMainButton(
                                key: Key('btnSelanjutnya'),
                                title: "Selanjutnya",
                                onPressed: () async {
                                  _hideKeyboard();
                                  await provider.nextProgress(context, 1);
                                },
                              )
                      ]),
                )),
          )),
    );
  }
}
