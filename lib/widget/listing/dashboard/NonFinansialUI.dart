import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/informasi_nonfinansial.dart';
import 'package:santaraapp/widget/listing/registrasi/RegistrasiStepsUI.dart';
import 'package:santaraapp/widget/widget/components/listing/QuestionsList.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class InformasiNonFinansialUI extends StatefulWidget {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  InformasiNonFinansialUI({@required this.isEdit, @required this.emitenDetail});

  @override
  _InformasiNonFinansialUIState createState() =>
      _InformasiNonFinansialUIState(isEdit, emitenDetail);
}

class _InformasiNonFinansialUIState extends State<InformasiNonFinansialUI> {
  bool isEdit;
  PralistingEmiten emitenDetail;
  _InformasiNonFinansialUIState(this.isEdit, this.emitenDetail);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
          create: (_) => InformasiNonFinansialProvider(
              isEdit: isEdit, emitenDetail: emitenDetail)),
    ], child: NonFinansialUI());
  }
}

class NonFinansialUI extends StatefulWidget {
  @override
  _NonFinansialUIState createState() => _NonFinansialUIState();
}

class _NonFinansialUIState extends State<NonFinansialUI> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider =
        Provider.of<InformasiNonFinansialProvider>(context, listen: true);
    if (provider.firstFetch) {
      provider.fetchQuestionEdit();
    }
    return WillPopScope(
      onWillPop: () async {
        if (provider.hasMakeChanges) {
          PopupHelper.handleClosePage(context);
          RegistrationHelper.steps.infoNonFinansial = false;
        } else {
          Navigator.pop(context, 'refresh');
        }
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 2,
          title: Text("Informasi Non-Finansial",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            provider.isEdit
                ? Container()
                : IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrasiStepsUI()));
                    })
          ],
        ),
        body: Container(
          color: Colors.white,
          height: SizeConfig.screenHeight,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.all(_size * 2.5),
                  itemCount: provider.questionModel.length,
                  itemBuilder: (context, index) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        provider.isSubmitError
                            ? provider.questionModel[index].groupValue == -1
                                ? Padding(
                                    padding: EdgeInsets.only(left: _size * 2.5),
                                    child: Text(
                                      "*Mohon isi pilihan dibawah ini",
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: _size * 3),
                                    ),
                                  )
                                : Container()
                            : Container(),
                        QuestionsList(
                            questionModel: provider.questionModel[index],
                            onChanged: (val) =>
                                provider.handleSelectAnswer(index, val)),
                      ],
                    );
                  },
                ),
                Container(
                  padding: EdgeInsets.all(_size * 5),
                  child: provider.isEdit
                      ? SantaraMainButton(
                          key: Key('btnSimpan'),
                          title: "Simpan Perubahan",
                          onPressed: () async {
                            await provider.nextProccess(context, 0);
                          },
                        )
                      : SantaraMainButton(
                          key: Key('btnSelanjutnya'),
                          title: "Selanjutnya",
                          onPressed: () async {
                            await provider.nextProccess(context, 1);
                          },
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
