import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import '../../../helpers/PopupHelper.dart';
import '../../../helpers/RegistrationHelper.dart';
import '../../../helpers/SizeConfig.dart';
import '../../../models/Regency.dart';
import '../../../models/listing/Category.dart';
import '../../../models/listing/PralistingEmiten.dart';
import '../../../utils/logger.dart';
import '../../widget/components/main/SantaraAlertDialog.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../../widget/components/main/SantaraDocumentPicker.dart';
import '../../widget/components/main/SantaraDropDown.dart';
import '../../widget/components/main/SantaraField.dart';
import '../registrasi/RegistrasiStepsUI.dart';
import 'pages/SearchRegency.dart';
import 'viewmodel/identitas_calon_penerbit.dart';

class IdentitasPenerbitUI extends StatefulWidget {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  IdentitasPenerbitUI({@required this.isEdit, @required this.emitenDetail});
  @override
  _IdentitasPenerbitUIState createState() =>
      _IdentitasPenerbitUIState(isEdit, emitenDetail);
}

class _IdentitasPenerbitUIState extends State<IdentitasPenerbitUI> {
  bool isEdit;
  PralistingEmiten emitenDetail;
  _IdentitasPenerbitUIState(this.isEdit, this.emitenDetail);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
          create: (_) => IdentitasCalonPenerbitProvider(
              isEdit: isEdit, emitenDetail: emitenDetail)),
    ], child: IdentitasPenerbitBody());
  }
}

class IdentitasPenerbitBody extends StatefulWidget {
  @override
  _IdentitasPenerbitBodyState createState() => _IdentitasPenerbitBodyState();
}

class _IdentitasPenerbitBodyState extends State<IdentitasPenerbitBody> {
  @override
  void initState() {
    super.initState();
  }

  void handleRadio(String value) {
    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: false);
    provider.bentukBadanUsaha = value;
    if (!provider.isBentukBadanUsahaValid) {
      provider.isBentukBadanUsahaValid = true;
    }
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  // field input widget
  Widget inputFieldsBuilder(BuildContext context) {
    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: false);
    var _size = SizeConfig.safeBlockHorizontal;
    if (provider.firstFetch) {
      if (provider.categoryState == FetchCategoryState.loading) {
        provider.fetchCategories(context);
        provider.categoryState = FetchCategoryState.loaded;
      }

      if (provider.regencyState == FetchRegencyState.loading) {
        provider.fetchRegency(context);
        // provider.regencyState = FetchRegencyState.loaded;
      }

      if (provider.isEdit) {
        provider.bentukBadanUsaha = provider.emitenDetail.businessEntity;
        provider.fetchSubcategories(context, provider.emitenDetail.categoryId);
        provider.category = provider.emitenDetail.categoryId;
        if (provider.emitenDetail.subCategoryId != null) {
          provider.subcategory = provider.emitenDetail.subCategoryId;
        }
        // jika kondisi form adalah edit, maka setelah app selesai memuat regencies, categories, atau subcategories
        // maka nilai hasMakeChanges dikembalikan ke false
        provider.hasMakeChanges = false;
      }
      provider.firstFetch = false;
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(_size * 5, _size * 5, _size * 5, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // nama perusahaan
          SantaraTextField(
            key: Key('txtNamaPerusahaan'),
            // enabled: !provider.isEdit,
            initialValue:
                !provider.isEdit ? "" : provider.emitenDetail.companyName,
            hintText: "PT. Ayam Warna Warni",
            labelText: "Nama Perusahaan",
            inputType: TextInputType.text,
            validator: provider.handleNamaPerusahaan,
            onSaved: (String val) {
              provider.namaPerusahaan = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          // bidang usaha
          SantaraTextField(
            key: Key('txtMerekDagang'),
            // enabled: !provider.isEdit,
            initialValue:
                !provider.isEdit ? "" : provider.emitenDetail.trademark,
            hintText: "Sop Ayam Pak Min",
            labelText: "Merek Dagang",
            inputType: TextInputType.text,
            validator: provider.handlemerekDagang,
            onSaved: (String val) {
              provider.merekDagang = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          Container(
            height: _size * 15,
            child: SantaraDropDown(
              key: Key('selectJenisUsaha'),
              icon: provider.categoryState == FetchCategoryState.loading
                  ? CupertinoActivityIndicator()
                  : null,
              isExpanded: true,
              hint: Text("Jenis Usaha"),
              items: provider.jenisUsaha.map((Category e) {
                return DropdownMenuItem(value: e.id, child: Text(e.category));
              }).toList(),
              value: provider.categoryState == FetchCategoryState.loaded
                  ? provider.isEdit
                      ? provider.emitenDetail != null
                          ? provider.category < 1
                              ? provider.emitenDetail.categoryId
                              : provider.category
                          : 0
                      : provider.category < 1
                          ? 0
                          : provider.category
                  : 0,
              onChanged: (selected) {
                _hideKeyboard();
                provider.category = selected;
                provider.resetSubcategories();
                provider.fetchSubcategories(context, selected);
                if (!provider.isCategoryValid) {
                  provider.isCategoryValid = true;
                }
                // print("Changed");
              },
            ),
          ),
          provider.isCategoryValid
              ? Container()
              : Container(
                  margin: EdgeInsets.only(top: _size),
                  height: _size * 5,
                  child: Text(
                    "*Jenis usaha tidak boleh kosong!",
                    style: TextStyle(color: Colors.red, fontSize: _size * 3),
                  ),
                ),
          SizedBox(
            height: _size * 5,
          ),
          Container(
            height: _size * 15,
            child: SantaraDropDown(
              key: Key('selectSubSektorEkonomi'),
              icon: provider.subcategoryState == FetchSubcategoryState.loading
                  ? CupertinoActivityIndicator()
                  : null,
              isExpanded: true,
              hint: Text("Pilih Sub Sektor Ekonomi"),
              items: provider.subJenisUsaha != null &&
                      provider.subJenisUsaha.length > 0
                  ? provider.subJenisUsaha.map((Subcategory e) {
                      return DropdownMenuItem(
                          value: e.id, child: Text(e.subCategory));
                    }).toList()
                  : [],
              value: provider.subcategory == null || provider.subcategory < 1
                  ? 0
                  : provider.subcategory,
              onChanged: (selected) {
                _hideKeyboard();
                provider.subcategory = selected;
                if (!provider.isSubCategoryValid) {
                  provider.isSubCategoryValid = true;
                }
                // print("Changed");
              },
            ),
          ),
          provider.isSubCategoryValid
              ? Container()
              : Container(
                  margin: EdgeInsets.only(top: _size),
                  height: _size * 5,
                  child: Text(
                    "*Sub sektor ekonomi tidak boleh kosong!",
                    style: TextStyle(color: Colors.red, fontSize: _size * 3),
                  ),
                ),
          SizedBox(
            height: _size * 5,
          ),
          // lokasii
          InkWell(
            onTap: () async {
              if (provider.regencyState == FetchRegencyState.loaded) {
                final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SearchRegency(
                      regencies: provider.regencies,
                    ),
                  ),
                );
                if (result != null) {
                  provider.regency = result;
                }
              }
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                border: Border.all(
                  width: 2,
                  color: Color(
                    0xffDDDDDD,
                  ),
                ),
              ),
              child: AbsorbPointer(
                absorbing: true,
                child: SearchableDropdown(
                  key: Key('selectKotaLokasiUsaha'),
                  icon: provider.regencyState == FetchRegencyState.loading
                      ? CupertinoActivityIndicator()
                      : Icon(Icons.arrow_drop_down),
                  items: provider.regencies.map((Regency e) {
                    return DropdownMenuItem(value: e, child: Text(e.name));
                  }).toList(),
                  underline: Container(),
                  value: provider.regency,
                  hint: Text("Kota Lokasi Usaha"),
                  searchHint: Text("Kota Lokasi Usaha"),
                  onChanged: (Regency selected) async {
                    provider.regency = Regency();
                    await Future.delayed(Duration(milliseconds: 100));
                    // print(">> Selected : ");
                    // print(selected.name);werty
                    _hideKeyboard();
                    provider.regency = selected;
                    if (!provider.isRegencyValid) {
                      provider.isRegencyValid = true;
                    }
                    setState(() {});
                  },
                  isExpanded: true,
                  searchFn: (String keyword, items) {
                    List<int> ret = List<int>();
                    if (keyword != null &&
                        items != null &&
                        keyword.isNotEmpty) {
                      keyword.split(" ").forEach((k) {
                        int i = 0;
                        items.forEach((item) {
                          if (k.isNotEmpty &&
                              (item.value.name
                                  .toString()
                                  .toLowerCase()
                                  .contains(k.toLowerCase()))) {
                            ret.add(i);
                          }
                          i++;
                        });
                      });
                    }
                    if (keyword.isEmpty) {
                      ret = Iterable<int>.generate(items.length).toList();
                    }
                    return (ret);
                  },
                  // validator: (value) {
                  //   if (value == null) {
                  //     return "Kota lokasi usaha tidak boleh kosong";
                  //   } else {
                  //     return null;
                  //   }
                  // },
                ),
              ),

              // child: SantaraDropDown(
              //   icon: provider.regencyState == FetchRegencyState.loading
              //       ? CupertinoActivityIndicator()
              //       : null,
              //   isExpanded: true,
              //   hint: Text("Kota Lokasi Usaha"),
              //   items: provider.regencies.map((Regency e) {
              //     return DropdownMenuItem(value: e, child: Text(e.name));
              //   }).toList(),
              //   value: provider.regency == null
              //       ? provider.regencies[0]
              //       : provider.regency,
              // provider.regencyState == FetchRegencyState.loaded
              //     ? provider.isEdit
              //         ? provider.emitenDetail != null
              //             ? Regency(
              //                 id: provider.emitenDetail.subCategoryId,
              //                 uuid: "",
              //                 name: "",
              //                 provinceId: 0,
              //               )
              //             : provider.regencies[0]
              //         : provider.regency == null
              //             ? provider.regencies[0]
              //             : provider.regency
              //     : provider.regencies[0],
              // onChanged: (selected) {
              //   _hideKeyboard();
              //   provider.regency = selected;
              //   if (!provider.isRegencyValid) {
              //     provider.isRegencyValid = true;
              //   }
              //   print("Changed");
              // },
              // ),
            ),
          ),
          provider.isRegencyValid
              ? Container()
              : Container(
                  margin: EdgeInsets.only(top: _size),
                  height: _size * 5,
                  child: Text(
                    "*Kota lokasi usaha tidak boleh kosong!",
                    style: TextStyle(color: Colors.red, fontSize: _size * 3),
                  ),
                ),
          SizedBox(
            height: _size * 5,
          ),
          // SantaraTextField(
          //   initialValue: !provider.isEdit ? "" : provider.emitenDetail.city,
          //   hintText: "Yogyakarta",
          //   labelText: "Kota Lokasi Usaha",
          //   inputType: TextInputType.text,
          //   validator: provider.handlekotaLokasiUsaha,
          //   onSaved: (String val) {
          //     provider.kotaLokasiUsaha = val;
          //   },
          // ),
          // SizedBox(
          //   height: _size * 5,
          // ),
          // alamad
          SantaraTextField(
            key: Key('txtAlamatPerusahaan'),
            // enabled: !provider.isEdit,
            initialValue: !provider.isEdit ? "" : provider.emitenDetail.address,
            hintText: """
Alamat lengkap. 
              """,
            labelText: "Alamat Lengkap Perusahaan",
            lines: 3,
            height: 100,
            inputType: TextInputType.multiline,
            validator: provider.handlealamatLengkapUsaha,
            onSaved: (String val) {
              provider.alamatLengkapUsaha = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
        ],
      ),
    );
  }

  // field input widget
  Widget inputSecondFields(BuildContext context) {
    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: false);
    var _size = SizeConfig.safeBlockHorizontal;
    return Padding(
      padding: EdgeInsets.fromLTRB(_size * 5, _size * 5, _size * 5, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // lokasii
          SantaraTextField(
            key: Key('txtLamaUsaha'),
            // enabled: !provider.isEdit,
            initialValue: !provider.isEdit
                ? ""
                : provider.emitenDetail.businessLifespan.toString(),
            hintText: "5",
            labelText: "Lama Usaha (bulan)",
            inputType: TextInputType.number,
            validator: provider.handlelamaUsaha,
            onSaved: (val) {
              provider.lamaUsaha = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          // lokasii
          SantaraTextField(
            key: Key('txtJumlahCabang'),
            // enabled: !provider.isEdit,
            initialValue: !provider.isEdit
                ? "0"
                : provider.emitenDetail.branchCompany.toString(),
            hintText: "10",
            labelText: "Jumlah Cabang",
            inputType: TextInputType.number,
            validator: provider.handlejumlahCabang,
            onSaved: (String val) {
              provider.jumlahCabang = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          // lokasii
          SantaraTextField(
            // enabled: !provider.isEdit,
            key: Key('txtJumlahKaryawan'),
            initialValue: !provider.isEdit
                ? "0"
                : provider.emitenDetail.employee.toString(),
            hintText: "200",
            labelText: "Jumlah Karyawan",
            inputType: TextInputType.number,
            validator: provider.handlejumlahKaryawan,
            onSaved: (val) {
              provider.jumlahKaryawan = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          SantaraTextField(
            key: Key('txtDeskripsiUsaha'),
            initialValue: !provider.isEdit
                ? ""
                : provider.emitenDetail.businessDescription,
            hintText: """
Deskripsi usaha 
              """,
            labelText: "Deskripsi usaha",
            lines: 3,
            height: 100,
            inputType: TextInputType.multiline,
            validator: provider.handledeskripsiUsaha,
            onSaved: (String val) {
              provider.deskripsiUsaha = val;
            },
          ),
          SizedBox(
            height: _size * 5,
          ),
          Container(
            child: provider.isEdit
                ? SantaraMainButton(
                    key: Key('btnSimpan'),
                    title: "Simpan Perubahan",
                    onPressed: () async {
                      _hideKeyboard();
                      await provider.validateInputs(context, 0).then((value) {
                        provider.hasMakeChanges = false;
                      });
                    },
                  )
                : SantaraMainButton(
                    key: Key('btnSelanjutnya'),
                    title: "Selanjutnya",
                    onPressed: () {
                      try {
                        _hideKeyboard();
                        provider.validateInputs(context, 1);
                      } catch (e, stack) {
                        santaraLog(e, stack);
                      }
                    },
                  ),
          ),
          SizedBox(
            height: _size * 5,
          ),
        ],
      ),
    );
  }

  // berkas badan usaha
  Widget berkasBadanBuilder(BuildContext context) {
    return SantaraDocPicker(
        title: "Berkas Badan Usaha",
        isEmpty: true,
        onPressed: () {
          // print("Select File");
        });
  }

  // berkas badan usaha
  Widget docPerizinanBuilder(BuildContext context) {
    return SantaraDocPicker(
        title: "Dokumen Perizinan",
        isEmpty: true,
        onPressed: () {
          // print("Select File");
        });
  }

  // checkbox widget
  Widget checkboxDocument(BuildContext context, CheckBoxModel data) {
    return Padding(
      padding: EdgeInsets.only(bottom: 15),
      child: Row(
        children: <Widget>[
          Container(
            height: 18,
            width: 18,
            child: Checkbox(
                activeColor: Colors.black,
                onChanged: (value) {
                  setState(() {
                    data.isChecked
                        ? data.isChecked = false
                        : data.isChecked = true;
                  });
                },
                value: data.isChecked),
          ),
          Container(
            width: 10,
          ),
          Text(
            data.title,
            style: TextStyle(color: Color(0xff676767), fontSize: 15),
          )
        ],
      ),
    );
  }

  // dokumen perizinan yang dimiliki
  Widget docPerizinanDimilikiBuilder(BuildContext context) {
    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: false);
    if (provider.docsPerizinan.length < 1) {
      provider.initializePerizinan();
    }
    SizeConfig().init(context);
    return Container(
        margin: EdgeInsets.all(20),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 15),
                child: Text(
                  "Dokumen Perizinan yang Dimiliki",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4),
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: provider.docsPerizinan.length,
                itemBuilder: (context, index) {
                  return checkboxDocument(
                      context, provider.docsPerizinan[index]);
                },
              )
            ]));
  }

  // radio button buat bentuk usaha
  Widget bentukUsahaBuilder(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;

    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: false);
    return Container(
      margin: EdgeInsets.only(right: _size * 2.5, top: 0, left: _size * 2.5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: _size * 1.25, left: _size * 2.5),
            child: Text(
              "Bentuk Badan Usaha",
              style:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: _size * 4),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "PT",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("PT")
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "CV",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("CV")
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "UD",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("UD")
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "Firma",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("Firma")
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "Koperasi",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("Koperasi")
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: "Yang Lain",
                      groupValue: provider.bentukBadanUsaha,
                      onChanged: handleRadio,
                    ),
                    Text("Yang Lain")
                  ],
                ),
              ),
            ],
          ),
          provider.isBentukBadanUsahaValid
              ? Container()
              : Container(
                  margin: EdgeInsets.only(top: 2, left: _size * 2.5),
                  height: _size * 5,
                  child: Text(
                    "*Bentuk badan usaha tidak boleh kosong!",
                    style: TextStyle(color: Colors.red, fontSize: _size * 3),
                  ),
                ),
        ],
      ),
    );
  }

  // widget dialog
  void showAlertDialog(BuildContext context) {
    SizeConfig().init(context);
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        title: "Konfirmasi Perubahan",
        description: "Apakah Anda yakin ingin memperbarui data ?",
        onYakin: () {
          Navigator.pop(context);
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // var _size = SizeConfig.safeBlockHorizontal;
    final provider =
        Provider.of<IdentitasCalonPenerbitProvider>(context, listen: true);
    return WillPopScope(
      onWillPop: () async {
        if (provider.hasMakeChanges) {
          PopupHelper.handleClosePage(context);
          RegistrationHelper.steps.identitasPenerbit = false;
        } else {
          Navigator.pop(context, 'refresh');
        }
        return;
      },
      child: Scaffold(
        key: provider.identitasScaffold,
        appBar: AppBar(
          elevation: 2,
          title: Text("Identitas Calon Penerbit",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            provider.isEdit
                ? Container()
                : IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrasiStepsUI()));
                    })
          ],
        ),
        body: Container(
          height: SizeConfig.screenHeight,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Form(
              key: provider.calonPenerbitFormKey,
              autovalidate: provider.autoValidate,
              child: Column(
                // shrinkWrap: true,
                // physics: NeverScrollableScrollPhysics(),
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // editImageSection(context),
                  inputFieldsBuilder(context),
                  bentukUsahaBuilder(context),
                  inputSecondFields(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
