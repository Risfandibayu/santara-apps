import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/helpers/YoutubeThumbnailGenerator.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/models/listing/UploadImageModel.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/listing/dashboard/viewmodel/media_bisnis.dart';
import 'package:santaraapp/widget/listing/detail_emiten/YoutubePlayerUI.dart';
import 'package:santaraapp/widget/listing/registrasi/RegistrasiStepsUI.dart';
import 'package:santaraapp/widget/widget/components/listing/SantaraNotification.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:shimmer/shimmer.dart';

class MediaBisnisUI extends StatefulWidget {
  final bool isEdit;
  final PralistingEmiten emitenDetail;
  MediaBisnisUI({@required this.isEdit, @required this.emitenDetail});

  @override
  _MediaBisnisUIState createState() =>
      _MediaBisnisUIState(isEdit, emitenDetail);
}

class _MediaBisnisUIState extends State<MediaBisnisUI> {
  bool isEdit;
  PralistingEmiten emitenDetail;
  _MediaBisnisUIState(this.isEdit, this.emitenDetail);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
          create: (_) =>
              MediaBisnisProvider(isEdit: isEdit, emitenDetail: emitenDetail)),
    ], child: MediaBisnisBody());
  }
}

class MediaBisnisBody extends StatefulWidget {
  @override
  _MediaBisnisBodyState createState() => _MediaBisnisBodyState();
}

class _MediaBisnisBodyState extends State<MediaBisnisBody>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  final key = new GlobalKey<ScaffoldState>();

  Widget imageCard(BuildContext context, UploadDataImage image) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    var _height = MediaQuery.of(context).size.height;
    var _newPicture = apiLocalImage + "/uploads/emiten_picture/" + image.url;
    return Container(
      width: _size * 62.5,
      height: _height / 4.5,
      margin: EdgeInsets.only(right: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Stack(
          children: [
            SantaraCachedImage(
              image: _newPicture,
              fit: BoxFit.cover,
            ),
            ClipRRect(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5)),
              child: image.isMain
                  ? Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        width: double.infinity,
                        height: _size * 7.5,
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.7),
                        ),
                        child: Center(
                          child: Text(
                            "Gambar Utama",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }

  Widget newImageCard(BuildContext context, UploadDataImage image) {
    var _height = MediaQuery.of(context).size.height;
    var _size = SizeConfig.safeBlockHorizontal;

    return Container(
      width: _size * 62.5,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      height: _height / 4.5,
      margin: EdgeInsets.only(right: _size * 2.5),
      child: AssetThumb(
          asset: image.asset,
          width: (_size * 62.5).toInt(),
          height: (_height / 4.5).floor()),
    );
  }

  Widget imageCardUploading(BuildContext context, UploadDataImage image) {
    var _height = MediaQuery.of(context).size.height;
    // final provider = Provider.of<MediaBisnisProvider>(context, listen:true);
    var _size = SizeConfig.safeBlockHorizontal;

    return Container(
        width: _size * 62.5,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        height: _height / 4.5,
        margin: EdgeInsets.only(right: _size * 2.5),
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
              child: FittedBox(
                fit: BoxFit.cover,
                child: SizedBox(
                  width: _size * 62.5,
                  height: _height / 4.5,
                  child: AssetThumb(
                      asset: image.asset,
                      width: (_size * 62.5).toInt(),
                      height: (_height / 4.5).floor()),
                ),
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              height: _height / 4.5,
              width: image.progress,
              color: Colors.blue.withOpacity(.7),
            ),
            Shimmer.fromColors(
                baseColor: Colors.black.withOpacity(.5),
                highlightColor: Colors.white.withOpacity(.5),
                child: Container(
                  height: _height / 4.5,
                  width: _size * 62.5,
                  color: Colors.white.withOpacity(0.5),
                )),
          ],
        ));
  }

  Widget imagePicker(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      height: _height / 5.5,
      width: _size * 62.5,
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Color(0xffB8B8B8)),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.file_upload,
            size: _size * 10,
            color: Color(0xffB8B8B8),
          ),
          Container(height: _size),
          Text(
            "Unggah Gambar",
            style: TextStyle(color: Color(0xffB8B8B8)),
          )
        ],
      ),
    );
  }

  void _showSnackbar(String msg) {
    key.currentState.showSnackBar(SnackBar(
      content: Text(
        "$msg",
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.redAccent,
    ));
  }

  Widget editImageSection(BuildContext context) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: true);
    if (provider.imagelist.length < 1) {
      provider.getImages();
    }
    var _height = SizeConfig.screenHeight;
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(_size * 5, _size * 5, 0, _size * 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SantaraNotification(
                message: Text(
                  "Kualitas dan kelengkapan foto/video yang baik dapat menjadi bahan pertimbangan para investor dalam menentukan pengajuan bisnis yang Anda miliki.",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: _size * 2.875),
                ),
                backgroundColor: Color(0xffEEF1FF)),
            Container(
              height: SizeConfig.safeBlockHorizontal * 8.75,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Tampilkan 10 Foto Mengenai Usaha Anda",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: _size * 3.5),
              ),
              subtitle: Text("Ukuran file maksimal 20 Mb"),
            ),
            Container(
                height: _height / 5,
                padding: EdgeInsets.only(bottom: 4),
                child: provider.imagelist.length > 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        cacheExtent: 100.0,
                        scrollDirection: Axis.horizontal,
                        itemCount: provider.imagelist.length < 10
                            ? provider.imagelist.length + 1
                            : provider.imagelist.length,
                        itemBuilder: (context, index) {
                          UploadDataImage img =
                              index == provider.imagelist.length
                                  ? null
                                  : provider.imagelist[index];
                          return index == provider.imagelist.length
                              ? InkWell(
                                  onTap: () {
                                    provider.isInProgress
                                        ? _showSnackbar(
                                            "Mohon tunggu proses upload selesai")
                                        : provider.pickImageV2(context);
                                  },
                                  child: imagePicker(context))
                              : InkWell(
                                  onTap: () {
                                    _showModal(context, img);
                                  },
                                  child: img.isUploading
                                      ? newImageCard(context, img)
                                      : imageCard(context, img));
                        },
                      )
                    : InkWell(
                        onTap: () {
                          provider.isInProgress
                              ? _showSnackbar(
                                  "Mohon tunggu proses upload selesai")
                              : provider.pickImageV2(context);
                        },
                        child: imagePicker(context))),
            Text("Tekan lama pada gambar untuk menampilkan pengaturan",
                style:
                    TextStyle(color: Colors.grey[700], fontSize: _size * 3.25),
                overflow: TextOverflow.visible),
          ],
        ));
  }

  Widget _videoCard(BuildContext context, String url) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(right: _size * 5),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => YoutubePlayerUI(
                        url: "$url",
                      )));
        },
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        // "https://image.shutterstock.com/image-photo/closeup-young-asian-beautiful-woman-260nw-1111221518.jpg"
                        image: url == null || url == ""
                            ? AssetImage("assets/Preload.jpeg")
                            : NetworkImage(
                                YoutubeThumbnailGenerator.getThumbnail(
                                    url, ThumbnailQuality.medium),
                              ),
                        fit: BoxFit.cover)),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[850].withOpacity(0.7),
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    height: _size * 12.5,
                    width: _size * 12.5,
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: _size * 10,
                    ),
                  ),
                ))),
      ),
    );
  }

  // handle on click
  // nambah index untuk set gambar utama
  _handleImageClick(BuildContext context, EmitenAssets image) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: false);
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: context,
        builder: (context) => Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                topRight: Radius.circular(5),
              )),
              child: Wrap(
                children: <Widget>[
                  ListTile(
                    title: Text("Ubah Gambar"),
                    onTap: () {
                      provider.regPickSingleImageV2(context, image);
                      Navigator.pop(context);
                    },
                  ),
                  Container(
                    height: 4,
                    width: double.infinity,
                    color: Color(0xffF4F4F4),
                  ),
                  ListTile(
                    title: Text("Hapus Gambar"),
                    onTap: () {
                      provider.regDeleteImage(image);
                      Navigator.pop(context);
                    },
                  ),
                  Container(
                    height: 4,
                    width: double.infinity,
                    color: Color(0xffF4F4F4),
                  ),
                  ListTile(
                    title: Text("Jadikan Gambar Utama"),
                    onTap: () {
                      provider.setAsMainImage(image);
                      Navigator.pop(context);
                    },
                  ),
                  Container(
                    height: 5,
                  )
                ],
              ),
            ));
  }

  Widget regImageCard(BuildContext context, EmitenAssets image) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    var _height = MediaQuery.of(context).size.height;
    return Container(
        width: _size * 62.5,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        height: _height / 4.5,
        margin: EdgeInsets.only(right: 10),
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
              child: FittedBox(
                fit: BoxFit.cover,
                child: SizedBox(
                  width: _size * 62.5,
                  height: _height / 4.5,
                  child: AssetThumb(
                      asset: image.asset,
                      width: (_size * 62.5).toInt(),
                      height: (_height / 4.5).floor()),
                ),
              ),
            ),
            image.isMain
                ? Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: double.infinity,
                      height: _size * 7.5,
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.7),
                      ),
                      child: Center(
                        child: Text(
                          "Gambar Utama",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ));
  }

  Widget newImageSection(BuildContext context) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: true);
    var _height = SizeConfig.screenHeight;
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(_size * 5, _size * 5, 0, _size * 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SantaraNotification(
                message: Text(
                  "Kualitas dan kelengkapan foto/video yang baik dapat menjadi bahan pertimbangan para investor dalam menentukan pengajuan bisnis yang Anda miliki.",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: _size * 2.875),
                ),
                backgroundColor: Color(0xffEEF1FF)),
            Container(
              height: SizeConfig.safeBlockHorizontal * 8.75,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Tampilkan 10 Foto Mengenai Usaha Anda",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: _size * 3.5),
              ),
              subtitle: Text("Ukuran file maksimal 20 Mb"),
            ),
            // Container(
            //   height: _size * 2,
            // ),
            Container(
                height: _height / 5,
                child: RegistrationHelper.emiten.assets != null
                    ? ListView.builder(
                        shrinkWrap: true,
                        cacheExtent: 100.0,
                        scrollDirection: Axis.horizontal,
                        itemCount: RegistrationHelper.emiten.assets.length < 10
                            ? RegistrationHelper.emiten.assets.length + 1
                            : RegistrationHelper.emiten.assets.length,
                        itemBuilder: (context, index) {
                          EmitenAssets img =
                              index == RegistrationHelper.emiten.assets.length
                                  ? EmitenAssets()
                                  : RegistrationHelper.emiten.assets[index];
                          return index ==
                                  RegistrationHelper.emiten.assets.length
                              ? InkWell(
                                  key: Key('pickImage'),
                                  onTap: () {
                                    provider.pickImagesReg(context);
                                  },
                                  child: imagePicker(context))
                              : InkWell(
                                  onTap: () {
                                    _handleImageClick(context, img);
                                  },
                                  child: regImageCard(context, img));
                        },
                      )
                    : InkWell(
                        key: Key('pickImage'),
                        onTap: () {
                          provider.pickImagesReg(context);
                        },
                        child: imagePicker(context))),
          ],
        ));
  }

  Widget youtubeVideoSection(BuildContext context) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: true);
    var _size = SizeConfig.safeBlockHorizontal;
    if (provider.isEdit) {
      if (provider.firstSet) {
        provider.setYoutubeEdit();
      }
    }
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(_size * 5, 0, 0, _size * 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Video Tentang Usaha Anda",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: _size * 3.5),
              ),
              subtitle: Text("Sematkan link video youtube mengenai usaha Anda"),
            ),
            Container(
              margin: EdgeInsets.only(right: _size * 5),
              child: TextField(
                key: Key('txtYoutube'),
                controller: provider.ytVideo,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    hintText: "https://www.youtube.com/watch?v=h7cxXtuRDaY",
                    errorText: provider.isYtError
                        ? "Mohon masukan url youtube yang valid"
                        : ""),
                onEditingComplete: () {
                  provider.hasMakeChanges = true;
                  FocusScope.of(context).unfocus();
                  provider.validateYtUrl(provider.ytVideo.text);
                  if (provider.isYtError) {
                  } else {
                    provider.setYt = true;
                    RegistrationHelper.emiten.youtube = provider.ytVideo.text;
                  }
                },
                onChanged: (val) {
                  if (val.length > 0) {
                    provider.hasMakeChanges = true;
                    if (provider.isYtError) {
                      provider.setYtError = false;
                      provider.setYt = false;
                    }
                  } else {
                    provider.setYt = false;
                    provider.setYtError = false;
                    provider.hasMakeChanges = false;
                  }
                },
              ),
            ),
            provider.isYtError
                ? Text(
                    "Tidak dapat memproses video, mohon periksa url yang anda masukan")
                : provider.isYtSetted
                    ? Container(
                        height: SizeConfig.screenHeight / 4,
                        child: _videoCard(context, provider.ytVideo.text))
                    : Container(),
            Container(
              margin: EdgeInsets.only(right: _size * 5, top: _size * 5),
              child: provider.isEdit
                  ? SantaraMainButton(
                      title: "Simpan Perubahan",
                      onPressed: () {
                        provider.simpanPerubahan(context);
                      },
                    )
                  : SantaraMainButton(
                      title: "Selanjutnya",
                      onPressed: () async {
                        await provider.validateForm(context);
                      },
                    ),
            )
          ],
        ));
  }

  _showModal(BuildContext context, UploadDataImage image) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: false);
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: context,
        builder: (context) => Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                topRight: Radius.circular(5),
              )),
              child: Wrap(
                children: <Widget>[
                  image.progress == 666.0
                      ? Container()
                      : ListTile(
                          title: Text("Ubah Gambar"),
                          onTap: () {
                            // updateGambar
                            // var emit = EmitenAssets(
                            //     id: 0,
                            //     filepath: image.filepath,
                            //     type: 'image',
                            //     url: image.url,
                            //     asset: image.asset,
                            //     isMain: image.isMain);
                            image.isUploading
                                ? provider.pickSingleImage(context, image)
                                : provider.regPickSingleImage(context, image);
                            Navigator.pop(context);
                          },
                        ),
                  Container(
                    height: 4,
                    width: double.infinity,
                    color: Color(0xffF4F4F4),
                  ),
                  ListTile(
                    title: Text("Hapus Gambar"),
                    onTap: () {
                      if (provider.imagelist.length < 2) {
                        Navigator.pop(context);
                        PopupHelper.showPopMessage(
                          context,
                          "Tidak Dapat Menghapus",
                          "Gambar harus diisi, anda harus mengunggah gambar lain sebelum menghapus gambar ini.",
                        );
                      } else {
                        image.isUploading
                            ? provider.deleteImage(image)
                            : provider.deleteImageEdit(image);
                        Navigator.pop(context);
                      }
                    },
                  ),
                  Container(
                    height: 4,
                    width: double.infinity,
                    color: Color(0xffF4F4F4),
                  ),
                  image.isUploading
                      ? ListTile(
                          title: Text("Tidak Dapat Dijadikan Gambar Utama"),
                          subtitle:
                              Text("Mohon Simpan Perubahan Terlebih Dahulu"),
                        )
                      : ListTile(
                          title: Text("Jadikan Gambar Utama"),
                          onTap: () {
                            provider.editSetGambarUtama(context, image);
                            Navigator.pop(context);
                          },
                        ),
                  Container(
                    height: 5,
                  )
                ],
              ),
            ));
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
      reverseDuration: Duration(milliseconds: 400),
    );
    Future.delayed(Duration(milliseconds: 100), () {
      final provider = Provider.of<MediaBisnisProvider>(context, listen: false);
      if (provider.emitenState == EmitenState.loading) {
        provider.fetchEmitenDetail(context, provider.emitenDetail.uuid);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    RegistrationHelper.emiten.assets.clear();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<MediaBisnisProvider>(context, listen: false);
    var _height = SizeConfig.screenHeight;
    return WillPopScope(
      onWillPop: () async {
        if (provider.hasMakeChanges) {
          PopupHelper.handleClosePage(context);
          RegistrationHelper.steps.media = false;
        } else {
          Navigator.pop(context);
        }
        return false;
      },
      child: Scaffold(
        key: key,
        appBar: AppBar(
          elevation: 2,
          title: Text("Media",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            provider.isEdit
                ? Container()
                : IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrasiStepsUI()));
                    })
          ],
        ),
        body: Container(
          height: _height,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                provider.isEdit
                    ? editImageSection(context)
                    : newImageSection(context),
                youtubeVideoSection(context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
