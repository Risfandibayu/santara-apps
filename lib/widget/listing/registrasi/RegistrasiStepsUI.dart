import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/RegistrationHelper.dart';
import 'package:santaraapp/helpers/SizeConfig.dart';
import 'package:santaraapp/widget/widget/components/listing/SantaraStepper.dart';

class RegistrasiStepsUI extends StatefulWidget {
  @override
  _RegistrasiStepsUIState createState() => _RegistrasiStepsUIState();
}

class _RegistrasiStepsUIState extends State<RegistrasiStepsUI> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          title: Text("Form",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
          color: Colors.white,
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          child: Padding(
            padding: EdgeInsets.fromLTRB(_size * 8, _size * 15, _size * 8, 0),
            child: ListView(
              children: <Widget>[
                SantaraStepper(
                  title: "Identitas Calon Penerbit",
                  isActive: RegistrationHelper.steps.identitasPenerbit,
                ),
                SantaraStepper(
                  title: "Informasi Finansial",
                  isActive: RegistrationHelper.steps.infoFinansial,
                ),
                SantaraStepper(
                  title: "Informasi Non-Finansial",
                  isActive: RegistrationHelper.steps.infoNonFinansial,
                ),
                SantaraStepper(
                  title: "Lampiran Dokumen",
                  isActive: RegistrationHelper.steps.lampiran,
                ),
                SantaraStepper(
                  title: "Media",
                  isActive: RegistrationHelper.steps.media,
                  isEnd: true,
                ),
              ],
            ),
          ),
        ));
  }
}
