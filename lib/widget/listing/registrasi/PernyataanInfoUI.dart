import 'package:flutter/material.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/RegistrationHelper.dart';
import '../../../helpers/SizeConfig.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../models/listing/ListingDummies.dart';
import '../../../services/listing/listing_service.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../detail_emiten/DetailEmitenUI.dart';

class PernyataanInfoUI extends StatefulWidget {
  @override
  _PernyataanInfoUIState createState() => _PernyataanInfoUIState();
}

class _PernyataanInfoUIState extends State<PernyataanInfoUI> {
  ListingApiService listingApiService = ListingApiService();
  final pernyataanScaffold = GlobalKey<ScaffoldState>();
  bool _hasSuccessSubmit =
      false; // nandain apakah user udah pernah ngirim data, jika
  String uuid = '';
  String _error = '';
  void _showSuccessSubmit(String uuid) {
    var _size = SizeConfig.safeBlockHorizontal;
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(_size * 4),
              topRight: Radius.circular(_size * 4)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: _size * 5,
                ),
                Image.asset(
                  "assets/icon/success.png",
                  height: _size * 40,
                ),
                Container(
                  height: _size * 15,
                  child: Center(
                    child: Text('Pengajuan bisnis Anda berhasil dikirim!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: _size * 4.5,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  "Pengajuan Anda akan tayang setelah dilakukan pengecekan berdasarkan syarat dan ketentuan Santara. Pengajuan bisnis diluar jam kerja akan ditinjau pada jam kerja hari berikutnya",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: _size * 3.75),
                ),
                Padding(
                  padding: EdgeInsets.all(_size * 5),
                  child: SantaraMainButton(
                    title: "Lihat Detail",
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) => PralistingDetailPage(
                              status: 4,
                              uuid: uuid,
                              position: null,
                            ),
                          ),
                          (Route<dynamic> route) => true);
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  Future<void> _handleSubmitData() async {
    // print(">> Posting...");
    try {
      EmitenDetail _data = RegistrationHelper.emiten;
      await listingApiService.postRegistration(_data).then((value) async {
        // jika pakai api newPostRegistration ( plugn http dart, returnnya pakai yang ini )
        // final respStr = await value.stream.bytesToString();
        // final data = json.decode(respStr);
        // end newpostregiistration

        // jika pakai dio pakai ini
        var data = value.data;
        // end
        if (value != null) {
          if (value.statusCode == 200) {
            Navigator.pop(context);
            setState(() {
              // jika submit data berhasil successsubmit jadi true
              // fungsi variable ini untuk membatasi user agar tidak bisa kembali ke halaman sebelumnya
              // jika submit data berhasil. Jika user klik button back maka tidak ada action apa2
              // sebaliknya jika submit gagal user masih bisa kembali ke page sebelumnya
              _hasSuccessSubmit = true;
              uuid = data['uuid'];
            });
            _showSuccessSubmit(data['uuid']);
          } else {
            Navigator.pop(context);
            ToastHelper.showFailureToast(context, data['message']);
          }
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(
              context, 'Tidak dapat mengirim data, mohon ulangi proses ini!');
        }
      });
    } catch (e) {
      // print(">> Error : $e");
      setState(() {
        _error = e;
      });
      Navigator.pop(context);
      ToastHelper.showFailureToast(context, "$e");
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return WillPopScope(
      // jika submit data berhasil, maka user tidak bisa kembali ke halaman sebelumnya
      //
      onWillPop: () async {
        return _hasSuccessSubmit ? false : true;
      },
      child: Scaffold(
        key: pernyataanScaffold,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
          padding: EdgeInsets.all(_size * 5),
          color: Colors.white,
          height: SizeConfig.screenHeight,
          width: double.infinity,
          child:
              // _error.isNotEmpty
              //     ? Container(
              //         child: Text(
              //           _error,
              //           maxLines: 10,
              //         ),
              //       )
              //     :
              Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/santara/1.png",
                height: _size * 10,
              ),
              SizedBox(
                height: _size * 10,
              ),
              Text(
                "Pernyataan Informasi",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: _size * 4.5),
              ),
              SizedBox(
                height: _size * 5,
              ),
              Text(
                  "1. Dengan ini saya menyatakan, bahwa informasi yang saya sampaikan didalam form daftarkan  bisnis ini adalah sesuai dengan keadaan yang sebenar-benarnya."),
              SizedBox(
                height: _size * 4,
              ),
              Text(
                  "2. Bahwa saya menerima setiap hasil yang dikeluarkan oleh menu daftarkan bisnis ini dengan penuh kesadaran."),
              SizedBox(
                height: _size * 4,
              ),
              Text(
                  "3. Dengan disetujuinya surat pernyataan ini, maka saya tunduk pada ketentuan internal perihal seleksi calon penerbit yang dijalankan oleh platform Santara"),
              SizedBox(
                height: _size * 10,
              ),
              Text(
                "Apakah Anda setuju dengan Syarat dan Ketentuan diatas ?",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: _size * 3.5),
              ),
              SizedBox(
                height: _size * 5,
              ),
              SantaraMainButton(
                key: Key('btnSetuju'),
                title: "Saya Setuju",
                onPressed: () async {
                  if (_hasSuccessSubmit) {
                    _showSuccessSubmit(uuid);
                  } else {
                    PopupHelper.showLoading(context);
                    await _handleSubmitData();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
