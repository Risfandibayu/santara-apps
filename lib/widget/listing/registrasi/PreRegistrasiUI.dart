import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/KycStatusHelper.dart';
import '../../../helpers/RegistrationHelper.dart';
import '../../../helpers/SizeConfig.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../models/User.dart';
import '../../../models/listing/ListingDummies.dart';
import '../../../utils/api.dart';
import '../../../utils/helper.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../dashboard/IdentitasPenerbitUI.dart';

class PreRegistrasiUI extends StatefulWidget {
  @override
  _PreRegistrasiUIState createState() => _PreRegistrasiUIState();
}

class _PreRegistrasiUIState extends State<PreRegistrasiUI> {
  // ketika user click (i) modal disetor
  KycStatusHelper kycStatusHelper = KycStatusHelper();
  bool _hasLoaded = false;
  bool isCompleteKyc = true;
  void _handleProspektusKnowledge() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: _size * 4,
                          ),
                          Center(
                            child: Text(
                              "Prospektus adalah gabungan antara profil perusahaan dan laporan tahunan yang menjadikannya sebuah dokumen resmi yang digunakan oleh suatu lembaga/ perusahaan untuk memberikan gambaran mengenai saham yang ditawarkannya untuk dijual kepada publik dalam format PDF. Prospektus menjadi salah satu pertimbangan penting investor dalam mengambil keputusan pengajuan bisnis yang Anda ajukan",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color(0xff676767),
                                  fontSize: _size * 3),
                            ),
                          ),
                          Container(
                            height: _size * 8,
                          ),
                          Container(
                            height: _size * 8.5,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  Future<bool> kycStatus() async {
    final storage = FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    final uuid = await storage.read(key: 'uuid');
    if (Helper.check) {
      final http.Response response = await http.get(
          '$apiLocal/users/by-uuid/$uuid',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      final data = userFromJson(response.body);
      return data.trader.isVerified == 1 ? true : false;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    kycStatus().then((value) {
      setState(() {
        isCompleteKyc = value;
        _hasLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        color: Colors.white,
        width: double.infinity,
        height: SizeConfig.screenHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Persiapkan\nDokumen-Dokumen Berikut",
              style:
                  TextStyle(fontSize: _size * 4.5, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Container(
              height: _size * 5,
            ),
            Container(
              width: SizeConfig.screenWidth * .55,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: _size * 6,
                    width: _size * 6,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Color(0xffBF2D30)),
                    child: Center(
                      child: Text(
                        "1",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    width: _size * 3,
                  ),
                  Text(
                    "Prospektus Usaha",
                    style: TextStyle(fontSize: _size * 3.5),
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.info,
                        size: _size * 4,
                        color: Color(0xffDADADA),
                      ),
                      onPressed: _handleProspektusKnowledge)
                ],
              ),
            ),
            // Container(
            //   height: _size * 2,
            // ),
            Container(
              width: SizeConfig.screenWidth * .55,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    height: _size * 6,
                    width: _size * 6,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Color(0xffBF2D30)),
                    child: Center(
                      child: Text(
                        "2",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    width: _size * 3,
                  ),
                  Flexible(
                    child: Text(
                      "Foto dan Video Tentang Usaha",
                      style: TextStyle(fontSize: _size * 3.5),
                      maxLines: 2,
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: _size * 3,
            ),
            Container(
              margin: EdgeInsets.all(_size * 5),
              child: _hasLoaded
                  ? SantaraMainButton(
                      title: "Selanjutnya",
                      onPressed: () {
                        if (_hasLoaded) {
                          if (isCompleteKyc) {
                            RegistrationHelper.emiten =
                                EmitenDetail(assets: []);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => IdentitasPenerbitUI(
                                          isEdit: false,
                                          emitenDetail: null,
                                        )));
                          } else {
                            ToastHelper.showFailureToast(context,
                                "Status akun anda belum KYC, tidak dapat melanjutkan!");
                          }
                        } else {
                          ToastHelper.showFailureToast(context,
                              "Tidak dapat memeriksa status kyc, mohon periksa koneksi anda!");
                        }
                      },
                    )
                  : CupertinoActivityIndicator(),
            )
          ],
        ),
      ),
    );
  }
}
