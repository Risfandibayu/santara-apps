import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

import '../../../../core/utils/tools/logger.dart';
import '../../../../helpers/Constants.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../helpers/RupiahFormatter.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../models/listing/CommentModel.dart';
import '../../../../models/listing/ErrorHandler.dart';
import '../../../../models/listing/PralistingEmiten.dart';
import '../../../../services/listing/listing_service.dart';

enum EmitenState { loading, done, error, unauthorized }
enum CommentState { loading, done, empty, error }

class DetailEmitenProvider with ChangeNotifier {
  // set variable
  final ListingApiService apiService = ListingApiService();
  final rupiah =
      new NumberFormat("#,###"); // number format untuk rencana investasi
  TextEditingController invesCtrl =
      TextEditingController(); // controller investasi
  final _storage = new FlutterSecureStorage(); // secure storage
  // google map setup ( dibuat global vars)
  final Set<Marker> markers = {}; // markers
  Completer<GoogleMapController> mapController = Completer(); // map controller
  double mapZoom =
      14.4746; // default value zoomnya ( kalo bisa dibuat dinamis mantap jiwa )
  double rencanaPendanaanPerc = 0.0;
  ErrorHandler _scError = ErrorHandler(); // catch show case error
  bool _showcaseView = false; // nandain showcase view
  bool _showCaseLoaded = false; // showcase view udah pernah diload?
  bool _isLiked = false; // buat nandain status like di navbar
  bool _isComment = false; // nandain icon komen di navbar
  bool _isSendingComment = false; // nandai lagi ngirim commet di navbar
  bool _commented = true;
  bool _isMengajukanBisnis = false; // action jika klik ajukan bisnis ini
  bool _hasMengajukanInvestPlan = false;
  int _rencanaInvestasiValue = 10; // nilai default rencana investasi
  String _reportType = "";
  EmitenState _emitenState = EmitenState.loading;
  CommentState _commentState = CommentState.loading;
  // milih komentar yang mau dibales ( nyimpen indexnya, next bisa simpen dataya aja )
  int _indexComment;
  int _indexReply;
  PralistingEmiten _emiten = PralistingEmiten();

  CommentModel _comments = CommentModel();

  // value getter
  bool get showcaseView => _showcaseView;
  ErrorHandler get scError => _scError;
  bool get showCaseLoaded => _showCaseLoaded;
  bool get isLiked => _isLiked;
  bool get isComment => _isComment;
  bool get isSendingComment => _isSendingComment;
  bool get commented => _commented;
  bool get isMengajukanBisnis => _isMengajukanBisnis;
  CommentModel get comments => _comments;
  int get rencanaInvestasiValue => _rencanaInvestasiValue;
  int get indexComment => _indexComment;
  int get indexReply => _indexReply;
  String get reportType => _reportType;
  EmitenState get emitenState => _emitenState;
  PralistingEmiten get emiten => _emiten;
  CommentState get commentState => _commentState;
  bool get hasMengajukanInvestPlan => _hasMengajukanInvestPlan;

  int digitParser(String value) {
    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    double _doubleValue = double.parse(_onlyDigits);
    return _doubleValue.toInt();
  }

  // value setter
  set hasMengajukanInvestPlan(bool value) {
    _hasMengajukanInvestPlan = value;
    notifyListeners();
  }

  set emitenState(EmitenState value) {
    _emitenState = value;
    notifyListeners();
  }

  set commentState(CommentState value) {
    _commentState = value;
    notifyListeners();
  }

  set reportType(String val) {
    // print(">> Setting value $val");
    _reportType = val;
    notifyListeners();
  }

  set isLiked(bool value) {
    _isLiked = value;
    notifyListeners();
  }

  set indexComment(int value) {
    _indexComment = value;
    notifyListeners();
  }

  set indexReply(int value) {
    _indexReply = value;
    notifyListeners();
  }

  set investValue(String value) {
    invesCtrl.text = value;
    notifyListeners();
  }

  set rencanaInvestasiValue(int value) {
    _rencanaInvestasiValue = value;
    notifyListeners();
  }

  set isMengajukanBisnis(bool value) {
    _isMengajukanBisnis = value;
    notifyListeners();
  }

  set isComment(bool value) {
    _isComment = value;
    notifyListeners();
  }

  set isSendingComment(bool value) {
    _isSendingComment = value;
    notifyListeners();
  }

  set commented(bool value) {
    _commented = value;
    notifyListeners();
  }

  void handleSliderChanged(int val) {
    _rencanaInvestasiValue = val;
    notifyListeners();
  }

  String rupiahText(String value) {
    try {
      var _newValue = RupiahToText.convertRupiah(value);
      return _newValue;
    } catch (e) {
      return "-";
    }
  }

  Future<void> fetchComments(String uuid) async {
    try {
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Success");
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        await apiService.fetchCommentsV2(uuid).then((value) {
          // print(">> Fetching comments...");
          if (value != null) {
            final res = json.decode(value.body);
            if (value.statusCode == 200) {
              var _val = CommentModel.fromJson(res);
              _comments = _val;
              _comments.comments = _val.comments.reversed.toList();
              for (var i = 0; i < _comments.comments.length; i++) {
                _comments.comments[i].commentHistories =
                    _comments.comments[i].commentHistories.length > 1
                        ? _comments.comments[i].commentHistories.sublist(
                            (_comments.comments[i].commentHistories.length - 2),
                            _comments.comments[i].commentHistories.length)
                        : _comments.comments[i].commentHistories;
              }
              if (_comments.comments.length < 1) {
                _commentState = CommentState.empty;
                notifyListeners();
              } else {
                _commentState = CommentState.done;
                notifyListeners();
              }
              _done = true;
            } else if (value.statusCode == 404) {
              _commentState = CommentState.empty;
              _done = true;
              notifyListeners();
            } else {
              _commentState = CommentState.error;
              notifyListeners();
            }
          } else {
            _commentState = CommentState.error;
          }
        });
        i++;
      }
    } catch (e) {
      // print(">> Comment State : ");
      // print(e);
      _commentState = CommentState.error;
    }
    notifyListeners();
  }

  // null handler total pendanaan
  int _pendanaan(int value) {
    return value == null ? 0 : value;
  }

  Future<void> fetchEmitenDetail(String uuid) async {
    try {
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Success");
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        await apiService.fetchPralistingDetail(uuid).then((value) {
          // print(">> Fetch Emiten Detail..");
          if (value != null) {
            if (value.statusCode == 200) {
              final res = json.decode(value.body);
              _emiten = PralistingEmiten.fromJson(res);
              _emitenState = EmitenState.done;
              rencanaPendanaanPerc = (_pendanaan(_emiten.totalInvestmentPlans) /
                  _pendanaan(_emiten.capitalNeeds));
              _done = true;
            } else if (value.statusCode == 401) {
              _emitenState = EmitenState.unauthorized;
            } else {
              _emitenState = EmitenState.error;
            }
          } else {
            _emitenState = EmitenState.error;
          }
        });
        i++;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      _emitenState = EmitenState.error;
    }
    notifyListeners();
  }

  Future<void> sendReport(BuildContext context, String reportType,
      String reportReason, String uuid) async {
    // print(">> Reporting..");
    PopupHelper.showLoading(context);
    try {
      var _desc = reportType + ";" + reportReason;
      await apiService.reportComment(uuid, _desc).then((value) {
        final res = json.decode(value.body);
        if (value.statusCode == 200) {
          Navigator.pop(context);
          ToastHelper.showSuccessToast(context, res["message"]);
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, res["message"]);
        }
      });
    } catch (e) {
      // print(e);
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat melaporkan ulasan ini!");
    }
  }

  Future<void> cancelInvestment(BuildContext context, String uuid) async {
    // print(">> Reporting..");
    PopupHelper.showLoading(context);
    try {
      await apiService.cancelInvestmentPlan(uuid).then((value) {
        final res = json.decode(value.body);
        // print(res);
        if (value.statusCode == 200) {
          Navigator.pop(context);
          ToastHelper.showSuccessToast(context, res["message"]);
          hasMengajukanInvestPlan = true;
          emitenState = EmitenState.loading;
          invesCtrl.clear();
          fetchEmitenDetail(uuid);
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, res["message"]);
        }
      });
    } catch (e) {
      // print(e);
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat membatalkan rencana pengajuan!");
    }
  }

  Future<void> replyComment(
      BuildContext context, int index, String uuid, String comment) async {
    // print(">> Replying..");
    if (index != null) {
      // reply ulasan
      try {
        await apiService.sendReply(uuid, comment).then((value) {
          final res = json.decode(value.body);
          if (value.statusCode == 200) {
            ToastHelper.showSuccessToast(context, res['message']);
          } else {
            ToastHelper.showFailureToast(context, res['message']);
          }
        });
      } catch (e) {
        ToastHelper.showFailureToast(context, "Tidak dapat mengirim komentar!");
      }
      _comments = CommentModel();
      _commentState = CommentState.loading;
      notifyListeners();
    } else {
      // buat ulasan
      try {
        await apiService.sendComment(uuid, comment).then((value) {
          final res = json.decode(value.body);
          if (value.statusCode == 200) {
            ToastHelper.showSuccessToast(context, res['message']);
          } else {
            ToastHelper.showFailureToast(context, res['message']);
          }
        });
      } catch (e) {
        ToastHelper.showFailureToast(context, "Tidak dapat mengirim komentar!");
      }
      _comments = CommentModel();
      _commentState = CommentState.loading;
      // fetchComments(emiten.uuid);
      notifyListeners();
    }
  }

  Future<void> handleDeleteComment(
      int type, int indexComment, int indexReply, String uuid) async {
    // type 0 = comment
    // type 1 = reply
    // print(">> Deleting...");
    if (type == 0) {
      _comments.comments.removeAt(indexComment);
    } else if (type == 1) {
      _comments.comments[indexComment].commentHistories.removeAt(indexReply);
    } else {
      // print(">> no aaction available!");
    }
    await apiService.deleteComment(uuid).then((value) {
      if (value.statusCode == 200) {
        // print("OK");
      } else {
        // print("NOT OK");
      }
    });
    notifyListeners();
  }

  Future<void> likeEmiten(String uuid) async {
    // print(">> Liking...");
    try {
      await apiService.sendLike(uuid).then((value) {
        // final res = json.decode(value.body);
      });
    } catch (e) {
      // print(e);
    }
  }

  Future<void> voteEmiten(String uuid) async {
    // print(">> Voting..");
    try {
      // emiten.isVote = emiten.isVote == 1 ? 0 : 1;
      if (emiten.isVote == 1) {
        emiten.isVote = 0;
        emiten.totalVotes -= 1;
      } else {
        emiten.isVote = 1;
        emiten.totalVotes += 1;
      }
      notifyListeners();
      await apiService.sendVote(uuid).then((value) {
        final res = json.decode(value.body);
        // print(res);
      });
    } catch (e) {
      // print(e);
    }
  }

  Future<void> investmentPlan(
      BuildContext context, String uuid, int amount) async {
    // print(">> Amount : $amount");
    try {
      // PopupHelper.showLoading(context);
      // Future.delayed(Duration(milliseconds: 500), () {
      //   hasMengajukanInvestPlan = false; // kalau udah pernah ngajuin jadi false
      //   Navigator.pop(context);
      // });
      PopupHelper.showLoading(context);
      await apiService.sendInvestment(uuid, amount).then((value) {
        if (value != null) {
          final res = json.decode(value.body);
          if (value.statusCode == 200) {
            ToastHelper.showSuccessToast(context, res["message"]);
            hasMengajukanInvestPlan = false;
            emitenState = EmitenState.loading;
            fetchEmitenDetail(uuid);
          } else if (value.statusCode == 404) {
            ToastHelper.showFailureToast(
                context, "Akun anda belum diverifikasi");
          } else {
            ToastHelper.showFailureToast(context, res["message"]);
          }
        }
      });
      Navigator.pop(context);
    } catch (e) {
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Tidak dapat mengirim rencana investasi!");
      // print(e);
    }
  }

  // get status tampilkan showcase
  Future<void> getShowCaseStatus() async {
    // print(">> Getting showcase status");
    try {
      String value =
          await _storage.read(key: Constants.detailEmitenShowcaseAdm);
      if (value != null) {
        _showcaseView = false;
        _showCaseLoaded = true;
        // print(">> Value is not null");
      } else {
        _showcaseView = true;
        await _storage.write(
            key: Constants.detailEmitenShowcaseAdm, value: 'show');
        _showCaseLoaded = true;
        _showcaseView = true;
        notifyListeners();
        // print(">> Value is null, start to set value");
      }
    } catch (e) {
      // print(">> An error has occured");
      _showCaseLoaded = true;
      _showcaseView = true;
      _scError = ErrorHandler(
          status: 0,
          errorMessage: e.toString(),
          userError: 'Terjadi kesalahan saat menampilkan showcase!');
    }
    notifyListeners();
  }
}
