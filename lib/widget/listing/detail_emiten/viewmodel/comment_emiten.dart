import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/services/listing/listing_service.dart';

enum CommentState { loading, sending, loaded, none, error }

class CommentEmitenProvider with ChangeNotifier {
  // Constructor
  final CommentModel commentEmiten;
  CommentEmitenProvider({this.commentEmiten});

  // Definisikan Variable
  bool _isLiked = false; // buat nandain status like di navbar
  bool _isComment = false; // nandain icon komen di navbar
  bool _isSendingComment = false; // nandai lagi ngirim commet di navbar
  bool _commented = true;
  String _reportType = ""; // jenis report ulasan
  int _indexComment; // index comment terpilih
  int _indexReply; // index reply terpilih
  int _commentsLength = 0;
  CommentState _commentState =
      CommentState.loading; // default state nggak ngapa2in
  CommentModel _comments = CommentModel();
  final ListingApiService apiService = ListingApiService();
  // Getter Variable
  bool get isLiked => _isLiked;
  bool get isComment => _isComment;
  bool get isSendingComment => _isSendingComment;
  bool get commented => _commented;
  CommentModel get comments => _comments;
  int get commentsLength => _commentsLength;
  int get indexComment => _indexComment;
  int get indexReply => _indexReply;
  String get reportType => _reportType;
  CommentState get commentState => _commentState;

  // Setter Variable

  // value setter
  set reportType(String val) {
    // print(">> Setting value $val");
    _reportType = val;
    notifyListeners();
  }

  set isLiked(bool value) {
    _isLiked = value;
    notifyListeners();
  }

  set indexComment(int value) {
    _indexComment = value;
    notifyListeners();
  }

  set indexReply(int value) {
    _indexReply = value;
    notifyListeners();
  }

  set isComment(bool value) {
    _isComment = value;
    notifyListeners();
  }

  set isSendingComment(bool value) {
    _isSendingComment = value;
    notifyListeners();
  }

  set commented(bool value) {
    _commented = value;
    notifyListeners();
  }

  set commentState(CommentState value) {
    _commentState = value;
    notifyListeners();
  }

  set comments(CommentModel value) {
    _comments = value;
    notifyListeners();
  }

  set commentsLength(int value) {
    _commentsLength = value;
    notifyListeners();
  }

  Future<void> sendReport(BuildContext context, String reportType,
      String reportReason, String uuid) async {
    PopupHelper.showLoading(context);
    try {
      var _desc = reportType + ";" + reportReason;
      await apiService.reportComment(uuid, _desc).then((value) {
        final res = json.decode(value.body);
        if (value.statusCode == 200) {
          Navigator.pop(context);
          ToastHelper.showSuccessToast(context, res["message"]);
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, res["message"]);
        }
      });
    } catch (e) {
      // print(e);
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat melaporkan ulasan ini!");
    }
  }

  Future<void> replyComment(
      BuildContext context, int index, String uuid, String comment) async {
    if (index != null) {
      // reply ulasan
      try {
        await apiService.sendReply(uuid, comment).then((value) {
          final res = json.decode(value.body);
          if (value.statusCode == 200) {
            ToastHelper.showSuccessToast(context, res['message']);
          } else {
            ToastHelper.showFailureToast(context, res['message']);
          }
        });
      } catch (e) {
        ToastHelper.showFailureToast(context, "Tidak dapat mengirim komentar!");
      }
      _commentState = CommentState.loading;
      notifyListeners();
    } else {
      // buat ulasan
      try {
        await apiService.sendComment(uuid, comment).then((value) {
          final res = json.decode(value.body);
          if (value.statusCode == 200) {
            ToastHelper.showSuccessToast(context, res['message']);
          } else {
            ToastHelper.showFailureToast(context, res['message']);
          }
        });
      } catch (e) {
        ToastHelper.showFailureToast(context, "Tidak dapat mengirim komentar!");
      }
      _commentState = CommentState.loading;
      notifyListeners();
    }
    fetchComments(commentEmiten.uuid);
    notifyListeners();
  }

  Future<void> fetchComments(String uuid) async {
    try {
      var i = 1;
      bool _done = false;
      while (i <= 5) {
        if (_done) {
          // print(">> Success");
          break;
        }
        if (i > 5) {
          _done = true;
          break;
        }
        await apiService.fetchComments(uuid).then((value) {
          // print(">> Fetching Comments..");
          if (value != null) {
            // print(">> OKAY");
            _comments = value;
            _commentsLength = _commentsLength > 0
                ? _commentsLength
                : value.comments.length < 4 ? value.comments.length : 4;
            _comments.comments = value.comments.reversed.toList();
            _commentState = CommentState.loaded;
            _done = true;
            notifyListeners();
          } else {
            _commentState = CommentState.error;
          }
        });
        i++;
      }
    } catch (e) {
      // print(e);
      _commentState = CommentState.error;
    }
    notifyListeners();
  }

  Future<void> handleDeleteComment(
      int type, int indexComment, int indexReply, String uuid) async {
    // type 0 = comment
    // type 1 = reply

    if (type == 0) {
      _comments.comments.removeAt(indexComment);
    } else if (type == 1) {
      _comments.comments[indexComment].commentHistories.removeAt(indexReply);
    } else {
      // print(">> no aaction available!");
    }
    await apiService.deleteComment(uuid).then((value) {
      if (value.statusCode == 200) {
        // print("OK");
      } else {
        // print("NOT OK");
        // print(value.body.toString());
      }
    });
    notifyListeners();
  }
}
