import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubePlayerUI extends StatefulWidget {
  final String url;
  YoutubePlayerUI({@required this.url});
  @override
  _YoutubePlayerUIState createState() => _YoutubePlayerUIState(url);
}

class _YoutubePlayerUIState extends State<YoutubePlayerUI> {
  String url;
  _YoutubePlayerUIState(this.url);
  YoutubePlayerController _controller;
  bool _isPlayerReady = false;

  void initializeYoutube() {
    WidgetsFlutterBinding.ensureInitialized();
    _controller = YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId("$url"),
      flags: YoutubePlayerFlags(mute: false, autoPlay: true),
    );
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.blueAccent,
      ),
    );
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    initializeYoutube();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Center(
        child: Container(
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: YoutubePlayer(
                  controller: _controller,
                  showVideoProgressIndicator: true,
                  bufferIndicator: CupertinoActivityIndicator(),
                  progressIndicatorColor: Theme.of(context).primaryColor,
                  onReady: () {
                    setState(() {
                      _isPlayerReady = true;
                    });
                  },
                ),
              ),
              _isPlayerReady
                  ? Container()
                  : Align(
                      child: CircularProgressIndicator(),
                      alignment: Alignment.center,
                    )
            ],
          ),
        ),
      ),
    );
  }
}
