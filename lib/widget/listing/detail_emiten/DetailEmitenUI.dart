import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/RupiahFormatter.dart';
import '../../../helpers/SizeConfig.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../helpers/YoutubeThumbnailGenerator.dart';
import '../../../models/listing/ListingDummies.dart';
import '../../../utils/api.dart';
import '../../widget/WebView.dart';
import '../../widget/components/listing/CommentSection.dart';
import '../../widget/components/listing/HeroPhotoViewer.dart';
import '../../widget/components/listing/ListingDialog.dart';
import '../../widget/components/listing/RepliesSection.dart';
import '../../widget/components/main/SantaraAlertDialog.dart';
import '../../widget/components/main/SantaraAppBar.dart';
import '../../widget/components/main/SantaraAppBetaTesting.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../../widget/components/main/SantaraCachedImage.dart';
import '../../widget/components/main/TextIconView.dart';
import '../dashboard/PengaturanBisnisUI.dart';
import 'CommentEmitenUI.dart';
import 'YoutubePlayerUI.dart';
import 'viewmodel/detail_emiten.dart';

enum PopAction { share, edit }

class DetailEmitenUI extends StatefulWidget {
  final int status; // status emiten
  final int position;
  final String uuid;

  // keterangan status :
  // 1 = Nampilin button more vertical ( pop menu ) yang isinya Share & Edit
  // 2 = Nampilin share button / sudah verified
  // 3 = Nampilin button edit / belom verified
  // 4 = Nampilin button edit / belom verified

  DetailEmitenUI(
      {Key key, @required this.status, this.position, @required this.uuid})
      : super(key: key);
  @override
  _DetailEmitenUIState createState() =>
      _DetailEmitenUIState(status, uuid, position);
}

class _DetailEmitenUIState extends State<DetailEmitenUI>
    with SingleTickerProviderStateMixin {
  int status;
  int position;
  String uuid;
  _DetailEmitenUIState(this.status, this.uuid, this.position);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => DetailEmitenProvider()),
      ],
      child: Scaffold(
        body: ShowCaseWidget(
          builder: Builder(
              builder: (context) => EmitenBodyUI(
                    position: position,
                    status: status,
                    uuid: uuid,
                  )),
        ),
      ),
    );
  }
}

class EmitenBodyUI extends StatefulWidget {
  final int status;
  final int position;
  final String uuid;
  EmitenBodyUI(
      {Key key, @required this.status, this.position, @required this.uuid})
      : super(key: key);

  @override
  _EmitenBodyUIState createState() =>
      _EmitenBodyUIState(status, uuid, position);
}

class _EmitenBodyUIState extends State<EmitenBodyUI>
    with SingleTickerProviderStateMixin {
  // set variable yang terkait dengan ui ( global key, controller, dll )
  // var _emitenID = '7336f14a-ab32-46cc-961d-76f3db54373c';
  GlobalKey _one = GlobalKey();
  int position;
  int status;
  String uuid;
  _EmitenBodyUIState(this.status, this.uuid, this.position);
  TabController _tabController;
  TextEditingController _commentController = TextEditingController();
  var _commentFocusNode = new FocusNode();
  var _currentIndex = 0;
  // temporary vide card
  var _datas = [];
  // data

  // method yang digunakan untuk ngecek apakah pralisting sudah masuk panggung utama, atau masih dalam tahap review
  // sehingga nantinya app akan ngehide ulasan atau menu2 yg mengharuskan pralisting sudah masuk panggung utama
  bool showMainFeatures() {
    return status < 3 ? true : false;
  }

  // handle reload data ketika user back from pengaturan page ui
  // juga ketika user buka pertama kali page detailemitenui
  void handleBackFromPengaturan() {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    if (provider.emitenState == EmitenState.loading) {
      provider.fetchEmitenDetail(uuid).then((value) {
        setState(() {
          provider.hasMengajukanInvestPlan =
              provider.emiten.investmentPlan != null ? false : true;
          if (provider.emiten.pictures != null) {
            _datas = provider.emiten.pictures.split(","); // img
          }
          if (provider.emiten.videoUrl == null ||
              provider.emiten.videoUrl.isEmpty) {
            // _datas.insert(0, provider.emiten.videoUrl);
            // print(">> No push");
          } else {
            _datas.insert(0, provider.emiten.videoUrl);
          }
        });
      });
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => ShowCaseWidget.of(context).startShowCase([_one]));
    _tabController =
        new TabController(vsync: this, length: 5, initialIndex: _currentIndex);
    Future.delayed(Duration(milliseconds: 100), () {
      final provider =
          Provider.of<DetailEmitenProvider>(context, listen: false);
      provider.getShowCaseStatus();
      handleBackFromPengaturan();

      if (provider.commentState == CommentState.loading) {
        provider.fetchComments(uuid).then((value) {
          setState(() {});
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  // ketika use click (i) ulasan
  void _handleCommentPeng() {
    var _size = SizeConfig.safeBlockHorizontal;
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return ListingAlertDialog(
              title: "Pedoman Komunitas",
              subtitle:
                  "Semua ulasan yang Anda berikan akan langsung terlihat oleh publik. Pengguna dapat melaporkan spam dan komentar yang tidak pantas kepada pihak Santara. Komentar yang melanggar tata krama/etika atau mengandung SARA dapat dihapus secara sepihak oleh Santara.",
              buttonTitle: "Mengerti",
              showCloseButton: false,
              height: _size * 68,
              onClose: () => Navigator.pop(context),
              onTap: () => Navigator.pop(context));
        });
  }

  // widget dialog ketika user igin menghaspu ulasan
  void _deleteComment(BuildContext context, int type, int indexComment,
      int indexReply, String uuid) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        height: SizeConfig.screenHeight / 4,
        title: "Hapus Ulasan ?",
        description: "Apakah Anda yakin ingin menghapus ulasan ini?",
        onYakin: () {
          provider
              .handleDeleteComment(type, indexComment, indexReply, uuid)
              .then((value) {
            provider.fetchComments(provider.emiten.uuid).then((value) {
              provider.commentState = CommentState.done;
              setState(() {});
            });
          });
          Navigator.pop(context);
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void handleSendReport(String type, String desc, String uuid) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    provider.sendReport(context, type, desc, uuid);
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  // handle ketika user mau report ulasan
  void _handleReport(BuildContext context, int type, int indexComment,
      int indexReply, String uuid) {
    // final provider = Provider.of<DetailEmitenProvider>(context, listen:false);
    SizeConfig().init(context);
    String _radioValue = "";
    var _size = SizeConfig.safeBlockHorizontal;
    TextEditingController reasonCtrl = TextEditingController();
    // set up the AlertDialog
    var alert = StatefulBuilder(builder: (context, setState) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(_size * 2.5))),
        contentPadding: EdgeInsets.all(0),
        content: Container(
          padding: EdgeInsets.all(_size * 3.75),
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: _size * 10,
                              width: _size * 10,
                              child: Icon(
                                Icons.close,
                                size: _size * 5,
                              ),
                            ),
                          )),
                      Container(
                        height: _size * 5,
                      ),
                      Text(
                        "Bantu Santara Menjaga Komunitas yang Sehat",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: _size * 4),
                      ),
                      Container(
                        height: _size * 2,
                      ),
                      Text(
                        "Mengapa Anda tidak ingin melihat kiriman ini ?",
                        style: TextStyle(fontSize: _size * 3.5),
                      ),
                      Container(height: _size),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini mengganggu atau tidak menarik",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini mengganggu atau tidak menarik",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value:
                                  "Menurut saya, ini tidak seharusnya ada di Santara",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Menurut saya, ini tidak seharusnya ada di Santara",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini adalah spam",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini adalah spam",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: _size * 3,
                      ),
                      Text(
                        "Deskripsi",
                        style: TextStyle(fontSize: _size * 3),
                      ),
                      Container(
                        height: _size,
                      ),
                      TextFormField(
                        controller: reasonCtrl,
                        maxLines: 3,
                        maxLength: 100,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: _size * 2, vertical: _size * 2),
                          hintText:
                              "Alasan mengapa anda ingin melaporkan ulasan ini..",
                          hintStyle: TextStyle(fontSize: _size * 3),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1, color: Color(0xffDDDDDD)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                        ),
                      ),
                      Container(
                        height: _size * 3,
                      ),
                      Container(
                        height: _size * 9,
                        child: SantaraMainButton(
                          title: "Kirim",
                          isActive: _radioValue.isNotEmpty,
                          onPressed: () {
                            if (reasonCtrl.text.isEmpty ||
                                reasonCtrl.text == null) {
                              ToastHelper.showFailureToast(
                                  context, "Deskripsi tidak boleh kosong");
                            } else {
                              Navigator.pop(context);
                              handleSendReport(
                                  _radioValue, reasonCtrl.text, uuid);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // Gambar di caroussel
  Widget imageCard(BuildContext context, EmitenAssets assets) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HeroPhotoViewWrapper(
              tag: "${assets.id}",
              imageProvider: NetworkImage("${assets.url}"),
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Hero(
          tag: "${assets.id}",
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: SantaraCachedImage(
              width: SizeConfig.screenWidth * .85,
              // placeholder: 'assets/Preload.jpeg',
              image: "${assets.url}",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  // Gambar di carousel ( BARU )
  Widget _newImageCard(BuildContext context, String picture) {
    var _newPicture = apiLocalImage + "/uploads/emiten_picture/$picture";
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HeroPhotoViewWrapper(
              tag: "$picture",
              imageProvider: NetworkImage("$_newPicture"),
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Hero(
          tag: "$picture",
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: SantaraCachedImage(
              width: SizeConfig.screenWidth * .85,
              // placeholder: 'assets/Preload.jpeg',
              image: "$_newPicture",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  // video di caroussel
  Widget _companyVideoCard(BuildContext context, EmitenAssets assets) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(right: _size * 2.5),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => YoutubePlayerUI(
                        url: "${assets.url}",
                      )));
        },
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: Container(
                width: SizeConfig.screenWidth * .85,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        // "https://image.shutterstock.com/image-photo/closeup-young-asian-beautiful-woman-260nw-1111221518.jpg"
                        image: NetworkImage(
                          YoutubeThumbnailGenerator.getThumbnail(
                              assets.url, ThumbnailQuality.medium),
                        ),
                        fit: BoxFit.cover)),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[850].withOpacity(0.7),
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    height: _size * 12.5,
                    width: _size * 12.5,
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: _size * 10,
                    ),
                  ),
                ))),
      ),
    );
  }

  Widget _newCompanyVideoCard(BuildContext context, String url) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      margin: EdgeInsets.only(right: _size * 2.5),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => YoutubePlayerUI(
                        url: "$url",
                      )));
        },
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: Container(
                width: SizeConfig.screenWidth * .85,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        // "https://image.shutterstock.com/image-photo/closeup-young-asian-beautiful-woman-260nw-1111221518.jpg"
                        image: NetworkImage(
                          YoutubeThumbnailGenerator.getThumbnail(
                              url, ThumbnailQuality.medium),
                        ),
                        fit: BoxFit.cover)),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[850].withOpacity(0.7),
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    height: _size * 12.5,
                    width: _size * 12.5,
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: _size * 10,
                    ),
                  ),
                ))),
      ),
    );
  }

  // bagian paling atas detail emiten ( video card, caroussel)
  Widget topSection(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    var _height = SizeConfig.screenHeight;
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: SantaraAppBetaTesting(),
          ),
          provider.emiten.pictures == null || _datas == null
              ? Container()
              : Container(
                  height: _height / 4,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: _datas != null && _datas.length != null
                        ? _datas.length
                        : 0,
                    itemBuilder: (context, index) {
                      String image = _datas[index];
                      return index == 0
                          ? provider.emiten.videoUrl == null ||
                                  provider.emiten.videoUrl.isEmpty
                              ? _newImageCard(context, image)
                              : _newCompanyVideoCard(context, image)
                          : _newImageCard(context, image);
                    },
                  )),
          Container(
            height: 20,
          ),
          position != null
              ? Text(
                  "#$position BANYAK DIAJUKAN",
                  style: TextStyle(
                      color: Color(0xffBF2D30),
                      fontWeight: FontWeight.w600,
                      fontSize: SizeConfig.safeBlockHorizontal * 3),
                )
              : Container(),
          Container(
            height: SizeConfig.safeBlockHorizontal * 2,
          ),
          Text(
            // "${emitenDetail.category}",
            "${provider.emiten.category}",
            style: TextStyle(
                color: Color(0xff292F8D),
                fontSize: SizeConfig.safeBlockHorizontal * 3),
          ),
          Text(
            "${provider.emiten.trademark}",
            style: TextStyle(
                color: Colors.black,
                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                fontWeight: FontWeight.bold),
          ),
          Text(
            "${provider.emiten.companyName}",
            style: TextStyle(
                color: Colors.black,
                fontSize: SizeConfig.safeBlockHorizontal * 3.5),
          ),
          Container(
            height: 15,
          ),
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextIconView(
                    iconData: Icons.person,
                    text: "${provider.emiten.totalVotes} Orang Mengajukan"),
                Container(
                  width: 1,
                  height: 20,
                  color: Colors.grey,
                ),
                TextIconView(
                    iconData: Icons.favorite_border,
                    text: "${provider.emiten.totalLikes} Orang Menyukai"),
              ],
            ),
          ),
          Container(
            height: 15,
          ),
          LinearPercentIndicator(
            padding: EdgeInsets.only(right: 20),
            alignment: MainAxisAlignment.center,
            animation: true,
            animationDuration: 2000,
            width: SizeConfig.screenWidth - _size * 10,
            lineHeight: 22,
            percent: showMainFeatures()
                ? provider.rencanaPendanaanPerc > 1
                    ? 1
                    : num.parse(
                        provider.rencanaPendanaanPerc.toStringAsFixed(1))
                : 0.0,
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: Color(0xFF0E7E4A),
            center: Text(
              showMainFeatures()
                  ? '${(provider.rencanaPendanaanPerc * 100) > 100 ? 100 : (provider.rencanaPendanaanPerc * 100).toStringAsFixed(2)} %'
                  : '0%',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          Container(
            height: 10,
          ),
          Text(
              showMainFeatures()
                  ? RupiahFormatter.initialValueFormat(
                          "${provider.emiten.totalInvestmentPlans > provider.emiten.capitalNeeds ? provider.emiten.capitalNeeds : provider.emiten.totalInvestmentPlans}") +
                      " Rencana pendanaan dari " +
                      RupiahFormatter.initialValueFormat(
                          "${provider.emiten.capitalNeeds}")
                  : "Rp0 terkumpul dari " +
                      RupiahFormatter.initialValueFormat(
                          "${provider.emiten.capitalNeeds}"),
              style: TextStyle(
                  fontWeight: FontWeight.w600, fontSize: _size * 3.5)),
        ],
      ),
    );
  }

  // tabs emiten
  Widget emitenTabs(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(left: 20),
      child: TabBar(
          controller: _tabController,
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          isScrollable: true,
          tabs: [
            Container(
                height: 40, child: Center(child: Text("Deskripsi Singkat"))),
            Container(
                height: 40,
                child: Center(child: Text("Identitas Calon Penerbit"))),
            Container(
                height: 40, child: Center(child: Text("Informasi Finansial"))),
            Container(
                height: 40,
                child: Center(child: Text("Informasi Non Finansial"))),
            Container(height: 40, child: Center(child: Text("Lokasi"))),
          ]),
    );
  }

  // tab content deskripsi emiten
  Widget deskripsiEmiten(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              color: Colors.white,
              child: Text("${provider.emiten.businessDescription}",
                  textAlign: TextAlign.justify)),
          Container(
            height: 5,
          ),
          provider.emiten.prospektus == null
              ? Container()
              : Container(
                  padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
                  color: Colors.white,
                  child: _prospektusSection(context)),
          Container(
            height: 5,
          ),
          showMainFeatures() ? _ajukanBisnisSection(context) : Container(),
          Container(
            height: SizeConfig.safeBlockHorizontal * 2,
          ),
          _commentSection(context)
          // comment section
        ],
      ),
    );
  }

  // section ajukan bisnis
  Widget _ajukanBisnisSection(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    var _size = SizeConfig.safeBlockHorizontal;
    if (provider.emiten.isVote == 1) {
      // jika pernah ngevote
      provider.isMengajukanBisnis = true;
    }
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Apakah anda ingin mengajukan penerbit ini listing di Santara ?",
            style:
                TextStyle(fontWeight: FontWeight.bold, fontSize: _size * 3.5),
            textAlign: TextAlign.center,
          ),
          Container(
            height: SizeConfig.safeBlockHorizontal * 5,
          ),
          // Set batasan ke button ajukan bisnis ini, batasan :
          // 1. Bisnis sedang dalam tahap proses
          // 2. Bisnis ditolak atau tidak lolos screening
          provider.emiten.isVote == 1
              ? Container()
              : SantaraMainButton(
                  title: "Ajukan Bisnis Ini",
                  onPressed: () {
                    setState(() {});
                    provider.isMengajukanBisnis = true;
                    provider.voteEmiten(uuid);
                  },
                ),
          provider.isMengajukanBisnis
              ? Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SantaraOutlineButton(
                      title: "Batalkan Pengajuan",
                      onPressed: () async {
                        provider.isMengajukanBisnis = false;
                        setState(() {});
                        await provider.voteEmiten(uuid).then((value) async {
                          if (!provider.hasMengajukanInvestPlan) {
                            await provider.cancelInvestment(context, uuid);
                            // hacky,
                            // krn notifyListener ga ngupdate viewnya
                            setState(() {
                              provider.emitenState = EmitenState.done;
                            });
                          }
                        });
                      },
                    ),
                    Container(
                      height: SizeConfig.safeBlockHorizontal * 10,
                    ),
                    Text(
                      "Masukan rencana investasi jika bisnis ini listing di Santara",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: _size * 3.5),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      height: SizeConfig.safeBlockHorizontal * 10,
                    ),
                    Container(
                      width: double.infinity,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Rencana Investasi",
                            style: TextStyle(
                                fontSize: _size * 3.5,
                                fontWeight: FontWeight.w600),
                          ),
                          Container(
                            width: _size * 1.25,
                          ),
                          Container(
                              width: _size * 40,
                              // height: _size * 10,
                              child: TextFormField(
                                enabled: provider.hasMengajukanInvestPlan,
                                controller: provider.invesCtrl,
                                maxLines: 1,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: _size * 2, vertical: _size),
                                  hintText: provider.emiten.investmentPlan !=
                                          null
                                      ? RupiahFormatter.initialValueFormat(
                                          "${provider.emiten.investmentPlan}")
                                      : "Rp 100,000",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: Color(0xffDDDDDD)),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                ),
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  RupiahFormatter(maxDigits: 14),
                                ],
                                onSaved: (value) {
                                  String _onlyDigits =
                                      value.replaceAll(RegExp('[^0-9]'), "");
                                  double _doubleValue =
                                      double.parse(_onlyDigits);
                                  provider.investValue =
                                      _doubleValue.toString();
                                },
                              )),
                        ],
                      ),
                    ),
                    Container(
                      height: _size * 5,
                    ),
                    // komen dulu boys

                    // Slider(
                    //   value: provider.rencanaInvestasiValue.toDouble(),
                    //   onChanged: (val) {
                    //     setState(() {
                    //       provider.handleSliderChanged(val.toInt());
                    //       final formatter =
                    //           new NumberFormat("###,###,###", "id_ID");
                    //       String newText =
                    //           "Rp " + formatter.format(val * 10000);
                    //       provider.invesCtrl.text = newText;
                    //     });
                    //   },
                    //   min: 0,
                    //   divisions: 10,
                    //   max: 100,
                    // ),
                    // Text(
                    //   "*Geser slidebar untuk memasukan rencana investasi Anda",
                    //   style: TextStyle(color: Color(0xffB8B8B8)),
                    // ),
                    // Container(
                    //   height: _size * 5,
                    // ),
                    // provider.hasMengajukanInvestPlan
                    //     ?
                    SantaraMainButton(
                      isActive: provider.hasMengajukanInvestPlan,
                      title: "Ajukan Rencana Investasi",
                      onPressed: () async {
                        _hideKeyboard();
                        var _amount = provider.digitParser(
                            provider.invesCtrl.text.isEmpty
                                ? "100000"
                                : provider.invesCtrl.text);
                        if (_amount < 1) {
                          ToastHelper.showFailureToast(
                              context, "Mohon masukan nilai yang valid!");
                        } else if (_amount > provider.emiten.capitalNeeds) {
                          ToastHelper.showFailureToast(
                              context, "Nilai lebih besar dari kebutuhan!");
                        } else {
                          await provider.investmentPlan(context, uuid, _amount);
                          setState(() {
                            provider.emitenState = EmitenState.done;
                          });
                        }
                      },
                    )
                    // : SantaraMainButton(
                    //     title: "Batalkan Rencana Investasi",
                    //     onPressed: () async {
                    //       await provider.cancelInvestment(context, uuid);
                    //     },
                    //   )
                  ],
                )
              : Container()
        ],
      ),
    );
  }

  // comment section ( view ulasan )
  Widget _commentSection(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    var _paddingSize = _size * 4;

    return provider.commentState == CommentState.loading
        ? Center(
            child: CupertinoActivityIndicator(),
          )
        : provider.commentState == CommentState.error
            ? Center(
                child: Container(
                  height: SizeConfig.screenHeight / 8,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.replay),
                          onPressed: () {
                            provider.commentState = CommentState.loading;
                            provider
                                .fetchComments(provider.emiten.uuid)
                                .then((value) {
                              provider.commentState = CommentState.done;
                              setState(() {});
                            });
                          }),
                      Text("Terjadi Kesalahan"),
                    ],
                  ),
                ),
              )
            : provider.commentState == CommentState.empty
                ? _noUlasan(context)
                : provider.comments.comments != null &&
                        provider.comments.comments.length != null
                    ? provider.comments.comments.length > 0
                        ? Container(
                            color: Colors.white,
                            // height: _size * 200,
                            padding: EdgeInsets.fromLTRB(
                                _paddingSize, _paddingSize, 0, _paddingSize),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: _paddingSize,
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                          left: _size * 1.25,
                                          bottom: _size * 2.5),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text(
                                              "${provider.comments.totalComments ?? ''} Ulasan",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: _size * 4.5)),
                                          InkWell(
                                            onTap: () {
                                              _handleCommentPeng();
                                            },
                                            child: Container(
                                              margin:
                                                  EdgeInsets.only(left: _size),
                                              height: _size * 6,
                                              width: _size * 6,
                                              child: Icon(
                                                Icons.info,
                                                size: _size * 5,
                                                color: Color(0xffDADADA),
                                              ),
                                            ),
                                          )
                                        ],
                                      )),
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: provider.comments.comments !=
                                                  null &&
                                              provider.comments.comments
                                                      .length !=
                                                  null
                                          ? provider.comments.comments.length <
                                                  4
                                              ? provider
                                                  .comments.comments.length
                                              : 4
                                          : 0,
                                      itemBuilder: (context, index) {
                                        return UserComment(
                                          showAction: showMainFeatures(),
                                          maxRadius: _size * 6.25,
                                          onDelete: () {
                                            _deleteComment(
                                                context,
                                                0,
                                                index,
                                                index,
                                                provider.comments
                                                    .comments[index].uuid);
                                          },
                                          onReport: () {
                                            _handleReport(
                                                context,
                                                1,
                                                index,
                                                0,
                                                provider.comments
                                                    .comments[index].uuid);
                                          },
                                          data:
                                              provider.comments.comments[index],
                                          onReply: () {
                                            // print('>> selected index : $index');
                                            provider.indexComment = index;
                                            _commentFocusNode.requestFocus();
                                          },
                                          replies: provider
                                                          .comments
                                                          .comments[index]
                                                          .commentHistories !=
                                                      null &&
                                                  provider
                                                          .comments
                                                          .comments[index]
                                                          .commentHistories
                                                          .length >
                                                      0
                                              ? ListView.builder(
                                                  itemBuilder:
                                                      (context, indexReply) {
                                                    // Ambil 2 komentar terakhir
                                                    var _histories = provider
                                                        .comments
                                                        .comments[index]
                                                        .commentHistories;
                                                    // var _histories = _rawhistories
                                                    //             .length >
                                                    //         1
                                                    //     ? _rawhistories.sublist(
                                                    //         (_rawhistories
                                                    //                 .length -
                                                    //             2),
                                                    //         _rawhistories
                                                    //             .length)
                                                    //     : _rawhistories;

                                                    return RepliesSection(
                                                      showAction:
                                                          showMainFeatures(),
                                                      maxRadius: _size * 3.75,
                                                      data: _histories[
                                                          indexReply],
                                                      onReply: () {
                                                        provider.indexReply =
                                                            indexReply;
                                                        provider.indexComment =
                                                            index;
                                                        _commentFocusNode
                                                            .requestFocus();
                                                      },
                                                      onReport: () {
                                                        _handleReport(
                                                            context,
                                                            1,
                                                            index,
                                                            indexReply,
                                                            _histories[
                                                                    indexReply]
                                                                .uuid);
                                                      },
                                                      onDelete: () {
                                                        _deleteComment(
                                                            context,
                                                            1,
                                                            index,
                                                            indexReply,
                                                            _histories[
                                                                    indexReply]
                                                                .uuid);
                                                      },
                                                    );
                                                  },
                                                  itemCount: provider
                                                              .comments
                                                              .comments[index]
                                                              .commentHistories
                                                              .length >
                                                          2
                                                      ? 2
                                                      : provider
                                                          .comments
                                                          .comments[index]
                                                          .commentHistories
                                                          .length,
                                                  physics:
                                                      NeverScrollableScrollPhysics(),
                                                  shrinkWrap: true,
                                                )
                                              : Container(),
                                        );
                                      }),
                                  Container(
                                    height: _paddingSize,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    margin:
                                        EdgeInsets.only(right: _paddingSize),
                                    child: OutlineButton(
                                      borderSide: BorderSide.none,
                                      onPressed: () async {
                                        // _showAllComments(context);
                                        final result = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    CommentEmitenUI(
                                                      emitenComment:
                                                          provider.comments,
                                                      showAction:
                                                          showMainFeatures(),
                                                    )));
                                        if (result != null) {
                                          provider
                                              .fetchComments(uuid)
                                              .then((value) {
                                            provider.commentState =
                                                CommentState.done;
                                            setState(() {});
                                          });
                                        }
                                      },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "Lihat Selengkapnya",
                                            style: TextStyle(
                                                color: Color(0xff218196),
                                                fontSize: _size * 3.5),
                                          ),
                                          Container(
                                            width: _size * 1.25,
                                          ),
                                          Icon(
                                            Icons.expand_more,
                                            color: Color(0xff218196),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: _paddingSize * 5,
                                  ),
                                ]))
                        : _noUlasan(context)
                    : _noUlasan(context);
  }

  Widget _noUlasan(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    var _paddingSize = _size * 4;

    return Container(
        color: Colors.white,
        height: SizeConfig.screenHeight / 2,
        padding:
            EdgeInsets.fromLTRB(_paddingSize, _paddingSize, 0, _paddingSize),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: _paddingSize,
              ),
              Container(
                margin: EdgeInsets.only(left: 5, bottom: 10),
                child: Text("0 Ulasan",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockHorizontal * 4.5)),
              ),
              Center(
                child: Text(
                  "Belum ada ulasan untuk bisnis ini",
                  style: TextStyle(color: Color(0xffB8B8B8)),
                ),
              ),
            ]));
  }

  // tab informasi finansial
  Widget financeEmiten(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    return Container(
      // padding: EdgeInsets.only(left: 20, right: 20),
      // color: Colors.white,
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.white,
            child: ListView(
                padding: EdgeInsets.all(0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  ListTile(
                    title: Text("Besar Kebutuhan Dana",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(
                        provider.rupiahText("${provider.emiten.capitalNeeds}")),
                  ),
                  ListTile(
                    title: Text("Rata-rata omset perbulan saat ini",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider
                        .rupiahText("${provider.emiten.monthlyTurnover}")),
                  ),
                  ListTile(
                    title: Text("Rata-rata laba per bulan saat ini",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider
                        .rupiahText("${provider.emiten.monthlyProfit}")),
                  ),
                  ListTile(
                    title: Text("Rata-rata omzet per bulan tahun sebelumnya",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider.rupiahText(
                        "${provider.emiten.monthlyTurnoverPreviousYear}")),
                  ),
                  ListTile(
                    title: Text("Rata-rata laba per bulan tahun sebelumnya",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider.rupiahText(
                        "${provider.emiten.monthlyProfitPreviousYear}")),
                  ),
                  ListTile(
                    title: Text("Total Hutang Bank/Lembaga Pembiayaan",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider
                        .rupiahText("${provider.emiten.totalBankDebt}")),
                  ),
                  ListTile(
                    title: Text("Nama Bank/Lembaga Pembiayaan",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider.emiten.bankNameFinancing ?? "-"),
                  ),
                  ListTile(
                    title: Text("Total Modal disetor",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle: Text(provider
                        .rupiahText("${provider.emiten.totalPaidCapital}")),
                  ),
                  ListTile(
                    title: Text("Nilai Per-lembar saham",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                    subtitle:
                        Text(provider.rupiahText("${provider.emiten.price}")),
                  ),
                  // Container(
                  //   height: SizeConfig.screenHeight / 6,
                  // )
                  // widgets
                  _listingOptionWidgets(context),
                  ListTile()
                ]),
          ),
        ],
      ),
    );
  }

  // tab lokasi
  Widget lokasiEmiten(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      // color: Colors.white,
      // padding: EdgeInsets.all(_size * 5),
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: _size * 5, right: _size * 5),
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: _size * 5,
                ),
                Text(
                  "Alamat Lengkap",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: _size * 4),
                ),
                Container(
                  height: _size * 2,
                ),
                InkWell(
                  onTap: () async {
                    String url = '';
                    if (Platform.isAndroid) {
                      url =
                          "https://maps.google.com/maps?q=${provider.emiten.address}";
                    } else if (Platform.isIOS) {
                      url =
                          "https://maps.apple.com/?q=${provider.emiten.address}";
                    } else {
                      url = '';
                    }
                    if (await canLaunch(url)) {
                      await launch(url);
                    }
                  },
                  child: Text(
                    "${provider.emiten.address}",
                    style: TextStyle(
                        color: Color(0xff58A7FF),
                        fontSize: _size * 3.5,
                        decoration: TextDecoration.underline),
                  ),
                ),
                Container(
                  height: _size * 5,
                )
              ],
            ),
          ),
          // Container(
          //   height: _size * 10,
          // ),
          Container(
            padding: EdgeInsets.only(left: _size * 5, right: _size * 5),
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Kota Lokasi Usaha",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: _size * 4),
                ),
                Container(
                  height: _size * 2,
                ),
                Text(
                  "${provider.emiten.regency}",
                  style: TextStyle(
                      color: Color(0xff58A7FF),
                      fontSize: _size * 3.5,
                      decoration: TextDecoration.underline),
                ),
                Container(
                  height: _size * 8,
                )
              ],
            ),
          ),
          _listingOptionWidgets(context),
          ListTile()

          // SizedBox(
          //   height: _size * 20,
          // )
        ],
      ),
    );
  }

  // identitas penerbit ( sosial media )
  Widget _socmedIcon(String image, String title, String url) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    WebViewWidget(url: "$url", appBarName: "$title")));
      },
      child: Container(
        padding: EdgeInsets.only(right: 10),
        child: Image.asset(
          "$image",
          width: SizeConfig.safeBlockHorizontal * 8,
          height: SizeConfig.safeBlockHorizontal * 8,
        ),
      ),
    );
  }

  Widget _listingOptionWidgets(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    return Column(
      children: <Widget>[
        Container(
          height: 5,
        ),
        provider.emiten.prospektus == null
            ? Container()
            : Container(
                padding: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
                color: Colors.white,
                child: _prospektusSection(context)),
        Container(
          height: 5,
        ),
        showMainFeatures() ? _ajukanBisnisSection(context) : Container(),
        Container(
          height: SizeConfig.safeBlockHorizontal * 2,
        )
      ],
    );
  }

  // informasi non finansial
  Widget potensiPasarEmiten(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    return Container(
      // padding: EdgeInsets.only(left: 20, right: 20),
      // color: Colors.white,
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.white,
            child: ListView(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                ListTile(
                  title: Text("Sistem Pencatatan Keuangan",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.financialRecordingSystem}"),
                ),
                ListTile(
                  title: Text("Reputasi Pinjaman Bank/Lainnya",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.bankLoanReputation}"),
                ),
                ListTile(
                  title: Text("Posisi pasar atas Produk dan/Jasa",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle:
                      Text("${provider.emiten.marketPositionForTheProduct}"),
                ),
                ListTile(
                  title: Text("Stategi kedepan",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.strategyEmiten}"),
                ),
                ListTile(
                  title: Text("Status Lokasi/Kantor/Tempat Usaha",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.officeStatus}"),
                ),
                ListTile(
                  title: Text("Tingkat Persaingan",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle:
                      Text("${provider.emiten.levelOfBusinessCompetition}"),
                ),
                ListTile(
                  title: Text("Kemampuan Manajerial",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.managerialAbility}"),
                ),
                ListTile(
                  title: Text("Kemampuan Teknis",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.technicalAbility}"),
                ),
                ListTile()
              ],
            ),
          ),
          _listingOptionWidgets(context),
          ListTile()
        ],
      ),
    );
  }

  // identitas calon penerbit
  Widget detailEmiten(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    return Container(
      // padding: EdgeInsets.only(left: 20, right: 20),
      // color: Colors.white,
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.white,
            child: ListView(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                // ${emitenDetail.category}
                ListTile(
                  title: Text("Nama Perusahaan",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.companyName}"),
                ),
                ListTile(
                  title: Text("Bentuk Legalitas Usaha",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.businessEntity}"),
                ),
                ListTile(
                  title: Text("Lama Usaha",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.businessLifespan} Bulan"),
                ),
                ListTile(
                  title: Text("Jumlah Cabang",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.branchCompany} Cabang"),
                ),
                ListTile(
                  title: Text("Jumlah Karyawan",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  subtitle: Text("${provider.emiten.employee} Karyawan"),
                ),
                // ListTile(
                //   title: Text("Media Sosial",
                //       style: TextStyle(
                //           color: Colors.black, fontWeight: FontWeight.bold)),
                //   subtitle: Row(
                //       mainAxisSize: MainAxisSize.max,
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       children: emitenDetail.socialMedia.map((val) {
                //         return _socmedIcon(
                //             "${val.logo}", "Instagram", "${val.url}");
                //       }).toList()),
                // )
              ],
            ),
          ),
          // widgets
          _listingOptionWidgets(context),
          ListTile()
          // Container(
          //   height: SizeConfig.safeBlockHorizontal * 8,
          // )
        ],
      ),
    );
  }

  // handle button edit ( Untuk edit emiten )
  void openSetting() async {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    provider.emitenState = EmitenState.loading;
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                PengaturanBisnisUI(emitenDetail: provider.emiten)));
    if (result != null) {
      // print(">> Amazing");
      handleBackFromPengaturan();
    }
  }

  // jika user pertama kali buka halaman ini, maka munculin showcase
  Widget _showCaseAppBarAction(int status) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    var _size = SizeConfig.safeBlockHorizontal;
    switch (status) {
      case 0:
        return Showcase.withWidget(
          key: _one,
          container: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Edit Bisnis\nyang Diajukan",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                  Container(height: 10),
                  Text(
                    "Anda dapat mengedit \ninformasi pada bisnis yang\nsedang diajukan",
                    maxLines: 4,
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
          ),
          height: 100,
          width: 200,
          child: InkWell(
            onTap: () {
              openSetting();
            },
            child: Container(
              width: _size * 15,
              height: _size * 10,
              child: Center(
                child: Text(
                  "Edit",
                  style: TextStyle(color: Color(0xff218196)),
                ),
              ),
            ),
          ),
        );
        break;
      case 1:
        return Container();
        // JANGAN DIHAPUS
        // return Showcase.withWidget(
        //   key: _one,
        //   container: ClipRRect(
        //     borderRadius: BorderRadius.all(Radius.circular(10)),
        //     child: Container(
        //       color: Colors.white,
        //       padding: EdgeInsets.all(10),
        //       child: Column(
        //         crossAxisAlignment: CrossAxisAlignment.start,
        //         children: <Widget>[
        //           Text("Edit & Bagikan Bisnis\nyang Diajukan",
        //               style:
        //                   TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        //           Container(height: 10),
        //           Text(
        //             "Anda dapat mengedit dan\nmembagikan ke sosial media\ninformasi pada bisnis yang\nsedang diajukan",
        //             maxLines: 4,
        //             style: TextStyle(fontSize: 12),
        //           ),
        //         ],
        //       ),
        //     ),
        //   ),
        //   height: 100,
        //   width: 200,
        //   child: PopupMenuButton<PopAction>(
        //     onSelected: (PopAction result) {
        //       if (result == PopAction.edit) {
        //         Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //                 builder: (context) =>
        //                     PengaturanBisnisUI(emitenDetail: provider.emiten)));
        //       } else if (result == PopAction.share) {
        //         // Share.share('check out my website https://example.com');
        //       } else {
        //         // print("Unkwnon acitno");
        //       }
        //     },
        //     itemBuilder: (BuildContext context) => <PopupMenuEntry<PopAction>>[
        //       PopupMenuItem<PopAction>(
        //           value: PopAction.share, child: Container()
        //           // TextIconView(
        //           //   iconData: Icons.share,
        //           //   margin: 10,
        //           //   iconSize: 4.5,
        //           //   text: "Bagikan",
        //           //   textSize: 4,
        //           // ),
        //           ),
        //       PopupMenuItem<PopAction>(
        //         value: PopAction.edit,
        //         child: TextIconView(
        //           iconData: Icons.edit,
        //           margin: 10,
        //           iconSize: 4.5,
        //           text: "Edit",
        //           textSize: 4,
        //         ),
        //       ),
        //     ],
        //   ),
        // );
        break;
      case 2:
        return Container();
        // return Showcase.withWidget(
        //     key: _one,
        //     container: ClipRRect(
        //       borderRadius: BorderRadius.all(Radius.circular(10)),
        //       child: Container(
        //         color: Colors.white,
        //         padding: EdgeInsets.all(10),
        //         child: Column(
        //           crossAxisAlignment: CrossAxisAlignment.start,
        //           children: <Widget>[
        //             Text("Bagikan",
        //                 style: TextStyle(
        //                     fontWeight: FontWeight.bold, fontSize: 14)),
        //             Container(height: 10),
        //             Text(
        //               "Anda dapat membagikan ke\nsosial media informasi pada\nbisnis yang sedang diajukan",
        //               maxLines: 3,
        //               style: TextStyle(fontSize: 12),
        //             ),
        //           ],
        //         ),
        //       ),
        //     ),
        //     height: 80,
        //     width: 200,
        //     child: Container()
        //     // IconButton(
        //     //     icon: Icon(Icons.share),
        //     //     onPressed: () {
        //     //       Share.share('check out my website https://example.com');
        //     //     }),
        //     );
        break;
      case 3:
        return Showcase.withWidget(
          key: _one,
          container: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Edit Bisnis\nyang Diajukan",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                  Container(height: 10),
                  Text(
                    "Anda dapat mengedit \ninformasi pada bisnis yang\nsedang diajukan",
                    maxLines: 4,
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
          ),
          height: 100,
          width: 200,
          child: InkWell(
            onTap: () {
              openSetting();
            },
            child: Container(
              width: _size * 15,
              height: _size * 10,
              child: Center(
                child: Text(
                  "Edit",
                  style: TextStyle(color: Color(0xff218196)),
                ),
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  // jika user sudah pernah membuka halaman ini, maka tidak perlu munculin showcase dan pakai appbar yang ini
  Widget _appBarAction(int status) {
    var _size = SizeConfig.safeBlockHorizontal;
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    switch (status) {
      case 0:
        return InkWell(
          onTap: () async {
            openSetting();
          },
          child: Container(
            width: _size * 15,
            height: _size * 10,
            child: Center(
              child: Text(
                "Edit",
                style: TextStyle(color: Color(0xff218196)),
              ),
            ),
          ),
        );
        break;
      case 1:
        return PopupMenuButton<PopAction>(
          onSelected: (PopAction result) {
            if (result == PopAction.edit) {
              openSetting();
            } else if (result == PopAction.share) {
              // Share.share('check out my website https://example.com');
            } else {
              // print("Unkwnon acitno");
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuEntry<PopAction>>[
            PopupMenuItem<PopAction>(value: PopAction.share, child: Container()
                // TextIconView(
                //   iconData: Icons.share,
                //   margin: 10,
                //   iconSize: 4.5,
                //   text: "Bagikan",
                //   textSize: 4,
                // ),
                ),
            PopupMenuItem<PopAction>(
              value: PopAction.edit,
              child: TextIconView(
                iconData: Icons.edit,
                margin: 10,
                iconSize: 4.5,
                text: "Edit",
                textSize: 4,
              ),
            ),
          ],
        );
        break;
      case 2:
        return Container();
        // IconButton(
        //     icon: Icon(Icons.share),
        //     onPressed: () {
        //       Share.share('check out my website https://example.com');
        //     });
        break;

      // 3 & 4 sama, 4 ketika user habis registrasi emiten, 3 user dari bisnisdiajukan
      case 3:
        return InkWell(
          onTap: () {
            openSetting();
          },
          child: Container(
            width: _size * 15,
            height: _size * 10,
            child: Center(
              child: Text(
                "Edit",
                style: TextStyle(color: Color(0xff218196)),
              ),
            ),
          ),
        );
        break;
      case 4:
        return InkWell(
          onTap: () {
            openSetting();
          },
          child: Container(
            width: _size * 15,
            height: _size * 10,
            child: Center(
              child: Text(
                "Edit",
                style: TextStyle(color: Color(0xff218196)),
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }

  // handler ketika user klik back button hardware atau yg di appbar
  _handleBackButton(BuildContext context) {
    // jika status emitennya 4 ( datang dari pernyataaninfoui atau setelah user melakukan registrasi,
    // maka ketika user klik button back langsung balik ke homepage), jika statusnya bukan 4 maka pop halaman biasa
    if (status == 4) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/index', (Route<dynamic> route) => false);
    } else {
      Navigator.pop(context, 'refresh');
    }
    return;
  }

  // app bar builder
  Widget _appBarBuilder(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);

    if (!provider.showCaseLoaded) {
      provider.getShowCaseStatus();
    }

    return PreferredSize(
        child: SantaraAppBar(
          leading: IconButton(
              icon: Icon(
                  Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios),
              onPressed: () {
                _handleBackButton(context);
              }),
          actions: <Widget>[
            provider.showcaseView
                ? _showCaseAppBarAction(status)
                : _appBarAction(status)
          ],
        ),
        preferredSize: Size.fromHeight(60));
  }

  // prospektus
  Widget _prospektusIcon() {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      width: _size * 4,
      height: _size * 4,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Color(0xff218196),
        ),
        shape: BoxShape.circle,
      ),
      child: Icon(
        Icons.arrow_downward,
        color: Color(0xff218196),
        size: _size * 3,
      ),
    );
  }

  Widget _prospektusItem(BuildContext context, String title) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    var _prospektusUrl =
        apiLocalImage + "/uploads/prospektus/" + provider.emiten.prospektus;
    var _size = SizeConfig.safeBlockHorizontal;
    return InkWell(
      onTap: () async {
        await _downloadProspektus(_prospektusUrl);
      },
      child: Container(
        // padding: EdgeInsets.all(_size * 2.5),
        // height: _size * 12.5,
        // width: SizeConfig.screenWidth * .4,
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Color(0xffDADADA)),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text("$title", style: TextStyle(fontSize: _size * 3.75)),
            Container(
              width: _size * 2,
            ),
            _prospektusIcon()
          ],
        ),
      ),
    );
  }

  _downloadProspektus(String url) async {
    // var newUrl = "https://docs.google.com/viewer?url=$url&embedded=true&a=bi&pagenumber=12";
    if (await canLaunch(url)) {
      try {
        await launch(url);
      } catch (e) {
        // print(">> Err : $e");
      }
    }
  }

  Widget _prospektusSection(BuildContext context) {
    // action download prospektus ( PDF )
    return Container(
      width: double.infinity,
      height: SizeConfig.safeBlockHorizontal * 10,
      child: _prospektusItem(context, "Prospektus"),
    );
  }

  // Sending Comment Method
  _sendComment(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    setState(() async {
      // Set loading ke button kirim
      provider.isSendingComment = true;
      // Proses kirim ulasan ke api
      await provider
          .replyComment(
              context,
              provider.indexComment,
              provider.indexComment == null
                  ? uuid
                  : provider.comments.comments[provider.indexComment].uuid,
              _commentController.text)
          .then((value) async {
        provider.isSendingComment =
            false; // jika berhasil, loading di button jadi button send lagi
        provider.isComment =
            false; // state sedang menulis komentar jadi false ( button send di hide )
        provider.indexComment = null; // komentar terpilih set ke null
        provider.indexReply = null; // reply terpilih set ke null
        _hideKeyboard(); // nyembunyiin keyboard
        _commentController.clear(); // clean textfield
        // fetch komentar terbaru
        await provider.fetchComments(provider.emiten.uuid).then((value) {
          provider.commentState =
              CommentState.done; // komentar berhasil di fetch
          setState(() {
            _tabController.index = 0;
          });
        });
      });
    });
  }

  // bottom navigation bar
  Widget _bottomNavbar(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider = Provider.of<DetailEmitenProvider>(context, listen: true);

    return provider.commentState == CommentState.loading
        ? Container()
        : Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Column(
              children: <Widget>[
                provider.indexComment == null
                    ? Container()
                    : Container(
                        padding: EdgeInsets.only(
                            left: _size * 3.75,
                            top: _size * 1.5,
                            right: _size * 3.75),
                        height: _size * 8,
                        decoration:
                            BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(146, 146, 146, 0.25),
                              offset: Offset(2, 0),
                              blurRadius: 5,
                              spreadRadius: 5)
                        ]),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            provider.indexReply == null
                                ? Text(
                                    "Membalas ke " +
                                        provider
                                            .comments
                                            .comments[provider.indexComment]
                                            .name,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontSize: _size * 3),
                                  )
                                : Text(
                                    "Membalas ke " +
                                        provider
                                            .comments
                                            .comments[provider.indexComment]
                                            .commentHistories[
                                                provider.indexReply]
                                            .name,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontSize: _size * 3),
                                  ),
                            Spacer(),
                            InkWell(
                              onTap: () {
                                provider.indexReply = null;
                                provider.indexComment = null;
                              },
                              child: Container(
                                height: _size * 7,
                                width: _size * 7,
                                child: Center(
                                  child: Icon(
                                    Icons.close,
                                    size: _size * 5,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: provider.indexComment != null
                          ? []
                          : [
                              BoxShadow(
                                  color: Color.fromRGBO(146, 146, 146, 0.25),
                                  offset: Offset(2, 0),
                                  blurRadius: 5,
                                  spreadRadius: 5)
                            ]),
                  height: _size * 17.5,
                  padding: EdgeInsets.all(_size * 3.75),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          width: provider.isComment
                              ? SizeConfig.screenWidth * .75
                              : SizeConfig.screenWidth * .8,
                          // height: SizeConfig.safeBlockHorizontal * 15,
                          child: TextField(
                            focusNode: _commentFocusNode,
                            controller: _commentController,
                            onChanged: (val) {
                              if (val.length > 0) {
                                if (!provider.isComment) {
                                  provider.isComment = true;
                                }
                              } else {
                                if (provider.isComment) {
                                  provider.isComment = false;
                                }
                              }
                              setState(() {});
                            },
                            textInputAction: TextInputAction.newline,
                            maxLines: 2,
                            maxLength: 500,
                            decoration: InputDecoration(
                                hintText: "Tulis Ulasan..",
                                counterText: '',
                                hintStyle: TextStyle(height: 2.0),
                                contentPadding: EdgeInsets.only(
                                    left: SizeConfig.safeBlockHorizontal * 5,
                                    right: 5),
                                filled: true,
                                fillColor: Color(0xffF4F4F4),
                                border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(50.0),
                                    ),
                                    borderSide: BorderSide(
                                        width: 1, color: Color(0xffB8B8B8)))),
                          )),
                      provider.isComment
                          ? provider.isSendingComment
                              ? CupertinoActivityIndicator()
                              : Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        width: 1, color: Color(0xffBF2D30)),
                                  ),
                                  child: Container(
                                    alignment: Alignment.center,
                                    // margin: EdgeInsets.only(bottom: 1),
                                    child: IconButton(
                                        icon: Icon(Icons.send,
                                            size: _size * 5,
                                            color: Color(0xffBF2D30)),
                                        onPressed: () {
                                          _sendComment(context);
                                        }),
                                  ))
                          : Container(),
                      provider.isComment
                          ? Container()
                          : InkWell(
                              onTap: () async {
                                if (provider.emiten.isLikes == 1) {
                                  provider.emiten.isLikes = 0;
                                  provider.emiten.totalLikes -= 1;
                                } else {
                                  provider.emiten.isLikes = 1;
                                  provider.emiten.totalLikes += 1;
                                }
                                setState(() {});
                                await provider.likeEmiten(uuid);
                              },
                              child: provider.emiten.isLikes == 1
                                  ? Icon(
                                      Icons.favorite,
                                      size: _size * 5.5,
                                      color: Color(0XFFBF2D30),
                                    )
                                  : Icon(
                                      Icons.favorite_border,
                                      size: _size * 5.5,
                                      color: Color(0xffdadada),
                                    ),
                            ),
                      // provider.isComment
                      //     ? Container()
                      //     : InkWell(
                      //         onTap: () {
                      //           // _showAllComments(context);
                      //         },
                      //         child: Container(
                      //           child: Stack(
                      //             children: <Widget>[
                      //               Icon(
                      //                 Icons.chat_bubble_outline,
                      //                 size: _size * 5.5,
                      //                 color: Color(0xffdadada),
                      //               ),
                      //               Positioned(
                      //                   right: 0,
                      //                   child: Container(
                      //                     padding: EdgeInsets.all(1),
                      //                     decoration: BoxDecoration(
                      //                       color: Colors.white,
                      //                       borderRadius:
                      //                           BorderRadius.circular(6),
                      //                     ),
                      //                     constraints: BoxConstraints(
                      //                       minWidth: _size * 3,
                      //                       minHeight: _size * 3,
                      //                     ),
                      //                     child: provider
                      //                                 .comments.totalComments !=
                      //                             null
                      //                         ? provider.comments
                      //                                         .totalComments >
                      //                                     0 &&
                      //                                 provider.comments
                      //                                         .totalComments <
                      //                                     100
                      //                             ? Text(
                      //                                 "${provider.comments.totalComments}",
                      //                                 style: TextStyle(
                      //                                   color:
                      //                                       Color(0xffDADADA),
                      //                                   fontSize: _size * 2,
                      //                                 ),
                      //                                 textAlign:
                      //                                     TextAlign.center,
                      //                               )
                      //                             : provider.comments
                      //                                         .totalComments >
                      //                                     99
                      //                                 ? Text(
                      //                                     "99+",
                      //                                     style: TextStyle(
                      //                                       color: Color(
                      //                                           0xffDADADA),
                      //                                       fontSize: _size * 2,
                      //                                     ),
                      //                                     textAlign:
                      //                                         TextAlign.center,
                      //                                   )
                      //                                 : Container()
                      //                         : Container(),
                      //                   ))
                      //             ],
                      //           ),
                      //         ),
                      //       )
                    ],
                  ),
                ),
              ],
            ),
          );
  }

  Widget contentBuilder(BuildContext context) {
    SizeConfig().init(context);
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/icon/search_not_found.png"),
          Container(height: 10),
          Text(
            "Terjadi kesalahan",
            style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          Container(height: 10),
          Text(
            "Tidak dapat memuat data pralisting",
            style: TextStyle(
              color: Colors.white,
              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.all(SizeConfig.safeBlockHorizontal * 5),
            child: SantaraMainButton(
              title: "Muat Ulang",
              onPressed: () {
                setState(() {
                  provider.emitenState = EmitenState.loading;
                  provider.fetchEmitenDetail(uuid);
                });
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DetailEmitenProvider>(context, listen: false);
    return WillPopScope(
      onWillPop: () {
        _handleBackButton(context);
      },
      child: Scaffold(
          appBar: _appBarBuilder(context),
          body: provider.emitenState == EmitenState.loading
              ? Center(
                  child: CupertinoActivityIndicator(),
                )
              : provider.emitenState == EmitenState.error
                  ? contentBuilder(context)
                  : Stack(
                      children: <Widget>[
                        NestedScrollView(
                          headerSliverBuilder: (context, value) {
                            return [
                              SliverToBoxAdapter(
                                child: topSection(context),
                              ),
                              SliverToBoxAdapter(
                                child: emitenTabs(context),
                              )
                            ];
                          },
                          body: Container(
                            child: TabBarView(
                              controller: _tabController,
                              children: [
                                deskripsiEmiten(context),
                                detailEmiten(context),
                                financeEmiten(context),
                                potensiPasarEmiten(context),
                                lokasiEmiten(context),
                              ],
                            ),
                          ),
                        ),
                        showMainFeatures()
                            ? _bottomNavbar(context)
                            : Container()
                        // navbar builder,
                      ],
                    )),
    );
  }
}

class VideoDataModel {
  final String video;
  final String thumbnail;
  bool isPlayed;
  VideoDataModel({this.video, this.thumbnail, this.isPlayed});
}
