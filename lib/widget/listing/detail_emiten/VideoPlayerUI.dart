// import 'dart:async';
// import 'dart:ui';
// import 'package:toast/toast.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:santaraapp/helpers/SizeConfig.dart';
// import 'package:video_player/video_player.dart';

// class VideoPlayerUI extends StatefulWidget {
//   final VideoPlayerController controller;
//   VideoPlayerUI({@required this.controller});

//   @override
//   _VideoPlayerUIState createState() => _VideoPlayerUIState(controller);
// }

// class _VideoPlayerUIState extends State<VideoPlayerUI> {
//   VideoPlayerController controller;
//   _VideoPlayerUIState(this.controller);

//   bool _isVideoHasEnded = false;
//   bool _visible = true;
//   bool _isNeedToHide = true;

//   @override
//   void initState() {
//     super.initState();
//     controller.addListener(_isVideoEnded);
//     _autoHideButton();
//   }

//   // tiap x detik ngehide button biar ga ganggu user
//   void _autoHideButton() {
//     Timer.periodic(Duration(seconds: 3), (Timer t) {
//       if (_isNeedToHide) {
//         if (_visible) {
//           setState(() {
//             _visible = false;
//             _isNeedToHide = false;
//           });
//         }
//       }
//     });
//   }

//   // listener buat ngecek videonya masih jalan apa tidak
//   void _isVideoEnded() {
//     if (controller.value.position == controller.value.duration) {
//       setState(() {
//         _isVideoHasEnded = true;
//       });
//     }
//   }

//   Widget _blurButton(BuildContext context, Widget button) {
//     return ClipRRect(
//         borderRadius: BorderRadius.all(Radius.circular(5)),
//         child: Container(
//           width: 60.0,
//           height: 40.0,
//           decoration: BoxDecoration(color: Colors.grey[850].withOpacity(0.6)),
//           child: Center(
//             child: button,
//           ),
//         ));
//   }

//   @override
//   void dispose() {
//     SystemChrome.setEnabledSystemUIOverlays(
//         [SystemUiOverlay.top, SystemUiOverlay.bottom]);
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     SystemChrome.setEnabledSystemUIOverlays([]);
//     SizeConfig().init(context);
//     return Scaffold(
//         // resizeToAvoidBottomPadding: false,
//         body: Stack(
//       children: <Widget>[
//         InkWell(
//           onTap: () {
//             setState(() {
//               if (_visible) {
//                 _visible = false;
//                 _isNeedToHide = false;
//               } else {
//                 _visible = true;
//               }
//             });
//           },
//           child: Container(
//             height: SizeConfig.screenHeight,
//             width: SizeConfig.screenWidth,
//             color: Colors.black,
//             child: Center(
//                 child: SizedBox.expand(
//               child: FittedBox(
//                 fit: BoxFit.contain,
//                 child: SizedBox(
//                   width: controller.value.size?.width ?? 0,
//                   height: controller.value.size?.height ?? 0,
//                   child: VideoPlayer(controller),
//                 ),
//               ),
//             )),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Align(
//             alignment: Alignment.topLeft,
//             child: AnimatedOpacity(
//                 opacity: _visible ? 1.0 : 0.0,
//                 duration: Duration(milliseconds: 200),
//                 child: _blurButton(
//                     context,
//                     IconButton(
//                         icon: Icon(
//                           Icons.fullscreen_exit,
//                           color: Colors.white,
//                         ),
//                         onPressed: () {
//                           SystemChrome.setEnabledSystemUIOverlays(
//                               [SystemUiOverlay.top, SystemUiOverlay.bottom]);
//                           Navigator.pop(context);
//                         }))),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Align(
//             alignment: Alignment.bottomLeft,
//             child: AnimatedOpacity(
//                 opacity: _visible ? 1.0 : 0.0,
//                 duration: Duration(milliseconds: 200),
//                 child: _blurButton(
//                     context,
//                     _isVideoHasEnded
//                         ? IconButton(
//                             icon: Icon(
//                               Icons.replay,
//                               color: Colors.white,
//                             ),
//                             onPressed: () {
//                               setState(() {
//                                 controller.seekTo(Duration(seconds: 0));
//                                 controller.play();
//                                 _isVideoHasEnded = false;
//                                 Toast.show("Replayed", context,
//                                     backgroundColor:
//                                         Colors.grey[800].withOpacity(0.5),
//                                     duration: 2);
//                               });
//                             })
//                         : controller.value.isPlaying
//                             ? IconButton(
//                                 icon: Icon(Icons.pause, color: Colors.white),
//                                 onPressed: () {
//                                   setState(() {
//                                     controller.pause();
//                                     Toast.show("Paused", context,
//                                         backgroundColor:
//                                             Colors.grey[800].withOpacity(0.5),
//                                         duration: 2);
//                                   });
//                                 })
//                             : IconButton(
//                                 icon: Icon(
//                                   Icons.play_arrow,
//                                   color: Colors.white,
//                                 ),
//                                 onPressed: () {
//                                   setState(() {
//                                     controller.play();
//                                   });
//                                 }))),
//           ),
//         ),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Align(
//             alignment: Alignment.bottomRight,
//             child: AnimatedOpacity(
//                 opacity: _visible ? 1.0 : 0.0,
//                 duration: Duration(milliseconds: 200),
//                 child: _blurButton(
//                     context,
//                     controller.value.volume > 0
//                         ? IconButton(
//                             icon: Icon(Icons.volume_up, color: Colors.white),
//                             onPressed: () {
//                               setState(() {
//                                 controller.setVolume(0);
//                                 Toast.show("Muted", context,
//                                     backgroundColor:
//                                         Colors.grey[800].withOpacity(0.5),
//                                     duration: 2);
//                               });
//                             })
//                         : IconButton(
//                             icon: Icon(
//                               Icons.volume_off,
//                               color: Colors.white,
//                             ),
//                             onPressed: () {
//                               setState(() {
//                                 controller.setVolume(1.0);
//                                 Toast.show("Sound Activated", context,
//                                     backgroundColor:
//                                         Colors.grey[800].withOpacity(0.5),
//                                     duration: 2);
//                               });
//                             }))),
//           ),
//         ),
//         controller.value.isBuffering
//             ? Align(
//                 alignment: Alignment.center,
//                 child: CupertinoActivityIndicator(),
//               )
//             : Container()
//       ],
//     ));
//   }
// }
