import 'package:flutter/material.dart';
import 'package:santaraapp/models/listing/Pralisting.dart';
import 'package:santaraapp/widget/widget/components/listing/ListingGridContent.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBar.dart';
import 'package:santaraapp/widget/widget/components/main/TextIconView.dart';

class DetailEmitenUI extends StatefulWidget {
  @override
  _DetailEmitenUIState createState() => _DetailEmitenUIState();
}

class _DetailEmitenUIState extends State<DetailEmitenUI>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  TextEditingController _commentController = TextEditingController();

  var _currentIndex = 0;
  bool _isComment = false;
  bool _isSendingComment = false;
  bool _commented = true;

  @override
  void initState() {
    super.initState();
    _tabController =
        new TabController(vsync: this, length: 5, initialIndex: _currentIndex);
  }

  Widget imageCard(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: FadeInImage.assetNetwork(
          width: 250,
          placeholder: 'assets/Preload.jpeg',
          image:
              "https://prods3.imgix.net/images/articles/2018_04/facebook-youtube-cooking-channel-update.jpg",
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget topSection(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              height: _height / 4,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return imageCard(context);
                },
              )),
          Container(
            height: 20,
          ),
          Text(
            "Kuliner",
            style: TextStyle(color: Color(0xff292F8D), fontSize: 16),
          ),
          Text(
            "Sop Ayam Pak Min",
            style: TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Text(
            "PT. Ayam Warna Warni",
            style: TextStyle(color: Colors.black, fontSize: 14),
          ),
          Container(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              TextIconView(iconData: Icons.check, text: "30 Orang Mengajukan"),
              Container(
                width: 1,
                height: 20,
                color: Colors.grey,
              ),
              TextIconView(iconData: Icons.person, text: "50 Orang Menyukai"),
            ],
          )
        ],
      ),
    );
  }

  Widget emitenTabs(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      child: TabBar(
          controller: _tabController,
          indicatorColor: Color((0xFFBF2D30)),
          labelColor: Color((0xFFBF2D30)),
          unselectedLabelColor: Colors.black,
          isScrollable: true,
          tabs: [
            Container(
                height: 40, child: Center(child: Text("Deskripsi Singkat"))),
            Container(
                height: 40, child: Center(child: Text("Detail Perusahaan"))),
            Container(height: 40, child: Center(child: Text("Finansial"))),
            Container(height: 40, child: Center(child: Text("Potensi Pasar"))),
            Container(height: 40, child: Center(child: Text("Lokasi"))),
          ]),
    );
  }

  Widget deskripsiEmiten(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(20), color: Colors.white, child: Text("""
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget lectus aliquam vulputate vulputate purus urna porttitor id. Tincidunt nulla donec facilisis consectetur accumsan habitant integer. Eu, volutpat lectus ac in cursus. Venenatis nulla felis morbi sed dolor quis. Scelerisque nisl sit proin aliquam pellentesque lacus, purus. Pellentesque congue cursus eu pellentesque. Habitant congue tempus scelerisque aenean.
Enim tristique amet ultrices tincidunt. Auctor diam porta nunc, velit dolor odio a. Sed nunc pulvinar id urna leo pharetra at vulputate. A venenatis tristique congue neque massa sodales vitae. Pharetra ornare egestas eros, fringilla potenti mi, nisl. Augue malesuada tincidunt tincidunt rhoncus. Ut magna quis consectetur phasellus eget lectus odio lacinia nullam. Tortor, cras malesuada donec amet, sit lacus.
Vulputate varius vitae diam rhoncus, ac. Odio vitae fringilla dui dui libero, in risus proin enim. Sed aliquet dignissim pretium diam tristique. Lobortis in mauris sit ultricies integer arcu. Nunc ullamcorper faucibus in sed vel sollicitudin tellus, vitae sodales. Quis aliquet tincidunt mauris quis risus. Sed adipiscing accumsan tempor, feugiat natoque.
                  """, textAlign: TextAlign.justify)),
          Container(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.all(20),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Apakah anda ingin mengajukan penerbit ini listing di Santara ?",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    OutlineButton(
                        color: Color(0xffDDDDDD),
                        highlightColor: Color(0xffBF2D30),
                        highlightedBorderColor: Color(0xffDDDDDD),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0)),
                            side: BorderSide(width: 4)),
                        child: Text(
                          "Ya",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {}),
                    Container(
                      width: 30,
                    ),
                    OutlineButton(
                        color: Color(0xffDDDDDD),
                        highlightColor: Color(0xffBF2D30),
                        highlightedBorderColor: Color(0xffDDDDDD),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0)),
                            side: BorderSide(width: 4)),
                        child: Text(
                          "Tidak",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {}),
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 5,
          ),
          Container(
            color: Colors.white,
            height: 400,
            padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.only(left: 5, bottom: 10),
                  child: Text("Pra Listing Lainnya",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                ),
                Container(
                  height: 280,
                  child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 4,
                      itemBuilder: (context, index) {
                        Data dummy = Data(
                            id: index,
                            uuid: "${index}ashiap",
                            image:
                                "https://www.aljazeera.com/mritems/imagecache/mbdxxlarge/mritems/Images/2019/12/25/7c5e3b5239e64e5688b6e4464dae6c11_18.jpg",
                            thumbnail:
                                "https://www.aljazeera.com/mritems/imagecache/mbdxxlarge/mritems/Images/2019/12/25/7c5e3b5239e64e5688b6e4464dae6c11_18.jpg",
                            tagName: "Kuliner",
                            businessName: "Sop Ayam Mr.Min",
                            companyName: "PT. Kembar Cipta Boga",
                            submissions: int.parse("100"),
                            likes: int.parse("88"),
                            comments: int.parse("90"));
                        return Container(
                          width: 180,
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5)),
                                  child: FadeInImage.assetNetwork(
                                    height: _height / 4,
                                    placeholder: 'assets/Preload.jpeg',
                                    image: dummy.image,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        dummy.tagName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff292F8D)),
                                      ),
                                      Text(dummy.businessName),
                                      Text(
                                        dummy.companyName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 11),
                                      ),
                                      Container(
                                        height: 15,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        // crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Icon(
                                            Icons.favorite_border,
                                            size: 15,
                                            color: Color(0xffdadada),
                                          ),
                                          Container(
                                            width: 20,
                                          ),
                                          Icon(
                                            Icons.chat_bubble_outline,
                                            size: 15,
                                            color: Color(0xffdadada),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
          Container(
            height: 5,
          ),
          _commented
              ? Container(
                  color: Colors.white,
                  height: 500,
                  padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: 20,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5, bottom: 10),
                          child: Text("2 Komentar",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                        ),
                        Expanded(
                          child: ListView.builder(
                              itemCount: 4,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  leading: CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        "https://www.greenscene.co.id/wp-content/uploads/2020/01/Kaido-1-696x497.jpg"),
                                    radius: 30.0,
                                  ),
                                  isThreeLine: true,
                                  title: Text("Kaido",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("""
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget lectus aliquam vulputate vulputate purus urna porttitor id. Tincidunt nulla donec facilisis consectetur accumsan habitant integer.
                                    """),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text(
                                            "12 Februari 2020",
                                            style: TextStyle(fontSize: 12),
                                          ),
                                          Container(
                                            width: 5,
                                          ),
                                          Text(
                                            "●",
                                            style: TextStyle(fontSize: 12),
                                          ),
                                          Container(
                                            width: 5,
                                          ),
                                          Text(
                                            "Balas",
                                            style: TextStyle(fontSize: 12),
                                          )
                                        ],
                                      ),
                                      Container(
                                          alignment: Alignment.centerLeft,
                                          child: ListTile(
                                            leading: CircleAvatar(
                                              backgroundImage: NetworkImage(
                                                  "https://www.greenscene.co.id/wp-content/uploads/2020/01/Kaido-1-696x497.jpg"),
                                            ),
                                            title: Text("Kaido",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            subtitle: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text("""
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget lectus aliquam vulputate vulputate purus urna porttitor id. Tincidunt nulla donec facilisis consectetur accumsan habitant integer. 
                                    """),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text(
                                                      "12 Februari 2020",
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      "●",
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      "Balas",
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                    ],
                                  ),
                                );
                              }),
                        ),
                      ]))
              : Container(
                  color: Colors.white,
                  height: 400,
                  padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: 20,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5, bottom: 10),
                          child: Text("0 Komentar",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                        ),
                        Center(
                          child: Text(
                            "Belum ada komentar untuk bisnis ini",
                            style: TextStyle(color: Color(0xffB8B8B8)),
                          ),
                        ),
                      ]))
        ],
      ),
    );
  }

  Widget detailEmiten(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      color: Colors.white,
      child: ListView(
        physics: ClampingScrollPhysics(),
        // mainAxisAlignment: MainAxisAlignment.start,
        // mainAxisSize: MainAxisSize.max,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListView(
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              ListTile(
                title: Text("Kategori Bisnis"),
                subtitle: Text("Kuliner",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Pemilik Bisnis"),
                subtitle: Text("PT. Ayam Warna-Warni",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Bentuk Badan Usaha"),
                subtitle: Text("UD",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Daerah Pemasaran"),
                subtitle: Text("Yogyakarta",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Lama Usaha"),
                subtitle: Text("48 Bulan",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Jumlah Karyawan"),
                subtitle: Text("24 karyawan",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Jumlah Cabang"),
                subtitle: Text("24 karyawan",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              ),
              ListTile(
                title: Text("Tanggal Pengajuan"),
                subtitle: Text("18 november 2019",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
              )
            ],
          ),
          ListTile(
            title: Text("Unduh Dokumen Perizinan Usaha",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
            trailing: Icon(Icons.file_download),
            onTap: () {},
          ),
          ListTile(
            title: Text("Unduh Berkas Badan Usaha",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
            trailing: Icon(Icons.file_download),
            onTap: () {},
          )
        ],
      ),
    );
  }

  Widget emitenTabView(BuildContext context) {
    return TabBarView(controller: _tabController, children: <Widget>[
      deskripsiEmiten(context),
      detailEmiten(context),
      Container(
        height: 300,
        color: Colors.blueAccent,
      ),
      Container(
        height: 400,
        color: Colors.purpleAccent,
      ),
      Container(
        height: 500,
        color: Colors.yellowAccent,
      ),
    ]);
  }

  _buildTabContext(int lineCount) => Container(
        child: ListView.builder(
          physics: const ClampingScrollPhysics(),
          itemCount: lineCount,
          itemBuilder: (BuildContext context, int index) {
            return Text('some content');
          },
        ),
      );

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: PreferredSize(
            child: SantaraAppBar(), preferredSize: Size.fromHeight(60)),
        body: Stack(
          children: <Widget>[
            NestedScrollView(
              headerSliverBuilder: (context, value) {
                return [
                  SliverToBoxAdapter(
                    child: topSection(context),
                  ),
                  SliverToBoxAdapter(
                    child: emitenTabs(context),
                  )
                ];
              },
              body: Container(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    deskripsiEmiten(context),
                    detailEmiten(context),
                    _buildTabContext(2),
                    _buildTabContext(100),
                    _buildTabContext(2),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                color: Colors.white,
                height: 80,
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        // decoration: BoxDecoration(
                        //   color: Color(0xffF4F4F4),
                        //   border:
                        //       Border.all(width: 1, color: Color(0xffB8B8B8)),
                        //   borderRadius: const BorderRadius.all(
                        //     const Radius.circular(50.0),
                        //   ),
                        // ),
                        width: _isComment ? _width * .75 : _width * .7,
                        height: 60,
                        child: TextField(
                          controller: _commentController,
                          onChanged: (val) {
                            if (val.length > 0) {
                              if (!_isComment) {
                                setState(() => _isComment = true);
                              }
                            } else {
                              if (_isComment) {
                                setState(() => _isComment = false);
                              }
                            }
                          },
                          decoration: InputDecoration(
                              hintText: "Tulis Komentar..",
                              contentPadding:
                                  EdgeInsets.only(left: 20, right: 5),
                              filled: true,
                              fillColor: Color(0xffF4F4F4),
                              border: OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(50.0),
                                  ),
                                  borderSide: BorderSide(
                                      width: 1, color: Color(0xffB8B8B8)))),
                        )),
                    _isComment
                        ? _isSendingComment
                            ? CircularProgressIndicator()
                            : Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      width: 1, color: Color(0xffBF2D30)),
                                ),
                                child: Center(
                                  child: IconButton(
                                      icon: Icon(Icons.send,
                                          color: Color(0xffBF2D30)),
                                      onPressed: () async {
                                        setState(
                                            () => _isSendingComment = true);
                                        await Future.delayed(
                                            Duration(milliseconds: 1000), () {
                                          setState(() {
                                            _isSendingComment = false;
                                            _isComment = false;
                                          });
                                          _commentController.clear();
                                        });
                                      }),
                                ))
                        : Container(),
                    _isComment
                        ? Container()
                        : Icon(
                            Icons.favorite_border,
                            color: Color(0xffdadada),
                          ),
                    _isComment
                        ? Container()
                        : Icon(
                            Icons.chat_bubble_outline,
                            color: Color(0xffdadada),
                          ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
