import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../../helpers/SizeConfig.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../models/listing/CommentModel.dart';
import '../../widget/components/listing/CommentSection.dart';
import '../../widget/components/listing/RepliesSection.dart';
import '../../widget/components/main/SantaraAlertDialog.dart';
import '../../widget/components/main/SantaraButtons.dart';
import 'viewmodel/comment_emiten.dart';

class CommentEmitenUI extends StatefulWidget {
  final CommentModel emitenComment;
  final bool showAction;
  CommentEmitenUI(
      {Key key, @required this.emitenComment, this.showAction = true})
      : super(key: key);

  @override
  _CommentEmitenUIState createState() =>
      _CommentEmitenUIState(emitenComment, showAction);
}

class _CommentEmitenUIState extends State<CommentEmitenUI> {
  CommentModel emitenComment;
  bool showAction;
  _CommentEmitenUIState(this.emitenComment, this.showAction);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (_) =>
                  CommentEmitenProvider(commentEmiten: emitenComment)),
        ],
        child: CommentEmitenBody(
          showAction: showAction,
        ));
  }
}

class CommentEmitenBody extends StatefulWidget {
  final bool showAction;
  CommentEmitenBody({Key key, this.showAction = true}) : super(key: key);

  @override
  _CommentEmitenBodyState createState() => _CommentEmitenBodyState(showAction);
}

class _CommentEmitenBodyState extends State<CommentEmitenBody> {
  var _commentFocusNode = new FocusNode();
  final bool showAction;
  _CommentEmitenBodyState(this.showAction);
  TextEditingController _commentController = TextEditingController();

  // widget dialog ketika user igin menghaspu ulasan
  void _deleteComment(BuildContext context, int type, int indexComment,
      int indexReply, String uuid) {
    final provider = Provider.of<CommentEmitenProvider>(context, listen: false);
    SizeConfig().init(context);
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        title: "Hapus Ulasan ?",
        description: "Apakah Anda yakin ingin menghapus ulasan ini?",
        onYakin: () {
          provider
              .handleDeleteComment(type, indexComment, indexReply, uuid)
              .then((value) {
            provider.fetchComments(provider.commentEmiten.uuid);
          });
          Navigator.pop(context);
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void handleSendReport(String type, String desc, String uuid) {
    final provider = Provider.of<CommentEmitenProvider>(context, listen: false);
    provider.sendReport(context, type, desc, uuid);
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  // handle ketika user mau report ulasan
  void _handleReport(BuildContext context, int type, int indexComment,
      int indexReply, String uuid) {
    // final provider = Provider.of<DetailEmitenProvider>(context, listen:false);
    SizeConfig().init(context);
    String _radioValue = "";
    var _size = SizeConfig.safeBlockHorizontal;
    TextEditingController reasonCtrl = TextEditingController();
    // set up the AlertDialog
    var alert = StatefulBuilder(builder: (context, setState) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(_size * 2.5))),
        contentPadding: EdgeInsets.all(0),
        content: Container(
          padding: EdgeInsets.all(_size * 3.75),
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: _size * 10,
                              width: _size * 10,
                              child: Icon(
                                Icons.close,
                                size: _size * 5,
                              ),
                            ),
                          )),
                      Container(
                        height: _size * 5,
                      ),
                      Text(
                        "Bantu Santara Menjaga Komunitas yang Sehat",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: _size * 4),
                      ),
                      Container(
                        height: _size * 2,
                      ),
                      Text(
                        "Mengapa Anda tidak ingin melihat kiriman ini ?",
                        style: TextStyle(fontSize: _size * 3.5),
                      ),
                      Container(height: _size),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini mengganggu atau tidak menarik",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini mengganggu atau tidak menarik",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value:
                                  "Menurut saya, ini tidak seharusnya ada di Santara",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Menurut saya, ini tidak seharusnya ada di Santara",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Radio(
                              value: "Ini adalah spam",
                              groupValue: _radioValue,
                              onChanged: (val) {
                                setState(() {
                                  _radioValue = val;
                                });
                              },
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap),
                          Flexible(
                            child: Text(
                              "Ini adalah spam",
                              maxLines: 2,
                              style: TextStyle(fontSize: _size * 3.5),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: _size * 3,
                      ),
                      Text(
                        "Deskripsi",
                        style: TextStyle(fontSize: _size * 3),
                      ),
                      Container(
                        height: _size,
                      ),
                      TextFormField(
                        controller: reasonCtrl,
                        maxLines: 3,
                        maxLength: 100,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: _size * 2, vertical: _size * 2),
                          hintText:
                              "Alasan mengapa anda ingin melaporkan ulasan ini..",
                          hintStyle: TextStyle(fontSize: _size * 3),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1, color: Color(0xffDDDDDD)),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                        ),
                      ),
                      Container(
                        height: _size * 3,
                      ),
                      Container(
                        height: _size * 9,
                        child: SantaraMainButton(
                          title: "Kirim",
                          isActive: _radioValue.isNotEmpty,
                          onPressed: () {
                            if (reasonCtrl.text.isEmpty ||
                                reasonCtrl.text == null) {
                              ToastHelper.showFailureToast(
                                  context, "Deskripsi tidak boleh kosong");
                            } else {
                              Navigator.pop(context);
                              handleSendReport(
                                  _radioValue, reasonCtrl.text, uuid);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // bottom navigation bar
  Widget _bottomNavbar(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider = Provider.of<CommentEmitenProvider>(context, listen: false);
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Column(
        children: <Widget>[
          provider.indexComment == null
              ? Container()
              : Container(
                  padding: EdgeInsets.only(
                      left: _size * 3.75,
                      top: _size * 1.5,
                      right: _size * 3.75),
                  height: _size * 8,
                  decoration:
                      BoxDecoration(color: Colors.transparent, boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(146, 146, 146, 0.25),
                        offset: Offset(2, 0),
                        blurRadius: 5,
                        spreadRadius: 5)
                  ]),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      provider.indexReply == null
                          ? Text(
                              "Membalas ke " +
                                  provider.comments
                                      .comments[provider.indexComment].name,
                              style: TextStyle(
                                  color: Colors.grey[600], fontSize: _size * 3),
                            )
                          : Text(
                              "Membalas ke " +
                                  provider
                                      .comments
                                      .comments[provider.indexComment]
                                      .commentHistories[provider.indexReply]
                                      .name,
                              style: TextStyle(
                                  color: Colors.grey[600], fontSize: _size * 3),
                            ),
                      Spacer(),
                      InkWell(
                        onTap: () {
                          provider.indexReply = null;
                          provider.indexComment = null;
                        },
                        child: Container(
                          height: _size * 7,
                          width: _size * 7,
                          child: Center(
                            child: Icon(
                              Icons.close,
                              size: _size * 5,
                              color: Colors.grey[600],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: provider.indexComment != null
                    ? []
                    : [
                        BoxShadow(
                            color: Color.fromRGBO(146, 146, 146, 0.25),
                            offset: Offset(2, 0),
                            blurRadius: 5,
                            spreadRadius: 5)
                      ]),
            height: _size * 17.5,
            padding: EdgeInsets.all(_size * 3.75),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    width: provider.isComment
                        ? SizeConfig.screenWidth * .75
                        : SizeConfig.screenWidth * .9,
                    height: SizeConfig.safeBlockHorizontal * 15,
                    child: TextField(
                      focusNode: _commentFocusNode,
                      controller: _commentController,
                      onChanged: (val) {
                        if (val.length > 0) {
                          if (!provider.isComment) {
                            provider.isComment = true;
                          }
                        } else {
                          if (provider.isComment) {
                            provider.isComment = false;
                          }
                        }
                      },
                      textInputAction: TextInputAction.newline,
                      maxLines: 2,
                      maxLength: 500,
                      decoration: InputDecoration(
                          hintText: "Tulis Ulasan..",
                          counterText: '',
                          hintStyle: TextStyle(height: 2.0),
                          contentPadding: EdgeInsets.only(
                              left: SizeConfig.safeBlockHorizontal * 5,
                              right: 5),
                          filled: true,
                          fillColor: Color(0xffF4F4F4),
                          border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(50.0),
                              ),
                              borderSide: BorderSide(
                                  width: 1, color: Color(0xffB8B8B8)))),
                    )),
                provider.isComment
                    ? provider.isSendingComment
                        ? CupertinoActivityIndicator()
                        : Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: 1, color: Color(0xffBF2D30)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              child: IconButton(
                                  icon: Icon(Icons.send,
                                      size: _size * 4,
                                      color: Color(0xffBF2D30)),
                                  onPressed: () async {
                                    provider.isSendingComment = true;
                                    provider
                                        .replyComment(
                                            context,
                                            provider.indexComment,
                                            provider.indexComment == null
                                                ? provider.commentEmiten.uuid
                                                : provider
                                                    .comments
                                                    .comments[
                                                        provider.indexComment]
                                                    .uuid,
                                            _commentController.text)
                                        .then((value) {
                                      provider.isSendingComment = false;
                                      provider.isComment = false;
                                      provider.indexComment = null;
                                      provider.indexReply = null;
                                      _hideKeyboard();
                                      _commentController.clear();
                                    });
                                  }),
                            ))
                    : Container(),
                // Container()
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Widget _loadMoreComments(BuildContext context, VoidCallback onTap) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      width: double.infinity,
      child: OutlineButton(
        borderSide: BorderSide.none,
        onPressed: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Lihat Ulasan Lainnnya",
              style: TextStyle(color: Color(0xff218196), fontSize: _size * 3.5),
            ),
            Container(
              width: _size * 1.25,
            ),
            Icon(
              Icons.expand_more,
              color: Color(0xff218196),
            )
          ],
        ),
      ),
    );
  }

  Widget _titleBar(BuildContext context) {
    final provider = Provider.of<CommentEmitenProvider>(context, listen: true);
    var _text = "";
    switch (provider.commentState) {
      case CommentState.loaded:
        _text = provider.comments != null
            ? "${provider.comments.totalComments ?? ''} Ulasan"
            : "Tidak ada ulasan";
        break;
      default:
        _text = "";
        break;
    }
    return Text(
      _text,
      style: TextStyle(
          fontWeight: FontWeight.bold, color: Colors.white, fontSize: 15),
    );
  }

  Widget _loadAllComments(BuildContext context) {
    var _size = SizeConfig.safeBlockHorizontal;
    return Container(
      height: _size * 10,
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 1,
            width: _size * 5,
            color: Colors.grey,
          ),
          Container(
            width: _size * 6,
          ),
          Text(
            "Sembunyikan balasan",
            style: TextStyle(
                fontSize: _size * 3,
                fontWeight: FontWeight.bold,
                color: Colors.grey),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var _size = SizeConfig.safeBlockHorizontal;
    final provider = Provider.of<CommentEmitenProvider>(context, listen: false);
    if (provider.commentState == CommentState.loading) {
      // if (provider.commentEmiten == null) {
      provider.fetchComments(provider.commentEmiten.uuid);
      // } else {
      //   provider.comments = provider.commentEmiten;
      //   provider.commentState = CommentState.loaded;
      // }
    }
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, 'refresh');
        return;
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            elevation: 2,
            iconTheme: IconThemeData(color: Colors.white),
            title: _titleBar(context),
            backgroundColor: Color(ColorRev.mainBlack),
          ),
          body: Stack(
            children: <Widget>[
              Container(
                color: Colors.black,
                height: SizeConfig.screenHeight,
                width: double.infinity,
                margin: showAction
                    ? EdgeInsets.only(
                        bottom: _size * 17.5,
                      )
                    : null,
                child: provider.commentState == CommentState.loading
                    ? Center(
                        child: CupertinoActivityIndicator(),
                      )
                    : provider.commentState == CommentState.error
                        ? Center(
                            child: Container(
                              height: SizeConfig.screenHeight / 8,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(Icons.replay),
                                      onPressed: () {
                                        provider.commentState =
                                            CommentState.loading;
                                        provider.fetchComments(
                                            provider.commentEmiten.uuid);
                                      }),
                                  Text("Terjadi Kesalahan"),
                                ],
                              ),
                            ),
                          )
                        : provider.commentState == CommentState.loaded
                            ? ListView.builder(
                                padding: EdgeInsets.all(_size * 5),
                                shrinkWrap: true,
                                // physics: NeverScrollableScrollPhysics(),
                                itemCount: provider.comments.comments != null
                                    ? provider.commentsLength <
                                            provider.comments.comments.length
                                        ? provider.commentsLength + 1
                                        : provider.commentsLength
                                    : 0,
                                itemBuilder: (context, index) {
                                  int _commentsLength = provider.commentsLength;
                                  int repliesLength = 0;
                                  if (index <
                                      provider.comments.comments.length) {
                                    repliesLength = index >= _commentsLength
                                        ? 0
                                        : provider.comments.comments[index]
                                                    .commentHistories !=
                                                null
                                            ? provider.comments.comments[index]
                                                .commentHistories.length
                                            : 0;
                                  }

                                  return index >= _commentsLength
                                      ? provider.commentsLength >=
                                              provider.comments.comments.length
                                          ? Container()
                                          : _loadMoreComments(
                                              context,
                                              () {
                                                int _more = 0;
                                                int _actualLength = provider
                                                    .comments.comments.length;
                                                // print(
                                                //     ">> Actual Length : $_actualLength");
                                                // print(
                                                //     ">> Comments Length : ${provider.commentsLength}");
                                                if (provider.commentsLength <
                                                    _actualLength) {
                                                  _more = (_actualLength -
                                                              provider
                                                                  .commentsLength) <
                                                          5
                                                      ? _actualLength -
                                                          provider
                                                              .commentsLength
                                                      : 5;
                                                  provider.commentsLength =
                                                      provider.commentsLength +
                                                          _more;
                                                  // print(
                                                  //     ">> Length : ${provider.commentsLength}");
                                                }
                                              },
                                            )
                                      : index <
                                              provider.comments.comments.length
                                          ? UserComment(
                                              showAction: showAction,
                                              maxRadius: _size * 6.25,
                                              onDelete: () {
                                                _deleteComment(
                                                    context,
                                                    0,
                                                    index,
                                                    index,
                                                    provider.comments
                                                        .comments[index].uuid);
                                              },
                                              onReport: () {
                                                _handleReport(
                                                    context,
                                                    1,
                                                    index,
                                                    0,
                                                    provider.comments
                                                        .comments[index].uuid);
                                              },
                                              data: provider
                                                  .comments.comments[index],
                                              onReply: () {
                                                // print('>> selected index : $index');
                                                provider.indexComment = index;
                                                _commentFocusNode
                                                    .requestFocus();
                                              },
                                              replies: provider
                                                              .comments
                                                              .comments[index]
                                                              .commentHistories !=
                                                          null &&
                                                      provider
                                                              .comments
                                                              .comments[index]
                                                              .commentHistories
                                                              .length >
                                                          0
                                                  ? Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        // _loadAllComments(context), next progress,
                                                        ListView.builder(
                                                          itemBuilder: (context,
                                                              indexReply) {
                                                            return RepliesSection(
                                                              showAction:
                                                                  showAction,
                                                              maxRadius:
                                                                  _size * 3.75,
                                                              // jika mau pakai loadAllComments, maka set isFirstReply, index == 0 ? true : false
                                                              isFirstReply:
                                                                  false,
                                                              data: provider
                                                                      .comments
                                                                      .comments[
                                                                          index]
                                                                      .commentHistories[
                                                                  indexReply],
                                                              onReply: () {
                                                                provider.indexReply =
                                                                    indexReply;
                                                                provider.indexComment =
                                                                    index;
                                                                _commentFocusNode
                                                                    .requestFocus();
                                                              },
                                                              onReport: () {
                                                                _handleReport(
                                                                    context,
                                                                    1,
                                                                    index,
                                                                    indexReply,
                                                                    provider
                                                                        .comments
                                                                        .comments[
                                                                            index]
                                                                        .commentHistories[
                                                                            indexReply]
                                                                        .uuid);
                                                              },
                                                              onDelete: () {
                                                                _deleteComment(
                                                                    context,
                                                                    1,
                                                                    index,
                                                                    indexReply,
                                                                    provider
                                                                        .comments
                                                                        .comments[
                                                                            index]
                                                                        .commentHistories[
                                                                            indexReply]
                                                                        .uuid);
                                                              },
                                                            );
                                                          },
                                                          itemCount:
                                                              repliesLength,
                                                          physics:
                                                              NeverScrollableScrollPhysics(),
                                                          shrinkWrap: true,
                                                        )
                                                      ],
                                                    )
                                                  : Container(),
                                            )
                                          : Container();
                                })
                            : Container(),
              ),
              showAction ? _bottomNavbar(context) : Container()
            ],
          )),
    );
  }
}
