import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../blocs/video/video_bloc.dart';
import '../../blocs/video/video_category_bloc.dart';
import '../../core/usecases/unauthorize_usecase.dart';
import '../../models/video/Video.dart';
import '../../models/video/VideoCategory.dart';
import '../widget/components/listing/ListingListContent.dart';
import '../widget/components/main/SantaraAppBetaTesting.dart';
import 'DetailVideo.dart';
import 'SearchVideo.dart';

class VideoUI extends StatefulWidget {
  @override
  _VideoUIState createState() => _VideoUIState();
}

class _VideoUIState extends State<VideoUI> {
  DateFormat dateFormat = new DateFormat("dd MMM yyyy", "id");

  VideoBloc _videoBloc;
  VideoCategoryBloc _videoCategoryBloc;

  String _categoryStr = "Semua";
  String _titleStr = "";
  String _urut = "Urutkan";
  int _sort = 0;

  Future showSortVideo<bool>(int data) {
    return showDialog(
        context: context,
        builder: (_) {
          int vid = data;
          return AlertDialog(
            backgroundColor: Colors.black,
            title: Text("Tampilkan Video Berdasarkan",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Color(ColorRev.mainwhite))),
            content: StatefulBuilder(builder: (ctx, setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Radio(
                          hoverColor: Colors.white,
                          value: 1,
                          groupValue: vid,
                          onChanged: (value) {
                            setState(() => vid = value);
                          }),
                      Text("Terbaru",
                          style: TextStyle(
                              fontSize: 14, color: Color(0xFF858585))),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                          value: 2,
                          groupValue: vid,
                          onChanged: (value) {
                            setState(() => vid = value);
                          }),
                      Text("Paling Disukai",
                          style: TextStyle(
                              fontSize: 14, color: Color(0xFF858585))),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                          value: 3,
                          groupValue: vid,
                          onChanged: (value) {
                            setState(() => vid = value);
                          }),
                      Text("Paling Banyak Dilihat",
                          style: TextStyle(
                              fontSize: 14, color: Color(0xFF858585))),
                    ],
                  ),
                  Container(height: 16),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () => Navigator.of(context).pop(0),
                          child: Container(
                            height: 36,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(
                                    color: Color(ColorRev.mainwhite))),
                            child: Center(
                                child: Text("Batal",
                                    style: TextStyle(
                                        color: Color(ColorRev.mainwhite)))),
                          ),
                        ),
                      ),
                      Container(width: 6),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop(vid);
                            setState(() => _sort = vid);
                            _videoBloc.add(VideoRequested(
                                title: _titleStr,
                                category:
                                    _categoryStr == "Semua" ? "" : _categoryStr,
                                newest: _sort == 1 ? true : false,
                                likes: _sort == 2 ? true : false,
                                viewer: _sort == 3 ? true : false));
                          },
                          child: Container(
                            height: 36,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Color(ColorRev.mainRed)),
                            child: Center(
                                child: Text("Tampilkan",
                                    style: TextStyle(color: Colors.white))),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            }),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    _videoBloc = BlocProvider.of<VideoBloc>(context);
    _videoCategoryBloc = BlocProvider.of<VideoCategoryBloc>(context);
    _videoBloc.add(VideoRequested(
        title: "", category: "", likes: false, newest: false, viewer: false));
    _videoCategoryBloc.add(VideoCategoryRequested());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(context)),
        automaticallyImplyLeading: true,
        title: Image.asset('assets/santara/1.png', width: 110.0),
        centerTitle: true,
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white),
            onPressed: () {
              final result = Navigator.push(
                  context, MaterialPageRoute(builder: (_) => SearchVideo()));
              result.then((value) {
                if (value != null) {
                  setState(() => _titleStr = value);
                  _videoBloc.add(VideoRequested(
                      title: _titleStr,
                      category: _categoryStr == "Semua" ? "" : _categoryStr,
                      newest: _sort == 1 ? true : false,
                      likes: _sort == 2 ? true : false,
                      viewer: _sort == 3 ? true : false));
                }
              });
            },
          )
        ],
      ),
      body: Container(
        color: Color(ColorRev.mainBlack),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: SantaraAppBetaTesting(),
            ),
            // ui for tab category video
            BlocBuilder<VideoCategoryBloc, VideoCategoryState>(
              builder: (context, state) {
                // jika category masih loading
                if (state is VideoCategoryLoadInProgress) {
                  return Container(
                    height: 60,
                    child: ListView.builder(
                      physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 5,
                      itemBuilder: (_, i) {
                        return Container(
                          margin: EdgeInsets.fromLTRB(4, 8, 4, 8),
                          padding: EdgeInsets.all(8),
                          height: 40,
                          width: MediaQuery.of(context).size.width / 3,
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(20)),
                        );
                      },
                    ),
                  );
                }
                // jika kategori gagal nge get data
                if (state is VideoCategoryLoadFailure) {
                  return Text(
                    state.error.toString(),
                    style: TextStyle(color: Colors.red),
                  );
                }

                // jika berhasil nge get data
                if (state is VideoCategoryLoadSuccess) {
                  final category = state.category;
                  return Container(
                    height: 60,
                    child: ListView.builder(
                      physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      shrinkWrap: true,
                      itemCount: category.length,
                      itemBuilder: (_, i) {
                        return _category(category[i]);
                      },
                    ),
                  );
                }

                // default
                return Container();
              },
            ),

            // ui for menu sort
            ListTile(
              leading: Icon(
                Icons.sort,
                color: Colors.white,
              ),
              title: Text(
                _urut,
                style: TextStyle(color: Colors.white),
              ),
              onTap: () async {
                final result = await showSortVideo(_sort);
                setState(() {
                  if (result != null || result != 0) {
                    switch (result) {
                      case 1:
                        _urut = "Terbaru";
                        break;
                      case 2:
                        _urut = "Paling Disukai";
                        break;
                      case 3:
                        _urut = "Paling Banyak Dilihat";
                        break;
                      default:
                        _urut = "Urutkan";
                        break;
                    }
                  }
                });
              },
            ),

            // ui for video
            BlocBuilder<VideoBloc, VideoState>(
              builder: (context, state) {
                if (state is VideoLoadInProgress) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListingLoaderSegment(
                          height: MediaQuery.of(context).size.height / 4 + 48,
                          width: double.maxFinite,
                        ),
                        ListTile(
                          contentPadding: EdgeInsets.all(0),
                          leading: ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                30,
                              ),
                            ),
                            child: ListingLoaderSegment(
                              height: 50,
                              width: 50,
                            ),
                          ),
                          title: ListingLoaderSegment(
                            height: 12,
                            width: double.maxFinite,
                          ),
                          subtitle: Container(
                            margin: EdgeInsets.only(top: 10),
                            width: 80,
                            child: ListingLoaderSegment(
                              height: 10,
                              width: 80,
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }
                if (state is VideoLoadSuccess) {
                  final video = state.video;
                  if (video == null || video.length == 0) {
                    Flexible(
                        child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            height: MediaQuery.of(context).size.height / 3,
                            child: Image.asset(
                                "assets/icon/search_not_found.png")),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 12, 16, 4),
                          child: Text(
                            "Tidak ada hasil yang ditemukan",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: Colors.white),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                          child: Text(
                            "Coba kata kunci lain atau hapus filter penelusuran",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ));
                  } else {
                    return Flexible(
                      child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: video.length,
                        padding: EdgeInsets.fromLTRB(12, 4, 12, 0),
                        itemBuilder: (_, i) {
                          return _itemVideo(video[i], i);
                        },
                      ),
                    );
                  }
                }
                if (state is VideoLoadFailure) {
                  return _emptyVideo();
                }
                return Container();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _emptyVideo() {
    return Flexible(
        child: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            height: MediaQuery.of(context).size.height / 3,
            child: Image.asset("assets/icon/search_not_found.png")),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 12, 16, 4),
          child: Text(
            "Tidak ada hasil yang ditemukan",
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Text(
            "Coba kata kunci lain atau hapus filter penelusuran",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    ));
  }

  Widget _category(VideoCategory category) {
    return GestureDetector(
      onTap: () {
        setState(() => _categoryStr = category.category);
        _videoBloc.add(VideoRequested(
            title: _titleStr,
            category: category.category == "Semua" ? "" : category.category,
            newest: _sort == 1 ? true : false,
            likes: _sort == 2 ? true : false,
            viewer: _sort == 3 ? true : false));
      },
      child: Container(
          margin: EdgeInsets.fromLTRB(4, 8, 4, 8),
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          height: 40,
          decoration: _categoryStr == category.category
              ? BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Color(0xFFF9DBDB))
              : BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: Color(0xFFDDDDDD)),
                  color: Colors.white),
          child: Center(
            child: Text(
              category.category,
              style: TextStyle(
                  color: _categoryStr == category.category
                      ? Color(0xFFBF2D30)
                      : Colors.black),
            ),
          )),
    );
  }

  Widget _itemVideo(Videos video, int i) {
    var thumbnail = YoutubePlayer.getThumbnail(
        videoId: YoutubePlayer.convertUrlToId(video.link));
    return Column(
      children: <Widget>[
        Container(height: 6),
        InkWell(
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (_) => DetailVideo(video: video))),
          child: Container(
            height: MediaQuery.of(context).size.height / 4 + 48,
            decoration: BoxDecoration(
                color: Colors.grey[300],
                image: DecorationImage(
                    image: NetworkImage(thumbnail), fit: BoxFit.cover)),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                margin: EdgeInsets.only(right: 16),
                padding: EdgeInsets.all(9),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  border: Border.all(width: 1, color: Color(0xFFBF2D30)),
                ),
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/santara/2.png"))),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => DetailVideo(video: video))),
                      child: Text(video.title,
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                          overflow: TextOverflow.ellipsis),
                    ),
                    Text(
                        "${video.category} • ${video.views}x Ditonton • ${dateFormat.format(DateTime.parse(video.publishDate))}",
                        style: TextStyle(color: Colors.grey, fontSize: 12)),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(height: 6),
      ],
    );
  }

  Widget _duration(YoutubePlayerController controller) {
    var durasi = controller.value.metaData.duration;
    String second = (durasi.inSeconds % 60).toString();
    String minute = (durasi.inMinutes % 60).toString();
    String hour = (durasi.inHours.toString());
    if (second.length == 1) {
      second = "0" + second;
    }
    if (minute.length == 1) {
      minute = "0" + minute;
    }
    if (hour.length == 1) {
      hour = "0" + hour;
    }
    return Align(
      alignment: Alignment.bottomRight,
      child: durasi.inSeconds == 0
          ? Container()
          : Container(
              height: 30,
              padding: EdgeInsets.fromLTRB(12, 6, 12, 6),
              margin: EdgeInsets.fromLTRB(8, 0, 8, 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2),
                  color: Colors.black.withOpacity(0.5)),
              child: Text(
                durasi.inHours > 0
                    ? "$hour:$minute:$second"
                    : "$minute:$second",
                style: TextStyle(color: Colors.white),
              ),
            ),
    );
  }
}
