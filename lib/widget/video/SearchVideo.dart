import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../core/usecases/unauthorize_usecase.dart';

class SearchVideo extends StatefulWidget {
  @override
  _SearchVideoState createState() => _SearchVideoState();
}

class _SearchVideoState extends State<SearchVideo> {
  final TextEditingController searchVideo = TextEditingController();
  List<String> searchHistory = new List<String>();
  final storage = new FlutterSecureStorage();
  String searchString = "";

  Future getHistorySearch() async {
    String data;
    if (await storage.read(key: "searchHistory") == null) {
      data = "";
    } else {
      data = await storage.read(key: "searchHistory");
    }
    setState(() {
      searchString = data;
      List<String> s = data.split(",");
      List<String> s1 = [];
      for (var i = 0; i < s.length; i++) {
        if (s[i] != "") {
          s1.add(s[i]);
        }
      }
      searchHistory = s1;
    });
  }

  Future addHistory() async {
    await storage.write(key: "searchHistory", value: searchString);
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getHistorySearch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.pop(context, null);
          },
        ),
        titleSpacing: 0,
        title: Container(
          color: Colors.grey[200],
          margin: EdgeInsets.only(right: 8),
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: TextField(
            controller: searchVideo,
            decoration: InputDecoration(
              hintText: "Telusuri Video",
              border: InputBorder.none,
            ),
            onSubmitted: (str) {
              setState(() {
                if (searchString == "") {
                  searchString = str;
                } else {
                  searchString = str + "," + searchString;
                }
                addHistory();
                Navigator.pop(context, str);
              });
            },
          ),
        ),
      ),
      body: searchHistory.length == 0
          ? Container()
          : ListView.builder(
              padding: EdgeInsets.symmetric(vertical: 6),
              itemCount: searchHistory.length,
              itemBuilder: (_, i) {
                return _itemHistory(searchHistory[i]);
              },
            ),
    );
  }

  Widget _itemHistory(String str) {
    return InkWell(
      onTap: () => Navigator.pop(context, str),
      child: Container(
        height: 50,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Icon(
                Icons.search,
                color: Colors.grey,
              ),
            ),
            Expanded(
                child: Text(
              str,
              style: TextStyle(fontSize: 16),
              overflow: TextOverflow.ellipsis,
            ))
          ],
        ),
      ),
    );
  }
}
