import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shimmer/shimmer.dart';
import 'package:toast/toast.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../core/usecases/unauthorize_usecase.dart';
import '../../helpers/UserAgent.dart';
import '../../models/video/Video.dart';
import '../../utils/api.dart';

class DetailVideo extends StatefulWidget {
  final Videos video;
  DetailVideo({this.video});
  @override
  _DetailVideoState createState() => _DetailVideoState();
}

class _DetailVideoState extends State<DetailVideo> {
  var isExpanded = false;
  var isLoading = true;
  var loading = false;
  String token;
  DateFormat dateFormat = new DateFormat("dd MMM yyyy", "id");
  final storage = new FlutterSecureStorage();
  YoutubePlayerController _controller;
  List<Videos> videos;
  Videos detailVideo;

  Future getVideos() async {
    final http.Response response = await http.get(
        '$apiLocal/videos/video?title&category=${widget.video.category}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      setState(() {
        List<Videos> v2 = [];
        List<Videos> v = videosFromJson(response.body);
        for (var i = 0; i < v.length; i++) {
          if (v[i].uuid != widget.video.uuid) {
            v2.add(v[i]);
          }
        }
        videos = v2;
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
        videos = [];
      });
    }
  }

  Future getDetailVideo() async {
    final http.Response response = await http.get(
        '$apiLocal/videos/video/${widget.video.uuid}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      setState(() {
        detailVideo = Videos.fromJson(json.decode(response.body));
      });
    }
  }

  Future postLikeVideo() async {
    setState(() => loading = true);
    final http.Response response = await http.post(
        '$apiLocal/videos/like/${widget.video.uuid}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      setState(() {
        loading = false;
        getDetailVideo();
        Toast.show(data["message"], context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      });
    }
  }

  Future postDislikeVideo() async {
    setState(() => loading = true);
    final headers = await UserAgent.headers();
    final http.Response response = await http.post(
        '$apiLocal/videos/dislike/${widget.video.uuid}',
        headers: headers);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      setState(() {
        loading = false;
        getDetailVideo();
        Toast.show(data["message"], context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      });
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getLocalStorage().then((_) {
      getDetailVideo();
      getVideos();
    });
    _controller = YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId(widget.video.link),
      flags: YoutubePlayerFlags(autoPlay: true, mute: false),
    );
  }

  @override
  void dispose() {
    super.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/santara/1.png',
          width: 110,
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            YoutubePlayer(
              controller: _controller,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 2),
              child: detailVideo == null
                  ? Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                          height: 30,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey),
                    )
                  : Text(detailVideo.title,
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 16)),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
              child: detailVideo == null
                  ? Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                          margin: EdgeInsets.only(top: 12),
                          height: 16,
                          width: MediaQuery.of(context).size.width * 2 / 3,
                          color: Colors.grey),
                    )
                  : Text(
                      detailVideo.views.toString() +
                          "x Ditonton • Kategori : " +
                          detailVideo.category,
                      style: TextStyle(color: Colors.grey, fontSize: 13)),
            ),
            detailVideo == null
                ? Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.white,
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        height: 35,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey),
                  )
                : Container(
                    padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                    margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    height: 40,
                    color: Color(0xFFE6EAFA),
                    child: InkWell(
                      onTap: () => setState(() {
                        if (isExpanded) {
                          isExpanded = false;
                        } else {
                          isExpanded = true;
                        }
                      }),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Text(
                                  "Dipublikasikan tanggal ${dateFormat.format(DateTime.parse(detailVideo.publishDate))}")),
                          Icon(
                            isExpanded
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down,
                            color: Color(0xFF218196),
                          )
                        ],
                      ),
                    ),
                  ),
            isExpanded
                ? Container(
                    padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                    child: Text(detailVideo.description),
                  )
                : Container(),
            detailVideo == null
                ? Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.white,
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(20, 16, 20, 12),
                        height: 40,
                        width: MediaQuery.of(context).size.width / 2,
                        color: Colors.grey),
                  )
                : Container(
                    padding: const EdgeInsets.fromLTRB(20, 16, 20, 12),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                            onTap: loading
                                ? null
                                : () {
                                    if (detailVideo.isDislike == 1) {
                                      postDislikeVideo()
                                          .then((_) => postLikeVideo());
                                    } else {
                                      postLikeVideo();
                                    }
                                  },
                            child: detailVideo.isLike == 1
                                ? Icon(MdiIcons.thumbUp,
                                    color: Color(0xFFBF2D30))
                                : Icon(MdiIcons.thumbUpOutline)),
                        Container(width: 6),
                        Text(detailVideo.likes.toString()),
                        Container(width: 20),
                        GestureDetector(
                            onTap: loading
                                ? null
                                : () {
                                    if (detailVideo.isLike == 1) {
                                      postLikeVideo()
                                          .then((_) => postDislikeVideo());
                                    } else {
                                      postDislikeVideo();
                                    }
                                  },
                            child: detailVideo.isDislike == 1
                                ? Icon(MdiIcons.thumbDown,
                                    color: Color(0xFFBF2D30))
                                : Icon(MdiIcons.thumbDownOutline)),
                        Container(width: 6),
                        Text(detailVideo.dislikes.toString()),
                      ],
                    ),
                  ),
            Container(
              height: 6,
              color: Color(0xFFF4F4F4),
              margin: EdgeInsets.symmetric(vertical: 12),
            ),
            isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: Center(child: CircularProgressIndicator()))
                : ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.all(12),
                    itemCount: videos.length,
                    itemBuilder: (_, i) {
                      return _itemVideo(videos[i]);
                    },
                  )
          ],
        ),
      ),
    );
  }

  Widget _itemVideo(Videos videos) {
    var videoId = YoutubePlayer.convertUrlToId(videos.link);
    var thumbnail = YoutubePlayer.getThumbnail(videoId: videoId);
    return Container(
      margin: EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
              child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () {
                  _controller.pause();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => DetailVideo(video: videos)));
                },
                child: Container(
                    height: MediaQuery.of(context).size.height / 7,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                            image: NetworkImage(thumbnail), fit: BoxFit.cover)),
                    child: Center()),
              ),
            ],
          )),
          Container(width: 10),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 4),
              height: MediaQuery.of(context).size.height / 7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _controller.pause();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => DetailVideo(video: videos)));
                    },
                    child: Text(
                      videos.title,
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Santara Official",
                          style:
                              TextStyle(color: Colors.grey[700], fontSize: 13)),
                      Text("${videos.views}x Ditonton",
                          style:
                              TextStyle(color: Colors.grey[700], fontSize: 11)),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
