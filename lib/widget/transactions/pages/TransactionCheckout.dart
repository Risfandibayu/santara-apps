import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:toast/toast.dart';

import '../../../features/dana/presentation/pages/dana_webview.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../models/TokenHistory.dart';
import '../../../pages/Home.dart';
import '../../../utils/api.dart';
import '../../../utils/sizes.dart';
import '../../token/Payment.dart';
import '../../widget/components/main/SantaraCachedImage.dart';
import '../blocs/trans.checkout.bloc.dart';

class TransactionCheckout extends StatefulWidget {
  @override
  _TransactionCheckoutState createState() => _TransactionCheckoutState();
}

class _TransactionCheckoutState extends State<TransactionCheckout> {
  TransCheckoutBloc bloc;
  final rupiah = NumberFormat("#,##0");
  DateFormat dateFormat = DateFormat("EEEE, dd MMM yyyy HH:mm:ss", "id");
  DateFormat dateFormat2 = DateFormat("dd MMM yyyy HH:mm", "id");
  DateFormat dateFormat3 = DateFormat("dd MMM yyyy", "id");

  void showModalUnverified(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(Sizes.s15),
            topRight: Radius.circular(Sizes.s15),
          ),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Pembayaran gagal dilakukan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 50,
                      width: double.maxFinite,
                      color: Color(0xFFBF2D30),
                      child: Center(
                        child: Text(
                          "Mengerti",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _transItem(TokenHistory token, bool isVerified) {
    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    var picture = token.picture != null && token.picture.length > 0
        ? token.picture[0].picture
        : "";
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Sizes.s10),
      ),
      margin: EdgeInsets.all(Sizes.s8),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Sizes.s10),
              border: Border.all(
                color: Color(
                  0xFFC4C4C4,
                ),
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(Sizes.s15),
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(Sizes.s50),
                      child: SantaraCachedImage(
                        height: Sizes.s60,
                        width: Sizes.s60,
                        image: '$picture',
                        fit: BoxFit.cover,
                      ),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: token.category == null
                                  ? Container()
                                  : Text(
                                      token.category,
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontSize: FontSize.s12,
                                        color: Color(0xFF292F8D),
                                      ),
                                    ),
                            ),
                            Container(
                              width: Sizes.s30,
                              height: Sizes.s20,
                              child: PopupMenuButton<int>(
                                icon: Icon(
                                  Icons.more_horiz,
                                  size: Sizes.s20,
                                ),
                                // padding: EdgeInsets.only(right: 10),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(3.0),
                                  side: BorderSide(
                                    color: Color(0xffD6D6D6),
                                    width: 1,
                                  ),
                                ),
                                // margin: EdgeInsets.all(0),
                                onSelected: (int result) async {
                                  if (result == 1) {
                                    PopupHelper.popConfirmation(context,
                                        () async {
                                      Navigator.pop(context);
                                      bloc
                                        ..add(CancelTransCheckout(
                                            trxId: "${token.id}"));
                                    }, "Hapus Transaksi",
                                        "Apakah anda yakin ingin menghapus dan membatalkan transaksi ini?");
                                  }
                                },
                                itemBuilder: (BuildContext context) =>
                                    <PopupMenuEntry<int>>[
                                  PopupMenuItem<int>(
                                    height: Sizes.s25,
                                    value: 1,
                                    child: Text("Hapus Transaksi"),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Container(
                          child: token.trademark == null
                              ? Container()
                              : Text(
                                  token.trademark,
                                  overflow: TextOverflow.visible,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: FontSize.s16,
                                  ),
                                ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                "${token?.companyName ?? ''}",
                                overflow: TextOverflow.visible,
                                style: TextStyle(fontSize: FontSize.s12),
                              ),
                            ),
                            Container(
                              child: Text(
                                dateFormat3.format(
                                  DateTime.parse(
                                    token?.createdAt ?? DateTime(1970),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Theme(
                  data: theme,
                  child: ExpansionTile(
                    tilePadding: EdgeInsets.zero,
                    backgroundColor: Colors.transparent,
                    initiallyExpanded: false,
                    title: Container(
                      margin: EdgeInsets.only(left: Sizes.s15),
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: Color(0xFFBF2D30),
                        ),
                        borderRadius: BorderRadius.circular(Sizes.s5),
                      ),
                      width: double.maxFinite,
                      padding: EdgeInsets.all(Sizes.s10),
                      child: Text(
                        "Batas : " +
                            dateFormat.format(token.expiredDate == null
                                ? DateTime(1970)
                                : DateTime.parse(token.expiredDate)),
                        style: TextStyle(
                          color: Color(0xFFBF2D30),
                          fontSize: FontSize.s14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    trailing: Container(
                      width: Sizes.s60 + Sizes.s5,
                      child: Row(
                        children: [
                          Text(
                            "Detail",
                            style: TextStyle(
                              color: Color(0xff218196),
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Color(0xff218196),
                          )
                        ],
                      ),
                    ),
                    children: [
                      isProduction
                          ? Container()
                          : Row(
                              children: [
                                Text("fixed-va-${token.id}-t${token.id}rx"),
                                IconButton(
                                    icon: Icon(Icons.content_copy),
                                    onPressed: () {
                                      Clipboard.setData(ClipboardData(
                                          text:
                                              "fixed-va-${token.id}-t${token.id}rx"));
                                      Toast.show("Copied to Clipboard", context,
                                          duration: Toast.LENGTH_LONG,
                                          gravity: Toast.BOTTOM);
                                    })
                              ],
                            ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Text("No. Transaksi",
                                    style: TextStyle(fontSize: 15))),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "SAN-${token.id}-${token.codeEmiten}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                InkWell(
                                  onTap: () {
                                    Clipboard.setData(ClipboardData(
                                      text:
                                          "SAN-${token.id}-${token.codeEmiten}",
                                    ));
                                    Toast.show(
                                      "Copied to Clipboard",
                                      context,
                                      duration: Toast.LENGTH_LONG,
                                      gravity: Toast.BOTTOM,
                                    );
                                  },
                                  child: Container(
                                    width: Sizes.s35,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.link,
                                        size: FontSize.s25,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Harga Saham",
                                    style: TextStyle(fontSize: 15))),
                            Text("Rp ${rupiah.format(token.price)}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15))
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Jumlah Investasi",
                                    style: TextStyle(fontSize: 15))),
                            Text(
                                "${(token.amount / token.price).toStringAsFixed(0)} Lembar",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15))
                          ],
                        ),
                      ),
                      token.chanel == "VA"
                          ? Padding(
                              padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Text("Biaya Admin",
                                          style: TextStyle(fontSize: 15))),
                                  Text("Rp ${rupiah.format(token.fee)}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15))
                                ],
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: Sizes.s20),
                  height: 1,
                  color: Color(0xFFC4C4C4),
                ),
                Container(
                  // color: Color(0xFFF2F2F2),
                  padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Total",
                              style: TextStyle(
                                fontSize: FontSize.s12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "Rp ${rupiah.format(token.amount + token.fee)}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            )
                          ],
                        ),
                      ),
                      token.lastStatus == 'CREATED'
                          ? FlatButton(
                              color: Color(0xffFF572E),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(Sizes.s5),
                              ),
                              onPressed: () {
                                if (token.lastStatus == 'CREATED') {
                                  if (isVerified) {
                                    if (token.chanel == "DANA") {
                                      final result = Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => DanaWebview(
                                                    appbarName: "",
                                                    url: token.danaCheckoutUrl,
                                                    type: DanaWebviewType.trx,
                                                  )));
                                      result.then((_) {
                                        bloc..add(LoadTransCheckout());
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) => Home(index: 1)),
                                            (route) => false);
                                      });
                                    } else {
                                      final result = Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => Payment(
                                                  uuid: token.uuid,
                                                  chanel: token.chanel,
                                                  name: token.companyName,
                                                  emitenId: token.codeEmiten,
                                                  image: picture,
                                                  count: token.amount /
                                                      token.price,
                                                  bank: token.bank,
                                                  bankTo: token.bankTo,
                                                  price: token.price,
                                                  fee: token.fee,
                                                  amount: token.amount,
                                                  expiredDate:
                                                      token.expiredDate,
                                                  merchantCode:
                                                      token.merchantCode,
                                                  accountNumber:
                                                      token.accountNumber,
                                                  nameVa: token.name)));
                                      result.then((_) {
                                        bloc..add(LoadTransCheckout());
                                      });
                                    }
                                  } else if (!isVerified) {
                                    showModalUnverified(
                                        'Akun anda belum diverifikasi oleh admin');
                                  } else {
                                    showModalUnverified(
                                        'Terjadi kesalahan, harap hubungi CS Santara');
                                  }
                                }
                              },
                              child: Text(
                                "Bayar",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: FontSize.s14,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<TransCheckoutBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<TransCheckoutBloc, TransCheckoutState>(
      bloc: bloc,
      listener: (context, state) {
        if (state is TransCheckoutLoaded) {
          if (state?.cancelationSuccess != null) {
            Toast.show(
              "${state.cancelationSuccess}",
              context,
              duration: 3,
            );
            bloc..add(LoadTransCheckout());
          }

          if (state?.cancelationError != null) {
            Toast.show(
              "${state.cancelationError}",
              context,
              duration: 3,
            );
            bloc..add(LoadTransCheckout());
          }
        }
      },
      child: BlocBuilder<TransCheckoutBloc, TransCheckoutState>(
        builder: (context, state) {
          if (state is TransCheckoutUninitialized) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          } else if (state is TransCheckoutError) {
            return Center(
              child: Text(state.error),
            );
          } else if (state is TransCheckoutEmpty) {
            return Container(
              color: Color(ColorRev.mainBlack),
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset("assets/icon/empty_dividen.png"),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                    child: Text(
                      "Anda tidak memiliki transaksi pembelian",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            );
          } else if (state is TransCheckoutLoaded) {
            return RefreshIndicator(
              onRefresh: () async {
                bloc..add(LoadTransCheckout());
              },
              child: ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: state.datas.length,
                padding: EdgeInsets.all(Sizes.s10),
                itemBuilder: (context, index) {
                  TokenHistory history = state.datas[index];
                  return _transItem(history, state.isVerified);
                },
              ),
            );
          } else {
            return Center(
              child: Text("Unknown State!"),
            );
          }
        },
      ),
    );
  }
}
