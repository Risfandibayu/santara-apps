import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:toast/toast.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../models/TokenHistory.dart';
import '../../../utils/api.dart';
import '../../../utils/sizes.dart';
import '../../widget/components/main/SantaraCachedImage.dart';
import '../blocs/trans.history.bloc.dart';

class TransactionHistory extends StatefulWidget {
  @override
  _TransactionHistoryState createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory> {
  TransHistoryBloc bloc;
  final rupiah = NumberFormat("#,##0");
  DateFormat dateFormat = DateFormat("EEEE, dd MMM yyyy HH:mm:ss", "id");
  DateFormat dateFormat2 = DateFormat("dd MMM yyyy HH:mm", "id");
  DateFormat dateFormat3 = DateFormat("dd MMM yyyy", "id");

  Widget _transItem(TokenHistory token) {
    var picture = token.picture != null && token.picture.length > 0
        ? token.picture[0].picture
        : "";

    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Sizes.s10),
      ),
      margin: EdgeInsets.all(Sizes.s8),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Sizes.s10),
              border: Border.all(
                color: Color(
                  0xFFC4C4C4,
                ),
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(Sizes.s10),
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(Sizes.s50),
                      child: SantaraCachedImage(
                        height: Sizes.s60,
                        width: Sizes.s60,
                        image: '$picture',
                        fit: BoxFit.cover,
                      ),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: token.category == null
                                  ? Container()
                                  : Text(
                                      token.category,
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontSize: FontSize.s12,
                                        color: Color(0xFF292F8D),
                                      ),
                                    ),
                            ),
                          ],
                        ),
                        Container(
                          child: token.trademark == null
                              ? Container()
                              : Text(
                                  token.trademark,
                                  overflow: TextOverflow.visible,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: FontSize.s16,
                                  ),
                                ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                "${token?.companyName ?? ''}",
                                overflow: TextOverflow.visible,
                                style: TextStyle(fontSize: FontSize.s12),
                              ),
                            ),
                            Container(
                              child: Text(
                                dateFormat3.format(
                                  DateTime.parse(
                                    token?.createdAt ?? DateTime(1970),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: FontSize.s12,
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                // here
                Theme(
                  data: theme,
                  child: ExpansionTile(
                    tilePadding: EdgeInsets.zero,
                    backgroundColor: Colors.transparent,
                    initiallyExpanded: false,
                    title: Padding(
                      padding: EdgeInsets.only(left: Sizes.s15),
                      child: Row(
                        children: <Widget>[
                          token.lastStatus == 'WAITING FOR VERIFICATION'
                              ? Text(
                                  'Menunggu Konfirmasi',
                                  style: TextStyle(
                                    color: Color(0xFFE5A037),
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : token.lastStatus == 'VERIFIED'
                                  ? Text(
                                      token.desc == "buy"
                                          ? 'Pembelian Berhasil'
                                          : "Penjualan Berhasil",
                                      style: TextStyle(
                                        color: Color(0XFF0E7E4A),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  : Text(
                                      'Pembelian Gagal',
                                      style: TextStyle(
                                        color: Color(0XFFBF2D30),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                        ],
                      ),
                    ),
                    trailing: Container(
                      width: Sizes.s60 + Sizes.s5,
                      child: Row(
                        children: [
                          Text(
                            "Detail",
                            style: TextStyle(
                              color: Color(0xff218196),
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Color(0xff218196),
                          )
                        ],
                      ),
                    ),
                    children: [
                      isProduction
                          ? Container()
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text("fixed-va-${token.id}-t${token.id}rx"),
                                IconButton(
                                  icon: Icon(Icons.content_copy),
                                  onPressed: () {
                                    Clipboard.setData(ClipboardData(
                                        text:
                                            "fixed-va-${token.id}-t${token.id}rx"));
                                    Toast.show(
                                      "Copied to Clipboard",
                                      context,
                                      duration: Toast.LENGTH_LONG,
                                      gravity: Toast.BOTTOM,
                                    );
                                  },
                                )
                              ],
                            ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Text("No. Transaksi",
                                    style: TextStyle(fontSize: 15))),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "SAN-${token.id}-${token.codeEmiten}",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Clipboard.setData(
                                      ClipboardData(
                                        text:
                                            "SAN-${token.id}-${token.codeEmiten}",
                                      ),
                                    );
                                    Toast.show(
                                      "Copied to Clipboard",
                                      context,
                                      duration: Toast.LENGTH_LONG,
                                      gravity: Toast.BOTTOM,
                                    );
                                  },
                                  child: Container(
                                    width: Sizes.s35,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.link,
                                        size: FontSize.s25,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Harga Saham",
                                    style: TextStyle(fontSize: 15))),
                            Text("Rp ${rupiah.format(token.price)}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15))
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Text("Jumlah Investasi",
                                    style: TextStyle(fontSize: 15))),
                            Text(
                                "${(token.amount / token.price).toStringAsFixed(0)} Lembar",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15))
                          ],
                        ),
                      ),
                      token.chanel == "ONEPAY"
                          ? Padding(
                              padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Text("Biaya Admin",
                                          style: TextStyle(fontSize: 15))),
                                  Text(
                                      "Rp ${rupiah.format(token.fee)} (OTHER PAYMENT)",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15))
                                ],
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                Container(
                  // margin: EdgeInsets.only(top: Sizes.s10),
                  height: 1,
                  color: Color(0xFFC4C4C4),
                ),
                Container(
                  // color: Color(0xFFF2F2F2),
                  padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Total",
                              style: TextStyle(
                                fontSize: FontSize.s12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              token.chanel == "MARKET"
                                  ? token.desc == "buy"
                                      ? "Rp ${rupiah.format(token.amount + token.fee)}"
                                      : "Rp ${rupiah.format(token.amount - token.fee)}"
                                  : "Rp ${rupiah.format(token.amount)}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<TransHistoryBloc>(context);
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransHistoryBloc, TransHistoryState>(
      builder: (context, state) {
        if (state is TransHistoryUninitialized) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else if (state is TransHistoryError) {
          return Center(
            child: Text(state.error),
          );
        } else if (state is TransHistoryEmpty) {
          return Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset("assets/icon/empty_dividen.png"),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                  child: Text(
                    "Anda tidak memiliki transaksi pembelian",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          );
        } else if (state is TransHistoryLoaded) {
          return RefreshIndicator(
            onRefresh: () async {
              bloc..add(LoadTransHistory());
            },
            child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: state.datas.length,
              padding: EdgeInsets.all(Sizes.s10),
              itemBuilder: (context, index) {
                TokenHistory history = state.datas[index];
                return _transItem(history);
              },
            ),
          );
        } else {
          return Center(
            child: Text("Unknown State!"),
          );
        }
      },
    );
  }
}
