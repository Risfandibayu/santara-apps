import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/transactions/blocs/trans.checkout.bloc.dart';
import 'package:santaraapp/widget/transactions/blocs/trans.history.bloc.dart';
import 'package:santaraapp/widget/transactions/pages/TransactionCheckout.dart';
import 'package:santaraapp/widget/transactions/pages/TransactionHistory.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';

class TransactionsUI extends StatefulWidget {
  final int initialTab;

  const TransactionsUI({Key key, this.initialTab}) : super(key: key);

  @override
  _TransactionsUIState createState() => _TransactionsUIState();
}

class _TransactionsUIState extends State<TransactionsUI> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => TransCheckoutBloc(TransCheckoutUninitialized())
            ..add(LoadTransCheckout()),
        ),
        BlocProvider(
          create: (context) => TransHistoryBloc(TransHistoryUninitialized())
            ..add(LoadTransHistory()),
        ),
      ],
      child: Scaffold(
        key: _drawerKey,
        appBar: CustomAppbar(drawerKey: _drawerKey),
        drawer: CustomDrawer(),
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 50,
              child: DefaultTabController(
                initialIndex: widget.initialTab ?? 0,
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(50),
                    child: Container(
                      child: SafeArea(
                        child: TabBar(
                          indicatorColor: Color((0xFFBF2D30)),
                          labelColor: Color((0xFFBF2D30)),
                          unselectedLabelColor: Colors.black,
                          tabs: [
                            Container(
                              key: Key('mainTransactionTab'),
                              height: 50,
                              child: Center(
                                child: Text("Transaksi"),
                              ),
                            ),
                            Container(
                              key: Key('historyTransactionTab'),
                              height: 50,
                              child: Center(
                                child: Text("Riwayat Transaksi"),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      TransactionCheckout(),
                      TransactionHistory(),
                    ],
                  ),
                ),
              ),
            ),
            KycNotificationStatus(
              showCloseButton: false,
            )
          ],
        ),
      ),
    );
  }
}
