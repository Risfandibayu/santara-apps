import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/helpers/KycStatusHelper.dart';
import 'package:santaraapp/models/TokenHistory.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/transactions/TransactionsService.dart';
import 'package:santaraapp/utils/logger.dart';

class TransCheckoutEvent {}

class TransCheckoutState {}

class LoadTransCheckout extends TransCheckoutEvent {}

class CancelTransCheckout extends TransCheckoutEvent {
  String trxId;
  CancelTransCheckout({this.trxId});
}

class TransCheckoutUninitialized extends TransCheckoutState {}

class TransCheckoutUnauthorized extends TransCheckoutState {}

class TransCheckoutError extends TransCheckoutState {
  String error;
  TransCheckoutError({this.error});
}

class TransCheckoutEmpty extends TransCheckoutState {}

class TransCheckoutLoaded extends TransCheckoutState {
  DateTime currentTime;
  List<TokenHistory> datas;
  String cancelationError;
  String cancelationSuccess;
  bool isVerified;

  TransCheckoutLoaded({
    this.currentTime,
    this.datas,
    this.cancelationError,
    this.cancelationSuccess,
    this.isVerified = false,
  });

  TransCheckoutLoaded copyWith({
    DateTime currentTime,
    List<TokenHistory> datas,
    String cancelationError,
    String cancelationSuccess,
    bool isVerified,
  }) {
    return TransCheckoutLoaded(
      datas: datas ?? this.datas,
      cancelationError: cancelationError ?? this.cancelationError,
      cancelationSuccess: cancelationSuccess ?? this.cancelationSuccess,
      isVerified: isVerified ?? this.isVerified,
    );
  }
}

class TransCheckoutBloc extends Bloc<TransCheckoutEvent, TransCheckoutState> {
  TransCheckoutBloc(TransCheckoutState initialState)
      : super(TransCheckoutUninitialized());

  TransactionsService transactionsService = TransactionsService();
  ApiService apiService = ApiService();
  KycStatusHelper kycStatusHelper = KycStatusHelper();

  @override
  Stream<TransCheckoutState> mapEventToState(TransCheckoutEvent event) async* {
    if (event is LoadTransCheckout) {
      yield TransCheckoutUninitialized();
      try {
        // var currentTime = await apiService.getCurrentTime();
        var result = await transactionsService.getCheckoutTransactions();
        var isVerified = await kycStatusHelper.check();
        if (result.status == ApiStatus.success) {
          List<TokenHistory> datas = [];
          datas = tokenHistoryFromJson(jsonEncode(result.data));
          if (datas?.length != null && datas.length > 0) {
            yield TransCheckoutLoaded(
              cancelationSuccess: null,
              cancelationError: null,
              // currentTime: currentTime,
              datas: datas,
              isVerified: isVerified,
            );
          } else {
            yield TransCheckoutEmpty();
          }
        } else if (result.status == ApiStatus.notFound) {
          yield TransCheckoutEmpty();
        } else if (result.status == ApiStatus.unauthorized) {
          yield TransCheckoutUnauthorized();
        } else if (result.status == ApiStatus.badRequest) {
          yield TransCheckoutEmpty();
        } else {
          yield TransCheckoutError(error: result.message);
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        yield TransCheckoutError(error: "Tidak dapat memuat data transaksi!");
      }
    } else if (event is CancelTransCheckout) {
      if (state is TransCheckoutLoaded) {
        var data = state as TransCheckoutLoaded;
        yield data.copyWith(cancelationSuccess: null, cancelationError: null);
        try {
          var result = await transactionsService.cancelTransaction(event.trxId);
          if (result.status == ApiStatus.success) {
            yield data.copyWith(
              cancelationSuccess: "Transaksi Berhasil Dibatalkan!",
              cancelationError: null,
            );
          } else if (result.status == ApiStatus.unauthorized) {
            yield TransCheckoutUnauthorized();
          } else {
            yield data.copyWith(
              cancelationSuccess: null,
              cancelationError:
                  "[${result.message}] Terjadi kesalahan, Transaksi tidak dapat dilakukan!",
            );
          }
        } catch (e, stack) {
          santaraLog(e, stack);
          yield data.copyWith(
            cancelationSuccess: null,
            cancelationError:
                "Terjadi kesalahan, Transaksi tidak dapat dilakukan!",
          );
        }
      }
    }
  }
}
