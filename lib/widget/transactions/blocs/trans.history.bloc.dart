import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/TokenHistory.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/transactions/TransactionsService.dart';
import 'package:santaraapp/utils/logger.dart';

class TransHistoryEvent {}

class TransHistoryState {}

class LoadTransHistory extends TransHistoryEvent {}

class TransHistoryUninitialized extends TransHistoryState {}

class TransHistoryUnauthorized extends TransHistoryState {}

class TransHistoryError extends TransHistoryState {
  final String error;
  TransHistoryError({this.error});
}

class TransHistoryEmpty extends TransHistoryState {}

class TransHistoryLoaded extends TransHistoryState {
  List<TokenHistory> datas;
  TransHistoryLoaded({this.datas});
}

class TransHistoryBloc extends Bloc<TransHistoryEvent, TransHistoryState> {
  TransHistoryBloc(TransHistoryState initialState)
      : super(TransHistoryUninitialized());

  TransactionsService transactionsService = TransactionsService();

  @override
  Stream<TransHistoryState> mapEventToState(TransHistoryEvent event) async* {
    if (event is LoadTransHistory) {
      yield TransHistoryUninitialized();
      try {
        var result = await transactionsService.getHistoryTransactions();

        if (result.status == ApiStatus.success) {
          List<TokenHistory> datas = [];
          datas = tokenHistoryFromJson(jsonEncode(result.data));
          if (datas?.length != null && datas.length > 0) {
            yield TransHistoryLoaded(
              datas: datas,
            );
          } else {
            yield TransHistoryEmpty();
          }
        } else if (result.status == ApiStatus.notFound) {
          yield TransHistoryEmpty();
        } else if (result.status == ApiStatus.badRequest) {
          yield TransHistoryEmpty();
        } else if (result.status == ApiStatus.unauthorized) {
          yield TransHistoryUnauthorized();
        } else {
          yield TransHistoryError(error: result.message);
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        yield TransHistoryError(error: "Tidak dapat memuat data transakssi!");
      }
    }
  }
}
