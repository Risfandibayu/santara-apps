import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/BiometricHelper.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../services/auth/auth_service.dart';
import '../../../services/security/pin_service.dart';
import '../../../utils/sizes.dart';
import '../user/change_pin/ResetPinUI.dart';

// check  = basic ( input untuk checking )
// ensure = double check pin ( reset pin )
// reset = just pass new pin and finger to hit endpoint updatePin
enum SecurityType { check, ensure, reset }

class SecurityPinUI extends StatefulWidget {
  final bool showBackButton;
  final SecurityType type;
  final Function(String pin, bool finger) onSuccess;
  final String title;
  @required
  SecurityPinUI({
    this.onSuccess,
    this.showBackButton = false,
    this.type,
    this.title,
  });

  @override
  _SecurityPinUIState createState() =>
      _SecurityPinUIState(title, type, showBackButton, onSuccess);
}

class _SecurityPinUIState extends State<SecurityPinUI> {
  SecurityType type;
  Function(String pin, bool finger) onSuccess;
  bool showBackButton;
  String title;
  _SecurityPinUIState(
      this.title, this.type, this.showBackButton, this.onSuccess);
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  final BiometricHelper _auth = BiometricHelper();
  final _pinService = PinService();
  // security type check ( ngecek apakah pin yang dimasukan sama dengan pin di api db)
  bool isNotMatch = false;
  bool isLoading = true;

  // Variables
  Size _screenSize;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fifthDigit;
  int _sixthDigit;
  String _currentPIN;

  void setLoading(bool value) {
    setState(() {
      isLoading = value;
    });
  }

  void fingerPrintDialog(BuildContext context) {
    Dialog dialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 250.0,
        width: 300.0,
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 250,
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.bottomCenter,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                  // color: Colors.blue[300],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                ),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.fingerprint,
                          size: 100,
                        ),
                        Container(
                          height: 10,
                        ),
                        Text(
                          "Pindai Sidik Jari",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(
                          height: 10,
                        ),
                        Text(
                          "Sentuh sensor sidik jari\nuntuk melanjutkan",
                          style: TextStyle(color: Colors.grey),
                          textAlign: TextAlign.center,
                        )
                      ],
                    )),
              ),
            ),
            isLoading
                ? Container()
                : Align(
                    alignment: Alignment(.9, -.9),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  // Santara header image
  get _santaraHeader {
    return Container(
      color: Color(ColorRev.mainBlack),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            // color: Colors.blueAccent,
            child: Image.asset(
              "assets/santara/1.png",
              width: 300,
              height: 150,
            ),
          )
        ],
      ),
    );
  }

  // Keterangan input
  get _getInputLabel {
    return Text(
      "Masukan pin Santara",
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16.0,
        color: isNotMatch ? Color(0xffBF2D30) : Color(ColorRev.mainwhite),
        fontWeight: FontWeight.w700,
      ),
    );
  }

  // Field input
  get _getInputField {
    return Container(
      color: Color(ColorRev.mainBlack),
      width: _screenSize.width - Sizes.s100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _pinTextField(_firstDigit),
          _pinTextField(_secondDigit),
          _pinTextField(_thirdDigit),
          _pinTextField(_fourthDigit),
          _pinTextField(_fifthDigit),
          _pinTextField(_sixthDigit),
        ],
      ),
    );
  }

  // Keterangan step
  get _stepInfo {
    return Text(
      "1 Dari 2",
      style: TextStyle(
        fontSize: 14.0,
        color: Colors.grey[600],
        fontWeight: FontWeight.w600,
      ),
    );
  }

  // Keterangan step jika PIN tidak matching ( security type check )
  get _stepError {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Lupa PIN ? ",
          style: TextStyle(
            fontSize: 14.0,
            color: Colors.grey[600],
            fontWeight: FontWeight.w600,
          ),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ResetPinUI(type: ResetPinType.forgot),
                ),
              );
            },
            child: Container(
              height: 40,
              child: Center(
                child: Text(
                  "Atur Ulang PIN",
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Color(0xff218196),
                    decoration: TextDecoration.underline,
                    decorationStyle: TextDecorationStyle.solid,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  // awokwokwok
  get _pembatas {
    return Container(
      height: 25,
    );
  }

  // PIN Body
  get _pinBody {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _pembatas,
        _santaraHeader,
        _getInputLabel,
        _getInputField,
        type == SecurityType.ensure
            ? _stepInfo
            : isNotMatch
                ? _stepError
                : Container(),
        _keyboardSection,
        _pembatas,
      ],
    );
  }

  // Keyboard UI
  get _keyboardSection {
    return Container(
        color: Color(ColorRev.mainBlack),
        height: _screenSize.width - 80,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _pinInputButton(
                      label: "1",
                      key: "pin1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _pinInputButton(
                      label: "2",
                      key: "pin2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _pinInputButton(
                      label: "3",
                      key: "pin3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _pinInputButton(
                      label: "4",
                      key: "pin4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _pinInputButton(
                      label: "5",
                      key: "pin5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _pinInputButton(
                      label: "6",
                      key: "pin6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _pinInputButton(
                      label: "7",
                      key: "pin7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _pinInputButton(
                      label: "8",
                      key: "pin8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _pinInputButton(
                      label: "9",
                      key: "pin9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(
                    width: 80.0,
                  ),
                  _pinInputButton(
                      label: "0",
                      key: "pin0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _pinActionButton(
                      label:
                          Icon(Icons.keyboard_backspace, color: Colors.white),
                      onPressed: () {
                        setState(() {
                          if (_sixthDigit != null) {
                            _sixthDigit = null;
                          } else if (_fifthDigit != null) {
                            _fifthDigit = null;
                          } else if (_fourthDigit != null) {
                            _fourthDigit = null;
                          } else if (_thirdDigit != null) {
                            _thirdDigit = null;
                          } else if (_secondDigit != null) {
                            _secondDigit = null;
                          } else if (_firstDigit != null) {
                            _firstDigit = null;
                          }
                        });
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  _isFPActive() async {
    // PopupHelper.showLoading(context);
    setLoading(true);
    try {
      await _pinService.getExistFP().then((value) {
        if (value.statusCode == 200) {
          var parsed = json.decode(value.body);
          if (parsed["message"]) {
            // Navigator.pop(context);
            setLoading(false);
            initializeLocalAuth();
          } else {
            setLoading(false);
            // Navigator.pop(context);
          }
        } else {
          setLoading(false);
          // Navigator.pop(context);
        }
      });
    } catch (e) {
      // print(">> ERR : $e");
      // Navigator.pop(context);
      setLoading(false);
    }
  }

  @override
  void initState() {
    UnauthorizeUsecase.check(context);
    super.initState();
    if (widget.type == SecurityType.ensure) {
      setLoading(false);
    }
    if (widget.type == SecurityType.reset) {
      setLoading(false);
    }
    if (widget.type != SecurityType.reset) {
      Future.delayed(Duration(milliseconds: 200), () {
        if (type == SecurityType.check) {
          // Check apakah fingerprint aktif atau tidak
          _isFPActive();
          // End
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(ColorRev.mainBlack),
        iconTheme: IconThemeData(color: Color(ColorRev.mainwhite)),
        actions: [
          showBackButton
              ? isLoading
                  ? Container()
                  : IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        final _authService = AuthService();
                        PopupHelper.popConfirmation(context, () async {
                          Navigator.pop(context);
                          setLoading(true);
                          Platform.isIOS
                              ? await _authService.logoutV2()
                              : await _authService.logout();
                          Navigator.pushReplacementNamed(context, '/index');
                        }, "Keluar",
                            "Apakah anda yakin ingin keluar dari akun?");
                      },
                    )
              : Container(),
        ],
      ),
      key: _key,
      backgroundColor: Color(ColorRev.mainBlack),
      body: isLoading
          ? Center(
              child: CupertinoActivityIndicator(),
            )
          : Container(
              color: Color(ColorRev.mainBlack),
              width: _screenSize.width,
              child: _pinBody,
            ),
    );
  }

  // PIN textfield
  Widget _pinTextField(int digit) {
    return Container(
      color: Color(ColorRev.mainBlack),
      width: 35.0,
      height: 45.0,
      alignment: Alignment.center,
      child: Container(
        width: 15,
        height: 15,
        decoration: BoxDecoration(
            color: digit != null ? Color(0xffBF2D30) : Color(0xffC4C4C4),
            shape: BoxShape.circle),
      ),
    );
  }

  // Pin keyboard input Button
  Widget _pinInputButton({String label, String key, VoidCallback onPressed}) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        key: Key(key),
        onTap: onPressed,
        borderRadius: BorderRadius.circular(40.0),
        child: Container(
          height: 80.0,
          width: 80.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  //
  _pinActionButton({Widget label, VoidCallback onPressed}) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.circular(40.0),
      child: Container(
        height: 80.0,
        width: 80.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Center(
          child: label,
        ),
      ),
    );
  }

  // callback dari halaman sebelumnya
  void callBack(String data) async {
    setLoading(true);
    // PopupHelper.showLoading(context);
    try {
      await onSuccess(data, false);
      setLoading(false);
      // Navigator.pop(context);
    } catch (e) {
      setLoading(false);
      // Navigator.pop(context);
    }
  }

  void initializeLocalAuth() async {
    try {
      bool isBiometricAvailable = await _auth.isBiometricAvailable();
      if (isBiometricAvailable) {
        bool authStatus = false;
        if (Platform.isIOS) {
          var availableBiometrics = await _auth.getListOfBiometricTypes();
          if (availableBiometrics.contains(BiometricType.face)) {
            // Face ID.
            ToastHelper.showBasicToast(
                context, "Authentication must be used Touch ID");
          } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
            // Touch ID.
            authStatus = await _auth.authenticateUser();
          }
        } else {
          authStatus = await _auth.authenticateUser();
        }

        if (authStatus) {
          await onSuccess("", authStatus);
        }
      }
    } catch (e) {
      // print(">> ERR : $e");
    }
  }

  checkPIN(String otp) async {
    // PopupHelper.showLoading(context);
    setLoading(true);
    try {
      await _pinService.verifyPIN(otp).then((res) async {
        var parsed = res != null ? json.decode(res.body) : [];
        if (res.statusCode == 200) {
          setState(() {
            title = "Berhasil mencocokan PIN";
            isNotMatch = false;
            isLoading = false;
          });
          clearOtp();
          // Navigator.pop(context);
          await onSuccess(otp, false);
        } else {
          setState(() {
            title = "Pin tidak cocok, coba lagi";
            isNotMatch = true;
            isLoading = false;
          });
          ToastHelper.showFailureToast(context, parsed["message"]);
          clearOtp();
        }
      });
    } catch (e) {
      // print(e);
      // Navigator.pop(context);
      setLoading(false);
    }
  }

  // Current digit
  void _setCurrentDigit(int i) {
    setState(() {
      _currentDigit = i;
      if (_firstDigit == null) {
        _firstDigit = _currentDigit;
      } else if (_secondDigit == null) {
        _secondDigit = _currentDigit;
      } else if (_thirdDigit == null) {
        _thirdDigit = _currentDigit;
      } else if (_fourthDigit == null) {
        _fourthDigit = _currentDigit;
      } else if (_fifthDigit == null) {
        _fifthDigit = _currentDigit;
      } else if (_sixthDigit == null) {
        _sixthDigit = _currentDigit;
        var otp =
            "$_firstDigit$_secondDigit$_thirdDigit$_fourthDigit$_fifthDigit$_sixthDigit";
        // Akses API Pencocokan PIN Disini, contoh :
        if (type == SecurityType.ensure) {
          // STEPS Jika step adalah ensure ( double check PIN )
          // 1. set value ke _currentPIN
          // 2. Clear semua input
          // 3. Mencocokan kembali antara PIN baru dengan _currentPIN
          // 4. Jika sama lanjut ke step selanjutnya
          if (_currentPIN == null) {
            setState(() {
              _currentPIN = otp;
              title = "Masukan kembali PIN Anda";
            });
            clearOtp();
          } else {
            if (otp != _currentPIN) {
              setState(() {
                title = "Pin tidak cocok, coba lagi";
                isNotMatch = true;
              });
              clearOtp();
            } else {
              setState(() {
                title = "Berhasil";
                isNotMatch = false;
              });
              clearOtp();
              callBack(otp);
            }
          }
        } else if (type == SecurityType.check) {
          checkPIN(otp);
          // Steps jika typenya check ( cek PIN dimasukan == PIN yang diset )
        } else if (type == SecurityType.reset) {
          clearOtp();
          callBack(otp);
        }
      }
    });
  }

  void clearOtp() {
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    _fifthDigit = null;
    _sixthDigit = null;
    setState(() {});
  }
}
