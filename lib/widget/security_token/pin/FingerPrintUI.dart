import 'dart:io';

import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/BiometricHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';

class FingerPrintUI extends StatefulWidget {
  final Function callback;
  FingerPrintUI({@required this.callback});

  @override
  _FingerPrintUIState createState() => _FingerPrintUIState(callback);
}

class _FingerPrintUIState extends State<FingerPrintUI> {
  final BiometricHelper _auth = BiometricHelper();
  Function callback;
  _FingerPrintUIState(this.callback);
  // get image header
  get _imageHeader {
    return Container(
      height: 150,
      // width: 200,
      child: Image.asset("assets/icon/Sidik.png"),
    );
  }

  // title label
  get _titleLabel {
    return Text(
      "Akses Sidik Jari",
      style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  // subtitle label
  get _subtitleLabel {
    return Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
      ),
      child: Text(
        "Gunakan sidik jari di perangkat Anda\nsebagai pengganti PIN untuk masuk ke\nakun dan melakukan transaksi\n\nMulai dengan meletakan jari di\nsidik jari perangkat Anda.",
        style: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  get _stepLabel {
    return Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
      ),
      child: Text(
        "2 Dari 2",
        style: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  initializeLocalAuth() async {
    bool isBiometricAvailable = await _auth.isBiometricAvailable();
    if (isBiometricAvailable) {
      bool authStatus = false;
      if (Platform.isIOS) {
        var availableBiometrics = await _auth.getListOfBiometricTypes();
        if (availableBiometrics.contains(BiometricType.face)) {
          // Face ID.
          ToastHelper.showBasicToast(
              context, "Authentication must be use Touch ID");
        } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
          // Touch ID.
          authStatus = await _auth.authenticateUser();
        }
      } else {
        authStatus = await _auth.authenticateUser();
      }
      if (authStatus) {
        ToastHelper.showBasicToast(
          context,
          "Selamat, berhasil mengautentikasi sidik jari anda!",
        );
        Future.delayed(Duration(seconds: 3), callback());
      }
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    initializeLocalAuth();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        ToastHelper.showBasicToast(
            context, "Tidak dapat kembali ke halaman sebelumnya!");
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10, right: 10),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: callback,
                  child: Container(
                    alignment: Alignment.center,
                    height: 25,
                    width: 80,
                    child: Text(
                      "Lewati",
                      style: TextStyle(
                        color: Color(0xff676767),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
        body: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _imageHeader,
              Container(
                height: 20,
              ),
              _titleLabel,
              Container(
                height: 10,
              ),
              _subtitleLabel,
              Container(
                height: 50,
              ),
              _stepLabel
            ],
          ),
        ),
      ),
    );
  }
}
