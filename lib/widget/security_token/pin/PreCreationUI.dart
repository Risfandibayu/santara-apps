import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/services/auth/auth_service.dart';
import 'package:santaraapp/services/security/pin_service.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'FingerPrintUI.dart';
import 'SecurityPinUI.dart';

class PreCreationUI extends StatefulWidget {
  final Function onCreate;
  PreCreationUI({this.onCreate});
  @override
  _PreCreationUIState createState() => _PreCreationUIState(onCreate);
}

class _PreCreationUIState extends State<PreCreationUI> {
  Function onCreate;
  _PreCreationUIState(this.onCreate);
  final _pinService = PinService();
  final _authService = AuthService();

  onSuccessCallback(String data) {
    PopupHelper.showLoading(context);
    try {
      _pinService.registerPIN(data).then((res) {
        var parsed = res != null ? json.decode(res.body) : [];
        if (res.statusCode == 200) {
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FingerPrintUI(
                callback: onCreate,
              ),
            ),
          );
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, parsed["message"]);
        }
      });
    } catch (e) {
      // print(">> Err : $e");
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat mengirimkan data!");
    }
  }

  // get image header
  get _imageHeader {
    return Container(
      height: 200,
      // width: 200,
      child: Image.asset("assets/icon/security_alert.png"),
    );
  }

  // title label
  get _titleLabel {
    return Text(
      "Tingkatkan Keamanan Akun",
      style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  // subtitle label
  get _subtitleLabel {
    return Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
      ),
      child: Text(
        "Tingkatkan keamanan akun Santara\nAnda dengan membuat PIN",
        style: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  // button
  get _santaraButton {
    return SantaraMainButton(
      title: "Buat PIN",
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SecurityPinUI(
              onSuccess: (pin, finger) {
                onSuccessCallback(pin);
              },
              type: SecurityType.ensure,
              title: "Masukan PIN Anda",
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        ToastHelper.showBasicToast(context, "PIN Wajib Dibuat!");
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
              icon: Icon(
                Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () async {
                PopupHelper.showLoading(context);
                var value = Platform.isAndroid
                    ? await _authService.logoutV2()
                    : await _authService.logoutV2();

                if (value) {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                      (Route<dynamic> route) => false);
                } else {
                  Navigator.pop(context);
                  ToastHelper.showFailureToast(
                      context, "Terjadi kesalahan saat keluar!");
                }
              }),
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.only(left: 20, right: 20),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _imageHeader,
              Container(
                height: 20,
              ),
              _titleLabel,
              Container(
                height: 10,
              ),
              _subtitleLabel,
              Container(
                height: 50,
              ),
              _santaraButton
            ],
          ),
        ),
      ),
    );
  }
}
