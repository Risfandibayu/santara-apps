import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/ToastHelper.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../pin/SecurityPinUI.dart';

enum OtpType { password, phone, email, pin }

class OtpPhoneUI extends StatefulWidget {
  final OtpType type;
  OtpPhoneUI({@required this.type});
  @override
  _OtpPhoneUIState createState() => _OtpPhoneUIState(type);
}

class _OtpPhoneUIState extends State<OtpPhoneUI> {
  OtpType type;
  _OtpPhoneUIState(this.type);

  final c1 = TextEditingController();
  final c2 = TextEditingController();
  final c3 = TextEditingController();
  final c4 = TextEditingController();

  final f1 = FocusNode();
  final f2 = FocusNode();
  final f3 = FocusNode();
  final f4 = FocusNode();

  // 0 = ga error
  // 1 = OTP Tidak cocok
  // 2 = Batas maksimal percobaan terlampaui ( 3x max salah input OTP )

  int _errType = 0;
  int _errCount = 0;

  String maskEmail(String email) {
    var separated = email.split("@");
    int mailLength = separated[0].length;
    var show = (mailLength / 2).floor();
    var hide = mailLength - show;
    var replace = ("*" * hide);
    return mailLength > 0
        ? separated[0].replaceRange(1, mailLength, replace) + "@${separated[1]}"
        : "*@${separated[1]}";
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  clearOTP() {
    c1.clear();
    c2.clear();
    c3.clear();
    c4.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/icon/verification.png",
              height: 150,
            ),
            Container(
              height: 20,
            ),
            Text(
              "Masukan kode verifikasi",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18,
              ),
            ),
            Container(
              height: 20,
            ),
            Text(
              "Untuk keamanan kami telah mengirim kode\nverifikasi melalui SMS ke nomor\n08123***123",
              style: TextStyle(
                fontSize: 15,
              ),
              textAlign: TextAlign.center,
            ),
            Container(
              height: 100,
              width: 250,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  OtpField(
                    controller: c1,
                    focusNode: f1,
                    isError: _errType != 0 ? true : false,
                    onChanged: (String str) {
                      if (str.length == 1) {
                        f1.unfocus();
                        FocusScope.of(context).requestFocus(f2);
                      } else if (str.length == 0) {
                        f1.unfocus();
                        FocusScope.of(context).requestFocus(f1);
                      }
                    },
                  ),
                  OtpField(
                    controller: c2,
                    focusNode: f2,
                    isError: _errType != 0 ? true : false,
                    onChanged: (String str) {
                      if (str.length == 1) {
                        f2.unfocus();
                        FocusScope.of(context).requestFocus(f3);
                      } else if (str.length == 0) {
                        f2.unfocus();
                        FocusScope.of(context).requestFocus(f1);
                      }
                    },
                  ),
                  OtpField(
                    controller: c3,
                    focusNode: f3,
                    isError: _errType != 0 ? true : false,
                    onChanged: (String str) {
                      if (str.length == 1) {
                        f3.unfocus();
                        FocusScope.of(context).requestFocus(f4);
                      } else if (str.length == 0) {
                        f3.unfocus();
                        FocusScope.of(context).requestFocus(f4);
                      }
                    },
                  ),
                  OtpField(
                    controller: c4,
                    focusNode: f4,
                    isError: _errType != 0 ? true : false,
                    onChanged: (String str) {
                      if (str.length == 1) {
                        f4.unfocus();
                      } else if (str.length == 0) {
                        f4.unfocus();
                        FocusScope.of(context).requestFocus(f3);
                      }
                    },
                  ),
                ],
              ),
            ),
            Container(
              height: 5,
            ),
            _errType == 1
                ? Text(
                    "Kode tidak cocok. Coba lagi.",
                    style: TextStyle(
                      color: Color(0xffBF2D30),
                    ),
                  )
                : _errType == 2
                    ? Text(
                        "Batas maksimal percobaan.",
                        style: TextStyle(
                          color: Color(0xffBF2D30),
                        ),
                      )
                    : Container(),
            Center(
              child: CountdownFormatted(
                duration: Duration(minutes: 5),
                onFinish: () {
                  // print("Udah ya");
                },
                builder: (BuildContext ctx, String remaining) {
                  return Text(remaining); // 01:00:00
                },
              ),
            ),
            Container(
              height: 20,
            ),
            SantaraPickerButton(
              title: "Verifikasi",
              onPressed: () async {
                String otp = c1.text + c2.text + c3.text + c4.text;
                if (otp.isEmpty) {
                  ToastHelper.showBasicToast(context, "OTP tidak boleh kosong");
                } else {
                  // print(">> OTP : $otp");
                  PopupHelper.showLoading(context);
                  await Future.delayed(
                      Duration(seconds: 2), () => Navigator.pop(context));
                  if (otp == "1234") {
                    switch (type) {
                      case OtpType.password:
                        Navigator.pushNamed(context, '/change_password');
                        break;
                      case OtpType.email:
                        Navigator.pushNamed(context, '/change_email');
                        break;
                      case OtpType.phone:
                        dialogChangePhoneSuccess();
                        break;
                      default:
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SecurityPinUI(
                              type: SecurityType.ensure,
                              title: "Masukan PIN Anda",
                              onSuccess: (pin, finger) {
                                Navigator.pushReplacementNamed(
                                    context, '/index');
                              },
                            ),
                          ),
                        );
                        break;
                    }
                  } else {
                    setState(() {
                      _errCount += 1;
                      _errCount >= 3 ? _errType = 2 : _errType = 1;
                    });
                    clearOTP();
                  }
                }
              },
            ),
            Container(
              height: 35,
            ),
            Container(
              width: MediaQuery.of(context).size.width - 150,
              child: RichText(
                text: TextSpan(
                  text: _errType == 2
                      ? "Anda sudah melewati batas maksimal percobaan. "
                      : "Tidak menerima kode? ",
                  style: TextStyle(color: Color(0xff676767)),
                  children: <TextSpan>[
                    TextSpan(
                      text: "Kirim Ulang SMS",
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: Colors.blue,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () async {
                          PopupHelper.showLoading(context);
                          await Future.delayed(Duration(seconds: 2), () {
                            Navigator.pop(context);
                            ToastHelper.showBasicToast(
                              context,
                              "OTP berhasil dikirim, cek kembali kotak masuk SMS anda!",
                            );
                          });
                        },
                    )
                  ],
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void dialogChangePhoneSuccess() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset("assets/icon/success.png"),
                Container(height: 20),
                Text(
                  "Perubahan Nomor Handphone Berhasil",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(height: 20),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                        child: Text("Masuk",
                            style:
                                TextStyle(color: Colors.white, fontSize: 15))),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

class OtpField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final ValueChanged<String> onChanged;
  final bool isError;

  @required
  OtpField({
    this.controller,
    this.focusNode,
    this.onChanged,
    this.isError = false,
  });

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: TextFormField(
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          controller: controller,
          focusNode: focusNode,
          decoration: InputDecoration(
            border: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 8,
                color: isError ? Color(0xffBF2D30) : Colors.black,
              ),
            ),
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
            WhitelistingTextInputFormatter.digitsOnly
          ],
          onChanged: onChanged,
        ),
      ),
    );
  }
}
