import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:shimmer/shimmer.dart';

import '../../../core/usecases/unauthorize_usecase.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/ToastHelper.dart';
import '../../../models/User.dart';
import '../../../services/security/pin_service.dart';
import '../../../services/time/time_services.dart';
import '../../../utils/logger.dart';
import '../../widget/components/main/SantaraButtons.dart';
import '../../widget/components/main/SantaraCountdownTimer.dart';

class OtpViewUI extends StatefulWidget {
  final Function onSuccess;
  final User user;
  OtpViewUI({@required this.user, @required this.onSuccess});
  @override
  _OtpViewUIState createState() => _OtpViewUIState(user, onSuccess);
}

class _OtpViewUIState extends State<OtpViewUI> {
  User user;
  Function onSuccess;
  int secs;
  bool _hasResend = false;

  _OtpViewUIState(this.user, this.onSuccess);
  final _timeService = TimeService();
  final _pinService = PinService();
  DateTime today = DateTime.now();
  final c1 = TextEditingController();
  final c2 = TextEditingController();
  final c3 = TextEditingController();
  final c4 = TextEditingController();

  final f1 = FocusNode();
  final f2 = FocusNode();
  final f3 = FocusNode();
  final f4 = FocusNode();

  // 0 = ga error
  // 1 = OTP Tidak cocok
  // 2 = Batas maksimal percobaan terlampaui ( 3x max salah input OTP )

  int _errType = 0;
  int _errCount = 0;
  bool _allowResend = false;
  Widget _timer = Container();

  onSuccessCallback(String data) {
    try {
      _pinService.registerPIN(data).then((res) {
        var parsed = res != null ? json.decode(res.body) : [];
        if (res.statusCode == 200) {
          Navigator.pushReplacementNamed(context, '/index');
        } else {
          ToastHelper.showFailureToast(context, parsed["message"]);
        }
      });
    } catch (e) {
      // print(">> Err : $e");
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat mengirimkan data!");
    }
  }

  String maskEmail(String email) {
    var separated = email.split("@");
    int mailLength = separated[0].length;
    var show = (mailLength / 2).floor();
    var hide = mailLength - show;
    var replace = ("*" * hide);
    // print(mailLength);
    return mailLength > 0
        ? separated[0].replaceRange(1, mailLength, replace) + "@${separated[1]}"
        : "*@${separated[1]}";
  }

  String maskPhone(String phone) {
    int mailLength = phone.length;
    var show = (mailLength / 2).floor();
    var hide = mailLength - show;
    var replace = ("*" * hide);
    return mailLength > 0
        ? phone.replaceRange(show, mailLength - 2, replace)
        : "$phone";
  }

  resendOTP() async {
    if (_allowResend) {
      PopupHelper.showLoading(context);
      try {
        await _pinService.resendOTP().then((res) async {
          var msg = {"message": "Error"};
          var parsed = msg;
          if (res.statusCode == 200) {
            setState(() {
              secs = null;
              _allowResend = false;
            });
            await getSecondsTime();
            var response = res != null ? json.decode(res.body) : msg;
            parsed = response[0];
            await Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => OtpViewUI(
                  user: user,
                  onSuccess: () {
                    onSuccess();
                  },
                ),
              ),
            );
          } else {
            // print(">> Error..");
            final result = json.decode(res.body);
            parsed = {"message": "${result['message']}"};
            Navigator.pop(context);
          }
          ToastHelper.showBasicToast(context, parsed["message"]);
        });
      } catch (e) {
        Navigator.pop(context);
      }
    } else {
      ToastHelper.showFailureToast(context, "Mohon tunggu waktu habis!");
    }
  }

  getSecondsTime() async {
    try {
      await _timeService.otpExpiredTimeInSecond(false).then((value) {
        setState(() {
          _timer = Container();
          secs = value < 1 ? 1 : value;
          _timer = SantaraCountdownTimer(
              timerMaxSeconds: secs,
              onFinish: () {
                setState(() {
                  _allowResend = true;
                });
              });
        });
      });
    } catch (e) {
      // print(e);
      // print(stack);
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getSecondsTime();
  }

  @override
  void dispose() {
    super.dispose();
  }

  clearOTP() {
    c1.clear();
    c2.clear();
    c3.clear();
    c4.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Color(ColorRev.mainBlack),
        padding: EdgeInsets.all(15),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/icon/verification.png",
                  height: 150,
                ),
                Container(
                  height: 20,
                ),
                Text(
                  "Masukan Kode",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                      color: Colors.white),
                ),
                Container(
                  height: 20,
                ),
                Text(
                  "Kami telah mengirimkan kode ke\n${maskPhone(user.phone)}",
                  style: TextStyle(fontSize: 15, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: 100,
                  width: 250,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      OtpField(
                        controller: c1,
                        focusNode: f1,
                        isError: _errType != 0 ? true : false,
                        onChanged: (String str) {
                          if (str.length == 1) {
                            f1.unfocus();
                            FocusScope.of(context).requestFocus(f2);
                          } else if (str.length == 0) {
                            f1.unfocus();
                            FocusScope.of(context).requestFocus(f1);
                          }
                        },
                      ),
                      OtpField(
                        controller: c2,
                        focusNode: f2,
                        isError: _errType != 0 ? true : false,
                        onChanged: (String str) {
                          if (str.length == 1) {
                            f2.unfocus();
                            FocusScope.of(context).requestFocus(f3);
                          } else if (str.length == 0) {
                            f2.unfocus();
                            FocusScope.of(context).requestFocus(f1);
                          }
                        },
                      ),
                      OtpField(
                        controller: c3,
                        focusNode: f3,
                        isError: _errType != 0 ? true : false,
                        onChanged: (String str) {
                          if (str.length == 1) {
                            f3.unfocus();
                            FocusScope.of(context).requestFocus(f4);
                          } else if (str.length == 0) {
                            f3.unfocus();
                            FocusScope.of(context).requestFocus(f4);
                          }
                        },
                      ),
                      OtpField(
                        controller: c4,
                        focusNode: f4,
                        isError: _errType != 0 ? true : false,
                        onChanged: (String str) {
                          if (str.length == 1) {
                            f4.unfocus();
                          } else if (str.length == 0) {
                            f4.unfocus();
                            FocusScope.of(context).requestFocus(f3);
                          }
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 5,
                ),
                _errType == 1
                    ? Text(
                        "Kode tidak cocok. Coba lagi.",
                        style: TextStyle(
                          color: Color(0xffBF2D30),
                        ),
                      )
                    : _errType == 2
                        ? Text(
                            "Batas maksimal percobaan.",
                            style: TextStyle(
                              color: Color(0xffBF2D30),
                            ),
                          )
                        : Container(),
                Center(
                  child: secs == null
                      ? Container(
                          padding: EdgeInsets.fromLTRB(0, 16, 0, 30),
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[300],
                            highlightColor: Colors.white,
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              child: Container(
                                height: 30,
                                width: 100,
                                child: Image.asset(
                                  'arfa.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )
                      : _hasResend
                          ? Text(
                              "Kode OTP telah terkirim, mohon tunggu 2 menit lagi untuk mengirim kembali!",
                              style: TextStyle(color: Colors.white),
                            )
                          : _timer,
                ),
                Container(
                  height: 20,
                ),
                SantaraMainButton(
                  title: "Selanjutnya",
                  onPressed: () async {
                    String otp = c1.text + c2.text + c3.text + c4.text;
                    if (otp.isEmpty) {
                      ToastHelper.showBasicToast(
                        context,
                        "OTP tidak boleh kosong",
                      );
                    } else {
                      PopupHelper.showLoading(context);
                      try {
                        await _pinService.verifyOTP(otp).then((res) {
                          // logger.i("""
                          // >> Res : ${res.body}
                          // """);
                          var parsed = res != null ? json.decode(res.body) : [];
                          if (res.statusCode == 200) {
                            Navigator.pop(context);
                            onSuccess();
                          } else {
                            Navigator.pop(context);
                            ToastHelper.showFailureToast(
                              context,
                              parsed["message"],
                            );
                            setState(() {
                              _errCount += 1;
                              _errCount >= 3 ? _errType = 2 : _errType = 1;
                            });
                            clearOTP();
                          }
                        });
                      } catch (e, stack) {
                        santaraLog(e, stack);
                        // print(">> ERR : $e");
                        Navigator.pop(context);
                      }
                    }
                  },
                ),
                Container(
                  height: 35,
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 150,
                  child: RichText(
                    text: TextSpan(
                      text: _errType == 2
                          ? "Anda sudah melewati batas maksimal percobaan. "
                          : "Tidak menerima SMS? ",
                      style: TextStyle(color: Color(ColorRev.mainwhite)),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Kirim Ulang SMS",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                            decorationStyle: TextDecorationStyle.solid,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              await resendOTP();
                            },
                        )
                      ],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class OtpField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final ValueChanged<String> onChanged;
  final bool isError;

  @required
  OtpField({
    this.controller,
    this.focusNode,
    this.onChanged,
    this.isError = false,
  });

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: TextFormField(
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          controller: controller,
          focusNode: focusNode,
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 8,
                color: isError ? Color(0xffBF2D30) : Colors.white,
              ),
            ),
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
            WhitelistingTextInputFormatter.digitsOnly
          ],
          onChanged: onChanged,
        ),
      ),
    );
  }
}
