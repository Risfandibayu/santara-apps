import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/widget/security_token/otp/OtpPhoneUI.dart';
import 'package:santaraapp/widget/security_token/user/change_phone/changephone.bloc.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';

class ChangePhoneUI extends StatefulWidget {
  final OtpType otpType;
  final String pin;
  final bool finger;
  ChangePhoneUI({this.otpType, this.pin, @required this.finger});
  @override
  _ChangePhoneUIState createState() => _ChangePhoneUIState();
}

class _ChangePhoneUIState extends State<ChangePhoneUI> {
  CountryCode countryCode = new CountryCode(dialCode: "+62", code: "ID");
  final TextEditingController no = new TextEditingController();
  int isRightPhone = 0;
  final _bloc = ChangePhoneBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/icon/verification.png",
                height: 150,
              ),
              Container(
                height: 20,
              ),
              Text(
                "Atur Ulang Nomor Handphone",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
              Container(
                height: 5,
              ),
              Text(
                "Masukkan nomor handphone yang baru",
                style: TextStyle(
                  fontSize: 15,
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                height: 30,
              ),
              _phone(),
              Container(
                height: 40,
              ),
              Container(
                height: 50,
                child: SantaraPickerButton(
                  title: "Kirim",
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _bloc.submitPhone(context, countryCode.dialCode, widget.pin,
                        widget.finger);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _phone() {
    return Container(
        // margin: EdgeInsets.fromLTRB(0.0, 6.0, 30.0, 6.0),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(4.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 8),
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(4.0)),
                  child: CountryCodePicker(
                    onChanged: (value) {
                      setState(() => countryCode = value);
                    },
                    initialSelection: 'ID',
                  ),
                ),
                Flexible(
                  child: StreamBuilder(
                    stream: _bloc.phone,
                    builder: (context, snapshot) {
                      return TextFormField(
                        controller: no,
                        keyboardType: TextInputType.phone,
                        onChanged: _bloc.changePhone,
                        autofocus: false,
                        decoration: InputDecoration(
                          hintText: 'Contoh: 812xxxxx',
                          border: InputBorder.none,
                          errorText: snapshot.error,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
