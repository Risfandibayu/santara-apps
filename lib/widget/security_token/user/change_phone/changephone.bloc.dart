import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/security/phone_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/security_token/otp/OtpViewUI.dart';
import 'package:santaraapp/widget/security_token/user/SuccessChangeUI.dart';
import 'package:http/http.dart' as http;

class ChangePhoneBloc {
  final _apiService = PhoneApiService();
  // handling validasi password
  final validatePhone =
      StreamTransformer<String, String>.fromHandlers(handleData: (phone, sink) {
    if (phone.isEmpty) {
      sink.addError('Masukkan nomor telepon anda!');
    } else {
      String first = phone[0];
      if (first == "0") {
        sink.addError('Nomor tidak boleh diawali dengan 0');
      } else if (!RegExp(r"^[0-9]*$").hasMatch(phone)) {
        sink.addError('Masukkan inputan tanpa spesial karakter!');
      } else if (phone.length < 8) {
        sink.addError('No telepon kurang dari 8 digit!');
      } else {
        sink.add(phone);
      }
    }
  });

  // define vars
  final _phone = BehaviorSubject<String>(); // password

  // handling streams
  Stream<String> get phone => _phone.stream.transform(validatePhone);

  // Handle data change ( for specific stream )
  Function(String) get changePhone => _phone.sink.add;

  // get user data
  Future<User> getUserData() async {
    final storage = FlutterSecureStorage();
    User user = await Constants.getUserData();
    var token = await storage.read(key: 'token');
    final http.Response response = await http
        .get('$apiLocal/users/by-uuid/${user.uuid}', headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token'
    }).timeout(Duration(seconds: 15));
    if (response.statusCode == 200) {
      final data = userFromJson(response.body);
      return data;
    } else {
      return user;
    }
  }

  // hit api reset password
  submitPhone(
      BuildContext context, String dialPhone, String pin, bool finger) async {
    String newPhone = dialPhone + _phone.value;
    // print(">> HP BARU : $newPhone");
    PopupHelper.showLoading(context);
    try {
      final storage = FlutterSecureStorage();
      User datas;
      datas = userFromJson(await storage.read(key: 'user'));
      // cek nomor telepon
      User userData = await getUserData();
      // print(">> New Phone : $newPhone");
      // print(">> Current Phone : ${userData.phone}");
      if (userData.phone == newPhone) {
        Navigator.pop(context);
        ToastHelper.showFailureToast(
          context,
          "Nomor telepon harus berbeda dengan nomor sebelumnya!",
        );
      } else {
        await _apiService.requestResetPhone(newPhone).then((value) {
          if (value.statusCode == 200) {
            Navigator.pop(context);
            datas.phone = "$newPhone";
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => OtpViewUI(
                  user: datas,
                  onSuccess: () async {
                    PopupHelper.showLoading(context);
                    try {
                      await _apiService
                          .resetPhone(dialPhone + _phone.value, pin, finger)
                          .then((value) {
                        if (value.statusCode == 200) {
                          Navigator.pop(context);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SuccessChangeUI(
                                message: "Perubahan nomor handphone berhasil!",
                              ),
                            ),
                          );
                        } else {
                          var parsed = json.decode(value.body);
                          Navigator.pop(context);
                          ToastHelper.showFailureToast(
                              context, parsed["message"]);
                        }
                      });
                    } catch (e) {
                      // print(e);
                      Navigator.pop(context);
                      ToastHelper.showFailureToast(
                          context, "TIdak dapat mengirim data!");
                    }
                  },
                ),
              ),
            );
          } else {
            // print(value.body);
            var parsed = json.decode(value.body);
            Navigator.pop(context);
            ToastHelper.showFailureToast(context, parsed["message"]);
          }
        });
      }
    } catch (e) {
      // print(e);
      Navigator.pop(context);
      ToastHelper.showFailureToast(context, "Tidak dapat mengirim data!");
    }
  }

  // cleaning stream
  dispose() {
    _phone.close();
  }
}
