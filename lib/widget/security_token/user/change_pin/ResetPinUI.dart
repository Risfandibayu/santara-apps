import 'dart:io';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/Observer.dart';
import 'package:santaraapp/widget/widget/components/listing/ListingListContent.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBar.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';

import 'resetpin.bloc.dart';

enum ResetPinType { change, forgot }

class ResetPinUI extends StatefulWidget {
  final ResetPinType type;

  const ResetPinUI({Key key, @required this.type}) : super(key: key);
  @override
  _ResetPinUIState createState() => _ResetPinUIState();
}

class _ResetPinUIState extends State<ResetPinUI> {
  final _bloc = ResetPinBloc();

  get appBar {
    return PreferredSize(
        child: SantaraAppBar(
            leading: IconButton(
                icon: Icon(Platform.isAndroid
                    ? Icons.arrow_back
                    : Icons.arrow_back_ios),
                onPressed: () => Navigator.pop(context))),
        preferredSize: Size.fromHeight(60));
  }

  void _listener() {
    _bloc.state.listen((data) {
      switch (data) {
        case ResetPinState.sending:
          PopupHelper.showLoading(context);
          break;
        case ResetPinState.failed:
          Navigator.pop(context);
          break;
        default:
          null;
          break;
      }
    }, onError: (err) {
      ToastHelper.showFailureToast(context, err);
    });
  }

  @override
  void initState() {
    super.initState();
    _bloc.getUserData();
    _listener();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        color: Color(ColorRev.mainBlack),
        child: ListView(
          children: <Widget>[
            Container(
              height: 15,
            ),
            Observer<User>(
              stream: _bloc.user,
              onSuccess: (context, data) {
                return ListTile(
                  contentPadding: EdgeInsets.all(0),
                  leading: data.trader.photo == null
                      ? CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(Icons.person),
                        )
                      : ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: SantaraCachedImage(
                            height: Sizes.s50,
                            width: Sizes.s50,
                            image: GetAuthenticatedFile.convertUrl(
                              image: data.trader.photo,
                              type: PathType.traderPhoto,
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                  title: Text("${data.trader.name}",
                      style: TextStyle(color: Colors.white)),
                  subtitle: Text("${data.email}",
                      style: TextStyle(color: Colors.white)),
                );
              },
              onWaiting: (context) {
                return ListTile(
                  contentPadding: EdgeInsets.all(0),
                  leading: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(
                        30,
                      ),
                    ),
                    child: ListingLoaderSegment(
                      height: 50,
                      width: 50,
                    ),
                  ),
                  title: ListingLoaderSegment(
                    height: 12,
                    width: double.maxFinite,
                  ),
                  subtitle: Container(
                    margin: EdgeInsets.only(top: 10),
                    width: 80,
                    child: ListingLoaderSegment(
                      height: 10,
                      width: 80,
                    ),
                  ),
                );
              },
              onError: (context, err) => Text("Tidak dapat mengambil data!"),
            ),
            Container(
              height: 40,
            ),
            Text(
              "Untuk Melanjutkan, Masukan Password",
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.w700,
              ),
            ),
            Container(
              height: 20,
            ),
            Container(
              height: 50,
              child: StreamBuilder(
                stream: _bloc.password,
                builder: (context, snapshot) {
                  return SantaraTextField(
                    onChanged: _bloc.changePassword,
                    errorText: snapshot.error,
                    obscureText: true,
                    hintText: "Password",
                    labelText: "Password",
                  );
                },
              ),
            ),
            Container(
              height: 20,
            ),
            Container(
              height: 50,
              child: SantaraPickerButton(
                title: "Selanjutnya",
                onPressed: () {
                  // FocusScope.of(context).requestFocus(FocusNode());
                  _bloc.submit(context, widget.type);
                },
              ),
            ),
            Container(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
