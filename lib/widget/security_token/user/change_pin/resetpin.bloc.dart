import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/services/security/pin_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/security_token/otp/OtpViewUI.dart';
import 'package:santaraapp/widget/security_token/pin/SecurityPinUI.dart';
import 'package:santaraapp/widget/security_token/user/change_pin/ResetPinUI.dart';
import '../SuccessChangeUI.dart';

enum ResetPinState { sending, success, failed, uninitialized }

class ResetPinBloc {
  final storage = new FlutterSecureStorage();
  BehaviorSubject<User> _user = BehaviorSubject<User>();
  Stream<User> get user => _user.stream;
  final _pinService = PinService();
  // Validators
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length > 0) {
      sink.add(password);
    } else {
      sink.addError('Password tidak boleh kosong!');
    }
  });

  // Password controller
  final _passwordController = BehaviorSubject<String>();
  Function(String) get changePassword => _passwordController.sink.add;
  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);
  final _state =
      BehaviorSubject<ResetPinState>.seeded(ResetPinState.uninitialized);
  // is valid
  Stream<bool> get submitValid => Rx.combineLatest([password], (value) => true);
  Stream<ResetPinState> get state => _state.stream;

  getUserData() async {
    try {
      var token = await storage.read(key: 'token');
      var uuid = await storage.read(key: 'uuid');
      final http.Response response = await http.get(
          '$apiLocal/users/by-uuid/$uuid',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      if (response.statusCode == 200) {
        final data = userFromJson(response.body);
        _user.sink.add(data);
      } else {
        _user.sink.addError("Tidak dapat mengambil data!");
      }
    } catch (e) {
      _user.sink.addError(e);
    }
  }

  submit(BuildContext context, ResetPinType type) async {
    final password =
        _passwordController.value != null ? _passwordController.value : "";
    if (password.length > 0) {
      _state.sink.add(ResetPinState.sending);
      try {
        await _pinService.requestResetPIN(password).then((res) {
          var msg = {"message": "Error"};
          var parsed = res != null ? json.decode(res.body) : msg;
          // print(parsed.toString());
          if (res.statusCode == 200) {
            // print(">> OK");
            _state.sink.add(ResetPinState.success);
            Navigator.pop(context);
            if (type == ResetPinType.change) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OtpViewUI(
                    user: _user.value,
                    onSuccess: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SecurityPinUI(
                              type: SecurityType.check,
                              title: "Masukan PIN Lama Anda",
                              onSuccess: (oldPin, finger) async {
                                Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SecurityPinUI(
                                      type: SecurityType.reset,
                                      title: "Masukan PIN Baru Anda",
                                      onSuccess: (newPin, finger) async {
                                        PopupHelper.showLoading(context);
                                        try {
                                          await _pinService
                                              .updatePIN(oldPin, newPin, finger)
                                              .then((res) {
                                            var parsed = res != null
                                                ? json.decode(res.body)
                                                : [];
                                            if (res.statusCode == 200) {
                                              // print(">> SHOULD BE HERE!");
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      SuccessChangeUI(
                                                    message:
                                                        "PIN Berhasil Diubah",
                                                  ),
                                                ),
                                              );
                                              // PopupHelper.successChangeDialog(context);
                                              // PopupHelper.successChangeDialog(context);
                                            } else {
                                              // print(">> NOT OK ");
                                              Navigator.pop(context);
                                              ToastHelper.showFailureToast(
                                                  context, parsed["message"]);
                                            }
                                          });
                                        } catch (e, stack) {
                                          santaraLog(e, stack);
                                          // print(">> Err : $e");
                                          Navigator.pop(context);
                                          ToastHelper.showFailureToast(context,
                                              "Terjadi kesalahan saat mengirimkan data!");
                                        }
                                      },
                                    ),
                                  ),
                                );
                              }),
                        ),
                      );
                    },
                  ),
                ),
              );
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OtpViewUI(
                    user: _user.value,
                    onSuccess: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SecurityPinUI(
                            type: SecurityType.ensure,
                            title: "Masukan PIN Anda",
                            onSuccess: (pin, finger) async {
                              PopupHelper.showLoading(context);
                              try {
                                await _pinService.registerPIN(pin).then((res) {
                                  var parsed =
                                      res != null ? json.decode(res.body) : [];
                                  if (res.statusCode == 200) {
                                    // print(">> SHOULD BE HERE!");
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SuccessChangeUI(
                                          message: "PIN Berhasil Diubah",
                                        ),
                                      ),
                                    );
                                    // PopupHelper.successChangeDialog(context);
                                    // PopupHelper.successChangeDialog(context);
                                  } else {
                                    // print(">> NOT OK ");
                                    Navigator.pop(context);
                                    ToastHelper.showFailureToast(
                                        context, parsed["message"]);
                                  }
                                });
                              } catch (e, stack) {
                                santaraLog(e, stack);
                                // print(">> Err : $e");
                                Navigator.pop(context);
                                ToastHelper.showFailureToast(context,
                                    "Terjadi kesalahan saat mengirimkan data!");
                              }
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ),
              );
            }
          } else {
            _state.sink.add(ResetPinState.failed);
            _state.sink.addError(parsed["message"]);
          }
        });
      } catch (e, stack) {
        santaraLog(e, stack);
        // print(">>ERR : $e");
        _state.sink.add(ResetPinState.failed);
        _state.sink.addError("Tidak dapat mengirimkan data!");
      }
    } else {
      _state.sink.addError("Mohon cek kembali form anda!");
    }
  }

  submitOld(BuildContext context) async {
    final password = _passwordController.value;
    try {
      PopupHelper.showLoading(context);
      await _pinService.requestResetPIN(password).then((res) {
        var parsed = res != null ? json.decode(res.body) : [];
        if (res.statusCode == 200) {
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OtpViewUI(
                user: _user.value,
                onSuccess: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecurityPinUI(
                        type: SecurityType.ensure,
                        title: "Masukan PIN Anda",
                        onSuccess: (pin, finger) async {
                          PopupHelper.showLoading(context);
                          try {
                            _pinService.registerPIN(pin).then((res) {
                              var parsed =
                                  res != null ? json.decode(res.body) : [];
                              if (res.statusCode == 200) {
                                Navigator.pop(context);
                                PopupHelper.successChangeDialog(context);
                              } else {
                                Navigator.pop(context);
                                ToastHelper.showFailureToast(
                                    context, parsed["message"]);
                              }
                            });
                          } catch (e) {
                            Navigator.pop(context);
                            ToastHelper.showFailureToast(context,
                                "Terjadi kesalahan saat mengirimkan data!");
                          }
                        },
                      ),
                    ),
                  );
                },
              ),
            ),
          );
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, parsed["message"]);
        }
      });
    } catch (e) {
      Navigator.pop(context);
      ToastHelper.showFailureToast(context, "Tidak dapat mengirim data!");
    }
  }

  dispose() {
    _user.close();
    _passwordController.close();
  }
}
