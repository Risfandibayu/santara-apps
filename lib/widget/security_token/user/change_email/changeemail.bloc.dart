import 'dart:async';
import 'dart:convert';

import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/services/security/email_service.dart';

enum ResetEmailState { loading, loaded }

class ChangeEmailBloc {
  final _emailService = EmailApiService();

  // handling validasi email
  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(email)) {
      sink.addError('Email anda tidak valid, coba lagi');
    } else if (email.length <= 0) {
      sink.addError('Password minimal 8 karakter!');
    } else {
      sink.add(email);
    }
  });

  final _email = BehaviorSubject<String>();
  final _isSubmit = BehaviorSubject<int>.seeded(0); // status submit

  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<int> get isSubmit => _isSubmit.stream;

  Function(String) get changeEmail => _email.sink.add;

  submitEmail(String pin, bool finger) async {
    // Keterangan is submit
    // 0 = Ga ngapa ngapain
    // 1 = Submitting
    // 2 = Error submit
    // 3 = Success submit
    // *NEXT Dibuat jadi class sendiri ( buat state terpisah )
    final mail = _email.value;

    _isSubmit.sink.add(1);
    try {
      await _emailService.updateEmail(mail, pin, finger).then((value) {
        if (value.statusCode == 200) {
          // Jika submit email berhasil
          _isSubmit.sink.add(3);
        } else {
          // Jika submit password gagal
          _isSubmit.sink.add(2);
          if (value.statusCode == 500) {
            // Default error jika statusCode : 500 (Internal Server Error)
            _isSubmit.sink.addError("Tidak dapat menghubungkan!");
          } else {
            // Parse error value
            var parsed = value != null ? json.decode(value.body) : [];
            _isSubmit.sink.addError(parsed["message"]);
          }
        }
      });
    } catch (e) {
      // Catch error jika terjadi kesalahan saat mengolah data
      // print(">> ERR : $e");
      _isSubmit.sink.add(2);
      _isSubmit.sink.addError("Terjadi kesalahan saat mengirim data!");
    }
  }

  dispose() {
    _email.close();
    _isSubmit.close();
  }
}
