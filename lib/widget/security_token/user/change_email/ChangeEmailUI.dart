import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/services/auth/auth_service.dart';
import 'package:santaraapp/widget/security_token/user/change_email/changeemail.bloc.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';

class ChangeEmailUI extends StatefulWidget {
  final String pin;
  final bool finger;

  const ChangeEmailUI({this.pin, this.finger});
  @override
  _ChangeEmailUIState createState() => _ChangeEmailUIState();
}

class _ChangeEmailUIState extends State<ChangeEmailUI> {
  final _bloc = ChangeEmailBloc();
  final _authService = AuthService();

  // listening to isSubmit -> State ketika user submit kirim
  _listener() {
    _bloc.isSubmit.listen((data) {
      if (data == 1) {
        // jika valuenya 1 tampilkan loading dialog
        PopupHelper.showLoading(context);
      } else if (data == 2) {
        // jika valuenya 2 (gagal submit), hide loading dialog
        Navigator.pop(context);
      } else if (data == 3) {
        // jika valuenya 3 (sukses submit), hide loading dialog
        // kemudian arahkan user ke page sukses ubah password
        Navigator.pop(context);
        _authService.logout();
        dialogVerifEmail();
      }
    }, onError: (err) {
      // handling error, jika state ini ada error, maka tampilin lewat toast
      ToastHelper.showFailureToast(context, err.toString());
    });
  }

  @override
  void dispose() {
    // clean bloc if page is closed
    _bloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // initialize listener
    _listener();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        width: double.maxFinite,
        height: double.maxFinite,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/icon/verification.png",
                height: 150,
              ),
              Container(
                height: 20,
              ),
              Text(
                "Atur Ulang Alamat Email",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
              Container(
                height: 5,
              ),
              Text(
                "Masukkan alamat email yang baru",
                style: TextStyle(
                  fontSize: 15,
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                height: 30,
              ),
              Container(
                height: 75,
                child: StreamBuilder(
                  stream: _bloc.email,
                  builder: (context, snapshot) {
                    return Column(
                      children: <Widget>[
                        Container(
                          height: 50,
                          child: SantaraTextField(
                            onChanged: (String val) {
                              _bloc.changeEmail(val);
                            },
                            errorText: snapshot.error,
                            hintText: "email_saya@gmail.com",
                            labelText: "Alamat Email Baru",
                          ),
                        ),
                        snapshot.error != null
                            ? Container(
                                margin: EdgeInsets.only(top: 2),
                                height: 15,
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "${snapshot.error}",
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    );
                  },
                ),
              ),
              Container(
                height: 40,
              ),
              Container(
                height: 50,
                child: SantaraPickerButton(
                  title: "Kirim",
                  onPressed: () {
                    // print("object");
                    _bloc.submitEmail(widget.pin, widget.finger);
                    // dialogVerifEmail();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void dialogVerifEmail() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset("assets/icon/success.png"),
                Container(height: 20),
                Text(
                  "Verifikasi Email Anda",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(height: 10),
                Text(
                  "Verifikasi email anda untuk menyelesaikan proses ganti e-mail",
                  textAlign: TextAlign.center,
                ),
                Container(height: 20),
                GestureDetector(
                  onTap: () async {
                    // _authService.logout().then((value) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => Home(index: 4)),
                        (Route<dynamic> route) => false);
                    // });
                  },
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                        child: Text("Masuk",
                            style:
                                TextStyle(color: Colors.white, fontSize: 15))),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
