import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:http/http.dart' as http;

class SuccessChangeUI extends StatefulWidget {
  final String message;
  SuccessChangeUI({@required this.message});
  @override
  _SuccessChangeUIState createState() => _SuccessChangeUIState(message);
}

class _SuccessChangeUIState extends State<SuccessChangeUI> {
  String message;
  _SuccessChangeUIState(this.message);
  final storage = new FlutterSecureStorage();
  String username = 'Guest';
  String email = 'Guest@gmail.com';
  String token;
  var photo;
  var datas;
  var refreshToken;
  var tokenFirebase;

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    tokenFirebase = await storage.read(key: "tokenFirebase");
    var data = userFromJson(await storage.read(key: 'user'));
    setState(() => datas = data);
  }

  // get image header
  get _imageHeader {
    return Container(
      height: 150,
      // width: 200,
      child: Image.asset("assets/icon/success.png"),
    );
  }

  Future logout(BuildContext context) async {
    PopupHelper.showLoading(context);
    try {
      final id = datas.id;
      await http.post('$apiLocal/auth/logout', body: {
        "refreshToken": "$refreshToken",
        "user_id": "$id",
      });
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'uuid');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'userId');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.notif = 0;
      Navigator.pop(context);
    } catch (e) {
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((val) {
      logout(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        ToastHelper.showBasicToast(
            context, "Tidak dapat kembali ke halaman sebelumnya!");
        return false;
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(15),
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _imageHeader,
              Container(
                height: 20,
              ),
              Text(
                "$message",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              Container(
                height: 20,
              ),
              SantaraMainButton(
                title: "Masuk",
                onPressed: () {
                  Navigator.pushNamed(context, '/login');
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
