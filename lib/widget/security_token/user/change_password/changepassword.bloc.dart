import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/services/security/pass_service.dart';

class ChangePasswordBloc {
  final _apiService = PasswordApiService();
  // handling validasi password
  final validatePass1 = StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
    if (password.length < 8) {
      sink.addError('Password minimal 8 karakter!');
    } else if (!RegExp(r"^(?=.*[a-z])").hasMatch(password)) {
      sink.addError('Password harus mengandung huruf kecil!');
    } else if (!RegExp(r"^(?=.*[A-Z])").hasMatch(password)) {
      sink.addError('Password harus mengandung huruf besar!');
    } else if (!RegExp(r"^(?=.*[0-9])").hasMatch(password)) {
      sink.addError('Password harus mengandung angka!');
    } else if (!RegExp(r"^(?=.*[^\w\s])").hasMatch(password)) {
      sink.addError('Password harus mengandung spesial karakter!');
    } else {
      sink.add(password);
    }
  });

  // define vars
  final _pass1 = BehaviorSubject<String>(); // password
  final _pass2 = BehaviorSubject<String>(); // konfirmasi password
  final _passMatch =
      BehaviorSubject<bool>(); // password & konfirmasi password sama
  final _isSubmit = BehaviorSubject<int>.seeded(0); // status submit
  final _obscurePassword =
      BehaviorSubject<bool>.seeded(true); // show/hide password

  // handling streams
  Stream<String> get pass1 => _pass1.stream.transform(validatePass1);
  Stream<String> get pass2 => _pass2.stream.transform(validatePass1);
  Stream<bool> get passMatch => _passMatch.stream;
  Stream<int> get isSubmit => _isSubmit.stream;
  Stream<bool> get obscurePassword => _obscurePassword.stream;

  // Handle data change ( for specific stream )
  Function(String) get changePass1 => _pass1.sink.add;
  Function(String) get changePass2 => _pass2.sink.add;

  // set match password ( via UI )
  set setpassMatch(bool val) => _passMatch.sink.add(val);

  // hit api reset password
  submitPass(String pin, bool finger) async {
    // Keterangan is submit
    // 0 = Ga ngapa ngapain
    // 1 = Submitting
    // 2 = Error submit
    // 3 = Success submit
    // *NEXT Dibuat jadi class sendiri ( buat state terpisah )
    final pass = _pass1.value;
    final passConf = _pass2.value;

    if (_passMatch.value != null && _passMatch.value) {
      // Show popup loading
      _isSubmit.sink.add(1);
      try {
        await _apiService
            .resetPassword(pass, passConf, pin, finger)
            .then((value) {
          if (value.statusCode == 200) {
            // Jika submit password berhasil
            _isSubmit.sink.add(3);
          } else {
            // Jika submit password gagal
            _isSubmit.sink.add(2);
            if (value.statusCode == 500) {
              // Default error jika statusCode : 500 (Internal Server Error)
              _isSubmit.sink.addError("Tidak dapat menghubungkan!");
            } else {
              // Parse error value
              var parsed = value != null ? json.decode(value.body) : [];
              _isSubmit.sink.addError(parsed["message"]);
            }
          }
        });
      } catch (e) {
        // Catch error jika terjadi kesalahan saat mengolah data
        // print(">> ERR : $e");
        _isSubmit.sink.add(2);
        _isSubmit.sink.addError("Terjadi kesalahan saat mengirim data!");
      }
    } else {
      // Jika form tidak valid, tampilkan ini
      _isSubmit.sink.addError("Mohon cek kembali form yang anda isi!");
    }
  }

  // BUAT TESTING
  submitPassOld() async {
    // Keterangan is submit
    // 0 = Doin nothin
    // 1 = Submitting
    // 2 = Error submit
    // 3 = Success submit
    Random random = Random();

    if (_passMatch.value != null && _passMatch.value) {
      _isSubmit.sink.add(1);
      try {
        await Future.delayed(Duration(milliseconds: 1500));
        var err = random.nextBool();
        if (err) {
          _isSubmit.sink.add(2);
          _isSubmit.sink
              .addError("Terjadi kesalahan, tidak dapat mengirim data!");
        } else {
          _isSubmit.sink.add(3);
        }
      } catch (e) {
        _isSubmit.sink.add(2);
        _isSubmit.sink
            .addError("Terjadi kesalahan, tidak dapat mengirim data!");
      }
    } else {
      _isSubmit.sink.addError("Mohon cek kembali form yang anda isi!");
    }
  }

  // END

  // Bloc constructor
  ChangePasswordBloc() {
    _pass2.listen((data) {
      if (data == _pass1.value) {
        _passMatch.sink.add(true);
      } else {
        _passMatch.sink.add(false);
      }
    });
  }

  // cleaning stream
  dispose() {
    _pass1.close();
    _pass2.close();
    _passMatch.close();
    _isSubmit.close();
    _obscurePassword.close();
  }
}
