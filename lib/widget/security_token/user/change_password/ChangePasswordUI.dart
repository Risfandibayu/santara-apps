import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/widget/account/pages/SelfieKYCUI.dart';
import 'package:santaraapp/widget/security_token/user/change_password/changepassword.bloc.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraField.dart';
import '../SuccessChangeUI.dart';

class ChangePasswordUI extends StatefulWidget {
  final String pin;
  final bool finger;
  final int isNewResetPassword;

  const ChangePasswordUI({this.pin, this.finger, this.isNewResetPassword});
  @override
  _ChangePasswordUIState createState() => _ChangePasswordUIState();
}

class _ChangePasswordUIState extends State<ChangePasswordUI> {
  final _bloc = ChangePasswordBloc();
  final _pass2Ctrl = TextEditingController();

  // listening to isSubmit -> State ketika user submit kirim
  _listener() {
    _bloc.isSubmit.listen((data) {
      if (data == 1) {
        // jika valuenya 1 tampilkan loading dialog
        PopupHelper.showLoading(context);
      } else if (data == 2) {
        // jika valuenya 2 (gagal submit), hide loading dialog
        Navigator.pop(context);
      } else if (data == 3) {
        // jika valuenya 3 (sukses submit), hide loading dialog
        // kemudian arahkan user ke page sukses ubah password
        if(widget.isNewResetPassword == 0) {
          ToastHelper.showBasicToast(context, 'Silahkan verikasi Kyc terlebih dahulu');
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SelfieKycUI(
                fromReset: 1,
              ),
            ),
          );
        } else {
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SuccessChangeUI(
                message: "Perubahan password  berhasil.",
              ),
            ),
          );
        }
      }
    }, onError: (err) {
      // handling error, jika state ini ada error, maka tampilin lewat toast
      ToastHelper.showFailureToast(context, err.toString());
    });
  }

  @override
  void dispose() {
    // clean bloc if page is closed
    _bloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // initialize listener
    _listener();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(15),
          width: double.maxFinite,
          height: double.maxFinite,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/icon/verification.png",
                  height: 150,
                ),
                Container(
                  height: 20,
                ),
                Text(
                  "Atur Ulang Kata Sandi",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
                Container(
                  height: 20,
                ),
                // Field password
                Container(
                  height: 75,
                  child: StreamBuilder(
                    stream: _bloc.pass1,
                    builder: (context, snapshot) {
                      return Column(
                        children: <Widget>[
                          Container(
                            height: 50,
                            child: SantaraTextField(
                              onChanged: (String val) {
                                _bloc.changePass1(val);
                                // Jika field konfirmasi kata sandi ada isinya, maka clear datanya
                                if (_pass2Ctrl.text != null &&
                                    _pass2Ctrl.text.length > 0) {
                                  _pass2Ctrl.clear();
                                  _bloc.setpassMatch = false;
                                }
                              },
                              inputType: TextInputType.visiblePassword,
                              errorText: snapshot.error,
                              obscureText: true,
                              hintText: "Kata Sandi Baru",
                              labelText: "Kata Sandi Baru",
                            ),
                          ),
                          snapshot.error != null
                              ? Container(
                                  margin: EdgeInsets.only(top: 2),
                                  height: 15,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "${snapshot.error}",
                                      style: TextStyle(
                                        color: Colors.red,
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      );
                    },
                  ),
                ),
                // field konfirmasi password
                Container(
                  height: 50,
                  child: StreamBuilder(
                    stream: _bloc.pass2,
                    builder: (context, snapshot) {
                      return SantaraTextField(
                        controller: _pass2Ctrl,
                        onChanged: _bloc.changePass2,
                        obscureText: true,
                        inputType: TextInputType.visiblePassword,
                        hintText: "Konfirmasi Kata Sandi Baru",
                        labelText: "Konfirmasi Kata Sandi Baru",
                      );
                    },
                  ),
                ),
                Container(
                  height: 5,
                ),
                // jika field password & konfirmasi password matching
                StreamBuilder<bool>(
                    stream: _bloc.passMatch,
                    builder: (context, snapshot) {
                      return snapshot.data != null
                          ? Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                snapshot.data
                                    ? "Kata sandi baru cocok"
                                    : "Kata sandi baru tidak cocok",
                                style: TextStyle(
                                    color: snapshot.data
                                        ? Colors.green
                                        : Colors.red),
                              ),
                            )
                          : Container();
                    }),
                Container(
                  height: 40,
                ),
                Container(
                    height: 50,
                    child: SantaraPickerButton(
                      title: "Kirim",
                      onPressed: () {
                        // Hide keyboard, kemudian hit api reset password
                        FocusScope.of(context).requestFocus(FocusNode());
                        _bloc.submitPass(widget.pin, widget.finger);
                      },
                    )),
              ],
            ),
          ),
        ));
  }
}
