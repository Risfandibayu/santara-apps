import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/home/ListPenerbit.dart';
import 'package:santaraapp/widget/home/ListPralisting.dart';
import 'package:santaraapp/widget/home/ListTestimoni.dart';
import 'package:santaraapp/widget/home/Market.dart';
import 'package:santaraapp/widget/home/Soldout.dart';
import 'package:santaraapp/widget/home/comingsoon.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';

//component
import '../home/CarouselContent.dart';
import '../home/Button.dart';
import '../home/Status.dart';
import '../home/Partner.dart';

class Index extends StatefulWidget {
  @override
  _IndexState createState() => _IndexState();
}

class _IndexState extends State<Index> {
  final GlobalKey<ScaffoldState> _drawerKey = new GlobalKey<ScaffoldState>();
  int tnc;
  bool isCompleteKyc = true;
  Widget next;
  String token;
  String uuid;
  Widget notif = Container();
  String userType;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        appBar: CustomAppbar(drawerKey: _drawerKey),
        drawer: CustomDrawer(),
        body: Stack(
          children: [
            Container(
              color: Color(ColorRev.mainBlack),
              height: MediaQuery.of(context).size.height,
              child: ListView(
                key: Key('indexListViewKey'),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: SantaraAppBetaTesting(),
                  ),
                  CarouselContent(),
                  Button(),
                  // UKMReliefUI(),
                  // Status(),
                  Market(),
                  ListPenerbit(),
                  ListPralisting(),
                  // ComingSoon(),
                  Soldout(),

                  // wajib edit

                  // ListTestimoni(),
                  // Partner(),
                ],
              ),
            ),
            KycNotificationStatus(showCloseButton: false),
          ],
        ));
  }
}
