import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';

class Forgot extends StatefulWidget {
  @override
  _ForgotState createState() => _ForgotState();
}

class _ForgotState extends State<Forgot> {
  final TextEditingController emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _notifKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  Future forgot(String email) async {
    setState(() => isLoading = true);
    try {
      var headers = await UserAgent.headersNoAuth();
      headers['origin'] = urlOrigin;
      var url = '$apiLocal/auth/request-forgot-pass';
      final http.Response response = await http.post(url,
          body: {"email": email},
          headers: headers); //.timeout(Duration(seconds: 20));
      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
          Helper.reqResetPassword = Helper.reqResetPassword + 1;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Email telah berhasil dikirim'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home(index: 4)),
                          (Route<dynamic> route) => false);
                    },
                  )
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        String msg;
        if (response.body.contains("message")) {
          final data = jsonDecode(response.body);
          msg = data["message"];
        } else {
          msg = response.body;
        }
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text(msg),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text('Proses mengalami kendala, harap coba kembali'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
    // } on SocketException catch (_) {
    //   setState(() => isLoading = false);
    //   showDialog(
    //       context: context,
    //       builder: (BuildContext context) {
    //         return AlertDialog(
    //           content: Text('Proses mengalami kendala, harap coba kembali'),
    //           actions: <Widget>[
    //             FlatButton(
    //               child: Text('Ok'),
    //               onPressed: () {
    //                 Navigator.of(context).pop();
    //               },
    //             )
    //           ],
    //         );
    //       });
    // }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => Home(index: 4)),
            (Route<dynamic> route) => false);
      },
      child: Scaffold(
          backgroundColor: Color(ColorRev.mainBlack),
          body: ModalProgressHUD(
              inAsyncCall: isLoading,
              progressIndicator: CircularProgressIndicator(),
              opacity: 0.5,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: Stack(children: <Widget>[
                  ListView(children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              children: [
                                Image.asset(
                                  'assets/santara/Logo_Santara.png',
                                  height: MediaQuery.of(context).size.width / 3,
                                ),
                                SizedBox(height: 20),
                                Text(
                                  'masukan Email Akun Santara',
                                  style: TextStyle(
                                      color: Color(ColorRev.mainwhite),
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Container(
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(4.0),
                                              border: Border.all(
                                                  color: Colors.grey)),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 4),
                                          child: TextFormField(
                                              keyboardType:
                                                  TextInputType.emailAddress,
                                              controller: emailController,
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return 'Masukkan email anda';
                                                } else if (!RegExp(
                                                        r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                                                    .hasMatch(value)) {
                                                  return 'Email anda tidak valid, coba lagi';
                                                } else {
                                                  return null;
                                                }
                                              },
                                              autofocus: true,
                                              decoration: InputDecoration(
                                                icon: Icon(Icons.mail_outline),
                                                hintText: 'Email',
                                                errorMaxLines: 5,
                                                border: InputBorder.none,
                                              )),
                                        ),

                                        //button
                                        Container(
                                          key: _notifKey,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          child: InkWell(
                                            onTap: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                if (Helper.reqResetPassword <
                                                    3) {
                                                  forgot(emailController.text);
                                                } else {
                                                  showErrorMessage(
                                                      "Anda telah mencapai limit permintaan reset password");
                                                }
                                              }
                                            },
                                            child: Container(
                                              height: 48.0,
                                              decoration: BoxDecoration(
                                                color: Color(0xFFBF2D30),
                                                border: Border.all(
                                                    color: Colors.transparent,
                                                    width: 2.0),
                                                borderRadius:
                                                    BorderRadius.circular(4.0),
                                              ),
                                              child: Center(
                                                  child: Text('SUBMIT',
                                                      style: TextStyle(
                                                          color:
                                                              Colors.white))),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ))
                          ]),
                    )
                  ]),
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Platform.isIOS
                        ? IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () => Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(index: 4)),
                                (Route<dynamic> route) => false),
                          )
                        : Container(),
                  )
                ]),
              ))),
    );
  }

  void showErrorMessage(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  padding: const EdgeInsets.all(16),
                  child: Center(
                    child: Text('$msg',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }
}
