import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class ResendEmailVerification extends StatefulWidget {
  @override
  _ResendEmailVerificationState createState() =>
      _ResendEmailVerificationState();
}

class _ResendEmailVerificationState extends State<ResendEmailVerification> {
  final TextEditingController emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _notifKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  Future resend(String email) async {
    setState(() => isLoading = true);
    var url = '$apiLocal/auth/request-verification-email';
    try {
      final http.Response response = await http.post(url,
          body: {"email": email},
          headers: {"origin": urlOrigin}); //.timeout(Duration(seconds: 20));
      if (response.statusCode == 200) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Email telah berhasil dikirim'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home(index: 4)),
                          (Route<dynamic> route) => false);
                    },
                  )
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        String msg;
        if (response.body.contains("message")) {
          final data = jsonDecode(response.body);
          msg = data["message"];
        } else {
          msg = response.body;
        }
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text(msg),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
      // } on TimeoutException catch (_) {
      //   setState(() => isLoading = false);
      //   showDialog(
      //       context: context,
      //       builder: (BuildContext context) {
      //         return AlertDialog(
      //           content: Text('Proses mengalami kendala, harap coba kembali'),
      //           actions: <Widget>[
      //             FlatButton(
      //               child: Text('Ok'),
      //               onPressed: () {
      //                 Navigator.of(context).pop();
      //               },
      //             )
      //           ],
      //         );
      //       });
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ModalProgressHUD(
            inAsyncCall: isLoading,
            progressIndicator: CircularProgressIndicator(),
            opacity: 0.5,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Stack(children: <Widget>[
                ListView(children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
                    child: Column(children: <Widget>[
                      Image.asset('assets/santara/2.png',
                          height: 140.0, width: 160.0),
                      Padding(
                          padding: EdgeInsets.only(top: 30.0),
                          child: Container(
                            child: Column(children: <Widget>[
                              Form(
                                key: _formKey,
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                          child: Text(
                                        "Masukkan email anda untuk\nmendapatkan link verifikasi",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.center,
                                      )),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 10),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          border:
                                              Border.all(color: Colors.black)),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: TextFormField(
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          controller: emailController,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Masukkan email anda';
                                            } else if (!RegExp(
                                                    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                                                .hasMatch(value)) {
                                              return 'Email anda tidak valid, coba lagi';
                                            } else {
                                              return null;
                                            }
                                          },
                                          autofocus: true,
                                          decoration: InputDecoration(
                                            icon: Icon(Icons.account_circle),
                                            hintText: 'Email',
                                            errorMaxLines: 5,
                                            border: InputBorder.none,
                                          )),
                                    ),

                                    //button
                                    Container(
                                      key: _notifKey,
                                      padding: EdgeInsets.fromLTRB(
                                          30.0, 5.0, 30.0, 20.0),
                                      child: InkWell(
                                        onTap: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            resend(emailController.text);
                                          }
                                        },
                                        child: Container(
                                          height: 50.0,
                                          decoration: BoxDecoration(
                                            color: Colors.red[700],
                                            border: Border.all(
                                                color: Colors.transparent,
                                                width: 2.0),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: Center(
                                              child: Text('SUBMIT',
                                                  style: TextStyle(
                                                      fontSize: 18.0,
                                                      color: Colors.white))),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                          ))
                    ]),
                  )
                ]),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Platform.isIOS
                      ? IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () => Navigator.pop(context),
                        )
                      : Container(),
                )
              ]),
            )));
  }
}
