import 'dart:io';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/pages/Login.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/rev_color.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController no = new TextEditingController();
  final TextEditingController user = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController pass = new TextEditingController();
  final TextEditingController pass2 = new TextEditingController();
  FocusNode focusPass = FocusNode();
  FocusNode focusRePass = FocusNode();
  final _formKey = new GlobalKey<FormState>();
  CountryCode countryCode = new CountryCode(dialCode: "+62", code: "ID");

  bool isLoading = false;
  bool password = true;
  bool password2 = true;
  int isRightPhone = 0;
  String hintPassword = 'Password';
  String hintRePassword = 'Ulangi Password';

  //create new user
  Future<dynamic> createUser(BuildContext context) async {
    FirebaseAnalytics().logEvent(name: 'app_register', parameters: null);
    setState(() => isLoading = true);
    var url = '$apiLocal/auth/register';
    String phone = no.text.substring(0, 1) == "0"
        ? no.text.substring(1, no.text.length)
        : no.text;
    try {
      final http.Response response = await http.post(url,
          headers: {
            "Content-type": "application/json",
            "Accept": "application/json",
            "origin": urlOrigin
          },
          body: jsonEncode({
            "phone": countryCode.dialCode + phone,
            "name": user.text,
            "user": {
              "email": email.text,
              "password": pass.text,
              "confirmPassword": pass2.text
            },
          }),
          encoding: Encoding.getByName('utf-8'));
      if (response.statusCode == 200) {
        _popupEmailVerification();
      } else {
        setState(() => isLoading = false);
        String msg;
        if (response.body.contains('message')) {
          final data = jsonDecode(response.body);
          msg = data["message"];
          if (msg != null && msg.toLowerCase().contains("email")) {
            FirebaseAnalytics()
                .logEvent(name: 'app_email_double', parameters: null);
          }
        } else {
          msg = "Terjadi kesalahan, silahkan coba beberapa saat lagi.";
        }
        showWarning(msg);
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showWarning('Proses mengalami kendala, harap coba kembali');
    }
  }

  void showWarning(String msg) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(msg),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _popupEmailVerification() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Registrasi Anda telah berhasil.',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                      "Mohon periksa email anda, kami telah mengirimkan link untuk melakukan verifikasi alamat e-mail.",
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey[700],
                      ),
                      textAlign: TextAlign.center),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => Home(index: 4)),
                        (Route<dynamic> route) => false);
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => Login()));
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void _dataValidation() {
    String phone = no.text.substring(0, 1) == "0"
        ? no.text.substring(1, no.text.length)
        : no.text;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Text(
                    "Konfirmasi data pengguna",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
                Container(height: 10),
                Text(
                  "Apakah data yang anda masukkan sudah benar ?",
                  style: TextStyle(color: Colors.grey[700], fontSize: 14),
                  textAlign: TextAlign.center,
                ),
                // Row(
                //   children: <Widget>[
                // Column(
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: <Widget>[
                Text("Nama :",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Text("${user.text}",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Container(height: 6),
                Text("Email :",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Text("${email.text}",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Container(height: 6),
                Text("No Telepon :",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Text("${countryCode.dialCode + phone}",
                    style: TextStyle(color: Colors.grey[700], fontSize: 14)),
                Container(height: 6),
                //   ],
                // ),
                // Column(
                //   children: <Widget>[
                //     Text("    : ",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //     Text("    : ",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //     Text("    : ",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //   ],
                // ),
                // Column(
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: <Widget>[
                //     Text("${user.text}",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //     Text("${email.text}",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //     Text("${countryCode.dialCode + phone}",
                //         style: TextStyle(
                //             color: Colors.grey[700], fontSize: 14)),
                //   ],
                // )
                //   ],
                // ),
                Container(height: 16),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 1, color: Color(0xFFBF2D30)),
                              borderRadius: BorderRadius.circular(4)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                                child: Text("Batalkan",
                                    style:
                                        TextStyle(color: Color(0xFFBF2D30)))),
                          ),
                        ),
                      ),
                    ),
                    Container(width: 10),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          Future.delayed(Duration(milliseconds: 500), () {
                            createUser(context);
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFBF2D30),
                              borderRadius: BorderRadius.circular(4)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                                child: Text("Lanjutkan",
                                    style: TextStyle(color: Colors.white))),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
            // actions: <Widget>[
            //   FlatButton(
            //     color: Colors.red[900],
            //     onPressed: () {},
            //     child: Text(
            //       'Lanjutkan',
            //       style: TextStyle(color: Colors.white),
            //     ),
            //   ),
            //   FlatButton(
            //     color: Colors.red[900],
            //     onPressed: () {
            //       Navigator.pop(context);
            //     },
            //     child: Text(
            //       'Batalkan',
            //       style: TextStyle(color: Colors.white),
            //     ),
            //   )
            // ]
          );
        });
  }

  @override
  void initState() {
    super.initState();
    focusPass.addListener(() {
      if (focusPass.hasFocus) {
        hintPassword = 'Contoh : P4\$sword';
      } else {
        hintPassword = 'Password';
      }
      setState(() {});
    });
    focusRePass.addListener(() {
      if (focusRePass.hasFocus) {
        hintRePassword = 'Contoh : P4\$sword';
      } else {
        hintRePassword = 'Ulangi Password';
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back, color: Colors.black),
          ),
        ),
        backgroundColor: Color(ColorRev.mainBlack),
        body: ModalProgressHUD(
            inAsyncCall: isLoading,
            progressIndicator: CircularProgressIndicator(),
            opacity: 0.5,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Stack(children: <Widget>[
                ListView(children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    child: Column(
                      children: [
                        // Image.asset('assets/santara/2.png',
                        // height: MediaQuery.of(context).size.width / 3,
                        // width: MediaQuery.of(context).size.width / 3),
                        Column(
                          children: [
                            Image.asset(
                              'assets/santara/Logo_Santara.png',
                              height: MediaQuery.of(context).size.width / 3,
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Mendaftar Akun Di Santara',
                              style: TextStyle(
                                  color: Color(ColorRev.mainwhite),
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                      child: Form(
                          key: _formKey,
                          child: Column(children: <Widget>[
                            _fullname(),
                            Container(height: 10),
                            _phone(),
                            Container(height: 10),
                            _email(),
                            Container(height: 10),
                            _password(),
                            Container(height: 10),
                            _password2(),

                            //button
                            Container(
                              padding:
                                  EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
                              child: InkWell(
                                onTap: () {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  if (_formKey.currentState.validate()) {
                                    if (isRightPhone == 0) {
                                      // createUser(context);
                                      _dataValidation();
                                    }
                                  }
                                },
                                child: Container(
                                  height: 48.0,
                                  decoration: BoxDecoration(
                                    color: Color(ColorRev.mainRed),
                                    border: Border.all(
                                        color: Colors.transparent, width: 2.0),
                                    borderRadius: BorderRadius.circular(4.0),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Daftar',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ]))),
                ]),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Platform.isIOS
                      ? IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () => Navigator.pop(context),
                        )
                      : Container(),
                )
              ]),
            )));
  }

  Widget _fullname() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
          controller: user,
          keyboardType: TextInputType.text,
          validator: (value) {
            if (value.isEmpty) {
              return 'Masukkan nama lengkap anda';
            } else if (!RegExp(r"^[a-zA-Z\ ]*$").hasMatch(value)) {
              return "Masukkan input tanpa spesial karakter";
            } else {
              return null;
            }
          },
          autofocus: false,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              prefixIcon: Icon(Icons.person_outline),
              hintText: 'Nama lengkap',
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 5.0),
              ),
              errorMaxLines: 5)),
    );
  }

  Widget _phone() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30),
          height: 64,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4.0)),
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8),
                height: 64,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(4.0)),
                child: CountryCodePicker(
                  onChanged: (value) {
                    setState(() => countryCode = value);
                  },
                  initialSelection: 'ID',
                ),
              ),
              Flexible(
                  child: TextFormField(
                      controller: no,
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value.isEmpty) {
                          setState(() => isRightPhone = 1);
                          return null;
                        } else if (!RegExp(r"^[0-9]*$").hasMatch(value)) {
                          setState(() => isRightPhone = 2);
                          return null;
                        } else if (value.length < 7) {
                          setState(() => isRightPhone = 3);
                          return null;
                        } else if (value[0] == "0") {
                          setState(() => isRightPhone = 4);
                          return null;
                        } else {
                          setState(() => isRightPhone = 0);
                          return null;
                        }
                      },
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: 'Contoh: 812xxxxx',
                        border: InputBorder.none,
                      ))),
            ],
          ),
        ),
        isRightPhone == 0
            ? Container()
            : Container(
                margin: EdgeInsets.fromLTRB(42, 4, 8, 4),
                child: Text(
                    isRightPhone == 4
                        ? "Nomor tidak boleh diawali dengan 0"
                        : isRightPhone == 1
                            ? "Masukkan nomor telepon anda"
                            : isRightPhone == 2
                                ? "Masukkan inputan tanpa spesial karakter"
                                : "No telepon kurang dari 8 digit",
                    style: TextStyle(fontSize: 12, color: Colors.red[700])),
              ),
      ],
    );
  }

  Widget _email() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
          controller: email,
          keyboardType: TextInputType.emailAddress,
          validator: (value) {
            if (value.isEmpty) {
              return 'Masukkan email anda';
            } else if (!RegExp(
                    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
              return 'Masukkan email dengan Benar';
            } else {
              return null;
            }
          },
          autofocus: false,
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.mail_outline),
            fillColor: Colors.white,
            filled: true,
            hintText: 'Email',
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 5.0),
            ),
            errorMaxLines: 5,
          )),
    );
  }

  Widget _password() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
          focusNode: focusPass,
          controller: pass,
          obscureText: password,
          validator: (value) {
            if (value.isEmpty) {
              return 'Masukkan password';
            } else if (value.length < 8) {
              return 'Password minimal 8 karakter!';
            } else if (!RegExp(r"^(?=.*[a-z])").hasMatch(value)) {
              return 'Password harus mengandung huruf kecil!';
            } else if (!RegExp(r"^(?=.*[A-Z])").hasMatch(value)) {
              return 'Password harus mengandung huruf besar!';
            } else if (!RegExp(r"^(?=.*[0-9])").hasMatch(value)) {
              return 'Password harus mengandung angka!';
            } else if (!RegExp(r"^(?=.*[^\w\s])").hasMatch(value)) {
              return 'Password harus mengandung spesial karakter!';
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(Icons.lock_open),
            suffixIcon: IconButton(
                icon: Icon(password ? Icons.visibility_off : Icons.visibility),
                onPressed: () {
                  setState(() {
                    password ? password = false : password = true;
                  });
                }),
            hintText: hintPassword,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 5.0),
            ),
            errorMaxLines: 5,
          )),
    );
  }

  Widget _password2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: TextFormField(
          focusNode: focusRePass,
          controller: pass2,
          obscureText: password2,
          validator: (value) {
            if (value.isEmpty) {
              return 'Masukkan password';
            } else if (value.length < 8) {
              return 'Password minimal 8 karakter!';
            } else if (!RegExp(r"^(?=.*[a-z])").hasMatch(value)) {
              return 'Password harus mengandung huruf kecil!';
            } else if (!RegExp(r"^(?=.*[A-Z])").hasMatch(value)) {
              return 'Password harus mengandung huruf besar!';
            } else if (!RegExp(r"^(?=.*[0-9])").hasMatch(value)) {
              return 'Password harus mengandung angka!';
            } else if (!RegExp(r"^(?=.*[^\w\s])").hasMatch(value)) {
              return 'Password harus mengandung spesial karakter!';
            } else if (value != pass.text) {
              return 'Password tidak sama';
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(Icons.lock_open),
            suffixIcon: IconButton(
                icon: Icon(password2 ? Icons.visibility_off : Icons.visibility),
                onPressed: () {
                  setState(() {
                    password2 ? password2 = false : password2 = true;
                  });
                }),
            hintText: hintRePassword,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 5.0),
            ),
            errorMaxLines: 5,
          )),
    );
  }
}
