import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/account/pages/BusinessUI.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'pages/ProfileUI.dart';
import 'pages/blocs/user.business.bloc.dart';

class AccountUI extends StatefulWidget {
  @override
  _AccountUIState createState() => _AccountUIState();
}

class _AccountUIState extends State<AccountUI> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => UserBusinessBloc(UserBusinessUninitialized())
            ..add(LoadUserBusiness()),
        ),
      ],
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
            backgroundColor: Color(ColorRev.mainBlack),
            key: _drawerKey,
            appBar: CustomAppbar(
              drawerKey: _drawerKey,
              bottom: TabBar(
                indicatorColor: Color((0xFFBF2D30)),
                labelColor: Color((0xFFBF2D30)),
                unselectedLabelColor: Color(ColorRev.mainwhite),
                tabs: [
                  Tab(child: Text("Profil")),
                  Tab(child: Text("Bisnis Anda")),
                ],
              ),
            ),
            drawer: CustomDrawer(),
            body: TabBarView(children: [
              // profile
              ProfileUI(),
              // Bisnis Saya
              BusinessUI()
            ])),
      ),
    );
  }
}
