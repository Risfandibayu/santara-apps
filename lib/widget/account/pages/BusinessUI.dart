import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/account/pages/blocs/user.business.bloc.dart';
import 'package:santaraapp/widget/emiten/EmitenUI.dart';
import 'package:santaraapp/widget/financial_statements/FinancialStatementsUI.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_page.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraBusinessWidget.dart';

import '../../emiten/EmitenUI.dart';
import '../../financial_statements/FinancialStatementsUI.dart';
import '../../widget/components/main/SantaraAppBetaTesting.dart';
import '../../widget/components/main/SantaraBusinessWidget.dart';
import 'blocs/user.business.bloc.dart';

class BusinessUI extends StatefulWidget {
  @override
  _BusinessUIState createState() => _BusinessUIState();
}

class _BusinessUIState extends State<BusinessUI> {
  bool isEmpty = false;
  UserBusinessBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<UserBusinessBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBusinessBloc, UserBusinessState>(
        builder: (context, state) {
      if (state is UserBusinessUninitialized) {
        return Container(
          child: Center(
            child: CupertinoActivityIndicator(),
          ),
        );
      } else if (state is UserBusinessLoaded) {
        return Container(
          color: Color(ColorRev.mainBlack),
          width: double.maxFinite,
          height: double.maxFinite,
          child: isEmpty
              ? SantaraNoBusinessAvailable(pralisting: state?.pralisting ?? 0)
              : Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: SantaraAppBetaTesting(),
                    ),
                    state.pralisting > 0
                        ? SubmittedBusinessNotification(
                            message:
                                "Anda memiliki ${state.pralisting} bisnis yang sedang diajukan.",
                            onTap: () async {
                              final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BusinessFiledPage(),
                                ),
                              );
                              bloc..add(LoadUserBusiness());
                            })
                        : Container(),
                    Expanded(
                      flex: 1,
                      child: state.datas.length > 0
                          ? ListView.builder(
                              itemCount: state.datas.length,
                              itemBuilder: (context, index) {
                                var data = state.datas[index];
                                return SantaraBusinessCard(
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          data.status == "funding"
                                              ? EmitenUI(uuid: data.uuid)
                                              : FinancialStatementsUI(
                                                  uuid: "${data.uuid}",
                                                ),
                                    ),
                                  ),
                                  data: data,
                                );
                              },
                            )
                          : SantaraNoBusinessAvailable(
                              pralisting: state.pralisting,
                              callback: () {
                                bloc..add(LoadUserBusiness());
                              },
                            ),
                    ),
                  ],
                ),
        );
      } else if (state is UserBusinessNotFound) {
        return SantaraNoBusinessAvailable();
      } else if (state is UserBusinessError) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(state.error),
            IconButton(
              icon: Icon(Icons.replay),
              onPressed: () => bloc..add(LoadUserBusiness()),
            )
          ],
        );
      } else if (state is UserBusinessUnauthorized) {
        return Container();
      } else {
        return Container(
          child: Center(
            child: Text("Unknown State!"),
          ),
        );
      }
    });
  }
}
