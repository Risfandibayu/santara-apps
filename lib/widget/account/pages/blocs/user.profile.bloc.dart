import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/helpers/kyc/StatusPerusahaan.dart';
import 'package:santaraapp/helpers/kyc/StatusTraderKyc.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/logger.dart';

class UserProfileEvent {}

class UserProfileState {}

class LoadUserProfile extends UserProfileEvent {}

class UserProfileUninitialized extends UserProfileState {}

class UserProfileError extends UserProfileState {
  final String error;
  UserProfileError({this.error});
}

class UserProfileLoaded extends UserProfileState {
  List<MenuModel> menus;
  User user;
  EndStatus kycStatus;
  UserProfileLoaded({this.menus, this.user, this.kycStatus});
}

class UserProfileBloc extends Bloc<UserProfileEvent, UserProfileState> {
  UserProfileBloc(UserProfileState initialState)
      : super(UserProfileUninitialized());

  ApiService apiService = ApiService();
  final storage = FlutterSecureStorage();
  int loop = 0;
  int requestTimes = 0;

  List<MenuModel> menus = <MenuModel>[
    MenuModel(
        title: "Deposit",
        image: "assets/icon_menu/new/Deposit.png",
        routes: "/deposit"),
    MenuModel(
        title: "Withdraw",
        image: "assets/icon_menu/new/Withdraw.png",
        routes: "/withdraw"),
    MenuModel(
        title: "Wallet",
        image: "assets/icon_menu/new/Wallet.png",
        routes: "/wallet"),
    MenuModel(
        title: "Portofolio",
        image: "assets/icon_menu/new/Portfolio.png",
        routes: "/portfolio"),
    MenuModel(
        title: "Riwayat Pengguna",
        image: "assets/icon_menu/new/History.png",
        routes: "/journey"),
    MenuModel(
        title: "Dividen",
        image: "assets/icon_menu/new/Dividen.png",
        routes: "/dividend"),
    MenuModel(
        title: "Pasar Sekunder",
        image: "assets/icon_menu/new/Sekunder.png",
        routes: ""),
    MenuModel(
        title: "Video",
        image: "assets/icon_menu/new/Videos.png",
        routes: "/videos"),
  ].toList();

  Future<User> getUserData() async {
    try {
      var result = await apiService.getUserByUuid();
      if (result.statusCode != 200) {
        loop += 1;
        if (loop < 3) {
          getUserData();
        } else {
          return null;
        }
      } else {
        final data = userFromJson(json.encode(result.data));
        await storage.write(
          key: Constants.statusKyc,
          value: '${data.trader.isVerified == 1 ? true : false}',
        );
        return data;
      }
    } catch (e) {
      loop += 1;
      if (loop < 3) {
        getUserData();
      } else {
        return null;
      }
    }
  }

  Future checkKycStatus(String traderType) async {
    try {
      if (traderType == "personal") {
        KycPersonalService _service = KycPersonalService();
        await _service.statusIndividualKyc().then((res) {
          if (res != null) {
            var result = StatusTraderKycHelper.checkingIndividual(
                StatusKycTrader.fromJson(res.data));
            return result.status;
          }
        });
      } else {
        KycPerusahaanService _api = KycPerusahaanService();
        await _api.statusKycTrader().then((res) {
          if (res != null) {
            var result = StatusPerusahaanHelper.checkingStatus(
                StatusKycTrader.fromJson(res.data));
            return result.status;
          }
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      if (requestTimes < 3) {
        checkKycStatus(traderType);
        requestTimes++;
      }
    }
  }

  @override
  Stream<UserProfileState> mapEventToState(UserProfileEvent event) async* {
    if (event is LoadUserProfile) {
      try {
        var user = await getUserData();
        if (user != null) {
          if (user.trader.traderType == "personal") {
            KycPersonalService _service = KycPersonalService();
            await _service.statusIndividualKyc().then((res) async* {
              if (res != null) {
                var result = StatusTraderKycHelper.checkingIndividual(
                    StatusKycTrader.fromJson(res.data));
                yield UserProfileLoaded(
                  menus: menus,
                  user: user,
                  kycStatus: result.status,
                );
              } else {
                yield UserProfileError(
                  error: "Terjadi kesalahan, tidak dapat memuat status profil.",
                );
              }
            });
          }
        } else {
          yield UserProfileError(
            error: "Terjadi kesalahan, tidak dapat memuat profil.",
          );
        }
      } catch (e) {
        yield UserProfileError(
          error: "Terjadi kesalahan saat mencoba menampilkan data.",
        );
      }
    }
  }
}

class MenuModel {
  final String title;
  final String image;
  final String routes;

  MenuModel({this.title, this.image, this.routes});
}
