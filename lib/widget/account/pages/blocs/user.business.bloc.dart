/// Digunakan untuk menampilkan bisnis user ( jika user punya bisnis )
/// ada di halaman akun, tab bisnis anda
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/UserBusinessModel.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/financial_statements/FinanceService.dart';
import 'package:santaraapp/utils/logger.dart';

// Event
class UserBusinessEvent {}

// State
class UserBusinessState {}

// Event load data
class LoadUserBusiness extends UserBusinessEvent {}

// State jika data belum dimuat (pertama x buka page)
class UserBusinessUninitialized extends UserBusinessState {}

// State jika terjadi error
class UserBusinessError extends UserBusinessState {
  final String error;
  UserBusinessError({this.error});
}

// State jika user unauthorized

class UserBusinessUnauthorized extends UserBusinessState {}

// State jika load data berhasil
class UserBusinessLoaded extends UserBusinessState {
  final int pralisting; // Jika user memiliki bisnis yang sedang diajukan
  final List<UserBusinessModel> datas; // data daftar bisnis user
  UserBusinessLoaded({this.pralisting, this.datas});
}

// State jika data tidak ditemukan (kosong)
class UserBusinessNotFound extends UserBusinessState {}

// Bloc
class UserBusinessBloc extends Bloc<UserBusinessEvent, UserBusinessState> {
  UserBusinessBloc(UserBusinessState initialState) : super(initialState);

  // Manggil service
  FinanceService financeService = FinanceService();

  @override
  Stream<UserBusinessState> mapEventToState(UserBusinessEvent event) async* {
    // jika event adalah untuk memuat data
    if (event is LoadUserBusiness) {
      // set state menjadi uninitialized
      yield UserBusinessUninitialized();
      try {
        // mendapatkan data daftar bisnis user
        var result = await financeService.getUserBusinessList();
        // jika result tidak kosong ( null )
        if (result != null) {
          // jika berhasil request (status code 200)
          if (result.status == ApiStatus.success) {
            // inisialisasi variabel untuk nyimpen datanya
            List<UserBusinessModel> datas = [];
            // parsing data
            result.data['data']['list'].forEach((val) {
              var dummy = UserBusinessModel.fromJson(val);
              // menambahkan data kedalam variabel datas
              datas.add(dummy);
            });
            // data pralisting
            int pralisting = result.data['data']['pralisting'];
            // emit data kedalam state
            yield UserBusinessLoaded(
              datas: datas,
              pralisting: pralisting,
            );
          } else if (result.status == ApiStatus.notFound) {
            yield UserBusinessNotFound();
          } else if (result.status == ApiStatus.unauthorized) {
            yield UserBusinessUnauthorized();
          } else {
            // jika request gagal dilakukan ( statusCode 400/500/whatever lah )
            yield UserBusinessError(
              error: "[${result.message}] Tidak dapat memuat data!",
            );
          }
        } else {
          // jika request bernilai null
          yield UserBusinessError(
            error: "[no-value] Terjadi kesalahan saat mencoba merequest data!",
          );
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        // jika terjadi unexpected error
        yield UserBusinessError(
          error: "[01] Terjadi kesalahan saat mencoba merequest data!",
        );
      }
    }
  }
}
