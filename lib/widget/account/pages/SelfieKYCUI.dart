import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/services/auth/auth_service.dart';
import 'package:santaraapp/widget/security_token/user/SuccessChangeUI.dart';
import 'package:santaraapp/widget/widget/Camera.dart';

class SelfieKycUI extends StatefulWidget {
  final int fromReset;

  const SelfieKycUI({this.fromReset});

  @override
  _selfieUiState createState() => _selfieUiState();
}

class _selfieUiState extends State<SelfieKycUI> {
  final storage = new FlutterSecureStorage();

  File _image;
  final _authService = AuthService();
  bool isLoading = false;

  Future<bool> _onWillPop() async {
    return null ?? false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text('Foto KYC', style: TextStyle(color: Colors.black)),
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back_ios, color: Colors.black),
            )),
        body: isLoading == true
            ? LinearProgressIndicator(minHeight: 3)
            : Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: Text(
                        "Unggah Foto Selfie dengan Kartu Data Diri",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    // fieldWrapper.divider,
                    GestureDetector(
                        onTap: () async {
                          final result = await pickPhoto('camera');
                          if (result != null) _image = result;
                        },
                        child: Center(child: _imagePicker())),
                    GestureDetector(
                        onTap: () {
                          if (_image != null) {
                            submitKycNewImage(_image);
                          } else {
                            ToastHelper.showFailureToast(context,
                                'Silahkan melakukan selfie dengan KTP terlebih dahulu');
                          }
                        },
                        child:  Container(
                          height: 48.0,
                          margin: EdgeInsets.only(top: 10, left: 80, right: 80),
                          decoration: BoxDecoration(
                            color: Color(0xFFBF2D30),
                            border: Border.all(
                                color: Colors.transparent,
                                width: 2.0),
                            borderRadius:
                            BorderRadius.circular(
                                4.0),
                          ),
                          child: Center(
                              child: Text('Upload',
                                  style: TextStyle(
                                      color:
                                      Colors.white))),
                        ))
                  ],
                ),
              ),
      ),
    );
  }

  Future submitKycNewImage(File image) async {
    setState(() {
      isLoading = true;
    });
    await _authService.submitNewImage(image).then((value) {
      var data = jsonDecode(value);
      print(data);
      if (data != null && data['status']) {
        isLoading = false;
        showDialog(
          barrierDismissible: false,
            context: context,
            child: WillPopScope(
              onWillPop: _onWillPop,
              child: new AlertDialog(
                title: Text('Sukses'),
                content: Container(
                  child: Text('${data['data']['message']}'),
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      if (widget.fromReset == 1) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SuccessChangeUI(
                              message: "Perubahan password  berhasil.",
                            ),
                          ),
                        );
                      } else {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (route) => false);
                      }
                    },
                    child: Text('OK'),
                  )
                ],
              ),
            ));
      } else {
        setState(() {
          isLoading = false;
          ToastHelper.showFailureToast(context, data['error'][0]['message']);
        });
      }
    });
  }

  Widget _imagePicker() {
    return Center(
      child: DottedBorder(
        color: Color(0xFFB8B8B8),
        strokeWidth: 2,
        radius: Radius.circular(15),
        dashPattern: [5, 5],
        child: Container(
          width: 250,
          height: 150,
          decoration: BoxDecoration(
            color: Color(0xffF4F4F4),
            image: _image != null
                ? DecorationImage(image: FileImage(_image))
                : null,
          ),
          child: _image != null
              ? Container()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera_alt,
                      color: Color(0xFFB8B8B8),
                      size: 30,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Ambil Foto",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFB8B8B8),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }

  Future pickPhoto(String source) async {
    var cameras = await availableCameras();
    var frontCamera = cameras[1];
    File file;
    if (cameras.length == 0) {
      file = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      try {
        file = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => Camera(isKyc: 2, isSelfie: 1),
          ),
        );
        setState(() {
          if (file != null) {
            _image = File(file.path);
          }
        });
      } catch (e, stack) {
        ToastHelper.showFailureToast(context, e);
        return null;
      }
    }
  }
}
