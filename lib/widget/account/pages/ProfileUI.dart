import 'dart:convert';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/auth/auth_service.dart';
import 'package:santaraapp/services/security/pass_service.dart';
import 'package:santaraapp/services/time/time_services.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/account/pages/SelfieKYCUI.dart';
import 'package:santaraapp/widget/security_token/otp/OtpViewUI.dart';
import 'package:santaraapp/widget/security_token/pin/SecurityPinUI.dart';
import 'package:santaraapp/widget/security_token/user/change_password/ChangePasswordUI.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCountdownTimer.dart';

import '../../../blocs/current_time_bloc.dart';
import '../../../blocs/market/close_period_bloc.dart';
import '../../../features/dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../features/dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../features/dana/presentation/pages/dana_webview.dart';
import '../../../features/dana/presentation/widgets/dana_bottom_sheet.dart';
import '../../../features/dana/presentation/widgets/dana_overlay_notification.dart';
import '../../../features/dana/presentation/widgets/dana_popup.dart';
import '../../../features/deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../features/portofolio/presentation/bloc/portofolio_list_bloc.dart';
import '../../../helpers/Constants.dart';
import '../../../helpers/ImageViewerHelper.dart';
import '../../../helpers/PopupHelper.dart';
import '../../../helpers/RupiahFormatter.dart';
import '../../../helpers/kyc/StatusPerusahaan.dart';
import '../../../helpers/kyc/StatusTraderKyc.dart';
import '../../../models/StatusKycTrader.dart';
import '../../../pages/Home.dart';
import '../../../services/api_service.dart';
import '../../../services/kyc/KycPersonalService.dart';
import '../../../services/kyc/KycPerusahaanService.dart';
import '../../../services/market_trails/market_trails_services.dart';
import '../../../utils/api.dart';
import '../../../utils/helper.dart';
import '../../../utils/sizes.dart';
import '../../kyc/widgets/pra_kyc.dart';
import '../../setting_account/setting.dart';
import '../../widget/components/kyc/KycNotificationStatus.dart';
import '../../widget/components/kyc/blocs/kyc.notification.bloc.dart';
import '../../widget/components/main/SantaraAppBetaTesting.dart';
import '../../widget/components/main/SantaraCachedImage.dart';
import '../../widget/components/main/SantaraScaffoldNoState.dart';

class ProfileUI extends StatefulWidget {
  @override
  _ProfileUIState createState() => _ProfileUIState();
}

class _ProfileUIState extends State<ProfileUI> {
  final storage = new FlutterSecureStorage();
  var username, email, photo;
  final rupiah = new NumberFormat("#,##0");
  KycPersonalService _service = KycPersonalService();
  StatusKycTrader statusIndividual = StatusKycTrader();
  var result = {};
  Widget notif = Container();
  bool isCompleteKyc = true;
  bool isVerified = false;
  Widget next;
  User datas;
  var retryGetUser = 0;
  int requestTimes = 0;
  bool mainLoaded = false;

  // ignore: close_sinks
  KycNotifBloc bloc = KycNotifBloc(KycNotifUninitialized());
  ApiService apiService = ApiService();
  final _passService = PasswordApiService();
  final _authService = AuthService();
  final _timeService = TimeService();
  MarketTrailsServices _trailsServices = MarketTrailsServices();
  DateTime now;
  AuthDanaBloc _authDanaBloc;
  SaldoDepositBloc _saldoDepositBloc;
  DanaBalanceBloc _danaBalanceBloc;
  PortofolioListBloc _portofolioListBloc;
  ClosePeriodBloc _periodBloc;
  CurrentTimeBloc _currentTimeBloc;
  num saldo, dana, portofolio;
  bool isDanaActive = true;
  File _image;
  bool isLoading = false;
  bool imageLoading = false;
  bool isWithdraw = false;
  int statusKyc;
  String title = '';
  String message = '';

  int _errType = 0;
  int secs;
  bool _hasResend = false;
  Widget _timer = Container();
  Function onSuccess;
  bool _allowResend = false;
  User user;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  /// identifier untuk nandain apakah akun sedang dalam proses verifikasi
  /// untuk di-pass ke halaman pengaturan, dan digunakan sbg penentu apakah user hanya bisa lihat data kyc/edit data kyc
  EndStatus _kycStatusResult;

  // set status kyc ke localstorage / fluttersecurestorage
  Future saveKycStatus() async {
    await storage.write(key: Constants.statusKyc, value: '$isVerified');
  }

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    setState(() => isDanaActive = remoteConfig.getBool(isFeatureDanaActive));
  }

  Future retryChecking() async {
    if (retryGetUser < 3) {
      checking();
      setState(() => retryGetUser += retryGetUser + 1);
    }
  }

  Future checking({bool checkSession = true}) async {
    try {
      if (Helper.check) {
        // get user data
        var result = await apiService.getUserByUuid();
        // jika request berhasil
        if (result != null) {
          // jika result statusCode != 200 (error)
          if (result.statusCode != 200) {
            await retryChecking();
          } else {
            // parsing data result
            final data = userFromJson(json.encode(result.data));
            // set value ke setiap variable
            setState(() {
              datas = data;
              checkResetPassword();

              isVerified = data.trader.isVerified == 1 ? true : false;
              saveKycStatus();
              username = data.trader.name;
              email = data.email;
              photo = data.trader.traderType == "company"
                  ? data.trader.companyPhoto
                  : data.trader.photo;
              mainLoaded = true;
            });
            // jika trader adalah personal
            if (data.trader.traderType == "personal") {
              setState(() {
                notif = KycNotificationStatus(showCloseButton: false);
              });
              // lakukan pengecekan status kyc
              await checkKycStatus("personal");
            } else if (data.trader.traderType == "company") {
              // jika trader adalah perusahaan
              setState(() {
                notif = KycNotificationStatus(showCloseButton: false);
              });
              // lakukan pengecekan status kyc
              await checkKycStatus("company");
            } else if (data.trader == null ||
                data.trader.traderType == null ||
                data.trader.traderType == "") {
              // jika user belum memilih jenis trader
              setState(() {
                notif = KycNotificationStatus(showCloseButton: false);
                isCompleteKyc = false;
                next = PraKyc();
              });
            } else {
              setState(() {
                notif = KycNotificationStatus(showCloseButton: false);
                isCompleteKyc = false;
                next = PraKyc();
              });
            }
          }
        } else {
          // jika request gagal, lakukan request data ulang
          await retryChecking();
        }
      }
    } catch (e) {
      await retryChecking();
    }
  }

  getSecondsTime() async {
    try {
      await _timeService.otpExpiredTimeInSecond(false).then((value) {
        setState(() {
          _timer = Container();
          secs = value < 1 ? 1 : value;
          _timer = SantaraCountdownTimer(
              timerMaxSeconds: secs,
              onFinish: () {
                setState(() {
                  _allowResend = true;
                });
              });
        });
      });
    } catch (e) {
      // print(e);
      // print(stack);
    }
  }

  String maskPhone(String phone) {
    int mailLength = phone.length;
    var show = (mailLength / 2).floor();
    var hide = mailLength - show;
    var replace = ("*" * hide);
    return mailLength > 0
        ? phone.replaceRange(show, mailLength - 2, replace)
        : "$phone";
  }

  Future checkResetPassword() async {
    isLoading = true;
    try {
      await _passService.getCheckReset().then((value) {
        var data = json.decode(value.body);
        if (data != null && data['status']) {
          checkKyc();
        } else {
          isLoading = false;
          ToastHelper.showBasicToast(
              context, 'Silahkan ubah password terlebih dahulu');
          Future.delayed(Duration(seconds: 3)).then((value) {
            checkKyc();
            handleChangePassword();
          });
        }
      });
    } catch (err) {
      ToastHelper.showFailureToast(context, err);
    }
  }

  checkKyc() async {
    try {
      await _authService.checkStatusKyc().then((value) {
        var data = json.decode(value.body);
        statusKyc = data['data']['status'];

        //kyc ditolak / reject
        if (data != null && data['data']['status'] == 3) {
          setState(() {
            isLoading = false;
            isWithdraw = false;
            // message = 'Silahkan cek E-mail Anda untuk info selengkapnya';
            message = data['data']['message'];
            title = 'Ditolak';
          });

          // kyc disetujui
        } else if (data['data']['status'] == 2) {
          setState(() {
            isLoading = false;
            isWithdraw = true;
          });

          // kyc tunggu validasi 24jam
        } else if (data['data']['status'] == 1) {
          setState(() {
            isLoading = false;
            isWithdraw = false;
            message = data['data']['message'];
            title = 'Peringatan';
          });

          // belum kyc
        } else {
          //status false
          setState(() {
            isLoading = false;
            if (isVerified) {
              // ToastHelper.showBasicToast(
              //     context, 'Silahkan verikasi Kyc terlebih dahulu');
              isWithdraw = false;
              message = 'Silahkan verikasi Kyc terlebih dahulu';
              title = 'Peringatan';
              // Future.delayed(Duration(seconds: 3)).then((value) {
              //   handleCheckKyc();
              // });
            } else {
              print('isVerified$isVerified');
            }
          });
        }
      });
    } catch (err) {
      ToastHelper.showFailureToast(context, err);
    }
  }

  void handleCheckKyc() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SelfieKycUI(
          fromReset: 0,
        ),
      ),
    );
  }

  handleChangePassword() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          onSuccess: (pin, finger) async {
            PopupHelper.showLoading(context);
            try {
              await _passService.requestResetPassword().then((val) async {
                if (val.statusCode == 200) {
                  // pop loading
                  Navigator.pop(context);
                  // pop security pin ui
                  Navigator.pop(context);
                  var datas = userFromJson(await storage.read(key: 'user'));
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OtpViewUI(
                        user: datas,
                        onSuccess: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ChangePasswordUI(
                                  pin: pin,
                                  finger: finger,
                                  isNewResetPassword: statusKyc),
                            ),
                          );
                        },
                      ),
                    ),
                  );
                } else {
                  var validJson = isJson(val.body);
                  if (validJson) {
                    var parsed = json.decode(val.body);
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, parsed["message"]);
                  } else {
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, val.body);
                  }
                }
              });
            } catch (e) {
              ToastHelper.showFailureToast(
                  context, "Can't request reset password");
              // pop loading
              Navigator.pop(context);
            }
          },
          type: SecurityType.check,
          title: "Masukan PIN Anda",
        ),
      ),
    );
  }

  bool isJson(data) {
    var decodeSucceeded = false;
    try {
      var x = json.decode(data) as Map<String, dynamic>;
      decodeSucceeded = true;
    } on FormatException catch (e) {
      decodeSucceeded = false;
      // print('The provided string is not valid JSON');
    }
  }

  Future checkKycStatus(String traderType) async {
    try {
      if (traderType == "personal") {
        await _service.statusIndividualKyc().then((res) {
          if (res != null && res.statusCode == 200) {
            var data = StatusKycTrader.fromJson(res.data);
            var result = StatusTraderKycHelper.checkingIndividual(data);
            if (result != null) {
              setState(() {
                _kycStatusResult = result.status;
              });
            }
          }
        });
      } else {
        KycPerusahaanService _api = KycPerusahaanService();
        await _api.statusKycTrader().then((res) {
          if (res != null && res.statusCode == 200) {
            var result = StatusPerusahaanHelper.checkingStatus(
                StatusKycTrader.fromJson(res.data));
            if (result != null) {
              setState(() {
                _kycStatusResult = result.status;
              });
            }
          }
        });
      }
    } catch (e) {
      if (requestTimes < 3) {
        checkKycStatus(traderType);
        requestTimes++;
      }
    }
  }

  List<MenuModel> _menus = <MenuModel>[
    MenuModel(
        title: "Withdraw",
        image: "assets/icon_menu/new/Withdraw.png",
        routes: "/withdraw"),
    MenuModel(
        title: "Riwayat Pengguna",
        image: "assets/icon_menu/new/History.png",
        routes: "/journey"),
    MenuModel(
        title: "Dividen",
        image: "assets/icon_menu/new/Dividen.png",
        routes: "/dividend"),
    MenuModel(
        title: "Pasar Sekunder",
        image: "assets/icon_menu/new/Sekunder.png",
        routes: "/home_market"),
    MenuModel(
        title: "Video",
        image: "assets/icon_menu/new/Videos.png",
        routes: "/videos"),
  ].toList();

  // grid menus
  Widget _gridMenus(BuildContext context) {
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: _menus.length,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height -
                    (MediaQuery.of(context).size.width - 100)),
            crossAxisCount: 4),
        itemBuilder: (context, index) {
          return BlocBuilder<ClosePeriodBloc, ClosePeriodState>(
            builder: (context, statePeriod) {
              return BlocBuilder<CurrentTimeBloc, CurrentTimeState>(
                builder: (context, stateTime) {
                  return InkWell(
                    onTap: () async {
                      if (_menus[index].routes.isNotEmpty) {
                        if (_menus[index].routes == '/deposit') {
                          FirebaseAnalytics().logEvent(
                            name: 'app_select_deposit',
                            parameters: null,
                          );
                          await Navigator.pushNamed(
                              context, _menus[index].routes);
                        } else if (_menus[index].routes == '/withdraw') {
                          if (!isWithdraw) {
                            PopupHelper.warningInfo(
                              context,
                              "Withdraw ditutup",
                            );
                          } else {
                            FirebaseAnalytics().logEvent(
                              name: 'app_select_withdraw',
                              parameters: null,
                            );
                            await Navigator.pushNamed(
                                context, _menus[index].routes);
                          }
                        } else if (_menus[index].routes == '/home_market') {
                          if (statePeriod is ClosePeriodLoaded &&
                              stateTime is CurrentTimeLoaded) {
                            if (statePeriod.closePeriod
                                        .difference(stateTime.now)
                                        .inSeconds <
                                    1 ||
                                statePeriod.openPeriod
                                        .difference(stateTime.now)
                                        .inSeconds >
                                    1) {
                              PopupHelper.warningInfo(
                                context,
                                "Pasar sekunder ditutup",
                              );
                            } else {
                              await _trailsServices.createTrails(
                                  event: marketTrailsData["home_view"]["event"],
                                  note: marketTrailsData["home_view"]["note"]);
                              await Navigator.pushNamed(
                                  context, _menus[index].routes);
                            }
                          } else if (statePeriod is ClosePeriodLoading &&
                              stateTime is CurrentTimeLoading) {
                          } else {
                            PopupHelper.warningInfo(
                              context,
                              "Pasar sekunder ditutup",
                            );
                          }
                        } else if (_menus[index].routes == '/dividend') {
                          if (!isWithdraw) {
                            PopupHelper.warningInfo(
                              context,
                              "Dividen ditutup",
                            );
                          } else {
                            await Navigator.pushNamed(
                                context, _menus[index].routes);
                          }
                        } else {
                          await Navigator.pushNamed(
                              context, _menus[index].routes);
                        }
                        checking(checkSession: false);
                      } else {
                        PopupHelper.warningInfo(
                          context,
                          "Saat ini belum memasuki masa penawaran saham!",
                        );
                      }
                    },
                    child: Stack(children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                image: DecorationImage(
                                    image:
                                        ExactAssetImage(_menus[index].image))),
                          ),
                          Container(
                            height: 10,
                          ),
                          Text(
                            _menus[index].title,
                            style: TextStyle(
                                fontSize: 10, color: Color(ColorRev.mainwhite)),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                      _menus[index].routes == '/home_market'
                          ? statePeriod is ClosePeriodLoading ||
                                  stateTime is CurrentTimeLoading
                              ? Container(
                                  color: Colors.grey.withOpacity(0.2),
                                  child: Center(
                                      child: CupertinoActivityIndicator()))
                              : Container()
                          : Container()
                    ]),
                  );
                },
              );
            },
          );
        });
  }

  // end of grid menus

  // top section of user account
  Widget _accountTopSectionV2() {
    return Column(
      children: [
        // user data
        ListTile(
          contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 0),
          leading: photo == null
              ? Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      image: AssetImage('assets/icon/user.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              : ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: SantaraCachedImage(
                    height: 40,
                    width: 40,
                    image: GetAuthenticatedFile.convertUrl(
                      image: photo,
                      type: PathType.traderPhoto,
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
          title: username != null
              ? Text(
                  "$username",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(ColorRev.mainwhite)),
                )
              : mainLoaded
                  ? Text("-")
                  : Align(
                      alignment: Alignment.centerLeft,
                      child: CupertinoActivityIndicator(),
                    ),
          trailing: _kycStatusResult != null
              ? IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: Color(0xFFDDDDDD),
                    size: 30,
                  ),
                  onPressed: () async {
                    if (_kycStatusResult != null) {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Setting(
                            status: _kycStatusResult,
                          ),
                        ),
                      );
                      checking(checkSession: false);
                    }
                  },
                )
              : Container(
                  child: Text("-"),
                ),
        ),
        BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
          builder: (context, stateSaldo) {
            if (stateSaldo is SaldoDepositLoaded) {
              saldo = stateSaldo?.saldoDeposit?.idr ?? 0;
            } else if (stateSaldo is SaldoDepositEmpty ||
                stateSaldo is SaldoDepositError) {
              saldo = 0;
            }
            return BlocBuilder<PortofolioListBloc, PortofolioListState>(
              builder: (context, statePortofolio) {
                if (statePortofolio is PortofolioListLoaded) {
                  portofolio = statePortofolio?.portofolio?.total ?? 0;
                } else if (statePortofolio is PortofolioListError) {
                  portofolio = 0;
                }
                return BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
                  builder: (context, stateDana) {
                    if (stateDana is DanaBalanceLoaded) {
                      dana = stateDana?.balance ?? 0;
                    } else if (stateDana is DanaBalanceEmpty ||
                        stateDana is DanaBalanceError) {
                      dana = 0;
                    }
                    return Container(
                      // height: 50,
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      padding: EdgeInsets.all(Sizes.s10),
                      decoration: BoxDecoration(
                        color: Color(ColorRev.maingrey),
                        borderRadius: BorderRadius.circular(Sizes.s6),
                        boxShadow: [
                          BoxShadow(color: Color(0xFFE5E5E5), blurRadius: 1)
                        ],
                      ),
                      child: Column(
                        children: [
                          // menu
                          Row(children: [
                            // dompet
                            Expanded(
                              child: _menuAccount(
                                image: "assets/icon_menu/new/Deposit.png",
                                name: "Saldo Dompet",
                                isActive: true,
                                onTap: () =>
                                    Navigator.pushNamed(context, '/deposit'),
                                child: stateSaldo is SaldoDepositLoaded
                                    ? Text(
                                        RupiahFormatter.initialValueFormat(
                                            stateSaldo.saldoDeposit.idr
                                                .toString()),
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: Color(ColorRev.mainwhite)),
                                        textAlign: TextAlign.center,
                                      )
                                    : stateSaldo is SaldoDepositLoading ||
                                            stateSaldo is SaldoDepositInitial
                                        ? buildShimmer(12, 60)
                                        : Text(
                                            "-",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color:
                                                    Color(ColorRev.mainwhite)),
                                            textAlign: TextAlign.center,
                                          ),
                              ),
                            ),
                            Container(
                              height: Sizes.s50,
                              width: Sizes.s2,
                              margin:
                                  EdgeInsets.symmetric(horizontal: Sizes.s2),
                              color: Color(0xFFF0F0F0),
                            ),

                            // dana
                            // Expanded(
                            //     child: _menuAccount(
                            //   image: "assets/dana/dana.png",
                            //   name: "DANA",
                            //   isActive: isDanaActive,
                            //   onTap: () {
                            //     if (mainLoaded) {
                            //       if (datas.isDana == 1) {
                            //         if (stateDana is DanaBalanceLoaded) {
                            //           Navigator.push(
                            //             context,
                            //             MaterialPageRoute(
                            //               builder: (_) => DanaWebview(
                            //                 appbarName: "",
                            //                 url: stateDana.topupUrl,
                            //                 type: DanaWebviewType.trx,
                            //               ),
                            //             ),
                            //           );
                            //         }
                            //       } else {
                            //         if (isDanaActive) {
                            //           DanaBottomSheet.confirmConnectToDana(
                            //               context, () {
                            //             Navigator.pop(context);
                            //             _authDanaBloc.add(LoginDana());
                            //           });
                            //         } else {
                            //           PopupHelper.warningInfo(
                            //             context,
                            //             "Fitur DANA belum tersedia",
                            //           );
                            //         }
                            //       }
                            //     }
                            //   },
                            //   child: mainLoaded
                            //       ? datas.isDana == 1
                            //           ? _menuDana(stateDana)
                            //           : datas.isDana == 2
                            //               ? Row(
                            //                   mainAxisAlignment:
                            //                       MainAxisAlignment.center,
                            //                   children: [
                            //                     Text("Aktivasi",
                            //                         style: TextStyle(
                            //                             fontSize: 12,
                            //                             fontWeight:
                            //                                 FontWeight.bold,
                            //                             color: isDanaActive
                            //                                 ? Color(0xFF118EEA)
                            //                                 : Colors.grey)),
                            //                     GestureDetector(
                            //                       onTap: () => DanaPopup
                            //                           .resetPhoneNumber(
                            //                               context),
                            //                       child: Container(
                            //                         padding: EdgeInsets.all(4),
                            //                         child: Icon(Icons.info,
                            //                             size: Sizes.s12,
                            //                             color:
                            //                                 Color(0xFF118EEA)),
                            //                       ),
                            //                     ),
                            //                   ],
                            //                 )
                            //               : Text("Aktivasi",
                            //                   style: TextStyle(
                            //                       fontSize: 12,
                            //                       fontWeight: FontWeight.bold,
                            //                       color: isDanaActive
                            //                           ? Color(0xFF118EEA)
                            //                           : Colors.grey))
                            //       : buildShimmer(12, 60),
                            // )),
                            // Container(
                            //   height: Sizes.s50,
                            //   width: Sizes.s2,
                            //   margin:
                            //       EdgeInsets.symmetric(horizontal: Sizes.s2),
                            //   color: Color(0xFFF0F0F0),
                            // ),

                            //portofolio
                            Expanded(
                              child: _menuAccount(
                                  image: "assets/icon_menu/new/Portfolio.png",
                                  name: "Portofolio",
                                  isActive: true,
                                  onTap: () => Navigator.pushNamed(
                                      context, '/portfolio'),
                                  child: statePortofolio is PortofolioListLoaded
                                      ? Text(
                                          RupiahFormatter.initialValueFormat(
                                              statePortofolio.portofolio.total
                                                  .toString()),
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold,
                                              color: Color(ColorRev.mainwhite)),
                                          textAlign: TextAlign.center,
                                        )
                                      : statePortofolio is PortofolioListLoading
                                          ? buildShimmer(12, 60)
                                          : Text(
                                              "-",
                                              style: TextStyle(
                                                  color:
                                                      Color(ColorRev.mainwhite),
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                              textAlign: TextAlign.center,
                                            )),
                            ),
                          ]),
                          Container(
                            height: 2,
                            margin: EdgeInsets.all(Sizes.s10),
                            color: Color(0xFFF0F0F0),
                          ),

                          // total aset
                          Padding(
                            padding: EdgeInsets.fromLTRB(
                                Sizes.s10, Sizes.s5, Sizes.s10, Sizes.s10),
                            child: Row(
                              children: [
                                Text(
                                  "Total Aset",
                                  style: TextStyle(
                                      color: Color(ColorRev.mainwhite)),
                                ),
                                Container(width: 6),
                                GestureDetector(
                                    onTap: () {
                                      PopupHelper.showInfo(
                                        context,
                                        "Total Aset",
                                        "Total Aset adalah gabungan dari Saldo Rupiah, Saldo DANA dan total saham yang anda miliki.",
                                      );
                                    },
                                    child: Icon(Icons.help,
                                        color: Color(0xFFDADADA), size: 14)),
                                Container(width: 20),
                                saldo == null ||
                                        dana == null ||
                                        portofolio == null
                                    ? buildShimmer(12, 70)
                                    : Text(
                                        RupiahFormatter.initialValueFormat(
                                            (saldo +
                                                    (datas.isDana == 1
                                                        ? dana
                                                        : 0) +
                                                    portofolio)
                                                .toString()),
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: Color(ColorRev.mainwhite)),
                                        textAlign: TextAlign.center,
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              },
            );
          },
        )
      ],
    );
  }

  Widget _menuDana(DanaBalanceState stateDana) {
    if (stateDana is DanaBalanceLoaded) {
      return Text(
        RupiahFormatter.initialValueFormat(stateDana.balance.toString()),
        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      );
    } else if (stateDana is DanaBalanceError || stateDana is DanaBalanceEmpty) {
      return Text(
        "-",
        style: TextStyle(
            color: Colors.red, fontSize: 12, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      );
    } else {
      return buildShimmer(12, 60);
    }
  }

  Widget _menuAccount(
      {String image,
      String name,
      Widget child,
      bool isActive,
      Function onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            image,
            height: 40,
            width: 40,
            fit: BoxFit.contain,
            color: isActive ? null : Colors.grey,
          ),
          Container(
            height: 10,
          ),
          Text(
            name,
            style: TextStyle(fontSize: 10, color: Color(ColorRev.mainwhite)),
            textAlign: TextAlign.center,
          ),
          Container(height: 2),
          child,
        ],
      ),
    );
  }

  initializeAccount() async {
    try {
      checking();
    } catch (e) {
      // print(e);
      setState(() {
        mainLoaded = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initializeAccount();
    getRemoteConfig();
    //saldo
    _saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    _saldoDepositBloc.add(LoadSaldoDeposit(0));
    // dana  balance
    _danaBalanceBloc = BlocProvider.of<DanaBalanceBloc>(context);
    _danaBalanceBloc.add(LoadDanaBalance());
    // portofolio
    _portofolioListBloc = BlocProvider.of<PortofolioListBloc>(context);
    _portofolioListBloc.add(LoadPortofolioList(0));
    // auth dana
    _authDanaBloc = BlocProvider.of<AuthDanaBloc>(context);
    // market period
    _periodBloc = BlocProvider.of<ClosePeriodBloc>(context);
    _periodBloc.add(ClosePeriodRequested());
    // current time
    _currentTimeBloc = BlocProvider.of<CurrentTimeBloc>(context);
    _currentTimeBloc.add(CurrentTimeRequested());
  }

  @override
  Widget build(BuildContext context) {
    if (mainLoaded) {
      return BlocConsumer<AuthDanaBloc, AuthDanaState>(
        listener: (context, state) {
          if (state is LoginDanaSuccess) {
            final result = Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => DanaWebview(
                          appbarName: "",
                          url: state.url,
                          type: DanaWebviewType.auth,
                        )));
            result.then((value) {
              if (value != null) {
                _authDanaBloc.add(ApplyToken(authCode: value));
              }
            });
          }
          if (state is ApplyTokenSuccess) {
            Navigator.pushAndRemoveUntil(context,
                MaterialPageRoute(builder: (_) => Home()), (route) => false);
            DanaOverlayNotifications.showDanaOverlayNotification(
                "Akun DANA berhasil diaktivasi");
          }
          if (state is AuthDanaFailed) {
            DanaBottomSheet.danaActivationFailed(context, () {
              Navigator.pop(context);
              _authDanaBloc.add(LoginDana());
            });
          }
        },
        builder: (context, state) {
          return RefreshIndicator(
            color: Colors.grey,
            onRefresh: checking,
            child: ListView(
              children: <Widget>[
                isVerified ? Container() : notif,
                isLoading
                    ? LinearProgressIndicator(minHeight: 3)
                    : !isVerified
                        ? Container()
                        : isWithdraw == true
                            ? Container()
                            : statusKycWidget(title, message),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: SantaraAppBetaTesting(),
                ),
                SizedBox(height: Sizes.s10),
                // _accountTopSection(context),
                _accountTopSectionV2(),
                SizedBox(height: Sizes.s40),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: Sizes.s20),
                  child: _gridMenus(context),
                ),
              ],
            ),
          );
        },
      );
    } else {
      return Center(child: CupertinoActivityIndicator());
    }
  }

  Widget statusKycWidget(String title, String message) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(Sizes.s15),
          decoration: BoxDecoration(
            border: Border.all(
                width: 1,
                color:
                    statusKyc == 3 ? Colors.red.shade900 : Color(0xffF6911A)),
            borderRadius: BorderRadius.circular(Sizes.s10),
            color: statusKyc == 3 ? Colors.red.shade200 : Color(0xffFDE9CD),
          ),
          width: double.maxFinite,
          child: Container(
            margin: EdgeInsets.only(
                left: Sizes.s25,
                right: Sizes.s25,
                top: Sizes.s15,
                bottom: Sizes.s15),
            // color: Colors.blue,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                statusKyc == 3
                    ? Container()
                    : Icon(
                        Icons.info,
                        color: Color(0xffF6911A),
                        size: FontSize.s25,
                      ),
                SizedBox(
                  width: Sizes.s10,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "$title",
                        style: TextStyle(
                          fontSize: FontSize.s15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Flexible(
                        child: GestureDetector(
                          onTap: () {
                            if (statusKyc == 0) {
                              handleCheckKyc();
                            } else {
                              null;
                            }
                          },
                          child: Text(
                            "$message",
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              fontSize: FontSize.s14,
                              decorationStyle: TextDecorationStyle.solid,
                            ),
                          ),
                        ),
                      ),
                      statusKyc == 3
                          ? Flexible(
                              child: GestureDetector(
                                onTap: () => handleCheckKyc(),
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text('Verifikasi Ulang',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        decorationStyle:
                                            TextDecorationStyle.solid,
                                        decoration: TextDecoration.underline,
                                      )),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class MenuModel {
  final String title;
  final String image;
  final String routes;

  MenuModel({this.title, this.image, this.routes});
}
