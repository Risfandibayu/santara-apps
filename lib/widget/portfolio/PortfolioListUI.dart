import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../core/usecases/unauthorize_usecase.dart';
import '../../helpers/PopupHelper.dart';
import '../../utils/sizes.dart';
import '../home/AllPenerbitList.dart';
import '../widget/components/charts/build_chart.dart';
import '../widget/components/main/SantaraAppBetaTesting.dart';
import '../widget/components/main/SantaraButtons.dart';
import '../widget/components/main/SantaraDropDown.dart';
import '../widget/components/main/SantaraScaffoldNoState.dart';
import 'PortfolioDetailUI.dart';
import 'blocs/portfolio.list.bloc.dart';

class PortfolioListUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PortfolioListBloc>(
      create: (_) => PortfolioListBloc(PortfolioListUninitialized())
        ..add(LoadData(sort: "desc", category: "")),
      child: PortfolioListContent(),
    );
  }
}

class PortfolioListContent extends StatefulWidget {
  @override
  _PortfolioListContentState createState() => _PortfolioListContentState();
}

class _PortfolioListContentState extends State<PortfolioListContent> {
  bool _isEmpty = false;
  PortfolioListBloc bloc;
  final rupiah = NumberFormat("#,##0");
  final DateFormat dateFormat = DateFormat("dd MMMM yyyy", "id");
  TextEditingController searchKey = TextEditingController();

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    bloc = BlocProvider.of<PortfolioListBloc>(context);
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  // no saham
  Widget _buildNoSaham() {
    return Container(
      margin: EdgeInsets.all(Sizes.s20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/icon/no_business.png",
            width: Sizes.s250,
          ),
          SizedBox(height: Sizes.s30),
          Text(
            "Belum Ada Saham yang Dimiliki",
            style: TextStyle(
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: Sizes.s5),
          Text(
            "Anda belum memiliki saham. Yuk, pilih bisnis yang bagus-bagus",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: FontSize.s12,
              fontWeight: FontWeight.w300,
            ),
          ),
          SizedBox(height: Sizes.s20),
          SantaraMainButton(
            title: "Beli Saham",
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AllPenerbitList(),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PortfolioListBloc, PortfolioListState>(
      builder: (context, state) {
        if (state is PortfolioListLoaded) {
          PortfolioListLoaded portfolio = state;
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: state.isSearching
                ? AppBar(
                    titleSpacing: 0,
                    automaticallyImplyLeading: false,
                    title: Container(
                      padding: EdgeInsets.only(right: 20),
                      height: Sizes.s40,
                      child: TextField(
                        controller: searchKey,
                        onSubmitted: (String value) {
                          bloc..add(SearchPortfolio(keyword: value));
                        },
                        decoration: InputDecoration(
                          hintText: "Contoh : Ayam",
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(40.0),
                            ),
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                          fillColor: Colors.grey[200],
                          filled: true,
                          suffixIcon: IconButton(
                            padding: EdgeInsets.only(right: 10),
                            color: Colors.black,
                            icon: Icon(Icons.search),
                            onPressed: () => bloc
                              ..add(SearchPortfolio(keyword: searchKey.text)),
                          ),
                        ),
                      ),
                    ),
                    backgroundColor: Colors.white,
                    leading: IconButton(
                      icon: Icon(Icons.close),
                      color: Colors.black,
                      onPressed: () => bloc..add(ShowSearchBar()),
                    ),
                  )
                : AppBar(
                    title: Text(
                      "Portofolio",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    iconTheme: IconThemeData(color: Colors.black),
                    centerTitle: true,
                    backgroundColor: Colors.white,
                    actions: [
                      IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () => bloc..add(ShowSearchBar()),
                      )
                    ],
                  ),
            body: Container(
              color: Colors.white,
              height: double.maxFinite,
              width: double.maxFinite,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: SantaraAppBetaTesting(),
                    ),
                    // Nilai saham
                    Container(
                      margin: EdgeInsets.all(Sizes.s20),
                      height: Sizes.s60,
                      padding: EdgeInsets.all(Sizes.s10),
                      decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Color(0xffDDDDDD),
                          ),
                          borderRadius: BorderRadius.circular(Sizes.s5)),
                      child: Row(
                        children: [
                          Text(
                            "Nilai Saham",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.info,
                              size: FontSize.s18,
                              color: Color(0xffB8B8B8),
                            ),
                            onPressed: () => PopupHelper.showInfo(
                              context,
                              "Nilai Saham",
                              "Gabungan total saham yang Anda miliki dalam nominal Rupiah.",
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Rp. ${rupiah.format(portfolio?.data?.total ?? 0)}",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: FontSize.s14,
                            ),
                          )
                        ],
                      ),
                    ), // Filter
                    // Filter
                    Container(
                      margin:
                          EdgeInsets.only(left: Sizes.s20, right: Sizes.s20),
                      child: Row(
                        children: [
                          // Icon(
                          //   Icons.list,
                          //   size: FontSize.s25,
                          // ),
                          // SizedBox(width: Sizes.s20),
                          Expanded(
                            flex: 1,
                            child: SantaraDropDown(
                              icon: Image.asset(
                                "assets/icon/category_filter.png",
                              ),
                              isExpanded: true,
                              hint: Text(
                                  portfolio?.selectedCategory?.category ??
                                      "Semua"),
                              items: portfolio.categories
                                  .map(
                                    (e) => DropdownMenuItem(
                                      value: e.id,
                                      child: Text(
                                        e.category,
                                        // overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                  .toList(),
                              value: portfolio?.selectedCategory?.id,
                              onChanged: (selected) {
                                var first = portfolio.categories
                                    .where((e) => e.id == selected)
                                    .first;
                                bloc..add(SelectCategory(selected: first));
                              },
                            ),
                          ),
                          SizedBox(width: Sizes.s20),
                          Expanded(
                            flex: 1,
                            child: SantaraDropDown(
                              icon: Image.asset(
                                "assets/icon/sort_filter.png",
                              ),
                              isExpanded: true,
                              hint: Text(
                                portfolio.selectedSort != null
                                    ? portfolio.selectedSort['sort']
                                    : "Terbaru",
                              ),
                              items: portfolio.sort
                                  .map(
                                    (value) => DropdownMenuItem(
                                      value: value['value'],
                                      child: Text(
                                        value['sort'],
                                        // overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                  .toList(),
                              value: portfolio?.selectedSort != null
                                  ? portfolio?.selectedSort["value"]
                                  : null,
                              onChanged: (selected) {
                                var first = portfolio.sort
                                    .where((e) => e["value"] == selected)
                                    .first;
                                bloc..add(SelectSort(selected: first));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: Sizes.s20),
                    // Content List
                    portfolio?.data?.data?.length != null &&
                            portfolio.data.data.length > 0
                        ? ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: portfolio.dataLength <
                                    portfolio.data.data.length
                                ? portfolio.dataLength + 1
                                : portfolio.data.data.length,
                            itemBuilder: (context, index) {
                              var saham = portfolio.data.data[index];
                              return index >= portfolio.dataLength
                                  ? InkWell(
                                      onTap: () =>
                                          bloc..add(LoadMorePortfolio()),
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              bottom: Sizes.s20),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.keyboard_arrow_down,
                                                  color: Color(0xFF218196)),
                                              Container(width: Sizes.s12),
                                              Text(
                                                "Lihat Selengkapnya",
                                                style: TextStyle(
                                                    fontSize: FontSize.s16,
                                                    color: Color(0xFF218196),
                                                    fontWeight:
                                                        FontWeight.w600),
                                              )
                                            ],
                                          )),
                                    )
                                  : InkWell(
                                      onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              PortfolioDetailUI(
                                            uuid: "${saham.uuid}",
                                            title: "${saham.trademark}",
                                          ),
                                        ),
                                      ),
                                      child: Container(
                                        margin: EdgeInsets.all(Sizes.s20),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: Color(0xffDADADA),
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(Sizes.s5),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      Sizes.s5),
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    left: Sizes.s10,
                                                    right: Sizes.s10),
                                                alignment: Alignment.centerLeft,
                                                height: Sizes.s40,
                                                width: double.maxFinite,
                                                color: Color(0xffF0F0F0),
                                                child: Text(
                                                    "Tanggal Pembelian : ${dateFormat.format(saham.trxDate)}"),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.all(Sizes.s10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${saham.category}",
                                                    style: TextStyle(
                                                      color: Color(0xff292F8D),
                                                      fontSize: FontSize.s10,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    "${saham.trademark}",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: FontSize.s14,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                    ),
                                                  ),
                                                  Text(
                                                    "${saham.companyName}",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: FontSize.s10,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: Sizes.s15,
                                                        bottom: Sizes.s10),
                                                    height: 1,
                                                    color: Color(0xffDADADA),
                                                    width: double.maxFinite,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "Total Saham Dimiliki (Rp)",
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize:
                                                              FontSize.s10,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                      Text(
                                                        "Rp. ${rupiah.format(saham?.totalSaham ?? 0)}",
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize:
                                                              FontSize.s12,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              // margin:
                                              //     EdgeInsets.only(bottom: Sizes.s10),
                                              height: 1,
                                              color: Color(0xffDADADA),
                                              width: double.maxFinite,
                                            ),
                                            Theme(
                                              data: Theme.of(context).copyWith(
                                                  dividerColor:
                                                      Colors.transparent),
                                              child: ListTileTheme(
                                                contentPadding: EdgeInsets.only(
                                                    left: Sizes.s10,
                                                    right: Sizes.s10),
                                                child: ExpansionTile(
                                                  title: Text(""),
                                                  children: [
                                                    BuildFinancialChart(
                                                        data: saham)
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                            },
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Center(
                                child: Container(
                                  height: Sizes.s350,
                                  child: Image.asset(
                                    "assets/icon/search_not_found.png",
                                  ),
                                ),
                              ),
                              SizedBox(height: Sizes.s20),
                              Center(
                                child: Text(
                                  "Portofolio Tidak Ditemukan!",
                                  style: TextStyle(
                                    fontSize: FontSize.s16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              )
                            ],
                          )
                  ],
                ),
              ),
            ),
          );
        } else if (state is PortfolioListUninitialized) {
          return BuildScaffoldNoState(
            title: "Portofolio",
            content: Container(
              margin: EdgeInsets.all(Sizes.s20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildShimmer(Sizes.s50, double.maxFinite),
                    SizedBox(height: Sizes.s20),
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: buildShimmer(Sizes.s50, double.maxFinite),
                        ),
                        SizedBox(width: Sizes.s20),
                        Expanded(
                          flex: 2,
                          child: buildShimmer(Sizes.s50, double.maxFinite),
                        ),
                        SizedBox(width: Sizes.s20),
                        Expanded(
                          flex: 2,
                          child: buildShimmer(Sizes.s50, double.maxFinite),
                        )
                      ],
                    ),
                    SizedBox(height: Sizes.s20),
                    buildShimmer(Sizes.s200, double.maxFinite),
                    SizedBox(height: Sizes.s20),
                    buildShimmer(Sizes.s200, double.maxFinite),
                    SizedBox(height: Sizes.s20),
                    buildShimmer(Sizes.s200, double.maxFinite),
                  ],
                ),
              ),
            ),
          );
        } else if (state is PortfolioListError) {
          return BuildScaffoldNoState(
              title: "Portofolio",
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("${state.error}"),
                  IconButton(
                    icon: Icon(Icons.replay),
                    onPressed: () => bloc
                      ..add(LoadData(
                        category: state.category,
                        sort: state.sort,
                      )),
                  )
                ],
              ));
        } else if (state is PortfolioListEmpty) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text(
                "Portofolio",
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              iconTheme: IconThemeData(color: Colors.black),
              centerTitle: true,
              backgroundColor: Colors.white,
            ),
            body: Container(
              color: Colors.white,
              height: double.maxFinite,
              width: double.maxFinite,
              child: _buildNoSaham(),
            ),
          );
        } else {
          return BuildScaffoldNoState(
            title: "Portofolio",
            content: Center(
              child: Container(),
            ),
          );
        }
      },
    );
  }
}
