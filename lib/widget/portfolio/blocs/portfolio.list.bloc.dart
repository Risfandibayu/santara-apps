import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/BidangUsaha.dart';
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/financial_statements/PortfolioService.dart';
import 'package:santaraapp/utils/logger.dart';

// Event
class PortfolioListEvent {}

// State
class PortfolioListState {}

// Event load data list portfolio
class LoadData extends PortfolioListEvent {
  final String keyword;
  final String category;
  final String sort;

  LoadData({this.keyword = "", this.category = "", this.sort = ""});
}

// Event menampilkan searchbar (di appbar)
class ShowSearchBar extends PortfolioListEvent {}

// Event ketika user memilih urut berdasarkan ..
class SelectSort extends PortfolioListEvent {
  Map selected;
  SelectSort({this.selected});
}

// Event ketika user memilih kategori
class SelectCategory extends PortfolioListEvent {
  BidangUsaha selected;
  SelectCategory({this.selected});
}

// Event ketika user mencari daftar portfolio
class SearchPortfolio extends PortfolioListEvent {
  String keyword;
  SearchPortfolio({this.keyword});
}

// Event ketika user tap lihat selengkapnya
class LoadMorePortfolio extends PortfolioListEvent {}

// State ketika pertama x membuka page
class PortfolioListUninitialized extends PortfolioListState {}

// State ketika terjadi error
class PortfolioListError extends PortfolioListState {
  String category;
  String sort;
  String error;
  PortfolioListError({this.error, this.category, this.sort});

  PortfolioListError copyWith({String error}) {
    return PortfolioListError(error: error ?? this.error);
  }
}

// State ketika sesi telah berakhir
class PortfolioListUnauthorized extends PortfolioListState {}

// State ketika berhasil memuat data list
class PortfolioListLoaded extends PortfolioListState {
  PortfolioListModel data;
  List<Map> sort;
  List<BidangUsaha> categories;
  String keyword;
  Map selectedSort;
  BidangUsaha selectedCategory;
  bool isSearching;
  int dataLength;

  PortfolioListLoaded({
    this.data,
    this.sort,
    this.categories,
    this.keyword,
    this.selectedSort,
    this.selectedCategory,
    this.isSearching = false,
    this.dataLength,
  });

  PortfolioListLoaded copyWith({
    PortfolioListModel data,
    List<Map> sort,
    List<BidangUsaha> categories,
    String keyword,
    Map selectedSort,
    BidangUsaha selectedCategory,
    bool isSearching,
    int dataLength,
  }) {
    return PortfolioListLoaded(
      data: data ?? this.data,
      sort: sort ?? this.sort,
      categories: categories ?? this.categories,
      keyword: keyword ?? this.keyword,
      selectedSort: selectedSort ?? this.selectedSort,
      selectedCategory: selectedCategory ?? this.selectedCategory,
      isSearching: isSearching ?? this.isSearching,
      dataLength: dataLength ?? this.dataLength,
    );
  }
}

// State ketika data kosong
class PortfolioListEmpty extends PortfolioListState {}

// Bloc
class PortfolioListBloc extends Bloc<PortfolioListEvent, PortfolioListState> {
  // Constructor
  PortfolioListBloc(PortfolioListState initialState) : super(initialState);
  PortfolioService apiService = PortfolioService(); // api service portfolio
  ApiService service = ApiService(); // api service

  @override
  Stream<PortfolioListState> mapEventToState(PortfolioListEvent event) async* {
    // Pertama kali membuka list portfolio
    if (event is LoadData) {
      // emit state uninitialized (biar loadingnya keliatan)
      yield PortfolioListUninitialized();
      // load daftar kategori
      List<BidangUsaha> categories = [];
      try {
        categories.insert(0, BidangUsaha(id: 666, category: "Semua"));
      } catch (e, stack) {
        santaraLog(e, stack);
      }
      await service.getBidangUsaha().then((value) {
        categories.addAll(value);
      });
      // api call daftar portfoloio
      try {
        var result = await apiService.getPortfolios(
          event.keyword,
          event.category,
          event.sort,
        );
        // Jika status code == 200 (berhasil)
        if (result.status == ApiStatus.success) {
          // parsing data
          var data = PortfolioListModel.fromJson(result.data);
          // jika data tidak ada / kosong
          if (data?.data?.length != null && data.data.length > 0) {
            // Data urutkan berdasar
            List<Map> sortBy = [
              // {"id": "0", "sort": "Semua", "value": ""},
              {"id": "1", "sort": "Terbaru", "value": "desc"},
              {"id": "2", "sort": "Terlama", "value": "asc"},
              {"id": "3", "sort": "Nama Emiten A-Z", "value": "name_asc"},
              {"id": "4", "sort": "Nama Emiten Z-A", "value": "name_desc"},
              {
                "id": "5",
                "sort": "Total Saham Tertinggi",
                "value": "saham_desc"
              },
              {"id": "6", "sort": "Total Saham Terendah", "value": "saham_asc"},
            ];
            int dataLength = data?.data?.length ?? 0;
            // Jika jumlah portfolio > 5
            if (dataLength > 5) {
              dataLength = 5;
            }

            // Emit data ke state loaded
            yield PortfolioListLoaded(
              data: data,
              categories: categories,
              sort: sortBy,
              dataLength: dataLength,
            );
          } else {
            yield PortfolioListEmpty();
          }
        } else if (result.status == ApiStatus.unauthorized) {
          yield PortfolioListUnauthorized();
        } else if (result.status == ApiStatus.notFound) {
          yield PortfolioListEmpty();
        } else {
          // jika status code != 200 (error)
          // emit data error ke state error
          yield PortfolioListError(
            error: "[${result.message}] Tidak dapat menerima data!",
            sort: event.sort,
            category: event.category,
          );
        }
      } catch (e, stack) {
        // Jika terjadi unexpected error
        santaraLog(e, stack);
        yield PortfolioListError(
          error: e.toString(),
          sort: event.sort,
          category: event.category,
        );
      }
    } else if (event is ShowSearchBar) {
      // jika event menampilkan searchbar
      if (state is PortfolioListLoaded) {
        PortfolioListLoaded data = state;
        yield data.copyWith(isSearching: data.isSearching ? false : true);
      }
    } else if (event is SelectSort) {
      // jika event memilih urut berdasarkan
      if (state is PortfolioListLoaded) {
        PortfolioListLoaded data = state;
        try {
          yield PortfolioListUninitialized();
          // call api sorting data
          var result = await apiService.getPortfolios(
            data.keyword,
            "${data?.selectedCategory?.id ?? ''}",
            event.selected["value"],
          );
          // jika result berhasil
          if (result.status == ApiStatus.success) {
            // parsing data
            var response = PortfolioListModel.fromJson(result.data);
            // emit state loaded
            yield data.copyWith(data: response, selectedSort: event.selected);
          } else {
            // jika status code != 200 (error)
            // emit data error ke state error
            yield PortfolioListError(
              error: "[${result.message}] Tidak dapat menerima data!",
              sort: event?.selected["value"] ?? '',
              category: "${data?.selectedCategory?.id ?? ''}",
            );
          }
        } catch (e, stack) {
          santaraLog(e, stack);
          yield PortfolioListError(
            error: "[08] Tidak dapat memuat data!",
            sort: event.selected["value"],
            category: "${data.selectedCategory.id}",
          );
        }
      }
    } else if (event is SelectCategory) {
      // jika event memilih kategori
      if (state is PortfolioListLoaded) {
        PortfolioListLoaded data = state;
        try {
          yield PortfolioListUninitialized();
          // call api select category data
          var result = await apiService.getPortfolios(
            data.keyword,
            "${event?.selected?.id ?? ''}",
            data?.selectedSort != null ? data?.selectedSort["value"] : '',
          );
          // jika result berhasil
          if (result.status == ApiStatus.success) {
            // parsing data
            var response = PortfolioListModel.fromJson(result.data);
            // emit state loaded
            yield data.copyWith(
              data: response,
              selectedCategory: event.selected,
            );
          } else {
            // jika status code != 200 (error)
            // emit data error ke state error
            yield PortfolioListError(
              error: "[${result.message}] Tidak dapat menerima data!",
              sort:
                  data?.selectedSort != null ? data?.selectedSort["value"] : '',
              category: "${event?.selected?.id ?? ''}",
            );
          }
        } catch (e) {
          yield PortfolioListError(
            error: "[09] Tidak dapat memuat data!",
            sort: data?.selectedSort != null ? data?.selectedSort["value"] : '',
            category: "${event?.selected?.id ?? ''}",
          );
        }
      }
    } else if (event is SearchPortfolio) {
      // jika event adalah mencari daftar portfolio
      if (state is PortfolioListLoaded) {
        PortfolioListLoaded data = state;
        try {
          yield PortfolioListUninitialized();
          // call api select category data
          var result = await apiService.getPortfolios(
            event?.keyword ?? '',
            "${data?.selectedCategory?.id ?? ''}",
            data?.selectedSort != null ? data?.selectedSort["value"] : '',
          );
          // jika result berhasil
          if (result.status == ApiStatus.success) {
            // parsing data
            var response = PortfolioListModel.fromJson(result.data);
            // emit state loaded
            yield data.copyWith(
              data: response,
              keyword: event.keyword,
            );
          } else {
            // jika status code != 200 (error)
            // emit data error ke state error
            yield PortfolioListError(
              error: "[${result.message}] Tidak dapat menerima data!",
              sort:
                  data?.selectedSort != null ? data?.selectedSort["value"] : '',
              category: "${data?.selectedCategory?.id ?? ''}",
            );
          }
        } catch (e, stack) {
          santaraLog(e, stack);
          yield PortfolioListError(
            error: "[10] Tidak dapat memuat data!",
            sort: data?.selectedSort != null ? data?.selectedSort["value"] : '',
            category: "${data?.selectedCategory?.id ?? ''}",
          );
        }
      }
    } else if (event is LoadMorePortfolio) {
      if (state is PortfolioListLoaded) {
        PortfolioListLoaded data = state;
        if ((data.data.data.length - data.dataLength) > 5) {
          yield data.copyWith(dataLength: data.dataLength + 5);
        } else {
          yield data.copyWith(dataLength: data.data.data.length);
        }
      }
    }
  }
}
