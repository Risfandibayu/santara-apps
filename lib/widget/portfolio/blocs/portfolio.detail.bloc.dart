import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/models/ListPortfolio.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/financial_statements/PortfolioService.dart';
import 'package:santaraapp/utils/logger.dart';

// Event
class PortfolioDetailEvent {}

// State
class PortfolioDetailState {}

// Event ketika memuat data portfolio (Pertama buka page)
class LoadPortfolioDetail extends PortfolioDetailEvent {
  final String uuid;
  LoadPortfolioDetail(this.uuid);
}

// State ketika user pertama x membuka page (masi kosong)
class PortfolioDetailUninitialized extends PortfolioDetailState {}

// State ketika user unauthorized
class PortfolioDetailUnauthorized extends PortfolioDetailState {}

// State ketika berhasil memuat data portfolio
class PortfolioDetailLoaded extends PortfolioDetailState {
  final SahamData data;
  PortfolioDetailLoaded({this.data});

  PortfolioDetailLoaded copyWith({SahamData data}) {
    return PortfolioDetailLoaded(
      data: data ?? this.data,
    );
  }
}

// State ketika terjadi error saat memuat data protfolio
class PortfolioDetailError extends PortfolioDetailState {
  String error; // pesan error
  String uuid; // uuid detail emiten
  PortfolioDetailError({this.error, this.uuid});

  PortfolioDetailError copyWith({String error}) {
    return PortfolioDetailError(
      error: error ?? this.error,
      uuid: uuid ?? this.uuid,
    );
  }
}

// State ketika data tidak ditemukan
class PortfolioDetailEmpty extends PortfolioDetailState {}

// Bloc
class PortfolioDetailBloc
    extends Bloc<PortfolioDetailEvent, PortfolioDetailState> {
  // Constructor
  PortfolioDetailBloc(PortfolioDetailState initialState) : super(initialState);
  PortfolioService apiService = PortfolioService(); // portfolio service
  ApiService service = ApiService();

  @override
  Stream<PortfolioDetailState> mapEventToState(
      PortfolioDetailEvent event) async* {
    // Jika event adalah memuat portfolio detail
    if (event is LoadPortfolioDetail) {
      // emit uninitialized (biar muncul loadingnye)
      yield PortfolioDetailUninitialized();
      // api calling start
      var result = await apiService.getPortfolio(event.uuid);
      // jika resultnya tidak kosong
      if (result != null) {
        try {
          // jika status code == 200 (berhasil)
          if (result.status == ApiStatus.success) {
            // parsing data
            var portfolioDetail = SahamData.fromJson(result.data["data"]);
            // emit data ke state loaded
            yield PortfolioDetailLoaded(data: portfolioDetail);
          } else if (result.status == ApiStatus.notFound) {
            yield PortfolioDetailEmpty();
          } else if (result.status == ApiStatus.unauthorized) {
            yield PortfolioDetailUnauthorized();
          } else {
            // jika error / status code != 200
            // emit error ke state error
            yield PortfolioDetailError(
              error: "[${result.message}] Tidak dapat mengambil data!",
              uuid: event.uuid,
            );
          }
        } catch (e, stack) {
          // jika terjadi unexpected error
          santaraLog(e, stack);
          // emit data error ke state error
          yield PortfolioDetailError(
            error: "[06] Tidak dapat mengambil data!",
            uuid: event.uuid,
          );
        }
      } else {
        yield PortfolioDetailError(error: "Null result!", uuid: event.uuid);
      }
    }
  }
}
