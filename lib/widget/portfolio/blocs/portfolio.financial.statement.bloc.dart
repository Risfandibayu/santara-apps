// untuk tabel laporan keuangan
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:santaraapp/models/FinancialStatementPeriod.dart';
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/financial_statements/PortfolioService.dart';
import 'package:santaraapp/utils/logger.dart';

// Define event
class PortfolioFinancialStatementEvent {}

// Define state
class PortfolioFinancialStatementState {}

// Event load data tabelnya
class LoadPortfolioFinancialStatement extends PortfolioFinancialStatementEvent {
  final int type; // 0 = Penerbit / 1 = Pemodal
  final String uuid; // uuid emiten
  final DateTime dateStart; // filter data periode awal
  final DateTime dateEnd; // filter data periode akhir
  LoadPortfolioFinancialStatement(
    this.type,
    this.uuid,
    this.dateStart,
    this.dateEnd,
  );
}

// Event pilih periode awal / tanggal awal
class SelectDateStart extends PortfolioFinancialStatementEvent {
  final DateTime dateStart;
  SelectDateStart({this.dateStart});
}

// Event pilih periode akhir / tanggal akhir
class SelectDateEnd extends PortfolioFinancialStatementEvent {
  final DateTime dateEnd;
  SelectDateEnd({this.dateEnd});
}

// State data empty (Pertama kali buka halaman)
class PortfolioFinancialStatementUninitialized
    extends PortfolioFinancialStatementState {}

// State unauthorized
class PortfolioFinancialStatementUnauthorized
    extends PortfolioFinancialStatementState {}

// State jika data berhasil dimuat
class PortfolioFinancialStatementLoaded
    extends PortfolioFinancialStatementState {
  final String uuid; // uuid
  final DateTime firstDate; // data tanggal awal (di datepicker)
  final DateTime dateStart; // tanggal awal
  final DateTime dateEnd; // tanggal akhir
  final List<FinancialStatementPeriod> datas; // data tabelnya

  PortfolioFinancialStatementLoaded({
    this.uuid,
    this.firstDate,
    this.dateStart,
    this.dateEnd,
    this.datas,
  });

  PortfolioFinancialStatementLoaded copyWith({
    String uuid,
    DateTime dateStart,
    DateTime dateEnd,
    List<FinancialStatementPeriod> datas,
  }) {
    return PortfolioFinancialStatementLoaded(
      uuid: uuid ?? this.uuid,
      firstDate: firstDate ?? this.firstDate,
      dateStart: dateStart ?? this.dateStart,
      dateEnd: dateEnd ?? this.dateEnd,
      datas: datas ?? this.datas,
    );
  }
}

// state Jika error
class PortfolioFinancialStatementError
    extends PortfolioFinancialStatementState {
  final String error;
  final String uuid;
  final DateTime dateStart;
  final DateTime dateEnd;

  PortfolioFinancialStatementError({
    this.error,
    this.uuid,
    this.dateStart,
    this.dateEnd,
  });
}

// State jika data tidak ditemukan
class PortfolioFinancialStatementEmpty
    extends PortfolioFinancialStatementState {}

class PortfolioFinancialStatementBloc extends Bloc<
    PortfolioFinancialStatementEvent, PortfolioFinancialStatementState> {
  // constructor
  PortfolioFinancialStatementBloc(PortfolioFinancialStatementState initialState)
      : super(initialState);

  // Define api's
  PortfolioService apiService = PortfolioService();
  ApiService service = ApiService();
  // Define date format
  final DateFormat dateFormat = DateFormat("dd-MM-yyyy", "id");

  @override
  Stream<PortfolioFinancialStatementState> mapEventToState(
      PortfolioFinancialStatementEvent event) async* {
    // jika event adalah load data tabelnya
    if (event is LoadPortfolioFinancialStatement) {
      try {
        // setup tanggal jika datestart & dateend kosong
        var currentDate = DateTime.now(); // tanggal sekarang
        var dateStart =
            event?.dateStart ?? Jiffy(currentDate).subtract(months: 12); //
        var dateEnd = event?.dateEnd ?? currentDate;
        // set state menjadi uninitialized (nampilin loading)
        yield PortfolioFinancialStatementUninitialized();
        // Parsing tanggal agar dateStart menjadi tanggal 1 setiap bulan
        // dan dateEnd menjadi tanggal 28/29/30/31 setiap akhir bulan
        var dateStartParsed =
            dateStart.subtract(Duration(days: dateStart.day - 1));
        var dateEndParsed = DateTime(dateEnd.year, dateEnd.month + 1, 0);
        // call api's
        var result = await apiService.getPortfolioTable(
            event.uuid,
            dateFormat.format(dateStartParsed),
            dateFormat.format(dateEndParsed));
        // jika result tidak null
        if (result != null) {
          // jika statuscode == 200 (berhasil request)
          if (result.status == ApiStatus.success) {
            // define data list untuk dipass ke state loaded (PortfolioFinancialStatementLoaded)
            List<FinancialStatementPeriod> datas = [];
            if (result.data['data'] != null && result.data['data'].length > 0) {
              result.data['data'].forEach((val) {
                var period = FinancialStatementPeriod.fromJson(val);
                datas.add(period);
              });
            }
            // jika datanya kosong
            // data berhasil di load, emit data ke state PortfolioFinancialStatementLoadeds
            yield PortfolioFinancialStatementLoaded(
              uuid: event.uuid,
              firstDate: Jiffy(currentDate).subtract(months: 12),
              datas: datas,
              dateStart: event?.dateStart ?? dateStart,
              dateEnd: event?.dateEnd ?? currentDate,
            );
          } else if (result.status == ApiStatus.notFound) {
            yield PortfolioFinancialStatementEmpty();
          } else if (result.status == ApiStatus.unauthorized) {
            yield PortfolioFinancialStatementUnauthorized();
          } else {
            // jika request status code != 200
            yield PortfolioFinancialStatementError(
              error: "[${result.message}] Tidak dapat menerima data!",
              uuid: event.uuid,
              dateStart: event.dateStart,
              dateEnd: event.dateEnd,
            );
          }
        } else {
          // jika resultnya null
          yield PortfolioFinancialStatementError(
            error: "[NULL] Tidak dapat menerima data!",
            uuid: event.uuid,
            dateStart: event.dateStart,
            dateEnd: event.dateEnd,
          );
        }
      } catch (e, stack) {
        santaraLog(e, stack);
        // jika error ketika request
        yield PortfolioFinancialStatementError(
          error: "[07] Terjadi kesalahan!",
          uuid: event.uuid,
          dateStart: event.dateStart,
          dateEnd: event.dateEnd,
        );
      }
    } else if (event is SelectDateStart) {
      // event ketika memilih periode awal
      if (state is PortfolioFinancialStatementLoaded) {
        PortfolioFinancialStatementLoaded data = state;
        yield data.copyWith(dateStart: event.dateStart);
      }
    } else if (event is SelectDateEnd) {
      // event ketika memilih periode akhir
      if (state is PortfolioFinancialStatementLoaded) {
        PortfolioFinancialStatementLoaded data = state;
        yield data.copyWith(dateEnd: event.dateEnd);
      }
    }
  }
}
