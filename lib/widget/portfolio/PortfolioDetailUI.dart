import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../core/usecases/unauthorize_usecase.dart';
import '../../helpers/PopupHelper.dart';
import '../../models/ListPortfolio.dart';
import '../../utils/sizes.dart';
import '../financial_statements/widgets/financial_report_history_widget.dart';
import '../widget/components/charts/build_chart.dart';
import '../widget/components/main/SantaraFinancialStatements.dart';
import '../widget/components/main/SantaraScaffoldNoState.dart';
import 'blocs/portfolio.detail.bloc.dart';
import 'blocs/portfolio.financial.statement.bloc.dart';

class PortfolioDetailUI extends StatelessWidget {
  final String uuid;
  final String title;
  PortfolioDetailUI({this.uuid, this.title});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider(
        create: (context) => PortfolioDetailBloc(PortfolioDetailUninitialized())
          ..add(LoadPortfolioDetail(uuid)),
      ),
      BlocProvider(
        create: (context) => PortfolioFinancialStatementBloc(
            PortfolioFinancialStatementUninitialized())
          ..add(LoadPortfolioFinancialStatement(0, uuid, null, null)),
      ),
    ], child: PortfolioDetailBody(title: title));
  }
}

class PortfolioDetailBody extends StatefulWidget {
  final String title;
  PortfolioDetailBody({this.title});
  @override
  _PortfolioDetailBodyState createState() => _PortfolioDetailBodyState();
}

class _PortfolioDetailBodyState extends State<PortfolioDetailBody> {
  // Formatter rupiah
  final rupiah = NumberFormat("#,##0");

  // blocs
  PortfolioDetailBloc detailBloc;
  PortfolioFinancialStatementBloc financialStatementBloc;

  Widget get divider {
    return Container(
      height: 2,
      width: double.maxFinite,
      color: Color(0xffF4F4F4),
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    detailBloc = BlocProvider.of<PortfolioDetailBloc>(context);
    financialStatementBloc =
        BlocProvider.of<PortfolioFinancialStatementBloc>(context);
  }

  // Build loading detail protfolio
  Widget _buildPortfolioDetailLoader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.all(Sizes.s20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildShimmer(Sizes.s20, Sizes.s250),
              SizedBox(height: Sizes.s10),
              buildShimmer(Sizes.s2, double.maxFinite),
              SizedBox(height: Sizes.s10),
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s100),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s80),
                ],
              ),
              SizedBox(height: Sizes.s10),
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s150),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s120),
                ],
              ),
              SizedBox(height: Sizes.s10),
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s180),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s110),
                ],
              ),
              SizedBox(height: Sizes.s10),
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s200),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s100),
                ],
              ),
            ],
          ),
        ),
        divider,
        Container(
          margin: EdgeInsets.all(Sizes.s20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Title
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  buildShimmer(Sizes.s20, Sizes.s150),
                  SizedBox(width: Sizes.s10),
                  buildShimmer(Sizes.s15, Sizes.s100),
                ],
              ),
              // Divider
              SizedBox(height: Sizes.s10),
              buildShimmer(Sizes.s2, double.maxFinite),
              SizedBox(height: Sizes.s10),
              // Net Income
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s80),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s100),
                ],
              ),
              SizedBox(height: Sizes.s10),
              // NPM
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s50),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s50),
                ],
              ),
              SizedBox(height: Sizes.s10),
              // EPS
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s50),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s100),
                ],
              ),
              SizedBox(height: Sizes.s10),
              // Dividend Yield
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s110),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s80),
                ],
              ),
              SizedBox(height: Sizes.s10),
              // DPS
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s50),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s120),
                ],
              ),
              SizedBox(height: Sizes.s10),
              // PER*
              Row(
                children: [
                  buildShimmer(Sizes.s15, Sizes.s50),
                  Spacer(),
                  buildShimmer(Sizes.s15, Sizes.s80),
                ],
              ),
              SizedBox(height: Sizes.s10),
            ],
          ),
        ),
        divider,
        SizedBox(height: Sizes.s40),
        Container(
          margin: EdgeInsets.only(
            left: Sizes.s20,
            right: Sizes.s20,
          ),
          child: buildShimmer(
            Sizes.s270,
            double.maxFinite,
          ),
        ),
        SizedBox(height: Sizes.s40),
      ],
    );
  }

  // Build detail portfolio
  Widget _buildPortfolioDetail() {
    return BlocBuilder<PortfolioDetailBloc, PortfolioDetailState>(
      builder: (context, state) {
        if (state is PortfolioDetailUninitialized) {
          return _buildPortfolioDetailLoader();
        } else if (state is PortfolioDetailLoaded) {
          SahamData data = state?.data;
          // print(">> List Data Length : ${data.listData.length}");
          // print(">> List Data Net Income : ${data.listNetIncome.length}");
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(Sizes.s20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Informasi Kepemilikan Saham",
                      style: TextStyle(
                        fontSize: FontSize.s16,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Container(height: Sizes.s10),
                    divider,
                    Container(height: Sizes.s10),
                    FinanceTextRow(
                      title: "Saham ID",
                      value: "${data?.codeEmiten ?? '-'}",
                    ),
                    FinanceTextRow(
                      title: "Total Saham Dimiliki",
                      value: data?.totalSahamLembar != null
                          ? "${rupiah.format(data?.totalSahamLembar ?? 0)} Lembar"
                          : "-",
                    ),
                    FinanceTextRow(
                      title: "Total Saham Dalam Rupiah",
                      value: data?.totalSahamRupiah != null
                          ? "Rp. ${rupiah.format(data?.totalSahamRupiah ?? 0)}"
                          : "-",
                    ),
                    Row(
                      children: [
                        InkWell(
                          onTap: () => PopupHelper.showInfo(
                            context,
                            "Estimasi Dividen",
                            "Estimasi ini adalah hitungan perkiraan berdasarkan data historis penerbit sebelumnya. Nilai dividen yang Anda dapat bisa berbeda dari estimasi.",
                          ),
                          child: Container(
                            child: Row(
                              children: [
                                Text(
                                  "Proyeksi Yield Dividen (Annual) ",
                                  style: TextStyle(
                                    fontSize: FontSize.s14,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                Icon(
                                  Icons.info,
                                  size: FontSize.s16,
                                  color: Color(0xffdddddd),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Flexible(
                          child: Text(
                            data?.proyeksiDividendYeild != null
                                ? "Rp. ${rupiah.format(data?.proyeksiDividendYeild ?? 0)}"
                                : "-",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              divider,
              Container(
                margin: EdgeInsets.all(Sizes.s20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          "Kinerja ${data?.codeEmiten ?? '-'}, ${DateTime.now().year} ",
                          style: TextStyle(
                            fontSize: FontSize.s16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          " (Par Value Rp. ${data?.price != null ? rupiah.format(data?.price ?? 0) : '-'} /L)",
                          style: TextStyle(
                            fontSize: FontSize.s12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    Container(height: Sizes.s10),
                    divider,
                    Container(height: Sizes.s10),
                    FinanceTextRow(
                      title: "Net Income",
                      value: data?.netIncome != null
                          ? "Rp. ${rupiah.format(data?.netIncome ?? 0)}"
                          : "-",
                    ),
                    FinanceTextRow(
                      title: "NPM",
                      value: "${data?.npm ?? '-'}%",
                    ),
                    FinanceTextRow(
                      title: "EPS",
                      value: "Rp. ${data?.eps ?? '-'} /L",
                    ),
                    FinanceTextRow(
                      title: "Dividend Yield",
                      value: "${data?.dividenYield ?? '-'}% p.a",
                    ),
                    FinanceTextRow(
                      title: "DPS",
                      value: "Rp ${data?.dps ?? '-'} /L",
                    ),
                    FinanceTextRow(
                      title: "PER*",
                      value: "${data?.per ?? '-'}",
                    ),
                  ],
                ),
              ),
              divider,
              SizedBox(height: Sizes.s40),
              BuildFinancialChart(data: data),
              SizedBox(height: Sizes.s40),
            ],
          );
        } else if (state is PortfolioDetailError) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(state.error),
              IconButton(
                icon: Icon(Icons.replay),
                onPressed: () =>
                    detailBloc..add(LoadPortfolioDetail(state.uuid)),
              ),
            ],
          );
        } else if (state is PortfolioDetailEmpty) {
          return Center(
            child: Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Data Tidak Ditemukan",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: FontSize.s14,
                  ),
                ),
                SizedBox(height: Sizes.s15),
              ],
            )),
          );
        } else {
          return Container(
            child: Text("Unknown state!"),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${widget?.title ?? 'Portofolio Detail'}",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              divider,
              SizedBox(height: Sizes.s20),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildPortfolioDetail(),
                    divider,
                    // _buildLaporanKeuangan()
                    FinancialReportHistoryWidget(
                      financialStatementBloc: financialStatementBloc,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
