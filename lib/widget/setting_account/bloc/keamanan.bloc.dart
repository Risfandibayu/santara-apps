import 'dart:convert';
import 'package:rxdart/subjects.dart';
import 'package:santaraapp/services/security/pin_service.dart';

enum KeamananState { uninitialized, loading, success, error }

class KeamananBloc {
  final _pinService = PinService();

  final _state =
      BehaviorSubject<KeamananState>.seeded(KeamananState.uninitialized);
  Stream<KeamananState> get state => _state.stream;

  final _fingerPrint = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get fingerPrint => _fingerPrint.stream;

  getFingerPrintStatus() async {
    // print(">> WADADIDAW");
    // PopupHelper.showLoading(context);
    try {
      await _pinService.getExistFP().then((value) {
        if (value.statusCode == 200) {
          var parsed = json.decode(value.body);
          if (parsed["message"]) {
            _fingerPrint.sink.add(parsed["message"]);
          } else {
            _fingerPrint.sink.add(parsed["message"]);
          }
        } else {
          _fingerPrint.sink.addError("Tidak dapat menerima data!");
        }
      });
    } catch (e) {
      _fingerPrint.sink
          .addError("Terjadi kesalahan saat mengambil data fingerprint!");
    }
  }

  updateFingerPrint(bool value) async {
    _fingerPrint.sink.add(value);
    _state.sink.add(KeamananState.loading);
    try {
      await _pinService.updateFP(value).then((value) {
        if (value.statusCode == 200) {
          getFingerPrintStatus();
          // var parsed = json.decode(value.body);
          _state.sink.add(KeamananState.success);
        } else {
          _state.sink.add(KeamananState.error);
          _state.sink.addError("Terjadi kesalahan saat menerima data!");
        }
      });
    } catch (e) {
      _state.sink.add(KeamananState.error);
      _state.sink.addError("Terjadi kesalahan saat mengirim data!");
    }
  }

  dispose() {
    _fingerPrint.close();
    _state.close();
  }
}
