import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/helper.dart';

class ResetPassword2 extends StatefulWidget {
  final String user;
  ResetPassword2({this.user});
  @override
  _ResetPassword2State createState() => _ResetPassword2State();
}

class _ResetPassword2State extends State<ResetPassword2> {
  var isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  final TextEditingController pass1 = TextEditingController();
  final TextEditingController pass2 = TextEditingController();
  FocusNode focusPass = FocusNode();
  FocusNode focusRePass = FocusNode();
  bool password1 = true;
  bool password2 = true;
  var token;
  var datas;
  var refreshToken;
  String hintPassword = 'Password';
  String hintRePassword = 'Ulangi Password';

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
    datas = userFromJson(await storage.read(key: 'user'));
    refreshToken = await storage.read(key: "refreshToken");
  }

  Future changePassword() async {
    setState(() => isLoading = true);
    final response = await http.post(
      '$apiLocal/auth/reset-pass',
      body: {
        "user": widget.user,
        "newPassword": pass1.text,
        "newPasswordConfirmation": pass2.text
      },
      headers: {'Authorization': 'Bearer ' + token},
    );
    if (response.statusCode == 200) {
      setState(() => isLoading = false);
      changeSuccess();
    } else {
      setState(() => isLoading = false);
      final data = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(
                  data['message'] == null ? data['error'] : data['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future logout() async {
    if (token != null) {
      final id = datas.id;
      await http.post('$apiLocal/auth/logout', body: {
        "refreshToken": "$refreshToken",
        "user_id": "$id",
      }).timeout(Duration(seconds: 20));
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'AddressCyro');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      storage.delete(key: 'hasSubmitJob');
      storage.delete(key: 'hasSubmitPriority');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getLocalStorage();
    focusPass.addListener(() {
      if (focusPass.hasFocus) {
        hintPassword = 'Contoh : P4\$sword';
      } else {
        hintPassword = 'Kata Sandi';
      }
      setState(() {});
    });
    focusRePass.addListener(() {
      if (focusRePass.hasFocus) {
        hintRePassword = 'Contoh : P4\$sword';
      } else {
        hintRePassword = 'Ulangi Kata Sandi';
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Container(height: 40),
                  Image.asset("assets/icon/verification.png",
                      height: MediaQuery.of(context).size.width * 2 / 3,
                      width: MediaQuery.of(context).size.width * 2 / 3),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 6),
                    child: Text("Atur ulang kata sandi",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                    child: Text(
                      "Masukkan kata sandi yang baru",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  //password
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: TextFormField(
                        focusNode: focusPass,
                        obscureText: password1,
                        controller: pass1,
                        validator: (value) =>
                            value.isEmpty ? 'Masukkan kata sandi' : null,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: InputDecoration(
                          errorMaxLines: 5,
                          suffixIcon: IconButton(
                              icon: Icon(password1
                                  ? Icons.visibility_off
                                  : Icons.visibility),
                              onPressed: () {
                                setState(() {
                                  password1
                                      ? password1 = false
                                      : password1 = true;
                                });
                              }),
                          hintText: hintPassword,
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.grey)),
                        )),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(25, 0, 25, 10),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: TextFormField(
                        focusNode: focusRePass,
                        obscureText: password2,
                        controller: pass2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Masukkan kembali kata sandi baru';
                          } else if (value != pass1.text) {
                            return 'Kata sandi baru tidak cocok';
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: InputDecoration(
                          errorMaxLines: 5,
                          suffixIcon: IconButton(
                              icon: Icon(password2
                                  ? Icons.visibility_off
                                  : Icons.visibility),
                              onPressed: () {
                                setState(() {
                                  password2
                                      ? password2 = false
                                      : password2 = true;
                                });
                              }),
                          hintText: hintRePassword,
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.grey)),
                        )),
                  ),

                  //button
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 4.0, 20.0, 20.0),
                    child: InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          changePassword();
                        }
                      },
                      child: Container(
                        height: 48.0,
                        decoration: BoxDecoration(
                          color: Color(0xFF666EE8),
                          border:
                              Border.all(color: Colors.transparent, width: 2.0),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Center(
                            child: Text('Selanjutnya',
                                style: TextStyle(color: Colors.white))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 30),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Navigator.pop(context),
                ))
          ],
        ),
      ),
    );
  }

  void changeSuccess() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: Container(
            height: MediaQuery.of(context).size.width / 2 + 112,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: <Widget>[
                Image.asset("assets/icon/success.png",
                    height: MediaQuery.of(context).size.width / 2,
                    width: MediaQuery.of(context).size.width / 2),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      "Perubahan password berhasil",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => logout().then((_) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => Home(index: 4)),
                        (Route<dynamic> route) => false);
                  }),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text("Masuk",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ));
        });
  }
}
