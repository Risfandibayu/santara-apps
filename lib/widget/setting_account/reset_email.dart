import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:http/http.dart' as http;

class ResetEmail extends StatefulWidget {
  @override
  _ResetEmailState createState() => _ResetEmailState();
}

class _ResetEmailState extends State<ResetEmail> {
  var isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  final TextEditingController email = TextEditingController();
  var token;
  var datas;
  var refreshToken;

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
    datas = userFromJson(await storage.read(key: 'user'));
    refreshToken = await storage.read(key: "refreshToken");
  }

  Future changeEmail() async {
    setState(() => isLoading = true);
    final response = await http.put(
      '$apiLocal/traders/reset-email',
      body: {
        "email": email.text,
      },
      headers: {'Authorization': 'Bearer ' + token},
    );
    if (response.statusCode == 200) {
      setState(() => isLoading = false);
      changeSuccess();
    } else {
      setState(() => isLoading = false);
      final data = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(
                  data['message'] == null ? data['error'] : data['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future logout() async {
    if (token != null) {
      final id = datas.id;
      await http.post('$apiLocal/auth/logout', body: {
        "refreshToken": "$refreshToken",
        "user_id": "$id",
      }).timeout(Duration(seconds: 20));
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'AddressCyro');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      storage.delete(key: 'hasSubmitJob');
      storage.delete(key: 'hasSubmitPriority');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Container(height: 40),
                  Image.asset("assets/icon/verification.png",
                      height: MediaQuery.of(context).size.width * 2 / 3,
                      width: MediaQuery.of(context).size.width * 2 / 3),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 6),
                    child: Text("Atur Ulang Alamat Email",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                    child: Text(
                      "Masukkan alamat email yang baru",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  //password
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: TextFormField(
                        controller: email,
                        validator: (value) =>
                            value.isEmpty ? 'Masukkan email baru anda' : null,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: InputDecoration(
                          hintText: 'Email baru',
                          errorMaxLines: 5,
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.grey)),
                        )),
                  ),

                  //button
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 4.0, 20.0, 20.0),
                    child: InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          changeEmail();
                        }
                      },
                      child: Container(
                        height: 48.0,
                        decoration: BoxDecoration(
                          color: Color(0xFF666EE8),
                          border:
                              Border.all(color: Colors.transparent, width: 2.0),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Center(
                            child: Text('Selanjutnya',
                                style: TextStyle(color: Colors.white))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 30),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Navigator.pop(context),
                ))
          ],
        ),
      ),
    );
  }

  void changeSuccess() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: Container(
            height: MediaQuery.of(context).size.width / 2 + 160,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: <Widget>[
                Image.asset("assets/icon/success.png",
                    height: MediaQuery.of(context).size.width / 2,
                    width: MediaQuery.of(context).size.width / 2),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
                    child: Text(
                      "Perubahan e-mail berhasil",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 20),
                    child: Text(
                      "Verifikasi email mu untuk menyelesaikan proses ganti e-mail",
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => logout().then((_) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => Home(index: 4)),
                        (Route<dynamic> route) => false);
                  }),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text("Masuk",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ));
        });
  }
}
