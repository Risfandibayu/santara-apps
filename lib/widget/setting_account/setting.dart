import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../core/usecases/unauthorize_usecase.dart';
import '../../features/payment/presentation/pages/payment_setting_page.dart';
import '../../helpers/PopupHelper.dart';
import '../../helpers/ToastHelper.dart';
import '../../models/StatusKycTrader.dart';
import '../../models/User.dart';
import '../../services/kyc/KycPersonalService.dart';
import '../../utils/api.dart';
import 'keamanan.dart';
import 'kyc_perusahaan.dart';
import 'kyc_pribadi.dart';

class Setting extends StatefulWidget {
  final EndStatus status;
  Setting({this.status});
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  final storage = new FlutterSecureStorage();
  User user;
  var token, uuid;
  var phone, email;
  var currentVersion;
  var isLoading = true;
  var traderType;
  var retryGetUser = 0;

  Future getLocalStaorage() async {
    token = await storage.read(key: 'token');
    uuid = await storage.read(key: 'uuid');
  }

  Future getUser() async {
    setState(() => isLoading = true);
    final PackageInfo info = await PackageInfo.fromPlatform();
    final response = await http.get('$apiLocal/users/by-uuid/$uuid',
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      setState(() {
        isLoading = false;
        user = userFromJson(response.body);
        currentVersion = info.version;
        phone = user.phone;
        email = user.email;
        traderType = user.trader.traderType;
      });
    } else {
      if (retryGetUser < 3) {
        getUser();
        setState(() {
          retryGetUser = retryGetUser + 1;
        });
      } else {
        setState(() {
          isLoading = false;
        });
        showWarningGetUser();
      }
    }
  }

  showWarningGetUser() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 12),
                      child: Text("Terjadi kesalahan",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold)),
                    ),
                    Text("Silahkan coba beberapa saat lagi"),
                  ],
                )),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: Text("Oke"))
            ],
          );
        });
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getLocalStaorage().then((_) {
      getUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Informasi Akun",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      backgroundColor: Color(ColorRev.mainBlack),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : Stack(
              children: <Widget>[
                ListView(
                  padding: EdgeInsets.all(20),
                  children: <Widget>[
                    traderType == "personal"
                        ? _personal()
                        : traderType == "company"
                            ? _company()
                            : Container(
                                child: Center(
                                  child: Text(
                                      "Mohon lengkapi data kyc anda sebelum mengakses fitur ini"),
                                ),
                              ),
                  ],
                ),
                MediaQuery.of(context).size.height > 550
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text("Santara Apps Ver $currentVersion",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white)),
                        ))
                    : Container(),
              ],
            ),
    );
  }

  openPage(dynamic page) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => page));
  }

  _handleOpenKycCompany() {
    var _preview;
    if (widget.status != null) {
      if (widget.status == EndStatus.verifying) {
        _preview = true;
      } else {
        _preview = false;
      }
    } else {
      _preview = false;
    }
    if (_preview) {
      openPage(KycPerusahaanSetting(
        preview: true,
      ));
    } else {
      PopupHelper.pratinjauKycConfirmation(context, () {
        Navigator.pop(context);
        openPage(KycPerusahaanSetting(
          preview: true,
        ));
      }, () {
        Navigator.pop(context);
        // jika status uncomplete
        if (widget.status != null && widget.status != EndStatus.verified) {
          openPage(KycPerusahaanSetting(preview: false));
        } else {
          if (widget.status == EndStatus.verified) {
            PopupHelper.showConfirmationDataChange(context, () async {
              Navigator.pop(context);
              // jika status verified, maka tampilkan popup timer kyc (batas waktu edit terakhir)
              setState(() => isLoading = true);
              KycPersonalService _service = KycPersonalService();
              await _service.getTimerKyc().then((value) {
                setState(() => isLoading = false);
                // jika statusCode != 200 (unexpected error)
                if (value == null) {
                  ToastHelper.showBasicToast(
                      context, "Terjadi kesalahan, mohon coba lagi nanti");
                }
                // jika statusCode == 200 (User sudah verifikasi & batas waktu muncul)
                if (value.statusCode == 200) {
                  PopupHelper.showTimeLimitEditKyc(
                    context,
                    value?.data["message"] ?? "",
                  );
                } else if (value.statusCode == 400) {
                  // jika status code == 400 (Not found / belum pernah edit kyc)
                  openPage(KycPerusahaanSetting(
                    preview: false,
                  ));
                } else {
                  ToastHelper.showBasicToast(
                    context,
                    "Terjadi kesalahan, mohon coba lagi nanti",
                  );
                }
              });
            });
          } else {
            ToastHelper.showBasicToast(context, "Undefined Status");
          }
        }
      });
    }
  }

  _handleOpenKycPersonal() {
    var _preview;
    if (widget.status != null) {
      if (widget.status == EndStatus.verifying) {
        _preview = true;
      } else {
        _preview = false;
      }
    } else {
      _preview = false;
    }

    if (_preview) {
      openPage(KycPribadiSetting(
        preview: true,
      ));
    } else {
      PopupHelper.pratinjauKycConfirmation(context, () {
        Navigator.pop(context);
        openPage(KycPribadiSetting(
          preview: true,
        ));
      }, () {
        Navigator.pop(context);
        // jika status uncomplete
        if (widget.status != null && widget.status != EndStatus.verified) {
          openPage(KycPribadiSetting(preview: false));
        } else {
          if (widget.status == EndStatus.verified) {
            PopupHelper.showConfirmationDataChange(context, () async {
              Navigator.pop(context);
              // jika status verified, maka tampilkan popup timer kyc (batas waktu edit terakhir)
              setState(() => isLoading = true);
              KycPersonalService _service = KycPersonalService();
              await _service.getTimerKyc().then((value) {
                setState(() => isLoading = false);
                // jika status code != 200
                if (value == null) {
                  ToastHelper.showBasicToast(
                      context, "Terjadi kesalahan, mohon coba lagi nanti");
                }
                // jika statusCode == 200 (User sudah verifikasi & batas waktu muncul)
                if (value.statusCode == 200) {
                  PopupHelper.showTimeLimitEditKyc(
                    context,
                    value?.data["message"] ?? "",
                  );
                } else if (value.statusCode == 400) {
                  openPage(KycPribadiSetting(
                    preview: false,
                  ));
                } else {
                  ToastHelper.showBasicToast(
                      context, "Terjadi kesalahan, mohon coba lagi nanti");
                }
              });
            });
          } else {
            // print(">> Status : ${widget.status}");
            ToastHelper.showBasicToast(context, "Undefined Status");
          }
        }
      });
    }
  }

  _handleOpenPaymentSetting() {
    Navigator.push(
        context, MaterialPageRoute(builder: (_) => PaymentSettingPage()));
  }

  Widget _personal() {
    return Column(
      children: <Widget>[
        PengaturanTile(
          title: "Keamanan",
          subtitle: "PIN, Kata Kunci & Fingerprint",
          onTap: () => openPage(KeamananUI()),
        ),
        PengaturanTile(
          title: "KYC",
          subtitle: "Nama Lengkap, Pendapatan Pertahun, Nomor Rekening",
          onTap: _handleOpenKycPersonal,
        ),
        PengaturanTile(
          title: "Pengaturan Pembayaran",
          subtitle: "DANA",
          onTap: _handleOpenPaymentSetting,
        ),
      ],
    );
  }

  Widget _company() {
    return Column(
      children: <Widget>[
        PengaturanTile(
          title: "Keamanan",
          subtitle: "PIN, Kata Kunci & Fingerprint",
          onTap: () => openPage(KeamananUI()),
        ),
        PengaturanTile(
          title: "KYC",
          subtitle: "Nama Perusahaan, Pendapatan Pertahun, Nomor Rekening",
          onTap: _handleOpenKycCompany,
        ),
        PengaturanTile(
          title: "Pengaturan Pembayaran",
          subtitle: "DANA",
          onTap: _handleOpenPaymentSetting,
        ),
      ],
    );
  }
}

class PengaturanTile extends StatelessWidget {
  final VoidCallback onTap;
  final String title;
  final String subtitle;

  PengaturanTile({
    @required this.onTap,
    @required this.title,
    @required this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "$title",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
              subtitle: Text(
                "$subtitle",
                style: TextStyle(color: Colors.grey),
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
            Container(
              height: 0.5,
              color: Color(0xffF4F4F4),
              width: double.infinity,
            )
          ],
        ),
      ),
    );
  }
}
