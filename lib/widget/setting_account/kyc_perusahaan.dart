import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/alamat/AlamatPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/aset/AsetPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/bank/BankPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/biodata_perusahaan/BiodataPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/dokumen/DokPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/pajak/PajakPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/pj/PjPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_perusahaan_new/profit/ProfitPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/syarat_ketentuan/SyaratKetentuanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/alamat/AlamatPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/aset_perusahaan/AsetPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/bank_perusahaan/BankPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/biodata_perusahaan/BiodataPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/dokumen_perusahaan/DokumenPerusahaanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/pajak_perizinan/PajakPerizinanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/penanggung_jawab/PenanggungJawabUI.dart';
import 'package:santaraapp/widget/kyc/view_data/perusahaan/profit_perusahaan/ProfitPerusahaanUI.dart';

class KycPerusahaanSetting extends StatefulWidget {
  final bool preview;
  KycPerusahaanSetting({@required this.preview});
  @override
  _KycPerusahaanSettingState createState() => _KycPerusahaanSettingState();
}

class _KycPerusahaanSettingState extends State<KycPerusahaanSetting> {
  KycPerusahaanService _service = KycPerusahaanService();
  bool isLoaded = false;
  bool isError = false;
  StatusKycTrader statusTrader = StatusKycTrader();
  var result = {};
  User company;

  bool isActive(String status) {
    switch (status) {
      case "verifying":
        return false;
        break;
      case "rejected":
        return true;
        break;
      case "kustodian_rejected":
        return true;
        break;
      case "kustodian_verifying":
        return false;
        break;
      case "verified":
        return true;
        break;
      case "empty":
        return false;
        break;
      case "data_update":
        return true;
        break;
      case "waiting":
        return false;
        break;
      default:
        return false;
    }
  }

  openPage(Widget page) async {
    final res = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => page,
      ),
    );
    getSubmissionView();
  }

  bool showTnc() {
    if (statusTrader.statusKyc1 != "empty" &&
        statusTrader.statusKyc1 != "rejected" &&
        statusTrader.statusKyc2 != "empty" &&
        statusTrader.statusKyc2 != "rejected" &&
        statusTrader.statusKyc3 != "empty" &&
        statusTrader.statusKyc3 != "rejected" &&
        statusTrader.statusKyc4 != "empty" &&
        statusTrader.statusKyc4 != "rejected" &&
        statusTrader.statusKyc5 != "empty" &&
        statusTrader.statusKyc5 != "rejected" &&
        statusTrader.statusKyc6 != "empty" &&
        statusTrader.statusKyc6 != "rejected" &&
        statusTrader.statusKyc7 != "empty" &&
        statusTrader.statusKyc7 != "rejected" &&
        statusTrader.statusKyc8 != "empty" &&
        statusTrader.statusKyc8 != "rejected") {
      return true;
    } else {
      return false;
    }
  }

  // jika view data
  getSubmissionView() async {
    setState(() {
      isLoaded = false;
      isError = false;
    });
    try {
      await _service.getPerusahaanData().then((compres) async {
        if (compres.statusCode == 200) {
          await _service.statusKycTrader().then((res) {
            setState(() {
              statusTrader = StatusKycTrader.fromJson(res.data);
              company = User.fromJson(compres.data);
              isLoaded = true;
              isError = false;
            });
          });
        } else if (compres.statusCode == 400) {
          ToastHelper.showFailureToast(context, compres.data["message"]);
          setState(() {
            isLoaded = true;
            isError = true;
          });
        } else {
          ToastHelper.showFailureToast(
            context,
            "Tidak dapat mengambil data perusahaan!",
          );
          setState(() {
            isLoaded = true;
            isError = true;
          });
        }
      });
    } catch (e) {
      // print(e);
      setState(() {
        isLoaded = true;
        isError = true;
      });
    }
  }

  // jika edit data
  getSubmissionId() async {
    setState(() {
      isLoaded = false;
      isError = false;
    });
    try {
      await _service.getSubmissionId().then((value) async {
        if (value.statusCode == 200) {
          await _service.getPerusahaanData().then((compres) async {
            if (compres.statusCode == 200) {
              await _service.statusKycTrader().then((res) {
                if (value != null) {
                  setState(() {
                    result = value.data;
                    statusTrader = StatusKycTrader.fromJson(res.data);
                    company = User.fromJson(compres.data);
                    isLoaded = true;
                    isError = false;
                  });
                } else {
                  setState(() {
                    isLoaded = true;
                    isError = true;
                  });
                }
              });
            } else if (compres.statusCode == 400) {
              ToastHelper.showFailureToast(context, value.data["message"]);
              setState(() {
                isLoaded = true;
                isError = true;
              });
            } else {
              ToastHelper.showFailureToast(
                context,
                "Tidak dapat mengambil data perusahaan!",
              );
              setState(() {
                isLoaded = true;
                isError = true;
              });
            }
          });
        } else if (value.statusCode == 400) {
          ToastHelper.showFailureToast(context, value.data["message"]);
          setState(() {
            isLoaded = true;
            isError = true;
          });
        } else {
          setState(() {
            isLoaded = true;
            isError = true;
          });
          ToastHelper.showFailureToast(
            context,
            "Tidak dapat mengambil submission!",
          );
        }
      });
    } catch (e) {
      // print(e);
      setState(() {
        isLoaded = true;
        isError = true;
      });
    }
  }

  // get submission id
  getSubmission() async {
    if (widget.preview) {
      // print(">> HELLO");
      getSubmissionView();
    } else {
      // print(">> HELLO");
      getSubmissionId();
    }
  }

  @override
  void initState() {
    super.initState();
    getSubmission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(widget.preview ? "Lihat Data KYC" : "Ubah Data KYC",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.white,
        padding: EdgeInsets.all(Sizes.s15),
        child: isLoaded
            ? isError
                ? Center(
                    child: IconButton(
                        icon: Icon(Icons.replay),
                        onPressed: () => getSubmission()),
                  )
                : ListView(
                    children: [
                      ItemSetting(
                        title: "Biodata Perusahaan",
                        subtitle: "Profil Perusahaan",
                        onTap: () => widget.preview
                            ? openPage(ViewBiodataPerusahaanUI(
                                status: statusTrader.statusKyc1,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(BiodataPerusahaanUI(
                                status: statusTrader.statusKyc1,
                                submissionId: statusTrader.statusKyc1 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc1,
                      ),
                      ItemSetting(
                        title: "Pajak & Perizinan",
                        subtitle: "Informasi Pajak & Nomor Perizinan Usaha",
                        onTap: () => widget.preview
                            ? openPage(ViewPajakPerizinanUI(
                                status: statusTrader.statusKyc2,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(PajakPerusahaanUI(
                                status: statusTrader.statusKyc2,
                                submissionId: statusTrader.statusKyc2 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc1 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc2,
                      ),
                      ItemSetting(
                        title: "Alamat",
                        subtitle: "Alamat Perusahaan & Nomor Telepon/Fax ",
                        onTap: () => widget.preview
                            ? openPage(ViewAlamatUI(
                                status: statusTrader.statusKyc3,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(AlamatPerusahaanUI(
                                status: statusTrader.statusKyc3,
                                submissionId: statusTrader.statusKyc3 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc2 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc3,
                      ),
                      ItemSetting(
                        title: "Penanggung Jawab",
                        subtitle: "Penanggung Jawab Perusahaan",
                        onTap: () => widget.preview
                            ? openPage(ViewPenanggungJawabUI(
                                status: statusTrader.statusKyc4,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(PjPerusahaanUI(
                                status: statusTrader.statusKyc4,
                                submissionId: statusTrader.statusKyc4 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc3 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc4,
                      ),
                      ItemSetting(
                        title: "Aset Perusahaan",
                        subtitle: "Ket. Aset & Profit Perusahaan",
                        onTap: () => widget.preview
                            ? openPage(ViewAsetPerusahaanUI(
                                status: statusTrader.statusKyc5,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(AsetPerusahaanUI(
                                status: statusTrader.statusKyc5,
                                submissionId: statusTrader.statusKyc5 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc4 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc5,
                      ),
                      ItemSetting(
                        title: "Profit Perusahaan",
                        subtitle: "Total Kekayaan & Preferensi Investasi",
                        onTap: () => widget.preview
                            ? openPage(ViewProfitPerusahaanUI(
                                status: statusTrader.statusKyc6,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(ProfitPerusahaanUI(
                                status: statusTrader.statusKyc6,
                                submissionId: statusTrader.statusKyc6 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc5 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc6,
                      ),
                      ItemSetting(
                        title: "Dokumen Perusahaan",
                        subtitle: "Dokumen Perusahaan",
                        onTap: () => widget.preview
                            ? openPage(ViewDokumenPerusahaanUI(
                                status: statusTrader.statusKyc7,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(DokPerusahaanUI(
                                status: statusTrader.statusKyc7,
                                submissionId: statusTrader.statusKyc7 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc6 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc7,
                      ),
                      ItemSetting(
                        title: "Bank Perusahaan",
                        subtitle: "Akun Bank Perusahaan",
                        onTap: () => widget.preview
                            ? openPage(ViewBankPerusahaanUI(
                                status: statusTrader.statusKyc8,
                                submissionId: result["id"].toString(),
                                data: company,
                              ))
                            : openPage(BankPerusahaanUI(
                                status: statusTrader.statusKyc8,
                                submissionId: statusTrader.statusKyc8 ==
                                        "rejected"
                                    ? "${company.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                              )),
                        status: statusTrader.statusKyc7 == "empty"
                            ? "waiting"
                            : statusTrader.statusKyc8,
                      ),
                      // widget.preview
                      //     ? Container()
                      //     :
                      ItemSetting(
                        title: "Syarat & Ketentuan",
                        subtitle: "Syarat & Ketentuan Santara",
                        onTap: () => openPage(KycSyaratKetentuanUI()),
                        status: showTnc()
                            ? company.trader.tnc == 0
                                ? "tnc_empty"
                                : "verified"
                            : null,
                      ),
                    ],
                  )
            : Center(
                child: CupertinoActivityIndicator(),
              ),
      ),
    );
  }
}

class ItemSetting extends StatelessWidget {
  final String title;
  final String subtitle;
  final String status;
  final VoidCallback onTap;

  ItemSetting({
    @required this.title,
    @required this.subtitle,
    @required this.status,
    this.onTap,
  });

  bool isActive(String status) {
    switch (status) {
      case "verifying":
        return true;
        break;
      case "rejected":
        return true;
        break;
      case "kustodian_rejected":
        return true;
        break;
      case "kustodian_verifying":
        return true;
        break;
      case "verified":
        return true;
        break;
      case "empty":
        return true;
        break;
      case "data_update":
        return true;
        break;
      case "waiting":
        return false;
        break;
      case "tnc_empty":
        return true;
        break;
      default:
        return false;
    }
  }

  Widget messageOption(String status) {
    bool error = false;
    String message = "";
    switch (status) {
      case "verifying":
        error = false;
        message = "Sedang dalam proses verifikasi!";
        break;
      case "rejected":
        error = true;
        message = "Formulir ditolak!";
        break;
      case "kustodian_rejected":
        error = true;
        message = "Formulir ditolak oleh kustodian!";
        break;
      case "kustodian_verifying":
        error = false;
        message = "Formulir sedang diverifikasi oleh kustodian!";
        break;
      case "verified":
        error = false;
        message = "";
        break;
      case "empty":
        error = true;
        message = "Formulir ini masih kosong!";
        break;
      case "waiting":
        error = true;
        message = "";
        break;
      case "tnc_empty":
        error = true;
        message = "Syarat dan Ketentuan Belum Disetujui!";
        break;
      default:
        error = true;
        message = "";
    }
    return message.isEmpty
        ? Container()
        : ItemMessage(
            message: message,
            isError: error,
          );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(0),
          onTap: () {
            if (isActive(status)) {
              onTap();
            } else {
              if (status == "waiting") {
                PopupHelper.showPopMessage(
                  context,
                  "Pengisian Formulir",
                  "Silahkan lengkapi data formulir yang masih kosong terlebih dahulu",
                  textAlign: TextAlign.center,
                );
              }
            }
          },
          title: Text(
            title,
            style: TextStyle(
              color: isActive(status) ? Colors.black : Color(0xffB8B8B8),
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  subtitle,
                  style: TextStyle(
                    color: Color(0xffB8B8B8),
                    fontSize: FontSize.s12,
                  ),
                ),
                messageOption(status),
                Container(
                  height: 5,
                )
              ],
            ),
          ),
          trailing: Icon(
            Icons.keyboard_arrow_right,
            color: isActive(status) ? Colors.black : Color(0xffB8B8B8),
          ),
        ),
        Container(
          height: Sizes.s3,
          color: Color(0xffF4F4F4),
        )
      ],
    );
  }
}

class ItemMessage extends StatelessWidget {
  final String message;
  final bool isError;
  ItemMessage({@required this.message, this.isError = true});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.info,
          color: isError ? Color(0xffBF2D30) : Colors.amber[600],
          size: Sizes.s15,
        ),
        Container(
          width: 5,
        ),
        Text(
          "$message",
          style: TextStyle(
              color: isError ? Color(0xffBF2D30) : Colors.amber[600],
              fontSize: FontSize.s12),
        )
      ],
    );
  }
}
