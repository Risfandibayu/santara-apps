import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../core/usecases/unauthorize_usecase.dart';
import '../../helpers/PopupHelper.dart';
import '../../helpers/ToastHelper.dart';
import '../../models/User.dart';
import '../../services/security/email_service.dart';
import '../../services/security/pass_service.dart';
import '../../services/security/pin_service.dart';
import '../security_token/otp/OtpPhoneUI.dart';
import '../security_token/otp/OtpViewUI.dart';
import '../security_token/pin/SecurityPinUI.dart';
import '../security_token/user/change_email/ChangeEmailUI.dart';
import '../security_token/user/change_password/ChangePasswordUI.dart';
import '../security_token/user/change_pin/ResetPinUI.dart';
import '../widget/Observer.dart';
import 'bloc/keamanan.bloc.dart';
import 'verif_password.dart';

class KeamananUI extends StatefulWidget {
  @override
  _KeamananUIState createState() => _KeamananUIState();
}

class _KeamananUIState extends State<KeamananUI> {
  bool isActive = false;
  final _pinService = PinService();
  final _bloc = KeamananBloc();
  final _passService = PasswordApiService();
  final _emailService = EmailApiService();
  final storage = new FlutterSecureStorage();

  bool isJson(data) {
    var decodeSucceeded = false;
    try {
      var x = json.decode(data) as Map<String, dynamic>;
      decodeSucceeded = true;
    } on FormatException catch (e) {
      decodeSucceeded = false;
      // print('The provided string is not valid JSON');
    }

    return decodeSucceeded;
  }

  void _listener() {
    _bloc.state.listen((data) {
      switch (data) {
        case KeamananState.loading:
          PopupHelper.showLoading(context);
          break;
        case KeamananState.success:
          Navigator.pop(context);
          // ToastHelper.showBasicToast(context, "Berhasil mengubah fingerprint!");
          break;
        case KeamananState.error:
          Navigator.pop(context);
          break;
        default:
          break;
      }
    }, onError: (error) {
      ToastHelper.showFailureToast(context, error);
    });
  }

  onSuccessCallback(String data) {
    PopupHelper.showLoading(context);
    try {
      _pinService.registerPIN(data).then((res) {
        var parsed = res != null ? json.decode(res.body) : [];
        if (res.statusCode == 200) {
          Navigator.pop(context);
          PopupHelper.successChangeDialog(context);
        } else {
          Navigator.pop(context);
          ToastHelper.showFailureToast(context, parsed["message"]);
        }
      });
    } catch (e) {
      Navigator.pop(context);
      ToastHelper.showFailureToast(
          context, "Terjadi kesalahan saat mengirimkan data!");
    }
  }

  get pembatas {
    return Container(
      height: 0.5,
      color: Color(0xffF4F4F4),
      width: double.infinity,
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    _listener();
    _bloc.getFingerPrintStatus();
  }

  _handleChangeEmail() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          onSuccess: (pin, finger) async {
            PopupHelper.showLoading(context);
            try {
              await _emailService.requestResetEmail().then((val) async {
                if (val.statusCode == 200) {
                  // pop loading
                  Navigator.pop(context);
                  // pop security pin
                  Navigator.pop(context);
                  var datas = userFromJson(await storage.read(key: 'user'));
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OtpViewUI(
                        user: datas,
                        onSuccess: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) =>
                                      ChangeEmailUI(pin: pin, finger: finger)));
                        },
                      ),
                    ),
                  );
                } else {
                  var validJson = isJson(val.body);
                  if (validJson) {
                    var parsed = json.decode(val.body);
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, parsed["message"]);
                  } else {
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, val.body);
                  }
                }
              });
            } catch (e) {
              ToastHelper.showFailureToast(
                  context, "Can't request reset email");
              // pop loading
              Navigator.pop(context);
            }
          },
          type: SecurityType.check,
          title: "Masukan PIN Anda",
        ),
      ),
    );
  }

  _handleChangePassword() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          onSuccess: (pin, finger) async {
            PopupHelper.showLoading(context);
            try {
              await _passService.requestResetPassword().then((val) async {
                if (val.statusCode == 200) {
                  // pop loading
                  Navigator.pop(context);
                  // pop security pin ui
                  Navigator.pop(context);
                  var datas = userFromJson(await storage.read(key: 'user'));
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OtpViewUI(
                        user: datas,
                        onSuccess: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  ChangePasswordUI(pin: pin, finger: finger),
                            ),
                          );
                        },
                      ),
                    ),
                  );
                } else {
                  var validJson = isJson(val.body);
                  if (validJson) {
                    var parsed = json.decode(val.body);
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, parsed["message"]);
                  } else {
                    // pop loading
                    Navigator.pop(context);
                    ToastHelper.showFailureToast(context, val.body);
                  }
                }
              });
            } catch (e) {
              ToastHelper.showFailureToast(
                  context, "Can't request reset password");
              // pop loading
              Navigator.pop(context);
            }
          },
          type: SecurityType.check,
          title: "Masukan PIN Anda",
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Keamanan",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: Color(ColorRev.mainBlack),
        padding: EdgeInsets.all(15),
        child: ListView(
          children: <Widget>[
            // ubah email
            ListTile(
              onTap: _handleChangeEmail,
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Ubah Email",
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
            pembatas,
            // ubah no telpon
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SecurityPinUI(
                      title: "Masukan PIN Anda",
                      type: SecurityType.check,
                      onSuccess: (pin, finger) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VerifPassword(
                                otpType: OtpType.phone,
                                pin: pin,
                                finger: finger),
                          ),
                        );
                      },
                    ),
                  ),
                );
              },
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Ubah No Telepon",
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
            pembatas,
            // ubah password
            ListTile(
              onTap: _handleChangePassword,
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Ubah Kata Sandi",
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
            pembatas,
            // ubah pin
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ResetPinUI(type: ResetPinType.change),
                  ),
                );
              },
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Ubah PIN",
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
              ),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.grey,
              ),
            ),
            pembatas,
            ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                "Fingerprint",
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
              ),
              trailing: Observer<bool>(
                stream: _bloc.fingerPrint,
                onSuccess: (context, data) {
                  return Switch(
                    value: data,
                    onChanged: (value) async {
                      await _bloc.updateFingerPrint(value);
                      if (value) {
                        ToastHelper.showBasicToast(
                            context, "Fingerprint Aktif");
                      } else {
                        ToastHelper.showBasicToast(
                            context, "Fingerprint Tidak Aktif");
                      }
                    },
                    // activeTrackColor: Colors.grey[300],
                    activeColor: Color(0xffBF2D30),
                  );
                },
                onWaiting: (context) => CircularProgressIndicator(),
                onError: (context, err) {
                  return Container(
                    child: Text(
                      "Tidak dapat mengambil data!",
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
