import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/security_token/otp/OtpPhoneUI.dart';
import 'package:santaraapp/widget/security_token/user/change_phone/ChangePhoneUI.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class VerifPassword extends StatefulWidget {
  final OtpType otpType;
  final String pin;
  final bool finger;

  VerifPassword({this.otpType, this.pin, @required this.finger});
  @override
  _VerifPasswordState createState() => _VerifPasswordState();
}

class _VerifPasswordState extends State<VerifPassword> {
  var isLoading = false;
  var refreshToken;
  var username, email, photo, token;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController pass = TextEditingController();
  final storage = new FlutterSecureStorage();
  bool password = true;

  Future getUser() async {
    token = await storage.read(key: 'token');
    refreshToken = await storage.read(key: "refreshToken");
    final data = userFromJson(await storage.read(key: 'user'));
    setState(() {
      username = data.trader.name;
      email = data.email;
      photo = data.trader.photo;
    });
  }

  Future verifyPassword() async {
    setState(() => isLoading = true);
    final response = await http.post(
      '$apiLocal/auth/verify-action',
      body: {'password': pass.text},
      headers: {'Authorization': 'Bearer ' + token},
    );
    if (response.statusCode == 200) {
      setState(() => isLoading = false);
      switch (widget.otpType) {
        case OtpType.phone:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChangePhoneUI(
                // otpType: OtpType.phone,
                pin: widget.pin,
                finger: widget.finger,
              ),
            ),
          );
          break;
        default:
          // print("default");
          break;
      }
    } else {
      setState(() => isLoading = false);
      final data = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(
                  data['message'] == null ? data['error'] : data['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              title: Image.asset('assets/santara/1.png', width: 110.0),
              centerTitle: true,
              iconTheme: IconThemeData(color: Colors.white),
            ),
            backgroundColor: Color(ColorRev.mainBlack),
            body: ListView(
              children: <Widget>[
                _account(),
                _form(),
              ],
            )));
  }

  Widget _account() {
    return Container(
      child: Column(
        children: <Widget>[
          //Account
          Container(
            height: MediaQuery.of(context).size.height / 3.4,
            width: MediaQuery.of(context).size.width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                photo == null
                    ? Container(
                        height: 80,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          image: DecorationImage(
                              image: AssetImage('assets/icon/user.png'),
                              fit: BoxFit.cover),
                        ),
                      )
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: SantaraCachedImage(
                          height: 80,
                          width: 80,
                          image: GetAuthenticatedFile.convertUrl(
                            image: photo,
                            type: PathType.traderPhoto,
                          ),
                          // placeholder: 'assets/icon/user.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                Container(width: 20),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('$username',
                              style: TextStyle(
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                          Padding(
                            padding: const EdgeInsets.only(top: 2, bottom: 6),
                            child: Text('$email',
                                style: TextStyle(
                                    fontSize: 16.0, color: Color(0xFF858585))),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
            child: Text("Untuk melanjutkan, masukkan password",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
          ),
          //password
          Container(
            margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
            child: TextFormField(
                obscureText: password,
                controller: pass,
                validator: (value) =>
                    value.isEmpty ? 'Masukkan password' : null,
                keyboardType: TextInputType.text,
                autofocus: false,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  suffixIcon: IconButton(
                      icon: Icon(
                          password ? Icons.visibility_off : Icons.visibility),
                      onPressed: () {
                        setState(() {
                          password ? password = false : password = true;
                        });
                      }),
                  hintText: 'Password',
                  errorMaxLines: 5,
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.grey)),
                )),
          ),

          //button
          Container(
            padding: EdgeInsets.fromLTRB(20.0, 4.0, 20.0, 20.0),
            child: InkWell(
              onTap: () {
                if (_formKey.currentState.validate()) {
                  verifyPassword();
                }
              },
              child: Container(
                height: 48.0,
                decoration: BoxDecoration(
                  color: Color(0xFF666EE8),
                  border: Border.all(color: Colors.transparent, width: 2.0),
                  borderRadius: BorderRadius.circular(4.0),
                ),
                child: Center(
                    child: Text('Selanjutnya',
                        style: TextStyle(color: Colors.white))),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
