import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/models/User.dart';

class JobPersonal extends StatefulWidget {
  final User user;
  final String token;
  JobPersonal({this.user, this.token});
  @override
  _JobPersonalState createState() => _JobPersonalState();
}

class _JobPersonalState extends State<JobPersonal> {
  final rupiah = new NumberFormat("#,##0");

  String kodeAkunPajak;
  List<Map> kodeAkun = [
    {"id": "1010","name": "1010 Individual - Domestik"},
    {"id": "1011","name": "1011 Individual - Foreign"},
    {"id": "1083","name": "1083 Individual Foreign - Kitas"},
    {"id": "1242","name": "1242 Individual Foreign Kitas - NPWP"},
  ];
  getKodeAkunPajak() {
    for (var i = 0; i < kodeAkun.length; i++) {
      if (kodeAkun[i]["id"] == widget.user.job.taxAccountCode) {
        kodeAkunPajak = kodeAkun[i]["name"];
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getKodeAkunPajak();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Pekerjaan & Aset",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Pekerjaan"),
          _itemData("Pekerjaan", widget.user.job.name == null ? "-" : widget.user.job.name),
          _itemData("Deskripsi Bisnis", widget.user.job.descriptionJob == null ? "-" : widget.user.job.descriptionJob),
          _title("Total Aset"),
          _itemData("Sumber Dana", widget.user.job.sourceOfInvestorFunds == null ? "-" : widget.user.job.sourceOfInvestorFunds),
          _itemData("Pendapatan Pertahun Sebelum Pajak", widget.user.job.income == null ? "-" :  "Rp ${rupiah.format(num.parse(widget.user.job.income))}"),
          _title("Informasi Pajak"),
          _itemData("Kode Akun Pajak", kodeAkunPajak == null ? "" : kodeAkunPajak),
          _itemData("No NPWP", widget.user.job.npwp == null ? "-" : widget.user.job.npwp),
        ],
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}