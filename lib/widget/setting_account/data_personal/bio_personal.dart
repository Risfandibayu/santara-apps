import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/ImageViewerHelper.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class BioPersonal extends StatefulWidget {
  final User user;
  final String token;
  BioPersonal({this.user, this.token});
  @override
  _BioPersonalState createState() => _BioPersonalState();
}

class _BioPersonalState extends State<BioPersonal> {
  // warga negara
  Country warga;
  List<Country> negara = List<Country>();
  Future getWn() async {
    final http.Response response = await http.get('$apiLocal/countries/');
    setState(() {
      negara = countryFromJson(response.body);
      for (var i = 0; i < negara.length; i++) {
        if (negara[i].id == widget.user.trader.countryId) {
          warga = negara[i];
        }
      }
    });
  }

  // pendidikan terakhir
  String pilihPendidikan;
  List<Map> educations = [
    {"id": "1", "name": "Lainnya"},
    {"id": "2", "name": "SD"},
    {"id": "3", "name": "SMP"},
    {"id": "4", "name": "SMA / SMK / D1 / D2"},
    {"id": "5", "name": "D3 / Akademi"},
    {"id": "6", "name": "S1"},
    {"id": "7", "name": "S2"},
    {"id": "8", "name": "S3"},
  ];
  getPendidikanTerakhir() {
    for (var i = 0; i < educations.length; i++) {
      if (educations[i]["id"] == widget.user.trader.educationId.toString()) {
        pilihPendidikan = educations[i]["name"];
      }
    }
  }

  String pilihStatus;
  List<Map> status = [
    {"id": "1", "name": "Single"},
    {"id": "2", "name": "Menikah (Merried)"},
    {"id": "3", "name": "Duda (Widower)"},
    {"id": "4", "name": "Janda (Widow)"},
  ];
  getStatus() {
    for (var i = 0; i < status.length; i++) {
      if (status[i]["id"] == widget.user.trader.maritalStatus) {
        pilihStatus = status[i]["name"];
      }
    }
  }

  String pilihHubungan;
  List<Map> hubungan = [
    {"id": "1", "name": "Orang Tua Kandung"},
    {"id": "2", "name": "Saudara Kandung"},
    {"id": "3", "name": "Anak Kandung"},
    {"id": "4", "name": "Pasangan (Suami Istri)"},
    {"id": "5", "name": "Kakek Nenek Kandung"},
    {"id": "6", "name": "Cucu / Cicit Kandung"},
  ];
  getHubungan() {
    for (var i = 0; i < hubungan.length; i++) {
      if (hubungan[i]["id"] == widget.user.trader.heirRelation) {
        pilihHubungan = hubungan[i]["name"];
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getWn();
    getPendidikanTerakhir();
    getHubungan();
    getStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Biodata Pribadi",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _photo(),
          _itemData("Nama",
              widget.user.trader.name == null ? "-" : widget.user.trader.name),
          _itemData(
              "Tempat Lahir",
              widget.user.trader.birthPlace == null
                  ? "-"
                  : widget.user.trader.birthPlace),
          _itemData(
              "Tanggal Lahir",
              widget.user.trader.birthDate == null
                  ? "-"
                  : widget.user.trader.birthDate),
          _itemData(
              "Jenis Kelamin",
              widget.user.trader.gender == "m"
                  ? "Laki-laki"
                  : widget.user.trader.gender == "f"
                      ? "Perempuan"
                      : "-"),
          _itemData("Pendidikan Terakhir",
              pilihPendidikan == null ? "" : pilihPendidikan),
          _itemData("Kewarganegaraan", warga == null ? "" : warga.name),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: Text("Foto KTP",
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                overflow: TextOverflow.visible),
          ),
          _fotoKTP(),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: Text("Foto Selfie dengan KTP",
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                overflow: TextOverflow.visible),
          ),
          _fotoSelfie(),
          _itemData(
              "Status Pernikahan", pilihStatus == null ? "-" : pilihStatus),
          _itemData(
              "Nama Pasangan",
              widget.user.trader.spouseName == null
                  ? "-"
                  : widget.user.trader.spouseName),
          _itemData(
              "Nama Gadis Ibu Kandung",
              widget.user.trader.motherMaidenName == null
                  ? ""
                  : widget.user.trader.motherMaidenName),
          _itemData("Nama Lengkap Ahli Waris",
              widget.user.trader.heir == null ? "" : widget.user.trader.heir),
          _itemData("Hubungan dengan Ahli Waris",
              pilihHubungan == null ? "-" : pilihHubungan),
          _itemData(
              "No Handphone Ahli Waris",
              widget.user.trader.heirPhone == null
                  ? ""
                  : widget.user.trader.heirPhone),
        ],
      ),
    );
  }

  Widget _fotoKTP() {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(width: 1, color: Colors.grey)),
      child: Column(
        children: <Widget>[
          // ktp == null
          //     ? Container() :
          Container(
            height: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            width: MediaQuery.of(context).size.width,
            child: SantaraCachedImage(
              height: MediaQuery.of(context).size.width,
              width: MediaQuery.of(context).size.width,
              // placeholder: 'assets/Preload.jpeg',
              image: apiLocalImage +
                  widget.user.trader.idcardPhoto.replaceAll("null", ""),
            ),
          ),
        ],
      ),
    );
  }

  Widget _fotoSelfie() {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(width: 1, color: Colors.grey)),
      child: Column(
        children: <Widget>[
          // selfie == null
          //     ? Container() :
          Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            height: MediaQuery.of(context).size.width,
            width: MediaQuery.of(context).size.width,
            child: SantaraCachedImage(
              height: MediaQuery.of(context).size.width,
              width: MediaQuery.of(context).size.width,
              // placeholder: 'assets/Preload.jpeg',
              image: apiLocalImage +
                  widget.user.trader.verificationPhoto.replaceAll("null", ""),
            ),
          ),
        ],
      ),
    );
  }

  Widget _photo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Foto Profil",
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
        ),
        Container(
          height: 100,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 12),
          margin: EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xFFB8B8B8)),
            borderRadius: BorderRadius.circular(4),
          ),
          child: GestureDetector(
            // onTap: () {
            //   _changePhoto();
            // },
            child: widget.user == null
                ? Container(
                    height: 80,
                    width: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80),
                    ),
                    child: Center(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(80),
                          child: Image.asset("assets/Preload.jpeg",
                              height: 80, width: 80)),
                    ),
                  )
                : Container(
                    height: 80,
                    width: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80),
                    ),
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(80),
                        child: SantaraCachedImage(
                          height: 80,
                          width: 80,
                          // placeholder: 'assets/Preload.jpeg',
                          image: GetAuthenticatedFile.convertUrl(
                            image: widget.user.trader.photo,
                            type: PathType.traderPhoto,
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
          ),
        )
      ],
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}
