import 'package:flutter/material.dart';
import 'package:santaraapp/models/BankInvestor.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class BankPersonal extends StatefulWidget {
  final User user;
  final String token;
  BankPersonal({this.user, this.token});
  @override
  _BankPersonalState createState() => _BankPersonalState();
}

class _BankPersonalState extends State<BankPersonal> {

  // bank investor
  BankInvestor bankInvestor;
  BankInvestor bankInvestor2;
  BankInvestor bankInvestor3;
  List<BankInvestor> bankInvestors = List<BankInvestor>();
  List<DropdownMenuItem<BankInvestor>> dropDownBankInvestor = [];
  Future getBankInvestor() async {
    final http.Response response = await http.get('$apiLocal/banks/bank-investors');
    setState(() {
      bankInvestors = bankInvestorFromJson(response.body);
      if (widget.user.traderBank != null) {
        for (var i = 0; i < bankInvestors.length; i++) {
          if (bankInvestors[i].id == widget.user.traderBank.bankInvestor1) {
            bankInvestor = bankInvestors[i];
          }
          if (bankInvestors[i].id == widget.user.traderBank.bankInvestor2) {
            bankInvestor2 = bankInvestors[i];
          }
          if (bankInvestors[i].id == widget.user.traderBank.bankInvestor3) {
            bankInvestor3 = bankInvestors[i];
          }
        }
      }
    });
  }

  String pilihCurrency;
  String pilihCurrency2;
  String pilihCurrency3;
  List<Map> currency = [
    {"value": "IDR", "name": "IDR (Indonesian Rupiah)"},
    {"value": "USD", "name": "USD (United State Dollar)"},
    {"value": "SGD", "name": "SGD (Singapore Dollar)"},
  ];
  getCurrency() {
    if (widget.user.traderBank != null) {
      for (var i = 0; i < currency.length; i++) {
        if (currency[i]["value"] == widget.user.traderBank.accountCurrency1){
          pilihCurrency = currency[i]["name"];
        }
        if (currency[i]["value"] == widget.user.traderBank.accountCurrency2){
          pilihCurrency2 = currency[i]["name"];
        }
        if (currency[i]["value"] == widget.user.traderBank.accountCurrency3){
          pilihCurrency3 = currency[i]["name"];
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getBankInvestor();
    getCurrency();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Bank",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Akun Bank"),
          _itemData("Nama Pemilik Rekening", widget.user.traderBank == null || widget.user.traderBank.accountName1 == null ? "-" : widget.user.traderBank.accountName1),
          _itemData("Nama Bank", bankInvestor == null ? "-" : bankInvestor.bank),
          _itemData("Mata Uang Utama", pilihCurrency == null ? "-" : pilihCurrency),
          _itemData("No Rekening", widget.user.traderBank == null || widget.user.traderBank.accountNumber1 == null ? "-" : widget.user.traderBank.accountNumber1),
          _title("Akun Bank 2"),
          _itemData("Nama Pemilik Rekening", widget.user.traderBank == null || widget.user.traderBank.accountName2 == null ? "-" : widget.user.traderBank.accountName2),
          _itemData("Nama Bank", bankInvestor2 == null ? "-" : bankInvestor2.bank),
          _itemData("Mata Uang Utama", pilihCurrency2 == null ? "-" : pilihCurrency2),
          _itemData("No Rekening", widget.user.traderBank == null || widget.user.traderBank.accountNumber2 == null ? "-" : widget.user.traderBank.accountNumber2),
          _title("Akun Bank 3"),
          _itemData("Nama Pemilik Rekening", widget.user.traderBank == null || widget.user.traderBank.accountName3 == null ? "-" : widget.user.traderBank.accountName3),
          _itemData("Nama Bank", bankInvestor3 == null ? "-" : bankInvestor3.bank),
          _itemData("Mata Uang Utama", pilihCurrency3 == null ? "-" : pilihCurrency3),
          _itemData("No Rekening", widget.user.traderBank == null || widget.user.traderBank.accountNumber3 == null ? "-" : widget.user.traderBank.accountNumber3),
        ],
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}