import 'package:flutter/material.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/Province.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class AddressPersonal extends StatefulWidget {
  final User user;
  final String token;
  AddressPersonal({this.user, this.token});
  @override
  _AddressPersonalState createState() => _AddressPersonalState();
}

class _AddressPersonalState extends State<AddressPersonal> {

  Country idCardCountry;
  Country countryDomicile;
  List<Country> negara = List<Country>();
  Future getCountry() async {
    final http.Response response = await http.get('$apiLocal/countries/');
    setState(() {
      negara = countryFromJson(response.body);
      for (var i = 0; i < negara.length; i++) {
        if (negara[i].id == widget.user.trader.idcardCountry) {
          idCardCountry = negara[i];
        }
        if (negara[i].id == widget.user.trader.countryDomicile) {
          countryDomicile = negara[i];
        }
      }
    });
  }

  Province idCardProvince;
  Province provinceDomicile;
  List<Province> listProv = new List<Province>();
  Future getProvinces() async {
    final http.Response response = await http.get('$apiLocal/province/');
    if (response.statusCode == 200) {
      setState(() {
        listProv = provinceFromJson(response.body);
        for (var i = 0; i < listProv.length; i++) {
          if (listProv[i].id == widget.user.trader.idcardProvince) {
            idCardProvince = listProv[i];
            getRegencies(idCardProvince.id, true);
          }
          if (listProv[i].name.toLowerCase() == widget.user.trader.province.toLowerCase()) {
            provinceDomicile = listProv[i];
            getRegencies(provinceDomicile.id, false);
          }
        }
      });
    }
  }

  Regency idCardRegency;
  Regency regencyDomicile;
  Future getRegencies(int id, bool isKTP) async {
    final http.Response response = await http.get('$apiLocal/regencies/$id');
    if (response.statusCode == 200) {
      setState(() {
        var regList = regencyFromJson(response.body);
        for (var i = 0; i < regList.length; i++) {
          if (isKTP) {
            if (regList[i].id == widget.user.trader.idcardRegency){
              idCardRegency = regList[i];
            }
          } else {
            if (regList[i].name.toLowerCase() == widget.user.trader.regency.toLowerCase()){
              regencyDomicile = regList[i];
            }
          }
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getCountry();
    getProvinces();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Alamat",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Alamat Sesuai KTP"),
          _itemData("Negara", idCardCountry == null ? "-" : idCardCountry.name),
          _itemData("Provinsi", idCardProvince == null ? "OTHERS" : idCardProvince.name),
          _itemData("Kabupaten / Kota", idCardRegency == null ? "OTHERS" : idCardProvince.name),
          _itemData("Alamat Lengkap", widget.user.trader.idcardAddress == null ? "-" : widget.user.trader.idcardAddress),
          _itemData("Kode Pos", widget.user.trader.idcardPostalCode == null ? "-" : widget.user.trader.idcardPostalCode),
          _title("Alamat Tinggal Sekarang"),
          _itemData("Negara", countryDomicile == null ? "-" : countryDomicile.name),
          _itemData("Provinsi", provinceDomicile == null ? "OTHERS" : provinceDomicile.name),
          _itemData("Kabupaten / Kota", regencyDomicile == null ? "OTHERS" : regencyDomicile.name),
          _itemData("Alamat Lengkap", widget.user.trader.address == null ? "-" : widget.user.trader.address),
          _itemData("Kode Pos", widget.user.trader.postalCode == null ? "-" : widget.user.trader.postalCode),
          _title("No. Handphone"),
          _itemData("No Handphone", widget.user.phone == null ? "-" : widget.user.phone),
          _itemData("No Telp Rumah", widget.user.trader.phoneHome == null ? "-" : widget.user.trader.phoneHome),
        ],
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}