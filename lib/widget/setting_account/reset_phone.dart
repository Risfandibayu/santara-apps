import 'dart:convert';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/helper.dart';

class ResetPhone extends StatefulWidget {
  @override
  _ResetPhoneState createState() => _ResetPhoneState();
}

class _ResetPhoneState extends State<ResetPhone> {
  var isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  final focusNo = new FocusNode();
  final TextEditingController phone = TextEditingController();
  var token;
  var datas;
  var refreshToken;
  int isRightPhone = 0;
  List<CountryCode> elements;
  CountryCode countryCode = new CountryCode(dialCode: "+62", code: "ID");

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
    datas = userFromJson(await storage.read(key: 'user'));
    refreshToken = await storage.read(key: "refreshToken");
  }

  Future changePhone() async {
    setState(() => isLoading = true);
    final response = await http.put(
      '$apiLocal/traders/reset-phone',
      body: {
        "phone": countryCode.dialCode + phone.text,
      },
      headers: {'Authorization': 'Bearer ' + token},
    );
    if (response.statusCode == 200) {
      setState(() => isLoading = false);
      changeSuccess();
    } else {
      setState(() => isLoading = false);
      final data = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(
                  data['message'] == null ? data['error'] : data['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future logout() async {
    if (token != null) {
      final id = datas.id;
      await http.post('$apiLocal/auth/logout', body: {
        "refreshToken": "$refreshToken",
        "user_id": "$id",
      }).timeout(Duration(seconds: 20));
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'AddressCyro');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      storage.delete(key: 'hasSubmitJob');
      storage.delete(key: 'hasSubmitPriority');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: CircularProgressIndicator(),
        opacity: 0.5,
        child: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Container(height: 40),
                  Image.asset("assets/icon/verification.png",
                      height: MediaQuery.of(context).size.width * 2 / 3,
                      width: MediaQuery.of(context).size.width * 2 / 3),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 6),
                    child: Text("Atur Ulang Nomor Handphone",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                    child: Text(
                      "Masukkan nomor handphone yang baru",
                      textAlign: TextAlign.center,
                    ),
                  ),

                  //nohp
                  Container(
                      margin: EdgeInsets.fromLTRB(30.0, 8.0, 30.0, 2.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: isRightPhone == 0
                                  ? Colors.grey
                                  : Color(0xFFBF2D30)),
                          borderRadius: BorderRadius.circular(4.0)),
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 8),
                            height: 55,
                            decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius: BorderRadius.circular(4.0)),
                            child: CountryCodePicker(
                              onChanged: (value) {
                                setState(() => countryCode = value);
                              },
                              initialSelection: 'ID',
                            ),
                          ),
                          Flexible(
                              child: TextFormField(
                                  controller: phone,
                                  keyboardType: TextInputType.phone,
                                  focusNode: focusNo,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      FocusScope.of(context)
                                          .requestFocus(focusNo);
                                      setState(() => isRightPhone = 1);
                                      return null;
                                    } else if (!RegExp(r"^[0-9]*$")
                                        .hasMatch(value)) {
                                      FocusScope.of(context)
                                          .requestFocus(focusNo);
                                      setState(() => isRightPhone = 2);
                                      return null;
                                    } else if (value.length < 7) {
                                      FocusScope.of(context)
                                          .requestFocus(focusNo);
                                      setState(() => isRightPhone = 3);
                                      return null;
                                    } else {
                                      setState(() => isRightPhone = 0);
                                      return null;
                                    }
                                  },
                                  autofocus: false,
                                  decoration: InputDecoration(
                                    hintText: 'Contoh: 812xxxxxxx',
                                    border: InputBorder.none,
                                  ))),
                        ],
                      )),
                  isRightPhone == 0
                      ? Container(
                          margin: EdgeInsets.only(bottom: 20),
                        )
                      : Container(
                          margin: EdgeInsets.fromLTRB(40, 0, 8, 20),
                          child: Text(
                              isRightPhone == 1
                                  ? "Masukkan nomor telepon anda"
                                  : isRightPhone == 2
                                      ? "Masukkan inputan tanpa spesial karakter"
                                      : "No telepon kurang dari 8 digit",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.red[700])),
                        ),

                  //button
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 4.0, 20.0, 20.0),
                    child: InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          if (isRightPhone == 0) {
                            changePhone();
                          }
                        }
                      },
                      child: Container(
                        height: 48.0,
                        decoration: BoxDecoration(
                          color: Color(0xFF666EE8),
                          border:
                              Border.all(color: Colors.transparent, width: 2.0),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Center(
                            child: Text('Selanjutnya',
                                style: TextStyle(color: Colors.white))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 30),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () => Navigator.pop(context),
                ))
          ],
        ),
      ),
    );
  }

  void changeSuccess() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: Container(
            height: MediaQuery.of(context).size.width / 2 + 160,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: <Widget>[
                Image.asset("assets/icon/success.png",
                    height: MediaQuery.of(context).size.width / 2,
                    width: MediaQuery.of(context).size.width / 2),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
                    child: Text(
                      "Perubahan e-mail berhasil",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 20),
                    child: Text(
                      "Verifikasi email mu untuk menyelesaikan proses ganti e-mail",
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => logout().then((_) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (_) => Home(index: 4)),
                        (Route<dynamic> route) => false);
                  }),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text("Masuk",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ));
        });
  }
}
