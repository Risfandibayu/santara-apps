import 'package:flutter/material.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/Province.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class AddressCompany extends StatefulWidget {
  final User user;
  final String token;
  AddressCompany({this.user, this.token});
  @override
  _AddressCompanyState createState() => _AddressCompanyState();
}

class _AddressCompanyState extends State<AddressCompany> {

  Country country;
  List<Country> negara = List<Country>();
  Future getCountry() async {
    final http.Response response = await http.get('$apiLocal/countries/');
    setState(() {
      negara = countryFromJson(response.body);
      for (var i = 0; i < negara.length; i++) {
        if (negara[i].id == widget.user.trader.companyCountryAddress) {
          country = negara[i];
        }
      }
    });
  }

  Province province;
  List<Province> listProv = new List<Province>();
  Future getProvinces() async {
    final http.Response response = await http.get('$apiLocal/province/');
    if (response.statusCode == 200) {
      setState(() {
        listProv = provinceFromJson(response.body);
        for (var i = 0; i < listProv.length; i++) {
          if (listProv[i].id == widget.user.trader.companyProvinceAddress) {
            province = listProv[i];
            getRegencies(province.id);
          }
        }
      });
    }
  }

  Regency regency;
  Future getRegencies(int id) async {
    final http.Response response = await http.get('$apiLocal/regencies/$id');
    if (response.statusCode == 200) {
      setState(() {
        var regList = regencyFromJson(response.body);
        for (var i = 0; i < regList.length; i++) {
          if (regList[i].id == widget.user.trader.companyRegencyAddress){
            regency = regList[i];
          }
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getCountry();
    getProvinces();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Alamat Perusahaan",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Alamat Perusahaan"),
          _itemData("Negara", country == null ? "" : country.name),
          _itemData("Provinsi", province == null ? "OTHERS" : province.name),
          _itemData("Kabupaten Kota", regency == null ? "OTHERS" : regency.name),
          _itemData("Alamat Perusahaan", widget.user.trader.companyAddress == null ? "" : widget.user.trader.companyAddress),
          _itemData("No Handphone", widget.user.phone == null ? "" : widget.user.phone),
          _itemData("No Telp Kantor", widget.user.trader.companyPhoneNumber == null ? "-" : widget.user.trader.companyPhoneNumber),
        ]
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}