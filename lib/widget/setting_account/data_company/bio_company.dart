import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/models/CompanyCharacter.dart';
import 'package:santaraapp/models/CompanyType.dart';
import 'package:santaraapp/models/Country.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class BioCompany extends StatefulWidget {
  final User user;
  final String token;
  BioCompany({this.user, this.token});
  @override
  _BioCompanyState createState() => _BioCompanyState();
}

class _BioCompanyState extends State<BioCompany> {
  DateFormat dateFormat = DateFormat("dd LLLL yyyy", "id");
  CompanyType pilihJenis;
  List<CompanyType> jenis;
  Future getCompanyType() async {
    final http.Response response = await http.get('$apiLocal/company/types');
    if (response.statusCode == 200) {
      setState(() {
        jenis = companyTypeFromJson(response.body);
        for (var i = 0; i < jenis.length; i++) {
          if (jenis[i].id == widget.user.trader.companyType) {
            pilihJenis = jenis[i];
          }
        }
      });
    } else {
      jenis = null;
    }
  }

  CompanyCharacter pilihKarakter;
  List<CompanyCharacter> karakter;
  Future getCompanyCharacter() async {
    final http.Response response =
        await http.get('$apiLocal/company/characters');
    if (response.statusCode == 200) {
      setState(() {
        karakter = companyCharacterFromJson(response.body);
        for (var i = 0; i < karakter.length; i++) {
          if (karakter[i].id == widget.user.trader.companyCharacter) {
            pilihKarakter = karakter[i];
          }
        }
      });
    } else {
      jenis = null;
    }
  }

  Country pilihDomisili;
  List<Country> domisili = List<Country>();
  Future getCountries() async {
    final http.Response response = await http.get('$apiLocal/countries/');
    setState(() {
      domisili = countryFromJson(response.body);
      for (var i = 0; i < domisili.length; i++) {
        if (domisili[i].id == widget.user.trader.companyCountryDomicile) {
          pilihDomisili = domisili[i];
        }
      }
    });
  }

  String pilihKodePajak;
  List<Map> kodePajak = [
    {"code": "1016", "name": "1016 - Asabri"},
    {"code": "1018", "name": "1018 - Badan Usaha Tetap"},
    {"code": "1077", "name": "1077 - Badan Usaha Tetap Khusus Non Tax"},
    {"code": "1008", "name": "1008 - Bank - Domestik"},
    {"code": "1009", "name": "1009 - Bank -  Foreign (Joint Venture)"},
    {"code": "1036", "name": "1036 - Bank - Pure Foreign"},
    {"code": "1001", "name": "1001 - Broker (Local Broker)"},
    {"code": "1002", "name": "1002 - Custodian Bank"},
    {"code": "1067", "name": "1067 - Financial Institution"},
    {"code": "1078", "name": "1078 - Government of Indonesia"},
    {"code": "1035", "name": "1035 - Institution - Domestic (Non FI)"},
    {"code": "1017", "name": "1017 - Institution - Foreign (Non Fi)"},
    {"code": "1004", "name": "1004 - Institution Foreign No Tax"},
    {"code": "1100", "name": "1100 - Insurance Non NPWP"},
    {"code": "1019", "name": "1019 - Insurance NPWP"},
    {"code": "1005", "name": "1005 - Issuer"},
    {"code": "1015", "name": "1015 - Jamsostek JHT"},
    {"code": "1082", "name": "1082 - Jamsostek Non JHT"},
    {"code": "1013", "name": "1013 - Koperasi"},
    {"code": "1006", "name": "1006 - Mutual Fund"},
    {"code": "1080", "name": "1080 - Mutual Fund - More then 5 years"},
    {"code": "1007", "name": "1007 - Pension Fund"},
    {"code": "1098", "name": "1098 - Perusahaan Terbatas Non NPWP"},
    {"code": "1037", "name": "1037 - Perusahaan Terbatas NPWP"},
    {"code": "1014", "name": "1014 - Taspen"},
    {"code": "1099", "name": "1099 - Yayasan Non NPWP"},
    {"code": "1012", "name": "1012 - Yayasan NPWP"},
  ];
  getKodePajak() {
    for (var i = 0; i < kodePajak.length; i++) {
      if (kodePajak[i]["code"] == widget.user.trader.taxAccountCode) {
        pilihKodePajak = kodePajak[i]["name"];
      }
    }
  }

  bool isDate(String str) {
    try {
      DateTime.parse(str);
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    getKodePajak();
    getCountries();
    getCompanyCharacter();
    getCompanyType();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text("Biodata Perusahaan",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            _title("Profil Perusahaan"),
            _itemData(
                "Nama Perusahaan",
                widget.user.trader.name == null
                    ? "-"
                    : widget.user.trader.name),
            _itemData(
                "Jenis Perusahaan", pilihJenis == null ? "-" : pilihJenis.type),
            _itemData("Karakter Perusahaan",
                pilihKarakter == null ? "-" : pilihKarakter.character),
            _itemData(
                "Apakah Perusahaan Anda berbasis Syariah",
                widget.user.trader.companySyariah == null
                    ? "-"
                    : widget.user.trader.companySyariah == "Y"
                        ? "Ya"
                        : "Tidak"),
            _itemData("Domisili Perusahaan",
                pilihDomisili == null ? "-" : pilihDomisili.name),
            _itemData(
                "Tempat Pendirian Usaha",
                widget.user.trader.companyEstablishmentPlace == null
                    ? "-"
                    : widget.user.trader.companyEstablishmentPlace),
            _itemData(
              "Tanggal Pendirian Usaha",
              widget.user.trader.companyDateEstablishment != null && isDate(widget.user.trader.companyDateEstablishment)
                  ? dateFormat.format(
                      DateTime.parse(
                          widget.user.trader.companyDateEstablishment),
                    ) : "-",
            ),
            _itemData(
                "Email", widget.user.email == null ? "-" : widget.user.email),
            _itemData(
                "Email Lain",
                widget.user.trader.anotherEmail == null
                    ? "-"
                    : widget.user.trader.anotherEmail),
            _title("Informasi Pajak"),
            _itemData("Kode Akun Pajak",
                pilihKodePajak == null ? "-" : pilihKodePajak),
            _itemData(
                "No NPWP",
                widget.user.trader.npwp == null
                    ? "-"
                    : widget.user.trader.npwp),
            _title("Nomor Perizinan Perusahaan"),
            _itemData(
                "Nomor SIUP",
                widget.user.trader.siup == null
                    ? "-"
                    : widget.user.trader.siup),
            _itemData(
                "Nomor Akta Pendirian Usaha",
                widget.user.trader.companyCertificateNumber == null
                    ? "-"
                    : widget.user.trader.companyCertificateNumber),
            _itemData("Nomor NIB",
                widget.user.trader.nib == null ? "-" : widget.user.trader.nib),
          ],
        ));
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 30, 0, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}
