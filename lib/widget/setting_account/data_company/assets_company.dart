import 'package:flutter/material.dart';
import 'package:santaraapp/models/User.dart';

class AssetCompany extends StatefulWidget {
  final User user;
  final String token;
  AssetCompany({this.user, this.token});
  @override
  _AssetCompanyState createState() => _AssetCompanyState();
}

class _AssetCompanyState extends State<AssetCompany> {

  String pilihDokumen;
  List<Map> dokumenList = [
    {"id": 1, "name": "Copy AD/ART"},
    {"id": 2, "name": "SKMK (Surat Keputusan Mentri Keuangan)"},
    {"id": 3, "name": "Surat Ketetapan Pemerintah RI"},
    {"id": 4, "name": "Surat Pernyataan Efektif dari OJK"},
    {"id": 5, "name": "Keputusan Dirjen Pajak"},
    {"id": 6, "name": "Surat Keputusan dari OJK"},
  ];
  getDokumenList() {
    for (var i = 0; i < dokumenList.length; i++) {
      if (widget.user.trader.companyTypeDocument != null && dokumenList[i]["name"].toLowerCase() == widget.user.trader.companyTypeDocument.toLowerCase()) {
        pilihDokumen = dokumenList[i]["name"];
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getDokumenList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Aset & Dokumen",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Keterangan Aset"),
          _itemData("Sumber Dana", widget.user.trader.sourceOfFunds == null ? "-" : widget.user.trader.sourceOfFunds),
          _title("Profit Perusahaan"),
          _itemData("Profit Perusahaan 1 Tahun Terakhir", widget.user.trader.companyIncome1 == null ? "-" : widget.user.trader.companyIncome1),
          _itemData("Profit Perusahaan 2 Tahun Terakhir", widget.user.trader.companyIncome2 == null ? "-" : widget.user.trader.companyIncome2),
          _itemData("Profit Perusahaan 3 Tahun Terakhir", widget.user.trader.companyIncome3 == null ? "-" : widget.user.trader.companyIncome3),
          _title("Total Kekayaan"),
          _itemData("Total Kekayaan 1 Tahun Terakhir", widget.user.trader.companyTotalProperty1 == null ? "-" : widget.user.trader.companyTotalProperty1),
          _itemData("Total Kekayaan 2 Tahun Terakhir", widget.user.trader.companyTotalProperty2 == null ? "-" : widget.user.trader.companyTotalProperty2),
          _itemData("Total Kekayaan 3 Tahun Terakhir", widget.user.trader.companyTotalProperty3 == null ? "-" : widget.user.trader.companyTotalProperty3),
          _title("Dokumen Perusahaan"),
          _itemData("Pilih Dokumen Perusahaan", pilihDokumen == null ? "-" : pilihDokumen),
          _itemData("Nomor Dokumen Perusahaan", widget.user.trader.companyDocumentNumber == null ? "-" : widget.user.trader.companyDocumentNumber),
          _itemData("Tanggal Kadaluarsa Dokumen", widget.user.trader.companyDocumentExpiredDate == null ? "-" : widget.user.trader.companyDocumentExpiredDate),
        ]
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}