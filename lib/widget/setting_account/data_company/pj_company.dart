import 'package:flutter/material.dart';
import 'package:santaraapp/models/User.dart';

class PjCompany extends StatefulWidget {
  final User user;
  final String token;
  PjCompany({this.user, this.token});
  @override
  _PjCompanyState createState() => _PjCompanyState();
}

class _PjCompanyState extends State<PjCompany> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Penanggung Jawab",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _title("Penanggung Jawab Perusahaan"),
          _itemData("Nama Lengkap", widget.user.trader.companyResponsibleName1 == null ? "-" : widget.user.trader.companyResponsibleName1),
          _itemData("Jabatan", widget.user.trader.companyResponsiblePosition1 == null ? "-" : widget.user.trader.companyResponsiblePosition1),
          _itemData("No KTP", widget.user.trader.companyResponsibleIdcardNumber1 == null ? "-" : widget.user.trader.companyResponsibleIdcardNumber1),
          _title("Penanggung Jawab Perusahaan 2"),
          _itemData("Nama Lengkap", widget.user.trader.companyResponsibleName2 == null ? "-" : widget.user.trader.companyResponsibleName2),
          _itemData("Jabatan", widget.user.trader.companyResponsiblePosition2 == null ? "-" : widget.user.trader.companyResponsiblePosition2),
          _itemData("No KTP", widget.user.trader.companyResponsibleIdcardNumber2 == null ? "-" : widget.user.trader.companyResponsibleIdcardNumber2),
          _title("Penanggung Jawab Perusahaan 3"),
          _itemData("Nama Lengkap", widget.user.trader.companyResponsibleName3 == null ? "-" : widget.user.trader.companyResponsibleName3),
          _itemData("Jabatan", widget.user.trader.companyResponsiblePosition3 == null ? "-" : widget.user.trader.companyResponsiblePosition3),
          _itemData("No KTP", widget.user.trader.companyResponsibleIdcardNumber3 == null ? "-" : widget.user.trader.companyResponsibleIdcardNumber3),
          _title("Penanggung Jawab Perusahaan 4"),
          _itemData("Nama Lengkap", widget.user.trader.companyResponsibleName4 == null ? "-" : widget.user.trader.companyResponsibleName4),
          _itemData("Jabatan", widget.user.trader.companyResponsiblePosition4 == null ? "-" : widget.user.trader.companyResponsiblePosition4),
          _itemData("No KTP", widget.user.trader.companyResponsibleIdcardNumber4 == null ? "-" : widget.user.trader.companyResponsibleIdcardNumber4),
        ]
      )
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,30,0,16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 2,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFE5E5E5),
          )
        ],
      ),
    );
  }

  Widget _itemData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
            child: Text(value,
                style: TextStyle(color: Color(0xFF676767), fontSize: 16)),
          ),
        ],
      ),
    );
  }
}