import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/alamat/AlamatUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/bank/KycBankUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/bio_keluarga/BioKeluargaUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/BioPribadiUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/informasi_pajak/InformasiPajakUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/pekerjaan/PekerjaanUI.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/syarat_ketentuan/SyaratKetentuanUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/alamat/AlamatUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/bank/BankUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/biodata/BiodataUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/biodata_keluarga/BiodataKeluargaUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/pajak/PajakUI.dart';
import 'package:santaraapp/widget/kyc/view_data/pribadi/pekerjaan/PekerjaanUI.dart';

class KycPribadiSetting extends StatefulWidget {
  final bool preview;
  KycPribadiSetting({@required this.preview});

  @override
  _KycPribadiSettingState createState() => _KycPribadiSettingState();
}

class _KycPribadiSettingState extends State<KycPribadiSetting> {
  KycPersonalService _service = KycPersonalService();
  bool isLoaded = false;
  bool isError = false;
  bool isEditKyc = false;

  StatusKycTrader statusIndividual = StatusKycTrader();
  var result = {};
  User userData = User();

  bool isActive(String status) {
    switch (status) {
      case "verifying":
        return true;
        break;
      case "rejected":
        return true;
        break;
      case "kustodian_rejected":
        return true;
        break;
      case "kustodian_verifying":
        return true;
        break;
      case "verified":
        return true;
        break;
      case "empty":
        return false;
        break;
      case "data_update":
        return false;
        break;
      case "waiting":
        return false;
        break;
      default:
        return false;
    }
  }

  openPage(Widget page) async {
    final res = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => page,
      ),
    );
    await getSubmissionView(sessionCheck: false);
  }

  // jika view data
  getSubmissionView({bool sessionCheck = true}) async {
    setState(() {
      isLoaded = false;
      isError = false;
    });
    try {
      await _service.getUserData().then((traderRes) async {
        if (traderRes.statusCode == 200) {
          await _service.statusIndividualKyc().then((res) {
            setState(() {
              userData = User.fromJson(traderRes.data);
              // print(">> TNC STATUS : ${userData.trader.tnc}");
              statusIndividual = StatusKycTrader.fromJson(res.data);
              isLoaded = true;
              isError = false;
            });
          });
        }
      });
    } catch (e) {
      // print(e);
      setState(() {
        isLoaded = true;
        isError = true;
      });
    }
  }

  bool showTnc() {
    if (statusIndividual.statusKyc1 != "empty" &&
        statusIndividual.statusKyc1 != "rejected" &&
        statusIndividual.statusKyc2 != "empty" &&
        statusIndividual.statusKyc2 != "rejected" &&
        statusIndividual.statusKyc3 != "empty" &&
        statusIndividual.statusKyc3 != "rejected" &&
        statusIndividual.statusKyc4 != "empty" &&
        statusIndividual.statusKyc4 != "rejected" &&
        statusIndividual.statusKyc5 != "empty" &&
        statusIndividual.statusKyc5 != "rejected" &&
        statusIndividual.statusKyc6 != "empty" &&
        statusIndividual.statusKyc6 != "rejected") {
      return true;
    } else {
      return false;
    }
  }

  // jika edit data
  getSubmissionId() async {
    setState(() {
      isLoaded = false;
      isError = false;
    });
    try {
      await _service.getSubmissionId().then((value) async {
        if (value.statusCode == 200) {
          await _service.getUserData().then((traderRawRes) async {
            await _service.statusIndividualKyc().then((res) {
              if (value != null) {
                setState(() {
                  var traderRes = User.fromJson(traderRawRes.data);
                  isEditKyc = traderRes.trader.isEditKyc != null
                      ? traderRes.trader.isEditKyc == 0
                          ? false
                          : true
                      : false;
                  userData = traderRes;
                  result = value.data;
                  statusIndividual = StatusKycTrader.fromJson(res.data);
                  // print(res.toJson());
                  isLoaded = true;
                  isError = false;
                });
              } else {
                setState(() {
                  isLoaded = true;
                  isError = true;
                });
              }
            });
          });
        } else if (value.statusCode == 400) {
          ToastHelper.showFailureToast(context, value.data["message"]);
          setState(() {
            isLoaded = true;
            isError = true;
          });
        } else {
          setState(() {
            isLoaded = true;
            isError = true;
          });
          ToastHelper.showFailureToast(
              context, "Tidak dapat mengambil submission!");
        }
      });
    } catch (e) {
      // print(e);
      setState(() {
        isLoaded = true;
        isError = true;
      });
    }
  }

  getSubmission() async {
    if (widget.preview) {
      await getSubmissionView();
    } else {
      await getSubmissionId();
    }
  }

  @override
  void initState() {
    super.initState();
    // print(">> Status : ${widget.preview}");
    getSubmission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(ColorRev.mainBlack),
        title: Text(widget.preview ? "Lihat Data KYC" : "Ubah Data KYC",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.black,
        padding: EdgeInsets.all(Sizes.s15),
        child: isLoaded
            ? isError
                ? Center(
                    child: IconButton(
                        icon: Icon(Icons.replay),
                        onPressed: () => getSubmission()),
                  )
                : ListView(
                    children: [
                      ItemSetting(
                        title: "Biodata Pribadi",
                        subtitle: "Data Diri, No. Telepon & Email",
                        onTap: () => widget.preview
                            ? openPage(
                                ViewBiodataUI(
                                  submissionId: null,
                                  isEdit: isActive(statusIndividual.statusKyc1),
                                  status: statusIndividual.statusKyc1,
                                ),
                              )
                            : openPage(
                                BiodataPribadiUI(
                                  submissionId: statusIndividual.statusKyc1 ==
                                          "rejected"
                                      ? "${userData.trader.lastKycSubmissionIdRejected}"
                                      : result["id"].toString(),
                                  isEdit: isEditKyc
                                      ? isEditKyc
                                      : isActive(statusIndividual.statusKyc1),
                                  status: statusIndividual.statusKyc1,
                                ),
                              ),
                        status: statusIndividual.statusKyc1,
                      ),
                      ItemSetting(
                        title: "Biodata Keluarga",
                        subtitle: "Status Pernikahan & Ahli Waris",
                        onTap: () => widget.preview
                            ? openPage(ViewBiodataKeluargaUI(
                                submissionId: null,
                                isEdit: isActive(statusIndividual.statusKyc2),
                                status: statusIndividual.statusKyc2,
                              ))
                            : openPage(BiodataKeluargaUI(
                                submissionId: statusIndividual.statusKyc2 ==
                                        "rejected"
                                    ? "${userData.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                                isEdit: isEditKyc
                                    ? isEditKyc
                                    : isActive(statusIndividual.statusKyc2),
                                status: statusIndividual.statusKyc2,
                              )),
                        status: statusIndividual.statusKyc1 == "empty"
                            ? "waiting"
                            : statusIndividual.statusKyc2,
                      ),
                      ItemSetting(
                        title: "Alamat",
                        subtitle: "Alamat KTP & Tempat Tinggal  ",
                        onTap: () => widget.preview
                            ? openPage(ViewAlamatUI(
                                submissionId: null,
                                isEdit: isActive(statusIndividual.statusKyc3),
                                status: statusIndividual.statusKyc3,
                              ))
                            : openPage(AlamatUI(
                                submissionId: statusIndividual.statusKyc3 ==
                                        "rejected"
                                    ? "${userData.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                                isEdit: isEditKyc
                                    ? isEditKyc
                                    : isActive(statusIndividual.statusKyc3),
                                status: statusIndividual.statusKyc3,
                              )),
                        status: statusIndividual.statusKyc2 == "empty"
                            ? "waiting"
                            : statusIndividual.statusKyc3,
                      ),
                      ItemSetting(
                        title: "Pekerjaan",
                        subtitle: "Pekerjaan, Total Aset & Motivasi",
                        onTap: () => widget.preview
                            ? openPage(ViewPekerjaanUI(
                                submissionId: null,
                                isEdit: isActive(statusIndividual.statusKyc4),
                                status: statusIndividual.statusKyc4,
                              ))
                            : openPage(KycPekerjaanUI(
                                submissionId: statusIndividual.statusKyc4 ==
                                        "rejected"
                                    ? "${userData.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                                isEdit: isEditKyc
                                    ? isEditKyc
                                    : isActive(statusIndividual.statusKyc4),
                                status: statusIndividual.statusKyc4,
                              )),
                        status: statusIndividual.statusKyc3 == "empty"
                            ? "waiting"
                            : statusIndividual.statusKyc4,
                      ),
                      ItemSetting(
                        title: "Informasi Pajak",
                        subtitle: "Pendapatan Per Tahun & No NPWP",
                        onTap: () => widget.preview
                            ? openPage(ViewPajakUI(
                                submissionId: null,
                                isEdit: isActive(statusIndividual.statusKyc5),
                                status: statusIndividual.statusKyc5,
                              ))
                            : openPage(InformasiPajakUI(
                                submissionId: statusIndividual.statusKyc5 ==
                                        "rejected"
                                    ? "${userData.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                                isEdit: isEditKyc
                                    ? isEditKyc
                                    : isActive(statusIndividual.statusKyc5),
                                status: statusIndividual.statusKyc5,
                              )),
                        status: statusIndividual.statusKyc4 == "empty"
                            ? "waiting"
                            : statusIndividual.statusKyc5,
                      ),
                      ItemSetting(
                        title: "Akun Bank",
                        subtitle: "Akun Bank & Rekening Efek",
                        onTap: () => widget.preview
                            ? openPage(ViewBankUI(
                                submissionId: null,
                                isEdit: isActive(statusIndividual.statusKyc6),
                                status: statusIndividual.statusKyc6,
                              ))
                            : openPage(KycBankUI(
                                submissionId: statusIndividual.statusKyc6 ==
                                        "rejected"
                                    ? "${userData.trader.lastKycSubmissionIdRejected}"
                                    : result["id"].toString(),
                                isEdit: isEditKyc
                                    ? isEditKyc
                                    : isActive(statusIndividual.statusKyc6),
                                status: statusIndividual.statusKyc6,
                              )),
                        status: statusIndividual.statusKyc5 == "empty"
                            ? "waiting"
                            : statusIndividual.statusKyc6,
                      ),
                      // widget.preview
                      //     ? Container()
                      //     :
                      ItemSetting(
                        title: "Syarat & Ketentuan",
                        subtitle: "Syarat & Ketentuan Santara",
                        onTap: () => openPage(KycSyaratKetentuanUI()),
                        status: showTnc()
                            ? userData.trader.tnc == 0
                                ? "tnc_empty"
                                : "verified"
                            : null,
                      ),
                    ],
                  )
            : Center(
                child: CupertinoActivityIndicator(),
              ),
      ),
    );
  }
}

class ItemSetting extends StatelessWidget {
  final String title;
  final String subtitle;
  final String status;
  final VoidCallback onTap;

  ItemSetting({
    @required this.title,
    @required this.subtitle,
    @required this.status,
    this.onTap,
  });

  bool isActive(String status) {
    switch (status) {
      case "verifying":
        return true;
        break;
      case "rejected":
        return true;
        break;
      case "kustodian_rejected":
        return true;
        break;
      case "kustodian_verifying":
        return true;
        break;
      case "verified":
        return true;
        break;
      case "empty":
        return true;
        break;
      case "data_update":
        return false;
        break;
      case "waiting":
        return false;
        break;
      case "tnc_empty":
        return true;
        break;
      default:
        return false;
    }
  }

  Widget messageOption(String status) {
    bool error = false;
    String message = "";
    switch (status) {
      case "verifying":
        error = false;
        message = "Sedang dalam proses verifikasi!";
        break;
      case "rejected":
        error = true;
        message = "Formulir ditolak!";
        break;
      case "kustodian_rejected":
        error = true;
        message = "Formulir ditolak oleh kustodian!";
        break;
      case "kustodian_verifying":
        error = false;
        message = "Formulir sedang diverifikasi oleh kustodian!";
        break;
      case "verified":
        error = false;
        message = "";
        break;
      case "empty":
        error = true;
        message = "Formulir ini masih kosong!";
        break;
      case "waiting":
        error = true;
        message = "";
        break;
      case "tnc_empty":
        error = true;
        message = "Syarat dan Ketentuan Belum Disetujui!";
        break;
      default:
        error = true;
        message = "";
    }
    return message.isEmpty
        ? Container()
        : ItemMessage(
            message: message,
            isError: error,
          );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          contentPadding: EdgeInsets.all(0),
          onTap: () {
            if (isActive(status)) {
              onTap();
            } else {
              if (status == "waiting") {
                PopupHelper.showPopMessage(
                  context,
                  "Pengisian Formulir",
                  "Silahkan lengkapi data formulir yang masih kosong terlebih dahulu",
                  textAlign: TextAlign.center,
                );
              }
            }
          },
          title: Text(
            title,
            style: TextStyle(
              color: isActive(status) ? Colors.white : Color(0xffB8B8B8),
              fontSize: FontSize.s14,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  subtitle,
                  style: TextStyle(
                    color: Color(0xffB8B8B8),
                    fontSize: FontSize.s12,
                  ),
                ),
                messageOption(status),
                Container(
                  height: 5,
                )
              ],
            ),
          ),
          trailing: Icon(
            Icons.keyboard_arrow_right,
            color: isActive(status) ? Colors.white : Color(0xffB8B8B8),
          ),
        ),
        Container(
          height: 0.5,
          color: Color(0xffF4F4F4),
        )
      ],
    );
  }
}

class ItemMessage extends StatelessWidget {
  final String message;
  final bool isError;
  ItemMessage({@required this.message, this.isError = true});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.info,
          color: isError ? Color(0xffBF2D30) : Colors.amber[600],
          size: Sizes.s15,
        ),
        Container(
          width: 5,
        ),
        Text(
          "$message",
          style: TextStyle(
              color: isError ? Color(0xffBF2D30) : Colors.amber[600],
              fontSize: FontSize.s12),
        )
      ],
    );
  }
}
