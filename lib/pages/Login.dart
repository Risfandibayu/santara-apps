import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/User.dart' as user;
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/services/security/pin_service.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/kyc/kyc_pribadi_new/biodata/bloc/biodata.bloc.dart';
import 'package:santaraapp/widget/login/resendemail/resend_email_verification.dart';
import 'package:santaraapp/widget/security_token/pin/PreCreationUI.dart';
import '../models/Login.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Login extends StatefulWidget {
  final int isFromDynamicLink;

  Login({this.isFromDynamicLink});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String firebaseToken;
  final _formKey = GlobalKey<FormState>();
  final _notifKey = GlobalKey<ScaffoldState>();
  final TextEditingController email = TextEditingController();
  final TextEditingController pass = TextEditingController();
  final storage = new FlutterSecureStorage();
  final apiService = ApiService();
  bool loading = false;
  bool password = true;
  int tryGetUser = 0;
  final selfieKtp = InputFieldBloc<File, Object>();
  BioPribadiBloc bloc;

  Future<bool> _isPinExist() async {
    bool ret = false;
    try {
      final _pinService = PinService();
      await _pinService.getExistPIN().then((res) {
        if (res.statusCode == 200) {
          var parsed = json.decode(res.body);
          ret = parsed["message"];
        } else {
          ret = false;
        }
      });
    } catch (e) {
      return false;
    }
    return ret;
  }

  Future sendTokenFirebase(String token) async {
    try {
      await updateTokenFCM();
      // Check apakah user sudah memiliki PIN atau belum
      _isPinExist().then((value) {
        setState(() {
          loading = false;
        });
        // jika sudah memiliki pin, langsung push ke page account
        if (value) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => Home(index: 4)),
              (Route<dynamic> route) => false);
        } else {
          // jika belum pastikan user mendaftarkan pin terlebih dahulu
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => PreCreationUI(
                onCreate: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => Home(index: 4)),
                      (Route<dynamic> route) => false);
                },
              ),
            ),
          );
        }
      });
      // end check
    } on SocketException catch (_) {
      _serverValidation('Proses mengalami kendala, harap coba kembali');
    }
  }

  Future getUserByUuid(String uuid, String token) async {
    final response = await http.get('$apiLocal/users/by-uuid/$uuid',
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      var data = user.userFromJson(response.body);
      storage.write(key: 'user', value: response.body);
      storage.write(key: 'phone', value: data.phone);
      Helper.username = data.trader == null ? "" : data.trader.name;
      Helper.email = data.email;
      Helper.userId = data.id;
      Helper.check = true;
      sendTokenFirebase(token);
    } else {
      if (tryGetUser < 3) {
        getUserByUuid(uuid, token);
        setState(() => tryGetUser += 1);
      } else {
        setState(() => loading = false);
        storage.delete(key: 'token');
        storage.delete(key: 'refreshToken');
        storage.delete(key: 'uuid');
        storage.delete(key: 'userId');
        Helper.refreshToken = null;
        // print(">> Wowowow");
        _validation();
      }
    }
  }

  Future<dynamic> login(String email, String pass) async {
    setState(() {
      loading = true;
    });
    try {
      final headers = await UserAgent.headersNoAuth();
      final response = await http
          .post('$apiLocal/auth/login',
              // 'https://inspiraspace.online:3601/auth/login',
              body: {"email": email, "password": pass},
              headers: headers)
          .timeout(Duration(seconds: 30));

      
      if (response.statusCode == 200) {
        try {
          final data = loginFromJson(response.body);
          storage.write(key: 'token', value: data.token.token);
          storage.write(key: 'refreshToken', value: data.token.refreshToken);
          storage.write(key: 'uuid', value: data.user.uuid);
          storage.write(key: 'userId', value: data.user.id.toString());
          storage.write(
              key: 'isResetPassword',
              value: data.user.isResetPassword.toString());
          storage.write(
              key: 'isVerifiedKyc', value: data.user.isVerifiedKyc.toString());
          Helper.refreshToken = data.token.refreshToken;
          Helper.reqResetPassword = 0;
          getUserByUuid(data.user.uuid, data.token.token);
        } catch (e, stack) {
          santaraLog(e, stack);
        }
      } else if (response.statusCode == 401) {
        setState(() {
          loading = false;
        });
        _validation();
      } else if (response.statusCode == 500) {
        final msg = jsonDecode(response.body);
        setState(() {
          loading = false;
        });
        _serverValidation(msg['message']);
      } else if (response.statusCode == 400) {
        final msg = jsonDecode(response.body);
        setState(() {
          loading = false;
        });
        _validationError(msg['message']);
      } else {
        final msg = jsonDecode(response.body);
        setState(() {
          loading = false;
        });
        _validationError(msg['message']);
      }
    } on TimeoutException catch (_) {
      setState(() {
        loading = false;
      });
      _serverValidation("Request Timed Out");
    } catch (e, stack) {
      santaraLog(e, stack);
      setState(() {
        loading = false;
      });
      _serverValidation("Proses mengalami kendala, harap coba kembali");
    }
    FirebaseAnalytics().logEvent(name: 'app_login', parameters: null);
  }

  void _validation() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
                'Email atau password anda salah, silahkan periksa kembali'),
            actions: <Widget>[
              FlatButton(
                key: Key('closeWrongPasswordButton'),
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _validationError(String errorMsg) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(errorMsg),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  if (errorMsg == "Anda belum melakukan verifikasi email") {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResendEmailVerification()));
                  } else {
                    Navigator.pop(context);
                  }
                },
              )
            ],
          );
        });
  }

  void _serverValidation(String msg) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(msg),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  Future updateTokenFCM() async {
    firebaseToken = await storage.read(key: "tokenFirebase");
    await apiService.updateFirebaseToken(firebaseToken);
    // debugPrint("Firebase Token : " + firebaseToken);
  }

  @override
  void initState() {
    super.initState();
    FirebaseAnalytics().logEvent(name: 'app_view_login_page', parameters: null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back, color: Colors.black),
          ),
        ),
        backgroundColor: Color(ColorRev.mainBlack),
        body: ModalProgressHUD(
            inAsyncCall: loading,
            progressIndicator: CircularProgressIndicator(),
            opacity: 0.5,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Stack(
                children: <Widget>[
                  ListView(children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              children: [
                                Image.asset(
                                  'assets/santara/Logo_Santara.png',
                                  height: MediaQuery.of(context).size.width / 3,
                                ),
                                SizedBox(height: 20),
                                Text(
                                  'Masuk ke Akun Santara',
                                  style: TextStyle(
                                      color: Color(ColorRev.mainwhite),
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: Container(
                                  child: Column(children: <Widget>[
                                    //username
                                    Form(
                                      key: _formKey,
                                      child: Column(
                                        children: <Widget>[
                                          widget.isFromDynamicLink == 1
                                              ? Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      30, 0, 30, 10),
                                                  padding: EdgeInsets.all(12),
                                                  decoration: BoxDecoration(
                                                    color: Color(0xFF5FE0B2),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                        "Selamat, email anda berhasil diverifikasi"),
                                                  ),
                                                )
                                              : Container(),
                                          //email
                                          Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: TextFormField(
                                                key: Key("emailField"),
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                controller: email,
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return 'Masukkan email anda';
                                                  } else if (!RegExp(
                                                          r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                                                      .hasMatch(value)) {
                                                    return 'Email anda tidak valid, coba lagi';
                                                  } else {
                                                    return null;
                                                  }
                                                },
                                                autofocus: true,
                                                decoration: InputDecoration(
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  prefixIcon:
                                                      Icon(Icons.mail_outline),
                                                  hintText: 'Email',
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: Colors.grey,
                                                        width: 5.0),
                                                  ),
                                                  errorMaxLines: 5,
                                                )),
                                          ),

                                          //password
                                          Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 10),
                                            child: TextFormField(
                                                key: Key("passwordField"),
                                                obscureText: password,
                                                controller: pass,
                                                validator: (value) =>
                                                    value.isEmpty
                                                        ? 'Masukkan password'
                                                        : null,
                                                keyboardType:
                                                    TextInputType.text,
                                                autofocus: false,
                                                decoration: InputDecoration(
                                                    fillColor: Colors.white,
                                                    filled: true,
                                                    suffixIcon: IconButton(
                                                        icon: Icon(password
                                                            ? Icons
                                                                .visibility_off
                                                            : Icons.visibility),
                                                        onPressed: () {
                                                          setState(() {
                                                            password
                                                                ? password =
                                                                    false
                                                                : password =
                                                                    true;
                                                          });
                                                        }),
                                                    prefixIcon:
                                                        Icon(Icons.lock_open),
                                                    hintText: 'Password',
                                                    border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: Colors.grey,
                                                          width: 5.0),
                                                    ),
                                                    errorMaxLines: 5)),
                                          ),
                                          //lupa password
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 15),
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  GestureDetector(
                                                    child: Text(
                                                      'Lupa Kata Sandi ?',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 13.0,
                                                          color: Color(ColorRev
                                                              .mainwhite)),
                                                    ),
                                                    onTap: () {
                                                      Navigator.pushNamed(
                                                          context, '/forgot');
                                                    },
                                                  ),
                                                ]),
                                          ),

                                          //button
                                          Container(
                                            key: _notifKey,
                                            padding: EdgeInsets.fromLTRB(
                                                20.0, 20.0, 20.0, 20.0),
                                            child: InkWell(
                                              key: Key('loggingIn'),
                                              onTap: () {
                                                if (_formKey.currentState
                                                    .validate()) {
                                                  login(email.text, pass.text);
                                                }
                                              },
                                              child: Container(
                                                height: 48.0,
                                                decoration: BoxDecoration(
                                                  color: Color(0xFFBF2D30),
                                                  border: Border.all(
                                                      color: Colors.transparent,
                                                      width: 2.0),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          4.0),
                                                ),
                                                child: Center(
                                                    child: Text('Masuk',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white))),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ]),
                                ))
                          ]),
                    )
                  ]),
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Platform.isIOS
                        ? IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () => Navigator.pop(context),
                          )
                        : Container(),
                  )
                ],
              ),
            )));
  }
}
