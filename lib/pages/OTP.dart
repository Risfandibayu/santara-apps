import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/services/time/time_services.dart';
import 'package:santaraapp/utils/api.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:santaraapp/helpers/SlideTransitionNavigation.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:shimmer/shimmer.dart';

class OTP extends StatefulWidget {
  final String phone;
  OTP({this.phone});
  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> with WidgetsBindingObserver {
  TextEditingController cNo1 = new TextEditingController();
  TextEditingController cNo2 = new TextEditingController();
  TextEditingController cNo3 = new TextEditingController();
  TextEditingController cNo4 = new TextEditingController();
  final FocusNode _focus1 = FocusNode();
  final FocusNode _focus2 = FocusNode();
  final FocusNode _focus3 = FocusNode();
  final FocusNode _focus4 = FocusNode();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final storage = new FlutterSecureStorage();
  final timeService = TimeService();
  bool loading = false;
  var nomorHp = "";
  var token;
  var datas;
  var refreshToken;
  int secs;

  Future logout() async {
    if (token != null) {
      final headers = await UserAgent.headers();
      final id = datas.id;
      await http
          .post('$apiLocal/auth/logout',
              body: {
                "refreshToken": "$refreshToken",
                "user_id": "$id",
              },
              headers: headers)
          .timeout(Duration(seconds: 20));
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'AddressCyro');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      storage.delete(key: 'hasSubmitJob');
      storage.delete(key: 'hasSubmitPriority');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  Future sendOtp(int no1, int no2, int no3, int no4) async {
    setState(() {
      loading = true;
    });
    var url = "$apiLocal/otp/verify-otp/";
    final code = "$no1$no2$no3$no4";
    try {
      final headers = await UserAgent.headers();
      final http.Response response =
          await http.post(url, headers: headers, body: {"code": code});
      if (response.statusCode == 200) {
        setState(() {
          loading = false;
        });
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                title: Text("Verifikasi OTP telah berhasil"),
                actions: <Widget>[
                  FlatButton(
                    color: Color(0xFF0E7E4A),
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          SlideTransitionNavigation(enterPage: Home(index: 4)),
                          (Route<dynamic> route) => false);
                      // logout();
                    },
                    child: Text(
                      "ok",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              );
            });
      } else if (response.statusCode == 400) {
        setState(() {
          loading = false;
        });
        final data = jsonDecode(response.body);
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
            data["message"],
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 1),
        ));
      } else {
        setState(() {
          loading = false;
        });
        final data = jsonDecode(response.body);
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
            data["message"],
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 1),
        ));
      }
    } on SocketException catch (_) {
      setState(() => loading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  Future getNewOTP() async {
    try {
      final http.Response response = await http.get("$apiLocal/otp/",
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      if (response.statusCode == 200) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (_) => OTP(
                      phone: widget.phone,
                    )));
        // _scaffoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text(
        //     "Kode OTP berhasil dikirimkan",
        //     style: TextStyle(color: Colors.white),
        //   ),
        //   backgroundColor: Colors.black,
        //   duration: Duration(seconds: 2),
        // ));
      } else if (response.statusCode == 500) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
            "Kode OTP hanya dikirim setiap 10 menit sekali",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 2),
        ));
      } else {
        final data = jsonDecode(response.body);
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
            data["message"],
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 2),
        ));
      }
    } on SocketException catch (e) {
      throw (e.message);
    }
  }

  getLocalStorage() async {
    token = await storage.read(key: "token");
    datas = userFromJson(await storage.read(key: 'user'));
    refreshToken = await storage.read(key: "refreshToken");
    await getNewOTP();
  }

  getSecondsTime() async {
    await timeService.otpExpiredTimeInSecond(true).then((value) {
      setState(() {
        secs = value;
      });
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    getLocalStorage();
    getSecondsTime();
    cNo1.text = "";
    cNo2.text = "";
    cNo3.text = "";
    cNo4.text = "";
    if (widget.phone != null) {
      int length = widget.phone.length;
      var star = "";
      for (var i = 0; i < length - 3; i++) {
        star += "*";
      }
      // var depan = widget.phone.replaceRange(0, length - 3, "*");
      nomorHp = widget.phone.replaceRange(0, length - 3, star);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    getSecondsTime();
  }

  void showHoldBackButton(String msg) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Peringatan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Container(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            height: 40,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Colors.grey[700]),
                                borderRadius: BorderRadius.circular(6)),
                            child: Center(
                                child: Text("Batal",
                                    style: TextStyle(color: Colors.grey[700]))),
                          ),
                        ),
                      ),
                      Container(width: 12),
                      Expanded(
                        child: InkWell(
                          onTap: () async {
                            await logout().then((_) {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(builder: (_) => Home()),
                                  (Route<dynamic> route) => false);
                            });
                          },
                          child: Container(
                            height: 40,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                                color: Color(0xFFBF2D30),
                                borderRadius: BorderRadius.circular(6)),
                            child: Center(
                                child: Text("Keluar",
                                    style: TextStyle(color: Colors.white))),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(height: 20),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        showHoldBackButton("Pendaftaran anda belum selesai, ingin keluar?");
      },
      child: Scaffold(
          key: _scaffoldKey,
          body: Container(
            child: Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('Verification.png', fit: BoxFit.contain),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Text("Masukkan kode verifikasi",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20)),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 20),
                          child: Text(
                            "Kode verifikasi telah dikirim melalui SMS ke nomor $nomorHp",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 17),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 10),
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  controller: cNo1,
                                  focusNode: _focus1,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (str) {
                                    if (str.length == 1) {
                                      setState(() {
                                        _focus1.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus2);
                                      });
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 10),
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  controller: cNo2,
                                  focusNode: _focus2,
                                  decoration: InputDecoration(
                                    focusColor: Colors.black,
                                  ),
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (str) {
                                    if (str.length == 1) {
                                      setState(() {
                                        _focus2.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus3);
                                      });
                                    } else if (str.length == 0) {
                                      setState(() {
                                        _focus2.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus1);
                                      });
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 10),
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  controller: cNo3,
                                  focusNode: _focus3,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (str) {
                                    if (str.length == 1) {
                                      setState(() {
                                        _focus3.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus4);
                                      });
                                    } else if (str.length == 0) {
                                      setState(() {
                                        _focus3.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus2);
                                      });
                                    }
                                  },
                                ),
                              ),
                            ),
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 10),
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  controller: cNo4,
                                  focusNode: _focus4,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (str) {
                                    if (str.length == 1) {
                                      setState(() {
                                        _focus4.unfocus();
                                        // FocusScope.of(context).requestFocus(_focus4);
                                      });
                                    } else if (str.length == 0) {
                                      setState(() {
                                        _focus4.unfocus();
                                        FocusScope.of(context)
                                            .requestFocus(_focus3);
                                      });
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        secs == null
                            ? Container(
                                padding: EdgeInsets.fromLTRB(0, 16, 0, 30),
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.white,
                                  child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    child: Container(
                                      height: 30,
                                      width: 100,
                                      child: Image.asset(
                                        'arfa.png',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                child: Countdown(
                                  duration: Duration(seconds: secs),
                                  builder: (BuildContext context,
                                      Duration remaining) {
                                    if (remaining.inSeconds <= 0) {
                                      // getNewOTP();
                                      return Text('00:00');
                                    } else {
                                      String second =
                                          (remaining.inSeconds % 60).toString();
                                      String minute =
                                          (remaining.inMinutes).toString();
                                      if (second.length == 1) {
                                        second = "0" + second;
                                      }
                                      if (minute.length == 1) {
                                        minute = "0" + minute;
                                      }
                                      return Text('$minute:$second');
                                    }
                                  },
                                ),
                              ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 20),
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            color: Color(0xFF1BC47D),
                            disabledColor: Colors.grey,
                            onPressed: cNo1.text == "" ||
                                    cNo2.text == "" ||
                                    cNo3.text == "" ||
                                    cNo4.text == ""
                                ? null
                                : () {
                                    // Navigator.of(context).pushAndRemoveUntil(SlideTransitionNavigation(enterPage: Home(index: 3)), (Route<dynamic> route)=>false);
                                    sendOtp(
                                        int.parse(cNo1.text),
                                        int.parse(cNo2.text),
                                        int.parse(cNo3.text),
                                        int.parse(cNo4.text));
                                  },
                            child: loading == false
                                ? Text(
                                    "Verifikasi",
                                    style: TextStyle(color: Colors.white),
                                  )
                                : CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              "Anda tidak menerima SMS ?",
                              style: TextStyle(fontSize: 14),
                            ),
                            InkWell(
                              onTap: () {
                                getNewOTP();
                              },
                              child: Text("Kirim ulang SMS",
                                  style: TextStyle(
                                      color: Color(0xFF1E9FF2), fontSize: 14)),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        logout().then((_) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(builder: (_) => Home()),
                              (Route<dynamic> route) => false);
                        });
                      },
                    ))
              ],
            ),
          )),
    );
  }
}
