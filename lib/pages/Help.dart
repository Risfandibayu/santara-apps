import 'package:flutter/material.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';

//components
import '../widget/help/FormQuestion.dart';
import '../widget/help/ListQuestions.dart';

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        appBar: CustomAppbar(drawerKey: _drawerKey),
        drawer: CustomDrawer(),
        body: Stack(
          children: <Widget>[
            DefaultTabController(
              length: 2,
              child: Scaffold(
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(50),
                  child: Container(
                    child: SafeArea(
                      child: TabBar(
                          indicatorColor: Color((0xFFBF2D30)),
                          labelColor: Color((0xFFBF2D30)),
                          unselectedLabelColor: Colors.black,
                          tabs: [
                            Container(
                                height: 50,
                                child: Center(
                                    child: Text("Pengajuan Pertanyaan"))),
                            Container(
                                height: 50,
                                child:
                                    Center(child: Text("Daftar Pertanyaan"))),
                          ]),
                    ),
                  ),
                ),
                body: TabBarView(
                  children: <Widget>[FormQuestion(), ListQuestions()],
                ),
              ),
            ),
            KycNotificationStatus(
              showCloseButton: false,
            )
          ],
        ));
  }
}
