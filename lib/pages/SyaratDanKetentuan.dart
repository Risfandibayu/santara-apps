import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/SlideTransitionNavigation.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class SyaratDanKetentuan extends StatefulWidget {
  @override
  _SyaratDanKetentuanState createState() => _SyaratDanKetentuanState();
}

class _SyaratDanKetentuanState extends State<SyaratDanKetentuan> {
  var isLoading = false;
  var isCheck = false;
  final storage = new FlutterSecureStorage();
  var token;

  Future sendData() async {
    setState(() {
      isLoading = true;
    });
    try {
      //sending
      final uri = Uri.parse('$apiLocal/traders/submit');
      var request = new http.MultipartRequest("POST", uri);
      request.headers[HttpHeaders.authorizationHeader] = 'Bearer $token';

      //adding items
      request.fields['submit'] = '1';

      var response = await request.send();
      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
        });
        Future.delayed(Duration(seconds: 1), () {
          return Navigator.of(context)
              .push(SlideTransitionNavigation(enterPage: Home()));
        });
      } else {
        setState(() {
          isLoading = false;
        });
        final respStr = await response.stream.bytesToString();
        var msg;
        if (respStr.contains("message")) {
          final data = jsonDecode(respStr);
          msg = data["message"];
        } else {
          msg = respStr;
        }

        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
            msg,
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 1),
        ));
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    } catch (e) {
      // print(e);
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return // WillPopScope(
        // onWillPop: () => Navigator.pushAndRemoveUntil(
        //     context,
        //     MaterialPageRoute(builder: (_) => Kyc3()),
        //     (Route<dynamic> route) => false),
        // child:
        Scaffold(
            appBar: AppBar(
              elevation: 1.0,
              backgroundColor: Colors.white,
              title: Image.asset(
                'assets/santara/1.png',
                width: 110,
              ),
              centerTitle: true,
              // leading: IconButton(
              //   icon: Icon(Icons.arrow_back),
              //   onPressed: () => Navigator.pushAndRemoveUntil(
              //       context,
              //       MaterialPageRoute(builder: (_) => Kyc3()),
              //       (Route<dynamic> route) => false)
              // ),
              iconTheme: IconThemeData(color: Colors.black),
            ),
            // bottomNavigationBar: BottomAppBar(
            //   child: Container(
            //       width: MediaQuery.of(context).size.width,
            //       height: 50,
            //       decoration: BoxDecoration(
            //         border: Border(top: BorderSide(color: Color(0xFFE5E5E5)))
            //       ),
            //       child: RaisedButton(
            //         disabledColor: Color(0xFFF3F3F3),
            //         color: Color(0xFFBF2D30),
            //         onPressed: isCheck ? () => sendData() : null,
            //         child: Text('Setuju', style: TextStyle(color: isCheck ? Colors.white : Colors.black),),
            //       ),
            //     ),
            // ),
            body: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 6),
                  child: Text(
                    "Syarat dan Ketentuan",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                    padding: EdgeInsets.only(right: 4),
                    height: MediaQuery.of(context).size.height / 2,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Color(0xFFF2F2F2),
                        border: Border.all(color: Color(0xFFDFDFDF)),
                        borderRadius: BorderRadius.circular(12)),
                    child: ScrollConfiguration(
                        behavior: ScrollBehavior(),
                        child: Scrollbar(
                          child: ListView(
                            padding: EdgeInsets.all(16),
                            scrollDirection: Axis.vertical,
                            children: <Widget>[
                              //definisi
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("DEFINISI",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "1. Program Investasi adalah suatu program layanan urun dana yang diselenggarakan oleh Penyelenggara dengan melakukan penawaran saham milik Penerbit kepada Pemodal atau masyarakat umum melalui jaringan sistem elektronik layanan urun dana  melalui penawaran saham berbasis teknologi informasi (equity crowdfunding) milik Penyelenggara yang bersifat terbuka sebagaimana diatur dalam Peraturan  Otoritas Jasa Keuangan Nomor : 57/POJK.04/2020.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Penyelenggara adalah PT Santara Daya Inspiratama, merupakan pihak yang menyediakan, mengelola, dan mengoperasikan Program Investasi.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Pengguna adalah Penerbit dan Pemodal.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Penerbit adalah badan usaha berbentuk perseroan terbatas, merupakan pihak yang melaksanakan Program Investasi menawarkan saham berbasis teknologi melalui Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "5. Pemodal adalah pihak yang melakukan pembelian saham Penerbit melalui Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "6. Saham adalah nilai lembar saham milik Penerbit selama Program Investasi berlangsung.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "7. Buyback adalah proses dimana Penerbit melakukan pembelian kembali Saham yang telah dijual oleh Penerbit kepada Pemodal.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "8. Hari Kalender adalah hari Senin sampai dengan hari Minggu, termasuk hari libur Nasional yang ditetapkan Pemerintah Indonesia sebagaimana perhitungan tahun kalender Masehi.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "9. Hari Kerja adalah hari kerja dimana Penyelenggara beroperasi.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //pelaksanaan program investasi
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("PELAKSANAAN PROGRAM INVESTASI",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "Pemodal dengan ini mengajukan pendaftaran kepada Penyelenggara dan Penyelenggara menerima pendaftaran dari Pemodal sebagai Anggota dalam rangka untuk melaksanakan pembelian Saham milik Penerbit melalui program jasa Layanan Urun Dana melalui penawaran saham berbasis teknologi dengan maksud dan tujuan, lingkup pekerjaan kerja sama, syarat dan ketentuan, serta batas tanggung jawab pada Peraturan Otoritas Jasa Keuangan Nomor : 57/POJK.04/2020 beserta perubahan-perubahannya.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //Masa penawaran saham
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "MASA PENAWARAN SAHAM",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Dalam  hal ini Pemodal melakukan pembelian Saham Penerbit selama masa penawaran Saham oleh Penerbit yang dilakukan paling lama 60 (enam puluh) Hari Kalender..",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Pemodal mengerti dan memahami bahwa Penerbit dapat membatalkan penawaran saham melalui Program Investasi sebelum berakhirnya masa penawaran saham dengan membayar sejumlah denda kepada Pemodal dan Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //pembelian saham
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "PEMBELIAN SAHAM",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "Pembelian saham oleh Pemodal dalam penawaran saham melalui Program Investasi dilakukan dengan menyetorkan sejumlah dana pada escrow account.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //penyerahan dana saham
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("PENYERAHAN DANA SAHAM",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Pemodal mengerti dan  memahami bahwa Penyelenggara wajib menyerahkan dana dari Pemodal kepada Penerbit paling lambat 21 (dua puluh satu) Hari Kerja setelah berakhirnya masa penawaran saham.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Manfaat bersih dari penempatan dana dikembalikan kepada Pemodal secara proporsional.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Berakhir masa penawaran adalah : \n a. Tanggal tertentu yang telah ditetapkan dan disepakati oleh Para Pihak; atau \n b. tanggal tertentu berakhirnya masa penawaran saham namun seluruh saham yang ditawarkan melalui Program Investasi telah dibeli oleh Pemodal.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Pemodal mengerti dan memahami bahwa Penerbit wajib menyerahkan saham kepada Penyelenggara untuk didistribusikan kepada Pemodal paling lambat 5 (lima) Hari Kerja setelah Penerbit menerima dana Pemodal dari Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "5. Pemodal mengerti dan memahami bahwa Penyelenggara wajib mendistribusikan saham kepada Pemodal paling lambat 10 (sepuluh) Hari Kerja setelah menerima saham dari Penerbit.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "6. Pendistribusian saham kepada Pemodal oleh Penyelenggara dapat dilakukan secara elektronik melalui penitipan kolektif pada kustodian atau pendistribusian secara fisik melalui pengiriman sertifikat saham.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "7. Pemodal mengerti dan memahami bahwa dalam hal penawaran saham batal demi hukum, maka Penyelenggara wajib mengembalikan dana beserta seluruh manfaat yang timbul dari dana tersebut selama dalam escrow account secara proporsional kepada Pemodal paling lambat 2 (dua) Hari Kerja setelah penawaran saham dinyatakan batal demi hukum.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //daftar pemegang saham
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "DAFTAR PEMEGANG SAHAM",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "Pemodal mengerti dan memahami bahwa Penerbit wajib mencatatkan kepemilikan saham Pemodal dalam daftar pemegang saham.",
                                    textAlign: TextAlign.justify),
                              ),
                              //penghimpunan dana
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "PENGHIMPUNAN DANA",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Pemodal mengerti dan memahami bahwa pembagian bagi hasil kepada para pemegang unit bagi hasil tidak bersifat lifetime sehingga Penerbit dapat melakukan opsi Buyback, dimana opsi Buyback baru dapat dilakukan paling cepat setelah pembagian bagi hasil 6 (enam) bulan pertama.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Pemodal mengerti dan memahami bahwa sesuai dengan prinsip bagi hasil, maka bagi hasil dibagikan ketika Penerbit mengalami profit yang dilaporkan dalam laporan keuangan setiap periode yang disepakati.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Pemodal mengerti dan memahami bahwa apabila terdapat beban operasional usaha yang harus dikeluarkan setiap periode tertentu, Penerbit  tidak memiliki hak untuk membebankannya kepada Pemodal, melainkan beban tersebut dimasukkan ke dalam penghitungan biaya operasional yang kemudian dilaporkan dalam laporan keuangan periode tersebut.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //kewajiban pemodal
                              Padding(
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    "KEWAJIBAN PEMODAL",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  )),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Penerbit wajib menjaga nama baik dan reputasi Penyelenggara dengan tidak melakukan promosi-promosi yang mengandung unsur suku, agama dan ras, atau tidak melakukan penyebaran informasi yang tidak benar dengan mengatasnamakan Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Pemodal wajib tunduk dan patuh pada ketentuan Perjanjian dan terms and conditions yang tercantum dalam website Penyelenggara serta tunduk dan patuh pada  Peraturan Otoritas Jasa Keuangan Nomor : 57/POJK.04/2020 beserta perubahan-perubahannya.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Pemodal wajib setuju dan sepakat bersedia untuk memberikan akses audit internal maupun audit eksternal yang ditunjuk Penyelenggara serta audit Otoritas Jasa Keuangan (OJK) atau regulator berwenang lainnya setiap kali dibutuhkan terkait pelaksanaan kerja sama ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //hak pemodal
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "HAK PEMODAL",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Pemodal berhak untuk melakukan pembelian Saham yang ditawarkan Penerbit melalui Program Investasi yang diselenggarakan Penyelenggara.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Pemodal berhak mendapatkan bantuan komunikasi penyebaran konten lewat channel resmi dan jaringan audience yang dimiliki Para Pihak.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //kewajiban penyelenggara
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("KEWAJIBAN PENYELENGGARA",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "1. Penyelenggara wajib memenuhi seluruh hak-hak Pemodal",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Penyelenggara memonitor, menganalisa, dan memastikan bahwa Penerbit  berada di jalur yang sesuai dengan visi misi Penyelenggara dan Program Investasi.",
                                    textAlign: TextAlign.justify),
                              ),
                              //hak penyelenggara
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("HAK PENYELENGGARA",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "1. Pemodal mengerti dan memahami bahwa Penyelenggara berhak menyetujui atau menolak permintaan tertulis dari Penerbit  dalam penggunaan dana hasil penjualan unit bagi hasil, jika dirasa tidak sesuai dengan tujuan program pemberdayaan UMKM dari Penyelenggara.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Pemodal mengerti dan memahami bahwa Penyelenggara berhak menghentikan seluruh partisipasi Penerbit dari Program Investasi jika Penerbit  dinilai tidak kooperatif, mencemarkan nama baik Penyelenggara atau diindikasi melakukan aktivitas yang melanggar hukum.",
                                    textAlign: TextAlign.justify),
                              ),
                              //perpajakan
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("PERPAJAKAN",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "Pembebanan pajak yang timbul dalam Perjanjian ini menjadi beban masing-masing pihak serta tunduk pada ketentuan hukum perpajakkan yang berlaku di wilayah Negara Republik Indonesia.",
                                    textAlign: TextAlign.justify),
                              ),
                              //hak atas kekayaan intelektual
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "HAK ATAS KEKAYAAN INTELEKTUAL",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Hak atas Kekayaan Intelektual yang timbul atas pelaksanaan Program Investasi dan izin Penyelenggara,  beserta fasilitas-fasilitas lain yang dimiliki Penyelenggara dan digunakan dalam kerja sama ini adalah tetap dan seterusnya milik Penyelenggara dan tidak ada penyerahan hak dari Penyelenggara kepada Pemodal dalam kerja sama ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Pemodal tidak berhak untuk mengubah, mengembangkan, membagikan dan/atau menjual baik seluruh maupun sebagian hak atas kekayaan intelektual yang timbul atas pengembangan, inovasi, perubahan berupa fitur dan/atau fungsi terhadap sistem teknologi informasi.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Penyelenggara dengan ini menjamin bahwa hak atas kekayaan intelektual yang terkandung dalam pelaksanaan Program Investasi ini tidak melanggar hak atas kekayaan intelektual milik pihak manapun, dan Penyelenggara membebaskan Pemodal dari segala tuntutan, gugatan dari pihak manapun, sehubungan dengan pelanggaran terhadap hak atas kekayaan intelektual yang terkandung dalam Program Investasi sesuai dengan terms and conditions ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //jangka waktu dan pengakhiran
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "JANGKA WAKTU DAN PENGAKHIRAN",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Jangka waktu kerja sama antara Penyelenggara dan Pemodal ini berlaku selama Penerbit turut serta dalam Program Investasi.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Kerja sama ini berakhir dengan sendirinya, dalam hal : \n a. Jangka waktu kerja sama Penyelenggara dengan Penerbit dalam Program Investasi berakhir; \n b. Penerbit mengajukan Buyback atas semua Saham yang dimiliki Pemodal;",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "3. Penyelenggara berhak untuk mengubah, membuat perjanjian baru atau menghentikan kerja sama ini sebelum berakhirnya Perjanjian atas pertimbangan Penyelenggara sendiri.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Dalam hal kerja sama ini berakhir dan/atau dinyatakan berakhir, maka Para Pihak sepakat bahwa ketentuan Informasi Rahasia sebagaimana diatur dalam Perjanjian ini tetap berlaku dan mengikat Para Pihak hingga kapanpun meskipun kerja sama telah berakhir.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "5. Pengakhiran/pembatalan kerja sama ini tidak menghapuskan kewajiban-kewajiban masing-masing Pihak yang telah atau akan timbul dan belum dilaksanakan pada saat berakhirnya kerja sama ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "6. Dalam hal pengakhiran/pembatalan kerja sama ini, Para Pihak sepakat untuk mengesampingkan keberlakuan ketentuan Pasal 1266 Kitab Undang-Undang Hukum Perdata, sepanjang ketentuan tersebut mensyaratkan adanya suatu putusan atau penetapan pengadilan untuk menghentikan atau mengakhiri suatu perjanjian, sehingga pengakhiran/pembatalan kerja sama ini cukup dilakukan dengan pemberitahuan tertulis dari salah satu Pihak.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //informasi rahasia
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "INFORMASI RAHASIA",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Salah satu Pihak (selanjutnya disebut “Pihak Pemberi”) dapat memberikan Informasi Rahasia kepada Pihak lainnya (selanjutnya disebut “Pihak Penerima”) dalam melaksanakan kerja sama ini. Para Pihak sepakat bahwa pemberian, penerimaan dan penggunaan Informasi Rahasia tersebut dilakukan sesuai dengan terms and conditions ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Informasi Rahasia yang dimaksud dalam ketentuan ini berarti informasi yang bersifat non-publik, termasuk namun tidak terbatas pada skema atau gambar produk, penjelasan material, spesifikasi, penjualan dan informasi mengenai klien, kebijaksanaan dan praktek bisnis Pihak Pemberi dan informasi mana dapat dimuat dalam media cetak, tulis, disk / tape / compact disk komputer atau media lainnya yang sesuai.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Tidak termasuk sebagai Informasi Rahasia adalah materi atau informasi yang mana dapat dibuktikan oleh Pihak Penerima bahwa: \n a. Pada saat penerimaannya sebagai milik publik (public domain) atau menjadi milik publik (public domain) atau menjadi milik publik (public domain) tanpa adanya pelanggaran oleh Pihak Penerima; \n b. Telah diketahui oleh Pihak Penerima pada saat diberikan oleh Pihak Pemberi; \n c. Telah didapatkan dari pihak ketiga tanpa adanya pelanggaran terhadap pengungkapan Informasi Rahasia; \n d. Dikembangkan sendiri oleh Pihak Penerima.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Pihak Penerima dengan ini menyatakan bahwa tidak akan mengungkapkan Informasi Rahasia apapun yang diberikan Pihak Pemberi ke pihak lainnya selain daripada yang diperlukan dalam melaksanakan tugas, peran dan kewajibannya dalam Perjanjian ini, tanpa terlebih dahulu memperoleh persetujuan dari Pihak Pemberi dan Pihak Penerima akan melakukan semua tindakan-tindakan pencegahan yang wajar untuk mencegah terjadinya pelanggaran atau kelalaian dalam pengungkapan, penggunaan, pembuatan salinan (copy) atau pengalihan Informasi Rahasia tersebut.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "5. Masing-masing Pihak berkewajiban untuk menyimpan segala rahasia data atau sistem yang diketahuinya baik secara langsung maupun tidak langsung sehubungan Pekerjaan yang dilaksanakan sesuai dengan Perjanjian dan bertanggung jawab atas segala kerugian yang diakibatkan karena pembocoran Informasi Rahasia tersebut, baik oleh masing-masing Pihak maupun karyawannya maupun perwakilannya.",
                                    textAlign: TextAlign.justify),
                              ),
                              //pernyataan dan jaminan
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "PERNYATAAN DAN JAMINAN",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Tanpa mengesampingkan pernyataan dan jaminan yang diberikan oleh salah satu Pihak ke Pihak lainnya dalam Program Investasi ini, masing-masing Pihak dengan ini menyatakan dan menjamin Pihak lainnya dalam kerja sama hal-hal sebagai berikut: \n a. Bahwa berdasarkan hukum negara Republik Indonesia, masing-masing Pihak cakap menurut hukum untuk memiliki harta kekayaan dan melakukan perbuatan hukum usahanya di wilayah Republik Indonesia serta memiliki segala perizinan yang diperlukan untuk menjalankan perbuatan hukum.; \n b. Bahwa masing-masing Pihak telah mengambil segala tindakan yang diperlukan untuk memastikan wakil dari masing-masing Pihak dalam kerja sama telah memiliki kuasa dan wewenang penuh untuk mengikatkan diri dalam persetujuan terms and conditions ini; \n c. Bahwa masing-masing Pihak telah memastikan bahwa kerja sama ini tidak melanggar ketentuan dari anggaran dasar masing-masing Pihak dalam kerja sama dan tidak bertentangan dengan perjanjian apapun yang dibuat oleh masing-masing Pihak dengan pihak ketiga. \n d. Pelaksanaan ketentuan-ketentuan dalam Program Investasi ini dilaksanakan secara profesional dengan penuh tanggung jawab dan atas dasar hubungan yang saling menguntungkan. \n e. Persetujuan  terms and conditions ini tidak bertentangan atau melanggar atau berbenturan dengan kaidah-kaidah hukum dan peraturan perundang-undangan serta kebijakan-kebijakan pemerintah Republik Indonesia atau pihak yang berwenang lainnya; \n f. Bersedia untuk menerapkan, mendukung dan mematuhi ketentuan hukum dan peraturan perundang-undangan yang berlaku di Republik Indonesia termasuk namun tidak terbatas pada peraturan mengenai tindak pidana korupsi, anti pencucian uang dan anti penyuapan; \n g. Masing-masing Pihak akan melaksanakan Program Investasi ini dengan itikad baik dan secara jujur. Tidak satupun ketentuan dalam kerja sama ini akan digunakan oleh salah satu Pihak untuk mengambil keuntungan secara tidak wajar dan mengakibatkan kerugian bagi Pihak lainnya dan tidak satupun ketentuan dalam Program Investasi ini dimaksudkan untuk memberikan keuntungan secara tidak wajar kepada Pihak lainnya.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Tanpa mengesampingkan pernyataan dan jaminan yang diberikan oleh Penerbit ke Penyelenggara, Pemodal dengan ini menyatakan dan menjamin kepada Penyelenggara hal-hal sebagai berikut: \n a. Bahwa Pemodal akan membebaskan Penyelenggara dari klaim, tuntutan, gugatan dari pihak ketiga atas kelalaian Pemodal dan/atau karyawan Pemodal dalam melaksanakan kerja sama ini; \n b. Bahwa Pemodal menyatakan tidak berkeberatan dalam hal Otoritas Jasa Keuangan dan/atau pihak lain yang sesuai undang-undang berwenang untuk melakukan pemeriksaan, akan melakukan pemeriksaan terhadap pelaksanaan kerja sama ini; \n c. Bahwa Pemodal bersedia untuk kemungkinan pengubahan, pembuatan atau pengambil alih kegiatan yang dilaksanakan oleh Pemodal atau penghentian kerja sama, dalam hal atas permintaan Otoritas Jasa Keuangan apabila diperlukan.",
                                    textAlign: TextAlign.justify),
                              ),
                              //keadaan memaksa
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("KEADAAN MEMAKSA",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Keadaan Memaksa atau Force Majeure adalah kejadian-kejadian yang terjadi diluar kemampuan dan kekuasaan Para Pihak sehingga menghalangi Para Pihak untuk melaksanakan Perjanjian ini, termasuk namun tidak terbatas pada adanya kebakaran, banjir, gempa bumi, badai, huru-hara, peperangan, epidemi, pertempuran, pemogokan, sabotase, embargo, peledakan yang mengakibatkan kerusakan sistem teknologi informasi yang menghambat pelaksanaan kerja sama ini, serta kebijaksanaan pemerintah Republik Indonesia yang secara langsung berpengaruh terhadap pelaksanaan kerja sama ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "2. Masing-masing Pihak dibebaskan untuk membayar denda apabila terlambat dalam melaksanakan kewajibannya dalam kerja sama ini, karena adanya hal-hal Keadaan Memaksa.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Keadaan Memaksa sebagaimana dimaksud harus diberitahukan oleh Pihak yang mengalami Keadaan Memaksa kepada Pihak lainnya dalam kerja sama ini paling lambat 7 (tujuh) Hari Kalender dengan melampirkan pernyataan atau keterangan tertulis dari pemerintah untuk dipertimbangkan oleh Pihak lainnya beserta rencana pemenuhan kewajiban yang tertunda akibat terjadinya Keadaan Memaksa.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Keadaan Memaksa yang menyebabkan keterlambatan pelaksanaan kerja sama ini baik untuk seluruhnya maupun sebagian bukan merupakan alasan untuk pembatalan kerja sama ini sampai dengan diatasinya Keadaan Memaksa tersebut.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //Pengalihan kerja sama
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("PENGALIHAN KERJA SAMA",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "Penerbit setuju dan sepakat untuk tidak mengalihkan sebagian atau keseluruhan hak dan kewajiban Penerbit dalam Program Investasi ini kepada pihak lainnya.",
                                    textAlign: TextAlign.justify),
                              ),
                              //Domisili hukum dan penyelesaian sengketa
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "DOMISILI HUKUM DAN PENYELESAIAN SENGKETA",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "1. Kerja sama ini dibuat, ditafsirkan dan dilaksanakan berdasarkan hukum negara Republik Indonesia.",
                                    textAlign: TextAlign.justify),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Setiap perselisihan yang timbul sehubungan dengan kerja sama ini, akan diupayakan untuk diselesaikan terlebih dahulu oleh Para Pihak dengan melaksanakan musyawarah untuk mufakat.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Apabila penyelesaian perselisihan secara musyawarah tidak berhasil mencapai mufakat sampai dengan 30 (tiga puluh) Hari Kalender sejak dimulainya musyawarah tersebut, maka Para Pihak sepakat untuk menyelesaikan perselisihan tersebut melalui proses pengadilan.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Para Pihak sepakat untuk menyelesaikan perselisihan di Pengadilan Negeri Sleman di Daerah Istimewa Yogyakarta tanpa mengurangi hak dari salah satu untuk mengajukan gugatan pada domisili pengadilan lainnya (non-exlusive jurisdiction).",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //kelalaian / wanprestasi
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "KELALAIAN / WANPRESTASI",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Dalam hal terjadi salah satu hal atau peristiwa yang ditetapkan di bawah ini, maka merupakan suatu kejadian kelalaian (wanprestasi) terhadap kerja sama ini: \n a. Kelalaian dalam kerja sama. Dalam hal salah satu Pihak terbukti sama sekali tidak melaksanakan kewajiban, atau melaksanakan kewajiban tetapi tidak sebagaimana disepakati, atau melaksanakan kewajiban tetapi tidak sesuai dengan waktu yang disepakati, atau melakukan sesuatu yang tidak diperbolehkan dalam terms and conditions. \n b. Pernyataan Tidak Benar. Dalam hal ternyata bahwa sesuatu pernyataan atau jaminan yang diberikan oleh salah satu Pihak kepada Pihak lainnya dalam kerja sama ini terbukti tidak benar atau tidak sesuai dengan kenyataannya dan menimbulkan kerugian langsung yang diderita salah satu Pihak. \n c. Kepailitan. Dalam hal salah satu Pihak dalam Perjanjian ini oleh instansi yang berwenang dinyatakan berada dalam keadaan pailit atau diberikan penundaan membayar hutang-hutang (surseance van betaling). \n d. Permohonan Kepailitan. Dalam hal salah satu Pihak dalam Perjanjian ini mengajukan permohonan kepada instansi yang berwenang untuk dinyatakan pailit atau untuk diberikan penundaan membayar hutang-hutang (surseance van betaling) atau dalam hal pihak lain mengajukan permohonan kepada instansi yang berwenang agar salah satu Pihak dalam Program Investasi ini dinyatakan dalam keadaan pailit.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Dalam hal suatu kejadian kelalaian terjadi dan berlangsung, maka Pihak yang tidak lalai berhak menyampaikan peringatan sebanyak 3 (tiga) kali dengan tenggang waktu 7 (tujuh) Hari Kalender diantara masing-masing peringatan.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Setelah menyampaikan 3 (tiga) kali peringatan, Pihak yang tidak lalai berhak mengajukan tuntutan berupa meminta pemenuhan prestasi dilakukan atau meminta prestasi dilakukan disertai ganti kerugian atau meminta ganti kerugian saja atau menuntut pembatalan Perjanjian atau menuntut pembatalan kerja sama disertai ganti kerugian.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //Penalti
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text("PENALTI",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                    "Apabila dalam kerja sama ini, Pemodal melanggar ketentuan dalam Program Investasi ini maka Pemodal dapat dihentikan dan dikeluarkan oleh Penyelenggara dalam Program Investasi ini.",
                                    textAlign: TextAlign.justify),
                              ),
                              //Mekanisme
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "MEKANISME DALAM HAL PENYELENGGARA TIDAK DAPAT MENJALANKAN OPERASIONALNYA",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "Mekanisme penyelesaian Program Investasi dalam hal Penyelenggara tidak dapat menjalankan operasional adalah sebagai berikut : \n 1. Mekanisme penyelesaian Program Investasi dalam hal Penyelenggara tidak dapat menjalankan operasionalnya akan diatur lebih lanjut dalam perjanjian layanan urun dana antara Penyelenggara dan Penerbit diatur lebih lanjut di Perjanjian Kerjasama Layanan Urun Dana (Equity Crowdfunding).",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              //Lain-lain
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "LAIN-LAIN",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "1. Para Pihak wajib tunduk dan patuh terhadap peraturan perundang-undangan yang berlaku di negara Republik Indonesia terkait pelaksanaan Program Investasi ini.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "2. Program Investasi ini diinterpretasikan dan dilaksanakan berdasarkan hukum yang berlaku di Negara Republik Indonesia.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "3. Segala sesuatu yang tidak atau belum cukup diatur dalam Program Investasi ini, akan diatur kemudian dalam bentuk Perjanjian Layanan Urun Dan atau dokumen lain yang dibuat secara tertulis dan ditandatangani oleh Penyelenggara dan Pemodal.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "4. Keabsahan, penafsiran dan pelaksanaan Program Investasi ini, diatur serta tunduk pada hukum dan peraturan perundang-undangan yang berlaku di negara Republik Indonesia.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "5. Dalam hal ada salah satu atau lebih ketentuan dalam terms and conditions ini dinyatakan tidak sah, tidak berlaku atau tidak dapat dilaksanakan berdasarkan hukum dan/atau peraturan perundang-undangan yang berlaku di Republik Indonesia, maka  kedua belah pihak setuju bahwa keabsahan ketentuan lainnya dalam Perjanjian Layanan Urun Dana tetap berlaku dan dapat dilaksanakan serta tidak akan terpengaruh.",
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              Text(
                                "6. Dokumen-dokumen atau kesepakatan-kesepakatan terkait dengan Program Investasi ini yang telah dibuat oleh Para Pihak sebelum disetujuinya terms and conditions ini dinyatakan dicabut dan tidak berlaku terhitung sejak disetujuinya terms and conditions.",
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          ),
                        ))),
                Container(
                  margin: EdgeInsets.only(right: 20, bottom: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Theme(
                        data: ThemeData(accentColor: Colors.black),
                        child: Checkbox(
                          value: isCheck,
                          onChanged: (value) {
                            setState(() => isCheck = value);
                          },
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16, bottom: 10),
                          child: Text(
                            "Dengan ini saya telah membaca dan menyetujui syarat dan ketentuan yang diberlakukan oleh SANTARA",
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  // decoration: BoxDecoration(
                  //   border: Border.all(color: Color(0xFFE5E5E5)),
                  //   // borderRadius: BorderRadius.circular(4)
                  // ),
                  child: RaisedButton(
                    disabledColor: Color(0xFFE0E0E0),
                    color: Color(0xFFBF2D30),
                    onPressed: isCheck ? () => sendData() : null,
                    child: Text(
                      'Setuju',
                      style: TextStyle(
                          color: isCheck ? Colors.white : Colors.black),
                    ),
                  ),
                ),
              ],
            )
            // ),
            );
  }
}
