import 'package:flutter/material.dart';

class Maintenance extends StatefulWidget {
  @override
  _MaintenanceState createState() => _MaintenanceState();
}

class _MaintenanceState extends State<Maintenance> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width*2/3,
              child: Image.asset("assets/icon/maintenance.png"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Text("Aplikasi sedang dalam perbaikan",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                textAlign: TextAlign.center),
            ),
            Text("Mohon tunggu beberapa saat dan coba lagi.\nMohon maaf atas ketidaknyamanannya.", 
              style: TextStyle(color: Colors.grey[700]), textAlign: TextAlign.center,)
          ],
        ),
      ),
    );
  }
}