import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'package:toast/toast.dart';

class ExpiredSessionUI extends StatefulWidget {
  @override
  _ExpiredSessionUIState createState() => _ExpiredSessionUIState();
}

class _ExpiredSessionUIState extends State<ExpiredSessionUI> {
  final storage = FlutterSecureStorage();
  Future<bool> onWillPop(BuildContext context) async {
    DateTime backButtonPressTime;
    DateTime currentTime = DateTime.now();
    const snackBarDuration = Duration(seconds: 3);
    bool backButtonHasNotBeenPressedOrSnackBarHasBeenClosed =
        backButtonPressTime == null ||
            currentTime.difference(backButtonPressTime) > snackBarDuration;

    if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
      backButtonPressTime = currentTime;
      Toast.show(
        "Tekan sekali lagi untuk keluar",
        context,
        duration: 3,
        gravity: Toast.BOTTOM,
      );
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return true;
    }
    return false;
  }

  logout() async {
    var token = await storage.read(key: "token");
    if (token != null) {
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'AddressCyro');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      storage.delete(key: 'hasSubmitJob');
      storage.delete(key: 'hasSubmitPriority');
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.notif = 0;
    }
  }

  @override
  void initState() {
    super.initState();
    logout();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return onWillPop(context);
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(Sizes.s30),
          width: double.maxFinite,
          height: double.maxFinite,
          color: Color(ColorRev.mainBlack),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/icon/forbidden.png",
                width: Sizes.s200,
              ),
              SizedBox(height: Sizes.s30),
              Text(
                "Sesi Kadaluarsa",
                style: TextStyle(
                    fontSize: FontSize.s20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              SizedBox(height: Sizes.s10),
              Text(
                "Sesi login kamu sudah kadaluarsa, mohon\nlakukan login kembali",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: FontSize.s14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white),
              ),
              SizedBox(height: Sizes.s20),
              Container(
                height: Sizes.s40,
                child: SantaraMainButton(
                  title: "Login Kembali",
                  onPressed: () {
                    Navigator.pushNamed(context, '/login');
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
