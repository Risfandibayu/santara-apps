import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';

import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class BantuanPage extends StatefulWidget {
  @override
  State<BantuanPage> createState() => _BantuanPageState();
}

class _BantuanPageState extends State<BantuanPage> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        appBar: CustomAppbar(drawerKey: _drawerKey),
        drawer: CustomDrawer(),
        backgroundColor: Color(ColorRev.mainBlack),
        body: Center(
          child: Container(
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.25,
                ),
                Image.asset(
                  'assets/icon/message_question.png',
                  scale: 2.1,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                      "Butuh bantuan? langsung chat dan tanyakan ke tim Santara melalui WhatsApp",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      )),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                InkWell(
                  onTap: () {
                    launchWhatsApp(
                        phone: "6281212227765",
                        message:
                            "Halo, apakah ada informasi terbaru mengenai Santara?");
                  },
                  child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                          color: Colors.red[500],
                          border: Border.all(),
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.05,
                      alignment: Alignment.center,
                      child: Text(
                        "Chat Sekarang",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      )),
                )
              ],
            ),
          ),
        ));
  }

  String waNumber = "6281212227765";
  String message = "Halo, apakah ada informasi terbaru mengenai Santara?";

  void launchWhatsApp({
    @required String phone,
    @required String message,
  }) async {
    try {
      String whatsAppUrl =
          "whatsapp://send?phone=$phone&text=${Uri.parse(message)}";
      if (Platform.isIOS) {
        await launch(whatsAppUrl, forceSafariVC: false);
      } else {
        await canLaunch(whatsAppUrl)
            ? launch(whatsAppUrl)
            : Toast.show(
                "Tidak dapat membuka whatsapp",
                context,
                duration: 3,
                gravity: Toast.BOTTOM,
              );
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      Toast.show(
        "Tidak dapat membuka WhatsApp",
        context,
        duration: 3,
        gravity: Toast.BOTTOM,
      );
    }
  }
}
