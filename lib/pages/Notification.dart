import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:string_validator/string_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Notification.dart';
import 'package:santaraapp/pages/Warning.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/api.dart';
import 'package:url_launcher/url_launcher.dart';

class Notif extends StatefulWidget {
  @override
  _NotifState createState() => _NotifState();
}

class _NotifState extends State<Notif> {
  bool check = false;
  int present = 15;
  List<Notifications> notif = [];
  var loading = true;
  var now = DateTime.now();
  var format = new DateFormat('dd MMM yyyy, HH:mm:ss', "ID");
  var _notif = NotifAssetTitles();
  Future getNotification() async {
    final headers = await UserAgent.headers();
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final http.Response response =
        await http.get('$apiLocal/notification/', headers: headers);
    if (response.statusCode == 200) {
      setState(() {
        notif = notificationsFromJson(response.body);
        loading = false;
      });
    } else if (response.statusCode == 401) {
      NavigatorHelper.pushToExpiredSession(context);
    } else if (response.statusCode == 404) {
      setState(() {
        notif = [];
        loading = false;
      });
    } else {
      setState(() {
        notif = null;
        loading = false;
      });
    }
  }

  Future checking() async {
    // SharedPreferences storage = await SharedPreferences.getInstance();
    final storage = new FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    if (token != null) {
      setState(() {
        check = true;
        getNotification();
      });
    } else {
      setState(() {
        check = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    checking();
  }

  NotifAssetTitles actionString(String action) {
    NotifAssetTitles _data = NotifAssetTitles();
    switch (action) {
      case 'beli':
        _data = NotifAssetTitles(
            title: 'Pemberitahuan',
            asset: 'assets/icon_notif/order_berhasil.png');
        break;
      case 'upload bukti':
        _data = NotifAssetTitles(
            title: 'Konfirmasi', asset: 'assets/icon_notif/upload_bukti.png');
        break;
      case 'pencairan':
        _data = NotifAssetTitles(
            title: 'Pencairan Dividen',
            asset: 'assets/icon_notif/dividen_sukses.png');
        break;
      case 'deposit':
        _data = NotifAssetTitles(
            title: 'Deposit', asset: 'assets/icon_notif/deposit.png');
        break;
      case 'withdraw':
        _data = NotifAssetTitles(
            title: 'Withdraw', asset: 'assets/icon_notif/withdraw.png');
        break;
      case 'vote pengajuan bisnis':
        _data = NotifAssetTitles(
            title: 'Pengajuan Bisnis',
            asset: 'assets/icon_notif/person_outline.png');
        break;
      case 'menyukai pengajuan bisnis':
        _data = NotifAssetTitles(
            title: 'Pralisting', asset: 'assets/icon_notif/love_border.png');
        break;
      case 'ulasan pengajuan bisnis':
        _data = NotifAssetTitles(
            title: 'Pralisting', asset: 'assets/icon_notif/chat_bubble.png');
        break;
      case 'peluncuran bisnis baru':
        _data = NotifAssetTitles(
            title: 'Penerbit Baru', asset: 'assets/icon_notif/new_emiten.png');
        break;
      case 'penyerahan dana':
        _data = NotifAssetTitles(
            title: 'Penerbit Penyerahan Dana',
            asset: 'assets/icon_notif/dividen_sukses.png');
        break;
      case 'pembagian dividen':
        _data = NotifAssetTitles(
            title: 'Penerbit Pembagian Dividen',
            asset: 'assets/icon_notif/dividen.png');
        break;
      case 'pembayaran':
        _data = NotifAssetTitles(
            title: 'Pembayaran', asset: 'assets/icon_notif/pembayaran.png');
        break;
      case 'beli saham berhasil':
      case 'jual saham berhasil':
      case 'beli saham masuk':
      case 'jual saham masuk':
      case 'market 3 day':
      case 'market 1 day':
      case 'market open':
      case 'market sell emiten':
      case 'market watchlist':
        _data = NotifAssetTitles(
            title: 'Pasar Sekunder', asset: 'assets/icon_notif/market.png');
        break;
      default:
        _data = NotifAssetTitles(
            title: 'Pemberitahuan',
            asset: 'assets/icon_notif/pemberitahuan.png');
    }
    return _data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: Navigator.of(context).pop,
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          shape: Border(
              bottom: BorderSide(color: Color(ColorRev.mainwhite), width: 0.5)),
          title: Image.asset(
            'assets/santara/1.png',
            width: 110,
          ),
          centerTitle: true,
          backgroundColor: Color(ColorRev.mainBlack),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Color(ColorRev.mainBlack),
        body: check
            ? loading
                ? Center(
                    heightFactor: 20,
                    child: CircularProgressIndicator(),
                  )
                : notif == null
                    ? Center(
                        child: Text(
                        'Periksa kembali koneksi internet anda',
                        style: TextStyle(color: Colors.white),
                      ))
                    : notif.length == 0
                        ? Container(
                            color: Color(ColorRev.mainBlack),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                    "assets/icon/notification_empty.png"),
                                Text(
                                  "Tidak ada notifikasi",
                                  style: TextStyle(
                                      color: Color(ColorRev.mainwhite)),
                                )
                              ],
                            ),
                          )
                        : ListView(
                            children: <Widget>[
                              ListView.builder(
                                itemCount: present >= notif.length
                                    ? notif.length
                                    : present,
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                itemBuilder: (BuildContext context, int index) {
                                  return _itemList(notif[index]);
                                },
                              ),
                              present >= notif.length
                                  ? Container()
                                  : _selengkpanya(),
                            ],
                          )
            : Warning(
                page: 'notifikasi', image: "assets/icon/login_dahulu.png"));
  }

  Widget _itemList(Notifications notif) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () async {
            try {
              if (notif.action != null && isURL(notif.action)) {
                if (await canLaunch(notif.action)) {
                  await launch(notif.action, forceSafariVC: false);
                } else {
                  throw 'Cannot launch ${notif.action}';
                }
              }
            } catch (e, stack) {
              santaraLog(e, stack);
            }
          },
          child: Container(
              color: Color(ColorRev.mainBlack),
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          margin: EdgeInsets.only(right: 8),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage(actionString(notif.action).asset),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            actionString(notif.action).title,
                            style: TextStyle(
                              fontSize: 12,
                              color: Color(
                                0xFFD4D4D4,
                              ),
                            ),
                          ),
                        ),
                        Text(
                          format.format(
                            DateTime.parse(
                              notif.createdAt,
                            ),
                          ),
                          style: TextStyle(fontSize: 12, color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(bottom: 5),
                                child: Text(notif.title,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                              ),
                              Text(
                                notif.message,
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: 0.5,
          color: Color(0XFFF5F5F5),
        ),
      ],
    );
  }

  Widget _selengkpanya() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
      child: FlatButton(
          onPressed: () {
            setState(() {
              present = present + 15;
            });
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.keyboard_arrow_down, color: Color(0xFF218196)),
              Container(width: 12),
              Text("Lihat Selengkapnya",
                  style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF218196),
                      fontWeight: FontWeight.w600)),
            ],
          )),
    );
  }
}

class NotifAssetTitles {
  final String title;
  final String asset;
  NotifAssetTitles({this.title, this.asset});
}
