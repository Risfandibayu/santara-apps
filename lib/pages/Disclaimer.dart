import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';

class Disclaimer extends StatelessWidget {
  final String text;
  Disclaimer({this.text});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(ColorRev.mainBlack),
        centerTitle: true,
        title: Image.asset('assets/santara/1.png', width: 110.0),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Color(ColorRev.mainBlack),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              text,
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.justify,
            ),
          ),
        ),
      ),
    );
  }
}
