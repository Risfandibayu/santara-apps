import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class Tutorial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll){
          overscroll.disallowGlow();
          return true;
        }, 
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              MaterialButton(
                child: Text('Skip >', style: TextStyle(color: Colors.grey),),
                onPressed: () async {
                  // SharedPreferences storage = await SharedPreferences.getInstance();
                  final storage = new FlutterSecureStorage();
                  storage.write(key: 'tutor', value: '1');
                  Navigator.of(context).pushNamedAndRemoveUntil('/index', (Route<dynamic> route)=>false);
                },
              ),
              Container(
                height: MediaQuery.of(context).size.height / 1.3,
                child: Carousel(
                  dotBgColor: Colors.transparent,
                  moveIndicatorFromBottom: 20,
                  autoplay: false,
                  dotColor: Colors.grey,
                  images: [
                    AssetImage('assets/tutorial/Onboard1.0.png'),
                    AssetImage('assets/tutorial/Onboard1.1.png'),
                    AssetImage('assets/tutorial/Onboard1.2.png'),
                    AssetImage('assets/tutorial/Onboard1.3.png'),
                    AssetImage('assets/tutorial/Onboard1.4.png')
                  ],
                ),
              )
              
              // Expanded(
              //   child: PageView(
              //     children: <Widget>[
              //       Container(
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Center(
              //               child: Image.asset('assets/tutorial/Illustration1.png', height: MediaQuery.of(context).size.height / 2.2,),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              //               child: Text('Investasi ke UMKM Terbaik di Indonesia', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              //               child: Text('Selamat datang di Santara. tempat dimana Anda bisa menemukan investasidari berbagai UMKM terbaik di Indonesia. UMKM di Santara terpilih dari seleksi ketat oleh tim kami', textAlign: TextAlign.center,),
              //             )
              //           ],
              //         ),
              //       ),
              //       Container(
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Container(
              //               margin: EdgeInsets.symmetric(vertical: 70),
              //               child: Center(
              //                 child: Image.asset('assets/tutorial/Illustration2.png', height: MediaQuery.of(context).size.height / 3,),
              //               ),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              //               child: Text('Sign Up', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              //               child: Text('Klik "Member Area" yang ada di menu bar pojok kanan atas. Lalu pada halaman Login, pilih menu "Daftar" yang ada di pojok kanan bawah.Isi data diri sampai Anda berhasil membuat akun', textAlign: TextAlign.center,),
              //             )
              //           ],
              //         ),
              //       ),
              //       Container(
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Container(
              //               margin: EdgeInsets.symmetric(vertical: 70),
              //               child: Center(
              //                 child: Image.asset('assets/tutorial/Illustration3.png', height: MediaQuery.of(context).size.height / 3,),
              //               ),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              //               child: Text('Pilih Penerbit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              //               child: Text('Setelah memilih UKM yang ada di homepage Santara, Anda bisa langsung klik "Booking Token" apabila token masih tersedia', textAlign: TextAlign.center,),
              //             )
              //           ],
              //         ),
              //       ),
              //       Container(
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Container(
              //               margin: EdgeInsets.symmetric(vertical: 70),
              //               child: Center(
              //                 child: Image.asset('assets/tutorial/Illustration4.png', height: MediaQuery.of(context).size.height / 3,),
              //               ),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              //               child: Text('Masukkan Nominal', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              //               child: Text('Masukkan jumlah rencana investasi Andapada bagian "Yuk Miliki Bisnis Ini" kemudian klik "Beli Token Sekarang", dan ikuti alur pembayarannya', textAlign: TextAlign.center,),
              //             )
              //           ],
              //         ),
              //       ),
              //       Container(
              //         child: Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Container(
              //               margin: EdgeInsets.symmetric(vertical: 70),
              //               child: Center(
              //                 child: Image.asset('assets/tutorial/Illustration5.png', height: MediaQuery.of(context).size.height / 3,),
              //               ),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              //               child: Text('Selamat', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center),
              //             ),
              //             Container(
              //               margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              //               child: Text('Setelahnya, Anda akan mendapatkan email notifikasi dari team Santara atas pembelian investasi. Selamat! Anda telah menjadi pemodal Santara dan akan mendapat bagi hasil pada jangka waktu yang telah ditetapkan', textAlign: TextAlign.center,),
              //             )
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
            ],
          )
        )
      )
    );
  }
}