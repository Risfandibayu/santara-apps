import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';

class Login1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        color: Color(ColorRev.mainBlack),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //logo image
                  Padding(
                    padding: EdgeInsets.only(top: 50.0, bottom: 60.0),
                    child: Container(
                      alignment: Alignment.center,
                      // width: 140.0,
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/santara/Logo_Santara.png',
                            height: MediaQuery.of(context).size.width / 3,
                          ),
                          SizedBox(height: 50),
                          Text(
                            'Selamat Datang Di Aplikasi Santara',
                            style: TextStyle(
                                color: Color(ColorRev.mainwhite),
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      // decoration: BoxDecoration(
                      //     image: DecorationImage(
                      //         image:
                      //             AssetImage('assets/santara/Logo_Santara.png'),
                      //         fit: BoxFit.fitHeight)),
                    ),
                  ),

                  //button signup
                  Padding(
                    padding: EdgeInsets.only(top: 50.0),
                    child: Container(
                      width: 230.0,
                      height: 50.0,
                      child: RaisedButton(
                        child: Text('Daftar'),
                        elevation: 5.0,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/register');
                        },
                      ),
                    ),
                  ),

                  //buton login
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Container(
                      width: 230.0,
                      height: 50.0,
                      child: RaisedButton(
                        key: Key('loginButton'),
                        child: Text(
                          'Masuk',
                          style: TextStyle(color: Colors.white),
                        ),
                        elevation: 5.0,
                        color: Colors.red[700],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          Navigator.pushNamed(context, '/login');
                        },
                      ),
                    ),
                  )
                ]),
          ],
        ));
  }
}
