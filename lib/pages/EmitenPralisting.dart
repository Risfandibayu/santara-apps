import 'dart:convert';

List<EmitenPralisting> emitenPralistingFromJson(String str) => List<EmitenPralisting>.from(json.decode(str).map((x) => EmitenPralisting.fromJson(x)));

String emitenPralistingToJson(List<EmitenPralisting> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmitenPralisting {
    int id;
    String uuid;
    String companyName;
    String trademark;
    String category;
    List<Picture> pictures;
    int isActive;
    int likes;
    int votes;
    int comments;
    String status;

    EmitenPralisting({
        this.id,
        this.uuid,
        this.companyName,
        this.trademark,
        this.category,
        this.pictures,
        this.isActive,
        this.likes,
        this.votes,
        this.comments,
        this.status,
    });

    factory EmitenPralisting.fromJson(Map<String, dynamic> json) => EmitenPralisting(
        id: json["id"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        category: json["category"],
        pictures: List<Picture>.from(json["pictures"].map((x) => Picture.fromJson(x))),
        isActive: json["is_active"],
        likes: json["likes"],
        votes: json["votes"],
        comments: json["comments"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        "pictures": List<dynamic>.from(pictures.map((x) => x.toJson())),
        "is_active": isActive,
        "likes": likes,
        "votes": votes,
        "comments": comments,
        "status": status,
    };
}

class Picture {
    String picture;

    Picture({
        this.picture,
    });

    factory Picture.fromJson(Map<String, dynamic> json) => Picture(
        picture: json["picture"],
    );

    Map<String, dynamic> toJson() => {
        "picture": picture,
    };
}
