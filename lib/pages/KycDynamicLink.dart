import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/Constants.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/helpers/kyc/StatusPerusahaan.dart';
import 'package:santaraapp/helpers/kyc/StatusTraderKyc.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/services/kyc/KycPersonalService.dart';
import 'package:santaraapp/services/kyc/KycPerusahaanService.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/widget/setting_account/setting.dart';
import 'package:url_launcher/url_launcher.dart';

class KycDynamicLinkUI extends StatefulWidget {
  @override
  _KycDynamicLinkUIState createState() => _KycDynamicLinkUIState();
}

class _KycDynamicLinkUIState extends State<KycDynamicLinkUI> {
  getData() async {
    if (Helper.check) {
      EndStatus _kycStatusResult;
      await Constants.getUserData().then((value) async {
        var traderType = value.trader.traderType;
        try {
          if (traderType == "personal") {
            var _service = KycPersonalService();
            await _service.statusIndividualKyc().then((res) {
              if (res != null) {
                if (res.statusCode == 200) {
                  var result = StatusTraderKycHelper.checkingIndividual(
                      StatusKycTrader.fromJson(res.data));
                  if (result != null) {
                    setState(() {
                      _kycStatusResult = result.status;
                    });
                  }
                } else if (res.statusCode == 401) {
                  NavigatorHelper.pushToExpiredSession(context);
                } else {
                  ToastHelper.showFailureToast(
                      context, "Error ${res.statusMessage}");
                }
              }
            });
          } else if (traderType == "company") {
            KycPerusahaanService _service = KycPerusahaanService();
            await _service.statusKycTrader().then((res) {
              if (res != null) {
                if (res.statusCode == 200) {
                  var result = StatusPerusahaanHelper.checkingStatus(
                      StatusKycTrader.fromJson(res.data));
                  if (result != null) {
                    setState(() {
                      _kycStatusResult = result.status;
                    });
                  }
                } else if (res.statusCode == 401) {
                  NavigatorHelper.pushToExpiredSession(context);
                }
              }
            });
          } else {
            ToastHelper.showFailureToast(
              context,
              "Anda belum memilih jenis trader!",
            );
          }
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => Setting(
                status: _kycStatusResult,
              ),
            ),
          );
        } catch (e) {
          ToastHelper.showFailureToast(context, "Tidak dapat membuka KYC");
          // Navigator.pop(context);
        }
      });
    } else {
      Navigator.pop(context);
      var url = "https://santara.co.id";
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Cannot launch $url';
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/index');
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
        ),
        body: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Center(
            child: CupertinoActivityIndicator(),
          ),
        ),
      ),
    );
  }
}
