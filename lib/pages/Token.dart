import 'package:flutter/material.dart';

//components
import 'package:santaraapp/widget/token/TokenPayment.dart';

class Token extends StatefulWidget {
  @override
  _TokenState createState() => _TokenState();
}

class _TokenState extends State<Token> {

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (Helper.check) {
    //   Helper.restartTimer(context, true);
    // }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset('assets/santara/1.png', width: 110,),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black
        ),
      ),
      body: TokenPayment(),
    );
  }
}