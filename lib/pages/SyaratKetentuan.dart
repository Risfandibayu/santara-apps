import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:url_launcher/url_launcher.dart';

class SyaratKetentuan extends StatefulWidget {
  final String urlPdf;
  final int data;
  SyaratKetentuan({this.urlPdf, this.data});
  @override
  _SyaratKetentuanState createState() => _SyaratKetentuanState();
}

class _SyaratKetentuanState extends State<SyaratKetentuan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Colors.black,
          centerTitle: true,
          title: Image.asset('assets/santara/1.png', width: 110.0),
        ),
        body: Container(
          color: Color(ColorRev.mainBlack),
          child: ListView(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height - 112,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Color(ColorRev.mainBlack),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Text(
                          widget.data == 0
                              ? "Syarat dan Ketentuan\nPemodal"
                              : widget.data == 1
                                  ? "Syarat dan Ketentuan\nPenerbit"
                                  : "Kebijakan dan Privasi",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                          textAlign: TextAlign.center),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.all(16),
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height / 6),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: <Widget>[
                          Icon(MdiIcons.pdfBox, size: 80, color: Colors.grey),
                          Text(
                            widget.data == 0
                                ? "tnc-pemodal.pdf"
                                : widget.data == 1
                                    ? "tnc-penerbit.pdf"
                                    : "privacy-policy.pdf",
                            style: TextStyle(color: Colors.black),
                          )
                        ],
                      ),
                    ),
                    FlatButton(
                      color: Color(0xFF1BC47D),
                      child: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Center(
                              child: Text(
                            "Download",
                            style: TextStyle(color: Colors.white),
                          ))),
                      onPressed: () async {
                        if (await canLaunch(widget.urlPdf)) {
                          await launch(widget.urlPdf);
                        } else {
                          throw 'Could not launch ${widget.urlPdf}';
                        }
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
