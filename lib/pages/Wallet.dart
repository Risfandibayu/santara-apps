import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Perdana.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/widget/widget/components/kyc/KycNotificationStatus.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAppBetaTesting.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';

class Wallet extends StatefulWidget {
  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  double saldo = 0.0;
  double assetBalance = 0.0;
  final rupiah = new NumberFormat("#,##0");
  final storage = new FlutterSecureStorage();
  var token;
  var isLoading = true;
  var loading = true;
  int present = 4;
  List<Perdana> perdanaList;

  Future getLocalStorage() async {
    token = await storage.read(key: 'token');
  }

  Future getSaldoRupiah() async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response =
          await http.get('$apiLocal/traders/idr', headers: headers);
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        setState(() {
          saldo = data['idr'] / 1;
          loading = false;
        });
      } else {
        setState(() {
          saldo = 0.0;
          loading = false;
        });
      }
    } on SocketException catch (_) {
      saldo = 0.0;
    }
  }

  Future getAllToken() async {
    try {
      final response = await http.get('$apiLocal/tokens/',
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      if (response.statusCode == 200) {
        setState(() {
          perdanaList = perdanaFromJson(response.body);
          for (var item in perdanaList) {
            assetBalance = assetBalance + item.amount / 1;
          }
          isLoading = false;
        });
      } else {
        setState(() {
          assetBalance = 0.0;
          perdanaList = [];
          isLoading = false;
        });
      }
    } on SocketException catch (_) {
      debugPrint("nothing");
    } catch (_, __) {
      debugPrint(__.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage().then((_) {
      getAllToken();
      getSaldoRupiah();
      present = 4;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // key: _drawerKey,
        // appBar: CustomAppbar(drawerKey: _drawerKey),
        // drawer: CustomDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text("Wallet",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: SantaraAppBetaTesting(),
                ),
                _ballance(),
                _token(),
              ],
            ),
            KycNotificationStatus(
              showCloseButton: false,
            )
          ],
        ));
  }

  Widget _ballance() {
    return Container(
      margin: EdgeInsets.fromLTRB(12, 16, 12, 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: Color(0xFFC4C4C4))),
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                _itemBallance(
                    "Saldo Rupiah",
                    loading
                        ? SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator())
                        : Text('Rp ${rupiah.format(saldo)}',
                            style: TextStyle(
                                fontSize: 16,
                                // color: Color(0xFF0E7E4A),
                                fontWeight: FontWeight.bold)),
                    false,
                    "Saldo Rupiah adalah dana yang diambil dari total saldo deposit anda"),
                _itemBallance(
                    "Total Saham",
                    loading || isLoading
                        ? SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator())
                        : Text('Rp ${rupiah.format(assetBalance)}',
                            style: TextStyle(
                                fontSize: 16,
                                // color: Color(0xFF0E7E4A),
                                fontWeight: FontWeight.bold)),
                    false,
                    "Total Saham adalah nilai keseluruhan saham yang anda miliki dalam nominal rupiah."),
              ],
            ),
          ),
          _itemBallance(
              "Asset Ballance",
              loading || isLoading
                  ? SizedBox(
                      height: 16, width: 16, child: CircularProgressIndicator())
                  : Text('Rp ${rupiah.format(assetBalance + saldo)}',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
              true,
              "Asset balance adalah gabungan dari Saldo Rupiah dan Total Saham yang anda miliki."),
        ],
      ),
    );
  }

  Widget _itemBallance(String title, Widget value, bool isLast, String desc) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
          color: isLast ? Color(0xFFF2F2F2) : null,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(4), bottomRight: Radius.circular(4))),
      child: Row(
        children: <Widget>[
          Text(title, style: TextStyle(color: Colors.black)),
          IconButton(
              icon: Icon(
                Icons.help,
                size: 16,
                color: Color(0xFF218196),
              ),
              onPressed: () => showParamInfo(title, desc)),
          Expanded(child: Container()),
          value
        ],
      ),
    );
  }

  Widget _token() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      isLoading || perdanaList.length == 0
          ? Container()
          : Container(
              margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
              child: Center(
                child: Text(
                  'Saham yang dimiliki',
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
              ),
            ),
      isLoading || loading
          ? Center(
              heightFactor: MediaQuery.of(context).size.width / 50,
              child: CircularProgressIndicator())
          : perdanaList.length == 0
              ? Container(
                  padding: EdgeInsets.only(top: 20),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/icon/empty_dividen.png",
                        width: MediaQuery.of(context).size.width / 2,
                        fit: BoxFit.fitWidth,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16, bottom: 6),
                        child: Text("Anda belum memiliki saham",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold)),
                      ),
                      Text("Yuk miliki bisnis di Santara",
                          style: TextStyle(
                              fontSize: 14, color: Color(0xFF676767))),
                    ],
                  ),
                )
              : Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio:
                                        MediaQuery.of(context).size.width /
                                            MediaQuery.of(context).size.height,
                                    mainAxisSpacing: 1,
                                    crossAxisSpacing: 1),
                            itemCount: present >= perdanaList.length
                                ? perdanaList.length
                                : present,
                            shrinkWrap: true,
                            padding: const EdgeInsets.all(8),
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              return _listPerdana(perdanaList[index]);
                            })),
                    perdanaList.length <= 4
                        ? Container()
                        : present >= perdanaList.length
                            ? Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
                                child: FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        present = 4;
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.keyboard_arrow_up,
                                            color: Color(0xFF218196)),
                                        Container(width: 12),
                                        Text("Lihat lebih sedikit",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Color(0xFF218196),
                                                fontWeight: FontWeight.w600)),
                                      ],
                                    )),
                              )
                            : Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
                                child: FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        present = present + 4;
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.keyboard_arrow_down,
                                            color: Color(0xFF218196)),
                                        Container(width: 12),
                                        Text("Lihat Selengkapnya",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Color(0xFF218196),
                                                fontWeight: FontWeight.w600)),
                                      ],
                                    )),
                              )
                  ],
                )
    ]);
  }

  Widget _listPerdana(Perdana perdana) {
    var picture = perdana.picture != null && perdana.picture.length > 0
        ? perdana.picture[0].picture
        : "";
    return Container(
        margin: EdgeInsets.all(4),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: Color(0xFFE6EAFA).withOpacity(0.75)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFFE6EAFA),
                blurRadius: 8,
              )
            ]),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //image
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4), topRight: Radius.circular(4)),
                child: SantaraCachedImage(
                  height: MediaQuery.of(context).size.height / 4.5,
                  width: double.maxFinite,
                  image: '$picture',
                  fit: BoxFit.cover,
                ),
              ),
              //content
              Container(
                margin: EdgeInsets.fromLTRB(10, 12, 10, 10),
                height: MediaQuery.of(context).size.height / 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      perdana.companyName,
                      maxLines: 2,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Jumlah saham", style: TextStyle(fontSize: 12)),
                        Text(
                          "${perdana.token} ${perdana.code}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Jumlah saham", style: TextStyle(fontSize: 12)),
                        Text(
                          "Rp ${rupiah.format(perdana.amount)}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ]));
  }

  void showParamInfo(String name, String info) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Text(name,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 10),
                    child: Text(info,
                        style:
                            TextStyle(color: Color(0xFF676767), fontSize: 15),
                        overflow: TextOverflow.visible,
                        textAlign: TextAlign.center),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
