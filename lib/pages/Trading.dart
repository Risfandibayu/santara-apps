import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class Trading extends StatefulWidget {
  @override
  _TradingState createState() => _TradingState();
}

class _TradingState extends State<Trading> {
  bool check;

  Future checking() async {
    final storage = new FlutterSecureStorage();
    final token = storage.read(key: 'token');
    if(token != null){
      setState(() {
        check = true;     
      });
    } else {
      setState(() {
        check = false;     
      });
    }
  }

  @override
  void initState(){
    super.initState();
    checking();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (Helper.check) {
    //   Helper.restartTimer(context, true);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
          child: Image.asset('assets/ongoing/cookin.jpg', fit: BoxFit.fill),
      ),
    );
  }
}