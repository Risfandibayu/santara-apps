import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  var total = 0;
  var jumlah = 0;
  var hasil = 0;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (Helper.check) {
    //   Helper.restartTimer(context, true);
    // }
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
      child: Column(
        children:<Widget>[
          Container(
            height: 120.0,
            child: Card(
              elevation: 5.0,
              child: Row(
                children: <Widget>[
                  //image
                  Container(
                    padding: EdgeInsets.only(left:10.0),
                    width: 80.0,
                    child: Image.network('http://cdn.santarax.com/images/logo/emiten/cyronium.jpg')
                  ),
                  //detail
                  Container(
                    padding: EdgeInsets.only(top:20.0, left: 20.0),
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('CYRONIUM', style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold),),
                      Padding(padding: EdgeInsets.only(top: 5.0),),
                      Text('Rp.30.000.000,-', style: TextStyle(fontSize: 15.0),),
                      Padding(
                        padding: EdgeInsets.only(top:10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Rp.$hasil,-', style: TextStyle(fontSize: 15.0),
                            ),
                            Container(
                              width: 30.0,
                              height: 30.0,
                              child: RaisedButton(
                                color: Colors.red[700],
                                onPressed: (){
                                  setState(() {
                                    if(jumlah == 0){
                                      jumlah = 0;
                                    }else{
                                      setState(() {
                                        jumlah--;
                                        hasil = hasil - 3000000;
                                      });
                                    }
                                  });
                                },
                                child: Text('-', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                              ),
                            ),
                            Container(
                              width: 50.0,
                              height: 30.0,
                              decoration: BoxDecoration(
                                border: Border.all( color: Colors.black)
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(top: 5.0),
                                child: Text('$jumlah', textAlign: TextAlign.center,)
                                ),
                              ),
                            Container(
                              width: 30.0,
                              height: 30.0,
                              child: RaisedButton(
                                color: Colors.red[700],
                                onPressed: (){
                                  setState(() {
                                    jumlah++;
                                    hasil = jumlah*3000000;
                                  });
                                },
                                child: Text('+', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ),
          ),
        ]
      ),
    );
  }
}