import 'dart:convert';
import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../core/usecases/unauthorize_usecase.dart';
import '../models/User.dart';
import '../models/UserJourney.dart';
import '../utils/api.dart';
import '../widget/widget/components/main/SantaraAppBetaTesting.dart';

class Journey extends StatefulWidget {
  @override
  _JourneyState createState() => _JourneyState();
}

class _JourneyState extends State<Journey> {
  String token;
  final storage = new FlutterSecureStorage();
  DateFormat dateFormat = new DateFormat("dd MMM yyyy \t HH:mm", "id");
  final rupiah = new NumberFormat("#,##0");

  int lastPage = 0;
  int page = 1;
  int offset = 4;
  String activity;
  String status;
  String sort = "true";
  String search;
  var searchKey = TextEditingController();
  bool isLoading = true;
  bool isLoadingPage = false;
  List<Datum> journey;

  bool isFilter = false;
  bool isSort = false;
  bool isSearch = false;
  String msg;
  User user;

  //list dropdown choice yang disukai
  List<Map> sortBy = [
    {"id": "1", "sort": "Terbaru", "value": "true"},
    {"id": "2", "sort": "Terlama", "value": "false"},
  ];
  List<DropdownMenuItem<String>> dropDownSortBy;
  List<DropdownMenuItem<String>> getChoiceSortBy() {
    List<DropdownMenuItem<String>> items = new List();
    sortBy.map((value) {
      items.add(DropdownMenuItem(
        value: value['value'],
        child: Text(value['sort']),
      ));
    }).toList();
    return items;
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
  }

  Future getDataJourney() async {
    var rawUser = await storage.read(key: "user");
    var url =
        '$apiLocal/journeys?activity=$activity&sort=$sort&search=$search&limit=$page&offset=$offset&status=$status';
    final http.Response response = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      setState(() {
        if (rawUser != null) {
          user = User.fromJson(json.decode(rawUser));
        }
        if (page == 1) {
          isLoading = false;
          var data = userJourneyFromJson(response.body);
          lastPage = data.lastPage;
          journey = data.data;
        } else {
          var data = userJourneyFromJson(response.body);
          lastPage = data.lastPage;
          for (var item in data.data) {
            journey.add(item);
          }
          isLoadingPage = false;
          isLoading = false;
        }
      });
    } else {
      var data = jsonDecode(response.body);
      setState(() {
        msg = data["message"];
        isLoading = false;
        isLoadingPage = false;
        journey = null;
      });
    }
  }

  String totalTrxMarket(String title, dynamic total, dynamic fee) {
    if (title == "Penjualan Saham Sekunder") {
      return "Rp ${rupiah.format(total + fee)}";
    } else {
      return "Rp ${rupiah.format(total - fee)}";
    }
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    dropDownSortBy = getChoiceSortBy();
    sort = dropDownSortBy[0].value;
    getLocalStorage().then((_) {
      getDataJourney();
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, 'refresh');
        return;
      },
      child: Scaffold(
        backgroundColor: isFilter || isSort ? null : Colors.white,
        appBar: isSearch
            ? AppBar(
                title: Container(
                  child: TextField(
                    controller: searchKey,
                    onSubmitted: (value) {
                      setState(() {
                        isLoading = true;
                        page = 1;
                        search = value;
                        getDataJourney();
                      });
                    },
                    decoration: InputDecoration(hintText: "Search..."),
                  ),
                ),
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: Icon(Icons.close),
                  color: Colors.black,
                  onPressed: () {
                    setState(() {
                      if (isSearch) {
                        isSearch = false;
                      } else {
                        isSearch = true;
                      }
                    });
                  },
                ),
              )
            : AppBar(
                title: Image.asset('assets/santara/1.png', width: 110.0),
                centerTitle: true,
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.black),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.search, color: Colors.black),
                    onPressed: () {
                      setState(() {
                        if (isSearch) {
                          isSearch = false;
                        } else {
                          isSearch = true;
                        }
                      });
                    },
                  )
                ],
              ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(height: 12),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: SantaraAppBetaTesting(),
              ),
              // filter and sort
              Padding(
                padding: const EdgeInsets.all(16),
                child: Row(
                  children: <Widget>[
                    Flexible(
                        flex: 2,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              if (isSort) {
                                isSort = false;
                              } else {
                                isSort = true;
                                isFilter = false;
                              }
                            });
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.sort, color: Colors.black),
                              Container(width: 8),
                              Text("Urutkan", style: TextStyle(fontSize: 15))
                            ],
                          ),
                        )),
                    Flexible(
                        flex: 3,
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              if (isFilter) {
                                isFilter = false;
                              } else {
                                isFilter = true;
                                isSort = false;
                              }
                            });
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.filter_list, color: Colors.black),
                              Container(width: 8),
                              Text("Filter", style: TextStyle(fontSize: 15))
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              isFilter ? _kategory() : Container(),
              isFilter ? _status() : Container(),
              isSort ? _sortby() : Container(),

              //list data
              isLoading
                  ? Container(
                      height: isSort || isFilter
                          ? 100
                          : MediaQuery.of(context).size.height - 200,
                      child: Center(child: CircularProgressIndicator()))
                  : journey == null || journey.length == 0
                      ? Container(
                          height: isSort || isFilter
                              ? 100
                              : MediaQuery.of(context).size.height - 200,
                          child: Center(
                            child: Text(msg),
                          ),
                        )
                      : Container(
                          decoration: isSort || isFilter
                              ? BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20)),
                                  color: Colors.white,
                                  boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey[300],
                                          blurRadius: 5,
                                          offset: Offset(0, -3))
                                    ])
                              : BoxDecoration(),
                          child: Column(
                            children: <Widget>[_list(), _pagination()],
                          )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _list() {
    return ListView.builder(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: journey.length,
      padding: isSort || isFilter
          ? EdgeInsets.fromLTRB(8, 20, 8, 8)
          : EdgeInsets.fromLTRB(8, 0, 8, 8),
      itemBuilder: (_, i) {
        if (journey[i].activity == "Registrasi Akun" ||
            journey[i].activity == "Penggantian Nomor Handphone" ||
            journey[i].activity == "Penggantian Alamat Email" ||
            journey[i].activity == "Penggantian Password" ||
            journey[i].activity == "KYC") {
          return _itemList(journey[i]);
        } else {
          return _itemListWithExpand(journey[i]);
        }
      },
    );
  }

  Widget _pagination() {
    return page >= lastPage
        ? Container()
        : isLoadingPage
            ? Container(child: Center(child: CircularProgressIndicator()))
            : Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 12),
                child: FlatButton(
                    onPressed: () {
                      setState(() {
                        isLoadingPage = true;
                        page = page + 1;
                        getDataJourney();
                      });
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.keyboard_arrow_down,
                            color: Color(0xFF218196)),
                        Container(width: 12),
                        Text("Lihat Selengkapnya",
                            style: TextStyle(
                                fontSize: 16,
                                color: Color(0xFF218196),
                                fontWeight: FontWeight.w600)),
                      ],
                    )),
              );
  }

  Widget _itemList(Datum data) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xFFDDDDDD)),
          borderRadius: BorderRadius.circular(2)),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                // icon
                Container(
                    padding: EdgeInsets.only(right: 12),
                    child: Image.asset(data.activity == "Registrasi Akun"
                        ? "assets/icon_journey/reg_sukses.png"
                        : data.activity == "Penggantian Nomor Handphone"
                            ? "assets/icon_journey/change_phone.png"
                            : data.activity == "Penggantian Alamat Email"
                                ? "assets/icon_journey/change_email.png"
                                : data.activity == "Penggantian Password"
                                    ? "assets/icon_journey/change_password.png"
                                    : "assets/icon_journey/kyc.png")),

                // title dan subtitla aktivitas
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(data.activity,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      data.information1 == null
                          ? Container()
                          : Text(
                              data.information1,
                              style: TextStyle(fontSize: 15),
                            )
                    ],
                  ),
                )
              ],
            ),
            Container(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                // tanggal dan status aktivitas
                Text("${dateFormat.format(data.createdAt)}"),
                Text(data.status,
                    style: TextStyle(
                        color: data.status == "Berhasil"
                            ? Color(0xFF0E7E4A)
                            : data.status == "Gagal"
                                ? Color(0xFFBF2D30)
                                : Color(0xFFE5A037),
                        fontWeight: FontWeight.bold)),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _itemListWithExpand(Datum data) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      scrollOnExpand: false,
      scrollOnCollapse: true,
      child: Container(
        margin: EdgeInsets.all(8),
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xFFDDDDDD)),
            borderRadius: BorderRadius.circular(5)),
        child: Column(
          children: <Widget>[
            Container(
              height: 30,
              width: double.maxFinite,
              color: Color(0xffF0F0F0),
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${dateFormat.format(data.createdAt)}",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff858585),
                    ),
                  ),
                  Text(
                    data.status,
                    style: TextStyle(
                      color: data.status == "Berhasil"
                          ? Color(0xFF0E7E4A)
                          : data.status == "Gagal"
                              ? Color(0xFFBF2D30)
                              : Color(0xFFE5A037),
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                header: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // icon
                          Container(
                            padding: EdgeInsets.only(right: 20),
                            child: Image.asset(
                              data.activity == "Deposit"
                                  ? "assets/icon_journey/deposit.png"
                                  : data.activity == "Dividen"
                                      ? "assets/icon_journey/dividen.png"
                                      : data.activity == "Withdraw"
                                          ? "assets/icon_journey/withdraw.png"
                                          : "assets/icon_journey/perdana.png",
                              width: 35,
                            ),
                          ),
                          // title dan subtitle aktivitas
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  data.activity,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                data.information1 == null
                                    ? Container()
                                    : Text(
                                        data.information1,
                                        style: TextStyle(fontSize: 12),
                                      )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                collapsed: data.activity == "Dividen"
                    ? Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 20),
                            height: 3,
                            width: double.maxFinite,
                            color: Color(0xffF0F0F0),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 0, 0, 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                // tanggal dan status aktivitas
                                Text(
                                  data.activity == "Dividen"
                                      ? "Nominal yang Dicairkan"
                                      : "Total Pembelian",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                data.activity == "Dividen"
                                    ? Text(
                                        data.information4 != null
                                            ? "Rp ${rupiah.format(int.parse(data.information4))}"
                                            : "-",
                                        style: TextStyle(
                                          color: data.status != "Berhasil"
                                              ? Colors.black
                                              : Color(0xFF0E7E4A),
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )
                                    : Text(
                                        data.information3 == null ||
                                                data.information3 == ""
                                            ? ""
                                            : data.information4 == null
                                                ? "Rp ${rupiah.format(int.parse(data.information3))}"
                                                : totalTrxMarket(
                                                    data.activity,
                                                    int.parse(
                                                        data.information3),
                                                    RegExp(r"^[a-zA-Z ]*$")
                                                            .hasMatch(data
                                                                .information4)
                                                        ? 0
                                                        : int.parse(
                                                            data.information4)),
                                        style: TextStyle(
                                          color: data.information4 == null
                                              ? Colors.black
                                              : Color(0xFF0E7E4A),
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                // Text(
                                //   data.status,
                                //   style: TextStyle(
                                //     color: Color(0xFF0E7E4A),
                                //     fontWeight: FontWeight.bold,
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ],
                      )
                    : Container(height: 5),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: _contentExpand(data),
                    ),
                    Container(
                      height: 1,
                      color: Colors.grey[200],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _contentExpand(Datum data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 10,
            bottom: data.activity == "Dividen" ? 0 : 10,
          ),
          height: 3,
          width: double.maxFinite,
          color: Color(0xffF0F0F0),
        ),

        // information 2
        Text(data.activity == "Deposit" || data.activity == "Withdraw"
            ? data.information2 == "DANA"
                ? "Metode Pembayaran"
                : "Nama Bank"
            : data.activity == "Dividen"
                ? ""
                : "Nama Brand"),
        data.activity == "Dividen"
            ? Container()
            : Container(
                margin: EdgeInsets.only(
                  bottom: data.activity == "Dividen" ? 0 : 10,
                ),
                child: Text(
                  data.information2 == null ? "" : data.information2,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),

        // information 3
        data.information3 != null
            ? Text(
                data.activity == "Dividen"
                    ? "Metode Pencairan"
                    : data.activity == "Penjualan Saham Sekunder"
                        ? "Total Penjualan"
                        : data.activity == "Pembelian Saham Sekunder"
                            ? "Total Pembelian"
                            : data.activity == "Pembelian Saham Perdana"
                                ? "Nominal yang Harus Dibayar"
                                : data.activity == "Penjualan Saham"
                                    ? "Jumlah Saham Yang Dijual"
                                    : data.information2 == "DANA"
                                        ? "Nomor Handphone"
                                        : "Nomor Rekening",
              )
            : Container(),
        data.information3 == null
            ? Container()
            : data.activity == "Deposit" ||
                    data.activity == "Withdraw" ||
                    data.activity == "Dividen"
                ? Text(
                    data.information3 == null ? "" : data.information3,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                : Text(
                    data.information3 == null || data.information3 == ""
                        ? ""
                        : data.information4 == null
                            ? "Rp ${rupiah.format(int.parse(data.information3))}"
                            : totalTrxMarket(
                                data.activity,
                                int.parse(data.information3),
                                RegExp(r"^[a-zA-Z ]*$")
                                        .hasMatch(data.information4)
                                    ? 0
                                    : int.parse(data.information4)),
                    style: TextStyle(
                        color: data.information4 == null
                            ? Color(0xFF0E7E4A)
                            : Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold)),
        data.information3 != null ? Container(height: 8) : Container(),
        data.activity == "Deposit" || data.activity == "Withdraw"
            ? Text(data.activity == "Deposit"
                ? "Nominal yang Disetorkan "
                : data.activity == "Deposit"
                    ? "Nominal yang Ditarik "
                    : data.activity == "Dividen"
                        ? ""
                        : "Nominal yang Dicairkan")
            : Container(),

        // information 4
        data.activity == "Deposit" || data.activity == "Withdraw"
            ? Text(
                "Rp ${rupiah.format(data.information4 == null || data.information4 == '' || data.information4.contains('undefined') || RegExp(r"^[a-zA-Z ]*$").hasMatch(data.information4) ? 0 : num.parse(data.information4))}",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF0E7E4A),
                ),
              )
            : Container(),
        // Container(height: 12),

        // Nama Pemilik Akun
        data.activity == "Dividen" ? Text("Nama Pemilik Akun") : Container(),
        data.activity == "Dividen"
            ? Container(
                margin: EdgeInsets.only(bottom: 10),
                child: Text(
                  "${user != null ? user.trader.name : '-'}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : Container(),

        // Nomor Telp. Pemilik Akun
        data.activity == "Dividen"
            ? Text(data.information3 != null && data.information3 == "DANA"
                ? "Nomor Telp. Pemilik Akun"
                : data.information3 != null &&
                        data.information3 == "Rekening Bank"
                    ? "Nomor Rekening"
                    : "")
            : Container(),
        data.activity == "Dividen"
            ? Text(
                data.information3 != null && data.information3 == "DANA"
                    ? "${user != null ? user.phone : '-'}"
                    : data.information3 != null &&
                            data.information3 == "Rekening Bank"
                        ? "${data.information5}"
                        : "",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            : Container(),

        // tanggal dan status aktivitas
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: <Widget>[
        //     Text("${dateFormat.format(data.createdAt)}"),
        //     _textStatus(data.status),
        //   ],
        // ),
        data.activity == "Dividen"
            ? Container(
                margin: EdgeInsets.only(top: 10, bottom: 15),
                height: 3,
                width: double.maxFinite,
                color: Color(0xffF0F0F0),
              )
            : Container(),
        data.activity == "Dividen"
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // tanggal dan status aktivitas
                  Text(
                    data.activity == "Dividen"
                        ? "Nominal yang Dicairkan"
                        : "Total Pembelian",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  data.activity == "Dividen"
                      ? Text(
                          data.information4 != null
                              ? "Rp ${rupiah.format(int.parse(data.information4))}"
                              : "-",
                          style: TextStyle(
                            color: data.status != "Berhasil"
                                ? Colors.black
                                : Color(0xFF0E7E4A),
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      : Text(
                          data.information3 == null || data.information3 == ""
                              ? ""
                              : data.information4 == null
                                  ? "Rp ${rupiah.format(int.parse(data.information3))}"
                                  : totalTrxMarket(
                                      data.activity,
                                      int.parse(data.information3),
                                      RegExp(r"^[a-zA-Z ]*$")
                                              .hasMatch(data.information4)
                                          ? 0
                                          : int.parse(data.information4)),
                          style: TextStyle(
                            color: data.information4 == null
                                ? Colors.black
                                : Color(0xFF0E7E4A),
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                ],
              )
            : Container(),
        SizedBox(height: 5)
      ],
    );
  }

  Widget _textStatus(String status) {
    Color color;
    if (status == null) {
      return Container();
    } else {
      switch (status.toLowerCase()) {
        case "berhasil":
          color = Color(0xFF0E7E4A);
          break;
        case "terverifikasi":
          color = Color(0xFF0E7E4A);
          break;
        case "gagal":
          color = Color(0xFFBF2D30);
          break;
        case "menunggu verifikasi":
          color = Color(0xFFE5A037);
          break;
        case "menunggu verifikasi bank kustodian":
          color = Color(0xFFE5A037);
          break;
        case "pembaruan data":
          color = Color(0xFF666EE8);
          break;
        case "ditolak oleh admin":
          color = Color(0xFFBF2D30);
          break;
        case "ditolak bank kustodian":
          color = Color(0xFFBF2D30);
          break;
        default:
          color = Color(0xFFE5A037);
          break;
      }
      return Text(
        status,
        style: TextStyle(
          color: color,
          fontWeight: FontWeight.bold,
        ),
      );
    }
  }

  Widget _status() {
    return Container(
      margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Status",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
          ),
          Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              _itemStatus("Berhasil"),
              _itemStatus("Gagal"),
              _itemStatus("Menunggu Pembayaran"),
              _itemStatus("Menunggu Verifikasi"),
            ],
          ),
          Container(height: 12)
        ],
      ),
    );
  }

  Widget _kategory() {
    return Container(
      margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Kategori",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
          ),
          Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              _itemKategori("Registrasi Akun"),
              _itemKategori("Penggantian Nomor Handphone"),
              _itemKategori("Penggantian Alamat Email"),
              _itemKategori("Penggantian Password"),
              _itemKategori("KYC"),
              _itemKategori("Pembelian Saham Perdana"),
              _itemKategori("Deposit"),
              _itemKategori("Dividen"),
              _itemKategori("Withdraw"),
              _itemKategori("Penjualan Saham"),
              _itemKategori("Pembelian Saham Sekunder"),
              _itemKategori("Penjualan Saham Sekunder"),
            ],
          )
        ],
      ),
    );
  }

  Widget _itemStatus(String name) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isLoading = true;
          page = 1;
          if (status == name) {
            status = null;
            getDataJourney();
          } else {
            status = name;
            getDataJourney();
          }
        });
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
        margin: EdgeInsets.all(4),
        decoration: status == name
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Color((0xFFF9DBDB)))
            : BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                border: Border.all(color: Color(0xFFDDDDDD))),
        child: Text(name,
            style: TextStyle(
                color: status == name ? Color(0xFFC57172) : Color(0xFF676767),
                fontSize: 14)),
      ),
    );
  }

  Widget _itemKategori(String name) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isLoading = true;
          page = 1;
          if (activity == name) {
            activity = null;
            getDataJourney();
          } else {
            activity = name;
            getDataJourney();
          }
        });
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
        margin: EdgeInsets.all(4),
        decoration: activity == name
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Color((0xFFF9DBDB)))
            : BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                border: Border.all(color: Color(0xFFDDDDDD))),
        child: Text(name,
            style: TextStyle(
                color: activity == name ? Color(0xFFC57172) : Color(0xFF676767),
                fontSize: 14)),
      ),
    );
  }

  Widget _sortby() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        margin: EdgeInsets.fromLTRB(16, 0, 16, 20),
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            color: Colors.white, border: Border.all(color: Color(0xFFD0D0D0))),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            value: sort,
            items: dropDownSortBy,
            onChanged: (selected) {
              setState(() {
                sort = selected;
                isLoading = true;
                page = 1;
                getDataJourney();
              });
            },
          ),
        ));
  }
}
