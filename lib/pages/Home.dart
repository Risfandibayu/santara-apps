import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:santaraapp/pages/bantuanpages.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/home/AllComingsoonList.dart';
import 'package:santaraapp/widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'package:santaraapp/widget/widget/components/market/toast_helper.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../helpers/Constants.dart';
import '../helpers/UserAgent.dart';
import '../models/Notification.dart';
import '../models/Popup.dart';
import '../models/User.dart';
import '../pages/Help.dart';
import '../utils/api.dart';
import '../utils/helper.dart';
import '../utils/logger.dart';
import '../widget/account/AccountUI.dart';
import '../widget/emiten/EmitenUI.dart';
import '../widget/home/AllPenerbitList.dart';
//component
import '../widget/index/Index.dart';
import '../widget/kyc/widgets/pra_kyc.dart';
import '../widget/listing/dashboard/PraListingUI.dart';
import '../widget/transactions/TransactionsUI.dart';
import 'KycDynamicLink.dart';
import 'Login.dart';
import 'Login1.dart';
import 'OTP.dart';
import 'Warning.dart';

class Home extends StatefulWidget {
  final index;
  final int isSuksesDaftarUKM;
  final int isFirstOpen;
  final uuidPage;
  final spesificPage;
  Home(
      {this.index,
      this.isSuksesDaftarUKM,
      this.isFirstOpen,
      this.spesificPage,
      this.uuidPage});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with WidgetsBindingObserver {
  String token;
  var datas;
  var refreshToken;
  var tokenFirebase;
  int isVerified;
  int isOtp;
  // int tnc;
  String newNotifString = "";
  List<Notifications> newNotif;
  List<Notifications> oldNotif = [];
  bool loading = false;
  final TextEditingController email1 = TextEditingController();
  final TextEditingController pass1 = TextEditingController();

  int indexHome = 0;
  final storage = new FlutterSecureStorage();

  DateTime backButtonPressTime;
  static const snackBarDuration = Duration(seconds: 3);

  Future<bool> onWillPop() async {
    DateTime currentTime = DateTime.now();

    bool backButtonHasNotBeenPressedOrSnackBarHasBeenClosed =
        backButtonPressTime == null ||
            currentTime.difference(backButtonPressTime) > snackBarDuration;

    if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
      backButtonPressTime = currentTime;
      Toast.show("Tap sekali lagi untuk keluar", context,
          duration: 3, gravity: Toast.BOTTOM);
      return false;
    }
    return true;
  }

  Widget page(int current) {
    switch (current) {
      case 0:
        return new Index();
      case 1:
        return Helper.check
            ? TransactionsUI()
            : Warning(page: 'transaksi', image: "assets/icon/login_dahulu.png");
      case 2:
        return Helper.check
            // ? Help()
            ? BantuanPage()
            : Warning(page: 'bantuan', image: "assets/icon/login_dahulu.png");
      case 3:
        return Helper.check
            ? AllComngsoonList()
            : Warning(
                page: 'Coming Soon', image: "assets/icon/login_dahulu.png");
      case 4:
        return Helper.check ? AccountUI() : Login1();
      default:
        return Index();
    }
  }

  String waNumber = "6281212227765";
  String waMesssage = "Halo, apakah ada informasi terbaru mengenai Santara?";

  Future getWhatsAppMessage() async {
    final response =
        await http.get('$apiLocal/contacts'); //.timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      Uri uri = Uri.parse(data["whatsapp"]);
      Map map = uri.queryParameters;
      waNumber = map["phone"];
      waMesssage = map["text"];
    }
  }

  void onTab(int index) {
    setState(() {
      indexHome = index;
    });

    // setState(() {
    //   if (index == 2) {
    //     // launchWhatsApp(
    //     //   phone: waNumber,
    //     //   message: waMesssage,
    //     // );
    //     Navigator.push(context, MaterialPageRoute(builder: (context) {
    //       return AllComngsoonList();
    //     }));
    //   } else {
    //     indexHome = index;
    //   }
    // });
  }

  Future getNotification() async {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/notification/', headers: headers);
    if (response.statusCode == 200) {
      storage.write(key: "newNotif", value: response.body);
      final data = userFromJson(await storage.read(key: 'user'));
      await storage.write(key: 'phone', value: data.phone);
      setState(() {
        newNotifString = response.body;
        newNotif = notificationsFromJson(response.body);
        storage.read(key: "oldNotif").then((cache) {
          if (cache != null) {
            oldNotif = notificationsFromJson(cache);
          }
          Helper.notif = newNotif.length - oldNotif.length;
        });
      });
    }
  }

  Future saveKycStatus(int val) async {
    await storage.write(
        key: Constants.statusKyc, value: val == 1 ? 'true' : 'false');
  }

  Future checking() async {
    final token = await storage.read(key: 'token');
    final uuid = await storage.read(key: 'uuid');

    if (Helper.check) {
      getNotification();
      final http.Response response = await http.get(
          '$apiLocal/users/by-uuid/$uuid',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      final data = userFromJson(response.body);
      if (data.isOtp == 0) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => OTP(phone: data.phone)),
            (Route<dynamic> route) => false);
      } else if (data.trader == null) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => PraKyc(isEmpty: 1)),
            (Route<dynamic> route) => false);
      } else {
        await saveKycStatus(data.trader.isVerified);
        setState(() {
          isVerified = data.trader.isVerified;
          Helper.username = data.trader.name;
          Helper.email = data.email;
        });
      }
    } else {
      setState(() {
        Helper.notif = 0;
      });
      // print('nothing');
    }
  }

  void showModalSuksesDaftarEmiten() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width / 3 + 50,
                  width: MediaQuery.of(context).size.width / 3 + 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          MediaQuery.of(context).size.width / 3),
                      image: DecorationImage(
                          image: AssetImage('assets/icon/success.png'))),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Center(
                    child: Text(
                        'Data anda telah kami terima, tim kami akan segera mengulas data anda, kami akan segera menginformasikan kepada anda setelah data ditindak lanjuti',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, color: Colors.black)),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(8),
                  child: Center(
                    child: Text('Terima kasih.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Container(
                    height: 40,
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(6)),
                    child: Center(
                        child: Text("Mengerti",
                            style: TextStyle(color: Colors.white))),
                  ),
                ),
                Container(height: 12),
              ],
            ),
          );
        });
  }

  void showModalSessionTimedOut() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            color: Color(ColorRev.mainBlack),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width / 3 + 50,
                  width: MediaQuery.of(context).size.width / 3 + 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          MediaQuery.of(context).size.width / 3),
                      image: DecorationImage(
                          image: AssetImage("timeout.png"),
                          fit: BoxFit.fitWidth)),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                  child: Center(
                    child: Text(
                        'Periode waktu kamu telah\nkadaluarsa, silahkan login kembali',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Color(ColorRev.mainwhite))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 16),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    tokenFirebase = await storage.read(key: "tokenFirebase");
    if (token != null) {
      // getUser(token);
      datas = userFromJson(await storage.read(key: 'user'));
      refreshToken = await storage.read(key: "refreshToken");
      if (widget.spesificPage == 2) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PralistingDetailPage(
              uuid: widget.uuidPage,
              status: 2,
              position: null,
            ),
          ),
        );
      }
    } else {
      if (widget.spesificPage == 1) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EmitenUI(
              uuid: widget.uuidPage,
            ),
          ),
        );
      } else if (widget.spesificPage == 3) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => AllPenerbitList()));
      } else if (widget.spesificPage == 4) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Login(isFromDynamicLink: 1)));
      } else if (widget.spesificPage == 5) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => KycDynamicLinkUI(),
          ),
        );
      }
    }
  }

  Future getDataPopup() async {
    final http.Response response =
        await http.get('$apiLocal/information/pop-up');
    if (response.statusCode == 200) {
      var data = popupFromJson(response.body);
      if (data.length > 0) {
        if (data[0].type.toUpperCase() == "ONETIME") {
          if (await storage.read(key: "popup") == null) {
            showPopup(data[0]);
            await storage.write(key: "popup", value: data[0].uuid);
          } else {
            var uuid = await storage.read(key: "popup");
            List<String> arrayUuid = uuid.split(",");
            if (!arrayUuid.contains(data[0].uuid)) {
              showPopup(data[0]);
              await storage.write(
                  key: "popup", value: uuid + "," + data[0].uuid);
            }
          }
        } else {
          showPopup(data[0]);
        }
      }
    }
  }

  void showPopup(Popup popup) {
    if (widget.isFirstOpen == 0) {
      Future.delayed(Duration(seconds: 1), () {
        Helper.dialogPopupEvent(context, popup);
      });
    }
  }

  void showPopupRate() {
    Future.delayed(Duration(seconds: 1), () async {
      if (await storage.read(key: "isShowRate") == null) {
        Helper.dialogRateApp(context);
      } else {
        int isShowRate = int.parse(await storage.read(key: "isShowRate"));
        int timeInMilis = DateTime.now().millisecondsSinceEpoch;

        if (isShowRate != 0) {
          if (await storage.read(key: "timeShowRate") == null) {
            Helper.dialogRateApp(context);
          } else {
            int timeShowRate =
                int.parse(await storage.read(key: "timeShowRate"));
            if (timeInMilis >= timeShowRate) {
              Helper.dialogRateApp(context);
            }
          }
        }
      }
    });
  }

  Future getDataPopupUpdate() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    final currentVersion = info.version.split(".");
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.fetch(expiration: Duration(seconds: 0));
    await remoteConfig.activateFetched();
    final isMandatory = remoteConfig.getBool(isUpdateMandatoryAndroid);
    final isMandatoryIOS = remoteConfig.getBool(isUpdateMandatoryIos);
    final newVersion = remoteConfig.getString(version).split(".");
    final newVersionIOS = remoteConfig.getString(versionIOS).split(".");

    if (Platform.isAndroid) {
      var versionBool = [];
      for (var i = 0; i < currentVersion.length; i++) {
        if (num.parse(currentVersion[i]) > num.parse(newVersion[i])) {
          // kalo lebih besar maka tambah sebuah value bernilai true dan stop looping
          i = currentVersion.length;
          versionBool.add(true);
        } else if (num.parse(currentVersion[i]) < num.parse(newVersion[i])) {
          // kalo LEBIH KECIL maka tambah sebuah value bernilai false
          versionBool.add(false);
        } else {
          // kalo SAMA DENGAN maka tambah sebuah value bernilai true dan check index selanjutnya
          versionBool.add(true);
        }
      }
      // jika array versionBool memiliki value false maka show popup update
      if (versionBool.contains(false)) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return WillPopScope(
                onWillPop: () {
                  if (isMandatory) {
                    return SystemChannels.platform
                        .invokeMethod('SystemNavigator.pop');
                  } else {
                    return null;
                  }
                },
                child: AlertDialog(
                    backgroundColor: Color(ColorRev.mainBlack),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Image.asset('assets/icon/updateupdate.png'),
                            isMandatory
                                ? Container()
                                : Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                      icon: Icon(Icons.close,
                                          color: Colors.white),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                  )
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Text('Hello !',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Color(ColorRev.mainRed)))),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            'Update aplikasi versi terbaru sudah tersedia',
                            style: TextStyle(color: Color(ColorRev.mainwhite)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                              'Untuk dapat mengakses aplikasi, mohon update aplikasi terlebih dahulu',
                              style:
                                  TextStyle(color: Color(ColorRev.mainwhite)),
                              textAlign: TextAlign.center),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width / 1,
                          child: FlatButton(
                            onPressed: () async {
                              const url =
                                  "https://play.google.com/store/apps/details?id=id.co.santara.app";
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Cannot launch $url';
                              }
                            },
                            color: Color(ColorRev.maingreen),
                            child: Text(
                              'Update',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    )),
              );
            });
      }
    } else if (Platform.isIOS) {
      var versionBool = [];
      for (var i = 0; i < currentVersion.length; i++) {
        if (num.parse(currentVersion[i]) > num.parse(newVersionIOS[i])) {
          // kalo lebih besar maka tambah sebuah value bernilai true dan stop looping
          i = currentVersion.length;
          versionBool.add(true);
        } else if (num.parse(currentVersion[i]) < num.parse(newVersionIOS[i])) {
          // kalo LEBIH KECIL maka tambah sebuah value bernilai false
          versionBool.add(false);
        } else {
          // kalo SAMA DENGAN maka tambah sebuah value bernilai true dan check index selanjutnya
          versionBool.add(true);
        }
      }
      // jika array versionBool memiliki value false maka show popup update
      if (versionBool.contains(false)) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return WillPopScope(
                onWillPop: () {
                  if (isMandatoryIOS) {
                    return SystemChannels.platform
                        .invokeMethod('SystemNavigator.pop');
                  } else {
                    return null;
                  }
                },
                child: AlertDialog(
                    backgroundColor: Color(ColorRev.mainBlack),
                    actions: [],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Image.asset('assets/icon/update.png'),
                            isMandatoryIOS
                                ? Container()
                                : Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                      icon: Icon(Icons.close,
                                          color: Colors.white),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                  )
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Text('Hello !',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Color(ColorRev.mainwhite)))),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            'Update aplikasi versi terbaru sudah tersedia',
                            style: TextStyle(color: Color(ColorRev.mainwhite)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                              'Untuk dapat mengakses aplikasi, mohon update aplikasi terlebih dahulu',
                              style:
                                  TextStyle(color: Color(ColorRev.mainwhite)),
                              textAlign: TextAlign.center),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width / 1,
                          child: FlatButton(
                            onPressed: () async {
                              const url =
                                  "https://apps.apple.com/id/app/santara-app/id1473570177";
                              if (await canLaunch(url)) {
                                await launch(Uri.encodeFull(url));
                              } else {
                                throw 'Cannot launch $url';
                              }
                            },
                            color: Color(ColorRev.maingreen),
                            child: Text(
                              'Update',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    )),
              );
            });
      }
    }
  }

  openWhatsApp() async {
    var whatsAppUrl =
        "whatsapp://send?phone=+6281212227765&text=Halo, apakah ada informasi terbaru mengenai Santara?";
    if (Platform.isIOS) {
      if (await canLaunch('whatsapp://')) {
        await launch(whatsAppUrl, forceSafariVC: false);
      } else {
        await launch(whatsAppUrl, forceSafariVC: true);
      }
    } else {
      await canLaunch(whatsAppUrl)
          ? launch(whatsAppUrl)
          : Scaffold.of(context).showSnackBar(
              SnackBar(
                content: new Text("You need WhatsApp to access Sara chatbot"),
              ),
            );
    }
  }

  void launchWhatsApp({
    @required String phone,
    @required String message,
  }) async {
    try {
      String whatsAppUrl =
          "whatsapp://send?phone=$phone&text=${Uri.parse(message)}";
      if (Platform.isIOS) {
        await launch(whatsAppUrl, forceSafariVC: false);
      } else {
        await canLaunch(whatsAppUrl)
            ? launch(whatsAppUrl)
            : Toast.show(
                "Tidak dapat membuka whatsapp",
                context,
                duration: 3,
                gravity: Toast.BOTTOM,
              );
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      Toast.show(
        "Tidak dapat membuka WhatsApp",
        context,
        duration: 3,
        gravity: Toast.BOTTOM,
      );
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // user returned to our app
      Future.delayed(Duration(seconds: 1), () {
        getDataPopupUpdate();
      });
    } else if (state == AppLifecycleState.inactive) {
      // app is inactive
    } else if (state == AppLifecycleState.paused) {
      // user is about quit our app temporally
    } else if (state == AppLifecycleState.detached) {
      // app is still hosted on a flutter engine but is detached from any host views
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getWhatsAppMessage();
    if (widget.index != null) {
      indexHome = widget.index;
    }
    if (widget.isSuksesDaftarUKM == 1) {
      Future.delayed(Duration(milliseconds: 100), () {
        showModalSuksesDaftarEmiten();
      });
    } else if (widget.isSuksesDaftarUKM == 2) {
      Future.delayed(Duration(microseconds: 100), () {
        showModalSessionTimedOut();
      });
    }
    getLocalStorage();
    if (widget.isFirstOpen == 0) {
      Future.delayed(Duration(seconds: 1), () {
        getDataPopupUpdate();
        showPopupRate();
      });
    }
    getDataPopup();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    checking();
    getLocalStorage();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onWillPop,
        child: Scaffold(
            //Content Body
            body: page(indexHome),

            //Bottom Navigation Bar
            bottomNavigationBar: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Colors.white, width: 0.5),
                ),
              ),
              child: BottomNavigationBar(
                onTap: onTab,
                currentIndex: indexHome,
                type: BottomNavigationBarType.fixed,
                fixedColor: Colors.red,
                backgroundColor: Color(ColorRev.mainBlack),
                unselectedItemColor: Colors.white,
                items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      activeIcon: SizedBox(
                          height: 20, width: 20, child: Icon(Icons.home)),
                      icon: SizedBox(
                          height: 20, width: 20, child: Icon(Icons.home)),
                      title: Text(
                        'Home',
                        key: Key('tabHome'),
                      )),
                  BottomNavigationBarItem(
                      activeIcon: SizedBox(
                          key: Key('wallet-active'),
                          height: 20,
                          width: 20,
                          child: Icon(Icons.account_balance_wallet_rounded)),
                      icon: SizedBox(
                          key: Key('wallet-deactive'),
                          height: 20,
                          width: 20,
                          child: Icon(Icons.account_balance_wallet_rounded)),
                      // icon: Image.asset("assets/icon/transaksi.png",
                      //       color: indexHome == 1 ? Colors.red : null),
                      title: Text('Transaksi',
                          key: Key('tabTransaksi'),
                          style: TextStyle(
                              color:
                                  indexHome == 1 ? Colors.red : Colors.white))),
                  BottomNavigationBarItem(
                      activeIcon: SizedBox(
                          key: Key('help-active'),
                          height: 20,
                          width: 20,
                          child: Icon(Icons.help_outline)),
                      icon: SizedBox(
                          key: Key('help-deactive'),
                          height: 20,
                          width: 20,
                          child: Icon(Icons.help_outline)),
                      // icon: Icon(MdiIcons.helpCircleOutline),
                      title: Text(
                        'Bantuan',
                        key: Key('tabBantuan'),
                      )),
                  BottomNavigationBarItem(
                      activeIcon: SizedBox(
                          key: Key('pralisting-active'),
                          height: 18,
                          width: 18,
                          child: Icon(Icons.list)),
                      icon: SizedBox(
                          key: Key('pralisting-deactive'),
                          height: 20,
                          width: 20,
                          child: Icon(Icons.list)),
                      // icon: Image.asset("assets/icon/video.png",
                      //     color: indexHome == 3 ? Colors.red : null),
                      title: Text('Coming Soon',
                          key: Key('tabPralisting'),
                          style: TextStyle(
                              color: indexHome == 3 ? Colors.red : Colors.white,
                              fontSize: 11))),
                  BottomNavigationBarItem(
                    // icon: Icon(MdiIcons.accountOutline),
                    activeIcon: SizedBox(
                        key: Key('profile-active'),
                        height: 20,
                        width: 20,
                        child: Helper.check
                            ? Image.asset(
                                "assets/icon_bottom/profil_selected.png")
                            : Icon(Icons.person_rounded, color: Colors.red)),
                    icon: SizedBox(
                        key: Key('profile-deactive'),
                        height: 20,
                        width: 20,
                        child: Helper.check
                            ? Image.asset(
                                "assets/icon_bottom/profil_unselected.png")
                            : Icon(Icons.person_rounded)),
                    title: Text(
                      Helper.check ? 'Profil' : 'Masuk',
                      key: Key('tabAkun'),
                    ),
                  ),
                ],
              ),
            )

            //End Bottom Navigation Bar
            ));
  }
}
