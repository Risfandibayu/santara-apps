import 'package:flutter/material.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/widget/Appbar.dart';
import 'package:santaraapp/widget/widget/Drawer.dart';

class Warning extends StatefulWidget {
  final String page;
  final String image;
  Warning({this.page, this.image});

  @override
  _WarningState createState() => _WarningState();
}

class _WarningState extends State<Warning> {
  final GlobalKey<ScaffoldState> _drawerKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        appBar: widget.page == "notifikasi"
            ? null
            : CustomAppbar(drawerKey: _drawerKey),
        drawer: widget.page == "notifikasi" ? null : CustomDrawer(),
        body: Container(
          color: Color(ColorRev.mainBlack),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Image.asset(
                  widget.image,
                  height: MediaQuery.of(context).size.height / 2,
                  fit: BoxFit.fill,
                ),
              ),
              Text('Login dahulu',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(ColorRev.mainwhite))),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
                child: Text(
                  'Anda harus login terlebih dahulu untuk mengakses halaman ${widget.page}',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Color(ColorRev.mainwhite)),
                ),
              )
            ],
          ),
        ));
  }
}
