class KycValidatorHelper {
  // waktu delay validasi
  static int debounceDuration = 200;

  // Nama lengkap investor
  static Future<String> namaLengkap(String nama) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (nama.contains(RegExp(r'[0-9]'), 1)) {
      return "Nama tidak boleh mengandung angka";
    } else if (nama.length > 420) {
      return "Nama lengkap maksimal 420 karakter";
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(nama)) {
        return null;
      } else {
        return "Nama tidak boleh mengandung simbol";
      }
    }
  }

  // Email
  static Future<String> email(String email) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(email)) {
      return "Format email salah!";
    } else if (email.length > 55) {
      return "Maksimal email berjumlah 55 karakter!";
    } else {
      return null;
    }
  }

  // nomor telepon
  static Future<String> telepon(String telepon) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (telepon.length > 25) {
      return "Nomor telepon maksimal 25 karakter!";
    } else if (telepon[0] == "0") {
      return "Nomor telepon tidak boleh diawali dengan 0";
    } else if (!RegExp(r"^[0-9+#-]*$").hasMatch(telepon)) {
      return "Nomor telepon harus berisi angka!";
    } else {
      return null;
    }
  }

  // nik
  static Future<String> nik(String nik) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (nik.length < 15) {
      return "NIK minimal 15 karakter!";
    } else if (nik.length > 25) {
      return "NIK maksimal 25 karakter!";
    } else if (!RegExp(r"^[0-9]*$").hasMatch(nik)) {
      return "NIK harus berisi angka!";
    } else {
      return null;
    }
  }

  // nomor paspor
  static Future<String> paspor(String paspor) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (paspor.length > 50) {
      return "Paspor maksimal 50 karakter!";
    } else {
      return null;
    }
  }
}
