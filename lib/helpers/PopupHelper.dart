import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/utils/sizes.dart';
import 'package:santaraapp/widget/emiten/blocs/emiten.bloc.dart';
import 'package:santaraapp/widget/security_token/user/SuccessChangeUI.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraAlertDialog.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraButtons.dart';
import 'SizeConfig.dart';

class PopupHelper {
  static showLoading(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async {
              // Navigator.pop(context);
              return false;
            },
            child: AlertDialog(
              backgroundColor: Colors.transparent,
              elevation: 0,
              content: Container(
                child: CupertinoActivityIndicator(),
              ),
            ),
          );
        });
  }

  static void showInfo(BuildContext context, String title, String info) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              title: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 16, 8, 8),
                    child: Text(title,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 16),
                    child: Text(info,
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(4)),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text("Mengerti",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ),
                      ),
                    ),
                  )
                ],
              ));
        });
  }

  static void warningInfo(BuildContext context, String info) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              title: Column(
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8, 16, 8, 8),
                      child: Image.asset(
                        "assets/icon/forbidden.png",
                        width: MediaQuery.of(context).size.width / 3,
                        height: MediaQuery.of(context).size.width / 3,
                      )),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 16),
                    child: Text(info,
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(4)),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text("Mengerti",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ),
                      ),
                    ),
                  )
                ],
              ));
        });
  }

  static handleClosePage(BuildContext context) {
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
        title: "Perubahan Data Belum Disimpan",
        description:
            "Data yang belum disimpan akan hilang dan tidak dapat dikembalikan. Apakah Anda yakin ingin melanjutkan ?",
        height: 220,
        onYakin: () {
          Navigator.pop(context, 'x');
          Navigator.pop(context, 'x');
        },
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static handleCloseKyc(
      BuildContext context, VoidCallback onYes, VoidCallback onCancel) {
    // set up the AlertDialog
    var alert = SantaraAlertDialog(
      title: "Data Belum Disimpan",
      description:
          "Data yang belum disimpan akan hilang dan tidak dapat dikembalikan. Apakah Anda yakin ingin melanjutkan ?",
      height: 220,
      onYakin: onYes,
      onBatal: onCancel,
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static handleConfirmation(BuildContext context, VoidCallback onYakin) {
    var alert = SantaraAlertDialog(
        title: "Konfirmasi Perubahan",
        description: "Apakah Anda yakin ingin memperbarui data ?",
        height: 150,
        onYakin: onYakin,
        onBatal: () {
          Navigator.pop(context);
        });

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static popConfirmation(
    BuildContext context,
    VoidCallback onYakin,
    String title,
    String message, {
    String textCancel,
    String textConfirm,
  }) {
    var alert = SantaraAlertDialog(
      title: "$title",
      description: "$message",
      height: 150,
      onYakin: onYakin,
      textCancel: textCancel,
      textConfirm: textConfirm,
      onBatal: () {
        Navigator.pop(context);
      },
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static pratinjauKycConfirmation(
      BuildContext context, VoidCallback onLihat, VoidCallback onEdit) {
    var alert = AlertDialog(
      contentPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      content: Container(
        color: Colors.black, // height: height,
        width: double.infinity,
        child: Column(
          // shrinkWrap: true,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ),
            Text(
              "Pratinjau Data KYC",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: FontSize.s15,
                  color: Colors.white),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              width: double.maxFinite,
              height: Sizes.s20,
            ),
            Padding(
              padding: EdgeInsets.only(
                left: Sizes.s10,
                right: Sizes.s10,
              ),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: SantaraOutlineButton(
                          title: "Lihat KYC",
                          color: Colors.white,
                          onPressed: onLihat,
                        ),
                      ),
                    ),
                    Container(
                      width: 15,
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: SantaraMainButton(
                          title: "Edit KYC",
                          onPressed: onEdit,
                        ),
                      ),
                    )
                  ]),
            ),
            SizedBox(
              width: double.maxFinite,
              height: Sizes.s40,
            ),
          ],
        ),
      ),
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // Sukses ubah password ( Security Token )
  static successChangeDialog(BuildContext context) {
    Dialog dialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 250.0,
        width: 300.0,
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 250,
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.bottomCenter,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                  // color: Colors.blue[300],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  ),
                ),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          "assets/icon/check-circle.png",
                          height: 100,
                        ),
                        Container(
                          height: 10,
                        ),
                        Text(
                          "PIN Berhasil Diubah",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    )),
              ),
            ),
            Align(
              alignment: Alignment(.9, -.9),
              child: InkWell(
                onTap: () {
                  // print(">> HELLO");
                  // Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SuccessChangeUI(
                        message: "PIN Berhasil Diubah",
                      ),
                    ),
                  );
                },
                child: Container(
                  child: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async {
              // Navigator.pop(context);
              return false;
            },
            child: dialog,
          );
        });
  }

  // Popup message
  static showMessage(BuildContext context, String message,
      {TextAlign textAlign}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: height ?? 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(12),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 12,
                          ),
                          Center(
                            child: Text(
                              "$message",
                              textAlign: textAlign ?? TextAlign.start,
                              style: TextStyle(
                                  color: Color(0xff676767), fontSize: 12),
                            ),
                          ),
                          Container(
                            height: 12,
                          ),
                          Container(
                            height: 42,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  // Popup message
  static showMessageWithoutButton(BuildContext context, String message,
      {TextAlign textAlign}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: height ?? 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Text(
                              "$message",
                              textAlign: textAlign ?? TextAlign.start,
                              style: TextStyle(
                                  color: Color(0xff676767),
                                  fontSize: _size * 3),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  static Widget _sahamTile(String title, String subtitle) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "$title",
            style: TextStyle(fontSize: 14),
          ),
          Text(
            "$subtitle",
            style: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  // Popup kepemilikan saham
  static showKepemilikanSaham(BuildContext context, String batas,
      String dimiliki, String sisa, int isUnlimited) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          // var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(Sizes.s14),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: Sizes.s8,
                          ),
                          Center(
                            child: Text(
                              "Batas Kepemilikan Saham Anda",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s16,
                              ),
                            ),
                          ),
                          Container(
                            height: Sizes.s8,
                          ),
                          Text(
                            "Berdasarkan Peraturan POJK 57-2020 PASAL 56 Ayat 3, Dengan Rincian",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff676767),
                              fontSize: FontSize.s14,
                            ),
                          ),
                          Container(
                            height: Sizes.s8,
                          ),
                          _sahamTile(
                            "Batas Kepemilikan Saham",
                            batas != null
                                ? isUnlimited == 1
                                    ? "Unlimited"
                                    : "$batas"
                                : "-",
                          ),
                          _sahamTile(
                            "Saham yang sudah dimiliki",
                            dimiliki != null ? "$dimiliki" : "-",
                          ),
                          _sahamTile(
                            "Sisa batas kepemilikan saham",
                            sisa != null
                                ? isUnlimited == 1
                                    ? "Unlimited"
                                    : "$sisa"
                                : "-",
                          ),
                          Text(
                            "Untuk menambah batas kepemilikan saham, anda dapat memperbaharui informasi pendapatan pertahun anda pada halaman pengaturan, atau dengan mengunggah bukti kepemilikan rekening efek pada halaman pengaturan",
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          Container(
                            height: Sizes.s8,
                          ),
                          Container(
                            height: Sizes.s45,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  // Global popup (title & subtitle)
  static showPopMessage(BuildContext context, String title, String subtitle,
      {TextStyle textStyle, TextAlign textAlign}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: _size * 4,
                          ),
                          Center(
                            child: Text(
                              "$title",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: _size * 4.5,
                              ),
                            ),
                          ),
                          Container(
                            height: _size * 4,
                          ),
                          Text(
                            "$subtitle",
                            style: textStyle == null
                                ? TextStyle(
                                    fontSize: 14,
                                  )
                                : textStyle,
                            textAlign:
                                textAlign == null ? TextAlign.start : textAlign,
                          ),
                          Container(
                            height: _size * 8,
                          ),
                          Container(
                            height: _size * 8.5,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  // Popup jika user tap disabled field
  static showDataChange(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: _size * 4,
                          ),
                          Center(
                            child: Text(
                              "Perubahan Data",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: _size * 4.5,
                              ),
                            ),
                          ),
                          Container(
                            height: _size * 4,
                          ),
                          Text(
                            "Anda dapat mengubah data ini pada halaman keamanan di pengaturan",
                            style: TextStyle(fontSize: 14),
                            textAlign: TextAlign.center,
                          ),
                          Container(
                            height: _size * 8,
                          ),
                          Container(
                            height: _size * 8.5,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  static showInfoPajak(BuildContext context, Widget message) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
            elevation: 0,
            contentPadding: EdgeInsets.all(0),
            content: message,
          );
        });
  }

  // konfirmasi perubahan data
  // kyc baru
  static showConfirmationDataChange(BuildContext context, Function onYakin) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Center(
                            child: Text(
                              "Konfirmasi Perubahan Data",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s20,
                              ),
                            ),
                          ),
                          Container(
                            height: _size * 4,
                          ),
                          Text(
                            "Apakah Anda yakin ingin memperbarui data ? Ketentuan perubahan data : ",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Container(
                            height: Sizes.s10,
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(Sizes.s15),
                            height: Sizes.s110,
                            decoration: BoxDecoration(
                              color: Color(0xffF9F9F9),
                              borderRadius: BorderRadius.all(
                                Radius.circular(Sizes.s10),
                              ),
                              border: Border.all(
                                width: 1,
                                color: Color(0xffB8B8B8),
                              ),
                            ),
                            child: Scrollbar(
                              child: SingleChildScrollView(
                                child: Text(
                                  "1. Data yang belum diverifikasi tidak dapat melakukan aktifitas transaksi \n2. Proses verifikasi Maks. 2x24 Jam (hari kerja) \n3. Setiap perubahan data, tidak dapat dirubah kembali selama 30 Hari terhitung tanggal terakhir perubahan data.",
                                  style: TextStyle(
                                    fontSize: FontSize.s12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: Sizes.s40,
                                width: Sizes.s120,
                                child: SantaraOutlineButton(
                                  title: "Batal",
                                  onPressed: () => Navigator.pop(context),
                                ),
                              ),
                              Container(
                                height: Sizes.s40,
                                width: Sizes.s120,
                                child: SantaraMainButton(
                                  title: "Yakin",
                                  onPressed: onYakin,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  // batas waktu edit kyc
  static showTimeLimitEditKyc(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          var _size = SizeConfig.safeBlockHorizontal;
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              elevation: 0,
              contentPadding: EdgeInsets.all(0),
              content: Container(
                // height: 350,
                width: double.infinity,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(_size * 4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Text(
                              "Batas Waktu Perubahan Data",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: FontSize.s20,
                              ),
                            ),
                          ),
                          Container(
                            height: _size * 4,
                          ),
                          Text(
                            "Ketentuan perubahan data Santara, Setiap perubahan data, tidak dapat dirubah kembali selama 30 Hari terhitung tanggal terakhir perubahan data.\n\nAnda dapat merubah data ini pada :",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Container(
                            height: Sizes.s10,
                          ),
                          Text(
                            "$message",
                            style: TextStyle(
                              fontSize: FontSize.s14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: Sizes.s20,
                          ),
                          Container(
                            height: _size * 8.5,
                            child: SantaraMainButton(
                              title: "Mengerti",
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ));
        });
  }

  // show syarat dan ketentuan pembelian saham
  static void showModalSyaratDanKetentuan(
      BuildContext context, int invest, int harga, EmitenLoaded data) {
    final rupiah = NumberFormat("#,##0");
    final bool isUnlimited = data?.userData?.job?.isUnlimitedInvest != null &&
            data.userData.job.isUnlimitedInvest == 1
        ? true
        : false;
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(Sizes.s15),
            topRight: Radius.circular(Sizes.s15),
          ),
        ),
        builder: (builder) {
          return Container(
            color: Color(ColorRev.mainBlack),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(height: Sizes.s20),
                Container(
                  child: Text(
                    "Syarat dan ketentuan pembelian saham",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: FontSize.s15,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(height: Sizes.s20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Sizes.s20),
                  child: Center(
                    child: Text(
                      'Pembelian saham harus mengikuti ketentuan sebagai berikut :',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Container(height: Sizes.s15),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Sizes.s25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Minimal pembelian saham",
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "${invest < 0 ? 0 : invest} Lembar",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      Container(height: 6),
                      Text("Harga saham perlembar",
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.visible),
                      Text(
                        "Rp. ${rupiah.format(harga)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      Container(height: 20),
                      Text(
                        "Total saham yang dimiliki pada platform Santara",
                        style: TextStyle(color: Colors.white),
                        overflow: TextOverflow.visible,
                      ),
                      Text(
                        "Rp. ${rupiah.format(data.sahamDimiliki)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      Container(height: 6),
                      Text(
                        "Batas kepemilikan saham",
                        style: TextStyle(color: Colors.white),
                        overflow: TextOverflow.visible,
                      ),
                      Text(
                        data.traderType == 2
                            ? "Unlimited"
                            : data.userData != null
                                ? isUnlimited
                                    ? "Unlimited"
                                    : "Rp. ${rupiah.format(data.maksInvest)}"
                                : "Rp. ${rupiah.format(data.maksInvest)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      Container(height: 6),
                      Text(
                        "Sisa batas kepemilikan saham",
                        overflow: TextOverflow.visible,
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        data.traderType == 2
                            ? "Unlimited"
                            : isUnlimited
                                ? "Unlimited"
                                : "Rp. ${rupiah.format(data.sisaInvest)}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                Container(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Color(0xFFBF2D30),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Center(
                        child: Text(
                          "Mengerti",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(height: 20),
              ],
            ),
          );
        });
  }

  // show modal akun belum diverifikasi
  static void showModalUnverified(context, String msg) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(4),
                  height: 4,
                  width: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.grey),
                ),
                Container(
                  height: 60,
                  child: Center(
                    child: Text('Pembelian gagal dilakukan',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.black)),
                  ),
                ),
                Text(
                  msg,
                  style: TextStyle(fontSize: 15),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 50,
                      width: double.maxFinite,
                      color: Color(0xFFBF2D30),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  // show modal error saham lebih
  static void showModalErrorSahamLebih(context, EmitenLoaded data) {
    final rupiah = NumberFormat("#,##0");
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(height: 20),
                Container(
                  child: Text(
                      "Pembelian saham Anda melebihi batas peraturan kepemilikan saham",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.black)),
                ),
                Container(height: 15),
                Container(height: 1, color: Color(0xFFC4C4C4)),
                Container(height: 15),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Total saham yang dimiliki pada platform Santara",
                          overflow: TextOverflow.visible),
                      Text("Rp. ${rupiah.format(data.sahamDimiliki)}",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Container(height: 10),
                      Text("Batas kepemilikan saham",
                          overflow: TextOverflow.visible),
                      Text(
                          data.traderType == 2
                              ? "Unlimited"
                              : "Rp. ${rupiah.format(data.maksInvest)}",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Container(height: 10),
                      Text("Sisa batas kepemilikan saham",
                          overflow: TextOverflow.visible),
                      Text(
                          data.traderType == 2
                              ? "Unlimited"
                              : "Rp. ${rupiah.format(data.sisaInvest)}",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Container(height: 10),
                      Text("Total saham yang akan dibeli",
                          overflow: TextOverflow.visible),
                      Text(
                          "Rp. ${rupiah.format(data.totalHarga == null ? 0 : data.totalHarga)}",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Container(height: 10),
                      Text("Kelebihan pembelian saham",
                          overflow: TextOverflow.visible),
                      Text(
                          "Rp. ${rupiah.format((data.totalHarga == null ? 0 : data.totalHarga) - data.sisaInvest)}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFBF2D30))),
                    ],
                  ),
                ),
                Container(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () => Navigator.pop(context),
                    child: Container(
                      height: 40,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                          color: Color(0xFFBF2D30),
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text("Mengerti",
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                ),
                Container(height: 20),
              ],
            ),
          );
        });
  }

  // show konfirmasi pembyaran
  static void showModalKonfirmasi(
      context,
      String name,
      String codeEmiten,
      int harga,
      String picture,
      int totalHarga,
      TextEditingController controller,
      Function onBeli) {
    final rupiah = NumberFormat("#,##0");
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        builder: (builder) {
          return Container(
            color: Color(ColorRev.mainBlack),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(height: 20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Konfirmasi Pembelian",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Saham",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Text(
                        " :  ",
                        style: TextStyle(color: Colors.white),
                      ),
                      Expanded(
                        child:
                            Text(name, style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text("Kode saham",
                            style: TextStyle(color: Colors.white)),
                      ),
                      Text(" :  ", style: TextStyle(color: Colors.white)),
                      Expanded(
                        child: Text(codeEmiten,
                            style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text("Harga saham",
                            style: TextStyle(color: Colors.white)),
                      ),
                      Text(" :  ", style: TextStyle(color: Colors.white)),
                      Expanded(
                        child: Text("Rp. ${rupiah.format(harga)}",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          child: Text("Jumlah saham",
                              style: TextStyle(color: Colors.white))),
                      Text(" :  ", style: TextStyle(color: Colors.white)),
                      Expanded(
                        child: Text(
                            rupiah.format(
                                  num.parse(
                                    controller.text.replaceAll(
                                      ",",
                                      "",
                                    ),
                                  ),
                                ) +
                                " Lembar",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Text("Total",
                            style: TextStyle(color: Colors.white)),
                      ),
                      Text(" :  ", style: TextStyle(color: Colors.white)),
                      Expanded(
                        child: Text("Rp. ${rupiah.format(totalHarga)}",
                            style: TextStyle(color: Colors.white)),
                      ),
                    ],
                  ),
                ),
                Container(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          key: Key('batalBeliSaham'),
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            height: 40,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                color: Color(0xFFBF2D30),
                              ),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Center(
                              child: Text(
                                "Batal",
                                style: TextStyle(
                                  color: Color(0xFFBF2D30),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(width: 12),
                      Expanded(
                        child: InkWell(
                          key: Key('beliSaham'),
                          onTap: onBeli,
                          child: Container(
                            height: 40,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                                color: Color(0xFFBF2D30),
                                borderRadius: BorderRadius.circular(6)),
                            child: Center(
                              child: Text(
                                "Beli",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(height: 20),
              ],
            ),
          );
        });
  }
}
