// MODEL Untuk status laporan keuangan
import 'package:flutter/material.dart';

class FinancialStatementsStatusModel {
  final String title;
  final String subtitle;
  final Color color;
  FinancialStatementsStatusModel({this.title, this.subtitle, this.color});
}

// Type 0 = Laporan Keuangan
// Type 1 = Rencana Penggunaan Dana
FinancialStatementsStatusModel parseStatus(
    String status, String deadline, int type) {
  var report = type == 0 ? "Laporan keuangan" : "Rencana Penggunaan Dana";

  switch (status) {
    // orange
    case "empty":
      return FinancialStatementsStatusModel(
        title: "Anda belum membuat ${report.toLowerCase()} untuk bulan ini",
        subtitle:
            "Segera buat ${report.toLowerCase()} Anda sebelum tanggal $deadline.",
        color: Color(0xffFDE9CD),
      );
      break;
    // orange
    case "verifying":
      return FinancialStatementsStatusModel(
        title: "$report anda sedang dalam tahap verifikasi.",
        subtitle: "",
        color: Color(0xffFDE9CD),
      );
      break;
    // red
    case "rejected":
      return FinancialStatementsStatusModel(
        title: "$report anda ditolak.",
        subtitle: "Mohon perbaiki kembali sebelum mengirim kembali.",
        color: Color(0xffFFE2DF),
      );
      break;
    // blue
    case "update_data":
      return FinancialStatementsStatusModel(
        title: "Anda telah mengupdate ${report.toLowerCase()}.",
        subtitle: "",
        color: Color(0xffF4F5FA),
      );
      break;
    default:
      return null;
      break;
  }
}
