class BisnisItemHelper {
  // nampilin button more vertical ?
  static bool showRepost(String statusString) {
    var _show = false;
    switch (statusString) {
      case "waiting for verification":
        _show = false;
        break;
      case "rejected":
        _show = true;
        break;
      // case "rejected airing":
      //   _show = true;
      //   break;
      // case "rejected screeening":
      //   _show = true;
      //   break;
      case "verified":
        _show = false;
        break;
      default:
        _show = false;
        break;
    }
    return _show;
  }

  static bool showEdit(String statusString) {
    var _show = false;
    switch (statusString) {
      case "waiting for verification":
        _show = true;
        break;
      case "rejected":
        _show = true;
        break;
      // case "rejected airing":
      //   _show = true;
      //   break;
      // case "rejected screeening":
      //   _show = true;
      //   break;
      case "verified":
        _show = false;
        break;
      default:
        _show = false;
        break;
    }
    return _show;
  }

  static bool showDelete(String statusString) {
    var _show = false;
    switch (statusString) {
      case "waiting for verification":
        _show = true;
        break;
      case "rejected":
        _show = true;
        break;
      // case "rejected airing":
      //   _show = true;
      //   break;
      // case "rejected screeening":
      //   _show = true;
      //   break;
      case "verified":
        _show = false;
        break;
      default:
        _show = false;
        break;
    }
    return _show;
  }

  static bool checkMail(String statusString) {
    var _show = false;
    switch (statusString) {
      case "waiting for verification":
        _show = false;
        break;
      case "rejected":
        _show = true;
        break;
      // case "rejected airing":
      //   _show = true;
      //   break;
      // case "rejected screeening":
      //   _show = true;
      //   break;
      case "verified":
        _show = false;
        break;
      default:
        _show = false;
        break;
    }
    return _show;
  }

  // end
}
