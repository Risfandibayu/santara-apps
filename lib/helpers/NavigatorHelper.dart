import 'package:flutter/material.dart';
import 'package:santaraapp/pages/ExpiredSession.dart';

class NavigatorHelper {
  static pushToExpiredSession(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => ExpiredSessionUI(),
      ),
    );
  }
}
