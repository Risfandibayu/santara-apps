import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/core/data/models/file_upload_model.dart';
import 'package:santaraapp/core/data/models/pralisting_form_result.dart';
import 'package:santaraapp/core/data/models/submission_detail_model.dart';
import 'package:santaraapp/core/widgets/search_bottom_sheet/search_bottom_sheet.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_detail_page.dart';
import 'package:santaraapp/widget/pralisting/business_filed/presentation/pages/business_filed_page.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_1/domain/usecases/business_info_1_usecases.dart';
import 'package:santaraapp/widget/widget/components/kyc/KycFieldWrapper.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'PopupHelper.dart';
import 'ToastHelper.dart';

enum FieldState { pure, loading, loaded, error }

class PralistingHelper {
  static AutoScrollController controller;
  static KycFieldWrapper fieldWrapper;
  static final scrollDirection = Axis.vertical;
  static final numberFormat = NumberFormat("###,###,###", "en_us");

  static initAutoScroll(BuildContext context) {
    controller = AutoScrollController(
      viewportBoundaryGetter: () =>
          Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );
    fieldWrapper = KycFieldWrapper(controller: controller);
  }

  static void showSubmissionFailed(BuildContext context, {String message}) {
    ToastHelper.showFailureToast(
      context,
      message != null
          ? message
          : "Maaf, tidak dapat melanjutkan, mohon cek kembali form anda!",
    );
  }

  static Future<dynamic> showModal(
    context, {
    String title,
    String hintText,
    List lists,
    dynamic selected,
    bool showSearch,
  }) async {
    return showModalBottomSheet(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(5.0)),
      ),
      context: context,
      builder: (context) {
        return SearchBottomSheet(
          items: lists,
          selectedItem: selected,
          title: "$title",
          hintText: "$hintText",
          showSearch: showSearch,
        );
      },
    );
  }

  static void handleScrollValidation({
    @required FormBlocState state,
  }) {
    bool canScroll = true;
    if (state.fieldBlocs().isNotEmpty) {
      var index = 0;
      state.fieldBlocs().entries.forEach((element) {
        index += 1;
        // ignore: close_sinks
        SingleFieldBloc data = element.value;
        if (data.state.hasError && !data.state.isInitial) {
          if (canScroll) {
            fieldWrapper.scrollTo(index);
            canScroll = false;
            return;
          }
        }
      });
    }
  }

  static void handleAutoScroll({
    @required KycFieldWrapper fieldWrapper,
    @required FormBlocState state,
  }) {
    bool canScroll = true;
    if (state.fieldBlocs().isNotEmpty) {
      var index = 0;
      state.fieldBlocs().entries.forEach((element) {
        index += 1;
        // ignore: close_sinks
        SingleFieldBloc data = element.value;
        if (data.state.hasError && !data.state.isInitial) {
          if (canScroll) {
            fieldWrapper.scrollTo(index);
            canScroll = false;
            return;
          }
        }
      });
    }
  }

  static void handleErrorFields({
    @required FormBlocState state,
    @required Submission submission,
  }) {
    try {
      if (submission.submissionDetail != null &&
          submission.submissionDetail.length > 0) {
        state.fieldBlocs().entries.forEach((element) {
          var error = submission.submissionDetail.firstWhere(
            (e) => e?.fieldId == element?.key,
            orElse: () => null,
          );
          // ignore: close_sinks
          dynamic bloc = element.value;
          if (bloc is ListFieldBloc) {
            // ignore: close_sinks
            ListFieldBloc listFieldBlocs = element.value;
            listFieldBlocs.state.fieldBlocs.forEach((element) {
              dynamic blocmy = element;
              blocmy.error = "${error.error}";
            });
          } else {
            if (error != null && error.status == 1 && error.error.isNotEmpty) {
              print(">> Field : ${error.fieldId}");
              bloc.addFieldError("${error.error}");
            }
          }
        });
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  // get uploaded file name
  static List<String> getUploadedFileName(dynamic data) {
    List<FileUploadModel> datas = data.state.extraData;
    if (data != null && datas != null) {
      List<String> files = [];
      datas.forEach((element) {
        files.add(element.filename);
      });
      return files.toSet().toList();
    } else {
      return [];
    }
  }

  static String getSingleFile(dynamic data) {
    var result = getUploadedFileName(data);
    if (result != null && result.length > 0) {
      return result[0];
    } else {
      return "";
    }
  }

  static getBusinessOptions({
    @required String group,
    @required SelectFieldBloc bloc,
    @required GetListBusinessOptions getListBusinessOptions,
    bool buffer = false, // hack, ada error step 4, harus nunggu loading semua
  }) async {
    try {
      bloc.updateExtraData(FieldState.loading);
      // if (bloc.state.formBloc != null) {
      //   bloc.state.formBloc.emitLoading();
      // }
      final params = {
        'page': 1,
        'limit': 100,
        'group': group,
        'show_all': 'true',
      };
      final result = await getListBusinessOptions(params);

      if (result.isRight()) {
        final options = result.getOrElse(null);
        bloc.updateItems(options);
        bloc.updateExtraData(FieldState.loaded);
        if (buffer) {
          bloc.state.formBloc.emitSuccess(
            successResponse: FormResult(
              status: FormStatus.get_field_data_success,
              message: "success",
            ),
            canSubmitAgain: true,
          );
        }
      } else {
        bloc.updateExtraData(FieldState.error);
        if (buffer) {
          bloc.state.formBloc.emitFailure(
            failureResponse: FormResult(
              status: FormStatus.get_field_data_error,
              message: "error server",
            ),
          );
        }
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      bloc.updateExtraData(FieldState.error);
      if (buffer) {
        bloc.state.formBloc.emitFailure(
          failureResponse: FormResult(
            status: FormStatus.get_field_data_error,
            message: "error local",
          ),
        );
      }
    }
  }

  static String parseToDecimal(int value) {
    try {
      return numberFormat.format(value);
    } catch (e) {
      return "";
    }
  }

  static int removeComma(String value) {
    try {
      if (value != null) {
        return int.parse(value.replaceAll(",", ""));
      } else {
        return 0;
      }
    } catch (e) {
      return 0;
    }
  }

  static void handleOnSavePralisting(BuildContext context, {String message}) {
    Navigator.pop(context);
    ToastHelper.showSuccessToast(context, "${message ?? ''}");
    Navigator.pop(context);
    // Navigator.pushReplacement(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => BusinessFiledPage(),
    //   ),
    // );
  }

  static void handleBackButton(BuildContext context) {
    PopupHelper.popConfirmation(
      context,
      () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
      "Apakah anda yakin ingin keluar?",
      "Data yang telah diubah akan hilang, anda perlu mengisinya kembali.",
    );
  }
}
