import 'dart:async';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:santaraapp/utils/logger.dart';

class KycStatusHelper {
  int trying = 0;
  
  Future<bool> check() async {
    try {
      final storage = new FlutterSecureStorage();
      final token = await storage.read(key: 'token');
      final uuid = await storage.read(key: 'uuid');
      if (Helper.check) {
        final http.Response response = await http.get(
            '$apiLocal/users/by-uuid/$uuid',
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        final data = userFromJson(response.body);
        return data.trader.isVerified == 1 ? true : false;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      trying += 1;
      if (trying < 3) {
        await check();
      } else {
        return false;
      }
    }
  }
}
