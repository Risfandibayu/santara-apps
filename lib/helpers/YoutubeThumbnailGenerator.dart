import 'package:youtube_player_flutter/youtube_player_flutter.dart';

enum ThumbnailQuality { low, medium, high, max }

class YoutubeThumbnailGenerator {
  static getThumbnail(String youtubeUrl, ThumbnailQuality quality) {
    String _videoId = YoutubePlayer.convertUrlToId(youtubeUrl);
    String _thumbnailUrl = "";
    switch (quality) {
      case ThumbnailQuality.low:
        _thumbnailUrl = "https://img.youtube.com/vi/$_videoId/sddefault.jpg";
        break;
      case ThumbnailQuality.medium:
        _thumbnailUrl = "https://img.youtube.com/vi/$_videoId/mqdefault.jpg";
        break;
      case ThumbnailQuality.high:
        _thumbnailUrl = "https://img.youtube.com/vi/$_videoId/hqdefault.jpg";
        break;
      case ThumbnailQuality.max:
        _thumbnailUrl =
            "https://img.youtube.com/vi/$_videoId/maxresdefault.jpg";
        break;
      default:
        _thumbnailUrl =
            "https://img.youtube.com/vi/cLnEOWvio8c/maxresdefault.jpg";
        break;
    }
    return _thumbnailUrl;
  }
}
