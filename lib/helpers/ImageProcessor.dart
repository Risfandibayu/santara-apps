import 'dart:io';
import 'dart:math';
import 'package:image/image.dart' as IMG;
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

class ImageProcessor {
  static Future cropSquare(String srcFilePath, bool flip) async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;

    var bytes = await File(srcFilePath).readAsBytes();
    IMG.Image src = IMG.decodeImage(bytes);

    var cropSize = min(src.width, src.height);
    int offsetX = (src.width - min(src.width, src.height)) ~/ 3;
    int offsetY = (src.height - min(src.width, src.height)) ~/ 2;

    IMG.Image destImage =
        IMG.copyCrop(src, offsetX, offsetY, cropSize, cropSize);

    if (flip) {
      destImage = IMG.flipVertical(destImage);
    }

    var jpg = IMG.encodeJpg(destImage);
    var finalFile =
        await File(appDocPath + "/" + DateTime.now().toIso8601String() + ".jpg")
            .writeAsBytes(jpg);
    // print(">> Image Information : ");
    // IMG.Image srcx = IMG.decodeImage(await File(srcFilePath).readAsBytes());
    // print(">> Width : ${srcx.width}");
    // print(">> Height : ${srcx.height}");
    return finalFile;
  }

  // cek ekstensi file
  static Future<bool> checkImageExtension(
      File file, List<String> extensions) async {
    if (await file.exists()) {
      final String fileName = path.basename(file.path);
      final delim = fileName.lastIndexOf('.');
      String ext = fileName.substring(delim >= 0 ? delim : 0);
      return extensions.contains(ext.toLowerCase()) ? true : false;
    } else {
      return false;
    }
  }
}
