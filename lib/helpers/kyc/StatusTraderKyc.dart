import 'package:santaraapp/models/StatusKycTrader.dart';

class StatusResult {
  String title;
  String subtitle;
  bool isVerifying;
  EndStatus status;
  StatusResult({
    this.title,
    this.subtitle,
    this.isVerifying,
    this.status,
  });
}

class StatusTraderKycHelper {
  static bool isRejected(String status) {
    if (status != null) {
      return status.toLowerCase().contains("rejected") ? true : false;
    } else {
      return false;
    }
  }

  static bool isEmpty(String status) {
    if (status != null) {
      return status.toLowerCase().contains("empty") ? true : false;
    } else {
      return false;
    }
  }

  static bool isVerifying(String status) {
    if (status != null) {
      return status.toLowerCase().contains("verifying") ? true : false;
    } else {
      return false;
    }
  }

  static bool isVerified(String status) {
    if (status != null) {
      return status.toLowerCase().contains("verified") ? true : false;
    } else {
      return false;
    }
  }

  static StatusResult checkingCompany(StatusKycTrader res) {
    // jika ada salah satu step yang masih kosong
    if (isEmpty(res.statusKyc1) ||
        isEmpty(res.statusKyc2) ||
        isEmpty(res.statusKyc3) ||
        isEmpty(res.statusKyc4) ||
        isEmpty(res.statusKyc5) ||
        isEmpty(res.statusKyc6) ||
        isEmpty(res.statusKyc7) ||
        isEmpty(res.statusKyc8)) {
      // jika step kosong
      return StatusResult(
        title: "Anda belum menyelesaikan proses KYC",
        subtitle: "Tap disini untuk menyelesaikan KYC",
        isVerifying: false,
        status: EndStatus.uncomplete,
      );
    } else {
      if (isRejected(res.statusKyc1) ||
          isRejected(res.statusKyc2) ||
          isRejected(res.statusKyc3) ||
          isRejected(res.statusKyc4) ||
          isRejected(res.statusKyc5) ||
          isRejected(res.statusKyc6) ||
          isRejected(res.statusKyc7) ||
          isRejected(res.statusKyc8)) {
        // jika status rejected
        return StatusResult(
          title: "Pengajuan KYC ditolak",
          subtitle: "Tap disini untuk mengajukan kembali",
          isVerifying: false,
          status: EndStatus.rejected,
        );
      } else {
        // jika sedang dalam tahap verifikasi
        if (isVerifying(res.statusKyc1) ||
            isVerifying(res.statusKyc2) ||
            isVerifying(res.statusKyc3) ||
            isVerifying(res.statusKyc4) ||
            isVerifying(res.statusKyc5) ||
            isVerifying(res.statusKyc6) ||
            isVerifying(res.statusKyc7) ||
            isVerifying(res.statusKyc8)) {
          // tahap verifikasi
          return StatusResult(
            title: "Akun Anda sedang dalam proses verifikasi",
            subtitle:
                "Anda dapat melakukan transaksi setelah akun diverifikasi",
            isVerifying: true,
            status: EndStatus.verifying,
          );
        } else {
          // jika sedang dalam tahap verifikasi
          if (isVerified(res.statusKyc1) ||
              isVerified(res.statusKyc2) ||
              isVerified(res.statusKyc3) ||
              isVerified(res.statusKyc4) ||
              isVerified(res.statusKyc5) ||
              isVerified(res.statusKyc6) ||
              isVerified(res.statusKyc7) ||
              isVerified(res.statusKyc8)) {
            // tahap verifikasi
            return StatusResult(
              status: EndStatus.verified,
            );
          } else {
            // ALL IS OK
            return null;
          }
        }
      }
    }
  }

  static StatusResult checkingIndividual(StatusKycTrader res) {
    // jika step kosong
    if (isEmpty(res.statusKyc1) ||
        isEmpty(res.statusKyc2) ||
        isEmpty(res.statusKyc3) ||
        isEmpty(res.statusKyc4) ||
        isEmpty(res.statusKyc5) ||
        isEmpty(res.statusKyc6)) {
      return StatusResult(
        title: "Anda belum menyelesaikan proses KYC",
        subtitle: "Tap disini untuk menyelesaikan KYC",
        isVerifying: false,
        status: EndStatus.uncomplete,
      );
    } else {
      // jika ada salah satu step yang masih kosong

      if (isRejected(res.statusKyc1) ||
          isRejected(res.statusKyc2) ||
          isRejected(res.statusKyc3) ||
          isRejected(res.statusKyc4) ||
          isRejected(res.statusKyc5) ||
          isRejected(res.statusKyc6)) {
        // jika status rejected
        return StatusResult(
          title: "Pengajuan KYC ditolak",
          subtitle: "Tap disini untuk mengajukan kembali",
          isVerifying: false,
          status: EndStatus.rejected,
        );
      } else {
        // jika sedang dalam tahap verifikasi
        if (isVerifying(res.statusKyc1) ||
            isVerifying(res.statusKyc2) ||
            isVerifying(res.statusKyc3) ||
            isVerifying(res.statusKyc4) ||
            isVerifying(res.statusKyc5) ||
            isVerifying(res.statusKyc6)) {
          // tahap verifikasi
          return StatusResult(
            title: "Akun Anda sedang dalam proses verifikasi",
            subtitle:
                "Anda dapat melakukan transaksi setelah akun diverifikasi",
            isVerifying: true,
            status: EndStatus.verifying,
          );
        } else {
          // jika telah terverifikasi
          if (isVerified(res.statusKyc1) ||
              isVerified(res.statusKyc2) ||
              isVerified(res.statusKyc3) ||
              isVerified(res.statusKyc4) ||
              isVerified(res.statusKyc5) ||
              isVerified(res.statusKyc6)) {
            return StatusResult(
              status: EndStatus.verified,
            );
          } else {
            // ALL IS OK
            return null;
          }
        }
      }
    }
  }

  static StatusResult check(String status) {
    switch (status) {
      case "data_updated":
        return null;
        break;
      case "verifying":
        return StatusResult(
          title: "Akun Anda sedang dalam proses verifikasi",
          subtitle: "Anda dapat melakukan transaksi setelah akun diverifikasi",
          isVerifying: true,
          status: EndStatus.verifying,
        );
        break;
      case "rejected":
        return StatusResult(
          title: "Pengajuan KYC ditolak",
          subtitle: "Tap disini untuk mengajukan kembali",
          isVerifying: false,
          status: EndStatus.rejected,
        );
        break;
      case "kustodian_rejected":
        return StatusResult(
          title: "Pengajuan KYC ditolak",
          subtitle: "Tap disini untuk mengajukan kembali",
          isVerifying: false,
          status: EndStatus.rejected,
        );
        break;
      case "empty":
        return StatusResult(
          title: "Anda belum menyelesaikan proses KYC",
          subtitle: "Tap disini untuk menyelesaikan KYC",
          isVerifying: false,
          status: EndStatus.uncomplete,
        );
        break;
      default:
        return null;
    }
  }
}
