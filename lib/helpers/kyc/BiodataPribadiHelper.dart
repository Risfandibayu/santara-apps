import 'dart:io';

class KycBiodataPribadiHelper {
  String namaLengkap;
  String tempatLahir;
  String tanggalLahir;
  String bulanLahir;
  String tahunLahir;
  File fotoKtp;
  File selfieKtp;

  KycBiodataPribadiHelper({
    this.namaLengkap,
    this.tempatLahir,
    this.tanggalLahir,
    this.bulanLahir,
    this.tahunLahir,
    this.fotoKtp,
    this.selfieKtp,
  });
}
