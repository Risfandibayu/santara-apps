import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:path/path.dart' as path;

class KycHelpers {
  static bool dayOfMonthCheckError(int day, String month) {
    var monthParsed = month.toLowerCase();
    var months = [
      "januari",
      "maret",
      "mei",
      "juli",
      "agustus",
      "oktober",
      "desember"
    ];
    if (months.contains(monthParsed)) {
      return day > 31 ? true : false;
    } else {
      return day > 30 ? true : false;
    }
  }

  static String getfiletype(String base) {
    final String fileName = path.basename(base);
    final delim = fileName.lastIndexOf('.');
    String ext = fileName.substring(delim >= 0 ? delim : 0);
    return ext.replaceFirst('.', '');
  }

  // day = field tanggal
  // month = field bulan
  // year = field tahun
  // datebloc = bloc untuk throw error ( nampilin di ui )
  // houldFromPast = jika tanggal harus dari masa lampau set jadi true
  static bool dateChecker(
    TextFieldBloc day,
    SelectFieldBloc month,
    TextFieldBloc year,
    TextFieldBloc dateBloc,
    bool shouldFromPast,
  ) {
    final today = DateTime.now();
    if (day.value.length < 2 ||
        int.parse(day.value) > 31 ||
        int.parse(day.value) < 1) {
      day.addFieldError("Tidak valid!");
      return true;
    } else if (month.value == null) {
      month.addFieldError("Bulan tidak valid!");
      return true;
    } else if (year.value.length < 4) {
      year.addFieldError("Tidak valid!");
      return true;
    } else if (int.parse(year.value) > today.year) {
      year.addFieldError("Tidak valid!");
      return true;
    } else if (int.parse(year.value) % 4 != 0) {
      if (month.value.name.toLowerCase() == "februari") {
        if (int.parse(day.value) > 28) {
          day.addFieldError("Tidak valid");
          return true;
        } else {
          if (shouldFromPast) {
            final DateTime userDob =
                DateTime.parse("${year.value}-${month.value.id}-${day.value}");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff > 0) {
              dateBloc
                  .addFieldError("Tanggal harus dari masa lampau / hari ini");
              return true;
            } else {
              dateBloc.clear();
              return false;
            }
          } else {
            dateBloc.clear();
            return false;
          }
        }
      } else {
        if (dayOfMonthCheckError(int.parse(day.value), month.value.name)) {
          day.addFieldError("Tidak valid");
          return true;
        } else {
          if (shouldFromPast) {
            final DateTime userDob =
                DateTime.parse("${year.value}-${month.value.id}-${day.value}");
            final diff = userDob.difference(DateTime.now()).inDays;
            if (diff > 0) {
              dateBloc
                  .addFieldError("Tanggal harus dari masa lampau / hari ini");
              return true;
            } else {
              dateBloc.clear();
              return false;
            }
          } else {
            dateBloc.clear();
            return false;
          }
        }
      }
    } else {
      if (dayOfMonthCheckError(int.parse(day.value), month.value.name)) {
        day.addFieldError("Tidak valid");
        return true;
      } else {
        if (shouldFromPast) {
          final DateTime userDob =
              DateTime.parse("${year.value}-${month.value.id}-${day.value}");
          final diff = userDob.difference(DateTime.now()).inDays;
          if (diff > 0) {
            dateBloc.addFieldError("Tanggal harus dari masa lampau / hari ini");
            return true;
          } else {
            dateBloc.clear();
            return false;
          }
        } else {
          dateBloc.clear();
          return false;
        }
      }
    }
  }

  static bool emailChecker(TextFieldBloc email) {
    if (email.value.length < 1) {
      email.addFieldError("Email tidak boleh kosong!");
      return true;
    } else if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(email.value)) {
      email.addFieldError("Format email salah!");
      return true;
    } else if (email.value.length > 55) {
      email.addFieldError("Maksimal email berjumlah 55 karakter!");
      return true;
    } else {
      return false;
    }
  }

  static bool npwpChecker(TextFieldBloc npwp, bool mandatory) {
    if (npwp.value != null && npwp.value.length > 0) {
      if (npwp.value.length > 20) {
        npwp.addFieldError("NPWP maksimal 20 karakter");
        return true;
      } else if (mandatory) {
        if (npwp.value.length < 1) {
          npwp.addFieldError("NPWP maksimal 20 karakter");
          return true;
        } else {
          return false;
        }
      } else if (!RegExp(r"^[0-9+#-]*$").hasMatch(npwp.value)) {
        npwp.addFieldError("NPWP tidak boleh berisi huruf");
        return true;
      } else {
        return false;
      }
    } else {
      if (mandatory) {
        npwp.addFieldError("NPWP Tidak boleh kosong!");
        return true;
      } else {
        return false;
      }
    }
  }

  // jika status verifying, disable setiap field input
  static bool fieldCheck(String status) {
    switch (status) {
      case "verifying":
        return false;
        break;
      default:
        return true;
        break;
    }
  }

  // jika status sudah terverifikasi, maka beberapa field tdk bisa diedit
  // Jenis Kelamin
  // Foto KTP
  // Foto Selfie dengan KTP
  // Nama gadis ibu kandung
  // Alamat Sesuai KTP (negara, provinsi, kabupaten, alamat lengkap, kode pos)
  static bool isVerified(String status) {
    return status == "verified" ? false : true;
  }

  // check apakah tahun kabisat
  static bool isLeap(String yearVal) {
    int year = int.parse(yearVal);
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
  }
}
