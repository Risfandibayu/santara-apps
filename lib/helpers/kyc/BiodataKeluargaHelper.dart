class BiodataKeluarga {
  bool isSubmitted;
  bool isEdited;
  String maritalStatus;
  String spouseName;
  String motherMaidenName;
  String heir;
  String heriRelation;
  String heirPhone;

  BiodataKeluarga({
    this.isSubmitted,
    this.isEdited,
    this.maritalStatus,
    this.spouseName,
    this.motherMaidenName,
    this.heir,
    this.heriRelation,
    this.heirPhone,
  });
}

class KycBioKeluargaHelper {
  static BiodataKeluarga dokumen = BiodataKeluarga(
    isSubmitted: null,
    isEdited: null,
    maritalStatus: null,
    spouseName: null,
    motherMaidenName: null,
    heir: null,
    heriRelation: null,
    heirPhone: null,
  );
}
