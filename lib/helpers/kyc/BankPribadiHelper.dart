class BankUser {
  bool isEdited;
  bool isSubmitted;
  String accountName1;
  int bankInvestor1;
  String accountNumber1;
  String accountName2;
  int bankInvestor2;
  String accountNumber2;
  String accountName3;
  int bankInvestor3;
  String accountNumber3;

  BankUser({
    this.isEdited,
    this.isSubmitted,
    this.accountName1,
    this.bankInvestor1,
    this.accountNumber1,
    this.accountName2,
    this.bankInvestor2,
    this.accountNumber2,
    this.accountName3,
    this.bankInvestor3,
    this.accountNumber3,
  });
}

class KycBankUserHelper {
  static BankUser dokumen = BankUser(
    isEdited: null,
    isSubmitted: null,
    accountName1: null,
    bankInvestor1: null,
    accountNumber1: null,
    accountName2: null,
    bankInvestor2: null,
    accountNumber2: null,
    accountName3: null,
    bankInvestor3: null,
    accountNumber3: null,
  );
}
