import 'dart:io';

class InformasiPajakUser {
  bool isEdited;
  bool isSubmitted;
  int income;
  int taxAccountCode;
  String npwp;
  String dateRegNpwp;
  bool haveSecurityAccount;
  String securityAccountRegDate;
  File securitiesAccount;
  String sidNumber;

  InformasiPajakUser({
    this.isEdited,
    this.isSubmitted,
    this.income,
    this.taxAccountCode,
    this.npwp,
    this.dateRegNpwp,
    this.haveSecurityAccount,
    this.securityAccountRegDate,
    this.securitiesAccount,
    this.sidNumber,
  });
}

class KycInformasiPajakHelper {
  static InformasiPajakUser dokumen = InformasiPajakUser(
    isEdited: null,
    isSubmitted: null,
    income: null,
    taxAccountCode: null,
    npwp: null,
    dateRegNpwp: null,
    haveSecurityAccount: null,
    securityAccountRegDate: null,
    securitiesAccount: null,
    sidNumber: null,
  );
}
