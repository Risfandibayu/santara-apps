class PekerjaanUser {
  bool isEdited;
  bool isSubmitted;
  String occupation;
  String businessDesc;
  int assetTotal;
  String fundsSource;
  String investmentPurposes;
  String investmentPurposesId;
  String code;

  PekerjaanUser({
    this.isEdited,
    this.isSubmitted,
    this.occupation,
    this.businessDesc,
    this.assetTotal,
    this.fundsSource,
    this.investmentPurposes,
    this.investmentPurposesId,
    this.code,
  });
}

class KycPekerjaanHelper {
  static PekerjaanUser dokumen = PekerjaanUser(
    isEdited: null,
    isSubmitted: null,
    occupation: null,
    businessDesc: null,
    assetTotal: null,
    fundsSource: null,
    investmentPurposes: null,
    investmentPurposesId: null,
    code: null,
  );
}
