class AlamatUser {
  bool isEdited;
  bool isSubmitted;
  String idCardCountry;
  String idCardProvince;
  String idCardRegency;
  String idCardAddress;
  String idCardPostalCode;
  String sameAddressWithIdCard;
  String country;
  String province;
  String regency;
  String address;
  String postalCode;

  AlamatUser({
    this.isEdited,
    this.isSubmitted,
    this.idCardCountry,
    this.idCardProvince,
    this.idCardRegency,
    this.idCardAddress,
    this.idCardPostalCode,
    this.sameAddressWithIdCard,
    this.country,
    this.province,
    this.regency,
    this.address,
    this.postalCode,
  });
}

class KycAlamatHelper {
  static AlamatUser dokumen = AlamatUser(
    isEdited: null,
    isSubmitted: null,
    idCardCountry: null,
    idCardProvince: null,
    idCardRegency: null,
    idCardAddress: null,
    idCardPostalCode: null,
    sameAddressWithIdCard: null,
    country: null,
    province: null,
    regency: null,
    address: null,
    postalCode: null,
  );
}
