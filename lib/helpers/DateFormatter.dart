import 'package:intl/intl.dart';

class DateFormatter {
  static String ddMMMyyyy(DateTime date) {
    if (date != null) {
      return DateFormat("dd MMM yyyy").format(date);
    } else {
      return "";
    }
  }

  static String ddMMMMyyyy(DateTime date) {
    if (date != null) {
      return DateFormat("dd MMMM yyyy").format(date);
    } else {
      return "";
    }
  }

  static String yyyy_MM_dd(DateTime date) {
    if (date != null) {
      return DateFormat("yyyy-MM-dd").format(date);
    } else {
      return "";
    }
  }
}
