// constants untuk nyimpen keys dari flutter_secure_storage atau sharedprefs
//
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/models/User.dart' as user;

class Constants {
  static String pralistingShowcase = 'pls'; // status show case pra listing
  static String detailEmitenShowcaseUsr =
      'dls_user'; // status show case detail emiten ( jika dia bukan pemilik bisnis )
  static String detailEmitenShowcaseAdm =
      'dls_admin'; // status show case detail emiten ( jika dia pemilik bisnis )
  static String statusKyc = 'kyc_status';
  static Future<user.User> getUserData() async {
    final storage = new FlutterSecureStorage();
    return user.userFromJson(await storage.read(key: 'user'));
  }
}
