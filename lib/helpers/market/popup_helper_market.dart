import 'package:flutter/material.dart';
import 'package:santaraapp/blocs/market/submit_transaction.dart';
import 'package:santaraapp/helpers/RupiahFormatter.dart';
import 'package:santaraapp/models/market/active_period.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/pages/Login.dart';
import 'package:santaraapp/widget/market/home_market.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';
import 'package:santaraapp/widget/market/transaksi_view.dart';
import 'package:santaraapp/widget/security_token/pin/SecurityPinUI.dart';
import 'package:santaraapp/widget/widget/components/market/field_pasar_sekunder.dart';
import 'package:super_tooltip/super_tooltip.dart';

class PopupHelperMarket {
  static verifyPIN(
      BuildContext context, Function(String pin, bool finger) callback) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          title: "Masukan PIN Anda",
          type: SecurityType.check,
          onSuccess: (pin, finger) {
            Navigator.pop(context);
            callback(pin, finger);
          },
        ),
      ),
    );
  }

  // item informasi di popup konfirmasi transaksi
  static Widget _itemKonfirmasiTransaksi(String name, Widget value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(flex: 1, child: Text(name)),
          Expanded(flex: 1, child: value),
        ],
      ),
    );
  }

  // convert int to name of day
  static String _dayToString(int day) {
    switch (day) {
      case 0:
        return "Minggu";
        break;
      case 1:
        return "Senin";
        break;
      case 2:
        return "Selasa";
        break;
      case 3:
        return "Rabu";
        break;
      case 4:
        return "Kamis";
        break;
      case 5:
        return "Jumat";
        break;
      case 6:
        return "Sabtu";
        break;
      default:
        return "";
    }
  }

  // popup konfirmasi transaksi jual/beli di market
  static void showKonfirmasiTransaksiSaham(
      {BuildContext context,
      String uuid,
      String emitenUuid,
      String code,
      String company,
      int price,
      int amount,
      @required double fee,
      TransactionType type,
      SubmitTransactionBloc bloc}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6), topRight: Radius.circular(6)),
        ),
        builder: (ctx) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                  child: Text(
                      type == TransactionType.beli
                          ? "Konfirmasi Pembelian"
                          : "Konfirmasi Penjualan",
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      _itemKonfirmasiTransaksi(
                          "Kode Saham",
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(code,
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(company),
                            ],
                          )),
                      _itemKonfirmasiTransaksi(
                          type == TransactionType.beli
                              ? "Beli di Harga"
                              : "Jual di Harga",
                          Text("Rp ${NumberFormatter.convertNumber(price)}",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      _itemKonfirmasiTransaksi(
                          "Jumlah (Lembar)",
                          Text(NumberFormatter.convertNumber(amount),
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      _itemKonfirmasiTransaksi(
                          "Harga Saham",
                          Text(
                              "Rp ${NumberFormatter.convertNumber(price * amount)}",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      _itemKonfirmasiTransaksi(
                          "Biaya Administrasi",
                          type == TransactionType.beli
                              ? Text(
                                  "Rp ${NumberFormatter.convertNumber((price * amount * fee).ceil())}",
                                  style: TextStyle(fontWeight: FontWeight.bold))
                              : Text(
                                  "- Rp ${NumberFormatter.convertNumber((price * amount * fee).ceil())}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFFFF4343)))),
                      Divider(
                        color: Color(0xFFB8B8B8),
                        height: 20,
                      ),
                      _itemKonfirmasiTransaksi(
                          "Total",
                          Text(
                              type == TransactionType.beli
                                  ? "Rp ${NumberFormatter.convertNumber(price * amount + ((price * amount * fee).ceil()))}"
                                  : "Rp ${NumberFormatter.convertNumber(price * amount - ((price * amount * fee).ceil()))}",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text(type == TransactionType.beli
                      ? "Note : Pembayaran transaksi ini akan ditarik dari saldo wallet Anda. Jika pembelian Anda berhasil saldo otomatis terpotong. Jika tidak, saldo akan dikembalikan ke wallet di jam penutupan hari ini."
                      : "Note : Transaksi ini akan masuk ke orderbook. Jika penjualan Anda berhasil maka saldo akan otomatis ditambahkan ke wallet Anda. Jika tidak, saham Anda akan tetap ada di portfolio Anda."),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: SantaraSecondaryButton(
                          key: Key('batal_jual_beli'),
                          child: Text(
                            "Batal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xFFBF2D30),
                              fontSize: 14,
                            ),
                          ),
                          height: 40,
                          onTap: () => Navigator.pop(context),
                        ),
                      ),
                      Container(width: 10),
                      Expanded(
                        child: SantaraMainButton(
                          key: Key('lanjut_jual_beli'),
                          child: Text(
                            "Lanjutkan",
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          height: 40,
                          onTap: () {
                            Navigator.pop(context);
                            verifyPIN(context, (pin, finger) {
                              if (uuid != null) {
                                bloc.add(SubmitEditEvent(
                                    uuid: uuid,
                                    emitenUuid: emitenUuid,
                                    codeEmiten: code,
                                    amount: amount,
                                    price: price,
                                    type: type == TransactionType.beli
                                        ? "buy"
                                        : "sell",
                                    pin: pin,
                                    finger: finger));
                              } else {
                                bloc.add(PostEvent(
                                    emitenUuid: emitenUuid,
                                    codeEmiten: code,
                                    amount: amount,
                                    price: price,
                                    type: type == TransactionType.beli
                                        ? "buy"
                                        : "sell",
                                    pin: pin,
                                    finger: finger));
                              }
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  // popup jika transaksi jual/beli berhasil
  static void showOrderBeliBerhasil(
      BuildContext context, bool isBeli, bool isNotEdit) {
    showDialog(
      barrierDismissible: isNotEdit,
      context: context,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async => isNotEdit,
          child: Center(
            child: AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset("assets/icon/success.png",
                      width: MediaQuery.of(context).size.width / 3,
                      height: MediaQuery.of(context).size.width / 3),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: Text(
                        isBeli ? "Order Beli Berhasil" : "Order Jual berhasil",
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold)),
                  ),
                  Text(
                    isBeli
                        ? "Order berhasil dan telah masuk ke antrian. Jika match, maka transaksi akan otomatis diproses dan saham akan masuk ke portofolio Anda."
                        : "Order berhasil dan telah masuk ke antrian. Jika match, maka transaksi akan otomatis diproses dan saham akan dikurangi dari portofolio Anda. ",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: SantaraMainButton(
                      key: Key('go_to_transaction_page'),
                      height: 40,
                      child: Text(
                        "Lihat Transaksi",
                        style: TextStyle(fontSize: 14, color: Colors.white),
                      ),
                      onTap: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (route) => false);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => HomeMarket()));
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => TransaksiView()));
                      },
                    ),
                  ),
                  SantaraSecondaryButton(
                    key: Key('go_to_home_market_page'),
                    height: 40,
                    child: Text(
                      "Home Pasar Sekunder",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xFFBF2D30),
                        fontSize: 14,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home(index: 4)),
                          (route) => false);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => HomeMarket()));
                    },
                    decoration: BoxDecoration(),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static void showTooltip(BuildContext context, String title, String desc) {
    var tooltip = SuperTooltip(
      popupDirection: TooltipDirection.down,
      arrowBaseWidth: 0,
      arrowLength: 0,
      arrowTipDistance: 0,
      borderWidth: 1,
      hasShadow: false,
      borderColor: Color(0xFFDADADA),
      content: new Material(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          title == null
              ? Container()
              : Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                  softWrap: true,
                ),
          title == null ? Container() : Container(height: 4),
          Text(
            desc,
            style: TextStyle(fontSize: 13),
            softWrap: true,
          )
        ],
      )),
    );

    tooltip.show(context);
  }

  // popup info ballance di market
  static void showPopupInfoBallance(
      BuildContext context, String title, String desc, String value) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(child: Container()),
                      InkWell(
                          child: Icon(Icons.close),
                          onTap: () => Navigator.pop(context))
                    ],
                  ),
                  Text(title,
                      style: TextStyle(
                          color: Color(0xFFBF2D30),
                          fontSize: 17,
                          fontWeight: FontWeight.bold)),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                    child: Text(
                      desc,
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(value,
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          );
        });
  }

  static void showHapusTransaksi(BuildContext context, String uuid, String code,
      String type, SubmitTransactionBloc bloc) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "Batalkan transaksi",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
              Container(height: 12),
              Text(
                "Anda yakin ingin membatalkan transaksi?",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
              Container(height: 20),
              SantaraMainButton(
                key: Key('hapus_transaksi'),
                height: 40,
                child: Text(
                  "Ya, hapus transaksi",
                  style: TextStyle(fontSize: 14, color: Colors.white),
                ),
                onTap: () {
                  Navigator.pop(context);
                  verifyPIN(context, (pin, finger) {
                    bloc.add(DeleteEvent(
                        trxUuid: uuid,
                        codeEmiten: code,
                        type: type,
                        pin: pin,
                        finger: finger));
                  });
                },
              ),
              Container(height: 6),
              SantaraSecondaryButton(
                key: Key('batal_hapus_transaksi'),
                height: 40,
                child: Text(
                  "Tidak, kembali ke halaman transaksi",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xFFBF2D30),
                    fontSize: 14,
                  ),
                ),
                onTap: () => Navigator.pop(context),
                decoration: BoxDecoration(),
              )
            ],
          ),
        );
      },
    );
  }

  static void notLoginYet({BuildContext context}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6), topRight: Radius.circular(6)),
        ),
        builder: (ctx) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  "assets/icon/forbidden.png",
                  scale: 2.5,
                ),
                Container(height: 10),
                Text(
                  "Maaf silahkan login terlebih dahulu",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Container(height: 16),
                SantaraMainButton(
                    key: Key('go_to_login_page_from_popup'),
                    child: Text(
                      "Masuk",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => Login()));
                    }),
                SantaraSecondaryButton(
                  key: Key('close_popup_login'),
                  child: Text("Tutup",
                      style: TextStyle(
                          color: Color(0xFFBF2D30),
                          fontWeight: FontWeight.bold)),
                  decoration: BoxDecoration(),
                  onTap: () => Navigator.pop(context),
                )
              ],
            ),
          );
        });
  }

  // popup jika belum login dan ingin mengakses page yg memerlukan login di market
  static void needLoginToAccessThisPage({BuildContext context, int isGoBack}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: isGoBack != 1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6), topRight: Radius.circular(6)),
        ),
        builder: (ctx) {
          return WillPopScope(
            onWillPop: () async => isGoBack != 1,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset(
                    "assets/icon/forbidden.png",
                    scale: 2.5,
                  ),
                  Container(height: 10),
                  Text(
                    "Login Terlebih Dahulu",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Container(height: 10),
                  Text(
                    isGoBack == 1
                        ? "Untuk melihat halaman ini Anda harus login terlebih dahulu"
                        : "Untuk melanjutkan ke halaman transaksi Anda harus login terlebih dahulu",
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.visible,
                    style: TextStyle(fontSize: 14),
                  ),
                  Container(height: 16),
                  SantaraMainButton(
                      key: Key('go_to_login_page_from_popup'),
                      child: Text(
                        "Masuk",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      onTap: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (route) => false);
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => Login()));
                      }),
                  Container(height: 6),
                  SantaraSecondaryButton(
                      key: Key('close_popup_login'),
                      child: Text("Tutup",
                          style: TextStyle(
                              color: Color(0xFFBF2D30),
                              fontWeight: FontWeight.bold)),
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFD23737)),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      onTap: () {
                        if (isGoBack == 1) {
                          // kembali ke page sebelum nya
                          Navigator.pop(context);
                          Navigator.pop(context);
                        } else {
                          // close popup
                          Navigator.pop(context);
                        }
                      })
                ],
              ),
            ),
          );
        });
  }

  // popup informasi jam buka pasar harian
  static void showActivePeriod(
      BuildContext context, ActivePeriod activePeriod) {
    double fontSize = MediaQuery.of(context).size.width / 30 > 15
        ? 15
        : MediaQuery.of(context).size.width / 30;
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(6), topRight: Radius.circular(6)),
      ),
      builder: (ctx) {
        return Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Pasar sekunder tutup",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 6),
                child: Text(
                    "Transaksi hanya bisa dilakukan pada hari dan jam kerja berikut :",
                    style: TextStyle(fontSize: 16)),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: Row(children: [
                    Expanded(
                        flex: 1,
                        child: Text("Hari Kerja",
                            style: TextStyle(
                                fontSize: fontSize,
                                fontWeight: FontWeight.bold))),
                    Expanded(
                        flex: 1,
                        child: Text("Jam Kerja",
                            style: TextStyle(
                                fontSize: fontSize,
                                fontWeight: FontWeight.bold))),
                  ])),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: activePeriod.data.length,
                    itemBuilder: (_, i) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 1,
                                child: Text(
                                    _dayToString(activePeriod.data[i].day),
                                    style: TextStyle(fontSize: fontSize))),
                            Expanded(
                              flex: 1,
                              child: activePeriod.data[i].hour == null
                                  ? Text("-",
                                      style: TextStyle(fontSize: fontSize))
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      physics: ClampingScrollPhysics(),
                                      itemCount:
                                          activePeriod.data[i].hour.length,
                                      itemBuilder: (_, ii) {
                                        return Text(
                                            activePeriod.data[i].hour[ii]
                                                    .startHour +
                                                ' - ' +
                                                activePeriod
                                                    .data[i].hour[ii].endHour,
                                            style:
                                                TextStyle(fontSize: fontSize));
                                      },
                                    ),
                            ),
                          ],
                        ),
                      );
                    }),
              ),
              Container(height: 16),
              SantaraMainButton(
                key: Key('close_popup_active_period'),
                height: 45,
                onTap: () => Navigator.pop(context),
                child: Text("Kembali",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        );
      },
    );
  }
}
