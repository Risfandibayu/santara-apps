import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class RupiahToText {
  static convertRupiah(String value) {
    final money = int.parse(value);
    final formatter = NumberFormat("###,###,###", "id_ID");
    String newText = "Rp " + formatter.format(money);
    return newText;
  }
}

class RupiahFormatter extends TextInputFormatter {
  RupiahFormatter({this.maxDigits});
  final int maxDigits;

  static String initialValueFormat(String text) {
    if (text != null && text.isNotEmpty) {
      double value = double.parse(text);
      final formatter = NumberFormat("###,###,###", "id_ID");
      String newText = "Rp " + formatter.format(value);
      return newText;
    } else {
      return "";
    }
  }

  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    if (maxDigits != null && newValue.selection.baseOffset > maxDigits) {
      return oldValue;
    }

    double value = double.parse(newValue.text);
    final formatter = NumberFormat("###,###,###", "id_ID");
    String newText = "Rp " + formatter.format(value);
    return newValue.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: newText.length));
  }
}

class NumberFormatter {
  static convertNumber(num value) {
    final formatter = NumberFormat("#,###");
    String newText = formatter.format(value);
    return newText;
  }

  static convertNumberDecimal(num value) {
    final formatter = NumberFormat("#,###.##");
    String newText = formatter.format(value);
    return newText;
  }

  static numberWithSymbol(num value) {
    final formatter = NumberFormat("###,###,###");
    String text = formatter.format(value);
    if (value >= 1000000000000) {
      num newValue = value / 1000000000000;
      text = newValue.toStringAsFixed(2) + "T";
    } else if (value >= 1000000000) {
      num newValue = value / 1000000000;
      text = newValue.toStringAsFixed(2) + "B";
    } else if (value >= 1000000) {
      num newValue = value / 1000000;
      text = newValue.toStringAsFixed(2) + "M";
    }
    // else if (value >= 10000) {
    //   num newValue = value / 1000;
    //   text = newValue.toStringAsFixed(2) + "K";
    // }
    return text;
  }
}

penyebut(var val) {
  double nilai = val.abs();
  var huruf = [
    "",
    "satu",
    "dua",
    "tiga",
    "empat",
    "lima",
    "enam",
    "tujuh",
    "delapan",
    "sembilan",
    "sepuluh",
    "sebelas"
  ];
  String temp = "";
  if (nilai < 12) {
    temp = " " + huruf[nilai.toInt()];
  } else if (nilai < 20) {
    temp = penyebut(nilai - 10) + " belas";
  } else if (nilai < 100) {
    temp = penyebut(nilai / 10) + " puluh" + penyebut(nilai % 10);
  } else if (nilai < 200) {
    temp = " seratus" + penyebut(nilai - 100);
  } else if (nilai < 1000) {
    temp = penyebut(nilai / 100) + " ratus" + penyebut(nilai % 100);
  } else if (nilai < 2000) {
    temp = " seribu" + penyebut(nilai - 1000);
  } else if (nilai < 1000000) {
    temp = penyebut(nilai / 1000) + " ribu" + penyebut(nilai % 1000);
  } else if (nilai < 1000000000) {
    temp = penyebut(nilai / 1000000) + " juta" + penyebut(nilai % 1000000);
  } else if (nilai < 1000000000000) {
    temp =
        penyebut(nilai / 1000000000) + " milyar" + penyebut(nilai % 1000000000);
  } else if (nilai < 1000000000000000) {
    temp = penyebut(nilai / 1000000000000) +
        " trilyun" +
        penyebut(nilai % 1000000000000);
  }
  return temp;
}

terbilang(var nilai) {
  try {
    String hasil = "";
    if (nilai < 0) {
      hasil = "minus " + penyebut(nilai);
    } else {
      hasil = penyebut(nilai);
    }
    return hasil.trim() + " rupiah";
  } catch (e) {
    return "";
  }
}

// number format
formatAngka(String angka) {
  final numberFormat = NumberFormat("#,##0");
  try {
    if (angka == "-") {
      return "-";
    } else {
      return numberFormat.parse(angka);
    }
  } catch (e) {
    return "-";
  }
}
