import 'package:flutter/material.dart';

class Connections extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Image.asset(
                'assets/icon/connection_empty.png',
                height: MediaQuery.of(context).size.height / 2,
                fit: BoxFit.fill,
              ),
            ),
            Text('Aduh, Putus', style: TextStyle(fontWeight: FontWeight.bold)),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
              child: Text(
                'Cek kuota internetmu atau koneksi Wi-Fimu dan coba lagi...',
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}
