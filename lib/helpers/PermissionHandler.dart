import 'package:permission_handler/permission_handler.dart';

class SantaraPermissionHandler {
  // nge-return nilai dari permission status, jika diijinkan true
  _permissionStatusHandler(PermissionStatus permissionStatus) {
    switch (permissionStatus) {
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  // ngedapetin status permission
  Future<bool> listenForPermissionStatus(
      PermissionGroup _permissionGroup) async {
    bool _status = false;
    try {
      final Future<PermissionStatus> statusFuture =
          PermissionHandler().checkPermissionStatus(_permissionGroup);
      await statusFuture.then((PermissionStatus status) {
        var boolStatus = _permissionStatusHandler(status);
        _status = boolStatus;
      });
    } catch (e) {
      // print(">> an error has occured : $e");
      _status = false;
    }
    return _status;
  }

  // ngerequest permission
  Future<bool> requestPermission(PermissionGroup permission) async {
    // print(">> Start requesting permission :");
    var result = await PermissionHandler().requestPermissions([permission]);
    // print(">> Request result : ");
    // print(result[permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }
}
