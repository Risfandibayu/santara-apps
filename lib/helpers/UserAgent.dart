import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:flutter_user_agent/flutter_user_agent.dart';
import 'package:santaraapp/utils/logger.dart';

class UserAgent {
  static Future<Map<String, dynamic>> headers() async {
    final storage = secureStorage.FlutterSecureStorage();
    try {
      await FlutterUserAgent.init();
      final token = await storage.read(key: 'token');
      final userAgent = await FlutterUserAgent.getPropertyAsync('userAgent');
      final appVersion =
          await FlutterUserAgent.getProperty('applicationVersion');
      final headers = {
        HttpHeaders.authorizationHeader: "Bearer $token",
        HttpHeaders.userAgentHeader: "$userAgent",
        "App-Version": "$appVersion",
      };
      return headers;
    } catch (e, stack) {
      santaraLog(e, stack);
      final token = await storage.read(key: 'token');
      final headers = {HttpHeaders.authorizationHeader: "Bearer $token"};
      return headers;
    }
  }

  static Future<Map<String, dynamic>> headersNoAuth() async {
    try {
      await FlutterUserAgent.init();
      final userAgent = await FlutterUserAgent.getPropertyAsync('userAgent');
      final appVersion =
          await FlutterUserAgent.getProperty('applicationVersion');
      final headers = {
        HttpHeaders.userAgentHeader: "$userAgent",
        "App-Version": "$appVersion",
      };
      return headers;
    } catch (e, stack) {
      santaraLog(e, stack);
      return null;
    }
  }
}
