import 'package:meta/meta.dart';
import 'package:dio/dio.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/utils/logger.dart';

class RestHelper {
  static Failure catchFailure({@required DioError error}) {
    try {
      logger.i(
          ">> Catch Failure Result <<\n${error.request.method}\n${error.request.uri}\n${error.response.data}");
      var response = error.response;
      switch (response.statusCode) {
        case 400:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "BAD REQUEST",
            data: response.data,
          );
          break;
        case 401:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "UNAUTHORIZED",
            data: response.data,
          );
          break;
        case 403:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: "FORBIDDEN",
            data: response.data,
          );
          break;
        case 404:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: "NOT FOUND",
            data: response.data,
          );
          break;
        case 405:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "METHOD_NOT_ALLOWED",
            data: response.data,
          );
          break;
        case 500:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: "SERVER_ERROR",
            data: response.data,
          );
          break;
        case 501:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "NOT_IMPLEMENTED",
            data: response.data,
          );
          break;
        case 502:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: "BAD_GATEWAY",
            data: response?.data,
          );
          break;
        case 503:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "SERVER_UNAVAILABLE",
            data: response.data,
          );
          break;
        case 504:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["meta"] != null
                ? response.data["meta"]["message"]
                : "GATEWAY_TIMEOUT",
            data: response.data,
          );
          break;
        default:
          return Failure(
            type: FailureType.serverError,
            apiStatus: response.statusCode,
            message: response.data["message"] ?? response.statusMessage,
            data: response.data,
          );
          break;
      }
    } catch (e, stack) {
      // print(">> ERROR : ${error.response}");
      // print(">> ERROR : $e");
      // print(">> STACK : $stack");
      return Failure(
        type: FailureType.serverError,
        apiStatus: error.response.statusCode,
        message: "${error.response}",
        data: null,
      );
    }
  }

  static Failure throwServerError(Response response) {
    return Failure(
      type: FailureType.serverError,
      apiStatus: response.statusCode,
      message: "SERVER_ERROR_OCCURED",
    );
  }

  static Failure throwLocalError({dynamic error, StackTrace stack}) {
    santaraLog(error, stack);
    return Failure(
      type: FailureType.localError,
      message: "LOCAL_ERROR_OCCURED",
    );
  }

  static Failure throwFailure(Failure error, {StackTrace stack}) {
    santaraLog(error, stack);
    printFailure(error);
    return Failure(
      type: error.type,
      apiStatus: error.apiStatus,
      message: error.message,
      data: error.data,
    );
  }
}
