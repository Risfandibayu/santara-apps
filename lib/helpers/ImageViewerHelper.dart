import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';

enum PathType {
  prospektus,
  emitenPicture,
  traderPhoto,
  traderIdCardPhoto,
  traderVerificationPhoto,
  depositPhoto,
  confirmationPhoto,
}

class GetAuthenticatedFile {
  static String convertUrl({@required String image, @required PathType type}) {
    try {
      String replacementUrl = "";
      String path = "";
      switch (type) {
        case PathType.prospektus:
          replacementUrl = "";
          path = "prospektus";
          break;
        case PathType.emitenPicture:
          replacementUrl = "";
          path = "emiten_picture";
          break;
        case PathType.traderPhoto:
          replacementUrl = "/uploads/trader/";
          path = "trader-photo";
          break;
        case PathType.traderIdCardPhoto:
          replacementUrl = "/uploads/trader/";
          path = "trader-idcard-photo";
          break;
        case PathType.traderVerificationPhoto:
          replacementUrl = "/uploads/trader/";
          path = "trader-verif-photo";
          break;
        case PathType.depositPhoto:
          replacementUrl = "";
          path = "deposit";
          break;
        case PathType.confirmationPhoto:
          replacementUrl = "";
          path = "konfirmasi";
          break;
        default:
          replacementUrl = "";
          path = "";
      }
      final String file =
          image.replaceAll("null", "").replaceAll("$replacementUrl", "");
      final String url = "$apiLocalImage/uploads/$path/$file";
      // logger.i(">> uri : $url");
      // return url;
      return image;
    } catch (e, stack) {
      santaraLog(e, stack);
      return "";
    }
  }
}
