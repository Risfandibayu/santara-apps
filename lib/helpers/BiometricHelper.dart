import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';

class BiometricHelper {
  final LocalAuthentication _localAuthentication = LocalAuthentication();

  // Ngecek apakah FP ada
  Future<bool> isBiometricAvailable() async {
    bool isAvailable = false;
    try {
      isAvailable = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      // print(e);
      return false;
    }

    // isAvailable
    //     ? print('Biometric is available!')
    //     : print('Biometric is unavailable.');

    return isAvailable;
  }

  // Ngedapetin jenis autentikasi ( FP or FR )
  Future<List<BiometricType>> getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      // print(e);
    }
    // print(listOfBiometrics);
    return listOfBiometrics;
  }

  // Ngelakuin otentikasi via jenis otentikasi yang terpilih Face Recognition atau Finger Print
  Future<bool> authenticateUser() async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason:
            "Letakkan sidik jari di perangkat Anda untuk melanjutkan",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      // print(e);
      return false;
    }

    // isAuthenticated
    //     ? print('User is authenticated!')
    //     : print('User is not authenticated.');

    return isAuthenticated;
  }


}
