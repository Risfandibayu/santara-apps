import 'package:string_validator/string_validator.dart';

class PralistingValidationHelper {
  static int debounceDuration = 200;
  // validator
  static String companyName(String value) {
    // await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length < 3) {
      return "Nama perusahaan minimal 3 karakter";
    } else if (value.length > 50) {
      return "Nama perusahaan maksimal 50 karakter";
    } else if (value.isEmpty) {
      return "Nama perusahaan tidak boleh kosong";
    } else if (value.contains(RegExp(r'[0-9]'), 1)) {
      return "Nama perusahaan tidak boleh menggunakan angka";
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(value)) {
        return null;
      } else {
        return "Nama tidak boleh mengandung simbol";
      }
    }
  }

  static String position(String value) {
    // await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length > 50) {
      return "Nama jabatan maksimal 50 karakter";
    } else if (value.isEmpty) {
      return "Nama jabatan tidak boleh kosong";
    } else {
      return null;
    }
  }

  static String fullName(String value) {
    // await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length > 50) {
      return "Nama maksimal 50 karakter";
    } else if (value.contains(RegExp(r'[0-9]'), 0)) {
      return "Nama tidak boleh menggunakan angka";
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(value)) {
        return null;
      } else if (value.isEmpty) {
        return null;
      } else if (value == null) {
        return null;
      } else {
        return "Nama tidak boleh mengandung simbol";
      }
    }
  }

  static String required(dynamic value) {
    if (value == null ||
        value == false ||
        ((value is Iterable || value is String || value is Map) &&
            value.length == 0)) {
      return "Data tidak boleh kosong";
    }
    return null;
  }

  static Future<String> ownerName(String value) async {
    // await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length > 50) {
      return "Nama maksimal 50 karakter";
    } else if (value.contains(RegExp(r'[0-9]'), 0)) {
      return "Nama tidak boleh menggunakan angka";
    } else {
      if (RegExp(r'^[a-zA-Z\d\-_\s]+$').hasMatch(value)) {
        return null;
      } else {
        return "Nama tidak boleh mengandung simbol";
      }
    }
  }

  static String stockTotalPercentage(String value) {
    try {
      value = value.replaceAll(",", ".");
      double numval = double.parse(value ?? 0);
      if (numval > 100) {
        return "Maksimal nilai yang dapat diinput adalah 100";
      } else if (numval <= 0) {
        return "Nilai harus lebih dari 0";
      } else {
        return null;
      }
    } catch (e) {
      return "Nilai yang anda masukan tidak valid!";
    }
  }

  static Future<String> trademark(String value) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length == 0) {
      return null;
    } else if (value.length < 3) {
      return "Merek dagang minimal 3 karakter";
    } else if (value.length > 50) {
      return "Merek dagang maksimal 50 karakter";
    } else if (value.isEmpty) {
      return "Merek dagang tidak boleh kosong";
    } else {
      return null;
    }
  }

  static Future<String> address(String value) async {
    await Future.delayed(Duration(milliseconds: debounceDuration));
    if (value.length < 3) {
      return "Alamat lengkap minimal 3 karakter";
    } else if (value.length > 500) {
      return "Alamat lengkap maksimal 500 karakter";
    } else {
      return null;
    }
  }

  static String max5000(String value) {
    if (value.length > 5000) {
      return "Maksimal input adalah 5000 karakter!";
    } else {
      return null;
    }
  }

  static String max6000(String value) {
    if (value.length > 6000) {
      return "Maksimal input adalah 6000 karakter!";
    } else {
      return null;
    }
  }

  static String validLink(String value) {
    if (value.isEmpty) {
      return null;
    } else if (isURL(value)) {
      return null;
    } else {
      return "Link yang anda masukan tidak valid!";
    }
  }

  static String validYoutubeLink(String value) {
    RegExp regExp = new RegExp(
      r"(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?",
      caseSensitive: false,
      multiLine: false,
    );
    if (value.length < 1 || value == null) {
      return "Url video tidak boleh kosong";
    } else if (regExp.hasMatch(value)) {
      return null;
    } else {
      return "Url video tidak valid!";
    }
  }
}
