import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CurrencyFormat extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    double value = double.parse(newValue.text);

    final money = NumberFormat("###,###,###", "en_us");

    String newText = money.format(value);

    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}

class DigitParser {
  static int parse(String value) {
    try {
      String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
      double _doubleValue = double.parse(_onlyDigits);
      return _doubleValue.toInt();
    } catch (e) {
      return 0;
    }
  }
}
