import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

// overlay type buat nandain jenis-jenis notifikasi overlay
// passed = Pengajuan bisnis Anda lolos ketahap berikutnya.Silahkan cek E-mail Anda untuk info selengkapnya. Cek E-Mail
// most_submitted = Selamat, pengajuan bisnis Anda masuk #1 paling banyak diajukan
// in_process = Pengajuan bisnis Anda sedang diverifikasi oleh Admin kami.Silahkan cek E-mail Anda untuk info selengkapnya. Cek E-Mail
// rejected = Pengajuan bisnis Anda stidak lolos verifikasi.Silahkan cek E-mail Anda untuk info selengkapnya. Cek E-Mail
// submitted = 1 Orang mengajukan bisnis Anda
// commented = 1 Orang memberikan ulasan bisnis Anda.
// liked = 1 Orang menyukai bisnis Anda.

enum OverlayType {
  passed,
  most_submitted,
  in_process,
  rejected,
  submitted,
  commented,
  liked
}

class OverlayNotifications {
  static showSantaraOverlayNotification(String message, String type,
      {Function onTap}) {
    IconData _icon;
    switch (type) {
      // case OverlayType.passed:
      //   _icon = Icons.check_circle_outline;
      //   break;
      // case OverlayType.most_submitted:
      //   _icon = Icons.trending_up;
      //   break;
      // case OverlayType.in_process:
      //   _icon = Icons.access_time;
      //   break;
      // case OverlayType.rejected:
      //   _icon = Icons.close;
      //   break;
      case "vote pengajuan bisnis":
        _icon = Icons.person;
        break;
      case "ulasan pengajuan bisnis":
        _icon = Icons.chat_bubble_outline;
        break;
      case "menyukai pengajuan bisnis":
        _icon = Icons.favorite_border;
        break;
      default:
        _icon = Icons.notifications;
        break;
    }

    showOverlayNotification((context) {
      return Card(
        margin: const EdgeInsets.symmetric(horizontal: 4),
        color: Color(0xffEEF1FF),
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              ListTile(
                leading: SizedBox.fromSize(
                    size: const Size(40, 40),
                    child: ClipOval(
                        child: Icon(
                      _icon,
                      color: Colors.black,
                    ))),
                onTap: onTap,
                // title: Text('FilledStacks'),
                title: Text(
                  '$message',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {
                    OverlaySupportEntry.of(context).dismiss();
                  },
                  child: Container(
                    height: 40,
                    width: 40,
                    child: Center(
                      child: Icon(
                        Icons.close,
                        size: 12,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }, duration: Duration(milliseconds: 8000));
  }
}

// Container(
//         decoration: BoxDecoration(
//             color: Color(0xffEEF1FF),
//             borderRadius: BorderRadius.all(Radius.circular(10)),
//             boxShadow: [
//               BoxShadow(
//                 color: Color.fromRGBO(202, 202, 202, 0.25),
//                 blurRadius: 10,
//                 offset: Offset(0, 10),
//               )
//             ]),
//         // padding: EdgeInsets.only(top: 50),
//         child: Stack(
//           children: <Widget>[
//             ListTile(
//               leading: Icon(_icon),
//               title: Text("$message"),
//             ),
//             Align(
//               alignment: Alignment.topRight,
//               child: Icon(Icons.close),
//             )
//           ],
//         ),
//       );
