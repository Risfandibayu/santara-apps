import 'package:santaraapp/models/listing/ListingDummies.dart';

class RegistrationSteps {
  bool identitasPenerbit;
  bool lampiran;
  bool media;
  bool infoFinansial;
  bool infoNonFinansial;

  RegistrationSteps(
      {this.identitasPenerbit,
      this.lampiran,
      this.media,
      this.infoFinansial,
      this.infoNonFinansial});
}

class RegistrationHelper {
  static RegistrationSteps steps = RegistrationSteps(
      identitasPenerbit: false,
      lampiran: false,
      media: false,
      infoFinansial: false,
      infoNonFinansial: false);
  static EmitenDetail emiten = EmitenDetail(assets: []);
}
