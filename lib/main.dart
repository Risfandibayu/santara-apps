import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:catcher/catcher.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/date_symbol_data_local.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/pages/ExpiredSession.dart';

// overlay notification
import 'package:overlay_support/overlay_support.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/account/pages/SelfieKYCUI.dart';
import 'package:url_launcher/url_launcher.dart';

import 'App.dart';
import 'blocs/simple_bloc_observer.dart';

// import 'package:root_checker/root_checker.dart';
// import 'package:trust_fall/trust_fall.dart';
// import 'package:flutter_jailbreak_detection/flutter_jailbreak_detection.dart';
// component
import 'features/deposit/main/presentation/pages/main_deposit_page.dart';
import 'features/dividen/presentation/pages/main_dividen_page.dart';
import 'features/withdraw/presenstation/pages/main_withdraw_page.dart';
import 'helpers/Connections.dart';
import 'helpers/OverlayNotifications.dart';
import 'helpers/SantaraReportHandler.dart';
import 'injector_container.dart' as di;
import 'models/Menu.dart';
import 'models/User.dart' as user;
import 'pages/Disclaimer.dart';
import 'pages/Home.dart';
import 'pages/Journey.dart';
import 'pages/KycDynamicLink.dart';
import 'pages/Login.dart';
import 'pages/Maintenance.dart';
import 'pages/Notification.dart';
import 'pages/SyaratDanKetentuan.dart';
import 'pages/SyaratKetentuan.dart';
import 'pages/Trading.dart';
import 'pages/Wallet.dart';
import 'services/api_service.dart';
import 'services/security/pin_service.dart';
import 'utils/api.dart';
import 'utils/fcm_topics.dart';
import 'utils/helper.dart';
import 'utils/logger.dart';
import 'utils/providers.dart';
import 'widget/home/AllPenerbitList.dart';
import 'widget/home/InfoAppUI.dart';
import 'widget/listing/dashboard/PraListingUI.dart';
import 'widget/login/forgotpassword/Forgot.dart';
import 'widget/login/register/Register.dart';
import 'widget/market/detail_saham_view.dart';
import 'widget/market/home_market.dart';
import 'widget/market/list_saham_view.dart';
import 'widget/market/orderbook_view.dart';
import 'widget/market/portofolio_view.dart';
import 'widget/market/transaksi_view.dart';
import 'widget/market/watchlist_view.dart';
import 'widget/portfolio/PortfolioListUI.dart';
import 'widget/pralisting/detail/presentation/pages/pralisting_detail_page.dart';
import 'widget/security_token/pin/PreCreationUI.dart';
import 'widget/security_token/pin/SecurityPinUI.dart';
import 'widget/security_token/user/change_email/ChangeEmailUI.dart';
import 'widget/security_token/user/change_password/ChangePasswordUI.dart';
import 'widget/token/MainHistoryTransaction.dart';
import 'widget/video/VideoUI.dart';
import 'widget/widget/ParseSlug.dart';
import 'widget/widget/WebView.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  Bloc.observer = SimpleBlocObserver();
  debugDefaultTargetPlatformOverride = Platform.isAndroid
      ? TargetPlatform.android
      : Platform.isIOS
          ? TargetPlatform.iOS
          : null; // jika android jadiin TargetPlatform.android

  /// STEP 1. Create catcher configuration.
  /// Debug configuration with dialog report mode and console handler. It will show dialog and once user accepts it, error will be shown   /// in console.
  CatcherOptions debugOptions =
      CatcherOptions(SilentReportMode(), [SantaraConsoleHandler()]);

  /// Release configuration. Same as above, but once user accepts dialog, user will be prompted to send email with crash to support.
  CatcherOptions releaseOptions = CatcherOptions(SilentReportMode(), [
    // EmailManualHandler(["all.tech@santara.co.id"])
    SantaraConsoleHandler()
    // SantaraReportHandler(
    //   "smtp.mailgun.org",
    //   587,
    //   "crashlytic@mg.santara.co.id",
    //   "Crash Report Santara Apps",
    //   "b12f0af4017ad1a85cd6e53deb5d0bfa-f7d0b107-13f9576b",
    //   ["all.tech@santara.co.id"],
    //   enableDeviceParameters: true,
    //   enableStackTrace: true,
    //   enableCustomParameters: true,
    //   enableApplicationParameters: true,
    //   sendHtml: true,
    //   printLogs: true,
    // )
  ]);

  /// STEP 2. Pass your root widget (MyApp) along with Catcher configuration:
  Catcher(
    MyApp(),
    debugConfig: debugOptions,
    releaseConfig: releaseOptions,
    enableLogger: false,
  );

  kNotificationSlideDuration = const Duration(milliseconds: 500);
  kNotificationDuration = const Duration(milliseconds: 1500);
  await initializeDateFormatting('id', '');
  bool isInDebugMode = false;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Crashlytics.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  runZonedGuarded(() {
    runApp(MyApp());
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone.');
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //with WidgetsBindingObserver {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);
  final storage = new FlutterSecureStorage();
  var token;
  var datas;
  var refreshToken;
  Menu menu;
  Future<void> _initializeFlutterFireFuture;

  Future getMenu() async {
    final response = await http.get("$apiLocal/information/menu");
    if (response.statusCode == 200) {
      setState(() => menu = menuFromJson(response.body));
    }
  }

  // Define an async function to initialize FlutterFire
  Future<void> _initializeFlutterFire() async {
    // Wait for Firebase to initialize
    await Firebase.initializeApp();

    /// if isProduction true
    /// Force enable crashlytics collection enabled if we're testing it.
    await FirebaseCrashlytics.instance
        .setCrashlyticsCollectionEnabled(isProduction);

    // Pass all uncaught errors to Crashlytics.
    Function originalOnError = FlutterError.onError as Function;
    FlutterError.onError = (FlutterErrorDetails errorDetails) async {
      await FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
      // Forward to original handler.
      originalOnError(errorDetails);
    };
  }

  @override
  void initState() {
    super.initState();
    _initializeFlutterFireFuture = _initializeFlutterFire();
    getMenu();
    _initializeFlutterFire();
  }

  @override
  Widget build(BuildContext context) {
    /// To set orientation always portrait
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    ///Set color status bar
    if (Platform.isIOS) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.white, //or set color with: Color(0xFF0000FF)
      ));
    }

// jika ingin mengaktifkan overlay notification, set wiget ini sebagai parent dari material app
// return OverlaySupport(
//         child:

    return MultiBlocProvider(
      providers: providers,
      child: OverlaySupport(
        child: MaterialApp(
          navigatorKey: Catcher.navigatorKey,
          builder: (_, child) {
            return MediaQuery(
                data: MediaQuery.of(_).copyWith(textScaleFactor: 1.0),
                child: child);
          },
          title: 'Santara',
          debugShowCheckedModeBanner: false,
          navigatorObservers: <NavigatorObserver>[observer],
          theme: ThemeData(
              primarySwatch: Colors.red,
              fontFamily: 'Nunito',
              backgroundColor: Colors.white),
          home: SplashScreen(),
          routes: {
            '/register': (context) => Register(),
            '/forgot': (context) => Forgot(),
            '/kycDynamicLink': (context) => KycDynamicLinkUI(),
            '/index': (context) => Home(),
            '/deposit': (context) => MainDepositPage(),
            '/wallet': (context) => Wallet(),
            '/login': (context) => Login(),
            '/tnc': (context) => SyaratDanKetentuan(),
            '/withdraw': (context) => MainWithdrawPage(),
            '/journey': (context) => Journey(),
            '/portfolio': (context) => PortfolioListUI(),
            '/trading': (context) => Trading(),
            '/dividend': (context) => MainDividenPage(),
            '/token': (context) => MainHistoryTransaction(),
            '/notification': (context) => Notif(),
            '/disclaimer': (context) => Disclaimer(
                  text: menu.disclaimer,
                ),
            '/snkPemodal': (context) =>
                SyaratKetentuan(urlPdf: menu.sKPemodal, data: 0),
            '/snkPenerbit': (context) =>
                SyaratKetentuan(urlPdf: menu.sKPenerbit, data: 1),
            '/kebijakan': (context) =>
                SyaratKetentuan(urlPdf: menu.kebijakan, data: 2),
            '/berita': (_) =>
                WebViewWidget(appBarName: "Berita Santara", url: menu.berita),
            '/tentang': (_) =>
                WebViewWidget(appBarName: "Tentang Santara", url: menu.about),
            '/petunjuk': (_) => WebViewWidget(
                appBarName: "Cara Investasi",
                url: 'https://santara.co.id/mulai-investasi'),
            '/faq': (_) => WebViewWidget(appBarName: "FAQ", url: menu.faq),
            '/syarat-ketentuan': (_) => WebViewWidget(
                appBarName: "Syarat dan Ketentuan",
                url: 'https://santara.co.id/t&c/'),
            '/fintech': (_) => WebViewWidget(
                appBarName: "Santara Website View",
                url: 'https://santara.co.id/welcome/aftech'),
            '/kominfo': (_) => WebViewWidget(
                appBarName: "Santara Website View",
                url: 'https://santarax.com/welcome/kominfo'),
            '/pse': (_) => WebViewWidget(
                appBarName: "Santara Website View",
                url: 'https://pse.kominfo.go.id/sistem/1613'),
            '/app_info': (context) => InfoAppUI(),
            '/pralisting': (context) => PraListingUI(),
            '/videos': (context) => VideoUI(),
            '/change_password': (context) => ChangePasswordUI(),
            '/change_email': (context) => ChangeEmailUI(),
            '/home_market': (context) => HomeMarket(),
            '/fotokyc': (context) => SelfieKycUI()
          },
        ),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final storage = new FlutterSecureStorage();
  final apiService = ApiService();
  var token;
  var datas;
  var refreshToken;
  var spesificPage; // 0 -> home 1 -> detail emiten, 2 -> detail emiten pralisting -> 3 -> list bisnis
  var uuidPage;
  String dynamicLink = "";
  PermissionStatus _status;
  //
  // void _updateStatus(PermissionStatus status) {
  //   if (status != _status) {
  //     // check status has changed
  //     setState(() {
  //       _status = status; // update
  //     });
  //   } else {
  //     if (status != PermissionStatus.granted) {
  //       PermissionHandler().requestPermissions(
  //           [PermissionGroup.locationWhenInUse]).then(_onStatusRequested);
  //     }
  //   }
  // }
  //
  // void _askPermission() {
  //   PermissionHandler().requestPermissions(
  //       [PermissionGroup.locationWhenInUse]).then(_onStatusRequested);
  // }
  //
  // void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
  //   final status = statuses[PermissionGroup.locationWhenInUse];
  //   if (status != PermissionStatus.granted) {
  //     // On iOS if "deny" is pressed, open App Settings
  //     PermissionHandler().openAppSettings();
  //   } else {
  //     _updateStatus(status);
  //   }
  // }

  Future<String> getPublicIP() async {
    try {
      const url = 'https://api.ipify.org';
      var response = await http.get(url);
      if (response.statusCode == 200) {
        // The response body is the IP in plain text, so just
        // return it as-is.
        return response.body;
      } else {
        // The request failed with a non-200 code
        // The ipify.org API has a lot of guaranteed uptime
        // promises, so this shouldn't ever actually happen.
        // print(response.statusCode);
        // print(response.body);
        return null;
      }
    } catch (e) {
      // Request failed due to an error, most likely because
      // the phone isn't connected to the internet.
      // print(e);
      return null;
    }
  }

  void fcmSubscribe() {
    _firebaseMessaging.subscribeToTopic(FcmTopics.newEmiten);
    _firebaseMessaging.subscribeToTopic(FcmTopics.lihatListPenerbit);
    _firebaseMessaging.subscribeToTopic(FcmTopics.lihatPenerbit);
    _firebaseMessaging.subscribeToTopic(FcmTopics.peluncuranBisnisBaru);
  }

  Future<String> _handleOnMessage(Map<String, dynamic> message) async {
    try {
      if (Platform.isAndroid) {
        logger.i(">> Message : $message");
        if (message['data']['action'] != null &&
            message['data']['action'] != 'no') {
          var url = message['data']['action'];
          return url;
        } else {
          return null;
        }
      } else if (Platform.isIOS) {
        if (message['action'] != null && message['action'] != 'no') {
          var url = message['action'];
          return url;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      return null;
    }
  }

  void _launchUrl(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: false);
      } else {
        throw 'Cannot launch $url';
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  Future<void> setupFirebase() async {
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidRecieveLocalNotification,
      defaultPresentSound: true,
      defaultPresentAlert: true,
      defaultPresentBadge: true,
    );
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: onSelectNotification,
    );

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        _getMsg(message);
        OverlayNotifications.showSantaraOverlayNotification(
          message['notification']['body'],
          message['data']['action'],
          onTap: () async {
            await _handleOnMessage(message).then((value) {
              _launchUrl(value);
            });
          },
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        chatMsg = message;
        await _handleOnMessage(message).then((value) {
          setState(() {
            dynamicLink = value;
          });
        });
      },
      onResume: (Map<String, dynamic> message) async {
        chatMsg = message;
        await _handleOnMessage(message).then((value) {
          _launchUrl(value);
        });
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
        sound: true,
        badge: true,
        alert: true,
        provisional: true,
      ),
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      debugPrint("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      logger.i(">> Firebase Token : $token");
      setState(() {
        storage.write(key: "tokenFirebase", value: token);
        // PreferenceHelper.saveString(PreferenceHelper.tokenFirebaseKey, token);
        tokenFirebase = token;
      });

      var updateToken = await apiService.updateFirebaseToken(token);
      if (updateToken.statusCode == 401) {
        NavigatorHelper.pushToExpiredSession(context);
      }
      // debugPrint('firebase token: $token');
    });
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      var parsedPayload = json.decode(payload);
      // logger.i(">> Payload : $parsedPayload");
      // setState(() => Helper.pickSomething = 1);
      await _handleOnMessage(parsedPayload).then((value) async {
        if (value != null) {
          _launchUrl(value);
        } else {
          await _select(payload);
        }
      });
    }
  }

  Future _select(String type) async {
    await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new Home()),
    );
    // setState(() => Helper.pickSomething = 0);
  }

  Future<void> onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    // await _showNotification();
    logger.i(">> Payload :$payload");
  }

  Future<void> _showNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      'your channel description',
      importance: Importance.max,
      priority: Priority.high,
      styleInformation: BigTextStyleInformation(''),
      playSound: true,
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );
    // var platformChannelSpecifics = NotificationDetails(
    //     androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    String title = "";
    String body = "";
    if (Platform.isAndroid) {
      title = '${message['notification']['title']}';
      body = '${message['notification']['body']}';
    } else {
      title = '${message['title']}';
      body = '${message['body']}';
    }

    await flutterLocalNotificationsPlugin.show(
      0,
      title,
      body,
      platformChannelSpecifics,
      payload: json.encode(message),
    );

    // await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((value) {
    //   value.
    // })
  }

  void _getMsg(Map<String, dynamic> message) {
    _showNotification(message);
  }

  Future<bool> _isPinExist() async {
    bool ret = false;
    try {
      final _pinService = PinService();
      await _pinService.getExistPIN().then((res) {
        if (res.statusCode == 200) {
          var parsed = json.decode(res.body);
          ret = parsed["message"];
        } else {
          ret = false;
        }
      });
    } catch (e) {
      return false;
    }
    return ret;
  }

  startTime() async {
    // bool isJailBroken = await FlutterJailbreakDetection.jailbroken; // If build for android
    // bool isJailBroken = await TrustFall.isJailBroken; // If build for iOS
    // bool isDeviceRooted = await RootChecker.isDeviceRooted;
    // if (isJailBroken) {
    //   showDialog(
    //       context: context,
    //       builder: (BuildContext context) {
    //         return WillPopScope(
    //           onWillPop: () {
    //             return SystemChannels.platform
    //                 .invokeMethod('SystemNavigator.pop');
    //           },
    //           child: AlertDialog(
    //             shape: RoundedRectangleBorder(
    //                 borderRadius: BorderRadius.circular(10)),
    //             title: Image.asset('assets/icon/update.png'),
    //             content: Column(
    //               mainAxisSize: MainAxisSize.min,
    //               children: <Widget>[
    //                 Container(
    //                     margin: EdgeInsets.only(top: 20),
    //                     child: Text('Sorry !', style: TextStyle(fontSize: 20))),
    //                 Container(
    //                   margin: EdgeInsets.only(top: 10),
    //                   child: Text(
    //                     'Anda tidak dapat masuk ke dalam aplikasi',
    //                     textAlign: TextAlign.center,
    //                   ),
    //                 ),
    //                 Container(
    //                   margin: EdgeInsets.only(top: 10),
    //                   child: Text(
    //                       'Device anda tidak support dengan aplikasi karena tidak sesuai dengan standar keamanan',
    //                       textAlign: TextAlign.center),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         );
    //       });
    // } else {
    //   if (isDeviceRooted) {
    //     showDialog(
    //         context: context,
    //         builder: (BuildContext context) {
    //           return WillPopScope(
    //             onWillPop: () {
    //               return SystemChannels.platform
    //                   .invokeMethod('SystemNavigator.pop');
    //             },
    //             child: AlertDialog(
    //               shape: RoundedRectangleBorder(
    //                   borderRadius: BorderRadius.circular(10)),
    //               title: Image.asset('assets/icon/update.png'),
    //               content: Column(
    //                 mainAxisSize: MainAxisSize.min,
    //                 children: <Widget>[
    //                   Container(
    //                       margin: EdgeInsets.only(top: 20),
    //                       child:
    //                           Text('Sorry !', style: TextStyle(fontSize: 20))),
    //                   Container(
    //                     margin: EdgeInsets.only(top: 10),
    //                     child: Text(
    //                       'Anda tidak dapat masuk ke dalam aplikasi',
    //                       textAlign: TextAlign.center,
    //                     ),
    //                   ),
    //                   Container(
    //                     margin: EdgeInsets.only(top: 10),
    //                     child: Text(
    //                         'Device anda tidak support dengan aplikasi karena memiliki akses root yang dilarang dalam standar keamanan',
    //                         textAlign: TextAlign.center),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //           );
    //         });
    //   } else {
    return new Timer(Duration(seconds: 3), () => launchPage);
    //   }
    // }
  }

  Future getRemoteConfig(String ip, {String dynamicLink}) async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    await remoteConfig.fetch(expiration: Duration(seconds: 0));
    await remoteConfig.activateFetched();
    final isMaintenanceForAndroid = remoteConfig.getBool(isMaintenanceAndroid);
    final isMaintenanceForIOS = remoteConfig.getBool(isMaintenanceIOS);
    final ipWhitelist = remoteConfig.getString(whitelistIp);
    if (ipWhitelist.contains(ip)) {
      Timer(Duration(seconds: 1),
          () => launchPage(false, dynamicLink: dynamicLink));
    } else {
      if (Platform.isAndroid) {
        if (isMaintenanceForAndroid) {
          Timer(Duration(seconds: 1), () => launchPage(true, dynamicLink: ''));
        } else {
          Timer(
            Duration(seconds: 1),
            () => launchPage(false, dynamicLink: dynamicLink),
          );
        }
      }
      if (Platform.isIOS) {
        if (isMaintenanceForIOS) {
          Timer(Duration(seconds: 1), () => launchPage(true, dynamicLink: ''));
        } else {
          Timer(
            Duration(seconds: 1),
            () => launchPage(false, dynamicLink: dynamicLink),
          );
        }
      }
    }
  }

  void launchPage(bool isMaintenance, {String dynamicLink}) async {
    try {
      if (isMaintenance) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Maintenance(),
          ),
        );
      } else {
        if (Helper.check) {
          bool pin = await _isPinExist();
          if (pin) {
            // jika sudah login dan sudah create pin
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => SecurityPinUI(
                  showBackButton: true,
                  onSuccess: (pin, finger) async {
                    try {
                      if (Helper.dynamicLinkData != null) {
                        setSpesificToOpen(Helper.dynamicLinkData);
                      } else {
                        Helper.isOnHomePage = true;
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Main(
                              spesificPage: spesificPage,
                              uuidPage: uuidPage,
                              dynamicLink: dynamicLink,
                            ),
                          ),
                        );
                      }
                    } catch (e, stack) {
                      santaraLog(e, stack);
                    }
                  },
                  type: SecurityType.check,
                  title: "Masukan PIN Anda",
                ),
              ),
            );
          } else {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => PreCreationUI(
                  onCreate: () {
                    if (Helper.dynamicLinkData != null) {
                      setSpesificToOpen(Helper.dynamicLinkData);
                    } else {
                      Helper.isOnHomePage = true;
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Main(
                            spesificPage: spesificPage,
                            uuidPage: uuidPage,
                            dynamicLink: dynamicLink,
                          ),
                        ),
                      );
                    }
                  },
                ),
              ),
            );
          }
        } else {
          Helper.isOnHomePage = true;
          if (Helper.dynamicLinkData != null) {
            setSpesificToOpen(Helper.dynamicLinkData);
          } else {
            await Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => Main(
                  spesificPage: spesificPage,
                  uuidPage: uuidPage,
                  dynamicLink: dynamicLink,
                ),
              ),
            );
          }
        }
      }
    } on TimeoutException catch (_) {
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (_) => Connections()), (route) => false);
    }
  }

  Future getLocalStorage() async {
    token = await storage.read(key: "token");
    if (token != null) {
      datas = user.userFromJson(await storage.read(key: 'user'));
      refreshToken = await storage.read(key: "refreshToken");
      Helper.check = true;
      Helper.username = datas.trader == null ? "" : datas.trader.name;
      Helper.email = datas.email;
      Helper.refreshToken = refreshToken;
      Helper.userId = datas.id;
    } else {
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.userId = 0;
      Helper.refreshToken = '';
      Helper.notif = 0;
    }
  }

  Future initDynamicLinks() async {
    try {
      final PendingDynamicLinkData data =
          await FirebaseDynamicLinks.instance.getInitialLink();
      final PendingDynamicLinkData deepLink = data;
      // final sets = ModalRoute.of(context).settings;
      // logger.i(">> Current Route : ${sets.name}");
      // Toast.show(data.link.toString(), context, duration: 3);
      if (deepLink.link != null) {
        // Navigator.pushNamed(context, deepLink.path);
        // mark the state is active (currently user on home page)
        Helper.dynamicLinkData = deepLink;
        if (Helper.isOnHomePage) {
          setSpesificToOpen(deepLink);
        }
        Helper.isOnHomePage = true;
      }

      FirebaseDynamicLinks.instance.onLink(
          onSuccess: (PendingDynamicLinkData res) async {
        final Uri deepLink = res.link;
        if (deepLink != null) {
          Helper.dynamicLinkData = res;
          // Navigator.pushNamed(context, deepLink.path);
          // mark the state is active (currently user on home page)
          if (Helper.isOnHomePage) {
            setSpesificToOpen(res);
          }
          Helper.isOnHomePage = true;
        }
      }, onError: (OnLinkErrorException e) async {
        // print('onLinkError');
        // print(e.message);
      });
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  void setSpesificToOpen(PendingDynamicLinkData data) async {
    try {
      var deepLink = data.link;
      var stringLink = deepLink.toString();
      var arrayLink = stringLink.split("/");
      Helper.dynamicLinkData = null;

      Catcher.navigatorKey.currentState.pushReplacement(
        MaterialPageRoute(
          builder: (context) => Main(spesificPage: 0),
        ),
      );

      if (stringLink.contains("bisnis/detail")) {
        // to open detail bisnis
        Catcher.navigatorKey.currentState.push(
          MaterialPageRoute(
            builder: (context) => ParseSlug(slug: arrayLink.last),
          ),
        );
      } else if (stringLink.contains("pra-listing/detail")) {
        Catcher.navigatorKey.currentState.push(
          MaterialPageRoute(
            builder: (context) => PralistingDetailPage(
              status: 2,
              uuid: arrayLink.last,
              position: 0,
            ),
          ),
        );
      } else if (stringLink.contains("list-bisnis")) {
        Catcher.navigatorKey.currentState.push(
          MaterialPageRoute(
            builder: (context) => AllPenerbitList(),
          ),
        );
      } else if (stringLink.contains("verify_email")) {
        Catcher.navigatorKey.currentState.push(
          MaterialPageRoute(
            builder: (context) => Login(isFromDynamicLink: 1),
          ),
        );
      } else if (stringLink.contains("user/dividend")) {
        Catcher.navigatorKey.currentState.pushNamed('/dividend');
      } else if (stringLink.contains("user/deposit")) {
        Catcher.navigatorKey.currentState.pushNamed('/deposit');
      } else if (stringLink.contains("user/withdraw")) {
        Catcher.navigatorKey.currentState.pushNamed('/withdraw');
      } else if (stringLink.contains("user/portofolio")) {
        Catcher.navigatorKey.currentState.pushNamed('/portfolio');
      } else if (stringLink.contains("user/video")) {
        Catcher.navigatorKey.currentState.pushNamed('/video');
      } else if (stringLink.contains("store")) {
        if (Platform.isIOS) {
          await launch('https://santara.co.id/ios', forceSafariVC: false);
        } else {
          await launch('https://santara.co.id/android', forceSafariVC: false);
        }
      } else if (stringLink.contains("kyc")) {
        Catcher.navigatorKey.currentState.pushNamed('/kycDynamicLink');
      } else if (stringLink == "$apiMarket/") {
        /// home market
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
      } else if (stringLink.contains("list-saham") &&
          stringLink.contains("market")) {
        /// list saham market
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState
            .push(MaterialPageRoute(builder: (context) => ListSahamView()));
      } else if (stringLink.contains("watchlist") &&
          stringLink.contains("market")) {
        /// watchlist
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState
            .push(MaterialPageRoute(builder: (context) => WatchlistView()));
      } else if (stringLink.contains("transaction") &&
          stringLink.contains("market")) {
        /// transaksi
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState
            .push(MaterialPageRoute(builder: (context) => TransaksiView()));
      } else if (stringLink.contains("portofolio") &&
          stringLink.contains("market")) {
        /// portofolio
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState
            .push(MaterialPageRoute(builder: (context) => PortofolioView()));
      } else if (stringLink.contains("orderbook") &&
          stringLink.contains("market")) {
        /// orderbook
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState
            .push(MaterialPageRoute(builder: (context) => OrderbookView()));
      } else if (stringLink.contains("$apiMarket/saham/")) {
        /// detail saham market
        Catcher.navigatorKey.currentState.pushNamed("/home_market");
        Catcher.navigatorKey.currentState.push(
          MaterialPageRoute(
            builder: (context) => DetailSahamView(
                id: num.parse(arrayLink[arrayLink.length - 2]),
                codeEmiten: stringLink.substring(stringLink.length - 4)),
          ),
        );
      } else {
        setState(() {
          spesificPage = 0;
        });
      }
      Helper.dynamicLinkData = null;
    } catch (e, stack) {
      Helper.dynamicLinkData = null;
      santaraLog(e, stack);
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            return true;
          },
          child: AlertDialog(
            title: Text('unknown connection'),
            content: Text("mohon gunakan koneksi wifi basecamp/goa"),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                },
              ),
            ],
          ),
        );
      },
    );
  }

  identifyIP() async {
    var myIp = await getPublicIP();
    // print(">> YOUR IP : $myIp");
    // if (myIp == "203.30.236.73" || myIp == "119.2.52.59") {
    await initDynamicLinks().then((_) {
      getRemoteConfig(myIp, dynamicLink: dynamicLink);
    });
    // } else {
    //   _showMyDialog();
    // }
  }

  @override
  void initState() {
    super.initState();
    getLocalStorage();
    fcmSubscribe();
    setupFirebase().then((value) {
      identifyIP();
    });
    // _askPermission();
    // PermissionHandler() // Check location permission has been granted
    //     .checkPermissionStatus(PermissionGroup
    //         .locationWhenInUse) //check permission returns a Future
    //     .then(_updateStatus);
    // .then((_) {
    // startTime();
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(ColorRev.mainBlack),
      key: _key,
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                margin: EdgeInsets.only(bottom: 50),
                width: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            new AssetImage('assets/santara/Logo_Santara.png'))),
              ),
            ),
            // Align(
            //   alignment: Alignment.bottomCenter,
            //   child: Column(
            //     mainAxisAlignment: MainAxisAlignment.end,
            //     children: <Widget>[
            //       Text("Berizin dan Diawasi Oleh :",
            //           style: TextStyle(
            //               fontWeight: FontWeight.bold,
            //               fontSize: MediaQuery.of(context).size.width / 36)),
            //       Container(
            //         height: MediaQuery.of(context).size.width / 4.5,
            //         width: MediaQuery.of(context).size.width / 3,
            //         margin: const EdgeInsets.only(bottom: 16),
            //         decoration: BoxDecoration(
            //           image: DecorationImage(
            //             image: AssetImage('assets/santara/ojk.png'),
            //             fit: BoxFit.contain,
            //           ),
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
