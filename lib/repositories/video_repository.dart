import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storages;
import 'package:meta/meta.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';

class VideoApiClient {
  final Dio dio;
  final storage = new storages.FlutterSecureStorage();

  VideoApiClient({@required this.dio}) : assert(dio != null);

  Future<Response> fetchVideo(String title, String category, bool likes,
      bool viewer, bool newest) async {
    try {
      // final token = await storage.read(key: "token");
      final headers = await UserAgent.headers();
      final videoUrl =
          '$apiLocal/videos/video?title=$title&category=$category&likes=$likes&viewer=$viewer&newest=$newest';
      final response = await this.dio.get(videoUrl,
          options: Options(headers: headers));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> fetchVideoCategory() async {
    try {
      final videoUrl = '$apiLocal/videos/category';
      final response = await this.dio.get(videoUrl);
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}

class VideoRepository {
  final VideoApiClient videoApiClient;

  VideoRepository({@required this.videoApiClient})
      : assert(videoApiClient != null);

  Future<Response> getVideo(String title, String category, bool likes,
      bool viewer, bool newest) async {
    return videoApiClient.fetchVideo(title, category, likes, viewer, newest);
  }

  Future<Response> getVideoCategory() async {
    return videoApiClient.fetchVideoCategory();
  }
}
