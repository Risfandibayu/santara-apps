class GlossariumMarket {
  static String potGlPercent =
      "% Pot G/L (% Potential Gain/Lost)\nPersentase kemungkinan keuntungan atau kerugian yang diperoleh saat menjual saham yang dimiliki saat ini.";

  static String amended =
      "Amended\nSaat terjadi perubahan harga dan/atau jumlah lembar saham pada transaksi.";

  static String assetBalance =
      "Asset Balance\nAsset Balance merupakan dana total yang menggabungkan antara dana rupiah Anda dengan nilai saham Anda.";

  static String avgPrice =
      "Avg(Average) Price (Orderbook)\nRata-rata harga saham perusahaan yang berhasil diproses(match) di hari ini.";

  static String bvps =
      "BVPS (Book Value Per Share)\nNilai buku perlembar saham perusahaan untuk mengetahui berapa jumlah uang yang akan diterima oleh pemegang saham apabila suatu perusahaan dibubarkan (dilikuidasi) atau jumlah uang yang dapat diterima oleh pemegang saham apabila semua aktiva (aset) perusahaan dijual sebesar nilai bukunya.";

  static String changePercent =
      "Change%\nPersentase perubahan harga antara harga penutupan hari sebelumnya dan harga pembentukan terkini pada hari ini.";

  static String change =
      "Change Price\nPerubahan harga antara harga penutupan hari sebelumnya dan harga pembentukan terkini pada hari ini.";

  static String danaAllocated =
      "Dana Allocated\nJumlah dana rupiah yang telah Anda transaksikan namun transaksi masih dalam proses.";

  static String danaTersedia =
      "Dana Tersedia\nJumlah dana rupiah Anda yang belum dialokasikan.";

  static String der =
      "DER (Debt to Equity Ratio)\nkemampuan suatu perusahaan dalam membayar semua utang-utangnya (baik hutang jangka pendek atau pun hutang jangka panjang).";

  static String ebitda =
      "EV/EBITDA (Enterprise Value/ Equity Before Interest, Tax, Depreciation, and Amortization)\nlaba perusahaan sebelum dikurangi dengan biaya bunga, pajak, dll.";

  static String fullMatch =
      "Full Matched (Status Transaksi)\nSaat seluruh transaksi telah match dan tidak tampil di orderbook lagi.";

  static String gain =
      "Gain\nKeuntungan yang mungkin didapatkan saat menjual saham yang telah dimiliki.";

  static String gps =
      "GPS (Gross Profit Growth)\nRasio profitabilitas untuk menghitung persentase kelebihan laba kotor terhadap pendapatan penjualan.";

  static String highPrice =
      "High Price\nHarga saham tertinggi yang berhasil diproses di hari ini.";

  static String lastPrice =
      "Last Price\nHarga terakhir yang telah terbentuk di pasar sekunder";

  static String lost =
      "Lost\nKerugian  yang mungkin didapatkan saat menjual saham yang telah dimiliki.";

  static String lowPrice =
      "Low Price\nHarga saham terendah yang berhasil diproses di hari ini.";

  static String marketCap =
      "Market Cap (Market Capitalization)\nNilai atau harga sebuah perusahaan yang dihitung dari jumlah keseluruhan nilai saham yang beredar.";

  static String marketValue =
      "Market Value\nNilai pasar dari saham yang dimiliki saat ini.";

  static String nilaiSaham =
      "Nilai Saham\nNilai valuasi saham Anda, sesuai perkembangan harga dipasar sekunder.";

  static String npm =
      "NPM (Net Profit Margin)\nRasio untuk mengukur seberapa besar kemampuan perusahaan dalam menghasilkan laba bersih dari penjualannya.";

  static String omzet =
      "Omzet\nHasil penjualan barang (dagangan) tertentu selama suatu masa jual belum dikurangi HPP dan biaya. Bisa juga disebut laba kotor atau pendapatan kotor yang dihasilkan usaha anda.";

  static String open =
      "Open (Status Transaksi)\nSaat transaksi yang diinputkan masih tersedia di orderbook.";

  static String opm =
      "OPM (Operating Profit Margin)\nKemampuan dalam menghasilkan laba operasi (laba usaha) dari penjualan bersih perusahaan selama periode waktu tertentu.";

  static String partialMatched =
      "Partial Matched (Status Transaksi)\nSaat sebagian transaksi telah match, dan sisa transaksi yang belum akan tetap tampil di orderbook lagi.";

  static String pbv =
      "PBV (Price to Book Value)\nPerbandingan antara harga saham dibandingkan dengan nilai buku perusahaan.";

  static String per =
      "PER (Price to Earnings Ratio)\nPerbandingan antara harga saham dibagi dengan laba perlembar sahamnya. Semakin kecil nilai PER menunjukkan semakin baik kinerja perusahaan tersebut dalam menghasilkan laba bersih dari setiap lembar saham yang dijadikan modal.";

  static String potGL =
      "Pot G/L (Potential Gain/Lost)\nKemungkinan keuntungan atau kerugian yang diperoleh saat menjual saham yang dimiliki saat ini.";

  static String profit =
      "Profit\nPendapatan bersih perusahaan atau jumlah uang yang Anda hasilkan dari penjualan dalam periode tertentu yang sudah dikurangi dengan HPP dan biaya.";

  static String prev =
      "Prev (Previous Price)\nHarga penutupan hari sebelumnya. Harga terakhir yang berhasil diproses di hari sebelumnya.";

  static String roa =
      "ROA (Return of Assets)\nKemampuan perusahaan dalam menghasilkan laba dari setiap aset/modal yang dimiliki. ROA positif menunjukkan bahwa dari total aset yang dimiliki perusahaan, perusahaan mampu menghasilkan laba. ROA negatif menunjukkan bahwa perusahaan sedang mengalami kerugian.";

  static String roe =
      "ROE (Return of Equity)\nKemampuan perusahaan untuk menghasilkan laba dari investasi pemegang saham di perusahaan tersebut. Nilai ROE yang semakin besar menunjukkan bahwa perusahaan lebih mampu mengelola modal yang dimiliki untuk menghasilkan laba.";

  static String roi =
      "ROI (Return of Investment)\nLaba atas investasi yang dihitung berdasarkan hasil pembagian dari pendapatan yang dihasilkan dengan besaran modal yang ditanam. ROI yang bernilai positif menunjukkan keuntungan, sedangkan jika bernilai negatif menunjukkan kerugian.";

  static String salesGrowth =
      "Sales Growth\nPeningkatan penjualan yang terjadi dari tahun ke tahun.";

  static String withdrawn =
      "Withdrawn (Status Transaksi)\nSaat transaksi yang telah diinputkan dibatalkan.";
}
