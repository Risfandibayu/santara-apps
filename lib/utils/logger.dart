import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:santaraapp/core/error/failure.dart';

var logger = Logger(
  printer: PrettyPrinter(
      colors: true, // Colorful log messages
      printEmojis: true, // Print an emoji for each log message
      printTime: false // Should each log print contain a timestamp
      ),
);

void santaraLog(dynamic e, StackTrace stack) {
  logger.e(">> Error : $e");
  logger.i(">> Stack : $stack");
}

void catchDioError(DioError e) {
  logger.e(e);
  logger.w(e.error);
  logger.i(e.request.uri);
  logger.i(e.response.data);
  logger.i(e.request.data.fields);
  logger.i(e.request.queryParameters);
}

void printFailure(Failure l) {
  logger.e("""
        >> Api Status : ${l.apiStatus} \n
        >> Message : ${l.message} \n
        >> Type : ${l.type} \n 
        >> Data : ${l.data}
        """);
}
