import 'package:flutter/material.dart';
import 'package:santaraapp/utils/sizes.dart';

class PralistingInputDecoration {
  static OutlineInputBorder get outlineInputBorder {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
        width: 2.0,
      ),
    );
  }

  static TextStyle get labelTextStyle {
    return TextStyle(
      fontSize: FontSize.s14,
      color: Colors.black,
      fontWeight: FontWeight.w600,
    );
  }

  static TextStyle hintTextStyle(bool hasValue) {
    return TextStyle(
      fontSize: FontSize.s14,
      color: hasValue ? Colors.black : Color(0xffB8B8B8),
      fontWeight: FontWeight.w400,
    );
  }
}
