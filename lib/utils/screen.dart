import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Sizes {
  static final util = ScreenUtil();

  // inisialisasi ukuran layar berdasarkan draft design
  // @FIGMA pakai Google Pixel 2
  static init(BuildContext context) {
    ScreenUtil.init(context,
        width: 1080, height: 1920, allowFontScaling: false);
  }

  // static width(num value) {
  //   util.setWidth(value);
  // }

  // static height(num value) {
  //   util.setHeight(value);
  // }

  // static fontSize(num value) {
  //   util.setSp(value);
  // }
}
