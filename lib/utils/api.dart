// // Jika app mau dijadiin production set isProduction -> true; Jika development -> false;
// bool isProduction = true;

// // set true jika ingin menampilkan info bahwa apps ini adalah versi OBT
// bool isShowOBTInfo = false;

// // version api
// final versionApi = isProduction ? 'v3.7.1' : 'v3.7.1';

// // base url api
// final apiLocal = isProduction
//     ? 'https://fire.santarax.com:3701/$versionApi'
//     : isShowOBTInfo
//         ? 'https://beta-api.santara.co.id:3701/$versionApi'
//         : 'https://tulabi.com:3801/$versionApi';

// // base url for image
// final apiLocalImage =
//     isProduction ? 'https://fire.santarax.com:3701' : 'https://tulabi.com:3801';

// // base url for origin (daftar, forgot, resend email verification)
// final urlOrigin =
//     isProduction ? "https://santara.co.id" : "https://dev.santara.id";

// // base url for asset (image and pdf)
// // final urlAsset = 'https://storage.googleapis.com/asset-santara/santara.co.id/';
// final urlAsset = isProduction
//     ? 'https://storage.googleapis.com/asset-santara/santara.co.id'
//     : 'https://storage.googleapis.com/asset-santara-staging/santara.co.id';

// final version =
//     isProduction ? 'update_current_version' : 'update_current_version_staging';

// final versionIOS = isProduction
//     ? 'update_current_version_for_ios'
//     : 'update_current_version_staging_for_ios';

// final isMaintenanceAndroid = isProduction
//     ? 'is_maintenance_android_prod'
//     : 'is_maintenance_android_staging';

// final isMaintenanceIOS =
//     isProduction ? 'is_maintenance_ios_prod' : 'is_maintenance_ios_staging';

// final whitelistIp = isProduction ? 'whitelist_ip_prod' : 'whitelist_ip_staging';

// final enableCrashLog = isProduction ? 'crashlog_prod' : 'crashlog_staging';

// final isUpdateMandatoryAndroid = isProduction
//     ? "is_update_mandatory"
//     : "is_update_mandatory_for_android_staging";

// final isUpdateMandatoryIos = isProduction
//     ? "is_update_mandatory_for_ios"
//     : "is_update_mandatory_for_ios_staging";

// final isOpenMarket =
//     isProduction ? "is_open_market_prod" : "is_open_market_dev";

// final isFeatureDanaActive =
//     isProduction ? "is_feature_dana_active_prod" : "is_feature_dana_active_dev";

// // DEVELOPMENT
// final mockApi = 'http://03332a15a238.ngrok.io/v3.4';

// // MARKET
// final apiMarket =
//     isProduction ? 'https://market.santara.co.id' : 'https://market.santara.id';

// final versionApiMarket = 'v1';

// // UPLOAD IMAGE URL
// final apiFileUpload =
//     isProduction ? 'https://fire.santarax.com:3701' : 'https://tulabi.com:3701';

// // Pralisting
// final apiPralisting = isProduction
//     ? 'https://pralisting.santara.co.id/api'
//     : 'https://pralisting.santara.id/api';

// final String salt = 'EqquWidsAuML95hz3kNROqy2vW83kMPJ';

//ini api yang baru
//---------------------->>>
// Jika app mau dijadiin production set isProduction -> true; Jika development -> false;
bool isProduction = true;

// set true jika ingin menampilkan info bahwa apps ini adalah versi OBT
bool isShowOBTInfo = false;

// version api
final versionApi = isProduction ? 'v3.7.1' : 'v3.7.1';

// base url api
final apiLocal = isProduction
    ? 'https://fire.santarax.com:3701/$versionApi'
    : isShowOBTInfo
        ? 'https://beta-api.santara.co.id:3701/$versionApi'
        : 'https://tulabi.com:3801/$versionApi';

// base url for image
final apiLocalImage =
    isProduction ? 'https://fire.santarax.com:3701' : 'https://tulabi.com';

// base url for origin (daftar, forgot, resend email verification)
final urlOrigin =
    isProduction ? "https://santara.co.id" : "https://dev.santara.id";

// base url for asset (image and pdf)
// final urlAsset = 'https://storage.googleapis.com/asset-santara/santara.co.id/';
final urlAsset = isProduction
    ? 'https://storage.googleapis.com/asset-santara/santara.co.id'
    : 'https://storage.googleapis.com/asset-santara-staging/santara.co.id';

final version =
    isProduction ? 'update_current_version' : 'update_current_version_staging';

final versionIOS = isProduction
    ? 'update_current_version_for_ios'
    : 'update_current_version_staging_for_ios';

final isMaintenanceAndroid = isProduction
    ? 'is_maintenance_android_prod'
    : 'is_maintenance_android_staging';

final isMaintenanceIOS =
    isProduction ? 'is_maintenance_ios_prod' : 'is_maintenance_ios_staging';

final whitelistIp = isProduction ? 'whitelist_ip_prod' : 'whitelist_ip_staging';

final enableCrashLog = isProduction ? 'crashlog_prod' : 'crashlog_staging';

final isUpdateMandatoryAndroid = isProduction
    ? "is_update_mandatory"
    : "is_update_mandatory_for_android_staging";

final isUpdateMandatoryIos = isProduction
    ? "is_update_mandatory_for_ios"
    : "is_update_mandatory_for_ios_staging";

final isOpenMarket =
    isProduction ? "is_open_market_prod" : "is_open_market_dev";

final isFeatureDanaActive =
    isProduction ? "is_feature_dana_active_prod" : "is_feature_dana_active_dev";

// DEVELOPMENT
final mockApi = 'http://03332a15a238.ngrok.io/v3.4';

// MARKET
final apiMarket =
    isProduction ? 'https://market.santara.co.id' : 'https://market.santara.id';

final versionApiMarket = 'v1';

//minimum_dividen
final minimumDividen =
    isProduction ? 'minimum_dividen_prod' : 'minimum_dividen_staging';

//update_text
final updateText = isProduction ? 'update_text_prod' : 'update_text_staging';

// UPLOAD IMAGE URL
final apiFileUpload =
    isProduction ? 'https://fire.santarax.com:3701' : 'https://tulabi.com:3801';

// Pralisting
final apiPralisting = isProduction
    ? 'https://pralisting.santara.co.id/api-user/v1.0.1'
    : 'https://pralisting.santara.id/api-user/v1.0.1';

// salt
final String salt = 'EqquWidsAuML95hz3kNROqy2vW83kMPJ';
