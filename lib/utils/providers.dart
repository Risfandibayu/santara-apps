import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/current_time_bloc.dart';
import '../blocs/home/banner_bloc.dart';
import '../blocs/market/ara_arb_bloc.dart';
import '../blocs/market/check_today_bloc.dart';
import '../blocs/market/close_period_bloc.dart';
import '../blocs/market/compare_fundamental_bloc.dart';
import '../blocs/market/data_chart_bloc.dart';
import '../blocs/market/emiten_by_uuid_bloc.dart';
import '../blocs/market/emiten_list_all_bloc.dart';
import '../blocs/market/emiten_list_bloc.dart';
import '../blocs/market/emiten_profile_bloc.dart';
import '../blocs/market/financial_bloc.dart';
import '../blocs/market/financial_chart_bloc.dart';
import '../blocs/market/fundamental_bloc.dart';
import '../blocs/market/glossarium_bloc.dart';
import '../blocs/market/glossarium_search_bloc.dart';
import '../blocs/market/history_market_bloc.dart';
import '../blocs/market/history_today_bloc.dart';
import '../blocs/market/market_fee_bloc.dart';
import '../blocs/market/market_fraction_bloc.dart';
import '../blocs/market/max_invest_bloc.dart';
import '../blocs/market/min_purchase_bloc.dart';
import '../blocs/market/pending_transaction_bloc.dart';
import '../blocs/market/portofolio_by_emiten_code_bloc.dart';
import '../blocs/market/portofolio_list_bloc.dart';
import '../blocs/market/profile_ballance_bloc.dart';
import '../blocs/market/submit_transaction.dart';
import '../blocs/market/summary_bloc.dart';
import '../blocs/market/watchlist_bloc.dart';
import '../blocs/user_bloc.dart';
import '../blocs/video/video_bloc.dart';
import '../blocs/video/video_category_bloc.dart';
import '../features/dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../features/dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../features/deposit/deposit/presentation/bloc/investment_limit/investment_limit_bloc.dart';
import '../features/deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../features/deposit/deposit_history/presentation/bloc/deposit_history_bloc.dart';
import '../features/disbursement/presentation/blocs/bank_withdraw/bank_withdraw_bloc.dart';
import '../features/disbursement/presentation/blocs/disbursement/disbursement_bloc.dart';
import '../features/dividen/presentation/blocs/dividen/dividen_bloc.dart';
import '../features/payment/presentation/bloc/payment_channel/payment_channel_bloc.dart';
import '../features/payment/presentation/bloc/payment_transaction/payment_transaction_bloc.dart';
import '../features/portofolio/presentation/bloc/portofolio_list_bloc.dart'
    as porto;
import '../features/transaction/presentation/bloc/checkout_list/checkout_list_bloc.dart';
import '../features/withdraw/presenstation/blocs/withdraw_history/withdraw_history_bloc.dart';
import '../injector_container.dart';
import '../repositories/video_repository.dart';
import '../widget/financial_statements/pages/blocs/chart.bloc.dart';
import '../widget/home/blocs/button.bloc.dart';
import '../widget/home/blocs/penerbit.list.bloc.dart';
import '../widget/home/blocs/testimoni.bloc.dart';
import '../widget/widget/components/kyc/blocs/kyc.notification.bloc.dart';

final dio = Dio();

final providers = [
  BlocProvider<BannerBloc>(
    create: (_) => BannerBloc(dio: dio)..add(BannerFetched()),
  ),
  BlocProvider(
    create: (context) => KycNotifBloc(
      KycNotifUninitialized(),
    )..add(KycNotifEvent()),
  ),
  BlocProvider<PenerbitListBloc>(
    create: (_) =>
        PenerbitListBloc(PenerbitListUninitialized())..add(PenerbitListEvent()),
  ),
  BlocProvider<TestimoniBloc>(
    create: (_) =>
        TestimoniBloc(TestimoniUninitialized())..add(TestimoniEvent()),
  ),
  BlocProvider<VideoBloc>(
    create: (context) => VideoBloc(
      videoRepository: VideoRepository(
        videoApiClient: VideoApiClient(
          dio: dio,
        ),
      ),
    ),
  ),
  BlocProvider<VideoCategoryBloc>(
      create: (context) => VideoCategoryBloc(
            videoRepository: VideoRepository(
              videoApiClient: VideoApiClient(
                dio: dio,
              ),
            ),
          )),
  BlocProvider<UserBloc>(create: (_) => UserBloc(dio: dio)),
  BlocProvider<ClosePeriodBloc>(
      create: (_) => ClosePeriodBloc(dio: dio)..add(ClosePeriodRequested())),
  BlocProvider<EmitenListBloc>(create: (_) => EmitenListBloc(dio: dio)),
  BlocProvider<EmitenListAllBloc>(create: (_) => EmitenListAllBloc(dio: dio)),
  BlocProvider<WatchlistBloc>(
      create: (_) => WatchlistBloc(dio: dio)..add(WatchlistRequested())),
  BlocProvider<PortofolioListBloc>(
      create: (_) =>
          PortofolioListBloc(dio: dio)..add(PortofolioListRequested())),
  // BlocProvider<BalanceBloc>(
  //     create: (_) => BalanceBloc(dio: dio)..add(BalanceRequested())),
  BlocProvider<MarketFractionBloc>(create: (_) => MarketFractionBloc(dio: dio)),
  BlocProvider<EmitenProfileBloc>(create: (_) => EmitenProfileBloc(dio: dio)),
  // BlocProvider<OrderbookListBloc>(create: (_) => OrderbookListBloc(dio: dio)),
  BlocProvider<HistoryMarketBloc>(create: (_) => HistoryMarketBloc(dio: dio)),
  BlocProvider<HistoryTodayBloc>(create: (_) => HistoryTodayBloc(dio: dio)),
  BlocProvider<SubmitTransactionBloc>(
      create: (_) => SubmitTransactionBloc(dio: dio)),
  BlocProvider<FinancialBloc>(create: (_) => FinancialBloc(dio: dio)),
  BlocProvider<FinancialChartBloc>(create: (_) => FinancialChartBloc(dio: dio)),
  BlocProvider<FundamentalBloc>(create: (_) => FundamentalBloc(dio: dio)),
  BlocProvider<ProfileBallanceBloc>(
      create: (_) => ProfileBallanceBloc(dio: dio)),
  BlocProvider<PendingTransactionBloc>(
      create: (_) => PendingTransactionBloc(dio: dio)),
  BlocProvider<EmitenByUuidBloc>(create: (_) => EmitenByUuidBloc(dio: dio)),
  BlocProvider<DataChartBloc>(create: (_) => DataChartBloc(dio: dio)),
  BlocProvider<CompareFundamentalBloc>(
      create: (_) => CompareFundamentalBloc(dio: dio)),
  BlocProvider<GlossariumBloc>(
      create: (_) => GlossariumBloc(dio: dio)..add(GlossariumRequested())),
  BlocProvider<GlossariumSearchBloc>(
      create: (_) => GlossariumSearchBloc(dio: dio)),
  BlocProvider<MarketFeeBloc>(
      create: (_) => MarketFeeBloc(dio: dio)..add(MarketFeeRequested())),
  BlocProvider<AraArbBloc>(create: (_) => AraArbBloc(dio: dio)),
  BlocProvider<MaxInvestBloc>(create: (_) => MaxInvestBloc(dio: dio)),
  BlocProvider<PortofolioByEmitenCodeBloc>(
      create: (_) => PortofolioByEmitenCodeBloc(dio: dio)),
  BlocProvider<CheckTodayBloc>(create: (_) => CheckTodayBloc(dio: dio)),
  BlocProvider<FinanceChartBloc>(
      create: (_) => FinanceChartBloc(ChartUninitialized())),
  BlocProvider<CurrentTimeBloc>(create: (_) => CurrentTimeBloc(dio: dio)),
  BlocProvider<SummaryBloc>(create: (_) => SummaryBloc(dio: dio)),
  BlocProvider<MinPurchaseBloc>(create: (_) => MinPurchaseBloc(dio: dio)),

  BlocProvider<ButtonBloc>(create: (_) => ButtonBloc(ButtonUninitialized())),

  // Deposit
  BlocProvider<SaldoDepositBloc>(create: (_) => sl<SaldoDepositBloc>()),
  BlocProvider<DepositHistoryBloc>(create: (_) => sl<DepositHistoryBloc>()),

  // Withdraw
  BlocProvider<WithdrawHistoryBloc>(create: (_) => sl<WithdrawHistoryBloc>()),

  // Portofolio
  BlocProvider<porto.PortofolioListBloc>(
      create: (_) => sl<porto.PortofolioListBloc>()),

  // limit investasi
  BlocProvider<InvestmentLimitBloc>(create: (_) => sl<InvestmentLimitBloc>()),

  // payment
  BlocProvider<PaymentChannelBloc>(
      create: (_) => sl<PaymentChannelBloc>()..add(LoadPaymentChannel())),
  BlocProvider<PaymentTransactionBloc>(
      create: (_) => sl<PaymentTransactionBloc>()),

  // Dana
  BlocProvider<DanaBalanceBloc>(create: (_) => sl<DanaBalanceBloc>()),
  BlocProvider<AuthDanaBloc>(create: (_) => sl<AuthDanaBloc>()),

  // disbursement
  BlocProvider<DisbursementBloc>(create: (_) => sl<DisbursementBloc>()),
  BlocProvider<BankWithdrawBloc>(create: (_) => sl<BankWithdrawBloc>()),

  // dividen
  BlocProvider<DividenBloc>(create: (_) => sl<DividenBloc>()),

  // transaction
  BlocProvider<CheckoutListBloc>(create: (_) => sl<CheckoutListBloc>()),
];
