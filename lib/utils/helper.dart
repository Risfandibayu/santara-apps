import 'dart:async';
import 'dart:io';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/models/Popup.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/widget/components/main/SantaraCachedImage.dart';
import 'package:url_launcher/url_launcher.dart';

import 'sizes.dart';

String tokenFirebase;
Map<String, dynamic> chatMsg;

class Helper {
  static bool check = false;
  static Timer timer;
  static String token = '';
  static String username = "";
  static String email = "";
  static int userId;
  static String refreshToken;
  static int notif = 0;
  static int reqResetPassword = 0;
  static final bool useBottomMenu = false;
  static int stackViewDividen = 0;
  static final storage = new FlutterSecureStorage();
  // Dyanmic Link Data
  static PendingDynamicLinkData dynamicLinkData;
  static bool isOnHomePage = false;

  static void dialogPopupEvent(BuildContext context, Popup popup) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 60 < 300
                    ? MediaQuery.of(context).size.width - 60
                    : 300,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    GestureDetector(
                      key: Key('closePopup'),
                      onTap: () => Navigator.pop(context),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(12, 12, 8, 6),
                        child: Icon(Icons.close, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 4 / 6 < 400
                    ? MediaQuery.of(context).size.height * 4 / 6
                    : 400,
                width: MediaQuery.of(context).size.width - 60 < 300
                    ? MediaQuery.of(context).size.width - 60
                    : 300,
                child: Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(Sizes.s8),
                      child: SantaraCachedImage(
                        image: popup.mobilePict,
                        fit: BoxFit.cover,
                      ),
                    ),
                    popup.actionText == null
                        ? Container()
                        : Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                              child: FlatButton(
                                  color: Color(0xFFBF2D30),
                                  textColor: Colors.white,
                                  child: Container(
                                    height: 42,
                                    width: MediaQuery.of(context).size.width *
                                        2 /
                                        3,
                                    child: Center(
                                      child: Text(
                                        popup.actionText,
                                      ),
                                    ),
                                  ),
                                  onPressed: () async {
                                    if (popup.mobileUrl.isNotEmpty) {
                                      try {
                                        await launch(popup.mobileUrl,
                                            forceSafariVC: false);
                                      } catch (_, __) {
                                        logger.i("""
                                          >> Error : 
                                          """ +
                                            _);
                                      }
                                      Navigator.pop(context);
                                    }
                                  }),
                            ),
                          ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  static void dialogRateApp(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Color(ColorRev.mainBlack),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(child: Container()),
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () => Navigator.pop(context),
                    )
                  ],
                ),
                Image.asset("assets/icon/rate.png"),
                Container(height: 20),
                Text(
                  "Bantu Santara untuk kemajuan UMKM Indonesia",
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Color(ColorRev.mainwhite)),
                  textAlign: TextAlign.center,
                ),
                Container(height: 20),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          int timeInMilis =
                              DateTime.now().millisecondsSinceEpoch +
                                  86400000; // +86400000 = 3 hari
                          await storage.write(
                              key: "timeShowRate", value: "$timeInMilis");
                          await storage.write(key: "isShowRate", value: "1");
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xFFC52D2F)),
                              borderRadius: BorderRadius.circular(4)),
                          child: Center(
                              child: Text("Ingatkan Nanti",
                                  style: TextStyle(
                                      color: Color(0xFFC52D2F), fontSize: 15))),
                        ),
                      ),
                    ),
                    Container(width: 12),
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          String url;
                          if (Platform.isAndroid) {
                            url =
                                "https://play.google.com/store/apps/details?id=id.co.santara.app";
                          } else {
                            url =
                                "https://apps.apple.com/id/app/santara-app/id1473570177";
                          }

                          await storage.write(key: "isShowRate", value: "0");
                          Navigator.pop(context);

                          if (await canLaunch(url)) {
                            await launch(url);
                          } else {
                            throw 'Cannot launch $url';
                          }
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Color(0xFFC52D2F)),
                          child: Center(
                              child: Text("Berikan Rating",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15))),
                        ),
                      ),
                    )
                  ],
                ),
                Container(height: 12),
                FlatButton(
                  key: Key('hideRating'),
                  child: Text("Tidak, Terimakasih",
                      style: TextStyle(fontSize: 15)),
                  textColor: Colors.white,
                  onPressed: () async {
                    await storage.write(key: "isShowRate", value: "0");
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          );
        });
  }

  // static void printDevice() async {
  //   DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  //   if (Platform.isIOS) {
  //     IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
  //     print("IOS");
  //     print('Model: ${iosInfo.utsname.machine}');
  //     print('Network Nodename: ${iosInfo.utsname.nodename}');
  //     print('Release Level: ${iosInfo.utsname.release}');
  //     print('Operating system name: ${iosInfo.utsname.sysname}');
  //     print('Version Level: ${iosInfo.utsname.version}');
  //     print('UUID: ${iosInfo.identifierForVendor}');
  //     print('Device Model: ${iosInfo.localizedModel}');
  //     print('Model: ${iosInfo.model}');
  //     print('Device Name: ${iosInfo.name}');
  //     print('System Name: ${iosInfo.systemName}');
  //     print('Sytem Version: ${iosInfo.systemVersion}');
  //   } else {
  //     AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
  //     print("ANDROID");
  //     print('Model: ${androidInfo.model}');
  //     print('ID: ${androidInfo.androidId}');
  //     print('Board: ${androidInfo.board}');
  //     print('Booloader System Number: ${androidInfo.bootloader}');
  //     print('Brand: ${androidInfo.brand}');
  //     print('Device: ${androidInfo.device}');
  //     print('Display: ${androidInfo.display}');
  //     print('Fingerprint: ${androidInfo.fingerprint}');
  //     print('Hardware: ${androidInfo.hardware}');
  //     print('Host: ${androidInfo.host}');
  //     print('Manufacturer: ${androidInfo.manufacturer}');
  //     print('Product: ${androidInfo.product}');
  //     print('Type: ${androidInfo.type}');
  //     print('Base OS: ${androidInfo.version.baseOS}');
  //     print('Codename: ${androidInfo.version.codename}');
  //     print('Incremental: ${androidInfo.version.incremental}');
  //     print('Release: ${androidInfo.version.release}');
  //     print('Security Patch: ${androidInfo.version.securityPatch}');
  //   }

  //   PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
  //     print("App Name: ${packageInfo.appName}");
  //     print("Package Name: ${packageInfo.packageName}");
  //     print("Version: ${packageInfo.version}");
  //     print("Build Number: ${packageInfo.buildNumber}");
  //   });
  // }
}
