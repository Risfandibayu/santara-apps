import 'api.dart';

class FcmTopics {
  static String base = isProduction ? 'prod-$versionApi' : 'dev-$versionApi';

  static String newEmiten = '$base-new-emiten';
  static String lihatListPenerbit = '$base-lihat_list_penerbit';
  static String peluncuranBisnisBaru = '$base-peluncuran_bisnis_baru';
  static String lihatPenerbit = '$base-lihat_penerbit';
}
