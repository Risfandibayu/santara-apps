import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as local;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class UserEvent extends Equatable {
  const UserEvent();
  @override
  List<Object> get props => [];
}

class UserRequested extends UserEvent {
  UserRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class UserState extends Equatable {
  const UserState();
  @override
  List<Object> get props => [];
}

class UserLoading extends UserState {}

class UserLoaded extends UserState {
  final User user;

  UserLoaded({this.user});

  @override
  List<Object> get props => [user];
}

class UserError extends UserState {}

// bloc
class UserBloc extends Bloc<UserEvent, UserState> {
  final Dio dio;
  UserBloc({@required this.dio}) : super(UserLoading());

  final storage = new local.FlutterSecureStorage();

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is UserRequested) {
      yield* _mapUserRequestedToState();
    }
  }

  Stream<UserState> _mapUserRequestedToState() async* {
    yield UserLoading();
    final response = await _fetchUser();
    try {
      if (response.statusCode == 200) {
        yield UserLoaded(user: userFromJson(jsonEncode(response.data)));
      } else {
        yield UserError();
      }
    } catch (_) {
      yield UserError();
    }
  }

  Future<Response> _fetchUser() async {
    final token = await storage.read(key: "token");
    final uuid = await storage.read(key: 'uuid');
    if (token != null) {
      try {
        final response = await dio.get('$apiLocal/users/by-uuid/$uuid',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
