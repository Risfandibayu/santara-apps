import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/profile_ballance.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class ProfileBallanceEvent extends Equatable {
  const ProfileBallanceEvent();
  @override
  List<Object> get props => [];
}

class ProfileBallanceRequested extends ProfileBallanceEvent {
  ProfileBallanceRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class ProfileBallanceState extends Equatable {
  const ProfileBallanceState();
  @override
  List<Object> get props => [];
}

class ProfileBallanceLoading extends ProfileBallanceState {}

class ProfileBallanceLoaded extends ProfileBallanceState {
  final ProfileBallance profileBallance;

  ProfileBallanceLoaded({this.profileBallance});

  @override
  List<Object> get props => [profileBallance];
}

class ProfileBallanceError extends ProfileBallanceState {}

// bloc
class ProfileBallanceBloc
    extends Bloc<ProfileBallanceEvent, ProfileBallanceState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  ProfileBallanceBloc({@required this.dio}) : super(ProfileBallanceLoading());

  @override
  Stream<ProfileBallanceState> mapEventToState(
      ProfileBallanceEvent event) async* {
    if (event is ProfileBallanceRequested) {
      yield* _mapProfileBallanceRequestedToState();
    }
  }

  Stream<ProfileBallanceState> _mapProfileBallanceRequestedToState() async* {
    yield ProfileBallanceLoading();
    final response = await _fetchProfileBallance();
    try {
      if (response.statusCode == 200) {
        yield ProfileBallanceLoaded(
            profileBallance:
                profileBallanceFromJson(jsonEncode(response.data)));
      } else {
        yield ProfileBallanceError();
      }
    } catch (_, __) {
      yield ProfileBallanceError();
    }
  }

  Future<Response> _fetchProfileBallance() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/user/profile',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
