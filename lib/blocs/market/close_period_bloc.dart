import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/utils/api.dart';

// file bloc
// untuk mendapatkan data periode buka tutup pasar

// event
abstract class ClosePeriodEvent extends Equatable {
  const ClosePeriodEvent();
  @override
  List<Object> get props => [];
}

class ClosePeriodRequested extends ClosePeriodEvent {}

class CloseNotifRequested extends ClosePeriodEvent {}

// state
abstract class ClosePeriodState extends Equatable {
  const ClosePeriodState();
  @override
  List<Object> get props => [];
}

class ClosePeriodLoading extends ClosePeriodState {}

class ClosePeriodLoaded extends ClosePeriodState {
  final DateTime openPeriod;
  final DateTime closePeriod;
  // final DateTime now;

  ClosePeriodLoaded({this.openPeriod, this.closePeriod});

  @override
  List<Object> get props => [openPeriod, closePeriod];
}

class ClosePeriodError extends ClosePeriodState {}

// bloc
class ClosePeriodBloc extends Bloc<ClosePeriodEvent, ClosePeriodState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  ClosePeriodBloc({this.dio}) : super(ClosePeriodLoading());

  @override
  Stream<ClosePeriodState> mapEventToState(ClosePeriodEvent event) async* {
    if (event is ClosePeriodRequested) {
      yield* _mapClosePeriodRequestedToState();
    }
    if (event is CloseNotifRequested) {
      yield ClosePeriodError();
    }
  }

  Stream<ClosePeriodState> _mapClosePeriodRequestedToState() async* {
    yield ClosePeriodLoading();
    final response = await _fetchClosePeriod();
    try {
      if (response.statusCode == 200) {
        yield ClosePeriodLoaded(
            openPeriod: DateTime.parse(response.data["data"]["open_at"]),
            closePeriod: DateTime.parse(response.data["data"]["close_at"]));
      } else {
        yield ClosePeriodError();
      }
    } catch (_, __) {
      yield ClosePeriodError();
    }
  }

  Future<Response> _fetchClosePeriod() async {
    try {
      final response =
          await dio.get('$apiMarket/cmn/$versionApiMarket/market-period/close');
      return response;
    } on DioError catch (e, stack) {
      santaraLog(e, stack);
      return e.response;
    }
  }
}
