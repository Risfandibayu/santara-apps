import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class EmitenListAllEvent extends Equatable {
  const EmitenListAllEvent();
  @override
  List<Object> get props => [];
}

class EmitenListAllRequested extends EmitenListAllEvent {
  final String group;
  final int sort;
  final int limit;
  final String key;
  EmitenListAllRequested({this.group, this.sort, this.limit, this.key});
  @override
  List<Object> get props => [group, sort, key];
}

class EmitenListAllSort extends EmitenListAllEvent {
  final String group;
  final int sort;
  final int limit;
  final List<Emiten> emitenList;
  final String key;
  EmitenListAllSort(
      {this.group, this.sort, this.limit, this.emitenList, this.key});
  @override
  List<Object> get props => [group, sort, emitenList, key];
}

class EmitenListAllPagination extends EmitenListAllEvent {
  final int page;
  final String key;
  EmitenListAllPagination({this.page, this.key});
  @override
  List<Object> get props => [page, key];
}

// state
abstract class EmitenListAllState extends Equatable {
  const EmitenListAllState();
  @override
  List<Object> get props => [];
}

class EmitenListAllLoading extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;

  EmitenListAllLoading({this.group, this.sort, this.emitenList});

  @override
  List<Object> get props => [group, sort, emitenList];
}

class EmitenListAllLoaded extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;
  final int lastPage;
  final int currentPage;

  EmitenListAllLoaded(
      {this.group,
      this.sort,
      this.emitenList,
      this.lastPage,
      this.currentPage});

  @override
  List<Object> get props => [group, sort, emitenList, lastPage, currentPage];
}

class EmitenListSearchLoaded extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;
  final int lastPage;

  EmitenListSearchLoaded(
      {this.group, this.sort, this.emitenList, this.lastPage});

  @override
  List<Object> get props => [group, sort, emitenList, lastPage];
}

class EmitenListAllPaginationLoading extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;

  EmitenListAllPaginationLoading({this.group, this.sort, this.emitenList});

  @override
  List<Object> get props => [group, sort, emitenList];
}

class EmitenListAllPaginationFailure extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;

  EmitenListAllPaginationFailure({this.group, this.sort, this.emitenList});

  @override
  List<Object> get props => [group, sort, emitenList];
}

class EmitenListAllError extends EmitenListAllState {
  final String group;
  final int sort;
  final List<Emiten> emitenList;

  EmitenListAllError({this.group, this.sort, this.emitenList});

  @override
  List<Object> get props => [group, sort, emitenList];
}

// bloc
class EmitenListAllBloc extends Bloc<EmitenListAllEvent, EmitenListAllState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  EmitenListAllBloc({this.dio})
      : super(EmitenListAllLoading(group: "", sort: 0, emitenList: []));

  @override
  Stream<EmitenListAllState> mapEventToState(EmitenListAllEvent event) async* {
    if (event is EmitenListAllRequested) {
      yield* _mapEmitenListAllRequestedToState(
          event.group, event.sort, event.limit, event.key);
    }
    if (event is EmitenListAllSort) {
      yield* _mapEmitenListAllSortToState(
          event.group, event.sort, event.limit, event.emitenList, event.key);
    }
    if (event is EmitenListAllPagination) {
      yield* _mapEmitenListAllPaginationToState(event.page, 0, event.key);
    }
  }

  Stream<EmitenListAllState> _mapEmitenListAllRequestedToState(
      String group, int sort, int limit, String key) async* {
    final currentState = state;
    yield EmitenListAllLoading(
        group: group, sort: sort, emitenList: currentState.props[2]);
    final response = await _fetchEmitenListAll(group, 1, limit, key);
    try {
      if (response.statusCode == 200) {
        var emitenListAll = emitenListFromJson(jsonEncode(response.data));
        if (key == "") {
          yield EmitenListAllLoaded(
              group: group,
              sort: sort,
              emitenList: emitenListAll.data,
              currentPage: emitenListAll.meta.currentPage,
              lastPage: emitenListAll.meta.lastPage);
        } else {
          yield EmitenListSearchLoaded(
              group: group,
              sort: sort,
              emitenList: emitenListAll.data,
              lastPage: emitenListAll.meta.lastPage);
        }
      } else {
        yield EmitenListAllError(
            group: group, sort: sort, emitenList: currentState.props[2]);
      }
    } catch (_) {
      yield EmitenListAllError(
          group: group, sort: sort, emitenList: currentState.props[2]);
    }
  }

  Stream<EmitenListAllState> _mapEmitenListAllSortToState(String group,
      int sort, int limit, List<Emiten> emitenList, String key) async* {
    yield EmitenListAllLoading(
        group: group, sort: sort, emitenList: emitenList);
    try {
      if (sort != 0) {
        yield EmitenListAllLoaded(
          group: group,
          sort: sort,
          emitenList: emitenList,
          currentPage: 1,
        );
      } else {
        final response = await _fetchEmitenListAll(group, 1, limit, key);
        if (response.statusCode == 200) {
          var emitenListAll = emitenListFromJson(jsonEncode(response.data));
          yield EmitenListAllLoaded(
              group: group,
              sort: sort,
              emitenList: emitenListAll.data,
              currentPage: emitenListAll.meta.currentPage,
              lastPage: emitenListAll.meta.lastPage);
        } else {
          yield EmitenListAllError(
              group: group, sort: sort, emitenList: emitenList);
        }
      }
    } catch (_) {
      yield EmitenListAllError(
          group: group, sort: sort, emitenList: emitenList);
    }
  }

  Stream<EmitenListAllState> _mapEmitenListAllPaginationToState(
      int page, int limit, String key) async* {
    final currentState = state;
    yield EmitenListAllPaginationLoading(
        group: currentState.props[0],
        sort: currentState.props[1],
        emitenList: currentState.props[2]);
    try {
      List<Emiten> data = currentState.props[2];
      final response =
          await _fetchEmitenListAll(currentState.props[0], page, limit, key);
      if (response.statusCode == 200) {
        var emitenListAll = emitenListFromJson(jsonEncode(response.data));
        List<Emiten> emitenList = data + emitenListAll.data;
        yield EmitenListAllLoaded(
            group: currentState.props[0],
            sort: currentState.props[1],
            emitenList: emitenList,
            currentPage: emitenListAll.meta.currentPage,
            lastPage: emitenListAll.meta.lastPage);
      } else {
        yield EmitenListAllPaginationFailure(
            group: currentState.props[0],
            sort: currentState.props[1],
            emitenList: currentState.props[2]);
      }
    } catch (_) {
      yield EmitenListAllPaginationFailure(
          group: currentState.props[0],
          sort: currentState.props[1],
          emitenList: currentState.props[2]);
    }
  }

  Future<Response> _fetchEmitenListAll(
      String group, int page, int limit, String key) async {
    final token = await storage.read(key: 'token');
    final keyWord = key == null ? "" : key;
    final lim = limit == null ? 10 : limit;
    final url =
        '$apiMarket/cmn/$versionApiMarket/emiten/home/?page=$page&limit=$lim&group=$group&search=$keyWord';
    try {
      Response response;
      if (token == null) {
        response = await dio.get(url);
      } else {
        response = await dio.get(url,
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
      }
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
