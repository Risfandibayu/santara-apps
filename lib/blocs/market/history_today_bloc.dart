import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/history_today.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class HistoryTodayEvent extends Equatable {
  const HistoryTodayEvent();
  @override
  List<Object> get props => [];
}

class HistoryTodayRequested extends HistoryTodayEvent {
  HistoryTodayRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class HistoryTodayState extends Equatable {
  const HistoryTodayState();
  @override
  List<Object> get props => [];
}

class HistoryTodayLoading extends HistoryTodayState {}

class HistoryTodayLoaded extends HistoryTodayState {
  final HistoryToday historyToday;

  HistoryTodayLoaded({this.historyToday});

  @override
  List<Object> get props => [historyToday];
}

class HistoryTodayError extends HistoryTodayState {}

// bloc
class HistoryTodayBloc extends Bloc<HistoryTodayEvent, HistoryTodayState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  HistoryTodayBloc({@required this.dio}) : super(HistoryTodayLoading());

  @override
  Stream<HistoryTodayState> mapEventToState(HistoryTodayEvent event) async* {
    if (event is HistoryTodayRequested) {
      yield* _mapHistoryTodayRequestedToState();
    }
  }

  Stream<HistoryTodayState> _mapHistoryTodayRequestedToState() async* {
    yield HistoryTodayLoading();
    final response = await _fetchHistoryToday();
    try {
      if (response.statusCode == 200) {
        yield HistoryTodayLoaded(
            historyToday: historyTodayFromJson(jsonEncode(response.data)));
      } else {
        yield HistoryTodayError();
      }
    } catch (_, __) {
      yield HistoryTodayError();
    }
  }

  Future<Response> _fetchHistoryToday() async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.get(
            '$apiMarket/trx/$versionApiMarket/order-book/today/',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
