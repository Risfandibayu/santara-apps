import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/market_fraction.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class MarketFractionEvent extends Equatable {
  const MarketFractionEvent();
  @override
  List<Object> get props => [];
}

class MarketFractionRequested extends MarketFractionEvent {}

class CloseNotifRequest extends MarketFractionEvent {}

// state
abstract class MarketFractionState extends Equatable {
  const MarketFractionState();
  @override
  List<Object> get props => [];
}

class MarketFractionLoading extends MarketFractionState {}

class MarketFractionLoaded extends MarketFractionState {
  final MarketFraction marketFraction;

  MarketFractionLoaded({this.marketFraction});

  @override
  List<Object> get props => [marketFraction];
}

class MarketFractionError extends MarketFractionState {}

// bloc
class MarketFractionBloc
    extends Bloc<MarketFractionEvent, MarketFractionState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  MarketFractionBloc({@required this.dio}) : super(MarketFractionLoading());

  @override
  Stream<MarketFractionState> mapEventToState(
      MarketFractionEvent event) async* {
    if (event is MarketFractionRequested) {
      yield* _mapMarketFractionRequestedToState();
    }
    if (event is CloseNotifRequest) {
      yield MarketFractionError();
    }
  }

  Stream<MarketFractionState> _mapMarketFractionRequestedToState() async* {
    yield MarketFractionLoading();
    final response = await _fetchMarketFraction();
    try {
      if (response.statusCode == 200) {
        yield MarketFractionLoaded(
            marketFraction: marketFractionFromJson(jsonEncode(response.data)));
      } else {
        yield MarketFractionError();
      }
    } catch (_, __) {
      yield MarketFractionError();
    }
  }

  Future<Response> _fetchMarketFraction() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/market-fraction/',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
