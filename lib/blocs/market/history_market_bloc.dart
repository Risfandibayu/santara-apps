import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/history_market.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class HistoryMarketEvent extends Equatable {
  const HistoryMarketEvent();
  @override
  List<Object> get props => [];
}

class HistoryMarketRequested extends HistoryMarketEvent {
  HistoryMarketRequested();
  @override
  List<Object> get props => [];
}

class HistoryMarketNextPageRequested extends HistoryMarketEvent {
  final int page;
  HistoryMarketNextPageRequested({this.page});
  @override
  List<Object> get props => [page];
}

// state
abstract class HistoryMarketState extends Equatable {
  const HistoryMarketState();
  @override
  List<Object> get props => [];
}

class HistoryMarketLoading extends HistoryMarketState {
  final List<History> historyMarket;
  final int lastPage;

  HistoryMarketLoading({this.historyMarket, this.lastPage});

  @override
  List<Object> get props => [historyMarket, lastPage];
}

class HistoryMarketLoaded extends HistoryMarketState {
  final List<History> historyMarket;
  final int lastPage;

  HistoryMarketLoaded({this.historyMarket, this.lastPage});

  @override
  List<Object> get props => [historyMarket, lastPage];
}

class HistoryMarketError extends HistoryMarketState {
  final List<History> historyMarket;
  final int lastPage;

  HistoryMarketError({this.historyMarket, this.lastPage});

  @override
  List<Object> get props => [historyMarket, lastPage];
}

class HistoryMarketPaginationLoading extends HistoryMarketState {
  final List<History> historyMarket;
  final int lastPage;

  HistoryMarketPaginationLoading({this.historyMarket, this.lastPage});

  @override
  List<Object> get props => [historyMarket, lastPage];
}

class HistoryMarketPaginationFailure extends HistoryMarketState {
  final List<History> historyMarket;
  final int lastPage;

  HistoryMarketPaginationFailure({this.historyMarket, this.lastPage});

  @override
  List<Object> get props => [historyMarket, lastPage];
}

// bloc
class HistoryMarketBloc extends Bloc<HistoryMarketEvent, HistoryMarketState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  HistoryMarketBloc({@required this.dio})
      : super(HistoryMarketLoading(historyMarket: [], lastPage: 0));

  @override
  Stream<HistoryMarketState> mapEventToState(HistoryMarketEvent event) async* {
    if (event is HistoryMarketRequested) {
      yield* _mapHistoryMarketRequestedToState(1);
    }
    if (event is HistoryMarketNextPageRequested) {
      yield* _mapHistoryMarketNextPageRequestedToState(event.page);
    }
  }

  Stream<HistoryMarketState> _mapHistoryMarketRequestedToState(
      int page) async* {
    yield HistoryMarketLoading(historyMarket: [], lastPage: 0);
    final currentState = state;
    final response = await _fetchHistoryMarket(page);
    try {
      if (response.statusCode == 200) {
        HistoryMarket history =
            historyMarketFromJson(jsonEncode(response.data));
        yield HistoryMarketLoaded(
            historyMarket: history.data, lastPage: history.meta.lastPage);
      } else {
        yield HistoryMarketError(
            historyMarket: currentState.props[0],
            lastPage: currentState.props[1]);
      }
    } catch (_, __) {
      yield HistoryMarketError(
          historyMarket: currentState.props[0],
          lastPage: currentState.props[1]);
    }
  }

  Stream<HistoryMarketState> _mapHistoryMarketNextPageRequestedToState(
      int page) async* {
    final currentState = state;
    yield HistoryMarketPaginationLoading(
        historyMarket: currentState.props[0], lastPage: currentState.props[1]);
    final response = await _fetchHistoryMarket(page);
    try {
      if (response.statusCode == 200) {
        List<History> data = currentState.props[0];
        HistoryMarket history =
            historyMarketFromJson(jsonEncode(response.data));
        yield HistoryMarketLoaded(
            historyMarket: data + history.data,
            lastPage: history.meta.lastPage);
      } else {
        yield HistoryMarketPaginationFailure(
            historyMarket: currentState.props[0],
            lastPage: currentState.props[1]);
      }
    } catch (_, __) {
      yield HistoryMarketPaginationFailure(
          historyMarket: currentState.props[0],
          lastPage: currentState.props[1]);
    }
  }

  Future<Response> _fetchHistoryMarket(int page) async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.get(
            '$apiMarket/trx/$versionApiMarket/order-book/history/?page=$page&limit=10&date=desc',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
