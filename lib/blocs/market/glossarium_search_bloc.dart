import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/glossarium_list.dart';
import 'package:santaraapp/utils/api.dart';

// file bloc
// untuk mendapatkan data glossarium market di menu search

// event
abstract class GlossariumSearchEvent extends Equatable {
  const GlossariumSearchEvent();
  @override
  List<Object> get props => [];
}

class GlossariumSearchRequested extends GlossariumSearchEvent {
  final String keySearch;

  GlossariumSearchRequested({this.keySearch});

  @override
  List<Object> get props => [];
}

// state
abstract class GlossariumSearchState extends Equatable {
  const GlossariumSearchState();
  @override
  List<Object> get props => [];
}

class GlossariumSearchLoading extends GlossariumSearchState {}

class GlossariumSearchLoaded extends GlossariumSearchState {
  final GlossariumList glossariumList;

  GlossariumSearchLoaded({this.glossariumList});

  @override
  List<Object> get props => [glossariumList];
}

class GlossariumSearchError extends GlossariumSearchState {}

// bloc
class GlossariumSearchBloc
    extends Bloc<GlossariumSearchEvent, GlossariumSearchState> {
  final Dio dio;
  GlossariumSearchBloc({@required this.dio}) : super(GlossariumSearchLoading());

  @override
  Stream<GlossariumSearchState> mapEventToState(
      GlossariumSearchEvent event) async* {
    if (event is GlossariumSearchRequested) {
      yield* _mapGlossariumSearchRequestedToState(event.keySearch);
    }
  }

  Stream<GlossariumSearchState> _mapGlossariumSearchRequestedToState(
      String keySearch) async* {
    yield GlossariumSearchLoading();
    final response = await _fetchGlossariumSearch(keySearch);
    try {
      if (response.statusCode == 200) {
        yield GlossariumSearchLoaded(
            glossariumList: glossariumListFromJson(jsonEncode(response.data)));
      } else {
        yield GlossariumSearchError();
      }
    } catch (_) {
      yield GlossariumSearchError();
    }
  }

  Future<Response> _fetchGlossariumSearch(String keySearch) async {
    try {
      final response = await dio
          .get('$apiMarket/cmn/$versionApiMarket/glosarium/?search=$keySearch');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
