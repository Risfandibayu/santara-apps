import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/emiten_list_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class EmitenListEvent extends Equatable {
  const EmitenListEvent();
  @override
  List<Object> get props => [];
}

class EmitenListRequested extends EmitenListEvent {
  @override
  List<Object> get props => [];
}

// state
abstract class EmitenListState extends Equatable {
  const EmitenListState();
  @override
  List<Object> get props => [];
}

class EmitenListLoading extends EmitenListState {}

class EmitenListLoaded extends EmitenListState {
  final EmitenList emitenList;

  EmitenListLoaded({this.emitenList});

  @override
  List<Object> get props => [emitenList];
}

class EmitenListError extends EmitenListState {}

// bloc
class EmitenListBloc extends Bloc<EmitenListEvent, EmitenListState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  EmitenListBloc({@required this.dio}) : super(EmitenListLoading());

  @override
  Stream<EmitenListState> mapEventToState(EmitenListEvent event) async* {
    if (event is EmitenListRequested) {
      yield* _mapEmitenListRequestedToState();
    }
  }

  Stream<EmitenListState> _mapEmitenListRequestedToState() async* {
    yield EmitenListLoading();
    final response = await _fetchEmitenList();
    try {
      if (response.statusCode == 200) {
        var emitenList = emitenListFromJson(jsonEncode(response.data));
        yield EmitenListLoaded(emitenList: emitenList);
      } else {
        yield EmitenListError();
      }
    } catch (_, __) {
      yield EmitenListError();
    }
  }

  Future<Response> _fetchEmitenList() async {
    final token = await storage.read(key: 'token');
    final url =
        '$apiMarket/cmn/$versionApiMarket/emiten/home/?page=1&limit=10&group=';
    try {
      Response response;
      if (token == null) {
        response = await dio.get(url);
      } else {
        response = await dio.get(url,
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
      }
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
