import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class MarketFeeEvent extends Equatable {
  const MarketFeeEvent();
  @override
  List<Object> get props => [];
}

class MarketFeeRequested extends MarketFeeEvent {
  MarketFeeRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class MarketFeeState extends Equatable {
  const MarketFeeState();
  @override
  List<Object> get props => [];
}

class MarketFeeLoading extends MarketFeeState {}

class MarketFeeLoaded extends MarketFeeState {
  final double feeSell;
  final double feeBuy;

  MarketFeeLoaded({this.feeSell, this.feeBuy});

  @override
  List<Object> get props => [feeSell, feeBuy];
}

class MarketFeeError extends MarketFeeState {}

// bloc
class MarketFeeBloc extends Bloc<MarketFeeEvent, MarketFeeState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  MarketFeeBloc({@required this.dio}) : super(MarketFeeLoading());

  @override
  Stream<MarketFeeState> mapEventToState(MarketFeeEvent event) async* {
    if (event is MarketFeeRequested) {
      yield* _mapMarketFeeRequestedToState();
    }
  }

  Stream<MarketFeeState> _mapMarketFeeRequestedToState() async* {
    yield MarketFeeLoading();
    final response = await _fetchMarketFee();
    try {
      if (response.statusCode == 200) {
        var data = response.data;
        yield MarketFeeLoaded(
            feeBuy: double.parse(
                (data["data"]["fee_buy"] / 100).toStringAsFixed(6)),
            feeSell: double.parse(
                (data["data"]["fee_sell"] / 100).toStringAsFixed(6)));
      } else {
        yield MarketFeeError();
      }
    } catch (_) {
      yield MarketFeeError();
    }
  }

  Future<Response> _fetchMarketFee() async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.get(
            '$apiMarket/cmn/$versionApiMarket/market-config/pricing',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
