import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/data_chart.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class DataChartEvent extends Equatable {
  const DataChartEvent();
  @override
  List<Object> get props => [];
}

class DataChartRequested extends DataChartEvent {
  final String codeEmiten;

  DataChartRequested({this.codeEmiten});
}

class DataChartWeekly extends DataChartEvent {
  final String codeEmiten;
  final DateTime start;
  final DateTime end;

  DataChartWeekly({this.codeEmiten, this.start, this.end});
}

class DataChartMonthly extends DataChartEvent {
  final String codeEmiten;
  final DateTime start;
  final DateTime end;

  DataChartMonthly({this.codeEmiten, this.start, this.end});
}

class DataChartAnnual extends DataChartEvent {
  final String codeEmiten;
  final DateTime start;
  final DateTime end;

  DataChartAnnual({this.codeEmiten, this.start, this.end});
}

class DataChartAll extends DataChartEvent {
  final String codeEmiten;

  DataChartAll({this.codeEmiten});
}

// state
abstract class DataChartState extends Equatable {
  const DataChartState();
  @override
  List<Object> get props => [];
}

class DataChartLoading extends DataChartState {}

class DataChartLoaded extends DataChartState {
  final List<Map> data;
  final List<DataChartItem> data2;
  final String time;
  DataChartLoaded({this.data, this.data2, this.time});
}

class DataChartDailyLoaded extends DataChartState {
  final String time;
  DataChartDailyLoaded({this.time});
}

class DataChartError extends DataChartState {}

// bloc
class DataChartBloc extends Bloc<DataChartEvent, DataChartState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  final dateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.mmm'Z'");
  DataChartBloc({this.dio}) : super(DataChartLoading());

  @override
  Stream<DataChartState> mapEventToState(DataChartEvent event) async* {
    if (event is DataChartRequested) {
      yield* _mapDataChartRequestedToState(event.codeEmiten);
    }
    if (event is DataChartWeekly) {
      yield* _mapDataChartWeeklyRequestedToState(
          event.codeEmiten, event.start, event.end);
    }
    if (event is DataChartMonthly) {
      yield* _mapDataChartMonthlyRequestedToState(
          event.codeEmiten, event.start, event.end);
    }
    if (event is DataChartAnnual) {
      yield* _mapDataChartAnnualRequestedToState(
          event.codeEmiten, event.start, event.end);
    }
    if (event is DataChartAll) {
      yield* _mapDataChartAllRequestedToState(event.codeEmiten);
    }
  }

  Stream<DataChartState> _mapDataChartRequestedToState(
      String codeEmiten) async* {
    yield DataChartLoading();
    try {
      yield DataChartDailyLoaded(time: 'daily');
    } catch (_) {
      yield DataChartError();
    }
  }

  Stream<DataChartState> _mapDataChartWeeklyRequestedToState(
      String codeEmiten, DateTime start, DateTime end) async* {
    yield DataChartLoading();
    final response = await _fetchDataChartWeekly(codeEmiten, start, end);
    try {
      if (response.statusCode == 200) {
        DataChart dataChart = dataChartFromJson(jsonEncode(response.data));
        var data1 = List<Map<dynamic, dynamic>>.from(
            dataChart.data.map((x) => x.toJson()));
        yield DataChartLoaded(
            data: data1, data2: dataChart.data, time: 'weekly');
      } else {
        yield DataChartError();
      }
    } catch (_, __) {
      yield DataChartError();
    }
  }

  Stream<DataChartState> _mapDataChartMonthlyRequestedToState(
      String codeEmiten, DateTime start, DateTime end) async* {
    yield DataChartLoading();
    final response = await _fetchDataChartMonthly(codeEmiten, start, end);
    try {
      if (response.statusCode == 200) {
        DataChart dataChart = dataChartFromJson(jsonEncode(response.data));
        var data1 = List<Map<dynamic, dynamic>>.from(
            dataChart.data.map((x) => x.toJson()));
        yield DataChartLoaded(
            data: data1, data2: dataChart.data, time: 'monthly');
      } else {
        yield DataChartError();
      }
    } catch (_) {
      yield DataChartError();
    }
  }

  Stream<DataChartState> _mapDataChartAnnualRequestedToState(
      String codeEmiten, DateTime start, DateTime end) async* {
    yield DataChartLoading();
    final response = await _fetchDataChartAnnual(codeEmiten, start, end);
    try {
      if (response.statusCode == 200) {
        DataChart dataChart = dataChartFromJson(jsonEncode(response.data));
        var data1 = List<Map<dynamic, dynamic>>.from(
            dataChart.data.map((x) => x.toJson()));
        yield DataChartLoaded(
            data: data1, data2: dataChart.data, time: 'annual');
      } else {
        yield DataChartError();
      }
    } catch (_) {
      yield DataChartError();
    }
  }

  Stream<DataChartState> _mapDataChartAllRequestedToState(
      String codeEmiten) async* {
    yield DataChartLoading();
    final response = await _fetchDataChartAll(codeEmiten);
    try {
      if (response.statusCode == 200) {
        DataChart dataChart = dataChartFromJson(jsonEncode(response.data));
        var data1 = List<Map<dynamic, dynamic>>.from(
            dataChart.data.map((x) => x.toJson()));
        yield DataChartLoaded(data: data1, data2: dataChart.data, time: 'all');
      } else {
        yield DataChartError();
      }
    } catch (_) {
      yield DataChartError();
    }
  }

  Future<Response> _fetchDataChartWeekly(
      String codeEmiten, DateTime start, DateTime end) async {
    try {
      final response = await dio.get(
          '$apiMarket/trx/$versionApiMarket/match-history/week-chart/?code_emiten=$codeEmiten&start_date=${dateFormat.format(start)}&end_date=${dateFormat.format(end)}');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _fetchDataChartMonthly(
      String codeEmiten, DateTime start, DateTime end) async {
    try {
      final response = await dio.get(
          '$apiMarket/trx/$versionApiMarket/match-history/month-chart/?code_emiten=$codeEmiten&start_date=${dateFormat.format(start)}&end_date=${dateFormat.format(end)}');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _fetchDataChartAnnual(
      String codeEmiten, DateTime start, DateTime end) async {
    try {
      final response = await dio.get(
          '$apiMarket/trx/$versionApiMarket/match-history/year-chart/?code_emiten=$codeEmiten&start_date=${dateFormat.format(start)}&end_date=${dateFormat.format(end)}');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _fetchDataChartAll(String codeEmiten) async {
    try {
      final response = await dio.get(
          '$apiMarket/trx/$versionApiMarket/match-history/all-chart/?code_emiten=$codeEmiten');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
