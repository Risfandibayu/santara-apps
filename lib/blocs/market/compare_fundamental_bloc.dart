import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/fundamental_list_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class CompareFundamentalEvent extends Equatable {
  const CompareFundamentalEvent();
  @override
  List<Object> get props => [];
}

class CompareFundamentalRequested extends CompareFundamentalEvent {
  final String listCode;
  CompareFundamentalRequested({this.listCode});
  @override
  List<Object> get props => [listCode];
}

// state
abstract class CompareFundamentalState extends Equatable {
  const CompareFundamentalState();
  @override
  List<Object> get props => [];
}

class CompareFundamentalLoading extends CompareFundamentalState {}

class CompareFundamentalLoaded extends CompareFundamentalState {
  final FundamentalList fundamentalList;

  CompareFundamentalLoaded({this.fundamentalList});

  @override
  List<Object> get props => [fundamentalList];
}

class CompareFundamentalError extends CompareFundamentalState {}

// bloc
class CompareFundamentalBloc
    extends Bloc<CompareFundamentalEvent, CompareFundamentalState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  CompareFundamentalBloc({this.dio})
      : super(CompareFundamentalLoading());

  @override
  Stream<CompareFundamentalState> mapEventToState(
      CompareFundamentalEvent event) async* {
    if (event is CompareFundamentalRequested) {
      yield* _mapCompareFundamentalRequestedToState(event.listCode);
    }
  }

  Stream<CompareFundamentalState> _mapCompareFundamentalRequestedToState(
      String listCode) async* {
    yield CompareFundamentalLoading();
    final response = await _fetchCompareFundamental(listCode);
    try {
      if (response.statusCode == 200) {
        yield CompareFundamentalLoaded(
            fundamentalList:
                fundamentalListFromJson(jsonEncode(response.data)));
      } else {
        yield CompareFundamentalError();
      }
    } catch (_, __) {
      yield CompareFundamentalError();
    }
  }

  Future<Response> _fetchCompareFundamental(String listCode) async {
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/emiten-fundamental/compare/?code_emiten=$listCode');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
