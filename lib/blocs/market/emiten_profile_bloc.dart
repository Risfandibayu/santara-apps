import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/emiten_profile_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class EmitenProfileEvent extends Equatable {
  const EmitenProfileEvent();
  @override
  List<Object> get props => [];
}

class EmitenProfileRequested extends EmitenProfileEvent {
  final int id;
  EmitenProfileRequested({this.id});
  @override
  List<Object> get props => [id];
}

// state
abstract class EmitenProfileState extends Equatable {
  const EmitenProfileState();
  @override
  List<Object> get props => [];
}

class EmitenProfileLoading extends EmitenProfileState {}

class EmitenProfileLoaded extends EmitenProfileState {
  final EmitenProfileModel emitenProfile;

  EmitenProfileLoaded({this.emitenProfile});

  @override
  List<Object> get props => [emitenProfile];
}

class EmitenProfileError extends EmitenProfileState {}

// bloc
class EmitenProfileBloc extends Bloc<EmitenProfileEvent, EmitenProfileState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  EmitenProfileBloc({@required this.dio}) : super(EmitenProfileLoading());

  @override
  Stream<EmitenProfileState> mapEventToState(EmitenProfileEvent event) async* {
    if (event is EmitenProfileRequested) {
      yield* _mapEmitenProfileRequestedToState(event.id);
    }
  }

  Stream<EmitenProfileState> _mapEmitenProfileRequestedToState(int id) async* {
    yield EmitenProfileLoading();
    final response = await _fetchEmitenProfile(id);
    try {
      if (response.statusCode == 200) {
        yield EmitenProfileLoaded(
            emitenProfile:
                emitenProfileModelFromJson(jsonEncode(response.data)));
      } else {
        yield EmitenProfileError();
      }
    } catch (_, __) {
      yield EmitenProfileError();
    }
  }

  Future<Response> _fetchEmitenProfile(int id) async {
    final token = await storage.read(key: 'token');
    final url = '$apiMarket/cmn/$versionApiMarket/emiten/profile/$id';
    try {
      Response response;
      if (token == null) {
        response = await dio.get(url);
      } else {
        response = await dio.get(url,
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
      }
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
