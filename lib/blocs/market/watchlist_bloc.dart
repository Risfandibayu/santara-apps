import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as local;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/watchlist_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class WatchlistEvent extends Equatable {
  const WatchlistEvent();
  @override
  List<Object> get props => [];
}

class WatchlistRequested extends WatchlistEvent {
  WatchlistRequested();
  @override
  List<Object> get props => [];
}

class SearchWatchlistRequested extends WatchlistEvent {
  final String key;
  SearchWatchlistRequested({this.key});
  @override
  List<Object> get props => [];
}

class AddWatchlistRequested extends WatchlistEvent {
  final int id;
  AddWatchlistRequested({this.id});
  @override
  List<Object> get props => [];
}

class DeleteWatchlistRequested extends WatchlistEvent {
  final int id;
  DeleteWatchlistRequested({this.id});
  @override
  List<Object> get props => [];
}

// state
abstract class WatchlistState extends Equatable {
  const WatchlistState();
  @override
  List<Object> get props => [];
}

class WatchlistLoading extends WatchlistState {
  final WatchlistModel watchlistModel;

  WatchlistLoading({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

class WatchlistActionLoading extends WatchlistState {
  final WatchlistModel watchlistModel;

  WatchlistActionLoading({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

class WatchlistAddSuccess extends WatchlistState {
  final WatchlistModel watchlistModel;
  final String msg;

  WatchlistAddSuccess({this.watchlistModel, this.msg});

  @override
  List<Object> get props => [watchlistModel, msg];
}

class WatchlistDeleteSuccess extends WatchlistState {
  final WatchlistModel watchlistModel;
  final String msg;

  WatchlistDeleteSuccess({this.watchlistModel, this.msg});

  @override
  List<Object> get props => [watchlistModel, msg];
}

class WatchlistActionFailure extends WatchlistState {
  final WatchlistModel watchlistModel;
  final String msg;

  WatchlistActionFailure({this.watchlistModel, this.msg});

  @override
  List<Object> get props => [watchlistModel, msg];
}

class WatchlistLoaded extends WatchlistState {
  final WatchlistModel watchlistModel;

  WatchlistLoaded({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

class WatchlistSearchLoaded extends WatchlistState {
  final WatchlistModel watchlistModel;

  WatchlistSearchLoaded({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

class WatchlistError extends WatchlistState {
  final WatchlistModel watchlistModel;

  WatchlistError({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

class SearchWatchlistError extends WatchlistState {
  final WatchlistModel watchlistModel;

  SearchWatchlistError({this.watchlistModel});

  @override
  List<Object> get props => [watchlistModel];
}

// bloc
class WatchlistBloc extends Bloc<WatchlistEvent, WatchlistState> {
  final Dio dio;
  final storage = local.FlutterSecureStorage();
  WatchlistBloc({@required this.dio})
      : super(WatchlistLoading(watchlistModel: WatchlistModel(data: [])));

  @override
  Stream<WatchlistState> mapEventToState(WatchlistEvent event) async* {
    if (event is WatchlistRequested) {
      yield* _mapWatchlistRequestedToState();
    }
    if (event is SearchWatchlistRequested) {
      yield* _mapSearchWatchlistRequestedToState(event.key);
    }
    if (event is AddWatchlistRequested) {
      yield* _mapAddWatchlistToState(event.id);
    }
    if (event is DeleteWatchlistRequested) {
      yield* _mapDeleteWatchlistToState(event.id);
    }
  }

  Stream<WatchlistState> _mapWatchlistRequestedToState() async* {
    final currentState = state;
    yield WatchlistLoading(watchlistModel: currentState.props[0]);
    final response = await _fetchWatchlist();
    try {
      if (response.statusCode == 200) {
        yield WatchlistLoaded(
            watchlistModel: watchlistModelFromJson(jsonEncode(response.data)));
      } else {
        yield WatchlistError(watchlistModel: WatchlistModel(data: []));
      }
    } catch (_, __) {
      yield WatchlistError(watchlistModel: WatchlistModel(data: []));
    }
  }

  Stream<WatchlistState> _mapSearchWatchlistRequestedToState(
      String key) async* {
    final currentState = state;
    yield WatchlistLoading(watchlistModel: currentState.props[0]);
    final response = await _fetchSearchWatchlist(key);
    try {
      if (response.statusCode == 200 &&
          (response.data["data"] != null || response.data["data"].length > 0)) {
        yield WatchlistSearchLoaded(
            watchlistModel: watchlistModelFromJson(jsonEncode(response.data)));
      } else {
        yield SearchWatchlistError(watchlistModel: WatchlistModel(data: []));
      }
    } catch (_, __) {
      yield SearchWatchlistError(watchlistModel: WatchlistModel(data: []));
    }
  }

  Stream<WatchlistState> _mapAddWatchlistToState(int id) async* {
    final currentState = state;
    yield WatchlistActionLoading(watchlistModel: currentState.props[0]);
    final response = await _addWatchlist(id);
    try {
      if (response.statusCode == 200 || response.statusCode == 201) {
        this.add(WatchlistRequested());
        yield WatchlistAddSuccess(
            watchlistModel: currentState.props[0],
            msg: response.data["meta"]["message"]);
      } else {
        yield WatchlistActionFailure(
            watchlistModel: currentState.props[0],
            msg: response.data["meta"]["message"]);
      }
    } catch (_) {
      yield WatchlistActionFailure(
          watchlistModel: currentState.props[0], msg: _.toString());
    }
  }

  Stream<WatchlistState> _mapDeleteWatchlistToState(int id) async* {
    final currentState = state;
    yield WatchlistActionLoading(watchlistModel: currentState.props[0]);
    final response = await _deleteWatchlist(id);
    try {
      if (response.statusCode == 200 || response.statusCode == 201) {
        this.add(WatchlistRequested());
        yield WatchlistDeleteSuccess(
            watchlistModel: currentState.props[0],
            msg: response.data["meta"]["message"]);
      } else {
        yield WatchlistActionFailure(
            watchlistModel: currentState.props[0],
            msg: response.data["meta"]["message"]);
      }
    } catch (_) {
      yield WatchlistActionFailure(
          watchlistModel: currentState.props[0], msg: _.toString());
    }
  }

  Future<Response> _fetchWatchlist() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/watchlist/',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _fetchSearchWatchlist(String key) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/watchlist/search/?search=$key',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _addWatchlist(int emitenId) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.post(
          '$apiMarket/cmn/$versionApiMarket/watchlist/',
          data: {
            "emiten_id": emitenId,
          },
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  Future<Response> _deleteWatchlist(int watchlistId) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.delete(
          '$apiMarket/cmn/$versionApiMarket/watchlist/id/$watchlistId',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
