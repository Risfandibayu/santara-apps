import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/models/market/ara_arb_model.dart';
import 'package:santaraapp/utils/api.dart';

// file bloc
// untuk value ara/arb by emiten uuid

// event
abstract class AraArbEvent extends Equatable {
  const AraArbEvent();
  @override
  List<Object> get props => [];
}

class AraArbRequested extends AraArbEvent {
  final String emitenUuid;
  const AraArbRequested({this.emitenUuid});
  @override
  List<Object> get props => [];
}

// state
abstract class AraArbState extends Equatable {
  const AraArbState();
  @override
  List<Object> get props => [];
}

class AraArbLoading extends AraArbState {}

class AraArbLoaded extends AraArbState {
  final AraArbModel araArb;

  AraArbLoaded({this.araArb});

  @override
  List<Object> get props => [araArb];
}

class AraArbError extends AraArbState {}

// bloc
class AraArbBloc extends Bloc<AraArbEvent, AraArbState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  AraArbBloc({this.dio}) : super(AraArbLoading());

  @override
  Stream<AraArbState> mapEventToState(AraArbEvent event) async* {
    if (event is AraArbRequested) {
      yield* _mapAraArbRequestedToState(event.emitenUuid);
    }
  }

  Stream<AraArbState> _mapAraArbRequestedToState(String emitenUuid) async* {
    yield AraArbLoading();
    final response = await _fetchAraArb(emitenUuid);
    try {
      if (response.statusCode == 200) {
        yield AraArbLoaded(
            araArb: araArbModelFromJson(jsonEncode(response.data)));
      } else {
        yield AraArbError();
      }
    } catch (_, __) {
      yield AraArbError();
    }
  }

  Future<Response> _fetchAraArb(String emitenUuid) async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.get(
            '$apiMarket/trx/$versionApiMarket/order-book/rejection/?emiten_uuid=$emitenUuid',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
