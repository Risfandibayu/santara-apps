import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/helpers/DateFormatter.dart';
import 'package:santaraapp/models/market/summary_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class SummaryEvent extends Equatable {
  const SummaryEvent();
  @override
  List<Object> get props => [];
}

class SummaryRequested extends SummaryEvent {
  final String start;
  final String end;
  SummaryRequested({this.start, this.end});
  @override
  List<Object> get props => [start, end];
}

// state
abstract class SummaryState extends Equatable {
  const SummaryState();
  @override
  List<Object> get props => [];
}

class SummaryLoading extends SummaryState {
  final String start;
  final String end;
  SummaryLoading({this.start, this.end});

  @override
  List<Object> get props => [start, end];
}

class SummaryLoaded extends SummaryState {
  final String start;
  final String end;
  final Summary summary;

  SummaryLoaded({this.start, this.end, this.summary});

  @override
  List<Object> get props => [start, end, summary];
}

class SummaryError extends SummaryState {
  final String start;
  final String end;
  final String error;
  SummaryError({this.start, this.end, @required this.error});

  @override
  List<Object> get props => [start, end, error];
}

// bloc
class SummaryBloc extends Bloc<SummaryEvent, SummaryState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  SummaryBloc({@required this.dio})
      : super(SummaryLoading(
            start: DateFormatter.yyyy_MM_dd(DateTime.now()),
            end: DateFormatter.yyyy_MM_dd(DateTime.now())));

  @override
  Stream<SummaryState> mapEventToState(SummaryEvent event) async* {
    if (event is SummaryRequested) {
      yield* _mapSummaryRequestedToState(event);
    }
  }

  Stream<SummaryState> _mapSummaryRequestedToState(
      SummaryRequested event) async* {
    yield SummaryLoading(start: event.start, end: event.end);
    final response = await _fetchSummary(event.start, event.end);
    try {
      if (response.statusCode == 200) {
        var data = summaryFromJson(jsonEncode(response.data));
        yield SummaryLoaded(summary: data, start: event.start, end: event.end);
      } else {
        yield SummaryError(
            start: event.start,
            end: event.end,
            error: response.data["meta"]["message"]);
      }
    } catch (_, __) {
      yield SummaryError(
          start: event.start, end: event.end, error: _.toString());
    }
  }

  Future<Response> _fetchSummary(start, end) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/user/trx-summary/?start_date=$start&end_date=$end',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
