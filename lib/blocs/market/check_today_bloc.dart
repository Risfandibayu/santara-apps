import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/models/market/active_period.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/market/jual_beli_view.dart';

// file bloc
// untuk mendapatkan data periode buka tutup pasar yang harian

// event
abstract class CheckTodayEvent extends Equatable {
  const CheckTodayEvent();
  @override
  List<Object> get props => [];
}

class CheckTodayRequested extends CheckTodayEvent {
  final TransactionType type;
  final String uuid;
  final String codeEmiten;
  final String trxUuid;
  final int amount;
  final int price;
  final double totalSaham;
  final int pending;

  CheckTodayRequested(
      {this.type,
      this.uuid,
      this.codeEmiten,
      this.trxUuid,
      this.amount,
      this.price,
      this.totalSaham,
      this.pending});

  @override
  List<Object> get props =>
      [type, uuid, codeEmiten, trxUuid, amount, price, totalSaham, pending];
}

// state
abstract class CheckTodayState extends Equatable {
  const CheckTodayState();
  @override
  List<Object> get props => [];
}

class CheckTodayInitial extends CheckTodayState {}

class CheckTodayLoading extends CheckTodayState {}

class CheckTodayLoaded extends CheckTodayState {
  final bool isOpen;
  final ActivePeriod activePeriod;
  final TransactionType type;
  final String uuid;
  final String codeEmiten;
  final String trxUuid;
  final int amount;
  final int price;
  final double totalSaham;
  final int pending;

  CheckTodayLoaded(
      {this.isOpen,
      this.activePeriod,
      this.type,
      this.uuid,
      this.codeEmiten,
      this.trxUuid,
      this.amount,
      this.price,
      this.totalSaham,
      this.pending});

  @override
  List<Object> get props => [
        isOpen,
        activePeriod,
        type,
        uuid,
        codeEmiten,
        trxUuid,
        amount,
        price,
        totalSaham,
        pending
      ];
}

class CheckTodayError extends CheckTodayState {
  final String message;

  CheckTodayError({this.message});
}

// bloc
class CheckTodayBloc extends Bloc<CheckTodayEvent, CheckTodayState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  CheckTodayBloc({this.dio}) : super(CheckTodayInitial());

  @override
  Stream<CheckTodayState> mapEventToState(CheckTodayEvent event) async* {
    if (event is CheckTodayRequested) {
      yield* _mapCheckTodayRequestedToState(event);
    }
  }

  Stream<CheckTodayState> _mapCheckTodayRequestedToState(
      CheckTodayRequested event) async* {
    yield CheckTodayLoading();
    final response = await _fetchCheckToday();
    final response2 = await _fetchActivePeriod();
    try {
      if (response.statusCode == 200 && response2.statusCode == 200) {
        yield CheckTodayLoaded(
            isOpen: response.data["data"],
            activePeriod: activePeriodFromJson(jsonEncode(response2.data)),
            amount: event.amount,
            codeEmiten: event.codeEmiten,
            price: event.price,
            totalSaham: event.totalSaham,
            trxUuid: event.trxUuid,
            type: event.type,
            uuid: event.uuid,
            pending: event.pending);
      } else {
        yield CheckTodayError(message: "Mohon maaf, sedang terjadi kesalahan");
      }
    } catch (_) {
      yield CheckTodayError(message: "Mohon maaf, sedang terjadi kesalahan");
    }
  }

  Future<Response> _fetchCheckToday() async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.get(
            '$apiMarket/cmn/$versionApiMarket/market-period/today',
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }

  Future<Response> _fetchActivePeriod() async {
    try {
      final response = await dio
          .get('$apiMarket/cmn/$versionApiMarket/market-period/active');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
