import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as local;
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class SubmitTransactionEvent extends Equatable {
  const SubmitTransactionEvent();
  @override
  List<Object> get props => [];
}

class PostEvent extends SubmitTransactionEvent {
  final String emitenUuid;
  final String codeEmiten;
  final int amount;
  final int price;
  final String type;
  final String pin;
  final bool finger;

  PostEvent(
      {@required this.emitenUuid,
      @required this.codeEmiten,
      @required this.amount,
      @required this.price,
      @required this.type,
      @required this.pin,
      @required this.finger});

  @override
  List<Object> get props =>
      [emitenUuid, codeEmiten, amount, price, type, pin, finger];
}

class DeleteEvent extends SubmitTransactionEvent {
  final String trxUuid;
  final String codeEmiten;
  final String type;
  final String pin;
  final bool finger;

  DeleteEvent(
      {@required this.trxUuid,
      @required this.codeEmiten,
      @required this.type,
      @required this.pin,
      @required this.finger});

  @override
  List<Object> get props => [trxUuid, codeEmiten, type, pin, finger];
}

class SubmitEditEvent extends SubmitTransactionEvent {
  final String uuid;
  final String emitenUuid;
  final String codeEmiten;
  final int amount;
  final int price;
  final String type;
  final String pin;
  final bool finger;

  SubmitEditEvent(
      {@required this.uuid,
      @required this.emitenUuid,
      @required this.codeEmiten,
      @required this.amount,
      @required this.price,
      @required this.type,
      @required this.pin,
      @required this.finger});

  @override
  List<Object> get props =>
      [uuid, emitenUuid, codeEmiten, amount, price, type, pin, finger];
}

// state
abstract class SubmitTransactionState extends Equatable {
  const SubmitTransactionState();
  @override
  List<Object> get props => [];
}

class SubmitTransactionLoading extends SubmitTransactionState {
  @override
  List<Object> get props => [];
}

class SubmitTransactionSuccess extends SubmitTransactionState {
  @override
  List<Object> get props => [];
}

class DeleteTransactionSuccess extends SubmitTransactionState {
  final String message;

  DeleteTransactionSuccess({this.message});
  @override
  List<Object> get props => [];
}

class SubmitTransactionFailure extends SubmitTransactionState {
  final String message;

  SubmitTransactionFailure(this.message);

  @override
  List<Object> get props => [message];
}

class SubmitTransactionInitial extends SubmitTransactionState {
  @override
  List<Object> get props => [];
}

// bloc
class SubmitTransactionBloc
    extends Bloc<SubmitTransactionEvent, SubmitTransactionState> {
  final Dio dio;
  final storage = local.FlutterSecureStorage();
  SubmitTransactionBloc({@required this.dio})
      : super(SubmitTransactionInitial());

  @override
  Stream<SubmitTransactionState> mapEventToState(
      SubmitTransactionEvent event) async* {
    if (event is PostEvent) {
      yield* _mapSubmitTransactionToState(event.emitenUuid, event.codeEmiten,
          event.amount, event.price, event.type, event.pin, event.finger);
    }
    if (event is SubmitEditEvent) {
      yield* _mapSubmitEditTransactionToState(
          event.uuid,
          event.emitenUuid,
          event.codeEmiten,
          event.amount,
          event.price,
          event.type,
          event.pin,
          event.finger);
    }
    if (event is DeleteEvent) {
      yield* _mapDeleteTransactionToState(
          event.trxUuid, event.codeEmiten, event.type, event.pin, event.finger);
    }
  }

  Stream<SubmitTransactionState> _mapSubmitTransactionToState(
      String emitenUuid,
      String codeEmiten,
      int amount,
      int price,
      String type,
      String pin,
      bool finger) async* {
    yield SubmitTransactionLoading();
    try {
      final response = await _submitTransaction(
          emitenUuid, codeEmiten, amount, price, type, pin, finger);
      if (response.statusCode == 200 || response.statusCode == 201) {
        yield SubmitTransactionSuccess();
      } else {
        yield SubmitTransactionFailure(response.data == ""
            ? "Sedang terjadi kesalahan : ${response.statusCode}"
            : response.data["meta"]["message"]);
      }
    } catch (e, _) {
      yield SubmitTransactionFailure(e.toString());
    }
  }

  Stream<SubmitTransactionState> _mapSubmitEditTransactionToState(
      String uuid,
      String emitenUuid,
      String codeEmiten,
      int amount,
      int price,
      String type,
      String pin,
      bool finger) async* {
    yield SubmitTransactionLoading();
    try {
      final response = await _editSubmitTransaction(
          uuid, emitenUuid, codeEmiten, amount, price, type, pin, finger);
      if (response.statusCode == 200 || response.statusCode == 201) {
        yield SubmitTransactionSuccess();
      } else {
        yield SubmitTransactionFailure(response.data["meta"]["message"]);
      }
    } catch (_, __) {
      yield SubmitTransactionFailure(_.toString());
    }
  }

  Stream<SubmitTransactionState> _mapDeleteTransactionToState(String uuid,
      String codeEmiten, String type, String pin, bool finger) async* {
    yield SubmitTransactionLoading();
    try {
      final response =
          await _deleteTransaction(uuid, codeEmiten, type, pin, finger);
      if (response.statusCode == 200) {
        yield DeleteTransactionSuccess(message: "Berhasil menghapus transaksi");
      } else {
        yield SubmitTransactionFailure(response.data["meta"]["message"]);
      }
    } catch (e) {
      yield SubmitTransactionFailure(e.toString());
    }
  }

  Future<Response> _submitTransaction(String emitenUuid, String codeEmiten,
      int amount, int price, String type, String pin, bool finger) async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.post(
            '$apiMarket/trx/$versionApiMarket/order-book/fire',
            data: {
              "emiten_uuid": emitenUuid,
              "code_emiten": codeEmiten,
              "amount": amount,
              "price": price,
              "type": type,
              "pin": pin,
              "finger": finger
            },
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }

  Future<Response> _editSubmitTransaction(
      String uuid,
      String emitenUuid,
      String codeEmiten,
      int amount,
      int price,
      String type,
      String pin,
      bool finger) async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.put(
            '$apiMarket/trx/$versionApiMarket/order-book/id/$uuid',
            data: {
              "emiten_uuid": emitenUuid,
              "code_emiten": codeEmiten,
              "amount": amount,
              "price": price,
              "type": type,
              "pin": pin,
              "finger": finger
            },
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }

  Future<Response> _deleteTransaction(String uuid, String codeEmiten,
      String type, String pin, bool finger) async {
    final token = await storage.read(key: 'token');
    if (token != null) {
      try {
        final response = await dio.delete(
            '$apiMarket/trx/$versionApiMarket/order-book/trx/?id=$uuid',
            data: {
              "code_emiten": codeEmiten,
              "type": type,
              "pin": pin,
              "finger": finger
            },
            options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"}));
        return response;
      } on DioError catch (e) {
        return e.response;
      }
    } else {
      return null;
    }
  }
}
