import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/financial_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

// event
abstract class FinancialEvent extends Equatable {
  const FinancialEvent();
  @override
  List<Object> get props => [];
}

class FinancialRequested extends FinancialEvent {
  final String uuid;
  FinancialRequested({this.uuid});
  @override
  List<Object> get props => [uuid];
}

// state
abstract class FinancialState extends Equatable {
  const FinancialState();
  @override
  List<Object> get props => [];
}

class FinancialLoading extends FinancialState {}

class FinancialLoaded extends FinancialState {
  final Financial financial;

  FinancialLoaded({this.financial});

  @override
  List<Object> get props => [financial];
}

class FinancialError extends FinancialState {}

// bloc
class FinancialBloc extends Bloc<FinancialEvent, FinancialState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  FinancialBloc({@required this.dio}) : super(FinancialLoading());

  @override
  Stream<FinancialState> mapEventToState(FinancialEvent event) async* {
    if (event is FinancialRequested) {
      yield* _mapFinancialRequestedToState(event.uuid);
    }
  }

  Stream<FinancialState> _mapFinancialRequestedToState(String uuid) async* {
    yield FinancialLoading();
    final response = await _fetchFinancial(uuid);
    try {
      if (response.statusCode == 200) {
        yield FinancialLoaded(financial: financialFromJson(response.body));
      } else {
        yield FinancialError();
      }
    } catch (_, __) {
      yield FinancialError();
    }
  }

  Future<http.Response> _fetchFinancial(String uuid) async {
    try {
      final response = await http.get('$apiLocal/portofolio/financial/$uuid');
      return response;
    } catch (e) {
      return null;
    }
  }
}
