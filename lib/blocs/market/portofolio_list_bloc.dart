import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/portofolio_list_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class PortofolioListEvent extends Equatable {
  const PortofolioListEvent();
  @override
  List<Object> get props => [];
}

class PortofolioListRequested extends PortofolioListEvent {
  PortofolioListRequested();
  @override
  List<Object> get props => [];
}

class PortofolioListNextPageRequested extends PortofolioListEvent {
  final int page;
  PortofolioListNextPageRequested({@required this.page});
  @override
  List<Object> get props => [];
}

// state
abstract class PortofolioListState extends Equatable {
  const PortofolioListState();
  @override
  List<Object> get props => [];
}

class PortofolioListLoading extends PortofolioListState {}

class PortofolioListLoaded extends PortofolioListState {
  final List<Portofolio> portofolioList;
  final int lastPage;
  final int currentPage;

  PortofolioListLoaded({this.portofolioList, this.lastPage, this.currentPage});

  @override
  List<Object> get props => [portofolioList];
}

class PortofolioListError extends PortofolioListState {}

// bloc
class PortofolioListBloc
    extends Bloc<PortofolioListEvent, PortofolioListState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  PortofolioListBloc({@required this.dio}) : super(PortofolioListLoading());

  @override
  Stream<PortofolioListState> mapEventToState(
      PortofolioListEvent event) async* {
    if (event is PortofolioListRequested) {
      yield* _mapPortofolioListRequestedToState();
    }
    if (event is PortofolioListNextPageRequested) {
      yield* _mapPortofolioListRequestedNextPageToState(event.page);
    }
  }

  Stream<PortofolioListState> _mapPortofolioListRequestedToState() async* {
    yield PortofolioListLoading();
    final response = await _fetchPortofolioList(1);
    try {
      if (response.statusCode == 200) {
        var data = portofolioListFromJson(jsonEncode(response.data));
        yield PortofolioListLoaded(
            portofolioList: data.data,
            lastPage: data.meta.lastPage,
            currentPage: data.meta.currentPage);
      } else {
        yield PortofolioListError();
      }
    } catch (_, __) {
      yield PortofolioListError();
    }
  }

  Stream<PortofolioListState> _mapPortofolioListRequestedNextPageToState(
      int page) async* {
    final currentState = state;
    final response = await _fetchPortofolioList(page);
    try {
      if (response.statusCode == 200) {
        var data = portofolioListFromJson(jsonEncode(response.data));
        if (currentState is PortofolioListLoaded) {
          yield PortofolioListLoaded(
              portofolioList: currentState.portofolioList + data.data,
              lastPage: data.meta.lastPage,
              currentPage: data.meta.currentPage);
        } else {
          yield PortofolioListLoaded(
              portofolioList: data.data,
              lastPage: data.meta.lastPage,
              currentPage: data.meta.currentPage);
        }
      } else {
        yield PortofolioListError();
      }
    } catch (_, __) {
      yield PortofolioListError();
    }
  }

  Future<Response> _fetchPortofolioList(int page) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/user/portofolio/?limit=10&page=$page',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
