import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/glossarium_list.dart';
import 'package:santaraapp/utils/api.dart';

// file bloc
// untuk mendapatkan data glossarium market

// event
abstract class GlossariumEvent extends Equatable {
  const GlossariumEvent();
  @override
  List<Object> get props => [];
}

class GlossariumRequested extends GlossariumEvent {}

// state
abstract class GlossariumState extends Equatable {
  const GlossariumState();
  @override
  List<Object> get props => [];
}

class GlossariumLoading extends GlossariumState {}

class GlossariumLoaded extends GlossariumState {
  final List<String> initial;
  final GlossariumList glossariumList;

  GlossariumLoaded({this.initial, this.glossariumList});

  @override
  List<Object> get props => [glossariumList];
}

class GlossariumError extends GlossariumState {}

// bloc
class GlossariumBloc extends Bloc<GlossariumEvent, GlossariumState> {
  final Dio dio;
  GlossariumBloc({@required this.dio}) : super(GlossariumLoading());

  @override
  Stream<GlossariumState> mapEventToState(GlossariumEvent event) async* {
    if (event is GlossariumRequested) {
      yield* _mapGlossariumRequestedToState();
    }
  }

  Stream<GlossariumState> _mapGlossariumRequestedToState() async* {
    yield GlossariumLoading();
    final response = await _fetchGlossarium();
    try {
      if (response.statusCode == 200) {
        GlossariumList data = glossariumListFromJson(jsonEncode(response.data));
        List<String> initial = ["#"];
        for (var item in data.data) {
          if (RegExp(r"^[a-zA-Z]*$").hasMatch(item.title[0]) &&
              !initial.contains(item.title[0])) {
            initial.add(item.title[0]);
          }
        }
        yield GlossariumLoaded(initial: initial, glossariumList: data);
      } else {
        yield GlossariumError();
      }
    } catch (_) {
      yield GlossariumError();
    }
  }

  Future<Response> _fetchGlossarium() async {
    try {
      final response =
          await dio.get('$apiMarket/cmn/$versionApiMarket/glosarium/');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
