import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class MaxInvestEvent extends Equatable {
  const MaxInvestEvent();
  @override
  List<Object> get props => [];
}

class MaxInvestRequested extends MaxInvestEvent {
  MaxInvestRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class MaxInvestState extends Equatable {
  const MaxInvestState();
  @override
  List<Object> get props => [];
}

class MaxInvestLoading extends MaxInvestState {}

class MaxInvestLoaded extends MaxInvestState {
  final double maxInvest;

  MaxInvestLoaded({this.maxInvest});

  @override
  List<Object> get props => [maxInvest];
}

class MaxInvestError extends MaxInvestState {}

// bloc
class MaxInvestBloc extends Bloc<MaxInvestEvent, MaxInvestState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  MaxInvestBloc({@required this.dio}) : super(MaxInvestLoading());

  @override
  Stream<MaxInvestState> mapEventToState(MaxInvestEvent event) async* {
    if (event is MaxInvestRequested) {
      yield* _mapMaxInvestRequestedToState();
    }
  }

  Stream<MaxInvestState> _mapMaxInvestRequestedToState() async* {
    yield MaxInvestLoading();
    final response = await _fetchMaxInvest();
    try {
      if (response.statusCode == 200) {
        yield MaxInvestLoaded(maxInvest: response.data["max_invest"] / 1);
      } else {
        yield MaxInvestError();
      }
    } catch (_, __) {
      yield MaxInvestError();
    }
  }

  Future<Response> _fetchMaxInvest() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get('$apiLocal/traders/max-invest',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
