import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/pending_transaction.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class PendingTransactionEvent extends Equatable {
  const PendingTransactionEvent();
  @override
  List<Object> get props => [];
}

class PendingTransactionRequested extends PendingTransactionEvent {
  PendingTransactionRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class PendingTransactionState extends Equatable {
  const PendingTransactionState();
  @override
  List<Object> get props => [];
}

class PendingTransactionLoading extends PendingTransactionState {}

class PendingTransactionLoaded extends PendingTransactionState {
  final PendingTransaction pendingTransaction;

  PendingTransactionLoaded({this.pendingTransaction});

  @override
  List<Object> get props => [pendingTransaction];
}

class PendingTransactionError extends PendingTransactionState {}

// bloc
class PendingTransactionBloc
    extends Bloc<PendingTransactionEvent, PendingTransactionState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  PendingTransactionBloc({@required this.dio})
      : super(PendingTransactionLoading());

  @override
  Stream<PendingTransactionState> mapEventToState(
      PendingTransactionEvent event) async* {
    if (event is PendingTransactionRequested) {
      yield* _mapPendingTransactionRequestedToState();
    }
  }

  Stream<PendingTransactionState>
      _mapPendingTransactionRequestedToState() async* {
    yield PendingTransactionLoading();
    final response = await _fetchPendingTransaction();
    try {
      if (response.statusCode == 200) {
        yield PendingTransactionLoaded(
            pendingTransaction:
                pendingTransactionFromJson(jsonEncode(response.data)));
      } else {
        yield PendingTransactionError();
      }
    } catch (_) {
      yield PendingTransactionError();
    }
  }

  Future<Response> _fetchPendingTransaction() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/trx/$versionApiMarket/order-book/pending',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
