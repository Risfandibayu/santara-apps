import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/portofolio_by_emiten_code.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class PortofolioByEmitenCodeEvent extends Equatable {
  const PortofolioByEmitenCodeEvent();
  @override
  List<Object> get props => [];
}

class PortofolioByEmitenCodeRequested extends PortofolioByEmitenCodeEvent {
  final String codeEmiten;
  PortofolioByEmitenCodeRequested({this.codeEmiten});
  @override
  List<Object> get props => [codeEmiten];
}

// state
abstract class PortofolioByEmitenCodeState extends Equatable {
  const PortofolioByEmitenCodeState();
  @override
  List<Object> get props => [];
}

class PortofolioByEmitenCodeLoading extends PortofolioByEmitenCodeState {}

class PortofolioByEmitenCodeLoaded extends PortofolioByEmitenCodeState {
  final PortofolioByEmitenCode portofolio;

  PortofolioByEmitenCodeLoaded({this.portofolio});

  @override
  List<Object> get props => [portofolio];
}

class PortofolioByEmitenCodeError extends PortofolioByEmitenCodeState {}

// bloc
class PortofolioByEmitenCodeBloc
    extends Bloc<PortofolioByEmitenCodeEvent, PortofolioByEmitenCodeState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  PortofolioByEmitenCodeBloc({@required this.dio})
      : super(PortofolioByEmitenCodeLoading());

  @override
  Stream<PortofolioByEmitenCodeState> mapEventToState(
      PortofolioByEmitenCodeEvent event) async* {
    if (event is PortofolioByEmitenCodeRequested) {
      yield* _mapPortofolioByEmitenCodeRequestedToState(event.codeEmiten);
    }
  }

  Stream<PortofolioByEmitenCodeState>
      _mapPortofolioByEmitenCodeRequestedToState(String codeEmiten) async* {
    yield PortofolioByEmitenCodeLoading();
    final response = await _fetchPortofolioByEmitenCode(codeEmiten);
    try {
      if (response.statusCode == 200) {
        yield PortofolioByEmitenCodeLoaded(
            portofolio:
                portofolioByEmitenCodeFromJson(jsonEncode(response.data)));
      } else {
        yield PortofolioByEmitenCodeError();
      }
    } catch (_, __) {
      yield PortofolioByEmitenCodeError();
    }
  }

  Future<Response> _fetchPortofolioByEmitenCode(String codeEmiten) async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/user/portofolio-emiten/?code_emiten=$codeEmiten',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
