import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/financial_chart_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class FinancialChartEvent extends Equatable {
  const FinancialChartEvent();
  @override
  List<Object> get props => [];
}

class FinancialChartRequested extends FinancialChartEvent {
  final String codeEmiten;
  FinancialChartRequested({this.codeEmiten});
  @override
  List<Object> get props => [codeEmiten];
}

// state
abstract class FinancialChartState extends Equatable {
  const FinancialChartState();
  @override
  List<Object> get props => [];
}

class FinancialChartLoading extends FinancialChartState {}

class FinancialChartLoaded extends FinancialChartState {
  final FinancialChart financialChart;

  FinancialChartLoaded({this.financialChart});

  @override
  List<Object> get props => [financialChart];
}

class FinancialChartError extends FinancialChartState {}

// bloc
class FinancialChartBloc
    extends Bloc<FinancialChartEvent, FinancialChartState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  FinancialChartBloc({@required this.dio}) : super(FinancialChartLoading());

  @override
  Stream<FinancialChartState> mapEventToState(
      FinancialChartEvent event) async* {
    if (event is FinancialChartRequested) {
      yield* _mapFinancialChartRequestedToState(event.codeEmiten);
    }
  }

  Stream<FinancialChartState> _mapFinancialChartRequestedToState(
      String codeEmiten) async* {
    yield FinancialChartLoading();
    final response = await _fetchFinancialChart(codeEmiten);
    try {
      if (response.statusCode == 200) {
        yield FinancialChartLoaded(
            financialChart: financialChartFromJson(jsonEncode(response.data)));
      } else {
        yield FinancialChartError();
      }
    } catch (_, __) {
      yield FinancialChartError();
    }
  }

  Future<Response> _fetchFinancialChart(String codeEmiten) async {
    // final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/emiten-fundamental/financial-chart/?code_emiten=$codeEmiten');
      // options: Options(
      //     headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
