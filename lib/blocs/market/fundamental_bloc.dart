import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/models/market/fundamental_list_model.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class FundamentalEvent extends Equatable {
  const FundamentalEvent();
  @override
  List<Object> get props => [];
}

class FundamentalRequested extends FundamentalEvent {
  final String codeEmiten;
  FundamentalRequested({this.codeEmiten});
  @override
  List<Object> get props => [codeEmiten];
}

// state
abstract class FundamentalState extends Equatable {
  const FundamentalState();
  @override
  List<Object> get props => [];
}

class FundamentalLoading extends FundamentalState {}

class FundamentalLoaded extends FundamentalState {
  final FundamentalList fundamental;

  FundamentalLoaded({this.fundamental});

  @override
  List<Object> get props => [fundamental];
}

class FundamentalError extends FundamentalState {}

// bloc
class FundamentalBloc extends Bloc<FundamentalEvent, FundamentalState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  FundamentalBloc({@required this.dio}) : super(FundamentalLoading());

  @override
  Stream<FundamentalState> mapEventToState(FundamentalEvent event) async* {
    if (event is FundamentalRequested) {
      yield* _mapFundamentalRequestedToState(event.codeEmiten);
    }
  }

  Stream<FundamentalState> _mapFundamentalRequestedToState(
      String codeEmiten) async* {
    yield FundamentalLoading();
    final response = await _fetchFundamental(codeEmiten);
    try {
      if (response.statusCode == 200) {
        yield FundamentalLoaded(
            fundamental: fundamentalListFromJson(jsonEncode(response.data)));
      } else {
        yield FundamentalError();
      }
    } catch (_) {
      yield FundamentalError();
    }
  }

  Future<Response> _fetchFundamental(String codeEmiten) async {
    // final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/emiten-fundamental/fundamental/?code_emiten=$codeEmiten');
      // options: Options(
      //     headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
