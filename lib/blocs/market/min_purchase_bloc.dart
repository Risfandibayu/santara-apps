import 'dart:io';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class MinPurchaseEvent extends Equatable {
  const MinPurchaseEvent();
  @override
  List<Object> get props => [];
}

class MinPurchaseRequested extends MinPurchaseEvent {
  MinPurchaseRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class MinPurchaseState extends Equatable {
  const MinPurchaseState();
  @override
  List<Object> get props => [];
}

class MinPurchaseLoading extends MinPurchaseState {}

class MinPurchaseLoaded extends MinPurchaseState {
  final double minPurchase;

  MinPurchaseLoaded({this.minPurchase});

  @override
  List<Object> get props => [minPurchase];
}

class MinPurchaseError extends MinPurchaseState {}

// bloc
class MinPurchaseBloc extends Bloc<MinPurchaseEvent, MinPurchaseState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  MinPurchaseBloc({@required this.dio}) : super(MinPurchaseLoading());

  @override
  Stream<MinPurchaseState> mapEventToState(MinPurchaseEvent event) async* {
    if (event is MinPurchaseRequested) {
      yield* _mapMinPurchaseRequestedToState();
    }
  }

  Stream<MinPurchaseState> _mapMinPurchaseRequestedToState() async* {
    yield MinPurchaseLoading();
    final response = await _fetchMinPurchase();
    try {
      if (response.statusCode == 200) {
        yield MinPurchaseLoaded(minPurchase: response.data["data"]["min_purchase"] / 1);
      } else {
        yield MinPurchaseError();
      }
    } catch (_, __) {
      yield MinPurchaseError();
    }
  }

  Future<Response> _fetchMinPurchase() async {
    final token = await storage.read(key: "token");
    try {
      final response = await dio.get(
          '$apiMarket/cmn/$versionApiMarket/market-config/minimum-purchase',
          options: Options(
              headers: {HttpHeaders.authorizationHeader: 'Bearer $token'}));
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
