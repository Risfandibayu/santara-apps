import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/models/market/emiten_by_uuid.dart';
import 'package:santaraapp/utils/api.dart';

// event
abstract class EmitenByUuidEvent extends Equatable {
  const EmitenByUuidEvent();
  @override
  List<Object> get props => [];
}

class EmitenByUuidRequested extends EmitenByUuidEvent {
  final String uuid;
  EmitenByUuidRequested({this.uuid});
  @override
  List<Object> get props => [uuid];
}

// state
abstract class EmitenByUuidState extends Equatable {
  const EmitenByUuidState();
  @override
  List<Object> get props => [];
}

class EmitenByUuidLoading extends EmitenByUuidState {}

class EmitenByUuidLoaded extends EmitenByUuidState {
  final EmitenByUuid emitenByUuid;

  EmitenByUuidLoaded({this.emitenByUuid});

  @override
  List<Object> get props => [emitenByUuid];
}

class EmitenByUuidError extends EmitenByUuidState {}

// bloc
class EmitenByUuidBloc extends Bloc<EmitenByUuidEvent, EmitenByUuidState> {
  final Dio dio;
  final storage = secureStorage.FlutterSecureStorage();
  EmitenByUuidBloc({this.dio}) : super(EmitenByUuidLoading());

  @override
  Stream<EmitenByUuidState> mapEventToState(EmitenByUuidEvent event) async* {
    if (event is EmitenByUuidRequested) {
      yield* _mapEmitenByUuidRequestedToState(event.uuid);
    }
  }

  Stream<EmitenByUuidState> _mapEmitenByUuidRequestedToState(
      String uuid) async* {
    yield EmitenByUuidLoading();
    final response = await _fetchEmitenByUuid(uuid);
    try {
      if (response.statusCode == 200) {
        yield EmitenByUuidLoaded(
            emitenByUuid: emitenByUuidFromJson(jsonEncode(response.data)));
      } else {
        yield EmitenByUuidError();
      }
    } catch (_, __) {
      yield EmitenByUuidError();
    }
  }

  Future<Response> _fetchEmitenByUuid(String uuid) async {
    try {
      final response =
          await dio.get('$apiMarket/cmn/$versionApiMarket/emiten/uuid/$uuid');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
