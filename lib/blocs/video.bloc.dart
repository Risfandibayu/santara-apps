import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rxdart/rxdart.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/video/Video.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/video/VideoCategory.dart';
import 'package:santaraapp/utils/api.dart';

class VideoBloc {
  final storage = new FlutterSecureStorage();

  PublishSubject<List<Videos>> _video = PublishSubject<List<Videos>>();
  Stream<List<Videos>> get video => _video.stream;

  PublishSubject<List<String>> _categoryList = PublishSubject<List<String>>();
  Stream<List<String>> get categoryList => _categoryList.stream;

  PublishSubject<String> _textSort = PublishSubject<String>();
  Stream<String> get textSort => _textSort.stream;

  PublishSubject<String> _category = PublishSubject<String>();
  Stream<String> get category => _category.stream;

  bool likes = false;
  bool viewer = false;
  bool newest = false;
  String categoryStr = "Semua";
  String keySearch = "";

  BehaviorSubject<bool> _likes;
  BehaviorSubject<bool> _viewer;
  BehaviorSubject<bool> _newest;
  BehaviorSubject<String> _categoryStr;
  BehaviorSubject<String> _keySearch;

  VideoBloc(
      {this.likes,
      this.viewer,
      this.newest,
      this.categoryStr,
      this.keySearch}) {
    _likes = new BehaviorSubject<bool>.seeded(this.likes);
    _viewer = new BehaviorSubject<bool>.seeded(this.viewer);
    _newest = new BehaviorSubject<bool>.seeded(this.newest);
    _categoryStr = new BehaviorSubject<String>.seeded(this.categoryStr);
    _keySearch = new BehaviorSubject<String>.seeded(this.keySearch);
  }

  void setLikes() {
    likes = true;
    viewer = false;
    newest = false;
    _likes.sink.add(likes);
    _viewer.sink.add(viewer);
    _newest.sink.add(newest);
  }

  void setViewer() {
    likes = false;
    viewer = true;
    newest = false;
    _likes.sink.add(likes);
    _viewer.sink.add(viewer);
    _newest.sink.add(newest);
  }

  void setNewest() {
    likes = false;
    viewer = false;
    newest = true;
    _likes.sink.add(likes);
    _viewer.sink.add(viewer);
    _newest.sink.add(newest);
  }

  void setCategory(str) {
    categoryStr = str;
    getVideo();
  }

  void setSearchKey(str) {
    keySearch = str;
    _keySearch.sink.add(keySearch);
    getVideo();
  }

  getVideo() async {
    String url = categoryStr == "Semua" && keySearch == null
        ? '$apiLocal/videos/video?title&category&likes=$likes&viewer=$viewer&newest=$newest'
        : categoryStr != "Semua" && keySearch == null
            ? '$apiLocal/videos/video?title&category=$categoryStr&likes=$likes&viewer=$viewer&newest=$newest'
            : categoryStr == "Semua" && keySearch != null
                ? '$apiLocal/videos/video?title=$keySearch&category&likes=$likes&viewer=$viewer&newest=$newest'
                : '$apiLocal/videos/video?title=$keySearch&category=$categoryStr&likes=$likes&viewer=$viewer&newest=$newest';
    try {
      // var token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final http.Response response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        _category.sink.add(categoryStr);
        if (likes) {
          _textSort.sink.add("Paling Disukai");
        } else if (newest) {
          _textSort.sink.add("Terbaru");
        } else if (viewer) {
          _textSort.sink.add("Paling Banyak Dilihat");
        } else {
          _textSort.sink.add("Urutkan");
        }
        final data = videosFromJson(response.body);
        _video.sink.add(data);
      } else {
        _video.sink.addError("Tidak dapat mengambil data!");
      }
    } catch (e) {
      _video.sink.addError(e);
    }
  }

  getCategoryList() async {
    try {
      final http.Response response =
          await http.get('$apiLocal/videos/category');
      if (response.statusCode == 200) {
        List<VideoCategory> cat = videoCategoryFromJson(response.body);
        List<String> data = ["Semua"];
        for (var i = 0; i < cat.length; i++) {
          data.add(cat[i].category);
        }
        _categoryList.sink.add(data);
      } else {
        _categoryList.sink.addError("Tidak dapat mengambil data!");
      }
    } catch (e) {
      _categoryList.sink.addError(e);
    }
  }

  dispose() {
    _video.close();
    _categoryList.close();
    _category.close();
    _likes.close();
    _newest.close();
    _viewer.close();
    _textSort.close();
    _categoryStr.close();
    _keySearch.close();
  }
}
