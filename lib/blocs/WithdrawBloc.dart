// import 'package:rxdart/rxdart.dart';
// import 'package:santaraapp/resources/Repository.dart';
// import 'package:santaraapp/models/Withdraw.dart';
// import 'package:santaraapp/bloc/BlocProvider.dart';

// class WithdrawBloc implements BlocBase {
//   final repository = Repository();
//   ReplaySubject withdrawFetcher = ReplaySubject<List<Withdraw>>();

//   List<Withdraw> _allHistory;
//   List<Withdraw> get allHistory => _allHistory;

//   void fetchAllHistory() async {
//     List<Withdraw> withdraw = await repository.getHistoryWithdraw();
//     withdrawFetcher.sink.add(withdraw);
//   }
  
//   @override
//   dispose(){
//     withdrawFetcher?.close();
//   }
// }