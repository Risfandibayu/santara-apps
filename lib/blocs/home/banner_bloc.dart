import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/home/Banners.dart';
import 'package:santaraapp/utils/api.dart';

abstract class BannerEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class BannerFetched extends BannerEvent {}

abstract class BannerState extends Equatable {
  const BannerState();

  @override
  List<Object> get props => [];
}

class BannerInitial extends BannerState {
  final List<String> string;

  const BannerInitial({
    this.string,
  });

  @override
  List<Object> get props => [string];
}

class BannerFailure extends BannerState {
  final List<String> string;

  const BannerFailure({
    this.string,
  });

  @override
  List<Object> get props => [string];
}

class BannerSuccess extends BannerState {
  final List<Banner> banner;

  const BannerSuccess({
    this.banner,
  });

  @override
  List<Object> get props => [banner];

  @override
  String toString() => 'BannerLoaded { Banners: ${banner.length} }';
}

class BannerBloc extends Bloc<BannerEvent, BannerState> {
  final Dio dio;

  static List<String> defaultValue = [
    "assets/slide/responsive1.jpg",
    "assets/slide/responsive1.jpg",
    "assets/slide/responsive1.jpg"
  ];

  BannerBloc({this.dio}) : super(BannerInitial(string: defaultValue));

  @override
  Stream<BannerState> mapEventToState(BannerEvent event) async* {
    final currentState = state;
    if (event is BannerFetched) {
      try {
        final res = await _fetchBanners();
        if (res.statusCode == 200) {
          if (currentState is BannerInitial) {
            yield BannerSuccess(banner: bannerFromJson(jsonEncode(res.data)));
            return;
          }
          if (currentState is BannerSuccess) {
            yield BannerSuccess(
              banner: bannerFromJson(res.data),
            );
          }
        } else {
          yield BannerFailure(string: defaultValue);
        }
      } catch (_) {
        yield BannerFailure(string: defaultValue);
      }
    }
  }

  Future<Response> _fetchBanners() async {
    try {
      final response = await dio.get("$apiLocal/headers");
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
