import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/video/VideoCategory.dart';
import 'package:santaraapp/repositories/video_repository.dart';

// event
abstract class VideoCategoryEvent extends Equatable {
  const VideoCategoryEvent();
}

class VideoCategoryRequested extends VideoCategoryEvent {
  @override
  List<Object> get props => [];
}

class VideoCategoryRefreshRequested extends VideoCategoryEvent {
  @override
  List<Object> get props => [];
}

// state
abstract class VideoCategoryState extends Equatable {
  const VideoCategoryState();

  @override
  List<Object> get props => [];
}

class VideoUnauthorized extends VideoCategoryState {}

class VideoCategoryInitial extends VideoCategoryState {}

class VideoCategoryLoadInProgress extends VideoCategoryState {}

class VideoCategoryLoadFailure extends VideoCategoryState {
  final String error;

  const VideoCategoryLoadFailure({this.error});

  @override
  List<Object> get props => [error];
}

class VideoCategoryLoadSuccess extends VideoCategoryState {
  final List<VideoCategory> category;

  const VideoCategoryLoadSuccess({@required this.category})
      : assert(category != null);

  @override
  List<Object> get props => [category];
}

// bloc
class VideoCategoryBloc extends Bloc<VideoCategoryEvent, VideoCategoryState> {
  final VideoRepository videoRepository;

  VideoCategoryBloc({@required this.videoRepository})
      : assert(videoRepository != null),
        super(VideoCategoryInitial());

  @override
  Stream<VideoCategoryState> mapEventToState(VideoCategoryEvent event) async* {
    if (event is VideoCategoryRequested) {
      yield* _mapVideoCategoryRequestedToState(event);
    } else if (event is VideoCategoryRefreshRequested) {
      yield* _mapVideoCategoryRefreshRequestedToState(event);
    }
  }

  Stream<VideoCategoryState> _mapVideoCategoryRequestedToState(
      VideoCategoryRequested event) async* {
    yield VideoCategoryLoadInProgress();
    try {
      final response = await videoRepository.getVideoCategory();
      if (response.statusCode == 200) {
        List<VideoCategory> category = [
          VideoCategory(category: "Semua", uuid: "")
        ];
        category.addAll(videoCategoryFromJson(jsonEncode(response.data)));
        yield VideoCategoryLoadSuccess(category: category);
      } else if(response.statusCode == 401) {
        yield VideoUnauthorized();
      } else {
        final data = jsonDecode(response.data);
        yield VideoCategoryLoadFailure(error: data["message"]);
      }
    } catch (_) {
      yield VideoCategoryLoadFailure(error: _.toString());
    }
  }

  Stream<VideoCategoryState> _mapVideoCategoryRefreshRequestedToState(
      VideoCategoryRefreshRequested event) async* {
    yield VideoCategoryLoadInProgress();
    try {
      final response = await videoRepository.getVideoCategory();
      List<VideoCategory> category = [
        VideoCategory(category: "Semua", uuid: "")
      ];
      category.addAll(videoCategoryFromJson(jsonEncode(response.data)));
      yield VideoCategoryLoadSuccess(category: category);
    } catch (_) {
      yield state;
    }
  }
}
