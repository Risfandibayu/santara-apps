import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/models/video/Video.dart';
import 'package:santaraapp/repositories/video_repository.dart';

// video event
abstract class VideoEvent extends Equatable {
  const VideoEvent();
}

class VideoRequested extends VideoEvent {
  final String title;
  final String category;
  final bool likes;
  final bool viewer;
  final bool newest;

  const VideoRequested(
      {this.title, this.category, this.likes, this.viewer, this.newest});

  @override
  List<Object> get props => [title, category, likes, viewer, newest];
}

class VideoRefreshRequested extends VideoEvent {
  final String title;
  final String category;
  final bool likes;
  final bool viewer;
  final bool newest;

  const VideoRefreshRequested(
      {this.title, this.category, this.likes, this.viewer, this.newest});

  @override
  List<Object> get props => [title, category, likes, viewer, newest];
}

// video state
abstract class VideoState extends Equatable {
  const VideoState();

  @override
  List<Object> get props => [];
}

class VideoInitial extends VideoState {}

class VideoLoadInProgress extends VideoState {}

class VideoLoadFailure extends VideoState {
  final String error;

  const VideoLoadFailure({this.error});

  @override
  List<Object> get props => [error];
}

class VideoLoadSuccess extends VideoState {
  final List<Videos> video;

  const VideoLoadSuccess({@required this.video}) : assert(video != null);

  @override
  List<Object> get props => [video];
}

// video bloc
class VideoBloc extends Bloc<VideoEvent, VideoState> {
  final VideoRepository videoRepository;

  VideoBloc({@required this.videoRepository})
      : assert(videoRepository != null),
        super(VideoInitial());

  @override
  Stream<VideoState> mapEventToState(VideoEvent event) async* {
    if (event is VideoRequested) {
      yield* _mapVideoRequestedToState(event);
    } else if (event is VideoRefreshRequested) {
      yield* _mapVideoRefreshRequestedToState(event);
    }
  }

  Stream<VideoState> _mapVideoRequestedToState(VideoRequested event) async* {
    yield VideoLoadInProgress();
    final response = await videoRepository.getVideo(
        event.title, event.category, event.likes, event.viewer, event.newest);
    try {
      if (response.statusCode == 200) {
        yield VideoLoadSuccess(
            video: videosFromJson(jsonEncode(response.data)));
      } else {
        final data = response.data;
        yield VideoLoadFailure(error: data["message"] ?? data["error"]);
      }
    } catch (_) {
      yield VideoLoadFailure(error: _);
    }
  }

  Stream<VideoState> _mapVideoRefreshRequestedToState(
      VideoRefreshRequested event) async* {
    yield VideoLoadInProgress();
    try {
      final response = await videoRepository.getVideo(
          event.title, event.category, event.likes, event.viewer, event.newest);
      yield VideoLoadSuccess(video: videosFromJson(jsonEncode(response.data)));
    } catch (_) {
      yield state;
    }
  }
}
