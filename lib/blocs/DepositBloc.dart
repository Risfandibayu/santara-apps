// import 'package:rxdart/rxdart.dart';
// import 'package:santaraapp/resources/Repository.dart';
// import 'package:santaraapp/models/Deposit.dart';
// import 'package:santaraapp/bloc/BlocProvider.dart';


// class DepositBloc implements BlocBase {
//   final repository = Repository();
//   ReplaySubject depositFetcher = ReplaySubject<List<Deposit>>();

//   List<Deposit> _allHistoryDeposit;
//   List<Deposit> get allHistoryDeposit => _allHistoryDeposit;

//   fetchAllHistoryDeposit() async {
//     List<Deposit> deposit = await repository.getHistoryDeposit();
//     depositFetcher.sink.add(deposit);
//   }

//   @override
//   dispose(){
//     depositFetcher?.close();
//   }
// }