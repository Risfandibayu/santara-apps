import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as localStorage;
import 'package:santaraapp/utils/api.dart';

// event
abstract class CurrentTimeEvent extends Equatable {
  const CurrentTimeEvent();
  @override
  List<Object> get props => [];
}

class CurrentTimeRequested extends CurrentTimeEvent {
  CurrentTimeRequested();
  @override
  List<Object> get props => [];
}

// state
abstract class CurrentTimeState extends Equatable {
  const CurrentTimeState();
  @override
  List<Object> get props => [];
}

class CurrentTimeLoading extends CurrentTimeState {}

class CurrentTimeLoaded extends CurrentTimeState {
  final DateTime now;

  CurrentTimeLoaded({this.now});

  @override
  List<Object> get props => [now];
}

class CurrentTimeError extends CurrentTimeState {}

// bloc
class CurrentTimeBloc extends Bloc<CurrentTimeEvent, CurrentTimeState> {
  final Dio dio;
  final storage = localStorage.FlutterSecureStorage();
  CurrentTimeBloc({@required this.dio}) : super(CurrentTimeLoading());

  @override
  Stream<CurrentTimeState> mapEventToState(CurrentTimeEvent event) async* {
    if (event is CurrentTimeRequested) {
      yield* _mapCurrentTimeRequestedToState();
    }
  }

  Stream<CurrentTimeState> _mapCurrentTimeRequestedToState() async* {
    yield CurrentTimeLoading();
    final response = await _fetchCurrentTime();
    try {
      if (response.statusCode == 200) {
        DateTime now = DateTime.parse(response.data["time"]);
        yield CurrentTimeLoaded(now: now);
      } else {
        yield CurrentTimeError();
      }
    } catch (_, __) {
      yield CurrentTimeError();
    }
  }

  Future<Response> _fetchCurrentTime() async {
    try {
      final response = await dio.get('$apiLocal/traders/ctime');
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
