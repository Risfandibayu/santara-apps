import 'package:dartz/dartz.dart';
import 'package:santaraapp/features/dana/data/models/result_apply_token_model.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/dana_balance_model.dart';

abstract class DanaRepository {
  Future<Either<Failure, List<DanaBalanceModel>>> getDanaBalance();
  Future<Either<Failure, String>> getUrlLoginDana();
  Future<Either<Failure, bool>> submitLogoutDana();
  Future<Either<Failure, ResultApplyTokenModel>> applyTokenDana(
      String authCode);
}
