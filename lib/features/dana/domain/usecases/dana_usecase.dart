import 'package:dartz/dartz.dart';
import 'package:santaraapp/features/dana/data/models/result_apply_token_model.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/dana_balance_model.dart';
import '../repositories/dana_repository.dart';

class GetDanaBalance implements UseCase<List<DanaBalanceModel>, NoParams> {
  final DanaRepository repository;
  GetDanaBalance(this.repository);

  @override
  Future<Either<Failure, List<DanaBalanceModel>>> call(NoParams params) async {
    return await repository.getDanaBalance();
  }
}

class GetUrlLoginDana implements UseCase<String, NoParams> {
  final DanaRepository repository;
  GetUrlLoginDana(this.repository);

  @override
  Future<Either<Failure, String>> call(NoParams params) async {
    return await repository.getUrlLoginDana();
  }
}

class SubmitLogoutDana implements UseCase<bool, NoParams> {
  final DanaRepository repository;
  SubmitLogoutDana(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) async {
    return await repository.submitLogoutDana();
  }
}

class ApplyTokenDana implements UseCase<ResultApplyTokenModel, String> {
  final DanaRepository repository;
  ApplyTokenDana(this.repository);

  @override
  Future<Either<Failure, ResultApplyTokenModel>> call(String authCode) async {
    return await repository.applyTokenDana(authCode);
  }
}
