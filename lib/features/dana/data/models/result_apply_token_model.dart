import 'dart:convert';

ResultApplyTokenModel resultApplyTokenModelFromJson(String str) =>
    ResultApplyTokenModel.fromJson(json.decode(str));

String resultApplyTokenModelToJson(ResultApplyTokenModel data) =>
    json.encode(data.toJson());

class ResultApplyTokenModel {
  ResultApplyTokenModel({
    this.accessToken,
    this.refreshToken,
    this.expiresIn,
    this.reExpiresIn,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.id,
  });

  String accessToken;
  String refreshToken;
  DateTime expiresIn;
  DateTime reExpiresIn;
  int userId;
  DateTime createdAt;
  DateTime updatedAt;
  int id;

  factory ResultApplyTokenModel.fromJson(Map<String, dynamic> json) =>
      ResultApplyTokenModel(
        accessToken: json["access_token"],
        refreshToken: json["refresh_token"],
        expiresIn: DateTime.parse(json["expires_in"]),
        reExpiresIn: DateTime.parse(json["re_expires_in"]),
        userId: json["user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "refresh_token": refreshToken,
        "expires_in": expiresIn.toIso8601String(),
        "re_expires_in": reExpiresIn.toIso8601String(),
        "user_id": userId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "id": id,
      };
}
