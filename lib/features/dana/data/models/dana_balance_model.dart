import 'dart:convert';

List<DanaBalanceModel> danaBalanceModelFromJson(String str) =>
    List<DanaBalanceModel>.from(
        json.decode(str).map((x) => DanaBalanceModel.fromJson(x)));

String danaBalanceModelToJson(List<DanaBalanceModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DanaBalanceModel {
  DanaBalanceModel({
    this.resourceType,
    this.value,
  });

  String resourceType;
  dynamic value;

  factory DanaBalanceModel.fromJson(Map<String, dynamic> json) =>
      DanaBalanceModel(
        resourceType: json["resourceType"],
        value: json["value"].runtimeType == String
            ? json["value"]
            : ValueClass.fromJson(json["value"]),
      );

  Map<String, dynamic> toJson() => {
        "resourceType": resourceType,
        "value": value.runtimeType == String ? value : value.toJson(),
      };
}

class ValueClass {
  ValueClass({
    this.amount,
    this.currency,
  });

  num amount;
  String currency;

  factory ValueClass.fromJson(Map<String, dynamic> json) => ValueClass(
        amount: json["amount"] == "0" ? 0 : num.parse(json["amount"]) / 100,
        currency: json["currency"],
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "currency": currency,
      };
}
