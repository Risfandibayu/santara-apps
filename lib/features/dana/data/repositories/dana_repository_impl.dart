import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/features/dana/data/models/result_apply_token_model.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/dana_repository.dart';
import '../datasources/dana_remote_data_source.dart';
import '../models/dana_balance_model.dart';

class DanaRepositoryImpl implements DanaRepository {
  final NetworkInfo networkInfo;
  final DanaRemoteDataSource remoteDataSource;

  DanaRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<DanaBalanceModel>>> getDanaBalance() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getDanaBalance();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, String>> getUrlLoginDana() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getUrlLoginDana();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> submitLogoutDana() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.submitLogoutDana();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, ResultApplyTokenModel>> applyTokenDana(
      String authCode) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.applyTokenDana(authCode);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
