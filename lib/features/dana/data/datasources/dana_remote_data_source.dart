import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../../../../utils/api.dart';
import '../models/dana_balance_model.dart';
import '../models/result_apply_token_model.dart';

abstract class DanaRemoteDataSource {
  Future<List<DanaBalanceModel>> getDanaBalance();
  Future<String> getUrlLoginDana();
  Future<bool> submitLogoutDana();
  Future<ResultApplyTokenModel> applyTokenDana(String authCode);
}

class DanaRemoteDataSourceImpl implements DanaRemoteDataSource {
  final RestClient client;
  DanaRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<DanaBalanceModel>> getDanaBalance() async {
    try {
      final result =
          await client.postExternalUrl(url: "$apiLocalImage/dana/get-balance");
      if (result.statusCode == 200) {
        return danaBalanceModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<String> getUrlLoginDana() async {
    try {
      final result = await client.postExternalUrl(
          url: "$apiLocalImage/dana/login", body: {"isSeamless": true});
      if (result.statusCode == 200) {
        return result.data["url"];
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> submitLogoutDana() async {
    try {
      final result =
          await client.postExternalUrl(url: "$apiLocalImage/dana/logout");
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<ResultApplyTokenModel> applyTokenDana(String authCode) async {
    try {
      final result = await client.postExternalUrl(
          url: "$apiLocalImage/dana/apply-token", body: {"authCode": authCode});
      if (result.statusCode == 200) {
        return ResultApplyTokenModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
