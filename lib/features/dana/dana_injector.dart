import '../../injector_container.dart';
import 'data/datasources/dana_remote_data_source.dart';
import 'data/repositories/dana_repository_impl.dart';
import 'domain/repositories/dana_repository.dart';
import 'domain/usecases/dana_usecase.dart';
import 'presentation/bloc/auth_dana/auth_dana_bloc.dart';
import 'presentation/bloc/dana_balance/dana_balance_bloc.dart';

void initDanaInjector() {
  // Bloc
  sl.registerFactory(
    () => DanaBalanceBloc(
      getDanaBalance: sl(),
      getUserByUuid: sl(),
    ),
  );
  sl.registerFactory(
    () => AuthDanaBloc(
      getUrlLoginDana: sl(),
      submitLogoutDana: sl(),
      applyTokenDana: sl(),
    ),
  );

  //! Global Usecase
  sl.registerLazySingleton(() => GetDanaBalance(sl()));
  sl.registerLazySingleton(() => GetUrlLoginDana(sl()));
  sl.registerLazySingleton(() => SubmitLogoutDana(sl()));
  sl.registerLazySingleton(() => ApplyTokenDana(sl()));

  //! Global Repository
  sl.registerLazySingleton<DanaRepository>(
    () => DanaRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<DanaRemoteDataSource>(
    () => DanaRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
