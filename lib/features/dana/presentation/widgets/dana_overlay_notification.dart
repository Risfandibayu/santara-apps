import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

import '../../../../core/utils/ui/screen_sizes.dart';

class DanaOverlayNotifications {
  static showDanaOverlayNotification(String message) {
    showOverlayNotification((context) {
      return SafeArea(
        child: Material(
          color: Colors.transparent,
          child: Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: EdgeInsets.only(top: Sizes.s80),
              child: Card(
                margin: EdgeInsets.symmetric(horizontal: Sizes.s20),
                color: Color(0xFFF5F5F5),
                child: SafeArea(
                  child: ListTile(
                    leading: SizedBox.fromSize(
                        size: const Size(40, 40),
                        child: ClipOval(
                            child: Image.asset("assets/dana/dana.png"))),
                    title: Text(
                      '$message',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }, duration: Duration(seconds: 5));
  }
}
