import 'package:flutter/material.dart';

import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';

class DanaPopup {
  static void resetPhoneNumber(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Setel Ulang Nomor Telepon",
                style: TextStyle(
                    fontSize: FontSize.s18, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: Sizes.s15),
              Text(
                "Santara tidak dapat menghubungkan DANA dikarenakan nomor telepon akun DANA Anda berubah.\n\nHubungkan kembali akun DANA Anda untuk dapat menggunakan layanan ini.",
                textAlign: TextAlign.left,
                style:
                    TextStyle(fontSize: FontSize.s14, color: Color(0xFF434343)),
              ),
              SizedBox(height: Sizes.s15),
              SantaraMainButton(
                title: "Mengerti",
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        );
      },
    );
  }
}
