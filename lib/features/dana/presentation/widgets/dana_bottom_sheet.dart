import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../../../utils/api.dart';
import '../../../../utils/sizes.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../pages/dana_webview.dart';

class DanaBottomSheet {
  static Widget _headerBottomSheet(BuildContext context, String title) {
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
      GestureDetector(
        child: Icon(
          Icons.close,
          color: Colors.black,
        ),
        onTap: () => Navigator.pop(context),
      ),
      Container(width: 5),
      Expanded(
        child: Text(
          title,
          overflow: TextOverflow.visible,
          style: TextStyle(
              fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
        ),
      ),
    ]);
  }

  static void confirmConnectToDana(BuildContext context, Function onConfirm) {
    List<String> data = [
      "Keamanan saldo terjamin",
      "Mudah dan Praktis",
      "Cukup pakai nomor handphone terdaftar di Santara"
    ];
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // title
              _headerBottomSheet(
                  context, "Mau Beli Saham Bisnis di Santara ?\nDANA-in Aja !"),
              SizedBox(height: Sizes.s16),

              // banner
              Stack(
                children: [
                  Container(
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Color(0xFF008CEB)),
                    child: Center(
                      child: Text(
                        "Beli saham lebih mudah pakai DANA",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Image.asset("assets/dana/dana_cover.png"),
                ],
              ),
              SizedBox(height: Sizes.s10),

              // text
              Text(
                  "Yuk, ikut DANA-in bisnis di Santara dengan aktivasi akun Dana Anda. Transaksi jadi semakin mudah!"),
              SizedBox(height: Sizes.s10),

              // nilai plus pake DANA
              ListView.separated(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: data.length,
                itemBuilder: (_, i) {
                  return ListTile(
                    leading: Icon(
                      Icons.check,
                      color: Color(0xFFBF2D30),
                    ),
                    title: Text(
                      data[i],
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  );
                },
                separatorBuilder: (_, i) {
                  return Container(
                    height: 1,
                    color: Color(0xFFDDDDDD),
                  );
                },
              ),
              SizedBox(height: Sizes.s16),

              // button
              SantaraMainButton(
                key: Key("confirm_connect_to_dana"),
                title: "Hubungkan akun DANA ke Santara Sekarang",
                color: Color(0xFF008CEB),
                onPressed: onConfirm,
              ),
              SizedBox(height: Sizes.s16),

              // ***
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text:
                      "Selanjutnya, Anda akan diarahkan ke halaman DANA. Dengan menghubungkan akun, Anda sudah setuju dengan ketentuan dari ",
                  style: TextStyle(
                      fontFamily: 'Nunito',
                      color: Colors.black,
                      fontSize: FontSize.s14),
                  children: [
                    TextSpan(
                      text: "DANA",
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => DanaWebview(
                                  type: DanaWebviewType.normal,
                                  appbarName: "Syarat dan Ketentuan",
                                  url: "https://www.dana.id/terms",
                                ),
                              ),
                            ),
                      style: TextStyle(
                          color: Color(0xFF008CEB),
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void danaActivationFailed(
      BuildContext context, Function reRegistration) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // title
              _headerBottomSheet(context, "Aktivasi Gagal"),
              SizedBox(height: Sizes.s16),

              // icon
              Image.asset(
                "assets/dana/dana_failed.png",
                width: MediaQuery.of(context).size.width / 2,
              ),
              SizedBox(height: Sizes.s16),

              // message
              Text(
                "Maaf, registrasi kamu tidak berhasil",
                style: TextStyle(
                    fontSize: Sizes.s16,
                    color: Color(0xFFF35E63),
                    fontWeight: FontWeight.bold),
              ),
              Text("Sistem sedang sibuk. Silahkan coba lagi nanti"),
              SizedBox(height: Sizes.s16),

              // button
              SantaraMainButton(
                key: Key("resubmit_dana_activation"),
                title: "DAFTARKAN KEMBALI",
                color: Color(0xFF008CEB),
                onPressed: reRegistration,
              ),
              SizedBox(height: Sizes.s8),
              SantaraOutlineButton(
                key: Key("cancel"),
                color: Color(0xFF008CEB),
                title: "Keluar",
                onPressed: () => Navigator.pop(context),
              )
            ],
          ),
        );
      },
    );
  }

  static void confirmDeactivationDana(BuildContext context, Function confirm) {
    bool isCheck = false;
    List<String> data = [
      "Saldo DANA akan tetap tersimpan di akun DANA",
      "Pengguna Dapat mencairkan saldo yang terdapat di akun DANA dengan menghubungi Customer Support DANA.",
      "Akun DANA dapat dihubungkan kembali melaui halaman utama",
    ];
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return StatefulBuilder(
          builder: (context, setState) {
            return Padding(
              padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // title
                  _headerBottomSheet(context, "Putuskan Akun"),
                  SizedBox(height: Sizes.s16),

                  // content
                  Text(
                      "Apakah Anda yakin ingin memutuskan akun DANA Anda dari platform SANTARA?"),
                  SizedBox(height: Sizes.s16),

                  // list
                  ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: data.length,
                    itemBuilder: (_, i) {
                      return Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: Sizes.s4),
                              child: Icon(
                                Icons.fiber_manual_record,
                                size: 10,
                                color: Color(0xFF118EEA),
                              ),
                            ),
                            Container(width: 10),
                            Flexible(
                              child: Text(
                                data[i],
                              ),
                            ),
                          ]);
                    },
                  ),
                  SizedBox(height: Sizes.s16),
                  // checkbox
                  Row(
                    children: [
                      Checkbox(
                        value: isCheck,
                        onChanged: (v) {
                          setState(() => isCheck = !isCheck);
                        },
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      ),
                      Text("Saya setuju penghapusan akun DANA"),
                    ],
                  ),
                  SizedBox(height: Sizes.s10),
                  // button
                  SantaraMainButton(
                    key: Key('confirm_deactivate_dana'),
                    title: "Lanjutkan",
                    color: Color(0xFF008CEB),
                    disableColor: Color(0xFFE4E4E4),
                    isActive: isCheck,
                    onPressed: confirm,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
