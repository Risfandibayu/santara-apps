import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import '../../../../core/utils/platforms/url_launcher.dart';

enum DanaWebviewType { auth, trx, normal }

class DanaWebview extends StatefulWidget {
  final String appbarName;
  final String url;
  final DanaWebviewType type;

  const DanaWebview({Key key, this.appbarName, this.url, @required this.type})
      : super(key: key);

  @override
  _DanaWebviewState createState() => _DanaWebviewState();
}

class _DanaWebviewState extends State<DanaWebview> {
  num _stackToView = 1;
  InAppWebViewController webView;

  void _handleLoad() {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.appbarName, style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: IndexedStack(
          index: _stackToView,
          children: <Widget>[
            InAppWebView(
              initialUrl: widget.url,
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                    debuggingEnabled: false,
                    preferredContentMode: UserPreferredContentMode.MOBILE),
              ),
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onLoadStart: (InAppWebViewController controller, String url) {
                if (url.contains("intent://link.dana.id/")) {
                  Navigator.pop(context);
                  UrlLauncher.open(url: url.replaceAll("intent://", "http://"));
                } else {
                  if (widget.type == DanaWebviewType.auth) {
                    if ((url.contains('dev.santara.id') ||
                            url.contains('santara.co.id')) &&
                        url.contains("auth_code")) {
                      var uri = Uri.parse(url);
                      String authCode;
                      uri.queryParameters.forEach((key, value) {
                        if (key == "auth_code") {
                          authCode = value;
                        }
                      });
                      Navigator.pop(context, authCode);
                    }
                  } else if (widget.type == DanaWebviewType.trx) {
                    if (url.contains('dev.santara.id') ||
                        url.contains('santara.co.id')) {
                      Navigator.pop(context, true);
                    }
                  } else {
                    if (url.contains('dev.santara.id') ||
                        url.contains('santara.co.id')) {
                      Navigator.pop(context);
                    }
                  }
                }
              },
              onLoadStop:
                  (InAppWebViewController controller, String url) async {
                _handleLoad();
              },
            ),
            Container(
              color: Colors.white,
              child: Center(
                child: CupertinoActivityIndicator(),
              ),
            ),
          ],
        ));
  }
}
