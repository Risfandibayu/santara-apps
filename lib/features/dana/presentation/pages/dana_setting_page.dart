import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';

import '../../../../core/utils/formatter/currency_formatter.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../pages/Home.dart';
import '../../../../widget/security_token/pin/SecurityPinUI.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../bloc/auth_dana/auth_dana_bloc.dart';
import '../bloc/dana_balance/dana_balance_bloc.dart';
import '../widgets/dana_bottom_sheet.dart';
import '../widgets/dana_overlay_notification.dart';

class DanaSettingPage extends StatefulWidget {
  @override
  _DanaSettingPageState createState() => _DanaSettingPageState();
}

class _DanaSettingPageState extends State<DanaSettingPage> {
  AuthDanaBloc _authDanaBloc;
  void _inputPin(BuildContext context, Function onSuccess) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          type: SecurityType.check,
          title: "Masukan PIN Anda",
          onSuccess: (pin, finger) {
            Navigator.pop(context);
            onSuccess();
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    _authDanaBloc = BlocProvider.of<AuthDanaBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthDanaBloc, AuthDanaState>(
      listener: (context, state) {
        if (state is LogoutDanaSuccess) {
          Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(builder: (_) => Home()), (route) => false);
          DanaOverlayNotifications.showDanaOverlayNotification(
              "Akun DANA berhasil dinonaktifkan");
        }
      },
      builder: (context, state) {
        return ModalProgressHUD(
          inAsyncCall: state is AuthDanaLoading,
          color: Colors.grey,
          opacity: 0.5,
          progressIndicator: CupertinoActivityIndicator(),
          child: Scaffold(
              appBar: AppBar(
                title: Text(
                  "Pengaturan Pembayaran",
                  style: TextStyle(color: Colors.black),
                ),
                centerTitle: true,
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.black),
              ),
              body: BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
                builder: (context, state) {
                  return SingleChildScrollView(
                    padding: EdgeInsets.all(Sizes.s20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: Sizes.s10),
                          child: Image.asset(
                            "assets/dana/dana_vertical.png",
                            height: 25,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        ListTile(
                          contentPadding: EdgeInsets.zero,
                          title: Text(
                            "Saldo",
                            style: TextStyle(
                              fontSize: FontSize.s12,
                              color: Color(0xFF858585),
                            ),
                          ),
                          subtitle: Text(
                            CurrencyFormatter.format(
                                state is DanaBalanceLoaded ? state.balance : 0),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SantaraMainButton(
                          title: "Putuskan Akun",
                          textColor: Color(0xFF858585),
                          color: Color(0xFFF5F5F5),
                          onPressed: () {
                            DanaBottomSheet.confirmDeactivationDana(context,
                                () {
                              Navigator.pop(context);
                              _inputPin(context, () {
                                _authDanaBloc.add(LogoutDana());
                              });
                            });
                          },
                        ),
                      ],
                    ),
                  );
                },
              )),
        );
      },
    );
  }
}
