import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/domain/usecases/user_usecase.dart';
import 'package:santaraapp/core/utils/constants/app_constants.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../domain/usecases/dana_usecase.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
part 'dana_balance_event.dart';
part 'dana_balance_state.dart';

class DanaBalanceBloc extends Bloc<DanaBalanceEvent, DanaBalanceState> {
  final GetDanaBalance _getDanaBalance;
  final GetUserByUuid _getUserByUuid;

  DanaBalanceBloc({
    @required GetUserByUuid getUserByUuid,
    @required GetDanaBalance getDanaBalance,
  })  : assert(getUserByUuid != null, getDanaBalance != null),
        _getDanaBalance = getDanaBalance,
        _getUserByUuid = getUserByUuid,
        super(DanaBalanceInitial());

  @override
  Stream<DanaBalanceState> mapEventToState(
    DanaBalanceEvent event,
  ) async* {
    if (event is LoadDanaBalance) {
      try {
        yield DanaBalanceLoading();

        final storage = secureStorage.FlutterSecureStorage();
        final userUuid = await storage.read(key: USER_UUID);
        final result = await _getDanaBalance(NoParams());
        final userData = await _getUserByUuid(userUuid);

        yield* userData.fold((failure) async* {
          yield DanaBalanceError();
        }, (user) async* {
          if (user.isDana == 1) {
            yield* result.fold((failure) async* {
              yield DanaBalanceError();
            }, (dana) async* {
              if (dana != null) {
                num balance;
                String ott;
                String topupUrl;
                String danaKyc;
                for (var item in dana) {
                  switch (item.resourceType) {
                    case "OTT":
                      ott = item.value;
                      break;
                    case "DANA_BALANCE":
                      balance = item.value.amount;
                      break;
                    case "TOPUP_URL":
                      topupUrl = item.value;
                      break;
                    case "DANA_KYC":
                      danaKyc = item.value;
                      break;
                    default:
                  }
                }
                yield DanaBalanceLoaded(
                  balance: balance,
                  ott: ott,
                  topupUrl: topupUrl,
                  danaKyc: danaKyc,
                );
              } else {
                yield DanaBalanceEmpty();
              }
            });
          } else {
            yield DanaBalanceLoaded(
              balance: null,
              ott: null,
              topupUrl: null,
              danaKyc: null,
            );
          }
        });
      } catch (e, stack) {
        santaraLog(e, stack);
        yield DanaBalanceError();
      }
    }
  }
}
