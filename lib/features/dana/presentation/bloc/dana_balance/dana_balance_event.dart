part of 'dana_balance_bloc.dart';

abstract class DanaBalanceEvent extends Equatable {
  const DanaBalanceEvent();

  @override
  List<Object> get props => [];
}

class LoadDanaBalance extends DanaBalanceEvent {}
