part of 'dana_balance_bloc.dart';

abstract class DanaBalanceState extends Equatable {
  const DanaBalanceState();

  @override
  List<Object> get props => [];
}

class DanaBalanceInitial extends DanaBalanceState {}

class DanaBalanceLoading extends DanaBalanceState {}

class DanaBalanceLoaded extends DanaBalanceState {
  final num balance;
  final String topupUrl;
  final String ott;
  final String danaKyc;

  DanaBalanceLoaded({this.balance, this.topupUrl, this.ott, this.danaKyc});
}

class DanaBalanceEmpty extends DanaBalanceState {}

class DanaBalanceError extends DanaBalanceState {}
