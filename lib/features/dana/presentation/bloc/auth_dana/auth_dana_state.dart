part of 'auth_dana_bloc.dart';

abstract class AuthDanaState extends Equatable {
  const AuthDanaState();

  @override
  List<Object> get props => [];
}

class AuthDanaInitial extends AuthDanaState {}

class AuthDanaLoading extends AuthDanaState {}

class LoginDanaSuccess extends AuthDanaState {
  final String url;

  LoginDanaSuccess({this.url});
}

class LogoutDanaSuccess extends AuthDanaState {}

class ApplyTokenSuccess extends AuthDanaState {}

class AuthDanaFailed extends AuthDanaState {
  final String message;

  AuthDanaFailed({this.message});
}
