import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../domain/usecases/dana_usecase.dart';

part 'auth_dana_event.dart';
part 'auth_dana_state.dart';

class AuthDanaBloc extends Bloc<AuthDanaEvent, AuthDanaState> {
  final GetUrlLoginDana _getUrlLoginDana;
  final SubmitLogoutDana _submitLogoutDana;
  final ApplyTokenDana _applyTokenDana;

  AuthDanaBloc(
      {@required GetUrlLoginDana getUrlLoginDana,
      @required SubmitLogoutDana submitLogoutDana,
      @required ApplyTokenDana applyTokenDana})
      : assert(getUrlLoginDana != null),
        assert(submitLogoutDana != null),
        assert(applyTokenDana != null),
        _getUrlLoginDana = getUrlLoginDana,
        _submitLogoutDana = submitLogoutDana,
        _applyTokenDana = applyTokenDana,
        super(AuthDanaInitial());

  @override
  Stream<AuthDanaState> mapEventToState(
    AuthDanaEvent event,
  ) async* {
    if (event is LoginDana) {
      yield* _mapLoginDanaToState(event);
    }
    if (event is LogoutDana) {
      yield* _mapLogoutDanaToState(event);
    }
    if (event is ApplyToken) {
      yield* _mapApplyTokenToState(event);
    }
  }

  Stream<AuthDanaState> _mapLoginDanaToState(LoginDana event) async* {
    try {
      yield AuthDanaInitial();
      yield AuthDanaLoading();
      final result = await _getUrlLoginDana(NoParams());

      yield* result.fold(
        (failure) async* {
          yield AuthDanaFailed();
        },
        (data) async* {
          if (data != null) {
            yield LoginDanaSuccess(url: data);
            yield AuthDanaInitial();
          } else {
            yield AuthDanaFailed();
            yield AuthDanaInitial();
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield AuthDanaFailed(message: e);
      yield AuthDanaInitial();
    }
  }

  Stream<AuthDanaState> _mapLogoutDanaToState(LogoutDana event) async* {
    try {
      yield AuthDanaInitial();
      yield AuthDanaLoading();
      final result = await _submitLogoutDana(NoParams());

      yield* result.fold(
        (failure) async* {
          yield AuthDanaFailed();
        },
        (data) async* {
          if (data != null) {
            yield LogoutDanaSuccess();
            yield AuthDanaInitial();
          } else {
            yield AuthDanaFailed();
            yield AuthDanaInitial();
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield AuthDanaFailed(message: e);
      yield AuthDanaInitial();
    }
  }

  Stream<AuthDanaState> _mapApplyTokenToState(ApplyToken event) async* {
    try {
      yield AuthDanaInitial();
      yield AuthDanaLoading();
      final result = await _applyTokenDana(event.authCode);

      yield* result.fold(
        (failure) async* {
          yield AuthDanaFailed();
        },
        (data) async* {
          if (data != null) {
            yield ApplyTokenSuccess();
            yield AuthDanaInitial();
          } else {
            yield AuthDanaFailed();
            yield AuthDanaInitial();
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield AuthDanaFailed(message: e);
      yield AuthDanaInitial();
    }
  }
}
