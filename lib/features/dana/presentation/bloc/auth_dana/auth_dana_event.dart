part of 'auth_dana_bloc.dart';

abstract class AuthDanaEvent extends Equatable {
  const AuthDanaEvent();

  @override
  List<Object> get props => [];
}

class LoginDana extends AuthDanaEvent {}

class LogoutDana extends AuthDanaEvent {}

class ApplyToken extends AuthDanaEvent {
  final String authCode;

  ApplyToken({this.authCode});
}
