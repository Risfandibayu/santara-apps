import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../../../core/usecases/unauthorize_usecase.dart';
import '../../../../widget/home/blocs/button.bloc.dart';
import '../../../../widget/widget/components/kyc/KycNotificationStatus.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../disbursement/presentation/blocs/bank_withdraw/bank_withdraw_bloc.dart';
import '../blocs/withdraw_history/withdraw_history_bloc.dart';
import 'withdraw_history_page.dart';
import 'withdraw_page.dart';

class MainWithdrawPage extends StatefulWidget {
  final int initialTab;

  const MainWithdrawPage({Key key, this.initialTab}) : super(key: key);
  @override
  _MainWithdrawPageState createState() => _MainWithdrawPageState();
}

class _MainWithdrawPageState extends State<MainWithdrawPage> {
  ButtonBloc buttonBloc;
  WithdrawHistoryBloc withdrawHistoryBloc;
  SaldoDepositBloc saldoDepositBloc;
  DanaBalanceBloc danaBalanceBloc;
  BankWithdrawBloc bankWithdrawBloc;

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    buttonBloc = BlocProvider.of<ButtonBloc>(context);
    buttonBloc.add(LoadButton());
    withdrawHistoryBloc = BlocProvider.of<WithdrawHistoryBloc>(context);
    withdrawHistoryBloc.add(LoadWithdrawHistory());
    saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    saldoDepositBloc.add(LoadSaldoDeposit(0));
    danaBalanceBloc = BlocProvider.of<DanaBalanceBloc>(context);
    danaBalanceBloc.add(LoadDanaBalance());
    bankWithdrawBloc = BlocProvider.of<BankWithdrawBloc>(context);
    bankWithdrawBloc.add(LoadBankList());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.initialTab ?? 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Penarikan Saldo",
            style: TextStyle(color: Colors.white),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(ColorRev.mainBlack),
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Color((0xFFBF2D30)),
            labelColor: Color((0xFFBF2D30)),
            unselectedLabelColor: Colors.white,
            tabs: [
              Tab(text: "Penarikan Saldo"),
              Tab(text: "Riwayat Withdraw"),
            ],
          ),
        ),
        body: Stack(children: [
          TabBarView(
            children: [
              WithdrawPage(),
              WithdrawHistoryPage(),
            ],
          ),
          KycNotificationStatus(
            showCloseButton: false,
          )
        ]),
      ),
    );
  }
}
