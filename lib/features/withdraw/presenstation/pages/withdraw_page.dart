import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/services/auth/auth_service.dart';

import '../../../../core/data/models/fee_model.dart';
import '../../../../core/data/models/user_model.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../pages/Home.dart';
import '../../../../utils/api.dart';
import '../../../../widget/home/blocs/button.bloc.dart';
import '../../../../widget/widget/components/main/SantaraBottomSheet.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../../../../widget/widget/components/main/SantaraField.dart';
import '../../../../widget/widget/components/market/toast_helper.dart';
import '../../../dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../disbursement/presentation/blocs/disbursement/disbursement_bloc.dart';
import '../../../disbursement/presentation/widgets/disbursement_bottom_sheet.dart';
import '../widgets/balance_information.dart';
import '../widgets/withdraw_tnc.dart';
import 'main_withdraw_page.dart';

class WithdrawPage extends StatefulWidget {
  @override
  _WithdrawPageState createState() => _WithdrawPageState();
}

class _WithdrawPageState extends State<WithdrawPage> {
  final _formKey = GlobalKey<FormState>();
  DisbursementBloc disbursementBloc;
  AuthDanaBloc _authDanaBloc;
  final _authService = AuthService();
  bool isDanaActive = true;
  TextEditingController _withdrawAmount = MoneyMaskedTextController(
      decimalSeparator: '', thousandSeparator: ',', precision: 0);
  TextEditingController _recieveAmount = MoneyMaskedTextController(
      decimalSeparator: '', thousandSeparator: ',', precision: 0);

  String accountName = '';
  String accountNumber = '';
  String bankName = '';

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    setState(() => isDanaActive = remoteConfig.getBool(isFeatureDanaActive));
  }

  @override
  void initState() {
    super.initState();
    getDataBank();
    getRemoteConfig();
    disbursementBloc = BlocProvider.of<DisbursementBloc>(context);
    _authDanaBloc = BlocProvider.of<AuthDanaBloc>(context);
  }

  getDataBank() async {
    try {
      await _authService.getBank().then((value) {
        var data = json.decode(value.body);
        print(data);
        if (data != null && value.statusCode == 200) {
          setState(() {
            accountName = data['data'][0]['account_name1'];
            accountNumber = data['data'][0]['account_number1'];
            bankName = data['data'][0]['bank'];
          });
        }
      });
    } catch (err) {
      ToastHelper.showFailureToast(context, err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
      builder: (context, stateSaldo) {
        num idr = 0;
        FeeModel fee;
        User user;
        if (stateSaldo is SaldoDepositLoaded) {
          user = stateSaldo.user;
          idr = stateSaldo.saldoDeposit.idr;
          fee = stateSaldo.fee;
        }
        return BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
          builder: (context, stateDana) {
            num dana = 0;
            String danaKyc;
            if (stateDana is DanaBalanceLoaded) {
              dana = stateDana.balance;
              danaKyc = stateDana.danaKyc;
            }
            return BlocConsumer<DisbursementBloc, DisbursementState>(
              listener: (context, state) {
                if (state is DisbursementSuccess) {
                  DisbursementBottomSheet.disbursementSuccess(
                      context: context,
                      title: "Pengajuan Penarikan Saldo Berhasil",
                      subtitle: "Penarikan saldo Anda sedang kami proses",
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (route) => false);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => MainWithdrawPage(initialTab: 1)),
                        );
                      });
                }
                if (state is DisbursementFailed) {
                  ToastHelper.showFailureToast(context, state.message);
                }
              },
              builder: (context, state) {
                return ModalProgressHUD(
                  inAsyncCall: state is DisbursementLoading,
                  color: Colors.grey,
                  opacity: 0.5,
                  progressIndicator: CupertinoActivityIndicator(),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      padding: EdgeInsets.all(Sizes.s20),
                      children: [
                        BalanceInformation(),
                        SizedBox(height: Sizes.s15),
                        SantaraTextField(
                          hintText: "Jumlah Penarikan",
                          labelText: "Jumlah Penarikan",
                          controller: _withdrawAmount,
                          prefixText: 'Rp ',
                          inputType: TextInputType.number,
                          enabled: stateSaldo is SaldoDepositLoaded,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Jumlah deposit tidak boleh kosong';
                            } else if (num.parse(value.replaceAll(",", "")) <
                                100000) {
                              return 'Jumlah penarikan kurang dari ketentuan min. penarikan';
                            } else if (num.parse(value.replaceAll(",", "")) >
                                200000000) {
                              return 'Jumlah penarikan melebihi batas maks. penarikan harian';
                            } else if (num.parse(value.replaceAll(",", "")) >
                                idr) {
                              return 'Dana tersedia tidak cukup untuk melakukan penarikan ini';
                            } else {
                              return null;
                            }
                          },
                          onChanged: (value) {
                            if (num.parse(value.replaceAll(",", "")) >=
                                100000) {
                              num willRecieve =
                                  num.parse(value.replaceAll(",", "")) - 15000;
                              _recieveAmount.text = willRecieve.toString();
                            }
                          },
                        ),
                        SizedBox(height: Sizes.s15),
                        WithdrawTnc(),
                        SizedBox(height: Sizes.s15),
                        SantaraTextField(
                          hintText: "Terima Bersih",
                          labelText: "Terima Bersih",
                          controller: _recieveAmount,
                          prefixText: 'Rp ',
                          inputType: TextInputType.number,
                          enabled: false,
                        ),
                        SizedBox(height: Sizes.s15),
                        BlocBuilder<ButtonBloc, ButtonState>(
                          builder: (context, stateButton) {
                            return SantaraMainButton(
                              title: "Pilih Metode Pencairan",
                              disableColor: Color(0xFFF5F5F5),
                              onPressed: () {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                if (_formKey.currentState.validate()) {
                                  if (stateButton is ButtonLoaded) {
                                    stateButton.verified ==
                                            ButtonStatus.verified
                                        ? DisbursementBottomSheet.chooseMethod(
                                            context: context,
                                            type: DisbursementType.withdraw,
                                            saldoDompet: idr,
                                            saldoDana: dana,
                                            total: num.parse(_withdrawAmount
                                                .text
                                                .replaceAll(",", "")),
                                            user: user,
                                            fee: fee,
                                            danaKyc: danaKyc,
                                            isDanaActive: isDanaActive,
                                            bloc: disbursementBloc,
                                            authDanaBloc: _authDanaBloc,
                                            accountName: accountName,
                                            accountNumber: accountNumber,
                                            bankName: bankName)
                                        : SantaraBottomSheet.show(
                                            context,
                                            "Maaf, akun anda belum terverifikasi.",
                                          );
                                  }
                                }
                              },
                            );
                          },
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
