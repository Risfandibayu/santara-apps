import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../core/widgets/shimmer/box_shimmer.dart';
import '../blocs/withdraw_history/withdraw_history_bloc.dart';
import '../widgets/withdraw_history_card.dart';
import '../widgets/withdraw_empty_widget.dart';

class WithdrawHistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WithdrawHistoryBloc, WithdrawHistoryState>(
      builder: (context, state) {
        if (state is WithdrawHistoryLoaded) {
          if (state.withdrawHistory.length > 0) {
            return ListView.builder(
              padding: EdgeInsets.all(Sizes.s10),
              itemCount: state.withdrawHistory.length,
              itemBuilder: (_, i) {
                return WithdrawHistoryCard(withdraw: state.withdrawHistory[i]);
              },
            );
          } else {
            return WithdrawEmptyWidget(
                message: "Anda Belum Memiliki Riwayat Penarikan Saldo");
          }
        } else if (state is WithdrawHistoryInitial ||
            state is WithdrawHistoryLoading) {
          return ListView.separated(
              itemCount: 3,
              padding: EdgeInsets.all(Sizes.s20),
              separatorBuilder: (_, i) {
                return Container(height: Sizes.s10);
              },
              itemBuilder: (_, i) {
                return BoxShimmer(
                    width: double.infinity, height: 100, borderRadius: 4);
              });
        } else if (state is WithdrawHistoryError) {
          return WithdrawEmptyWidget(message: state.message);
        } else {
          return WithdrawEmptyWidget(
              message: "Anda Belum Memiliki Riwayat Penarikan Saldo");
        }
      },
    );
  }
}
