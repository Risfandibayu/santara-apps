import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import 'balance_information_item.dart';

class BalanceInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
      builder: (context, state) {
        return Container(
            constraints: BoxConstraints(minHeight: 58),
            padding: EdgeInsets.symmetric(
              horizontal: Sizes.s20,
              vertical: Sizes.s16,
            ),
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Color(0xFFB8B8B8)),
              borderRadius: BorderRadius.circular(6),
            ),
            child: Row(
              children: [
                Expanded(
                  child: BalanceInformationItem(
                    name: "Saldo Tersedia",
                    info: "Saldo tersedia adalah dana yang bisa kamu tarik.",
                    value: state is SaldoDepositLoaded
                        ? state.saldoDeposit.idr
                        : 0,
                    isLoading: state is SaldoDepositInitial ||
                        state is SaldoDepositLoading,
                    isError: state is SaldoDepositEmpty ||
                        state is SaldoDepositError,
                    valueColor: Color(0xFF0E7E4A),
                  ),
                ),
                Expanded(
                  child: BalanceInformationItem(
                    name: "Saldo Tertahan",
                    info:
                        "Saldo tertahan adalah jumlah dana yang merupakan hasil penjualan Anda dipasar sekunder. Dana dapat ditarik 2 hari setelah transaksi penjualan di pasar sekunder dilakukan.",
                    value: state is SaldoDepositLoaded
                        ? state.saldoDeposit.pending
                        : 0,
                    isLoading: state is SaldoDepositInitial ||
                        state is SaldoDepositLoading,
                    isError: state is SaldoDepositEmpty ||
                        state is SaldoDepositError,
                  ),
                ),
              ],
            ));
      },
    );
  }
}
