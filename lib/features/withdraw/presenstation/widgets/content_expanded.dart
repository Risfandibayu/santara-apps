import 'package:flutter/material.dart';

import '../../../../core/utils/formatter/currency_formatter.dart';
import '../../data/models/withdraw_history_model.dart';

class ContentExpanded extends StatelessWidget {
  final WithdrawHistoryModel withdraw;

  const ContentExpanded({Key key, this.withdraw}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 1, color: Color(0xFFC4C4C4)),
        Container(height: 8),
        Text("Nama Bank"),
        Text(withdraw.bankTo,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        Container(height: 8),
        Text("Nomor Rekening"),
        Text(withdraw.accountNumber.toString(),
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        Container(height: 8),
        Text("Nominal yang ditarik"),
        Text(CurrencyFormatter.format(withdraw.amount),
            style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0E7E4A))),
        Text("Nominal yang diterima"),
        Text(CurrencyFormatter.format(withdraw.amount - withdraw.fee),
            style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0E7E4A))),
      ],
    );
  }
}
