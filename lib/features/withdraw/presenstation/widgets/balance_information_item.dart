import 'package:flutter/material.dart';
import 'package:santaraapp/core/utils/formatter/currency_formatter.dart';
import 'package:santaraapp/core/utils/ui/screen_sizes.dart';
import 'package:santaraapp/core/widgets/shimmer/box_shimmer.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';

class BalanceInformationItem extends StatelessWidget {
  final String name;
  final num value;
  final String info;
  final bool isLoading;
  final bool isError;
  final Color valueColor;

  const BalanceInformationItem(
      {Key key,
      this.name,
      this.value,
      this.info,
      this.isLoading,
      this.isError,
      this.valueColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              name,
              style: TextStyle(
                fontSize: FontSize.s12,
                color: Color(0xFF858585),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Sizes.s8),
              child: GestureDetector(
                  child: Icon(
                    Icons.info,
                    color: Color(0xFF858585),
                    size: Sizes.s12,
                  ),
                  onTap: () =>
                      PopupHelper.showMessageWithoutButton(context, info)),
            )
          ],
        ),
        Container(height: Sizes.s4),
        isLoading
            ? BoxShimmer(width: 60, height: 15, borderRadius: 15)
            : Text(
                isError ? "-" : CurrencyFormatter.format(value),
                style: TextStyle(
                  color: valueColor == null ? Colors.black : valueColor,
                  fontWeight: FontWeight.bold,
                ),
              )
      ],
    );
  }
}
