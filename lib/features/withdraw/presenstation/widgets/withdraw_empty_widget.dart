import 'package:flutter/material.dart';

import '../../../../core/utils/ui/screen_sizes.dart';

class WithdrawEmptyWidget extends StatelessWidget {
  final String message;

  const WithdrawEmptyWidget({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding:
            EdgeInsets.fromLTRB(Sizes.s15, Sizes.s10, Sizes.s15, Sizes.s15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/icon/empty_dividen.png"),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Text(
                message,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ));
  }
}
