import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/list_item_widget.dart/santara_bullet_list.dart';

class WithdrawTnc extends StatelessWidget {
  final List<String> tnc = [
    "Setiap transaksi akan dikenai biaya admin sebesar Rp 15.000 (lima belas ribu Rupiah)",
    "Minimal penarikan adalah Rp 100.000 (Seratus Ribu Rupiah)",
    "Maksimal penarikan per hari adalah Rp 200.000.000 / Hari (Dua Ratus Juta Rupiah).",
    "Lama waktu pencairan maksimal 3x24 jam hari kerja.",
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: tnc.length,
        itemBuilder: (_, i) {
          return SantaraBulletList(
              child: Text(
            tnc[i],
            style: TextStyle(color: Color(0xFF747474), fontSize: 14),
          ));
        });
  }
}
