import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/formatter/date_formatter.dart';
import '../../data/models/withdraw_history_model.dart';
import 'content_expanded.dart';

class WithdrawHistoryCard extends StatelessWidget {
  final WithdrawHistoryModel withdraw;

  const WithdrawHistoryCard({Key key, this.withdraw}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Color(0xFFB8B8B8),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                color: Color(0xFFF0F0F0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                )),
            height: 40,
            width: double.maxFinite,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  DateFormatter.ddMMMyyyy(withdraw.createdAt),
                  style: TextStyle(fontSize: 11),
                ),
                withdraw.isVerified == 1
                    ? Text("Berhasil",
                        style: TextStyle(
                            color: Color(0xFF0E7E4A),
                            fontWeight: FontWeight.bold))
                    : withdraw.isVerified == 2
                        ? Text("Gagal",
                            style: TextStyle(
                                color: Color(0xFFBF2D30),
                                fontWeight: FontWeight.bold))
                        : Text("Menunggu Verifikasi",
                            style: TextStyle(
                                color: Color(0xFFE5A037),
                                fontWeight: FontWeight.bold)),
              ],
            ),
          ),
          Container(
            child: ExpandableNotifier(
                child: ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                header: Padding(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.only(right: 12),
                              child: Image.asset(
                                  "assets/icon_journey/withdraw.png")),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Withdraw",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                Text(
                                  withdraw.accountName.toString(),
                                  style: TextStyle(fontSize: 15),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: ContentExpanded(withdraw: withdraw),
                    ),
                    Container(
                      height: 1,
                      color: Colors.grey[200],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            )),
          )
        ],
      ),
    );
  }
}
