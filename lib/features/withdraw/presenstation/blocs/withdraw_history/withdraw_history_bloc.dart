import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../data/models/withdraw_history_model.dart';
import '../../../domain/usecases/withdraw_history_usecase.dart';

part 'withdraw_history_event.dart';
part 'withdraw_history_state.dart';

class WithdrawHistoryBloc
    extends Bloc<WithdrawHistoryEvent, WithdrawHistoryState> {
  final GetWithdrawHistory _getWithdrawHistory;

  WithdrawHistoryBloc({@required GetWithdrawHistory getWithdrawHistory})
      : assert(getWithdrawHistory != null),
        _getWithdrawHistory = getWithdrawHistory,
        super(WithdrawHistoryInitial());

  @override
  Stream<WithdrawHistoryState> mapEventToState(
    WithdrawHistoryEvent event,
  ) async* {
    if (event is LoadWithdrawHistory) {
      try {
        yield WithdrawHistoryLoading();
        final result = await _getWithdrawHistory(NoParams());

        yield* result.fold(
          (failure) async* {
            yield WithdrawHistoryError(message: failure.message);
          },
          (withdrawHistory) async* {
            if (withdrawHistory != null) {
              yield WithdrawHistoryLoaded(withdrawHistory: withdrawHistory);
            } else {
              yield WithdrawHistoryEmpty();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield WithdrawHistoryError(message: e.toString());
      }
    }
  }
}
