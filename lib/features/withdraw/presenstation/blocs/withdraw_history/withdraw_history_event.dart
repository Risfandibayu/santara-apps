part of 'withdraw_history_bloc.dart';

abstract class WithdrawHistoryEvent extends Equatable {
  const WithdrawHistoryEvent();

  @override
  List<Object> get props => [];
}

class LoadWithdrawHistory extends WithdrawHistoryEvent {}
