part of 'withdraw_history_bloc.dart';

abstract class WithdrawHistoryState extends Equatable {
  const WithdrawHistoryState();

  @override
  List<Object> get props => [];
}

class WithdrawHistoryInitial extends WithdrawHistoryState {}

class WithdrawHistoryLoading extends WithdrawHistoryState {}

class WithdrawHistoryLoaded extends WithdrawHistoryState {
  final List<WithdrawHistoryModel> withdrawHistory;

  WithdrawHistoryLoaded({this.withdrawHistory});
  @override
  List<Object> get props => [withdrawHistory];
}

class WithdrawHistoryEmpty extends WithdrawHistoryState {}

class WithdrawHistoryError extends WithdrawHistoryState {
  final String message;

  WithdrawHistoryError({this.message});
}
