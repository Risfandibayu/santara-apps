import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/withdraw_history_repository.dart';
import '../datasources/withdraw_history_remote_data_source.dart';
import '../models/withdraw_history_model.dart';

class WithdrawHistoryRepositoryImpl implements WithdrawHistoryRepository {
  final NetworkInfo networkInfo;
  final WithdrawHistoryRemoteDataSource remoteDataSource;

  WithdrawHistoryRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<WithdrawHistoryModel>>>
      getWithdrawHistory() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getWithdrawHistory();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
