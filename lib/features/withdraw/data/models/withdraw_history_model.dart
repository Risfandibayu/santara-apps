import 'dart:convert';

List<WithdrawHistoryModel> withdrawHistoryModelFromJson(String str) =>
    List<WithdrawHistoryModel>.from(
        json.decode(str).map((x) => WithdrawHistoryModel.fromJson(x)));

String withdrawHistoryModelToJson(List<WithdrawHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class WithdrawHistoryModel {
  WithdrawHistoryModel({
    this.id,
    this.uuid,
    this.createdAt,
    this.updatedAt,
    this.amount,
    this.fee,
    this.bankTo,
    this.isVerified,
    this.traderId,
    this.verifiedBy,
    this.withdrawAt,
    this.isDeleted,
    this.createdBy,
    this.updatedBy,
    this.accountNumber,
    this.accountName,
  });

  int id;
  String uuid;
  DateTime createdAt;
  DateTime updatedAt;
  int amount;
  int fee;
  String bankTo;
  int isVerified;
  int traderId;
  dynamic verifiedBy;
  dynamic withdrawAt;
  int isDeleted;
  dynamic createdBy;
  dynamic updatedBy;
  String accountNumber;
  String accountName;

  factory WithdrawHistoryModel.fromJson(Map<String, dynamic> json) =>
      WithdrawHistoryModel(
        id: json["id"],
        uuid: json["uuid"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        amount: json["amount"],
        fee: json["fee"],
        bankTo: json["bank_to"],
        isVerified: json["is_verified"],
        traderId: json["trader_id"],
        verifiedBy: json["verified_by"],
        withdrawAt: json["withdraw_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        accountNumber: json["account_number"],
        accountName: json["account_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "amount": amount,
        "fee": fee,
        "bank_to": bankTo,
        "is_verified": isVerified,
        "trader_id": traderId,
        "verified_by": verifiedBy,
        "withdraw_at": withdrawAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "account_number": accountNumber,
        "account_name": accountName,
      };
}
