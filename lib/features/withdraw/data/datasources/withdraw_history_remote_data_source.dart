import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/withdraw_history_model.dart';

abstract class WithdrawHistoryRemoteDataSource {
  Future<List<WithdrawHistoryModel>> getWithdrawHistory();
}

class WithdrawHistoryRemoteDataSourceImpl
    implements WithdrawHistoryRemoteDataSource {
  final RestClient client;
  WithdrawHistoryRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<WithdrawHistoryModel>> getWithdrawHistory() async {
    try {
      final result = await client.get(path: "/withdraw/");
      if (result.statusCode == 200) {
        return withdrawHistoryModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
