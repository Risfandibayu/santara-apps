import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/withdraw_history_model.dart';
import '../repositories/withdraw_history_repository.dart';

class GetWithdrawHistory
    implements UseCase<List<WithdrawHistoryModel>, NoParams> {
  final WithdrawHistoryRepository repository;
  GetWithdrawHistory(this.repository);

  @override
  Future<Either<Failure, List<WithdrawHistoryModel>>> call(
      NoParams params) async {
    return await repository.getWithdrawHistory();
  }
}
