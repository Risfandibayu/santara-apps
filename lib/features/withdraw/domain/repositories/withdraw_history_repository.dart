import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/withdraw_history_model.dart';

abstract class WithdrawHistoryRepository {
  Future<Either<Failure, List<WithdrawHistoryModel>>> getWithdrawHistory();
}
