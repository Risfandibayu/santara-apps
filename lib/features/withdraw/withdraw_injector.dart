import '../../injector_container.dart';
import 'data/datasources/withdraw_history_remote_data_source.dart';
import 'data/repositories/withdraw_history_repository_impl.dart';
import 'domain/repositories/withdraw_history_repository.dart';
import 'domain/usecases/withdraw_history_usecase.dart';
import 'presenstation/blocs/withdraw_history/withdraw_history_bloc.dart';

void initWithdrawInjector() {
  // Bloc
  sl.registerFactory(() => WithdrawHistoryBloc(getWithdrawHistory: sl()));

  //! Global Usecase
  sl.registerLazySingleton(() => GetWithdrawHistory(sl()));

  //! Global Repository

  sl.registerLazySingleton<WithdrawHistoryRepository>(
    () => WithdrawHistoryRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<WithdrawHistoryRemoteDataSource>(
    () => WithdrawHistoryRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
