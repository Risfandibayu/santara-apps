import '../../injector_container.dart';
import 'data/datasources/transaction_data_source.dart';
import 'data/repositories/transaction_repository_impl.dart';
import 'domain/repositories/transaction_repository.dart';
import 'domain/usecases/transaction_usecase.dart';
import 'presentation/bloc/checkout_list/checkout_list_bloc.dart';

void initTransactionInjector() {
  // Bloc
  sl.registerFactory(() => CheckoutListBloc(getCheckoutTransaction: sl()));

  //! Global Usecase
  sl.registerLazySingleton(() => GetCheckoutTransaction(sl()));

  //! Global Repository
  sl.registerLazySingleton<TransactionRepository>(
    () => TransactionRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<TransactionRemoteDataSource>(
    () => TransactionRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
