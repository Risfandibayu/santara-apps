import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/checkout_list_model.dart';
import '../repositories/transaction_repository.dart';

class GetCheckoutTransaction
    implements UseCase<List<CheckoutListModel>, NoParams> {
  final TransactionRepository repository;
  GetCheckoutTransaction(this.repository);

  @override
  Future<Either<Failure, List<CheckoutListModel>>> call(NoParams params) async {
    return await repository.getCheckoutTransaction();
  }
}
