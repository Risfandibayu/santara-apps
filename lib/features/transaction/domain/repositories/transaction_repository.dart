import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/checkout_list_model.dart';

abstract class TransactionRepository {
  Future<Either<Failure, List<CheckoutListModel>>> getCheckoutTransaction();
}
