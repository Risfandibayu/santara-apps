import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../data/models/checkout_list_model.dart';
import '../../../domain/usecases/transaction_usecase.dart';

part 'checkout_list_event.dart';
part 'checkout_list_state.dart';

class CheckoutListBloc extends Bloc<CheckoutListEvent, CheckoutListState> {
  final GetCheckoutTransaction _getCheckoutTransaction;

  CheckoutListBloc({@required GetCheckoutTransaction getCheckoutTransaction})
      : assert(getCheckoutTransaction != null),
        _getCheckoutTransaction = getCheckoutTransaction,
        super(CheckoutListInitial());

  @override
  Stream<CheckoutListState> mapEventToState(
    CheckoutListEvent event,
  ) async* {
    if (event is LoadCheckoutList) {
      try {
        yield CheckoutListLoading();
        final result = await _getCheckoutTransaction(NoParams());

        yield* result.fold(
          (failure) async* {
            yield CheckoutListError();
          },
          (checkoutList) async* {
            if (checkoutList != null) {
              yield CheckoutListLoaded(checkoutList: checkoutList);
            } else {
              yield CheckoutListEmpty();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield CheckoutListError();
      }
    }
  }
}
