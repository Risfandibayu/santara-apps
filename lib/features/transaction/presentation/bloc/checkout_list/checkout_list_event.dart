part of 'checkout_list_bloc.dart';

abstract class CheckoutListEvent extends Equatable {
  const CheckoutListEvent();

  @override
  List<Object> get props => [];
}

class LoadCheckoutList extends CheckoutListEvent {}
