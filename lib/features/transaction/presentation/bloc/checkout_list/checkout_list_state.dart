part of 'checkout_list_bloc.dart';

abstract class CheckoutListState extends Equatable {
  const CheckoutListState();

  @override
  List<Object> get props => [];
}

class CheckoutListInitial extends CheckoutListState {}

class CheckoutListLoading extends CheckoutListState {}

class CheckoutListLoaded extends CheckoutListState {
  final List<CheckoutListModel> checkoutList;

  CheckoutListLoaded({this.checkoutList});
}

class CheckoutListEmpty extends CheckoutListState {}

class CheckoutListError extends CheckoutListState {}
