import 'dart:convert';

List<CheckoutListModel> checkoutListModelFromJson(String str) =>
    List<CheckoutListModel>.from(
        json.decode(str).map((x) => CheckoutListModel.fromJson(x)));

String checkoutListModelToJson(List<CheckoutListModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CheckoutListModel {
  CheckoutListModel({
    this.id,
    this.uuid,
    this.type,
    this.traderId,
    this.channel,
    this.amount,
    this.fee,
    this.emitenId,
    this.sukukProjectId,
    this.companyName,
    this.trademark,
    this.category,
    this.code,
    this.price,
    this.bank,
    this.bankTo,
    this.merchantCode,
    this.accountNumber,
    this.name,
    this.createdAt,
    this.expiredDate,
    this.pictures,
    this.lastStatus,
    this.danaCheckoutUrl,
  });

  int id;
  String uuid;
  String type;
  int traderId;
  String channel;
  int amount;
  int fee;
  int emitenId;
  dynamic sukukProjectId;
  String companyName;
  String trademark;
  String category;
  String code;
  int price;
  dynamic bank;
  dynamic bankTo;
  String merchantCode;
  String accountNumber;
  String name;
  DateTime createdAt;
  DateTime expiredDate;
  List<Picture> pictures;
  String lastStatus;
  String danaCheckoutUrl;

  factory CheckoutListModel.fromJson(Map<String, dynamic> json) =>
      CheckoutListModel(
        id: json["id"],
        uuid: json["uuid"],
        type: json["type"],
        traderId: json["trader_id"],
        channel: json["channel"],
        amount: json["amount"],
        fee: json["fee"],
        emitenId: json["emiten_id"],
        sukukProjectId: json["sukuk_project_id"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        category: json["category"],
        code: json["code"],
        price: json["price"],
        bank: json["bank"],
        bankTo: json["bank_to"],
        merchantCode: json["merchant_code"],
        accountNumber: json["account_number"],
        name: json["name"],
        createdAt: DateTime.parse(json["created_at"]),
        expiredDate: DateTime.parse(json["expired_date"]),
        pictures: List<Picture>.from(
            json["pictures"].map((x) => Picture.fromJson(x))),
        lastStatus: json["last_status"],
        danaCheckoutUrl: json["dana_checkout_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "type": type,
        "trader_id": traderId,
        "channel": channel,
        "amount": amount,
        "fee": fee,
        "emiten_id": emitenId,
        "sukuk_project_id": sukukProjectId,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        "code": code,
        "price": price,
        "bank": bank,
        "bank_to": bankTo,
        "merchant_code": merchantCode,
        "account_number": accountNumber,
        "name": name,
        "created_at": createdAt.toIso8601String(),
        "expired_date": expiredDate.toIso8601String(),
        "pictures": List<dynamic>.from(pictures.map((x) => x.toJson())),
        "last_status": lastStatus,
        "dana_checkout_url": danaCheckoutUrl,
      };
}

class Picture {
  Picture({
    this.picture,
  });

  String picture;

  factory Picture.fromJson(Map<String, dynamic> json) => Picture(
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
