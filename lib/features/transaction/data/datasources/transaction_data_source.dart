import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/checkout_list_model.dart';

abstract class TransactionRemoteDataSource {
  Future<List<CheckoutListModel>> getCheckoutTransaction();
}

class TransactionRemoteDataSourceImpl implements TransactionRemoteDataSource {
  final RestClient client;
  TransactionRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<CheckoutListModel>> getCheckoutTransaction() async {
    try {
      final result = await client.get(path: "/transactions/checkout");
      if (result.statusCode == 200) {
        return checkoutListModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
