import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/transaction_repository.dart';
import '../datasources/transaction_data_source.dart';
import '../models/checkout_list_model.dart';

class TransactionRepositoryImpl implements TransactionRepository {
  final NetworkInfo networkInfo;
  final TransactionRemoteDataSource remoteDataSource;

  TransactionRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<CheckoutListModel>>>
      getCheckoutTransaction() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getCheckoutTransaction();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
