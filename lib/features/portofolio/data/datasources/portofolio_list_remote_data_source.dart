import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/portofolio_list_model.dart';

abstract class PortofolioListRemoteDataSource {
  Future<PortofolioListModel> getPortofolioList();
}

class PortofolioListRemoteDataSourceImpl
    implements PortofolioListRemoteDataSource {
  final RestClient client;
  PortofolioListRemoteDataSourceImpl({@required this.client});

  @override
  Future<PortofolioListModel> getPortofolioList() async {
    try {
      final result = await client.get(path: "/portofolio/?category=");
      if (result.statusCode == 200) {
        return PortofolioListModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
