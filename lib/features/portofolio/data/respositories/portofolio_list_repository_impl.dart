import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/respositories/portofolio_list_repository.dart';
import '../datasources/portofolio_list_remote_data_source.dart';
import '../models/portofolio_list_model.dart';

class PortofolioListRepositoryImpl implements PortofolioListRepository {
  final NetworkInfo networkInfo;
  final PortofolioListRemoteDataSource remoteDataSource;

  PortofolioListRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, PortofolioListModel>> getPortofolioList() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPortofolioList();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
