import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/features/portofolio/data/models/portofolio_list_model.dart';
import 'package:santaraapp/features/portofolio/domain/respositories/portofolio_list_repository.dart';

class GetPortofolioList implements UseCase<PortofolioListModel, NoParams> {
  final PortofolioListRepository repository;
  GetPortofolioList(this.repository);

  @override
  Future<Either<Failure, PortofolioListModel>> call(NoParams params) async {
    return await repository.getPortofolioList();
  }
}
