import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/portofolio_list_model.dart';

abstract class PortofolioListRepository {
  Future<Either<Failure, PortofolioListModel>> getPortofolioList();
}
