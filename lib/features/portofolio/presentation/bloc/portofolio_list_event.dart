part of 'portofolio_list_bloc.dart';

abstract class PortofolioListEvent extends Equatable {
  const PortofolioListEvent();
  @override
  List<Object> get props => [];
}

class LoadPortofolioList extends PortofolioListEvent {
  final int retrial;

  LoadPortofolioList(this.retrial);
}
