part of 'portofolio_list_bloc.dart';

abstract class PortofolioListState extends Equatable {
  const PortofolioListState();
  @override
  List<Object> get props => [];
}

class PortofolioListLoading extends PortofolioListState {}

class PortofolioListLoaded extends PortofolioListState {
  final PortofolioListModel portofolio;

  PortofolioListLoaded({this.portofolio});
}

class PortofolioListError extends PortofolioListState {}

class PortofolioListEmpty extends PortofolioListState {}
