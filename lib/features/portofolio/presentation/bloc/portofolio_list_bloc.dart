import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';

import '../../data/models/portofolio_list_model.dart';
import '../../domain/usecases/portofolio_list_usecase.dart';

part 'portofolio_list_event.dart';
part 'portofolio_list_state.dart';

class PortofolioListBloc
    extends Bloc<PortofolioListEvent, PortofolioListState> {
  final GetPortofolioList getPortofoliolist;

  PortofolioListBloc({@required GetPortofolioList getPortofoliolist})
      : assert(getPortofoliolist != null),
        getPortofoliolist = getPortofoliolist,
        super(PortofolioListLoading());

  @override
  Stream<PortofolioListState> mapEventToState(
      PortofolioListEvent event) async* {
    yield PortofolioListLoading();
    if (event is LoadPortofolioList) {
      yield* _mapLoadEventToState(event);
    }
  }

  Stream<PortofolioListState> _mapLoadEventToState(
      LoadPortofolioList event) async* {
    try {
      final result = await getPortofoliolist(NoParams());

      yield* result.fold(
        (failure) async* {
          if (event.retrial < 3) {
            add(LoadPortofolioList(event.retrial + 1));
          } else {
            yield PortofolioListError();
          }
        },
        (portofolio) async* {
          if (portofolio != null) {
            yield PortofolioListLoaded(portofolio: portofolio);
          } else {
            if (event.retrial < 3) {
              add(LoadPortofolioList(event.retrial + 1));
            } else {
              yield PortofolioListEmpty();
            }
          }
        },
      );
    } catch (e, stack) {
      if (event.retrial < 3) {
        add(LoadPortofolioList(event.retrial + 1));
      } else {
        santaraLog(e, stack);
        yield PortofolioListError();
      }
    }
  }
}
