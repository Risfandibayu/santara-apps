import 'package:santaraapp/features/portofolio/presentation/bloc/portofolio_list_bloc.dart';

import '../../injector_container.dart';
import 'data/datasources/portofolio_list_remote_data_source.dart';
import 'data/respositories/portofolio_list_repository_impl.dart';
import 'domain/respositories/portofolio_list_repository.dart';
import 'domain/usecases/portofolio_list_usecase.dart';

void initPortofolioInjector() {
  // Bloc
  sl.registerFactory(() => PortofolioListBloc(getPortofoliolist: sl()));

  //! Global Usecase
  sl.registerLazySingleton(() => GetPortofolioList(sl()));

  //! Global Repository
  sl.registerLazySingleton<PortofolioListRepository>(
    () => PortofolioListRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<PortofolioListRemoteDataSource>(
    () => PortofolioListRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
