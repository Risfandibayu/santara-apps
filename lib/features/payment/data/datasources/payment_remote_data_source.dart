import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/payment_channel_model.dart';
import '../models/submit_deposit_model.dart';
import '../models/submit_transaction_model.dart';

abstract class PaymentRemoteDataSource {
  Future<PaymentChannelModel> getPaymentChannel();
  Future<bool> submitTransaction(SubmitTransactionModel body);
  Future<bool> submitDeposit(SubmitDepositModel body);
}

class PaymentRemoteDataSourceImpl implements PaymentRemoteDataSource {
  final RestClient client;
  PaymentRemoteDataSourceImpl({@required this.client});

  @override
  Future<PaymentChannelModel> getPaymentChannel() async {
    try {
      final result = await client.get(path: "/payment-channels");
      if (result.statusCode == 200) {
        return paymentChannelModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> submitTransaction(SubmitTransactionModel body) async {
    try {
      final result = await client.post(
          path: "/transactions/buy-token", body: body.toJson());
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> submitDeposit(SubmitDepositModel body) async {
    try {
      final result =
          await client.post(path: "/deposit/idr", body: body.toJson());
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
