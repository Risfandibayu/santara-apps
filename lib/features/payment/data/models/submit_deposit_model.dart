import 'dart:convert';

SubmitDepositModel submitDepositModelFromJson(String str) =>
    SubmitDepositModel.fromJson(json.decode(str));

String submitDepositModelToJson(SubmitDepositModel data) =>
    json.encode(data.toJson());

class SubmitDepositModel {
  SubmitDepositModel({
    this.amount,
    this.bankFrom,
    this.accountNumber,
    this.channel,
    this.bank,
    this.fee,
    this.pin,
    this.finger,
  });

  int amount;
  String bankFrom;
  String accountNumber;
  String channel;
  String bank;
  int fee;
  String pin;
  bool finger;

  factory SubmitDepositModel.fromJson(Map<String, dynamic> json) =>
      SubmitDepositModel(
        amount: json["amount"],
        bankFrom: json["bank_from"],
        accountNumber: json["account_number"],
        channel: json["channel"],
        bank: json["bank"],
        fee: json["fee"],
        pin: json["pin"],
        finger: json["finger"],
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "bank_from": bankFrom,
        "account_number": accountNumber,
        "channel": channel,
        "bank": bank,
        "fee": fee,
        "pin": pin,
        "finger": finger,
      };
}
