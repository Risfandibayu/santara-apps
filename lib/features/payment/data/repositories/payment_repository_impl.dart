import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/payment_repository.dart';
import '../datasources/payment_remote_data_source.dart';
import '../models/payment_channel_model.dart';
import '../models/submit_deposit_model.dart';
import '../models/submit_transaction_model.dart';

class PaymentRepositoryImpl implements PaymentRepository {
  final NetworkInfo networkInfo;
  final PaymentRemoteDataSource remoteDataSource;

  PaymentRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, PaymentChannelModel>> getPaymentChannel() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getPaymentChannel();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> submitDeposit(SubmitDepositModel body) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.submitDeposit(body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> submitTransaction(
      SubmitTransactionModel body) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.submitTransaction(body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
