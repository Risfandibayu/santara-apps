import 'package:santaraapp/features/payment/presentation/bloc/payment_transaction/payment_transaction_bloc.dart';

import '../../injector_container.dart';
import 'data/datasources/payment_remote_data_source.dart';
import 'data/repositories/payment_repository_impl.dart';
import 'domain/repositories/payment_repository.dart';
import 'domain/usecases/payment_usecase.dart';
import 'presentation/bloc/payment_channel/payment_channel_bloc.dart';

void initPaymentInjector() {
  // Bloc
  sl.registerFactory(() => PaymentChannelBloc(getPaymentChannel: sl()));
  sl.registerFactory(() =>
      PaymentTransactionBloc(submitTransaction: sl(), submitDeposit: sl()));

  //! Global Usecase
  sl.registerLazySingleton(() => GetPaymentChannel(sl()));
  sl.registerLazySingleton(() => SubmitTransaction(sl()));
  sl.registerLazySingleton(() => SubmitDeposit(sl()));

  //! Global Repository
  sl.registerLazySingleton<PaymentRepository>(
    () => PaymentRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<PaymentRemoteDataSource>(
    () => PaymentRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
