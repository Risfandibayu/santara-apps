import 'package:flutter/material.dart';

class VaPayment extends StatelessWidget {
  final String icon;
  final String name;
  final Function onTap;

  VaPayment({Key key, this.icon, this.name, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset("assets/bank/" + icon,
          height: 40, width: 60, fit: BoxFit.contain),
      title: Text(name),
      trailing: Icon(Icons.chevron_right),
      onTap: onTap,
    );
  }
}
