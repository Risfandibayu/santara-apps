import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/shimmer/box_shimmer.dart';

class PaymentMethodLoading extends StatelessWidget {
  final String icon;
  final String name;

  PaymentMethodLoading({Key key, this.icon, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Image.asset(icon, height: 25, width: 25, fit: BoxFit.contain),
        title: Text(name),
        subtitle: BoxShimmer(width: 60, height: 15, borderRadius: 15));
  }
}
