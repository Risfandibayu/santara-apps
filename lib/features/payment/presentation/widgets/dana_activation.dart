import 'package:flutter/material.dart';

class DanaActivation extends StatelessWidget {
  final bool isDanaActive;
  final Function onTap;

  const DanaActivation({Key key, this.isDanaActive, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Image.asset(
          "assets/dana/dana.png",
          height: 25,
          width: 25,
          fit: BoxFit.contain,
          color: isDanaActive ? null : Colors.grey,
        ),
        title: Text("DANA"),
        trailing: Text(
          "Aktivasi",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: isDanaActive ? Color(0xFF008CEB) : Colors.grey),
        ),
        onTap: onTap);
  }
}
