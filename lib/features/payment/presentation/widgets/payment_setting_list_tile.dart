import 'package:flutter/material.dart';
import 'package:santaraapp/core/widgets/shimmer/box_shimmer.dart';

class PaymentSettingListTile extends StatelessWidget {
  final String icon;
  final String name;
  final bool isActive;
  final bool isDanaActive;
  final Function onTap;

  const PaymentSettingListTile(
      {Key key,
      this.icon,
      this.name,
      this.isActive,
      this.isDanaActive,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        icon,
        height: 25,
        fit: BoxFit.fitHeight,
        color: isDanaActive ? null : Colors.grey,
      ),
      title: Text(name),
      subtitle: isActive
          ? Text(
              "Terhubung",
              style: TextStyle(
                fontSize: 12,
                color: Color(0xFFB8B8B8),
              ),
            )
          : null,
      trailing: isActive
          ? Icon(Icons.chevron_right)
          : Text(
              "Aktivasi",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: isDanaActive ? Color(0xFF008CEB) : Colors.grey),
            ),
      onTap: onTap,
    );
  }
}

class PaymentSettingOnLoading extends StatelessWidget {
  final String icon;
  final String name;

  const PaymentSettingOnLoading({Key key, this.icon, this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        icon,
        height: 25,
        fit: BoxFit.fitHeight,
      ),
      title: Text(name),
      subtitle: BoxShimmer(
        width: 60,
        height: 12,
        borderRadius: 12,
      ),
    );
  }
}

class PaymentSettingOnError extends StatelessWidget {
  final String icon;
  final String name;

  const PaymentSettingOnError({Key key, this.icon, this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        icon,
        height: 25,
        fit: BoxFit.fitHeight,
        color: Color(0xFFBABABA),
      ),
      title: Text(name, style: TextStyle(color: Color(0xFFBABABA))),
    );
  }
}
