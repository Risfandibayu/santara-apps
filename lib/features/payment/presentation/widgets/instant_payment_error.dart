import 'package:flutter/material.dart';

class InstantPaymentError extends StatelessWidget {
  final String icon;
  final String name;

  const InstantPaymentError({Key key, this.icon, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        icon,
        height: 25,
        width: 25,
        fit: BoxFit.contain,
        color: Colors.grey,
      ),
      title: Text(
        name,
        style: TextStyle(color: Colors.grey),
      ),
      onTap: null,
    );
  }
}
