import 'package:flutter/material.dart';

import '../../../../helpers/RupiahFormatter.dart';

class InstantPayment extends StatelessWidget {
  final String icon;
  final String name;
  final String saldo;
  final bool isActive;
  final Function onTap;

  InstantPayment(
      {Key key, this.icon, this.name, this.saldo, this.isActive, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(icon,
          height: 25,
          width: 25,
          fit: BoxFit.contain,
          color: isActive ? null : Colors.grey),
      title: Text(
        name,
        style: TextStyle(color: isActive ? Colors.black : Color(0xFF858585)),
      ),
      subtitle: Text(
        isActive
            ? RupiahFormatter.initialValueFormat(saldo)
            : RupiahFormatter.initialValueFormat(saldo) +
                " (Saldo tidak cukup)",
        style:
            TextStyle(color: isActive ? Color(0xFF434343) : Color(0xFF858585)),
      ),
      trailing: Icon(Icons.chevron_right),
      onTap: onTap,
    );
  }
}
