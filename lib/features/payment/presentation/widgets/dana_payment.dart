import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/widgets/bottom_sheet/payment_bottom_sheet.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../dana/presentation/widgets/dana_bottom_sheet.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import 'dana_activation.dart';
import 'instant_payment.dart';
import 'instant_payment_error.dart';
import 'payment_method_loading.dart';

class DanaPayment extends StatelessWidget {
  final String total;
  final bool isDanaActive;
  final Function(String phone) onTap;
  final AuthDanaBloc authDanaBloc;

  const DanaPayment(
      {Key key,
      this.total,
      @required this.isDanaActive,
      this.onTap,
      this.authDanaBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
      builder: (context, state) {
        if (state is DanaBalanceLoaded) {
          return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
            builder: (context, stateSaldo) {
              if (stateSaldo is SaldoDepositLoaded) {
                if (stateSaldo.user.isDana == 1) {
                  return InstantPayment(
                    icon: "assets/dana/dana.png",
                    name: "DANA",
                    saldo: state.balance.toString(),
                    isActive: state.balance >= num.parse(total),
                    onTap: state.balance >= num.parse(total)
                        ? () => PaymentBottomSheet.confirm(context,
                            icon: "assets/dana/dana.png",
                            title: "Saldo DANA",
                            saldo: state.balance,
                            total: num.parse(total),
                            channel: 0,
                            onTap: () => onTap(stateSaldo.user.phone))
                        : null,
                  );
                } else {
                  return DanaActivation(
                    isDanaActive: isDanaActive,
                    onTap: () {
                      if (isDanaActive) {
                        DanaBottomSheet.confirmConnectToDana(context, () {
                          Navigator.pop(context);
                          authDanaBloc.add(LoginDana());
                        });
                      } else {
                        PopupHelper.warningInfo(
                          context,
                          "Fitur DANA belum tersedia",
                        );
                      }
                    },
                  );
                }
              } else if (stateSaldo is SaldoDepositLoading ||
                  stateSaldo is SaldoDepositInitial) {
                return PaymentMethodLoading(
                  icon: "assets/dana/dana.png",
                  name: "DANA",
                );
              } else {
                return InstantPaymentError(
                  icon: "assets/dana/dana.png",
                  name: "DANA",
                );
              }
            },
          );
        } else if (state is DanaBalanceLoading || state is DanaBalanceInitial) {
          return PaymentMethodLoading(
            icon: "assets/dana/dana.png",
            name: "DANA",
          );
        } else {
          return InstantPaymentError(
            icon: "assets/dana/dana.png",
            name: "DANA",
          );
        }
      },
    );
  }
}
