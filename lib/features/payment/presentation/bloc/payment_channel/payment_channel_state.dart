part of 'payment_channel_bloc.dart';

abstract class PaymentChannelState extends Equatable {
  const PaymentChannelState();
}

class PaymentChannelLoading extends PaymentChannelState {
  @override
  List<Object> get props => [];
}

class PaymentChannelLoaded extends PaymentChannelState {
  final PaymentChannelModel paymentChannelModel;

  PaymentChannelLoaded({this.paymentChannelModel});
  @override
  List<Object> get props => [paymentChannelModel];
}

class PaymentChannelError extends PaymentChannelState {
  @override
  List<Object> get props => [];
}
