import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/features/payment/data/models/payment_channel_model.dart';
import 'package:santaraapp/features/payment/domain/usecases/payment_usecase.dart';

part 'payment_channel_event.dart';
part 'payment_channel_state.dart';

class PaymentChannelBloc
    extends Bloc<PaymentChannelEvent, PaymentChannelState> {
  final GetPaymentChannel getPaymentChannel;

  PaymentChannelBloc({@required GetPaymentChannel getPaymentChannel})
      : assert(getPaymentChannel != null),
        getPaymentChannel = getPaymentChannel,
        super(PaymentChannelLoading());

  @override
  Stream<PaymentChannelState> mapEventToState(
    PaymentChannelEvent event,
  ) async* {
    if (event is LoadPaymentChannel) {
      try {
        yield PaymentChannelLoading();
        final result = await getPaymentChannel(NoParams());

        yield* result.fold(
          (failure) async* {
            yield PaymentChannelError();
          },
          (paymentChannelModel) async* {
            if (paymentChannelModel != null) {
              yield PaymentChannelLoaded(
                  paymentChannelModel: paymentChannelModel);
            } else {
              yield PaymentChannelError();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield PaymentChannelError();
      }
    }
  }
}
