part of 'payment_channel_bloc.dart';

abstract class PaymentChannelEvent extends Equatable {
  const PaymentChannelEvent();
}

class LoadPaymentChannel extends PaymentChannelEvent {
  @override
  List<Object> get props => [];
}
