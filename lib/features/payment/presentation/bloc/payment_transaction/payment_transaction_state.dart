part of 'payment_transaction_bloc.dart';

abstract class PaymentTransactionState extends Equatable {
  const PaymentTransactionState();

  @override
  List<Object> get props => [];
}

class PaymentTransactionInitial extends PaymentTransactionState {}

class PaymentTransactionLoading extends PaymentTransactionState {}

class PaymentTransactionSuccess extends PaymentTransactionState {}

class PaymentTransactionFailed extends PaymentTransactionState {
  final String message;

  PaymentTransactionFailed({this.message});
}
