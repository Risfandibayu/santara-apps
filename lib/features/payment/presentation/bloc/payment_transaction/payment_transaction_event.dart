part of 'payment_transaction_bloc.dart';

abstract class PaymentTransactionEvent extends Equatable {
  const PaymentTransactionEvent();

  @override
  List<Object> get props => [];
}

class PostTransaction extends PaymentTransactionEvent {
  final SubmitTransactionModel body;

  PostTransaction({this.body});
}

class PostDeposit extends PaymentTransactionEvent {
  final SubmitDepositModel body;

  PostDeposit({this.body});
}
