import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/features/payment/data/models/submit_deposit_model.dart';
import 'package:santaraapp/features/payment/data/models/submit_transaction_model.dart';
import 'package:santaraapp/features/payment/domain/usecases/payment_usecase.dart';
import 'package:meta/meta.dart';

part 'payment_transaction_event.dart';
part 'payment_transaction_state.dart';

class PaymentTransactionBloc
    extends Bloc<PaymentTransactionEvent, PaymentTransactionState> {
  final SubmitTransaction _submitTransaction;
  final SubmitDeposit _submitDeposit;
  PaymentTransactionBloc(
      {@required SubmitTransaction submitTransaction,
      @required SubmitDeposit submitDeposit})
      : assert(submitTransaction != null),
        assert(submitDeposit != null),
        _submitTransaction = submitTransaction,
        _submitDeposit = submitDeposit,
        super(PaymentTransactionInitial());

  @override
  Stream<PaymentTransactionState> mapEventToState(
    PaymentTransactionEvent event,
  ) async* {
    if (event is PostTransaction) {
      yield* _submitTransactionToState(event);
    }
    if (event is PostDeposit) {
      yield* _submitDepositToState(event);
    }
  }

  Stream<PaymentTransactionState> _submitTransactionToState(
      PostTransaction event) async* {
    try {
      yield PaymentTransactionLoading();
      final result = await _submitTransaction(event.body);

      yield* result.fold(
        (failure) async* {
          yield PaymentTransactionFailed(message: failure.message);
        },
        (submit) async* {
          if (submit) {
            yield PaymentTransactionSuccess();
          } else {
            yield PaymentTransactionFailed(
                message: "Gagal melakukan transaksi");
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield PaymentTransactionFailed(message: e.message);
    }
  }

  Stream<PaymentTransactionState> _submitDepositToState(
      PostDeposit event) async* {
    try {
      yield PaymentTransactionLoading();
      final result = await _submitDeposit(event.body);

      yield* result.fold(
        (failure) async* {
          yield PaymentTransactionFailed(message: failure.message);
        },
        (submit) async* {
          if (submit) {
            yield PaymentTransactionSuccess();
          } else {
            yield PaymentTransactionFailed(message: "Gagal melakukan deposit");
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield PaymentTransactionFailed(message: e.message);
    }
  }
}
