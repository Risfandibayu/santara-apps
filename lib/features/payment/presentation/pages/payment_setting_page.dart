import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/utils/api.dart';

import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../pages/Home.dart';
import '../../../dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../dana/presentation/pages/dana_setting_page.dart';
import '../../../dana/presentation/pages/dana_webview.dart';
import '../../../dana/presentation/widgets/dana_bottom_sheet.dart';
import '../../../dana/presentation/widgets/dana_overlay_notification.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../widgets/payment_setting_list_tile.dart';

class PaymentSettingPage extends StatefulWidget {
  @override
  _PaymentSettingPageState createState() => _PaymentSettingPageState();
}

class _PaymentSettingPageState extends State<PaymentSettingPage> {
  AuthDanaBloc _authDanaBloc;
  DanaBalanceBloc _danaBalanceBloc;
  SaldoDepositBloc _saldoDepositBloc;
  bool isDanaActive = true;

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    setState(() => isDanaActive = remoteConfig.getBool(isFeatureDanaActive));
  }

  @override
  void initState() {
    super.initState();
    getRemoteConfig();
    _authDanaBloc = BlocProvider.of<AuthDanaBloc>(context);
    _danaBalanceBloc = BlocProvider.of<DanaBalanceBloc>(context);
    _danaBalanceBloc.add(LoadDanaBalance());
    _saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    _saldoDepositBloc.add(LoadSaldoDeposit(0));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthDanaBloc, AuthDanaState>(
        // bloc: _authDanaBloc,
        listener: (context, state) async {},
        builder: (context, state) {
          return ModalProgressHUD(
            inAsyncCall: state is AuthDanaLoading,
            color: Colors.grey,
            opacity: 0.5,
            progressIndicator: CupertinoActivityIndicator(),
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                centerTitle: true,
                title: Text(
                  "Pengaturan Pembayaran",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                iconTheme: IconThemeData(color: Colors.black),
              ),
              body: ListView(
                padding: EdgeInsets.all(Sizes.s10),
                children: [
                  BlocListener<DanaBalanceBloc, DanaBalanceState>(
                    listener: (context, danaState) async {
                      if (danaState is DanaBalanceLoaded) {
                        if (state is LoginDanaSuccess) {
                          final result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => DanaWebview(
                                appbarName: "",
                                url: state.url,
                                type: DanaWebviewType.auth,
                              ),
                            ),
                          );
                          if (result != null) {
                            _authDanaBloc.add(ApplyToken(authCode: result));
                          }
                        }

                        if (state is ApplyTokenSuccess) {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(builder: (_) => Home()),
                              (route) => false);
                          DanaOverlayNotifications.showDanaOverlayNotification(
                            "Akun DANA berhasil diaktivasi",
                          );
                        }

                        if (state is AuthDanaFailed) {
                          DanaBottomSheet.danaActivationFailed(context, () {
                            Navigator.pop(context);
                            _authDanaBloc.add(LoginDana());
                          });
                        }
                      }
                    },
                    child: BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
                      builder: (context, state) {
                        if (state is DanaBalanceLoaded) {
                          return BlocBuilder<SaldoDepositBloc,
                                  SaldoDepositState>(
                              builder: (context, stateSaldo) {
                            if (stateSaldo is SaldoDepositLoaded) {
                              return PaymentSettingListTile(
                                icon: "assets/dana/dana_vertical.png",
                                name: "DANA",
                                isActive: stateSaldo.user.isDana == 1,
                                isDanaActive: isDanaActive,
                                onTap: () async {
                                  if (stateSaldo.user.isDana == 1) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => DanaSettingPage(),
                                      ),
                                    );
                                  } else {
                                    if (isDanaActive) {
                                      DanaBottomSheet.confirmConnectToDana(
                                          context, () {
                                        Navigator.pop(context);
                                        _authDanaBloc.add(LoginDana());
                                      });
                                    } else {
                                      PopupHelper.warningInfo(
                                        context,
                                        "Fitur DANA belum tersedia",
                                      );
                                    }
                                  }
                                },
                              );
                            } else if (stateSaldo is SaldoDepositLoading ||
                                stateSaldo is SaldoDepositInitial) {
                              return PaymentSettingOnLoading(
                                icon: "assets/dana/dana_vertical.png",
                                name: "DANA",
                              );
                            } else {
                              return PaymentSettingOnError(
                                icon: "assets/dana/dana_vertical.png",
                                name: "DANA",
                              );
                            }
                          });
                        } else if (state is DanaBalanceInitial ||
                            state is DanaBalanceLoading) {
                          return PaymentSettingOnLoading(
                            icon: "assets/dana/dana_vertical.png",
                            name: "DANA",
                          );
                        } else {
                          return PaymentSettingOnError(
                            icon: "assets/dana/dana_vertical.png",
                            name: "DANA",
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
