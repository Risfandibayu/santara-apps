import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/models/BuyToken.dart';
import 'package:santaraapp/services/new_transaction/new_transaction_service.dart';
import 'package:santaraapp/services/transactions/deposit.dart';
import 'package:santaraapp/utils/rev_color.dart';
import 'package:santaraapp/widget/popup/checkout_transaction_success.dart';
import '../../../../core/usecases/unauthorize_usecase.dart';
import '../../../../core/widgets/bottom_sheet/payment_bottom_sheet.dart';
import '../../../../core/widgets/popup/checkout_transaction_success.dart';
import '../../../../core/widgets/popup/submit_failed_bca_popup.dart';
import '../../../../helpers/RupiahFormatter.dart';
import '../../../../pages/Home.dart';
import '../../../../utils/api.dart';
import '../../../../utils/sizes.dart';
import '../../../../widget/security_token/pin/SecurityPinUI.dart';
import '../../../../widget/widget/components/market/toast_helper.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../deposit/deposit_history/data/models/deposit_history_model.dart';
import '../../../deposit/deposit_history/presentation/bloc/deposit_history_bloc.dart';
import '../../../deposit/main/presentation/pages/main_deposit_page.dart';
import '../../../transaction/data/models/checkout_list_model.dart';
import '../../../transaction/presentation/bloc/checkout_list/checkout_list_bloc.dart';
import '../../data/models/payment_channel_model.dart';
import '../../data/models/submit_transaction_model.dart';
import '../bloc/payment_transaction/payment_transaction_bloc.dart';
import '../widgets/instant_payment.dart';
import '../widgets/instant_payment_error.dart';
import '../widgets/payment_method_loading.dart';

enum TransactionPaymentType { deposit, perdana }

class PaymentPage extends StatefulWidget {
  final String emitenUuid;
  final String name;
  final String codeEmiten;
  final num price;
  final num amount;
  final String total;
  final TransactionPaymentType type;

  const PaymentPage(
      {Key key,
      this.name,
      this.codeEmiten,
      this.emitenUuid,
      this.price,
      this.amount,
      this.total,
      @required this.type})
      : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  final storage = new FlutterSecureStorage();

  PaymentTransactionBloc _paymentTransactionBloc;
  SaldoDepositBloc _saldoDepositBloc;
  bool isExpand = false;
  final _depositService = DepositService();

  String fee = '';

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
  }

  String iconBank(String name) {
    switch (name.toLowerCase()) {
      case 'bca':
        return 'bca.png';
        break;
      case 'bni':
        return 'bni.png';
        break;
      case 'bri':
        return 'bri.png';
        break;
      case 'mandiri':
        return 'mandiri.png';
        break;
      case 'permata':
        return 'permatabank.png';
        break;
      default:
        return '';
    }
  }

  // new metode payment dengan onepay
  // dengan menggunakan refresh token, dan salt sebelum hit transaksi
  void _inputPin(String channel, String accountNumber, {VirtualAccount va}) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SecurityPinUI(
          type: SecurityType.check,
          title: "Masukan PIN Anda",
          onSuccess: (pin, finger) async {
            var responseRefreshToken =
                await NewTransactionService().refreshToken();
            var parsedRefreshToken = responseRefreshToken != null
                ? jsonDecode(responseRefreshToken.body)
                : [];
            // print('repres-token$parsedRefreshToken');
            if (parsedRefreshToken['status']) {
              storage.delete(key: 'token');
              storage.write(
                  key: 'token', value: parsedRefreshToken["data"]["newtoken"]);
              try {
                var responseBuytoken = await NewTransactionService()
                    .buyTokenService(BuyToken(
                        emitenUuid: widget.emitenUuid,
                        amount: widget.total,
                        channel: channel,
                        pin: pin,
                        finger: finger));
                var parsedBuyToken = responseBuytoken != null
                    ? jsonDecode(responseBuytoken.body)
                    : [];
                print(responseBuytoken.statusCode);
                print('buy-token$parsedBuyToken');
                // print(channel);
                if (channel == "ONEPAY") {
                  if (responseBuytoken.statusCode == 200) {
                    if (parsedBuyToken['status']) {
                      if (parsedBuyToken['data']['transaksi']['insertStatus'] ==
                          "00") {
                        DialogHelper.show(context,
                            url: parsedBuyToken['data']['transaksi']
                                ['redirectURL'],
                            channel: channel,
                            message: 'Silahkan lanjutkan pembayaran anda');
                      } else {
                        DialogHelper.showGagal(context,
                            message: parsedBuyToken['data']['transaksi']
                                ['insertMessage']);
                      }
                    } else {
                      DialogHelper.showGagal(context,
                          message: 'Gagal Melakukan Transaksi');
                    }
                  } else if (responseBuytoken.statusCode == 203) {
                    DialogHelper.showGagal(context,
                        message: parsedBuyToken['error'][0]['message']);
                  } else {
                    DialogHelper.showGagal(context,
                        message: parsedBuyToken['error'][0]['message']);
                  }
                } else {
                  if (responseBuytoken.statusCode == 200) {
                    DialogHelper.show(context, channel: channel, function: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => Home(index: 1)),
                          (route) => false);
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (_) =>
                      //             TransactionsUI(initialTab: 1)));
                    });
                  } else {
                    DialogHelper.showGagal(context,
                        message: parsedBuyToken['message']);
                  }
                }
              } catch (err) {
                print(err);
              }
            } else {
              DialogHelper.showGagal(context,
                  message: parsedRefreshToken['error'][0]['message']);
            }
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    getRemoteConfig();
    _paymentTransactionBloc = BlocProvider.of<PaymentTransactionBloc>(context);
    _saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    _saldoDepositBloc.add(LoadSaldoDeposit(0));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PaymentTransactionBloc, PaymentTransactionState>(
        listener: (context, state) {
      if (state is PaymentTransactionSuccess) {
        if (widget.type == TransactionPaymentType.deposit) {
          Navigator.pop(context);
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (_) => MainDepositPage(initialTab: 1)));
        } else {
          DialogHelper.show(context);
        }
      }
      if (state is PaymentTransactionFailed) {
        ToastHelper.showFailureToast(context, state.message);
      }
    }, builder: (context, state) {
      return ModalProgressHUD(
          inAsyncCall: state is PaymentTransactionLoading,
          opacity: 0.5,
          color: Colors.grey,
          progressIndicator: CupertinoActivityIndicator(),
          child: Scaffold(
              appBar: AppBar(
                title: Text(
                  "Pembayaran",
                  style: TextStyle(color: Colors.white),
                ),
                centerTitle: true,
                backgroundColor: Color(ColorRev.mainBlack),
                iconTheme: IconThemeData(color: Colors.white),
              ),
              bottomNavigationBar: _bottomNavigationBar,
              body: BlocBuilder<DepositHistoryBloc, DepositHistoryState>(
                  builder: (context, stateHistoryDeposit) {
                List<DepositHistoryModel> depositHistory;
                if (stateHistoryDeposit is DepositHistoryLoaded) {
                  depositHistory = stateHistoryDeposit.depositHistory;
                }
                return BlocBuilder<CheckoutListBloc, CheckoutListState>(
                    builder: (context, stateCheckoutList) {
                  List<CheckoutListModel> checkoutHistory;
                  if (stateCheckoutList is CheckoutListLoaded) {
                    checkoutHistory = stateCheckoutList.checkoutList;
                  }
                  return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
                      builder: (context, state) {
                    if (state is SaldoDepositLoaded) {
                      return ListView(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(Sizes.s20),
                            child: Text("Pembayaran Instant",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSize.s15,
                                )),
                          ),
                          widget.type == TransactionPaymentType.deposit
                              ? Container()
                              : InstantPayment(
                                  icon: "assets/icon_menu/new/Deposit.png",
                                  name: "Dompet",
                                  saldo: state.saldoDeposit.idr.toString(),
                                  isActive: state.saldoDeposit.idr >=
                                      num.parse(widget.total),
                                  onTap: state.saldoDeposit.idr >=
                                          num.parse(widget.total)
                                      ? () => PaymentBottomSheet.confirm(
                                          context,
                                          icon:
                                              "assets/icon_menu/new/Deposit.png",
                                          title: "Saldo Dompet",
                                          saldo: state.saldoDeposit.idr,
                                          total: num.parse(widget.total),
                                          channel: 1,
                                          onTap: () => _inputPin("WALLET", ""))
                                      : null),
                          _divider,
                          ListTile(
                            title: RichText(
                              text: TextSpan(
                                text: "Metode Pembayaran ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: FontSize.s15,
                                    fontFamily: 'Nunito'),
                                children: [
                                  TextSpan(
                                      text: "(Verifikasi Otomatis)",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal)),
                                ],
                              ),
                            ),
                            subtitle: Text(
                                "Transaksi akan dikenakan biaya admin sebesar ${RupiahFormatter.initialValueFormat(state.fee.transaction.onepay.toString())}"),
                          ),
                          ListTile(
                            // leading: Image.asset("assets/bank/bca.png",
                            //     height: 40, width: 60, fit: BoxFit.contain),
                            title: Text("OTHER PAYMENT",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            trailing: Icon(Icons.chevron_right),
                            onTap: () {
                              PaymentBottomSheet.confirm(context,
                                  icon: "",
                                  // title: "ONEPAY",
                                  title: "OTHER PAYMENT",
                                  saldo: 0,
                                  total: num.parse(widget.total),
                                  channel: 2, onTap: () {
                                _inputPin("ONEPAY", '');
                              });
                            },
                          ),
                        ],
                      );
                    } else if (state is SaldoDepositLoading ||
                        state is SaldoDepositInitial) {
                      return PaymentMethodLoading(
                        icon: "assets/icon_menu/new/Deposit.png",
                        name: "Dompet",
                      );
                    } else {
                      return InstantPaymentError(
                        icon: "assets/icon_menu/new/Deposit.png",
                        name: "Dompet",
                      );
                    }
                  });
                });
              })));
    });
  }

  get _divider {
    return Container(
      height: 10,
      color: Color(0xFFF4F4F4),
    );
  }

  get _bottomNavigationBar {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(blurRadius: Sizes.s5, color: Colors.grey.withOpacity(0.5)),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(6),
          topRight: Radius.circular(6),
        ),
      ),
      height: 58,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: Sizes.s15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.type == TransactionPaymentType.deposit
                  ? "Total Deposit"
                  : "Total Bayar",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: FontSize.s15,
              ),
            ),
            Container(width: 10),
            Row(
              children: [
                Text(
                  RupiahFormatter.initialValueFormat(widget.total),
                  style: TextStyle(
                    color: Color(0xFF0E7E4A),
                    fontWeight: FontWeight.bold,
                    fontSize: FontSize.s15,
                  ),
                ),
                widget.type == TransactionPaymentType.deposit
                    ? Container()
                    : IconButton(
                        icon: Icon(Icons.keyboard_arrow_up),
                        onPressed: () => PaymentBottomSheet.paymentDetail(
                            context,
                            name: widget.name,
                            codeEmiten: widget.codeEmiten,
                            amount: widget.amount,
                            price: widget.price,
                            total: widget.total))
              ],
            )
          ],
        ),
      ),
    );
  }
}
