import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/payment_channel_model.dart';
import '../../data/models/submit_deposit_model.dart';
import '../../data/models/submit_transaction_model.dart';
import '../repositories/payment_repository.dart';

class GetPaymentChannel implements UseCase<PaymentChannelModel, NoParams> {
  final PaymentRepository repository;
  GetPaymentChannel(this.repository);

  @override
  Future<Either<Failure, PaymentChannelModel>> call(NoParams params) async {
    return await repository.getPaymentChannel();
  }
}

class SubmitTransaction implements UseCase<bool, SubmitTransactionModel> {
  final PaymentRepository repository;
  SubmitTransaction(this.repository);

  @override
  Future<Either<Failure, bool>> call(SubmitTransactionModel body) async {
    return await repository.submitTransaction(body);
  }
}

class SubmitDeposit implements UseCase<bool, SubmitDepositModel> {
  final PaymentRepository repository;
  SubmitDeposit(this.repository);

  @override
  Future<Either<Failure, bool>> call(SubmitDepositModel body) async {
    return await repository.submitDeposit(body);
  }
}
