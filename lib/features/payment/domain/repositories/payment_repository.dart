import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/payment_channel_model.dart';
import '../../data/models/submit_deposit_model.dart';
import '../../data/models/submit_transaction_model.dart';

abstract class PaymentRepository {
  Future<Either<Failure, PaymentChannelModel>> getPaymentChannel();
  Future<Either<Failure, bool>> submitTransaction(SubmitTransactionModel body);
  Future<Either<Failure, bool>> submitDeposit(SubmitDepositModel body);
}
