import 'dart:convert';

import '../../../../../core/data/models/user_model.dart';

LoginResultModel loginFromJson(String str) =>
    LoginResultModel.fromJson(json.decode(str));

String loginToJson(LoginResultModel data) => json.encode(data.toJson());

class LoginResultModel {
  Token token;
  User user;

  LoginResultModel({
    this.token,
    this.user,
  });

  factory LoginResultModel.fromJson(Map<String, dynamic> json) =>
      LoginResultModel(
        token: Token.fromJson(json["token"]),
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "token": token.toJson(),
        "user": user.toJson(),
      };
}

class Token {
  String type;
  String token;
  String refreshToken;

  Token({
    this.type,
    this.token,
    this.refreshToken,
  });

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        type: json["type"],
        token: json["token"],
        refreshToken: json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "token": token,
        "refreshToken": refreshToken,
      };
}
