import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../../../../core/data/models/fee_model.dart';
import '../../../../core/data/models/user_model.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../helpers/ToastHelper.dart';
import '../../../../pages/Home.dart';
import '../../../../utils/api.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../../../dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../disbursement/presentation/blocs/disbursement/disbursement_bloc.dart';
import '../../../disbursement/presentation/widgets/disbursement_bottom_sheet.dart';
import '../blocs/dividen/dividen_bloc.dart';
import '../widgets/balance_information.dart';
import '../widgets/dividen_tnc.dart';
import 'main_dividen_page.dart';

class DividenPage extends StatefulWidget {
  @override
  _DividenPageState createState() => _DividenPageState();
}

class _DividenPageState extends State<DividenPage> {
  DisbursementBloc _disbursementBloc;
  AuthDanaBloc _authDanaBloc;
  bool isDanaActive = true;

  Future getRemoteConfig() async {
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    setState(() => isDanaActive = remoteConfig.getBool(isFeatureDanaActive));
  }

  @override
  void initState() {
    super.initState();
    getRemoteConfig();
    _disbursementBloc = BlocProvider.of<DisbursementBloc>(context);
    _authDanaBloc = BlocProvider.of<AuthDanaBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
      builder: (context, stateSaldo) {
        num idr = 0;
        User user;
        FeeModel fee;
        if (stateSaldo is SaldoDepositLoaded) {
          idr = stateSaldo.saldoDeposit.idr;
          user = stateSaldo.user;
          fee = stateSaldo.fee;
        }
        return BlocBuilder<DanaBalanceBloc, DanaBalanceState>(
          builder: (context, stateDana) {
            num dana = 0;
            String danaKyc;
            if (stateDana is DanaBalanceLoaded) {
              dana = stateDana.balance;
              danaKyc = stateDana.danaKyc;
            }
            return BlocConsumer<DisbursementBloc, DisbursementState>(
              listener: (context, state) {
                if (state is DisbursementSuccess) {
                  DisbursementBottomSheet.disbursementSuccess(
                      context: context,
                      title: "Pengajuan Pencairan Dividen Berhasil",
                      subtitle: "Pencairan dividen Anda sedang kami proses",
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home(index: 4)),
                            (route) => false);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => MainDividenPage(initialTab: 1)),
                        );
                      });
                }
                if (state is DisbursementFailed) {
                  ToastHelper.showFailureToast(context, state.message);
                }
              },
              builder: (context, state) {
                return ModalProgressHUD(
                    inAsyncCall: state is DisbursementLoading,
                    color: Colors.grey,
                    opacity: 0.5,
                    progressIndicator: CupertinoActivityIndicator(),
                    child: ListView(
                      padding: EdgeInsets.all(Sizes.s20),
                      children: [
                        BalanceInformation(),
                        SizedBox(height: Sizes.s15),
                        DividenTnc(),
                        SizedBox(height: Sizes.s15),
                        BlocBuilder<DividenBloc, DividenState>(
                          builder: (context, stateDividen) {
                            if (stateDividen is DividenLoaded) {
                              return SantaraMainButton(
                                isActive:
                                    stateDividen.total > 16500 && user != null,
                                title: "Pilih Metode Pencairan",
                                onPressed: () {
                                  DisbursementBottomSheet.chooseMethod(
                                      context: context,
                                      type: DisbursementType.dividen,
                                      saldoDompet: idr,
                                      saldoDana: dana,
                                      total: stateDividen.total,
                                      user: user,
                                      fee: fee,
                                      danaKyc: danaKyc,
                                      isDanaActive: isDanaActive,
                                      bloc: _disbursementBloc,
                                      authDanaBloc: _authDanaBloc);
                                },
                              );
                            } else {
                              return Container(
                                decoration: BoxDecoration(
                                    color: Color(0xFFF5F5F5),
                                    borderRadius: BorderRadius.circular(6)),
                                height: 50,
                                child: Center(
                                  child: CupertinoActivityIndicator(),
                                ),
                              );
                            }
                          },
                        ),
                      ],
                    ));
              },
            );
          },
        );
      },
    );
  }
}
