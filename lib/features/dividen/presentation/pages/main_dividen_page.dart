import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/usecases/unauthorize_usecase.dart';
import '../../../../widget/home/blocs/button.bloc.dart';
import '../../../../widget/widget/components/kyc/KycNotificationStatus.dart';
import '../../../dana/presentation/bloc/dana_balance/dana_balance_bloc.dart';
import '../../../deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../disbursement/presentation/blocs/bank_withdraw/bank_withdraw_bloc.dart';
import '../blocs/dividen/dividen_bloc.dart';
import 'dividen_history_page.dart';
import 'dividen_page.dart';

class MainDividenPage extends StatefulWidget {
  final int initialTab;

  const MainDividenPage({Key key, this.initialTab}) : super(key: key);
  @override
  _MainDividenPageState createState() => _MainDividenPageState();
}

class _MainDividenPageState extends State<MainDividenPage> {
  DividenBloc dividenBloc;
  ButtonBloc buttonBloc;
  DanaBalanceBloc danaBalanceBloc;
  SaldoDepositBloc saldoDepositBloc;
  BankWithdrawBloc bankWithdrawBloc;

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    dividenBloc = BlocProvider.of<DividenBloc>(context);
    dividenBloc.add(LoadDividen());
    buttonBloc = BlocProvider.of<ButtonBloc>(context);
    buttonBloc.add(LoadButton());
    danaBalanceBloc = BlocProvider.of<DanaBalanceBloc>(context);
    danaBalanceBloc.add(LoadDanaBalance());
    saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    saldoDepositBloc.add(LoadSaldoDeposit(0));
    bankWithdrawBloc = BlocProvider.of<BankWithdrawBloc>(context);
    bankWithdrawBloc.add(LoadBankList());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.initialTab ?? 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Dividen",
            style: TextStyle(color: Colors.black),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Color((0xFFBF2D30)),
            labelColor: Color((0xFFBF2D30)),
            unselectedLabelColor: Colors.black,
            tabs: [
              Tab(text: "Pencairan Dividen"),
              Tab(text: "Riwayat Dividen"),
            ],
          ),
        ),
        body: Stack(children: [
          TabBarView(
            children: [
              DividenPage(),
              DividenHistoryPage(),
            ],
          ),
          KycNotificationStatus(
            showCloseButton: false,
          )
        ]),
      ),
    );
  }
}
