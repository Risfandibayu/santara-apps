import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/core/utils/ui/screen_sizes.dart';
import 'package:santaraapp/core/widgets/shimmer/box_shimmer.dart';
import 'package:santaraapp/features/deposit/deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import 'package:santaraapp/features/dividen/data/models/dividen_model.dart';
import 'package:santaraapp/features/dividen/presentation/blocs/dividen/dividen_bloc.dart';
import 'package:santaraapp/features/dividen/presentation/widgets/dividen_history_card.dart';

class DividenHistoryPage extends StatefulWidget {
  @override
  _DividenHistoryPageState createState() => _DividenHistoryPageState();
}

class _DividenHistoryPageState extends State<DividenHistoryPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Sizes.s45),
          child: TabBar(
            labelPadding: EdgeInsets.only(top: Sizes.s8),
            indicatorColor: Color((0xFFBF2D30)),
            labelColor: Color((0xFFBF2D30)),
            unselectedLabelColor: Colors.black,
            indicatorSize: TabBarIndicatorSize.label,
            labelStyle: TextStyle(fontSize: FontSize.s12),
            tabs: [
              Tab(text: "Siap Dicairkan"),
              Tab(text: "Proses Pencairan"),
              Tab(text: "Sudah Dicairkan"),
            ],
          ),
        ),
        body: BlocBuilder<DividenBloc, DividenState>(
          builder: (context, state) {
            if (state is DividenLoaded) {
              return TabBarView(
                children: [
                  _dividenHistoryList(state.available, false),
                  _dividenHistoryList(state.inProgress, true),
                  _dividenHistoryList(state.disbursed, true),
                ],
              );
            } else if (state is DividenLoading || state is DividenInitial) {
              return TabBarView(
                children: [
                  _dividenLoading(),
                  _dividenLoading(),
                  _dividenLoading()
                ],
              );
            } else if (state is DividenError) {
              return TabBarView(
                children: [
                  _dividenEmpty(state.message),
                  _dividenEmpty(state.message),
                  _dividenEmpty(state.message)
                ],
              );
            } else {
              return TabBarView(
                children: [
                  _dividenEmpty(
                      "Anda Belum Memiliki Riwayat Penarikan Dividen"),
                  _dividenEmpty(
                      "Anda Belum Memiliki Riwayat Penarikan Dividen"),
                  _dividenEmpty(
                      "Anda Belum Memiliki Riwayat Penarikan Dividen"),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  Widget _dividenHistoryList(
      List<DividenHistoryModel> data, bool isWithExpand) {
    if (data.length > 0) {
      return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
        builder: (context, state) {
          if (state is SaldoDepositLoaded) {
            return ListView.builder(
                padding: EdgeInsets.fromLTRB(
                    Sizes.s15, Sizes.s10, Sizes.s15, Sizes.s15),
                itemCount: data.length,
                itemBuilder: (_, i) {
                  return DividenHistoryCard(
                    dividen: data[i],
                    user: state.user,
                    isWithExpand: isWithExpand,
                  );
                });
          } else if (state is SaldoDepositLoading ||
              state is SaldoDepositInitial) {
            return _dividenLoading();
          } else {
            return _dividenEmpty("Terjadi kesalahan");
          }
        },
      );
    } else {
      return _dividenEmpty("Anda Belum Memiliki Riwayat Penarikan Dividen");
    }
  }

  Widget _dividenLoading() {
    return ListView.separated(
      padding: EdgeInsets.fromLTRB(Sizes.s20, Sizes.s15, Sizes.s20, Sizes.s20),
      itemCount: 3,
      itemBuilder: (_, i) {
        return BoxShimmer(width: double.infinity, height: 100, borderRadius: 6);
      },
      separatorBuilder: (_, i) {
        return Container(height: 10);
      },
    );
  }

  Widget _dividenEmpty(String msg) {
    return Container(
        padding:
            EdgeInsets.fromLTRB(Sizes.s15, Sizes.s10, Sizes.s15, Sizes.s15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/icon/empty_dividen.png"),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Text(
                msg,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ));
  }
}
