import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/data/models/fee_model.dart';
import 'package:santaraapp/core/domain/usecases/fee_usecase.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../data/models/dividen_model.dart';
import '../../../domain/usecase/dividen_usecase.dart';

part 'dividen_event.dart';
part 'dividen_state.dart';

class DividenBloc extends Bloc<DividenEvent, DividenState> {
  final GetDividen _getDividen;
  final GetAllFee _getAllFee;

  DividenBloc({@required GetDividen getDividen, @required GetAllFee getAllFee})
      : assert(getDividen != null),
        assert(getAllFee != null),
        _getDividen = getDividen,
        _getAllFee = getAllFee,
        super(DividenInitial());

  @override
  Stream<DividenState> mapEventToState(
    DividenEvent event,
  ) async* {
    if (event is LoadDividen) {
      try {
        yield DividenLoading();
        final result = await _getDividen(NoParams());
        final dividenFee = await _getAllFee(NoParams());

        yield* dividenFee.fold((failure) async* {
          yield DividenError(message: failure.message);
        }, (fee) async* {
          yield* result.fold(
            (failure) async* {
              yield DividenError(message: failure.message);
            },
            (dividen) async* {
              if (dividen != null) {
                List<DividenHistoryModel> available = [];
                List<DividenHistoryModel> inProgress = [];
                List<DividenHistoryModel> disbursed = [];
                if (dividen.data.length > 0) {
                  for (var i = 0; i < dividen.data.length; i++) {
                    if (dividen.data[i].status == 0 ||
                        dividen.data[i].status == 3) {
                      available.add(dividen.data[i]);
                    } else if (dividen.data[i].status == 1) {
                      inProgress.add(dividen.data[i]);
                    } else {
                      disbursed.add(dividen.data[i]);
                    }
                  }
                }
                yield DividenLoaded(
                    total: dividen.total,
                    available: available,
                    inProgress: inProgress,
                    disbursed: disbursed,
                    fee: fee.dividend);
              } else {
                yield DividenEmpty();
              }
            },
          );
        });
      } catch (e, stack) {
        santaraLog(e, stack);
        yield DividenError(message: e.toString());
      }
    }
  }
}
