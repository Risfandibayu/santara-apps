part of 'dividen_bloc.dart';

abstract class DividenState extends Equatable {
  const DividenState();

  @override
  List<Object> get props => [];
}

class DividenInitial extends DividenState {}

class DividenLoading extends DividenState {}

class DividenLoaded extends DividenState {
  final num total;
  final List<DividenHistoryModel> available;
  final List<DividenHistoryModel> inProgress;
  final List<DividenHistoryModel> disbursed;
  final Dividend fee;

  DividenLoaded({
    this.total,
    this.available,
    this.inProgress,
    this.disbursed,
    this.fee,
  });
}

class DividenEmpty extends DividenState {}

class DividenError extends DividenState {
  final String message;

  DividenError({this.message});
}
