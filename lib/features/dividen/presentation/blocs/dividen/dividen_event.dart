part of 'dividen_bloc.dart';

abstract class DividenEvent extends Equatable {
  const DividenEvent();

  @override
  List<Object> get props => [];
}

class LoadDividen extends DividenEvent {}
