import 'package:flutter/material.dart';

import '../../../../core/widgets/list_item_widget.dart/santara_bullet_list.dart';

class DividenTnc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SantaraBulletList(
          child: Text(
        "Nama pemilik rekening harus sama dengan nama pemilik akun",
        style: TextStyle(color: Color(0xFF747474), fontSize: 14),
      )),
      SantaraBulletList(
          child: Text(
        "Normal Min. pencairan ke rekening Anda adalah Rp 16.500, dibawah nominal tersebut belum dapat dicairkan atau dapat diakumulasikan dengan bagi hasil lainnya dan atau periode pembagian selanjutnya.",
        style: TextStyle(color: Color(0xFF747474), fontSize: 14),
      )),
      SantaraBulletList(
          child: Text(
        "Lama waktu pencairan ke rekening pengguna Maks. 2x24 Jam hari kerja bank.",
        style: TextStyle(color: Color(0xFF747474), fontSize: 14),
      )),
    ]);
  }
}
