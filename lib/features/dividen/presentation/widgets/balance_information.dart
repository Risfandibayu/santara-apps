import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/formatter/currency_formatter.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../core/widgets/shimmer/box_shimmer.dart';
import '../blocs/dividen/dividen_bloc.dart';

class BalanceInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minHeight: 58),
      padding: EdgeInsets.symmetric(horizontal: Sizes.s20, vertical: Sizes.s10),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Color(0xFFB8B8B8)),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Total Dividen",
            style: TextStyle(
              color: Color(0xFF858585),
              fontSize: Sizes.s10,
            ),
          ),
          SizedBox(height: 2),
          BlocBuilder<DividenBloc, DividenState>(
            builder: (context, state) {
              if (state is DividenLoaded) {
                return Text(
                  CurrencyFormatter.format(state.total),
                  style: TextStyle(
                      color: Color(0xFF0E7E4A),
                      fontSize: Sizes.s16,
                      fontWeight: FontWeight.bold),
                );
              } else if (state is DividenLoading || state is DividenInitial) {
                return BoxShimmer(
                  width: MediaQuery.of(context).size.width / 3,
                  height: Sizes.s20,
                  borderRadius: Sizes.s10,
                );
              } else {
                return Text(
                  "-",
                  style: TextStyle(
                      color: Color(0xFF0E7E4A),
                      fontSize: Sizes.s16,
                      fontWeight: FontWeight.bold),
                );
              }
            },
          )
        ],
      ),
    );
  }
}
