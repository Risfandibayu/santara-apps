import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/core/utils/ui/screen_sizes.dart';

import '../../../../core/data/models/user_model.dart';
import '../../../../core/utils/formatter/currency_formatter.dart';
import '../../../../core/utils/formatter/date_formatter.dart';
import '../../data/models/dividen_model.dart';
import 'content_expanded.dart';

class DividenHistoryCard extends StatelessWidget {
  final DividenHistoryModel dividen;
  final User user;
  final bool isWithExpand;

  const DividenHistoryCard(
      {Key key, this.dividen, this.user, this.isWithExpand})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Color(0xFFB8B8B8),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                color: Color(0xFFF0F0F0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                )),
            height: 40,
            width: double.maxFinite,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  DateFormatter.ddMMMyyyy(dividen.createdAt),
                  style: TextStyle(fontSize: 11),
                ),
                Text(
                    dividen.status == 0 || dividen.status == 3
                        ? ""
                        : dividen.status == 1
                            ? "Menunggu Verifikasi"
                            : "Berhasil",
                    style: TextStyle(
                        color: dividen.status == 1
                            ? Color(0xFFE6A037)
                            : Color(0xFF0E7E4A),
                        fontSize: FontSize.s12,
                        fontWeight: FontWeight.bold)),
              ],
            ),
          ),
          isWithExpand
              ? Container(
                  child: ExpandableNotifier(
                      child: ScrollOnExpand(
                    scrollOnExpand: true,
                    scrollOnCollapse: true,
                    child: ExpandablePanel(
                      header: Padding(
                        padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                        child: Row(
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(right: 12),
                                child: Image.asset(
                                  "assets/icon_menu/new/Dividen.png",
                                  height: 40,
                                  width: 40,
                                )),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(dividen.companyName,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold)),
                                  Text(
                                      CurrencyFormatter.format(
                                          dividen.devidend),
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w800,
                                          color: Color(0xFF0E7E4A))),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      expanded: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 6, right: 6, bottom: 12),
                              child: Container(
                                  child: ContentExpanded(
                                dividen: dividen,
                                user: user,
                              ))),
                          Container(
                            height: 1,
                            color: Colors.grey[200],
                          )
                        ],
                      ),
                      builder: (_, collapsed, expanded) {
                        return Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Expandable(
                            collapsed: collapsed,
                            expanded: expanded,
                          ),
                        );
                      },
                    ),
                  )),
                )
              : Padding(
                  padding: EdgeInsets.all(12),
                  child: Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(right: 12),
                          child: Image.asset(
                            "assets/icon_menu/new/Dividen.png",
                            height: 40,
                            width: 40,
                          )),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(dividen.companyName,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                            Text(CurrencyFormatter.format(dividen.devidend),
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w800,
                                    color: Color(0xFF0E7E4A))),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}
