import 'package:flutter/material.dart';

import '../../../../core/data/models/user_model.dart';
import '../../data/models/dividen_model.dart';

class ContentExpanded extends StatelessWidget {
  final DividenHistoryModel dividen;
  final User user;

  const ContentExpanded({Key key, this.dividen, this.user}) : super(key: key);

  String _disbursement(String disbursement) {
    switch (disbursement.toLowerCase()) {
      case "rekening":
        return "Bank Transfer";
        break;
      case "wallet":
        return "Dompet Santara";
        break;
      case "dana":
        return "DANA";
        break;
      default:
        return disbursement;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 1, color: Color(0xFFC4C4C4)),
        Container(height: 8),
        Text("Metode Pencairan"),
        Text(_disbursement(dividen.disbursement),
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        dividen.disbursement.toLowerCase() == "dana"
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(height: 8),
                  Text("Nama Pemilik Akun"),
                  Text(user.trader.name,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  Container(height: 8),
                  Text("Nomor Telp. Pemilik Akun"),
                  Text(user.phone,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                ],
              )
            : Container()
      ],
    );
  }
}
