import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/dividen_repository.dart';
import '../datasources/dividen_remote_data_source.dart';
import '../models/dividen_model.dart';

class DividenRepositoryImpl implements DividenRepository {
  final NetworkInfo networkInfo;
  final DividenRemoteDataSource remoteDataSource;

  DividenRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, DividenModel>> getDividen() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getDividen();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
