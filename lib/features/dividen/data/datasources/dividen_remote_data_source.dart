import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/dividen_model.dart';

abstract class DividenRemoteDataSource {
  Future<DividenModel> getDividen();
}

class DividenRemoteDataSourceImpl implements DividenRemoteDataSource {
  final RestClient client;
  DividenRemoteDataSourceImpl({@required this.client});

  @override
  Future<DividenModel> getDividen() async {
    try {
      final result = await client.get(path: "/devidend/");
      if (result.statusCode == 200) {
        return DividenModel.fromJson(result.data);
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
