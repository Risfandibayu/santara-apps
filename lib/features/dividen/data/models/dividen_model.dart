import 'dart:convert';

DividenModel dividenModelFromJson(String str) =>
    DividenModel.fromJson(json.decode(str));

String dividenModelToJson(DividenModel data) => json.encode(data.toJson());

class DividenModel {
  DividenModel({
    this.total,
    this.data,
  });

  int total;
  List<DividenHistoryModel> data;

  factory DividenModel.fromJson(Map<String, dynamic> json) => DividenModel(
        total: json["total"],
        data: List<DividenHistoryModel>.from(
            json["data"].map((x) => DividenHistoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DividenHistoryModel {
  DividenHistoryModel({
    this.companyName,
    this.codeEmiten,
    this.devidend,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.disbursement,
  });

  String companyName;
  String codeEmiten;
  int devidend;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  String disbursement;

  factory DividenHistoryModel.fromJson(Map<String, dynamic> json) =>
      DividenHistoryModel(
        companyName: json["company_name"],
        codeEmiten: json["code_emiten"],
        devidend: json["devidend"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        disbursement: json["disbursement"],
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName,
        "code_emiten": codeEmiten,
        "devidend": devidend,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "disbursement": disbursement,
      };
}
