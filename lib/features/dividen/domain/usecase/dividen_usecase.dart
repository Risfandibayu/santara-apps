import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/dividen_model.dart';
import '../repositories/dividen_repository.dart';

class GetDividen implements UseCase<DividenModel, NoParams> {
  final DividenRepository repository;
  GetDividen(this.repository);

  @override
  Future<Either<Failure, DividenModel>> call(NoParams params) async {
    return await repository.getDividen();
  }
}
