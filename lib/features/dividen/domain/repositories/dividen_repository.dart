import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/dividen_model.dart';

abstract class DividenRepository {
  Future<Either<Failure, DividenModel>> getDividen();
}
