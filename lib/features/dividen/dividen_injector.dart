import '../../injector_container.dart';
import 'data/datasources/dividen_remote_data_source.dart';
import 'data/repositories/dividen_repository_impl.dart';
import 'domain/repositories/dividen_repository.dart';
import 'domain/usecase/dividen_usecase.dart';
import 'presentation/blocs/dividen/dividen_bloc.dart';

void initDividenInjector() {
  // Bloc
  sl.registerFactory(() => DividenBloc(getDividen: sl(), getAllFee: sl()));

  //! Global Usecase

  sl.registerLazySingleton(() => GetDividen(sl()));

  //! Global Repository
  sl.registerLazySingleton<DividenRepository>(
    () => DividenRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<DividenRemoteDataSource>(
    () => DividenRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
