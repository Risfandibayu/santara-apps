import '../../injector_container.dart';
import 'data/datasources/disbursement_remote_data_source.dart';
import 'data/repositories/disbursement_repository_impl.dart';
import 'domain/repositories/disbursement_repository.dart';
import 'domain/usecases/disbursement_usecase.dart';
import 'presentation/blocs/bank_withdraw/bank_withdraw_bloc.dart';
import 'presentation/blocs/disbursement/disbursement_bloc.dart';

void initDisbursementInjector() {
  // Bloc
  sl.registerFactory(
      () => BankWithdrawBloc(getBankList: sl(), getAllFee: sl()));
  sl.registerFactory(() => DisbursementBloc(
      requestWithdraw: sl(),
      requestDividen: sl(),
      requestDividenToWallet: sl()));

  //! Global Usecase
  sl.registerLazySingleton(() => GetBankList(sl()));
  sl.registerLazySingleton(() => RequestWithdraw(sl()));
  sl.registerLazySingleton(() => RequestDividen(sl()));
  sl.registerLazySingleton(() => RequestDividenToWallet(sl()));

  //! Global Repository
  sl.registerLazySingleton<DisbursementRepository>(
    () => DisbursementRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<DisbursementRemoteDataSource>(
    () => DisbursementRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
