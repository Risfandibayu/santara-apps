import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../data/models/bank_list_model.dart';
import '../../data/models/submit_dividen_model.dart';
import '../../data/models/submit_dividen_to_wallet_model.dart';
import '../../data/models/submit_withdraw_model.dart';

abstract class DisbursementRepository {
  Future<Either<Failure, List<BankListModel>>> getBankList();
  Future<Either<Failure, bool>> requestWithdraw(SubmitWithdrawModel body);
  Future<Either<Failure, bool>> requestDividen(SubmitDividenModel body);
  Future<Either<Failure, bool>> requestDividenToWallet(
      SubmitDividenToWalletModel body);
}
