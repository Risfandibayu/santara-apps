import 'package:dartz/dartz.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_model.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_to_wallet_model.dart';
import 'package:santaraapp/features/payment/data/models/submit_deposit_model.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/bank_list_model.dart';
import '../../data/models/submit_withdraw_model.dart';
import '../repositories/disbursement_repository.dart';

class GetBankList implements UseCase<List<BankListModel>, NoParams> {
  final DisbursementRepository repository;
  GetBankList(this.repository);

  @override
  Future<Either<Failure, List<BankListModel>>> call(NoParams params) async {
    return await repository.getBankList();
  }
}

class RequestWithdraw implements UseCase<bool, SubmitWithdrawModel> {
  final DisbursementRepository repository;
  RequestWithdraw(this.repository);

  @override
  Future<Either<Failure, bool>> call(SubmitWithdrawModel body) async {
    return await repository.requestWithdraw(body);
  }
}

class RequestDividen implements UseCase<bool, SubmitDividenModel> {
  final DisbursementRepository repository;
  RequestDividen(this.repository);

  @override
  Future<Either<Failure, bool>> call(SubmitDividenModel body) async {
    return await repository.requestDividen(body);
  }
}

class RequestDividenToWallet
    implements UseCase<bool, SubmitDividenToWalletModel> {
  final DisbursementRepository repository;
  RequestDividenToWallet(this.repository);

  @override
  Future<Either<Failure, bool>> call(SubmitDividenToWalletModel body) async {
    return await repository.requestDividenToWallet(body);
  }
}
