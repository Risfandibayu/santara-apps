part of 'disbursement_bloc.dart';

abstract class DisbursementState extends Equatable {
  const DisbursementState();

  @override
  List<Object> get props => [];
}

class DisbursementInitial extends DisbursementState {}

class DisbursementLoading extends DisbursementState {}

class DisbursementSuccess extends DisbursementState {}

class DisbursementFailed extends DisbursementState {
  final String message;

  DisbursementFailed({this.message});
}
