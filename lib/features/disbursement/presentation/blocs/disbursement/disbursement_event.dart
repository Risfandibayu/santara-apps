part of 'disbursement_bloc.dart';

abstract class DisbursementEvent extends Equatable {
  const DisbursementEvent();

  @override
  List<Object> get props => [];
}

class PostWithdraw extends DisbursementEvent {
  final SubmitWithdrawModel body;

  PostWithdraw({this.body});
}

class PostDividen extends DisbursementEvent {
  final SubmitDividenModel body;

  PostDividen({this.body});
}

class PostDividenToWallet extends DisbursementEvent {
  final SubmitDividenToWalletModel body;

  PostDividenToWallet({this.body});
}
