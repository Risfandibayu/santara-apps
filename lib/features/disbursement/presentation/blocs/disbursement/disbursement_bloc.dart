import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_model.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_to_wallet_model.dart';

import '../../../../../core/utils/tools/logger.dart';
import '../../../data/models/submit_withdraw_model.dart';
import '../../../domain/usecases/disbursement_usecase.dart';

part 'disbursement_event.dart';
part 'disbursement_state.dart';

class DisbursementBloc extends Bloc<DisbursementEvent, DisbursementState> {
  final RequestWithdraw _requestWithdraw;
  final RequestDividen _requestDividen;
  final RequestDividenToWallet _requestDividenToWallet;
  DisbursementBloc(
      {@required RequestWithdraw requestWithdraw,
      @required RequestDividen requestDividen,
      @required RequestDividenToWallet requestDividenToWallet})
      : assert(requestWithdraw != null),
        assert(requestDividen != null),
        assert(requestDividenToWallet != null),
        _requestWithdraw = requestWithdraw,
        _requestDividen = requestDividen,
        _requestDividenToWallet = requestDividenToWallet,
        super(DisbursementInitial());

  @override
  Stream<DisbursementState> mapEventToState(
    DisbursementEvent event,
  ) async* {
    if (event is PostWithdraw) {
      yield* _submitWithdrawToState(event);
    }
    if (event is PostDividen) {
      yield* _postDividenToState(event);
    }
    if (event is PostDividenToWallet) {
      yield* _postDividenToWalletToState(event);
    }
  }

  Stream<DisbursementState> _submitWithdrawToState(PostWithdraw event) async* {
    try {
      yield DisbursementLoading();
      final result = await _requestWithdraw(event.body);

      yield* result.fold(
        (failure) async* {
          yield DisbursementFailed(message: failure.message);
        },
        (submit) async* {
          if (submit) {
            yield DisbursementSuccess();
          } else {
            yield DisbursementFailed(message: "Gagal mengajukan penarikan");
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield DisbursementFailed(message: e.message);
    }
  }

  Stream<DisbursementState> _postDividenToState(PostDividen event) async* {
    try {
      yield DisbursementLoading();
      final result = await _requestDividen(event.body);

      yield* result.fold(
        (failure) async* {
          yield DisbursementFailed(message: failure.message);
        },
        (submit) async* {
          if (submit) {
            yield DisbursementSuccess();
          } else {
            yield DisbursementFailed(message: "Gagal mengajukan penarikan");
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield DisbursementFailed(message: e.message);
    }
  }

  Stream<DisbursementState> _postDividenToWalletToState(
      PostDividenToWallet event) async* {
    try {
      yield DisbursementLoading();
      final result = await _requestDividenToWallet(event.body);

      yield* result.fold(
        (failure) async* {
          yield DisbursementFailed(message: failure.message);
        },
        (submit) async* {
          if (submit) {
            yield DisbursementSuccess();
          } else {
            yield DisbursementFailed(message: "Gagal mengajukan penarikan");
          }
        },
      );
    } catch (e, stack) {
      santaraLog(e, stack);
      yield DisbursementFailed(message: e.message);
    }
  }
}
