part of 'bank_withdraw_bloc.dart';

abstract class BankWithdrawState extends Equatable {
  const BankWithdrawState();

  @override
  List<Object> get props => [];
}

class BankWithdrawInitial extends BankWithdrawState {}

class BankWithdrawLoading extends BankWithdrawState {}

class BankWithdrawLoaded extends BankWithdrawState {
  final List<BankListModel> bankList;
  final FeeModel fee;

  BankWithdrawLoaded({this.bankList, this.fee});
}

class BankWithdrawEmpty extends BankWithdrawState {}

class BankWithdrawError extends BankWithdrawState {}
