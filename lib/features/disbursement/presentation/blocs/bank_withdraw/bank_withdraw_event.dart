part of 'bank_withdraw_bloc.dart';

abstract class BankWithdrawEvent extends Equatable {
  const BankWithdrawEvent();

  @override
  List<Object> get props => [];
}

class LoadBankList extends BankWithdrawEvent {}
