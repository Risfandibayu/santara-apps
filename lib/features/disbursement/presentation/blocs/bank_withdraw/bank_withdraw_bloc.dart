import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data/models/fee_model.dart';
import '../../../../../core/domain/usecases/fee_usecase.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../../core/utils/tools/logger.dart';
import '../../../data/models/bank_list_model.dart';
import '../../../domain/usecases/disbursement_usecase.dart';

part 'bank_withdraw_event.dart';
part 'bank_withdraw_state.dart';

class BankWithdrawBloc extends Bloc<BankWithdrawEvent, BankWithdrawState> {
  final GetBankList _getBankList;
  final GetAllFee _getAllFee;

  BankWithdrawBloc(
      {@required GetBankList getBankList, @required GetAllFee getAllFee})
      : assert(getBankList != null),
        assert(getAllFee != null),
        _getBankList = getBankList,
        _getAllFee = getAllFee,
        super(BankWithdrawInitial());

  @override
  Stream<BankWithdrawState> mapEventToState(
    BankWithdrawEvent event,
  ) async* {
    if (event is LoadBankList) {
      try {
        yield BankWithdrawLoading();
        final result = await _getBankList(NoParams());
        final allFee = await _getAllFee(NoParams());

        yield* result.fold(
          (failure) async* {
            yield BankWithdrawError();
          },
          (bankList) async* {
            if (bankList != null) {
              yield* allFee.fold(
                (failure) async* {
                  yield BankWithdrawError();
                },
                (fee) async* {
                  if (fee != null) {
                    yield BankWithdrawLoaded(bankList: bankList, fee: fee);
                  } else {
                    yield BankWithdrawError();
                  }
                },
              );
            } else {
              yield BankWithdrawError();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield BankWithdrawError();
      }
    }
  }
}
