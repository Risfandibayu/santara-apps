import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import '../../../../core/data/models/fee_model.dart';
import '../../../../core/data/models/user_model.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../widget/security_token/pin/SecurityPinUI.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../../../../widget/widget/components/main/SantaraField.dart';
import '../../data/models/bank_list_model.dart';
import '../../data/models/submit_dividen_model.dart';
import '../../data/models/submit_withdraw_model.dart';
import '../blocs/bank_withdraw/bank_withdraw_bloc.dart';
import '../blocs/disbursement/disbursement_bloc.dart';
import '../widgets/disbursement_bottom_sheet.dart';

class FormAccountReceiver extends StatefulWidget {
  final DisbursementType type;
  final User user;
  final FeeModel fee;
  final num total;

  FormAccountReceiver({Key key, this.type, this.user, this.fee, this.total})
      : super(key: key);

  @override
  _FormAccountReceiverState createState() => _FormAccountReceiverState();
}

class _FormAccountReceiverState extends State<FormAccountReceiver> {
  final _formKey = GlobalKey<FormState>();
  var account = TextEditingController();
  var kota = TextEditingController();
  var cabang = TextEditingController();
  var name = TextEditingController();
  String _selectedBank;
  DisbursementBloc disbursementBloc;

  void _inputPin() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => SecurityPinUI(
          type: SecurityType.check,
          title: "Masukan PIN Anda",
          onSuccess: (pin, finger) {
            Navigator.pop(context);
            Navigator.pop(context);
            Navigator.pop(context);
            if (widget.type == DisbursementType.withdraw) {
              disbursementBloc.add(
                PostWithdraw(
                  body: SubmitWithdrawModel(
                    accountName: widget.user.trader.name,
                    accountNumber: account.text,
                    bankTo: _selectedBank,
                    amount: widget.total.toString(),
                    channel: "",
                    pin: pin,
                    finger: finger,
                  ),
                ),
              );
            } else {
              disbursementBloc.add(
                PostDividen(
                  body: SubmitDividenModel(
                    bank: _selectedBank,
                    accountName: widget.user.trader.name,
                    accountNumber: account.text,
                    bankKota: kota.text,
                    bankCabang: cabang.text,
                    channel: "",
                    pin: pin,
                    finger: finger,
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    disbursementBloc = BlocProvider.of<DisbursementBloc>(context);
    name.text = widget.user.trader.name;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Masukan Rekening Bank Penerima",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: Sizes.s20),
          children: [
            SizedBox(height: Sizes.s20),
            BlocBuilder<BankWithdrawBloc, BankWithdrawState>(
              builder: (context, state) {
                if (state is BankWithdrawLoaded) {
                  return Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        border: Border.all(
                            width: 1.5,
                            color: _selectedBank == null
                                ? Colors.red[700]
                                : Color(0xffDDDDDD))),
                    child: SearchableDropdown(
                      key: Key('bank_penerima'),
                      icon: Icon(Icons.arrow_drop_down),
                      items: state.bankList.map((BankListModel e) {
                        return DropdownMenuItem(value: e, child: Text(e.bank));
                      }).toList(),
                      underline: Container(),
                      value: _selectedBank == null ? '' : _selectedBank,
                      hint: Text("Nama Bank Penerima"),
                      searchHint: Text("Masukan Nama Bank"),
                      onChanged: (BankListModel selected) {
                        setState(() {
                          _selectedBank = selected.bank;
                        });
                      },
                      isExpanded: true,
                      searchFn: (String keyword, items) {
                        List<int> ret = List<int>();
                        if (keyword != null &&
                            items != null &&
                            keyword.isNotEmpty) {
                          keyword.split(" ").forEach((k) {
                            int i = 0;
                            items.forEach((item) {
                              if (k.isNotEmpty &&
                                  (item.value.bank
                                      .toString()
                                      .toLowerCase()
                                      .contains(k.toLowerCase()))) {
                                ret.add(i);
                              }
                              i++;
                            });
                          });
                        }
                        if (keyword.isEmpty) {
                          ret = Iterable<int>.generate(items.length).toList();
                        }
                        return (ret);
                      },
                    ),
                  );
                } else {
                  return SantaraTextField(
                    hintText: "",
                    labelText: "",
                    icon: CupertinoActivityIndicator(),
                    enabled: false,
                  );
                }
              },
            ),
            _selectedBank == null
                ? Container(
                    margin: EdgeInsets.only(left: 10, top: 4),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Nama Bank Penerima tidak boleh kosong!",
                        style: TextStyle(
                          color: Colors.red[700],
                          fontSize: 12,
                        ),
                      ),
                    ),
                  )
                : Container(),
            //
            widget.type == DisbursementType.dividen
                ? SizedBox(height: Sizes.s20)
                : Container(),
            widget.type == DisbursementType.dividen
                ? SantaraTextField(
                    controller: kota,
                    hintText: "Kota Bank",
                    labelText: "Kota Bank",
                    inputType: TextInputType.text,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Masukkan Kota Bank';
                      } else {
                        return null;
                      }
                    },
                  )
                : Container(),
            widget.type == DisbursementType.dividen
                ? SizedBox(height: Sizes.s20)
                : Container(),
            widget.type == DisbursementType.dividen
                ? SantaraTextField(
                    controller: cabang,
                    hintText: "Cabang Bank",
                    labelText: "Cabang Bank",
                    inputType: TextInputType.text,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Masukkan Cabang Bank';
                      } else {
                        return null;
                      }
                    },
                  )
                : Container(),
            SizedBox(height: Sizes.s20),
            SantaraTextField(
              controller: account,
              hintText: "Nomor Rekening Bank",
              labelText: "Nomor Rekening Bank",
              inputType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Masukkan Nomor Rekening';
                } else if (!RegExp(r"^[0-9]*$").hasMatch(value)) {
                  return 'Tidak boleh ada karakter';
                } else {
                  return null;
                }
              },
            ),
            SizedBox(height: Sizes.s20),
            SantaraTextField(
              controller: name,
              enabled: false,
              hintText: "Nama Pemilik Rekening",
              labelText: "Nama Pemilik Rekening",
            ),
            SizedBox(height: Sizes.s20),
            SantaraMainButton(
              title: "Tarik Saldo",
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  if (_selectedBank != null) {
                    DisbursementBottomSheet.confirmForm(
                        context: context,
                        type: widget.type,
                        total: widget.total,
                        user: widget.user,
                        fee: widget.fee,
                        method: DisbursementMethod.rekening,
                        bankName: _selectedBank,
                        accountNumber: account.text,
                        cabangBank: cabang.text,
                        kotaBank: kota.text,
                        onContinue: _inputPin);
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
