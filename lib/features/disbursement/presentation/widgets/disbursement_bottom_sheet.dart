import 'package:flutter/material.dart';

import '../../../../core/data/models/fee_model.dart';
import '../../../../core/data/models/user_model.dart';
import '../../../../core/utils/formatter/currency_formatter.dart';
import '../../../../core/utils/ui/screen_sizes.dart';
import '../../../../helpers/PopupHelper.dart';
import '../../../../widget/security_token/pin/SecurityPinUI.dart';
import '../../../../widget/widget/components/main/SantaraButtons.dart';
import '../../../dana/presentation/bloc/auth_dana/auth_dana_bloc.dart';
import '../../../dana/presentation/widgets/dana_bottom_sheet.dart';
import '../../data/models/submit_dividen_model.dart';
import '../../data/models/submit_dividen_to_wallet_model.dart';
import '../../data/models/submit_withdraw_model.dart';
import '../blocs/disbursement/disbursement_bloc.dart';
import '../pages/form_account_receiver.dart';

enum DisbursementType { withdraw, dividen }

enum DisbursementMethod { dana, rekening, wallet }

class DisbursementBottomSheet {
  static Widget _headerBottomSheet(BuildContext context, String title) {
    return Row(children: [
      GestureDetector(
        child: Icon(
          Icons.close,
          color: Colors.black,
        ),
        onTap: () => Navigator.pop(context),
      ),
      Container(width: 10),
      Text(
        title,
        style: TextStyle(
            fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    ]);
  }

  static Widget _method(BuildContext context, String icon, String name,
      bool isActive, bool isDanaActive, Function onTap,
      {Widget subtitle, AuthDanaBloc bloc}) {
    return ListTile(
      onTap: isActive
          ? onTap
          : name == "DANA"
          ? () {
        if (isDanaActive) {
          DanaBottomSheet.confirmConnectToDana(context, () {
            Navigator.pop(context);
            bloc.add(LoginDana());
          });
        } else {
          PopupHelper.warningInfo(
            context,
            "Fitur DANA belum tersedia",
          );
        }
      }
          : null,
      leading: Image.asset(
        icon,
        height: name == "Rekening Bank" ? 20 : 25,
        fit: BoxFit.fitHeight,
        color:
        !isActive && name == "DANA" && !isDanaActive ? Colors.grey : null,
      ),
      title: Text(
        name,
        style: TextStyle(
          color: Colors.black,
          fontSize: FontSize.s14,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: subtitle,
      trailing: isActive
          ? Icon(
        Icons.chevron_right,
        color: Color(0xFFCCCCCC),
      )
          : name == "DANA"
          ? Text(
        "Aktivasi",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: !isActive && name == "DANA" && !isDanaActive
                ? Colors.grey
                : Color(0xFF008CEB)),
      )
          : null,
    );
  }

  static Widget _danaLimit(BuildContext context) {
    return ListTile(
      onTap: () => tncDisbursementToDana(context),
      leading: Image.asset(
        "assets/dana/dana.png",
        height: 25,
        fit: BoxFit.fitHeight,
      ),
      title: Text(
        "DANA",
        style: TextStyle(
          color: Colors.black,
          fontSize: FontSize.s14,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        "Akun DANA mencapai limit",
        style: TextStyle(fontSize: 12, color: Colors.grey),
      ),
      trailing: Container(
        padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: Border.all(width: 1, color: Color(0xFF108EE9))),
        child: Text(
          "Pelajari",
          style: TextStyle(color: Color(0xFF108EE9), fontSize: 12),
        ),
      ),
    );
  }

  static Widget _itemConfirm(String title, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: Sizes.s4),
      child: Row(
        children: [
          Expanded(child: Text(title)),
          Text(" : "),
          Expanded(
            child: Text(
              value,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }

  static Widget _listOfNumber(String list, String content) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(list),
        Container(width: 4),
        Expanded(child: Text(content)),
      ],
    );
  }

  static void _inputPin(
      BuildContext context, Function(String pin, bool finger) onSuccess) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => SecurityPinUI(
          type: SecurityType.check,
          title: "Masukan PIN Anda",
          onSuccess: (pin, finger) {
            Navigator.pop(context);
            onSuccess(pin, finger);
          },
        ),
      ),
    );
  }

  static void chooseMethod({
    BuildContext context,
    DisbursementType type,
    num saldoDompet,
    num saldoDana,
    num total,
    String danaKyc,
    bool isDanaActive,
    User user,
    FeeModel fee,
    DisbursementBloc bloc,
    AuthDanaBloc authDanaBloc,
    String accountName,
    String accountNumber,
    String bankName
  }) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 20),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            // title
            _headerBottomSheet(
                context,
                type == DisbursementType.withdraw
                    ? "Pilih Metode Pencairan Dana"
                    : "Pilih Metode Pencairan Dividen"),
            SizedBox(height: Sizes.s16),

            // coution
            type == DisbursementType.withdraw
                ? Text(
              "Penarikan Rupiah diproses dengan jaringan LLG (Kliring) atau RTGS. Dana akan masuk ke rekening Anda maksimal tiga hari kerja bank.",
              style: TextStyle(
                color: Color(0xFF747474),
                fontSize: FontSize.s14,
              ),
            )
                : Container(),
            type == DisbursementType.withdraw
                ? SizedBox(height: Sizes.s16)
                : Container(),

            // method
            // wallet
            type == DisbursementType.withdraw
                ? Container()
                : _method(context, "assets/icon_menu/new/Deposit.png", "Dompet",
                true, true, () {
                  Navigator.pop(context);
                  confirm(
                      context: context,
                      type: type,
                      total: total,
                      user: user,
                      fee: fee,
                      method: DisbursementMethod.wallet,
                      onContinue: () {
                        _inputPin(context, (pin, finger) {
                          Navigator.pop(context);
                          if (type == DisbursementType.dividen) {
                            bloc.add(PostDividenToWallet(
                                body: SubmitDividenToWalletModel(
                                  pin: pin,
                                  finger: finger,
                                )));
                          }
                        });
                      });
                },
                subtitle: Text(
                  CurrencyFormatter.format(saldoDompet),
                  style: TextStyle(
                      color: Color(0xFF434343), fontSize: FontSize.s12),
                )),

            // dana
            // danaKyc == "22"
            // saldo dana + total penarikan - fee
            // ? ((saldoDana == null ? 0 : saldoDana) +
            //                 (total == null ? 0 : total)) -
            //             (type == DisbursementType.withdraw
            //                 ? fee.withdraw.dana
            //                 : fee.dividend.dana) >
            //         10000000
            //     ? _danaLimit(context)
            //     : _method(context, "assets/dana/dana.png", "DANA",
            //         user.isDana == 1, isDanaActive, () {
            //         Navigator.pop(context);
            //         confirm(
            //             context: context,
            //             type: type,
            //             total: total,
            //             user: user,
            //             fee: fee,
            //             method: DisbursementMethod.dana,
            //             onContinue: () {
            //               _inputPin(context, (pin, finger) {
            //                 Navigator.pop(context);
            //                 if (type == DisbursementType.withdraw) {
            //                   bloc.add(PostWithdraw(
            //                       body: SubmitWithdrawModel(
            //                     accountName: user.trader.name,
            //                     accountNumber: user.phone,
            //                     amount: total.toString(),
            //                     bankTo: "DANA",
            //                     channel: "DANA",
            //                     pin: pin,
            //                     finger: finger,
            //                   )));
            //                 } else {
            //                   bloc.add(PostDividen(
            //                       body: SubmitDividenModel(
            //                     accountName: user.trader.name,
            //                     accountNumber: user.phone,
            //                     bank: "DANA",
            //                     channel: "DANA",
            //                     bankCabang: "",
            //                     bankKota: "",
            //                     pin: pin,
            //                     finger: finger,
            //                   )));
            //                 }
            //               });
            //             });
            //       },
            //         subtitle: Text(
            //           user.isDana == 1
            //               ? CurrencyFormatter.format(saldoDana)
            //               : "-",
            //           style: TextStyle(
            //               color: Color(0xFF434343), fontSize: FontSize.s12),
            //         ),
            //         bloc: authDanaBloc)
            // saldo dana + total penarikan - fee
            // : ((saldoDana == null ? 0 : saldoDana) +
            //                 (total == null ? 0 : total)) -
            //             (type == DisbursementType.withdraw
            //                 ? fee.withdraw.dana
            //                 : fee.dividend.dana) >
            //         2000000
            //     ? _danaLimit(context)
            //     : _method(context, "assets/dana/dana.png", "DANA",
            //         user.isDana == 1, isDanaActive, () {
            //         Navigator.pop(context);
            //         confirm(
            //             context: context,
            //             type: type,
            //             total: total,
            //             user: user,
            //             fee: fee,
            //             method: DisbursementMethod.dana,
            //             onContinue: () {
            //               _inputPin(context, (pin, finger) {
            //                 Navigator.pop(context);
            //                 if (type == DisbursementType.withdraw) {
            //                   bloc.add(PostWithdraw(
            //                       body: SubmitWithdrawModel(
            //                     accountName: user.trader.name,
            //                     accountNumber: user.phone,
            //                     amount: total.toString(),
            //                     bankTo: "DANA",
            //                     channel: "DANA",
            //                     pin: pin,
            //                     finger: finger,
            //                   )));
            //                 } else {
            //                   bloc.add(PostDividen(
            //                       body: SubmitDividenModel(
            //                     accountName: user.trader.name,
            //                     accountNumber: user.phone,
            //                     bank: "DANA",
            //                     channel: "DANA",
            //                     bankCabang: "",
            //                     bankKota: "",
            //                     pin: pin,
            //                     finger: finger,
            //                   )));
            //                 }
            //               });
            //             });
            //       },
            //         subtitle: Text(
            //           user.isDana == 1
            //               ? CurrencyFormatter.format(saldoDana)
            //               : "-",
            //           style: TextStyle(
            //               color: Color(0xFF434343), fontSize: FontSize.s12),
            //         ),
            //         bloc: authDanaBloc),
            //
            // rekening bank
            _method(context, "assets/icon/rekening_bank.png", "$bankName - $accountNumber\n$accountName",
                true, true, () {
                  PopupHelper.warningInfo(
                    context,
                    "Fitur ini sedang ditutup",
                  );
                  // Navigator.pop(context);
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) => FormAccountReceiver(
                  //             type: type, user: user, fee: fee, total: total)));
                }),
            Container(
              height: 1,
              color: Color(0xFFF5F5F5),
            ),
            Container(
              height: 58,
              padding: EdgeInsets.symmetric(horizontal: Sizes.s15),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total Pencairan",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s15,
                      ),
                    ),
                    Container(width: 10),
                    Text(
                      CurrencyFormatter.format(total),
                      style: TextStyle(
                        color: Color(0xFF0E7E4A),
                        fontWeight: FontWeight.bold,
                        fontSize: FontSize.s15,
                      ),
                    ),
                  ]),
            ),
          ]),
        );
      },
    );
  }

  static void confirm(
      {BuildContext context,
        DisbursementType type,
        num total,
        User user,
        FeeModel fee,
        DisbursementMethod method,
        Function onContinue}) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Konfirmasi Penarikan",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(height: Sizes.s16),
              _itemConfirm("Jumlah Penarikan", CurrencyFormatter.format(total)),
              _itemConfirm(
                  "Biaya Admin",
                  CurrencyFormatter.format(type == DisbursementType.withdraw
                      ? method == DisbursementMethod.dana
                      ? fee.withdraw.dana
                      : fee.withdraw.bank
                      : method == DisbursementMethod.dana
                      ? fee.dividend.dana
                      : fee.dividend.bank)),
              _itemConfirm(
                  "Terima Bersih",
                  CurrencyFormatter.format(total -
                      (type == DisbursementType.withdraw
                          ? method == DisbursementMethod.dana
                          ? fee.withdraw.dana
                          : fee.withdraw.bank
                          : method == DisbursementMethod.dana
                          ? fee.dividend.dana
                          : fee.dividend.bank))),
              _itemConfirm("Nama Penerima", user.trader.name),
              method == DisbursementMethod.dana
                  ? _itemConfirm("Nama Akun Penerima", "DANA")
                  : Container(),
              _itemConfirm("Nomor Telp. Penerima", user.phone),
              SizedBox(height: Sizes.s16),
              Row(
                children: [
                  Expanded(
                    child: SantaraOutlineButton(
                      key: Key("cancel_confirm"),
                      title: "Kembali",
                      color: Color(0xFF0E7E4A),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  Container(width: 10),
                  Expanded(
                    child: SantaraMainButton(
                      key: Key("continue_confirm"),
                      title: "Lanjutkan",
                      color: Color(0xFF0E7E4A),
                      onPressed: onContinue,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  static void confirmForm(
      {BuildContext context,
        DisbursementType type,
        num total,
        User user,
        String bankName,
        String accountNumber,
        String kotaBank,
        String cabangBank,
        FeeModel fee,
        DisbursementMethod method,
        Function onContinue}) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Konfirmasi Penarikan",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(height: Sizes.s16),
              _itemConfirm("Jumlah Penarikan", CurrencyFormatter.format(total)),
              _itemConfirm(
                  "Biaya Admin SANTARA",
                  CurrencyFormatter.format(type == DisbursementType.withdraw
                      ? fee.withdraw.bank
                      : fee.dividend.bank)),
              _itemConfirm(
                  "Terima Bersih",
                  CurrencyFormatter.format(total -
                      (type == DisbursementType.withdraw
                          ? fee.withdraw.bank
                          : fee.dividend.bank))),
              _itemConfirm("Nama Penerima", user.trader.name),
              _itemConfirm("Nama Bank", bankName),
              _itemConfirm("Nomor Rekening", accountNumber),
              type == DisbursementType.withdraw
                  ? Container()
                  : _itemConfirm("Cabang Bank", cabangBank),
              type == DisbursementType.withdraw
                  ? Container()
                  : _itemConfirm("Kota Bank", kotaBank),
              SizedBox(height: Sizes.s16),
              Row(
                children: [
                  Expanded(
                    child: SantaraOutlineButton(
                      key: Key("cancel_confirm"),
                      title: "Kembali",
                      color: Color(0xFF0E7E4A),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  Container(width: 10),
                  Expanded(
                    child: SantaraMainButton(
                      key: Key("continue_confirm"),
                      title: "Lanjutkan",
                      color: Color(0xFF0E7E4A),
                      onPressed: onContinue,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  static void disbursementSuccess(
      {BuildContext context,
        String title,
        String subtitle,
        Function onPressed}) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Padding(
            padding: EdgeInsets.fromLTRB(15, 30, 15, 30),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  "assets/icon/disbursement_success.png",
                  width: MediaQuery.of(context).size.width / 2,
                  height: MediaQuery.of(context).size.width / 2,
                ),
                SizedBox(height: Sizes.s20),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
                SizedBox(height: Sizes.s10),
                Text(
                  subtitle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: Sizes.s20),
                SantaraMainButton(
                  title: "Mengerti",
                  onPressed: onPressed,
                )
              ],
            ),
          ),
        );
      },
    );
  }

  static void tncDisbursementToDana(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Sizes.s8),
          topRight: Radius.circular(Sizes.s8),
        ),
      ),
      builder: (builder) {
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 30, 15, 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _headerBottomSheet(context, "Potongan Syarat dan Ketentuan"),
              SizedBox(height: Sizes.s20),
              Image.asset(
                "assets/dana/dana_vertical.png",
                height: 25,
                fit: BoxFit.fitHeight,
              ),
              SizedBox(height: Sizes.s15),
              _listOfNumber("1",
                  "Pengguna dapat melakukan Transaksi apapun apabila memiliki Saldo DANA yang mencukupi. Jumlah Saldo DANA dibatasi sebesar:"),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: _listOfNumber("a.",
                    "Rp2.000.000 (dua juta Rupiah) untuk Akun DANA yang dimiliki oleh Pengguna DANA Tidak Terverifikasi;"),
              ),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: _listOfNumber("b.",
                    "Rp10.000.000 (sepuluh juta Rupiah) untuk Akun Premium; atau"),
              ),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: _listOfNumber("c.",
                    "Jumlah lainnya sebagaimana ditentukan dari waktu ke waktu sesuai dengan peraturan perundang-undangan yang berlaku."),
              ),
              _listOfNumber("2.",
                  "Batas maksimum Transaksi yang bersifat incoming (masuk) dalam 1 (satu) Akun DANA pada 1 (satu) bulan kalender adalah Rp20.000.000 (dua puluh juta Rupiah)."),
              _listOfNumber("3.",
                  "EDIK dapat melakukan penolakan Transaksi jika sewaktu-waktu sistem keamanan DANA menganggap bahwa Transaksi yang dilakukan tidak wajar."),
              SizedBox(height: Sizes.s20),
              SantaraMainButton(
                title: "Mengerti",
                color: Color(0xFF118EEA),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        );
      },
    );
  }
}
