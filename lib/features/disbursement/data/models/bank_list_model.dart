import 'dart:convert';

List<BankListModel> bankListModelFromJson(String str) =>
    List<BankListModel>.from(
        json.decode(str).map((x) => BankListModel.fromJson(x)));

String bankListModelToJson(List<BankListModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BankListModel {
  BankListModel({
    this.bankCode,
    this.bank,
  });

  String bankCode;
  String bank;

  factory BankListModel.fromJson(Map<String, dynamic> json) => BankListModel(
        bankCode: json["bank_code"],
        bank: json["bank"],
      );

  Map<String, dynamic> toJson() => {
        "bank_code": bankCode,
        "bank": bank,
      };
}
