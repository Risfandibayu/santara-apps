import 'dart:convert';

SubmitDividenModel submitDividenModelFromJson(String str) =>
    SubmitDividenModel.fromJson(json.decode(str));

String submitDividenModelToJson(SubmitDividenModel data) =>
    json.encode(data.toJson());

class SubmitDividenModel {
  SubmitDividenModel({
    this.bank,
    this.accountName,
    this.accountNumber,
    this.bankKota,
    this.bankCabang,
    this.channel,
    this.pin,
    this.finger,
  });

  String bank;
  String accountName;
  String accountNumber;
  String bankKota;
  String bankCabang;
  String channel;
  String pin;
  bool finger;

  factory SubmitDividenModel.fromJson(Map<String, dynamic> json) =>
      SubmitDividenModel(
        bank: json["bank"],
        accountName: json["account_name"],
        accountNumber: json["account_number"],
        bankKota: json["bank_kota"],
        bankCabang: json["bank_cabang"],
        channel: json["channel"],
        pin: json["pin"],
        finger: json["finger"],
      );

  Map<String, dynamic> toJson() => {
        "bank": bank,
        "account_name": accountName,
        "account_number": accountNumber,
        "bank_kota": bankKota,
        "bank_cabang": bankCabang,
        "channel": channel,
        "pin": pin,
        "finger": finger,
      };
}
