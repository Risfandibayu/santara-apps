import 'dart:convert';

SubmitWithdrawModel submitWithdrawModelFromJson(String str) =>
    SubmitWithdrawModel.fromJson(json.decode(str));

String submitWithdrawModelToJson(SubmitWithdrawModel data) =>
    json.encode(data.toJson());

class SubmitWithdrawModel {
  SubmitWithdrawModel({
    this.accountName,
    this.accountNumber,
    this.amount,
    this.bankTo,
    this.channel,
    this.pin,
    this.finger,
  });

  String accountName;
  String accountNumber;
  String amount;
  String bankTo;
  String channel;
  String pin;
  bool finger;

  factory SubmitWithdrawModel.fromJson(Map<String, dynamic> json) =>
      SubmitWithdrawModel(
        accountName: json["account_name"],
        accountNumber: json["account_number"],
        amount: json["amount"],
        bankTo: json["bank_to"],
        channel: json["channel"],
        pin: json["pin"],
        finger: json["finger"],
      );

  Map<String, dynamic> toJson() => {
        "account_name": accountName,
        "account_number": accountNumber,
        "amount": amount,
        "bank_to": bankTo,
        "channel": channel,
        "pin": pin,
        "finger": finger,
      };
}
