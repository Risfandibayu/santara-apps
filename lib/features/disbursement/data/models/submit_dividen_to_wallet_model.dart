import 'dart:convert';

SubmitDividenToWalletModel submitDividenToWalletModelFromJson(String str) =>
    SubmitDividenToWalletModel.fromJson(json.decode(str));

String submitDividenToWalletModelToJson(SubmitDividenToWalletModel data) =>
    json.encode(data.toJson());

class SubmitDividenToWalletModel {
  SubmitDividenToWalletModel({
    this.pin,
    this.finger,
  });

  String pin;
  bool finger;

  factory SubmitDividenToWalletModel.fromJson(Map<String, dynamic> json) =>
      SubmitDividenToWalletModel(
        pin: json["pin"],
        finger: json["finger"],
      );

  Map<String, dynamic> toJson() => {
        "pin": pin,
        "finger": finger,
      };
}
