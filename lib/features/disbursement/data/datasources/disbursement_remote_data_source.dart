import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_model.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_to_wallet_model.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/rest_client.dart';
import '../../../../core/utils/helpers/rest_helper.dart';
import '../models/bank_list_model.dart';
import '../models/submit_withdraw_model.dart';

abstract class DisbursementRemoteDataSource {
  Future<List<BankListModel>> getBankList();
  Future<bool> requestWithdraw(SubmitWithdrawModel body);
  Future<bool> requestDividen(SubmitDividenModel body);
  Future<bool> requestDividenToWallet(SubmitDividenToWalletModel body);
}

class DisbursementRemoteDataSourceImpl extends DisbursementRemoteDataSource {
  final RestClient client;
  DisbursementRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<BankListModel>> getBankList() async {
    try {
      final result = await client.get(path: "/banks/bank-withdraw");
      if (result.statusCode == 200) {
        return bankListModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> requestWithdraw(SubmitWithdrawModel body) async {
    try {
      final result = await client.post(path: "/withdraw/", body: body.toJson());
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> requestDividen(SubmitDividenModel body) async {
    try {
      final result = await client.put(path: "/devidend", body: body.toJson());
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }

  @override
  Future<bool> requestDividenToWallet(SubmitDividenToWalletModel body) async {
    try {
      final result = await client.post(path: "/devidend", body: body.toJson());
      if (result.statusCode == 200) {
        return true;
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
