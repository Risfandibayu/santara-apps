import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_to_wallet_model.dart';
import 'package:santaraapp/features/disbursement/data/models/submit_dividen_model.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/http/network_info.dart';
import '../../domain/repositories/disbursement_repository.dart';
import '../datasources/disbursement_remote_data_source.dart';
import '../models/bank_list_model.dart';
import '../models/submit_withdraw_model.dart';

class DisbursementRepositoryImpl implements DisbursementRepository {
  final NetworkInfo networkInfo;
  final DisbursementRemoteDataSource remoteDataSource;

  DisbursementRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<BankListModel>>> getBankList() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getBankList();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> requestWithdraw(
      SubmitWithdrawModel body) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.requestWithdraw(body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> requestDividen(SubmitDividenModel body) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.requestDividen(body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }

  @override
  Future<Either<Failure, bool>> requestDividenToWallet(
      SubmitDividenToWalletModel body) async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.requestDividenToWallet(body);
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
