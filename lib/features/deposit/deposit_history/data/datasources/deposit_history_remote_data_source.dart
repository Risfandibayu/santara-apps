import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/http/rest_client.dart';
import '../../../../../core/utils/helpers/rest_helper.dart';
import '../models/deposit_history_model.dart';

abstract class DepositHistoryRemoteDataSource {
  Future<List<DepositHistoryModel>> getDepositHistory();
}

class DepositHistoryRemoteDataSourceImpl
    implements DepositHistoryRemoteDataSource {
  final RestClient client;
  DepositHistoryRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<DepositHistoryModel>> getDepositHistory() async {
    try {
      final result = await client.get(path: "/deposit/");
      if (result.statusCode == 200) {
        return depositHistoryModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
