import 'dart:convert';

List<DepositHistoryModel> depositHistoryModelFromJson(String str) =>
    new List<DepositHistoryModel>.from(
        json.decode(str).map((x) => DepositHistoryModel.fromJson(x)));

String depositHistoryModelToJson(List<DepositHistoryModel> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class DepositHistoryModel {
  int id;
  String uuid;
  int amount;
  int status;
  dynamic confirmationPhoto;
  int traderId;
  dynamic verifiedAt;
  dynamic verifiedBy;
  dynamic bankTo;
  String bankFrom;
  String accountNumber;
  String createdAt;
  String updatedAt;
  int isDeleted;
  String fee;
  String chanel;
  String createdBy;
  String updatedBy;
  String danaCheckoutUrl;
  VirtualAccountData virtualAccount;

  DepositHistoryModel({
    this.id,
    this.uuid,
    this.amount,
    this.status,
    this.confirmationPhoto,
    this.traderId,
    this.verifiedAt,
    this.verifiedBy,
    this.bankTo,
    this.bankFrom,
    this.accountNumber,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
    this.createdBy,
    this.updatedBy,
    this.fee,
    this.chanel,
    this.danaCheckoutUrl,
    this.virtualAccount,
  });

  factory DepositHistoryModel.fromJson(Map<String, dynamic> json) {
    var virtualAccount;
    if (json["virtual_account"] != null) {
      virtualAccount = VirtualAccountData.fromJson(json["virtual_account"]);
    }
    return new DepositHistoryModel(
      id: json["id"],
      uuid: json["uuid"],
      amount: json["amount"].toInt(),
      status: json["status"],
      confirmationPhoto: json["confirmation_photo"],
      traderId: json["trader_id"],
      verifiedAt: json["verified_at"],
      verifiedBy: json["verified_by"],
      bankTo: json["bank_to"],
      bankFrom: json["bank_from"],
      accountNumber: json["account_number"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      isDeleted: json["is_deleted"],
      createdBy: json["created_by"],
      updatedBy: json["updated_by"],
      fee: json["fee"],
      chanel: json["channel"],
      danaCheckoutUrl:
          json["dana_checkout_url"] == null ? "" : json["dana_checkout_url"],
      virtualAccount: virtualAccount,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "amount": amount,
        "status": status,
        "confirmation_photo": confirmationPhoto,
        "trader_id": traderId,
        "verified_at": verifiedAt,
        "verified_by": verifiedBy,
        "bank_to": bankTo,
        "bank_from": bankFrom,
        "account_number": accountNumber,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "fee": fee,
        "channel": chanel,
        "dana_checkout_url": danaCheckoutUrl,
        "virtual_account": virtualAccount.toJson(),
      };
}

class VirtualAccountData {
  String name;
  String merchantCode;
  String accountNumber;

  VirtualAccountData({
    this.name,
    this.merchantCode,
    this.accountNumber,
  });

  factory VirtualAccountData.fromJson(Map<String, dynamic> json) =>
      new VirtualAccountData(
        name: json["name"],
        merchantCode: json["merchant_code"],
        accountNumber: json["account_number"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "merchant_code": merchantCode,
        "account_number": accountNumber,
      };
}
