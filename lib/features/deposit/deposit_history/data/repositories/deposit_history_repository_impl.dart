import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/http/network_info.dart';
import '../../domain/repositories/deposit_history_repository.dart';
import '../datasources/deposit_history_remote_data_source.dart';
import '../models/deposit_history_model.dart';

class DepositHistoryRepositoryImpl implements DepositHistoryRepository {
  final NetworkInfo networkInfo;
  final DepositHistoryRemoteDataSource remoteDataSource;

  DepositHistoryRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, List<DepositHistoryModel>>> getDepositHistory() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getDepositHistory();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
