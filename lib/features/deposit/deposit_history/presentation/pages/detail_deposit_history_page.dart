import 'dart:io';

import 'package:camera/camera.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:santaraapp/core/usecases/unauthorize_usecase.dart';
import 'package:santaraapp/features/deposit/main/presentation/pages/main_deposit_page.dart';
import 'package:santaraapp/models/PaymentChannel.dart';
import 'package:santaraapp/pages/Home.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/widget/widget/Camera.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:http_parser/http_parser.dart';
import 'package:async/async.dart';

class DetailDepositHistoryPage extends StatefulWidget {
  final String id;
  final String uuid;
  final String chanel;
  final int amount;
  final String merchantCode;
  final String accountNumber;
  final String nameVa;
  final int fee;
  DetailDepositHistoryPage(
      {this.id,
      this.uuid,
      this.chanel,
      this.amount,
      this.merchantCode,
      this.accountNumber,
      this.nameVa,
      this.fee = 0});
  @override
  _DetailDepositHistoryPageState createState() =>
      _DetailDepositHistoryPageState();
}

class _DetailDepositHistoryPageState extends State<DetailDepositHistoryPage> {
  final rupiah = new NumberFormat("#,##0");
  final angka = new NumberFormat("#,##0");
  var isExpanded = false;
  var isLoading = false;
  File bukti;
  List<CameraDescription> cameras;
  bool isBack = true;

  Future getAvailableCamera() async {
    cameras = await availableCameras();
  }

  PaymentChannel paymentChannel;
  String bank;

  Future getPaymentChannel() async {
    final http.Response response = await http.get('$apiLocal/payment-channels');
    if (response.statusCode == 200) {
      setState(() {
        paymentChannel = paymentChannelFromJson(response.body);
        dropDownItem = getDropDownMenuItems(paymentChannel.bankTransfer);
      });
    } else {
      paymentChannel = null;
    }
  }

  List<DropdownMenuItem<String>> dropDownItem;
  List<DropdownMenuItem<String>> getDropDownMenuItems(
      List<BankTransfer> bankList) {
    List<DropdownMenuItem<String>> items = new List();
    bankList.map((value) {
      items.add(new DropdownMenuItem(
          value: value.provider.toString(), child: Text(value.provider)));
    }).toList();
    return items;
  }

  Future confirmation(
      BuildContext context, File photo, String idBank, String uuid) async {
    setState(() => isLoading = true);
    try {
      final storage = new FlutterSecureStorage();
      final token = await storage.read(key: 'token');
      var image = new http.ByteStream(DelegatingStream.typed(photo.openRead()));
      var imageLength = await photo.length();

      //sending
      final uri = Uri.parse('$apiLocal/deposit/confirmation');
      final request = new http.MultipartRequest("PUT", uri);
      request.headers[HttpHeaders.authorizationHeader] = 'Bearer $token';

      //convert
      var gambar = new http.MultipartFile(
          'confirmation_photo', image, imageLength,
          filename: path.basename(photo.path),
          contentType: new MediaType('image', 'png'));

      //adding items
      request.files.add(gambar);
      request.fields['bank_id'] = idBank;
      request.fields['uuid'] = uuid;
      var response = await request.send(); //.timeout(Duration(seconds: 20));
      if (response.statusCode == 200) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text('Konfirmasi pembayaran berhasil dilakukan'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      // Navigator.of(context).popUntil(ModalRoute.withName('/deposit'));
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => Home(index: 4)),
                          (Route<dynamic> route) => false);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => MainDepositPage(initialTab: 1)));
                    },
                  )
                ],
              );
            });
      } else if (response.statusCode == 500) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content:
                    Text('Server bermasalah, silahkan coba beberapa saat lagi'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else if (response.statusCode == 504) {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text(
                    'Koneksi bermasalah, silahkan coba beberapa saat lagi'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      } else {
        setState(() => isLoading = false);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content:
                    Text('Terjadi kesalahan, silahkan coba beberapa saat lagi'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      }
    } on SocketException catch (_) {
      setState(() => isLoading = false);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    getPaymentChannel();
    getAvailableCamera();
    UnauthorizeUsecase.check(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(widget.chanel == "VA" ? "Virtual Account" : "Bank Transfer",
            style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.w600)),
      ),
      body: ListView(
        children: <Widget>[
          _detailPembayaran(),
          widget.chanel == "VA" ? _va() : _bankTransfer()
        ],
      ),
    );
  }

  Widget _detailPembayaran() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 30, 20, 20),
      padding: EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: _itemDetail(),
    );
  }

  Widget _itemDetail() {
    return Container(
      child: widget.chanel == "VA"
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                isProduction
                    ? Container()
                    : Row(
                        children: [
                          Text("fixed-va-${widget.id}-d${widget.id}"),
                          IconButton(
                              icon: Icon(Icons.content_copy),
                              onPressed: () {
                                Clipboard.setData(ClipboardData(
                                    text:
                                        "fixed-va-${widget.id}-d${widget.id}"));
                                Toast.show("Copied to Clipboard", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM);
                              })
                        ],
                      ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 20),
                  child: Row(
                    children: <Widget>[
                      Text("Transfer ke : "),
                      widget.merchantCode == null
                          ? Container()
                          : Container(
                              width: 75,
                              child: Image.asset(widget.merchantCode == "8808"
                                  ? 'assets/bank/bni.png'
                                  : widget.merchantCode == "8214"
                                      ? 'assets/bank/permatabank.png'
                                      : widget.merchantCode == "26215"
                                          ? 'assets/bank/bri.png'
                                          : widget.merchantCode == "12090"
                                              ? 'assets/bank/bca.png'
                                              : 'assets/bank/mandiri.png')),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Nomor Virtual Account"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "${widget.accountNumber}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 24),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                        child: Text("a/n ${widget.nameVa}",
                            style: TextStyle(color: Colors.grey)),
                      )),
                      InkWell(
                        onTap: () {
                          Clipboard.setData(
                              new ClipboardData(text: widget.accountNumber));
                          Toast.show("Copied to Clipboard", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                        },
                        child: Text("Salin",
                            style: TextStyle(
                                color: Color(0xFF218196),
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.right),
                      ),
                    ],
                  ),
                ),
                widget.chanel == "VA"
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text("Biaya admin"),
                      )
                    : Container(),
                widget.chanel == "VA"
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        margin:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                        child: Text(
                          "Rp ${rupiah.format(widget.fee)}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                      )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Jumlah yang harus ditransfer"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "Rp ${rupiah.format(widget.amount + widget.fee)}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    children: <Widget>[
                      Expanded(child: Container()),
                      InkWell(
                        onTap: () {
                          Clipboard.setData(new ClipboardData(
                              text: (widget.amount + widget.fee).toString()));
                          Toast.show("Copied to Clipboard", context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                        },
                        child: Text("Salin",
                            style: TextStyle(
                                color: Color(0xFF218196),
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.right),
                      ),
                    ],
                  ),
                )
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Jumlah yang harus dibayarkan"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  decoration: BoxDecoration(color: Color(0xFFF3F3F3)),
                  child: Text(
                    "Rp ${rupiah.format(widget.amount)}",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _bankTransfer() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
              "Transfer sejumlah nominal diatas ke salah satu rekening berikut untuk memproses pembayaran Anda.",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.w600)),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(20, 8, 20, 20),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.grey[300])),
          child: paymentChannel.bankTransfer.length == null
              ? Container(
                  height: 140,
                  child: Center(child: CircularProgressIndicator()))
              : ListView.separated(
                  separatorBuilder: (_, i) => Divider(
                    color: Colors.grey,
                  ),
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: paymentChannel.bankTransfer.length,
                  itemBuilder: (context, i) {
                    return _listBank(paymentChannel.bankTransfer[i]);
                  },
                ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 2, right: 4),
                child: Text("Bank Tujuan : "),
              ),
              Expanded(
                child: DropdownButton(
                  isDense: true,
                  isExpanded: true,
                  value: bank,
                  items: dropDownItem,
                  hint: Text("Pilih Bank"),
                  onChanged: (selected) {
                    setState(() {
                      bank = selected;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(20, 8, 20, 20),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.grey[300])),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Center(
                    child: Text('Unggah Bukti Pembayaran',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600))),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  // margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding: EdgeInsets.symmetric(vertical: 12),
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xFFE6EAFA),
                  child: Column(
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                    title: Text('Pilih ambil dari :'),
                                    content: Container(
                                      height: 100,
                                      width: 200,
                                      child: Column(
                                        children: <Widget>[
                                          FlatButton(
                                              color: Colors.red[900],
                                              child: Text(
                                                'Galeri',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              onPressed: () async {
                                                // setState(() {
                                                //   Helper.pickSomething = 1;
                                                // });
                                                await ImagePicker.pickImage(
                                                        source:
                                                            ImageSource.gallery)
                                                    .then((image) {
                                                  setState(() {
                                                    // Helper.pickSomething = 0;
                                                    bukti = image;
                                                  });
                                                });
                                                Navigator.pop(context);
                                              }),
                                          FlatButton(
                                            color: Colors.red[900],
                                            child: Text('Kamera',
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            onPressed: () async {
                                              if (cameras.length == 0) {
                                                await ImagePicker.pickImage(
                                                        source:
                                                            ImageSource.camera)
                                                    .then((image) {
                                                  setState(() {
                                                    bukti = image;
                                                  });
                                                });
                                                Navigator.pop(context);
                                              } else {
                                                Navigator.pop(context);
                                                final result = Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            Camera()));
                                                result.then((image) {
                                                  if (image != null) {
                                                    setState(() {
                                                      bukti = image;
                                                    });
                                                  }
                                                });
                                              }
                                            },
                                          )
                                        ],
                                      ),
                                    ));
                              });
                        },
                        child: bukti == null
                            ? Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 100),
                                child: Icon(
                                  Icons.folder,
                                  color: Color(0xFF218196),
                                  size: 50,
                                ),
                              )
                            : Image.file(bukti, fit: BoxFit.fill),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                child: Text(
                  "Max. ukuran file 10 mb. Gunakan format file .jpg, png atau .bmp",
                  style: TextStyle(fontSize: 12),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 16),
                width: MediaQuery.of(context).size.width / 1,
                child: FlatButton(
                  disabledColor: Colors.grey,
                  color: Color(0XFF4E5DE3),
                  onPressed: isLoading
                      ? null
                      : bukti == null || bank == null
                          ? null
                          : () {
                              confirmation(context, bukti, bank, widget.uuid);
                              setState(() {
                                bukti = null;
                              });
                            },
                  child: Text(isLoading ? 'Mengupload...' : 'Upload',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _va() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: Colors.grey[300])),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Cara Transfer",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          ),
          Container(
            height: 1,
            color: Colors.grey[300],
          ),
          widget.merchantCode == "8214"
              ? _permata()
              : widget.merchantCode == "26215"
                  ? _bri()
                  : widget.merchantCode == "8808"
                      ? _bni()
                      : widget.merchantCode == "12090"
                          ? _bca()
                          : _mandiri(),
          Container(height: 16),
        ],
      ),
    );
  }

  Widget caraTransfer(String title, Widget content) {
    return ExpandableNotifier(
        child: ScrollOnExpand(
      scrollOnExpand: false,
      scrollOnCollapse: true,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                header: Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      title,
                      style: TextStyle(fontSize: 16),
                    )),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: content,
                    ),
                    Container(
                      height: 1,
                      color: Colors.grey[200],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }

  Widget _mandiri() {
    String codeMandiri = "";
    if (widget.accountNumber.substring(0, 5) == "88608") {
      codeMandiri = "88608";
    } else if (widget.accountNumber.substring(0, 5) == "88908") {
      codeMandiri = "88908";
    }
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Kunjungi website Mandiri Internet Banking: "),
                      new TextSpan(
                        text: "https://ibank.bankmandiri.co.id/retail3/",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://ibank.bankmandiri.co.id/retail3/'),
                      ),
                      new TextSpan(
                          text:
                              "\n2. Login dengan memasukkan USER ID dan PIN\n3. Pilih 'Pembayaran’\n4. Pilih 'Multi Payment’\n5. Pilih 'No Rekening Anda’\n6. Pilih Penyedia Jasa 'Xendit $codeMandiri\n7. Pilih 'No Virtual Account’ \n8. Masukkan nomor Virtual Account anda\n9. Masuk ke halaman konfirmasi 1\n10. Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik 'Lanjutkan’\n11. Masuk ke halaman konfirmasi 2\n12. Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik 'Kirim’\n13. Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai"),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Log in Mobile Banking Mandiri Online (Install Mandiri Online by PT. Bank Mandiri (Persero Tbk.) dari App Store: \n"),
                      new TextSpan(
                        text:
                            "https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8'),
                      ),
                      new TextSpan(text: " atau Play Store: \n"),
                      new TextSpan(
                        text:
                            "https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&hl=en",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&hl=en'),
                      ),
                      new TextSpan(
                          text:
                              " \n2. Klik 'Icon Menu' di sebelah kiri atas\n3. Pilih menu 'Pembayaran’\n4. Pilih Buat Pembayaran Baru’\n5. Pilih 'Multi Payment’\n6. Pilih Penyedia Jasa 'Xendit $codeMandiri'\n7. Pilih 'No. Virtual'\n8. Masukkan no virtual account dengan kode perusahaan '$codeMandiri' lalu pilih 'Tambah Sebagai Nomor Baru'\n9. Masukkan 'Nominal' lalu pilih 'Konfirmasi'\n10. Pilih 'Lanjut'\n11. Muncul tampilan konfirmasi pembayaran\n12. Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih 'Konfirmasi'\n13. Masukkan 'PIN' dan transaksi telah selesai"),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM Mandiri",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Masukkan kartu ATM dan pilih 'Bahasa Indonesia'\n2. Ketik nomor PIN dan tekan BENAR\n3. Pilih menu 'BAYAR/BELI’\n4. Pilih menu 'MULTI PAYMENT’\n5. Ketik kode perusahaan, yaitu '$codeMandiri' (Xendit $codeMandiri), tekan 'BENAR’\n6. Masukkan nomor Virtual Account\n7. Isi NOMINAL, kemudian tekan 'BENAR’\n8. Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan 'YA'\n9. Muncul konfirmasi pembayaran. Tekan 'YA' untuk melakukan pembayaran\n10. Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri"),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _bri() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Masukan User ID dan Password \n2. Pilih menu Pembayaran \n3. Pilih menu BRIVA \n4. Pilih rekening Pembayar \n5. Masukkan Nomor Virtual Account BRI Anda (Contoh: 26215-xxxxxxxxxxxx) \n6. Masukkan nominal yang akan dibayar \n7. Masukkan password dan Mtoken anda"),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Log in ke Mobile Banking \n2. Pilih menu Pembayaran \n3. Pilih menu BRIVA \n4. Masukkan nomor BRI Virtual Account dan jumlah pembayaran \n5. Masukkan nomor PIN anda \n6. Tekan “OK” untuk melanjutkan transaksi \n7. Transaksi berhasil \n8. SMS konfirmasi akan masuk ke nomor telepon anda"),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM BRI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              "1. Pilih menu Transaksi Lain \n2. Pilih menu Lainnya \n3. Pilih menu Pembayaran \n4. Pilih menu Lainnya \n5. Pilih menu BRIVA \n6. Masukkan Nomor BRI Virtual Account (Contoh: 26215-xxxxxxxxxxxx), lalu tekan “Benar” \n7. Konfirmasi pembayaran, tekan “Ya” bila sesuai"),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _bni() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pilih “ Transaksi” \n2. Pilih “INFO & ADMINISTRASI TRANSFER” \n3. Pilih “ATUR REKENING TUJUAN” \n4. Tambah rekening tujuan lalu klik “OK” \n5. Isi data rekening lalu tekan “LANJUTKAN” \n6. Akan muncul detail konfirmasi, apabila sudah benar dan sesuai, masukkan 8 digit angka yang dihasilkan dari APPLI 2 pada token BNI anda lalu klik ”PROSES” \n7. Rekening tujuan berhasil ditambahkan \n8. Pilih “TRANSFER” \n9. Pilih “Transfer antar rek BNI” \n10. Lengkapi semua data akun penerima, lalu klik “LANJUTKAN” \n11. Transaksi anda telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Log in Mobile Banking \n2. Pilih menu “TRANSFER” \n3. Pilih “WITHIN BANK” \n4. Isi kolom “DEBIT ACCOUNT” kemudian klik menu “ TO ACCOUNT” \n5. Pilih menu ”AD HOC BENEFICIARY” \n6. Lengkapi datanya dengan mengisi: NICKNAME, NOMOR VIRTUAL ACCOUNT, DAN BENEFICIARY EMAIL ADDRESS \n7. Konfirmasi akan muncul lalu klik “CONTINUE” \n8. Isi semua form yang ada setelah itu klik “CONTINUE” \n9. Detail konfirmasi muncul dengan meminta password transaksi \n10. Setelah dilengkapi, klik “CONTINUE” \n11. Transaksi anda telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM BNI",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pilih "Menu Lainnya" \n2. Pilih "Transfer" \n3. Pilih “TRANSAKSI LAINNYA” \n4. Pilih ke “ REKENING BNI” \n5. Masukkan nomor Virtual Account , lalu tekan “BENAR” \n6. Isi NOMINAL, kemudian tekan ”YA" \n7. Konfirmasi transaksi telah selesai, tekan “TIDAK” untuk menyelesaikan transaksi'),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _permata() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "Internet Banking PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: '1. Buka situs\n'),
                      new TextSpan(
                        text: 'https://new.permatanet.com',
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => launch('https://new.permatanet.com'),
                      ),
                      new TextSpan(
                          text:
                              '\ndan login \n2. Pilih menu “pembayaran”. \n3. Pilih menu “Pembayaran Tagihan”. \n4. Pilih “Virtual Account” \n5. Pilih sumber pembayaran \n6. Pilih menu "Masukkan Daftar Tagihan Baru" \n7. Masukan nomor Virtual Account Anda \n8. Konfirmasi transaksi anda \n9. Masukkan SMS token response \n10. Pembayaran Anda berhasil \n11. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Buka Permata Mobile dan Login \n2. Pilih Pay "Pay Bills" / "Pembayaran Tagihan" \n3. Pilih menu “Transfer” \n4. Pilih sumber pembayaran \n5. Pilih “daftar tagihan baru” \n6. Masukan nomor Virtual Account Anda \n7. Periksa ulang mengenai transaksi yang anda ingin lakukan \n8. Konfirmasi transaksi anda \n9. Masukkan SMS token respons \n10. Pembayaran Anda berhasil \n11. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "Mobile Banking PERMATA X",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Buka Permata Mobile X dan Login \n2. Pilih Pay "Pay Bills"/ "Pembayaran Tagihan" \n3. Pilih “Virtual Account” \n4. Masukkan Nomor Virtual Account anda \n5. Detail pembayaran anda akan muncul di layar \n6. Nominal yang ditagihkan akan muncul di layar. Pilih sumber pembayaran \n7. Konfirmasi transaksi anda \n8. Masukkan kode response token anda \n9. Transaksi anda telah selesai \n10. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
        caraTransfer(
            "ATM PERMATA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Pada menu utama, pilih transaksi lain \n2. Pilih Pembayaran Transfer \n3. Pilih pembayaran lainnya \n4. Pilih pembayaran Virtual Account \n5. Masukkan nomor virtual account anda \n6. Pada halaman konfirmasi, akan muncul nominal yang dibayarkan, nomor, dan nama merchant, lanjutkan jika sudah sesuai \n7. Pilih sumber pembayaran anda dan lanjutkan \n8. Transaksi anda selesai \n9. Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.'),
                    ],
                  )),
            )),
      ],
    );
  }

  Widget _listBank(BankTransfer bank) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
              width: 75,
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
              child: Image.asset(
                  bank.provider == "BCA"
                      ? 'assets/bank/bca.png'
                      : bank.provider == "BRI"
                          ? 'assets/bank/bri.png'
                          : 'assets/bank/mandiri.png',
                  scale: MediaQuery.of(context).size.width / 24)),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    bank.accountName,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                    overflow: TextOverflow.visible,
                  ),
                  Text(
                    bank.accountNumber,
                    style: TextStyle(fontSize: 12),
                    overflow: TextOverflow.visible,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _bca() {
    return Column(
      children: <Widget>[
        caraTransfer(
            "ATM BCA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Masukkan kartu ke mesin ATM\n2. Masukkan 6 digit PIN Anda\n3. Pilih “Transaksi Lainnya”\n4. Pilih “Transfer”\n5. Pilih ke “ke Rekening BCA Virtual Account”\n6. Masukkan nomor BCA Virtual Account Anda, kemudian tekan “Benar”\n7. Masukkan jumlah yang akan dibayarkan, selanjutnya tekan “Benar”\n8. Validasi pembayaran Anda. Pastikan semua detail transaksi yang ditampilkan sudah benar, kemudian pilih “Ya”\n9. Pembayaran Anda telah selesai. Tekan “Tidak” untuk menyelesaikan transaksi, atau tekan “Ya” untuk melakukan transaksi lainnya'),
                    ],
                  )),
            )),
        caraTransfer(
            "M-BCA",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan login pada aplikasi BCA Mobile\n2. Pilih “m-BCA”, lalu masukkan kode akses m-BCA\n3. Pilih “m-Transfer”\n4. Pilih “BCA Virtual Account”\n5. Masukkan nomor BCA Virtual Account Anda, atau pilih dari Daftar Transfer\n6. Masukkan jumlah yang akan dibayarkan\n7. Masukkan PIN m-BCA Anda\n8. Transaksi telah berhasil'),
                    ],
                  )),
            )),
        caraTransfer(
            "Klik BCA Pribadi",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan login pada aplikasi KlikBCA Individual\n2. Masukkan User ID dan PIN Anda\n3. Pilih “Transfer Dana”\n4. Pilih “Transfer ke BCA Virtual Account”\n5. Masukkan nomor BCA Virtual Account Anda atau pilih dari Daftar Transfer\n6. Masukkan jumlah yang akan dibayarkan\n7. Validasi pembayaran. Pastikan semua datanya sudah benar, lalu masukkan kode yang diperoleh dari KEYBCA APPLI 1, kemudian klik “Kirim”\n8. Pembayaran telah selesai dilakukan'),
                    ],
                  )),
            )),
        caraTransfer(
            "Klik BCA Bisnis",
            Container(
              child: RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    style: new TextStyle(
                      color: Colors.black,
                      fontFamily: 'Nunito',
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              '1. Silahkan melakukan login di KlikBCA Bisnis\n2. Pilih “Transfer Dana” > “Daftar Transfer” > “Tambah”\n3. Masukkan nomor BCA Virtual Account, lalu “Kirim”\n4. Pilih “Transfer Dana”\n5. Pilih “Ke BCA Virtual Account”\n6. Pilih rekening sumber dana dan BCA Virtual Account tujuan\n7. Masukkan jumlah yang akan dibayarkan, lalu pilih “Kirim”\n8. Validasi Pembayaran. Sampai tahap ini berarti data berhasil di input. Kemudian pilih “simpan”\n9. Pilih “Transfer Dana” > “Otorisasi Transaksi”, lalu pilih transaksi yang akan diotorisasi\n10. Pembayaran telah selesai dilakukan'),
                    ],
                  )),
            )),
      ],
    );
  }
}
