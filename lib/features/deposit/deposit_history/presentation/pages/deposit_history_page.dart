import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/utils/ui/screen_sizes.dart';
import '../../../../../core/widgets/shimmer/box_shimmer.dart';
import '../bloc/deposit_history_bloc.dart';
import '../widgets/deposit_empty_widget.dart';
import '../widgets/deposit_history_card.dart';

class DepositHistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DepositHistoryBloc, DepositHistoryState>(
      builder: (context, state) {
        if (state is DepositHistoryLoaded) {
          if (state.depositHistory.length > 0) {
            return ListView.builder(
              padding: EdgeInsets.all(Sizes.s10),
              itemCount: state.depositHistory.length,
              itemBuilder: (_, i) {
                return DepositHistoryCard(deposit: state.depositHistory[i]);
              },
            );
          } else {
            return DepositEmptyWidget();
          }
        } else if (state is DepositHistoryInitial ||
            state is DepositHistoryLoading) {
          return ListView.separated(
              itemCount: 3,
              padding: EdgeInsets.all(Sizes.s20),
              separatorBuilder: (_, i) {
                return Container(height: Sizes.s10);
              },
              itemBuilder: (_, i) {
                return BoxShimmer(
                    width: double.infinity, height: 100, borderRadius: 4);
              });
        } else if (state is DepositHistoryError) {
          return DepositEmptyWidget();
        } else {
          return DepositEmptyWidget();
        }
      },
    );
  }
}
