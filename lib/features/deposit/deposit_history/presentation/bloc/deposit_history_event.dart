part of 'deposit_history_bloc.dart';

abstract class DepositHistoryEvent extends Equatable {
  const DepositHistoryEvent();
}

class LoadDepositHistory extends DepositHistoryEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}
