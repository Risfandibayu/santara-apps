part of 'deposit_history_bloc.dart';

abstract class DepositHistoryState extends Equatable {
  const DepositHistoryState();
  @override
  List<Object> get props => [];
}

class DepositHistoryInitial extends DepositHistoryState {}

class DepositHistoryLoading extends DepositHistoryState {}

class DepositHistoryLoaded extends DepositHistoryState {
  final List<DepositHistoryModel> depositHistory;

  DepositHistoryLoaded({this.depositHistory});
  @override
  List<Object> get props => [depositHistory];
}

class DepositHistoryError extends DepositHistoryState {}

class DepositHistoryEmpty extends DepositHistoryState {}
