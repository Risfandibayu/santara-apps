import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/features/deposit/deposit_history/data/models/deposit_history_model.dart';
import 'package:santaraapp/features/deposit/deposit_history/domain/usecases/deposit_history_usecase.dart';
part 'deposit_history_event.dart';
part 'deposit_history_state.dart';

class DepositHistoryBloc
    extends Bloc<DepositHistoryEvent, DepositHistoryState> {
  final GetDepositHistory getDepositHistory;

  DepositHistoryBloc({@required GetDepositHistory getDepositHistory})
      : assert(getDepositHistory != null),
        getDepositHistory = getDepositHistory,
        super(DepositHistoryInitial());

  @override
  Stream<DepositHistoryState> mapEventToState(
    DepositHistoryEvent event,
  ) async* {
    if (event is LoadDepositHistory) {
      try {
        yield DepositHistoryLoading();
        final result = await getDepositHistory(NoParams());

        yield* result.fold(
          (failure) async* {
            yield DepositHistoryError();
          },
          (depositHistory) async* {
            if (depositHistory != null) {
              yield DepositHistoryLoaded(depositHistory: depositHistory);
            } else {
              yield DepositHistoryEmpty();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield DepositHistoryError();
      }
    }
  }
}
