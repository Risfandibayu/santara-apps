import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import '../../../../../core/utils/formatter/date_formatter.dart';
import '../../../../../pages/Home.dart';
import '../../../../../utils/helper.dart';
import '../../../../dana/presentation/pages/dana_webview.dart';
import '../../../main/presentation/pages/main_deposit_page.dart';
import '../../data/models/deposit_history_model.dart';
import '../pages/detail_deposit_history_page.dart';
import 'content_expanded.dart';

class DepositHistoryCard extends StatelessWidget {
  final DepositHistoryModel deposit;

  DepositHistoryCard({Key key, this.deposit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Color(0xFFB8B8B8),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                color: Color(0xFFF0F0F0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                )),
            height: 40,
            width: double.maxFinite,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "${DateFormatter.ddMMMyyyy(DateTime.parse(deposit.createdAt))}",
                  style: TextStyle(fontSize: 11),
                ),
                deposit.status == 0 && deposit.confirmationPhoto == null
                    ? Text("Menunggu Pembayaran",
                        style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFFE5A037),
                            fontWeight: FontWeight.bold))
                    : deposit.confirmationPhoto != null && deposit.status == 0
                        ? Text("Menunggu Verifikasi",
                            style: TextStyle(
                                fontSize: 14,
                                color: Color(0xFFE5A037),
                                fontWeight: FontWeight.bold))
                        : deposit.status == 2
                            ? Text("Gagal",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color(0xFFBF2D30),
                                    fontWeight: FontWeight.bold))
                            : Text("Berhasil",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color(0xFF0E7E4A),
                                    fontWeight: FontWeight.bold))
              ],
            ),
          ),
          Container(
            child: ExpandableNotifier(
                child: ScrollOnExpand(
              scrollOnExpand: false,
              scrollOnCollapse: true,
              child: ExpandablePanel(
                header: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.only(right: 12),
                              child: Image.asset(
                                "assets/icon_menu/new/Deposit.png",
                                width: 50,
                              )),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Deposit",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                Text(
                                  Helper.username,
                                  style: TextStyle(fontSize: 15),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 6, right: 6, bottom: 12),
                      child: ContentExpanded(deposit: deposit),
                    ),
                    Container(
                      height: 1,
                      color: Colors.grey[200],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                    ),
                  );
                },
              ),
            )),
          ),
          deposit.status == 0 && deposit.confirmationPhoto == null
              ? Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      if (deposit.chanel == "DANA") {
                        final result = Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => DanaWebview(
                                      appbarName: "",
                                      url: deposit.danaCheckoutUrl,
                                      type: DanaWebviewType.trx,
                                    )));
                        result.then((value) {
                          if (value) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => Home(
                                          index: 4,
                                        )),
                                (route) => false);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => MainDepositPage(
                                          initialTab: 1,
                                        )));
                          }
                        });
                      } else {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return DetailDepositHistoryPage(
                            id: "${deposit.id}",
                            uuid: deposit.uuid,
                            chanel: deposit.chanel,
                            amount: deposit.amount,
                            merchantCode: deposit.virtualAccount == null
                                ? null
                                : deposit.virtualAccount.merchantCode,
                            accountNumber: deposit.virtualAccount == null
                                ? null
                                : deposit.virtualAccount.accountNumber,
                            nameVa: deposit.virtualAccount == null
                                ? null
                                : deposit.virtualAccount.name,
                            fee: deposit.fee != null
                                ? int.parse(deposit.fee)
                                : 0,
                          );
                        }));
                      }
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 40,
                      margin: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Color(0xffEEF1FF),
                          borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.only(left: 15, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Lanjutkan Pembayaran",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Colors.black,
                            size: 16,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
