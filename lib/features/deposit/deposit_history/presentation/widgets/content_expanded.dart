import 'package:flutter/material.dart';

import '../../../../../core/utils/formatter/currency_formatter.dart';
import '../../data/models/deposit_history_model.dart';

class ContentExpanded extends StatelessWidget {
  final DepositHistoryModel deposit;

  const ContentExpanded({Key key, this.deposit}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 1, color: Color(0xFFC4C4C4)),
        Container(height: 8),
        // Metode Pembayaran
        Text("Metode Pembayaran"),
        Text(
            deposit.chanel == "VA"
                ? "Virtual Account"
                : deposit.chanel.toString(),
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        Container(height: 8),

        // Nama Bank Anda
        deposit.chanel == "DANA"
            ? Container()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  deposit.chanel == "VA" ? Container() : Text("Nama Bank Anda"),
                  deposit.chanel == "VA"
                      ? Container()
                      : Text("${deposit.bankFrom.toString()}",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                  deposit.chanel == "VA" ? Container() : Container(height: 8),
                ],
              ),

        // Nomor Rekening Anda
        deposit.chanel == "DANA"
            ? Container()
            : Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                deposit.chanel == "VA"
                    ? Container()
                    : Text("Nomor Rekening Anda"),
                deposit.chanel == "VA"
                    ? Container()
                    : Text("${deposit.accountNumber.toString()}",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                deposit.chanel == "VA" ? Container() : Container(height: 8),
              ]),

        // Bank VA
        deposit.chanel == "DANA"
            ? Container()
            : Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                deposit.chanel == "VA"
                    ? Text("Bank Virtual Account")
                    : Container(),
                deposit.chanel == "VA"
                    ? Text(
                        deposit.virtualAccount.merchantCode == "8808"
                            ? 'Bank BNI'
                            : deposit.virtualAccount.merchantCode == "8214"
                                ? 'Bank Permata'
                                : deposit.virtualAccount.merchantCode == "26215"
                                    ? 'Bank BRI'
                                    : deposit.virtualAccount.merchantCode ==
                                            "12090"
                                        ? 'Bank BCA'
                                        : 'Bank Mandiri',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold))
                    : Container(),
                deposit.chanel == "VA" ? Container(height: 8) : Container(),
              ]),

        // Nominal
        Text("Nominal yang Disetorkan"),
        Text("${CurrencyFormatter.format(deposit.amount)}",
            style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0E7E4A))),
        Container(height: 12),

        deposit.fee == null || deposit.fee == "0"
            ? Container()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Biaya Admin"),
                  Text(
                      "${CurrencyFormatter.format(int.parse(deposit.fee != null ? deposit.fee : '0'))}",
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF0E7E4A))),
                  Container(height: 12),
                ],
              )
      ],
    );
  }
}
