import 'package:dartz/dartz.dart';

import '../../../../../core/error/failure.dart';
import '../../data/models/deposit_history_model.dart';

abstract class DepositHistoryRepository {
  Future<Either<Failure, List<DepositHistoryModel>>> getDepositHistory();
}
