import 'package:dartz/dartz.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/usecases/usecase.dart';
import 'package:santaraapp/features/deposit/deposit_history/data/models/deposit_history_model.dart';
import 'package:santaraapp/features/deposit/deposit_history/domain/repositories/deposit_history_repository.dart';

class GetDepositHistory
    implements UseCase<List<DepositHistoryModel>, NoParams> {
  final DepositHistoryRepository repository;
  GetDepositHistory(this.repository);

  @override
  Future<Either<Failure, List<DepositHistoryModel>>> call(
      NoParams params) async {
    return await repository.getDepositHistory();
  }
}
