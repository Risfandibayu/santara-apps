import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../../../../core/usecases/unauthorize_usecase.dart';
import '../../../../../widget/home/blocs/button.bloc.dart';
import '../../../../../widget/widget/components/kyc/KycNotificationStatus.dart';
import '../../../../transaction/presentation/bloc/checkout_list/checkout_list_bloc.dart';
import '../../../deposit/presentation/bloc/investment_limit/investment_limit_bloc.dart';
import '../../../deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import '../../../deposit/presentation/pages/deposit_page.dart';
import '../../../deposit_history/presentation/bloc/deposit_history_bloc.dart';
import '../../../deposit_history/presentation/pages/deposit_history_page.dart';

class MainDepositPage extends StatefulWidget {
  final int initialTab;

  const MainDepositPage({Key key, this.initialTab}) : super(key: key);

  @override
  _MainDepositPageState createState() => _MainDepositPageState();
}

class _MainDepositPageState extends State<MainDepositPage> {
  CheckoutListBloc checkoutListBloc;
  SaldoDepositBloc saldoDepositBloc;
  DepositHistoryBloc depositHistoryBloc;
  InvestmentLimitBloc investmentLimitBloc;
  ButtonBloc buttonBloc;

  @override
  void initState() {
    super.initState();
    UnauthorizeUsecase.check(context);
    checkoutListBloc = BlocProvider.of<CheckoutListBloc>(context);
    checkoutListBloc.add(LoadCheckoutList());
    saldoDepositBloc = BlocProvider.of<SaldoDepositBloc>(context);
    saldoDepositBloc.add(LoadSaldoDeposit(0));
    depositHistoryBloc = BlocProvider.of<DepositHistoryBloc>(context);
    depositHistoryBloc.add(LoadDepositHistory());
    investmentLimitBloc = BlocProvider.of<InvestmentLimitBloc>(context);
    investmentLimitBloc.add(LoadInvestmentLimit());
    buttonBloc = BlocProvider.of<ButtonBloc>(context);
    buttonBloc.add(LoadButton());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: widget.initialTab ?? 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Deposit",
            style: TextStyle(color: Colors.white),
          ),
          iconTheme: IconThemeData(color: Color(ColorRev.mainwhite)),
          backgroundColor: Color(ColorRev.mainBlack),
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Color((0xFFBF2D30)),
            labelColor: Color((0xFFBF2D30)),
            unselectedLabelColor: Color(ColorRev.mainwhite),
            tabs: [
              Tab(text: "Deposit"),
              Tab(text: "Riwayat Deposit"),
            ],
          ),
        ),
        backgroundColor: Color(ColorRev.mainBlack),
        body: Stack(children: [
          TabBarView(
            children: [
              DepositPage(),
              DepositHistoryPage(),
            ],
          ),
          KycNotificationStatus(
            showCloseButton: false,
          )
        ]),
      ),
    );
  }
}
