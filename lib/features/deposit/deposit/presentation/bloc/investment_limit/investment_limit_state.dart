part of 'investment_limit_bloc.dart';

abstract class InvestmentLimitState extends Equatable {
  const InvestmentLimitState();

  @override
  List<Object> get props => [];
}

class InvestmentLimitInitial extends InvestmentLimitState {
  @override
  List<Object> get props => [];
}

class InvestmentLimitLoading extends InvestmentLimitState {
  @override
  List<Object> get props => [];
}

class InvestmentLimitError extends InvestmentLimitState {
  Failure failure;
  InvestmentLimitError({this.failure});
  @override
  List<Object> get props => [failure];
}

class InvestmentLimitEmpty extends InvestmentLimitState {
  @override
  List<Object> get props => [];
}

class InvestmentLimitLoaded extends InvestmentLimitState {
  final bool isUnlimited;
  final double maxInvest;
  final double portofolio;
  InvestmentLimitLoaded(
      {@required this.isUnlimited,
      @required this.maxInvest,
      @required this.portofolio});

  @override
  List<Object> get props => [isUnlimited, maxInvest, portofolio];
}
