part of 'investment_limit_bloc.dart';

abstract class InvestmentLimitEvent extends Equatable {
  const InvestmentLimitEvent();

  @override
  List<Object> get props => [];
}

class LoadInvestmentLimit extends InvestmentLimitEvent {
  @override
  List<Object> get props => [];
}
