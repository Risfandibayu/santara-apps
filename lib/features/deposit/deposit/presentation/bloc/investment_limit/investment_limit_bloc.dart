import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/core/error/failure.dart';
import 'package:santaraapp/core/utils/constants/app_constants.dart';
import 'package:santaraapp/core/utils/tools/logger.dart';
import 'package:santaraapp/features/portofolio/domain/usecases/portofolio_list_usecase.dart';

import '../../../../../../core/domain/usecases/max_invest_usecase.dart';
import '../../../../../../core/domain/usecases/user_usecase.dart';
import '../../../../../../core/usecases/usecase.dart';

part 'investment_limit_event.dart';
part 'investment_limit_state.dart';

class InvestmentLimitBloc
    extends Bloc<InvestmentLimitEvent, InvestmentLimitState> {
  final GetMaxInvest _getMaxInvest;
  final GetPortofolioList _getPortofolioList;
  final GetUserByUuid _getUserByUuid;

  InvestmentLimitBloc(
      {@required GetMaxInvest getMaxInvest,
      @required GetPortofolioList getPortofolioList,
      @required GetUserByUuid getUserByUuid})
      : assert(getMaxInvest != null),
        assert(getPortofolioList != null),
        assert(getPortofolioList != null),
        _getMaxInvest = getMaxInvest,
        _getPortofolioList = getPortofolioList,
        _getUserByUuid = getUserByUuid,
        super(InvestmentLimitInitial());

  @override
  Stream<InvestmentLimitState> mapEventToState(
    InvestmentLimitEvent event,
  ) async* {
    final storage = secureStorage.FlutterSecureStorage();
    final uuid = await storage.read(key: USER_UUID);
    if (event is LoadInvestmentLimit) {
      try {
        yield InvestmentLimitLoading();
        final userData = await _getUserByUuid(uuid);
        final maxInvest = await _getMaxInvest(NoParams());
        final portofolioList = await _getPortofolioList(NoParams());

        yield* userData.fold(
          (failure) async* {
            yield InvestmentLimitError(failure: failure);
          },
          (user) async* {
            if (user != null) {
              final bool isUnlimited = user.trader.traderType == "company"
                  ? true
                  : user.job.isUnlimitedInvest == 1
                      ? true
                      : false;
              yield* maxInvest.fold(
                (failure) async* {
                  yield InvestmentLimitError(failure: failure);
                },
                (maxInvest) async* {
                  if (maxInvest != null) {
                    yield* portofolioList.fold((failure) async* {
                      yield InvestmentLimitError(failure: failure);
                    }, (portofolio) async* {
                      if (portofolio != null) {
                        yield InvestmentLimitLoaded(
                            isUnlimited: isUnlimited,
                            maxInvest: maxInvest.maxInvest,
                            portofolio: (portofolio.total / 1));
                      } else {
                        yield InvestmentLimitLoaded(
                            isUnlimited: isUnlimited,
                            maxInvest: maxInvest.maxInvest,
                            portofolio: 0);
                      }
                    });
                  } else {
                    yield InvestmentLimitEmpty();
                  }
                },
              );
            } else {
              yield InvestmentLimitEmpty();
            }
          },
        );
      } catch (e, stack) {
        santaraLog(e, stack);
        yield InvestmentLimitError();
      }
    }
  }
}
