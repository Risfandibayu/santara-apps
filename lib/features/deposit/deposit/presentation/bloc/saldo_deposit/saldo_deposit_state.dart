part of 'saldo_deposit_bloc.dart';

abstract class SaldoDepositState extends Equatable {
  const SaldoDepositState();
}

class SaldoDepositInitial extends SaldoDepositState {
  @override
  List<Object> get props => [];
}

class SaldoDepositLoading extends SaldoDepositState {
  @override
  List<Object> get props => [];
}

class SaldoDepositError extends SaldoDepositState {
  @override
  List<Object> get props => [];
}

class SaldoDepositEmpty extends SaldoDepositState {
  @override
  List<Object> get props => [];
}

class SaldoDepositLoaded extends SaldoDepositState {
  final User user;
  final SaldoDepositModel saldoDeposit;
  final FeeModel fee;
  final bool isDanaActive;
  SaldoDepositLoaded({
    this.user,
    @required this.saldoDeposit,
    this.fee,
    @required this.isDanaActive,
  });

  @override
  List<Object> get props => [saldoDeposit];
}
