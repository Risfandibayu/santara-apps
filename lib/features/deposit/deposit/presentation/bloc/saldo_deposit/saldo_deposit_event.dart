part of 'saldo_deposit_bloc.dart';

abstract class SaldoDepositEvent extends Equatable {
  const SaldoDepositEvent();
  @override
  List<Object> get props => [];
}

class LoadSaldoDeposit extends SaldoDepositEvent {
  final int retrial;

  LoadSaldoDeposit(this.retrial);
}
