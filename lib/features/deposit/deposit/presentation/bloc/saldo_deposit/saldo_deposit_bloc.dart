import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:meta/meta.dart';
import 'package:santaraapp/utils/api.dart';

import '../../../../../../core/data/models/fee_model.dart';
import '../../../../../../core/data/models/user_model.dart';
import '../../../../../../core/domain/usecases/fee_usecase.dart';
import '../../../../../../core/domain/usecases/user_usecase.dart';
import '../../../../../../core/usecases/usecase.dart';
import '../../../../../../core/utils/constants/app_constants.dart';
import '../../../../../../utils/logger.dart';
import '../../../data/models/saldo_deposit_model.dart';
import '../../../domain/usecases/deposit_usecases.dart';

part 'saldo_deposit_event.dart';
part 'saldo_deposit_state.dart';

class SaldoDepositBloc extends Bloc<SaldoDepositEvent, SaldoDepositState> {
  final GetUserByUuid _getUserByUuid;
  final GetSaldoDeposit _getSaldoDeposit;
  final GetAllFee _getAllFee;

  SaldoDepositBloc(
      {@required GetUserByUuid getUserByUuid,
      @required GetSaldoDeposit getSaldoDeposit,
      @required GetAllFee getAllFee})
      : assert(getUserByUuid != null),
        assert(getSaldoDeposit != null),
        assert(getAllFee != null),
        _getUserByUuid = getUserByUuid,
        _getSaldoDeposit = getSaldoDeposit,
        _getAllFee = getAllFee,
        super(SaldoDepositInitial());

  @override
  Stream<SaldoDepositState> mapEventToState(
    SaldoDepositEvent event,
  ) async* {
    final storage = secureStorage.FlutterSecureStorage();
    if (event is LoadSaldoDeposit) {
      try {
        yield SaldoDepositLoading();
        final userUuid = await storage.read(key: USER_UUID);
        final userData = await _getUserByUuid(userUuid);

        yield* userData.fold(
          (failure) async* {
            if (event.retrial < 3) {
              add(LoadSaldoDeposit(event.retrial + 1));
            } else {
              yield SaldoDepositError();
            }
          },
          (user) async* {
            if (user != null) {
              yield* _mapLoadSaldoEventToState(event, user);
            } else {
              if (event.retrial < 3) {
                add(LoadSaldoDeposit(event.retrial + 1));
              } else {
                yield SaldoDepositEmpty();
              }
            }
          },
        );
      } catch (e, stack) {
        if (event.retrial < 3) {
          add(LoadSaldoDeposit(event.retrial + 1));
        } else {
          santaraLog(e, stack);
          yield SaldoDepositError();
        }
      }
    }
  }

  Stream<SaldoDepositState> _mapLoadSaldoEventToState(
      LoadSaldoDeposit event, User user) async* {
    final result = await _getSaldoDeposit(NoParams());
    final allFee = await _getAllFee(NoParams());
    RemoteConfig remoteConfig = await RemoteConfig.instance;
    bool isDanaActive = true;
    isDanaActive = remoteConfig.getBool(isFeatureDanaActive);
    yield* result.fold(
      (failure) async* {
        if (event.retrial < 3) {
          add(LoadSaldoDeposit(event.retrial + 1));
        } else {
          yield SaldoDepositError();
        }
      },
      (deposit) async* {
        if (deposit != null) {
          yield* allFee.fold(
            (failure) async* {
              if (event.retrial < 3) {
                add(LoadSaldoDeposit(event.retrial + 1));
              } else {
                yield SaldoDepositError();
              }
            },
            (fee) async* {
              if (fee != null) {
                yield SaldoDepositLoaded(
                  user: user,
                  saldoDeposit: deposit,
                  fee: fee,
                  isDanaActive: isDanaActive,
                );
              } else {
                if (event.retrial < 3) {
                  add(LoadSaldoDeposit(event.retrial + 1));
                } else {
                  yield SaldoDepositEmpty();
                }
              }
            },
          );
        } else {
          if (event.retrial < 3) {
            add(LoadSaldoDeposit(event.retrial + 1));
          } else {
            yield SaldoDepositEmpty();
          }
        }
      },
    );
  }
}
