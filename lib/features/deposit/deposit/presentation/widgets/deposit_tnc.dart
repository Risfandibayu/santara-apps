import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../core/widgets/list_item_widget.dart/santara_bullet_list.dart';

class DepositTnc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SantaraBulletList(
          child: Text(
        "Minimal deposit adalah Rp 100.000 (Seratus Ribu Rupiah)",
        style: TextStyle(color: Colors.white, fontSize: 14),
      )),
      // _item(BlocBuilder<InvestmentLimitBloc, InvestmentLimitState>(
      //     builder: (context, state) {
      //   return RichText(
      //     text: TextSpan(
      //       style: TextStyle(
      //           color: Color(0xFF747474),
      //           fontSize: Sizes.s14,
      //           fontFamily: 'Nunito'),
      //       children: [
      //         TextSpan(
      //           text:
      //               "Maksimal deposit adalah sesuai dengan sisa batas kepemilikian investasi ",
      //         ),
      //         WidgetSpan(
      //           alignment: PlaceholderAlignment.middle,
      //           child: state is InvestmentLimitLoaded
      //               ? GestureDetector(
      //                   child: Icon(
      //                     Icons.info,
      //                     size: Sizes.s12,
      //                     color: Color(0xFF747474),
      //                   ),
      //                   onTap: () {
      //                     PopupHelper.showKepemilikanSaham(
      //                         context,
      //                         CurrencyFormatter.format(state.maxInvest),
      //                         CurrencyFormatter.format(state.portofolio),
      //                         CurrencyFormatter.format(
      //                             state.maxInvest - state.portofolio),
      //                         state.isUnlimited ? 1 : 0);
      //                   },
      //                 )
      //               : CupertinoActivityIndicator(radius: 6),
      //         ),
      //       ],
      //     ),
      //   );
      // }))
    ]);
  }
}
