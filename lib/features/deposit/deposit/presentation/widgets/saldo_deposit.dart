import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/saldo_deposit/saldo_deposit_bloc.dart';

class SaldoDeposit extends StatelessWidget {
  final Function(double idr) onLoaded;
  final Function onLoading;
  final Function onError;

  SaldoDeposit({Key key, this.onLoaded, this.onLoading, this.onError})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SaldoDepositBloc, SaldoDepositState>(
        builder: (context, state) {
      if (state is SaldoDepositLoaded) {
        return onLoaded(state.saldoDeposit.idr);
      } else if (state is SaldoDepositError || state is SaldoDepositEmpty) {
        return onError();
      } else {
        return onLoading();
      }
    });
  }
}
