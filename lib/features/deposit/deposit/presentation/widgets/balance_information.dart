import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/utils/formatter/currency_formatter.dart';
import '../../../../../core/widgets/shimmer/box_shimmer.dart';
import '../../../../../helpers/PopupHelper.dart';
import '../../../../../helpers/RupiahFormatter.dart';
import '../../../../../utils/sizes.dart';
import '../bloc/investment_limit/investment_limit_bloc.dart';
import 'saldo_deposit.dart';

class BalanceInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minHeight: 58),
      padding: EdgeInsets.symmetric(horizontal: Sizes.s20, vertical: Sizes.s10),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Color(0xFFB8B8B8)),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Saldo Anda",
                style: TextStyle(
                  color: Color(0xFF858585),
                  fontSize: Sizes.s10,
                ),
              ),
              SizedBox(height: 2),
              SaldoDeposit(
                onLoaded: (idr) {
                  return Text(
                    RupiahFormatter.initialValueFormat(idr.toString()),
                    style: TextStyle(
                        color: Color(0xFF0E7E4A),
                        fontSize: Sizes.s16,
                        fontWeight: FontWeight.bold),
                  );
                },
                onLoading: () {
                  return BoxShimmer(
                    width: MediaQuery.of(context).size.width / 3,
                    height: Sizes.s20,
                    borderRadius: Sizes.s10,
                  );
                },
                onError: () {
                  return Text(
                    RupiahFormatter.initialValueFormat("0"),
                    style: TextStyle(
                        color: Color(0xFF0E7E4A),
                        fontSize: Sizes.s16,
                        fontWeight: FontWeight.bold),
                  );
                },
              ),
            ],
          ),
          BlocBuilder<InvestmentLimitBloc, InvestmentLimitState>(
              builder: (context, state) {
            if (state is InvestmentLimitLoaded) {
              return GestureDetector(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.info,
                      size: 12,
                      color: Color(0xff218196),
                    ),
                    Container(
                      width: 3,
                    ),
                    Text(
                      "Batas kepemilikan saham",
                      style: TextStyle(
                        color: Color(0xff218196),
                        fontSize: 10,
                        decoration: TextDecoration.underline,
                        decorationStyle: TextDecorationStyle.solid,
                      ),
                    )
                  ],
                ),
                onTap: () {
                  PopupHelper.showKepemilikanSaham(
                      context,
                      CurrencyFormatter.format(state.maxInvest),
                      CurrencyFormatter.format(state.portofolio),
                      CurrencyFormatter.format(
                          state.maxInvest - state.portofolio),
                      state.isUnlimited ? 1 : 0);
                },
              );
            } else if (state is InvestmentLimitLoading ||
                state is InvestmentLimitInitial) {
              return BoxShimmer(height: 15, width: 60, borderRadius: 15);
            } else {
              return Container();
            }
          }),
        ],
      ),
    );
  }
}
