import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:santaraapp/helpers/NavigatorHelper.dart';
import 'package:santaraapp/utils/rev_color.dart';

import '../../../../../utils/sizes.dart';
import '../../../../../widget/home/blocs/button.bloc.dart';
import '../../../../../widget/widget/components/main/SantaraBottomSheet.dart';
import '../../../../../widget/widget/components/main/SantaraButtons.dart';
import '../../../../../widget/widget/components/main/SantaraField.dart';
import '../../../../payment/presentation/pages/payment_page.dart';
import '../bloc/investment_limit/investment_limit_bloc.dart';
import '../widgets/balance_information.dart';
import '../widgets/deposit_tnc.dart';

class DepositPage extends StatefulWidget {
  @override
  _DepositPageState createState() => _DepositPageState();
}

class _DepositPageState extends State<DepositPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _depositAmount = MoneyMaskedTextController(
      decimalSeparator: '', thousandSeparator: ',', precision: 0);
  // num limit = 0;
  bool isUnlimited = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener<InvestmentLimitBloc, InvestmentLimitState>(
      listener: (context, state) {
        if (state is InvestmentLimitError) {
          if (state.failure.apiStatus == 401) {
            NavigatorHelper.pushToExpiredSession(context);
          }
        }
      },
      child: BlocBuilder<InvestmentLimitBloc, InvestmentLimitState>(
        builder: (context, state) {
          if (state is InvestmentLimitLoaded) {
            // limit = state.maxInvest - state.portofolio;
            isUnlimited = state.isUnlimited;
          }
          return ModalProgressHUD(
              inAsyncCall: state is InvestmentLimitLoading,
              progressIndicator: CupertinoActivityIndicator(),
              color: Color(ColorRev.mainBlack),
              child: Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.all(Sizes.s20),
                  children: [
                    BalanceInformation(),
                    SizedBox(height: Sizes.s15),
                    SantaraTextField(
                      hintText: "Jumlah Deposit",
                      labelText: "Jumlah Deposit",
                      controller: _depositAmount,
                      prefixText: 'Rp ',
                      inputType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Jumlah deposit tidak boleh kosong';
                        } else if (num.parse(value.replaceAll(",", "")) <
                            100000) {
                          return 'Jumlah deposit kurang dari ketentuan min. deposit';
                          // } else if (!isUnlimited &&
                          //     num.parse(value.replaceAll(",", "")) > limit) {
                          //   return 'Jumlah deposit melebihi sisa batas kepemilikan investasi';
                        } else {
                          return null;
                        }
                      },
                    ),
                    SizedBox(height: Sizes.s15),
                    DepositTnc(),
                    SizedBox(height: Sizes.s15),
                    BlocBuilder<InvestmentLimitBloc, InvestmentLimitState>(
                        builder: (context, state) {
                      return BlocBuilder<ButtonBloc, ButtonState>(
                        builder: (context, stateButton) {
                          return SantaraMainButton(
                              title: "Pilih Metode Pembayaran",
                              disableColor: Color(0xFFF5F5F5),
                              isActive: state is InvestmentLimitLoaded ||
                                  stateButton is ButtonLoaded,
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  if (stateButton is ButtonLoaded) {
                                    stateButton.verified ==
                                            ButtonStatus.verified
                                        ? Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) => PaymentPage(
                                                      total: _depositAmount.text
                                                          .replaceAll(",", ""),
                                                      type:
                                                          TransactionPaymentType
                                                              .deposit,
                                                    )))
                                        : SantaraBottomSheet.show(
                                            context,
                                            "Maaf, akun anda belum terverifikasi.",
                                          );
                                  }
                                }
                              });
                        },
                      );
                    }),
                  ],
                ),
              ));
        },
      ),
    );
  }
}
