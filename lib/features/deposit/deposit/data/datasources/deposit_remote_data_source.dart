import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/http/rest_client.dart';
import '../../../../../helpers/RestHelper.dart';
import '../models/saldo_deposit_model.dart';

abstract class DepositRemoteDataSource {
  Future<SaldoDepositModel> getSaldoDeposit();
}

class DepositRemoteDataSourceImpl implements DepositRemoteDataSource {
  final RestClient client;
  DepositRemoteDataSourceImpl({@required this.client});

  @override
  Future<SaldoDepositModel> getSaldoDeposit() async {
    try {
      final result = await client.get(path: "/traders/idr");
      if (result.statusCode == 200) {
        return saldoDepositModelFromJson(jsonEncode(result.data));
      } else {
        throw RestHelper.throwServerError(result);
      }
    } on DioError catch (error) {
      throw RestHelper.catchFailure(error: error);
    } on Failure catch (e, stack) {
      throw RestHelper.throwFailure(e, stack: stack);
    } catch (e, stack) {
      throw RestHelper.throwLocalError(error: e, stack: stack);
    }
  }
}
