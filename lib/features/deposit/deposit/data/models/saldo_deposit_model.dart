import 'dart:convert';

SaldoDepositModel saldoDepositModelFromJson(String str) =>
    SaldoDepositModel.fromJson(json.decode(str));

String saldoDepositModelToJson(SaldoDepositModel data) =>
    json.encode(data.toJson());

class SaldoDepositModel {
  SaldoDepositModel({
    this.idr,
    this.refund,
    this.pending,
  });

  double idr;
  bool refund;
  double pending;

  factory SaldoDepositModel.fromJson(Map<String, dynamic> json) =>
      SaldoDepositModel(
        idr: json["idr"] == null ? 0 : json["idr"] / 1,
        refund: json["refund"],
        pending: json["pending"] == null ? 0 : json["pending"] / 1,
      );

  Map<String, dynamic> toJson() => {
        "idr": idr,
        "refund": refund,
        "pending": pending,
      };
}
