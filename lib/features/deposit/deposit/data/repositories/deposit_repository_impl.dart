import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/http/network_info.dart';
import '../../domain/repositories/deposit_repository.dart';
import '../datasources/deposit_remote_data_source.dart';
import '../models/saldo_deposit_model.dart';

class DepositRepositoryImpl implements DepositRepository {
  final NetworkInfo networkInfo;
  final DepositRemoteDataSource remoteDataSource;

  DepositRepositoryImpl({
    @required this.networkInfo,
    @required this.remoteDataSource,
  });
  @override
  Future<Either<Failure, SaldoDepositModel>> getSaldoDeposit() async {
    if (await networkInfo.isConnected) {
      try {
        final data = await remoteDataSource.getSaldoDeposit();
        return Right(data);
      } on Failure catch (failure) {
        return Left(failure);
      }
    } else {
      return Left(Failure(type: FailureType.noConnection));
    }
  }
}
