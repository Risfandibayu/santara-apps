import 'package:dartz/dartz.dart';

import '../../../../../core/error/failure.dart';
import '../../data/models/saldo_deposit_model.dart';

abstract class DepositRepository {
  Future<Either<Failure, SaldoDepositModel>> getSaldoDeposit();
}
