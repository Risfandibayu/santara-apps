import 'package:dartz/dartz.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../data/models/saldo_deposit_model.dart';
import '../repositories/deposit_repository.dart';

class GetSaldoDeposit implements UseCase<SaldoDepositModel, NoParams> {
  final DepositRepository repository;
  GetSaldoDeposit(this.repository);

  @override
  Future<Either<Failure, SaldoDepositModel>> call(NoParams params) async {
    return await repository.getSaldoDeposit();
  }
}
