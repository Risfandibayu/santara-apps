import '../../core/data/datasources/max_invest_data_source.dart';
import '../../core/data/repositories/max_invest_repository_impl.dart';
import '../../core/domain/repositories/max_invest_repository.dart';
import '../../core/domain/usecases/max_invest_usecase.dart';
import '../../injector_container.dart';
import 'deposit/data/datasources/deposit_remote_data_source.dart';
import 'deposit/data/repositories/deposit_repository_impl.dart';
import 'deposit/domain/repositories/deposit_repository.dart';
import 'deposit/domain/usecases/deposit_usecases.dart';
import 'deposit/presentation/bloc/investment_limit/investment_limit_bloc.dart';
import 'deposit/presentation/bloc/saldo_deposit/saldo_deposit_bloc.dart';
import 'deposit_history/data/datasources/deposit_history_remote_data_source.dart';
import 'deposit_history/data/repositories/deposit_history_repository_impl.dart';
import 'deposit_history/domain/repositories/deposit_history_repository.dart';
import 'deposit_history/domain/usecases/deposit_history_usecase.dart';
import 'deposit_history/presentation/bloc/deposit_history_bloc.dart';

void initDepositInjector() {
  // Bloc
  sl.registerFactory(() => SaldoDepositBloc(
      getUserByUuid: sl(), getSaldoDeposit: sl(), getAllFee: sl()));
  sl.registerFactory(() => DepositHistoryBloc(getDepositHistory: sl()));
  sl.registerFactory(() => InvestmentLimitBloc(
      getMaxInvest: sl(), getPortofolioList: sl(), getUserByUuid: sl()));

  //! Global Usecase

  sl.registerLazySingleton(() => GetSaldoDeposit(sl()));
  sl.registerLazySingleton(() => GetDepositHistory(sl()));
  sl.registerLazySingleton(() => GetMaxInvest(sl()));

  //! Global Repository
  sl.registerLazySingleton<DepositRepository>(
    () => DepositRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<DepositHistoryRepository>(
    () => DepositHistoryRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<MaxInvestRepository>(
    () => MaxInvestRepositoryImpl(
      networkInfo: sl(),
      dataSource: sl(),
    ),
  );

  //! Global Data Source
  sl.registerLazySingleton<DepositRemoteDataSource>(
    () => DepositRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<DepositHistoryRemoteDataSource>(
    () => DepositHistoryRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<MaxInvestRemoteDataSource>(
    () => MaxInvestRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}
