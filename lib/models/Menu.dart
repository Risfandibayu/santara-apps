import 'dart:convert';

Menu menuFromJson(String str) => Menu.fromJson(json.decode(str));

String menuToJson(Menu data) => json.encode(data.toJson());

class Menu {
    String about;
    String caraInvestasi;
    String berita;
    String faq;
    String sKPemodal;
    String sKPenerbit;
    String kebijakan;
    String disclaimer;

    Menu({
        this.about,
        this.caraInvestasi,
        this.berita,
        this.faq,
        this.sKPemodal,
        this.sKPenerbit,
        this.kebijakan,
        this.disclaimer,
    });

    factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        about: json["about"],
        caraInvestasi: json["cara_investasi"],
        berita: json["berita"],
        faq: json["faq"],
        sKPemodal: json["s&k_pemodal"],
        sKPenerbit: json["s&k_penerbit"],
        kebijakan: json["kebijakan"],
        disclaimer: json["disclaimer"],
    );

    Map<String, dynamic> toJson() => {
        "about": about,
        "cara_investasi": caraInvestasi,
        "berita": berita,
        "faq": faq,
        "s&k_pemodal": sKPemodal,
        "s&k_penerbit": sKPenerbit,
        "kebijakan": kebijakan,
        "disclaimer": disclaimer,
    };
}
