// end status kyc ( hasil akhir dari method checking company / checking individual )
enum EndStatus { verifying, verified, rejected, uncomplete, empty }

class StatusKycTrader {
  String statusKyc1;
  String statusKyc2;
  String statusKyc3;
  String statusKyc4;
  String statusKyc5;
  String statusKyc6;
  String statusKyc7;
  String statusKyc8;

  StatusKycTrader({
    this.statusKyc1,
    this.statusKyc2,
    this.statusKyc3,
    this.statusKyc4,
    this.statusKyc5,
    this.statusKyc6,
    this.statusKyc7,
    this.statusKyc8,
  });

  StatusKycTrader.fromJson(Map<String, dynamic> json) {
    statusKyc1 = json['status_kyc1'];
    statusKyc2 = json['status_kyc2'];
    statusKyc3 = json['status_kyc3'];
    statusKyc4 = json['status_kyc4'];
    statusKyc5 = json['status_kyc5'];
    statusKyc6 = json['status_kyc6'];
    statusKyc7 = json['status_kyc7'];
    statusKyc8 = json['status_kyc8'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_kyc1'] = this.statusKyc1;
    data['status_kyc2'] = this.statusKyc2;
    data['status_kyc3'] = this.statusKyc3;
    data['status_kyc4'] = this.statusKyc4;
    data['status_kyc5'] = this.statusKyc5;
    data['status_kyc6'] = this.statusKyc6;
    data['status_kyc7'] = this.statusKyc7;
    data['status_kyc8'] = this.statusKyc8;
    return data;
  }
}
