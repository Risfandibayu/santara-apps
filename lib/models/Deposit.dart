import 'dart:convert';

List<Deposit> depositFromJson(String str) =>
    new List<Deposit>.from(json.decode(str).map((x) => Deposit.fromJson(x)));

String depositToJson(List<Deposit> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Deposit {
  int id;
  String uuid;
  int amount;
  int status;
  dynamic confirmationPhoto;
  int traderId;
  dynamic verifiedAt;
  dynamic verifiedBy;
  dynamic bankTo;
  String bankFrom;
  String accountNumber;
  String createdAt;
  String updatedAt;
  int isDeleted;
  String fee;
  String chanel;
  String createdBy;
  String updatedBy;
  VirtualAccount virtualAccount;

  Deposit({
    this.id,
    this.uuid,
    this.amount,
    this.status,
    this.confirmationPhoto,
    this.traderId,
    this.verifiedAt,
    this.verifiedBy,
    this.bankTo,
    this.bankFrom,
    this.accountNumber,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
    this.createdBy,
    this.updatedBy,
    this.fee,
    this.chanel,
    this.virtualAccount,
  });

  factory Deposit.fromJson(Map<String, dynamic> json) {
    var virtualAccount;
    if (json["virtual_account"] != null) {
      virtualAccount = VirtualAccount.fromJson(json["virtual_account"]);
    }
    return new Deposit(
      id: json["id"],
      uuid: json["uuid"],
      amount: json["amount"].toInt(),
      status: json["status"],
      confirmationPhoto: json["confirmation_photo"],
      traderId: json["trader_id"],
      verifiedAt: json["verified_at"],
      verifiedBy: json["verified_by"],
      bankTo: json["bank_to"],
      bankFrom: json["bank_from"],
      accountNumber: json["account_number"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      isDeleted: json["is_deleted"],
      createdBy: json["created_by"],
      updatedBy: json["updated_by"],
      fee: json["fee"],
      chanel: json["channel"],
      virtualAccount: virtualAccount,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "amount": amount,
        "status": status,
        "confirmation_photo": confirmationPhoto,
        "trader_id": traderId,
        "verified_at": verifiedAt,
        "verified_by": verifiedBy,
        "bank_to": bankTo,
        "bank_from": bankFrom,
        "account_number": accountNumber,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "fee": fee,
        "channel": chanel,
        "virtual_account": virtualAccount.toJson(),
      };
}

class VirtualAccount {
  String name;
  String merchantCode;
  String accountNumber;

  VirtualAccount({
    this.name,
    this.merchantCode,
    this.accountNumber,
  });

  factory VirtualAccount.fromJson(Map<String, dynamic> json) =>
      new VirtualAccount(
        name: json["name"],
        merchantCode: json["merchant_code"],
        accountNumber: json["account_number"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "merchant_code": merchantCode,
        "account_number": accountNumber,
      };
}
