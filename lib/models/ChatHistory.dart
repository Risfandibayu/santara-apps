// To parse this JSON data, do
//
//     final chatHistory = chatHistoryFromJson(jsonString);

import 'dart:convert';

List<ChatHistory> chatHistoryFromJson(String str) => List<ChatHistory>.from(json.decode(str).map((x) => ChatHistory.fromJson(x)));

String chatHistoryToJson(List<ChatHistory> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ChatHistory {
    String uuid;
    String title;
    String status;
    String priority;
    String comment;
    String createdAt;
    String updatedAt;
    String createdBy;
    String updatedBy;

    ChatHistory({
        this.uuid,
        this.title,
        this.status,
        this.priority,
        this.comment,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy,
    });

    factory ChatHistory.fromJson(Map<String, dynamic> json) => ChatHistory(
        uuid: json["uuid"],
        title: json["title"],
        status: json["status"],
        priority: json["priority"],
        comment: json["comment"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "title": title,
        "status": status,
        "priority": priority,
        "comment": comment,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
