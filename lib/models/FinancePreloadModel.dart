class FinancePreloadModel {
  String businessName;
  String url;
  bool notifyFinancialReport;
  bool notifyFundsPlan;

  FinancePreloadModel(
      {this.businessName,
      this.url,
      this.notifyFinancialReport,
      this.notifyFundsPlan});

  FinancePreloadModel.fromJson(Map<String, dynamic> json) {
    businessName = json['business_name'];
    url = json['url'];
    notifyFinancialReport = json['notify_financial_report'];
    notifyFundsPlan = json['notify_funds_plan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['business_name'] = this.businessName;
    data['url'] = this.url;
    data['notify_financial_report'] = this.notifyFinancialReport;
    data['notify_funds_plan'] = this.notifyFundsPlan;
    return data;
  }
}
