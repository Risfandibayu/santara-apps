import 'dart:convert';

List<Transactions> welcomeFromJson(String str) => new List<Transactions>.from(json.decode(str).map((x) => Transactions.fromJson(x)));

String welcomeToJson(List<Transactions> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Transactions {
    int id;
    String uuid;
    int traderId;
    int amount;
    int emitenId;
    dynamic verificationPhoto;
    String pictures;
    String merchantCode;
    String accountNumber;
    String name;
    String emiten;
    String status;

    Transactions({
        this.id,
        this.uuid,
        this.traderId,
        this.amount,
        this.emitenId,
        this.verificationPhoto,
        this.pictures,
        this.merchantCode,
        this.accountNumber,
        this.name,
        this.emiten,
        this.status,
    });

    factory Transactions.fromJson(Map<String, dynamic> json) => new Transactions(
        id: json["id"],
        uuid: json["uuid"],
        traderId: json["trader_id"],
        amount: json["amount"],
        emitenId: json["emiten_id"],
        verificationPhoto: json["verification_photo"],
        pictures: json["pictures"],
        merchantCode: json["merchant_code"],
        accountNumber: json["account_number"],
        name: json["name"],
        emiten: json["emiten"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "trader_id": traderId,
        "amount": amount,
        "emiten_id": emitenId,
        "verification_photo": verificationPhoto,
        "pictures": pictures,
        "merchant_code": merchantCode,
        "account_number": accountNumber,
        "name": name,
        "emiten": emiten,
        "status": status,
    };
}