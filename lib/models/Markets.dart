// To parse this JSON data, do
//
//     final markets = marketsFromJson(jsonString);

import 'dart:convert';

List<Markets> marketsFromJson(String str) => new List<Markets>.from(json.decode(str).map((x) => Markets.fromJson(x)));

String marketsToJson(List<Markets> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Markets {
    int id;
    String uuid;
    dynamic seller;
    int traderId;
    int emitenId;
    String stock;
    String type;
    int status;
    String createdAt;
    String updatedAt;
    dynamic buyer;
    int isCanceled;

    Markets({
        this.id,
        this.uuid,
        this.seller,
        this.traderId,
        this.emitenId,
        this.stock,
        this.type,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.buyer,
        this.isCanceled,
    });

    factory Markets.fromJson(Map<String, dynamic> json) => new Markets(
        id: json["id"],
        uuid: json["uuid"],
        seller: json["seller"],
        traderId: json['trader_id'],
        emitenId: json["emiten_id"],
        stock: json["stock"],
        type: json["type"],
        status: json["status"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        buyer: json["buyer"],
        isCanceled: json["is_canceled"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "seller": seller,
        "trader_id": traderId,
        "emiten_id": emitenId,
        "stock": stock,
        "type": type,
        "status": status,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "buyer": buyer,
        "is_canceled": isCanceled,
    };
}
