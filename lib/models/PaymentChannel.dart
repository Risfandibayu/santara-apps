import 'dart:convert';

PaymentChannel paymentChannelFromJson(String str) => PaymentChannel.fromJson(json.decode(str));

String paymentChannelToJson(PaymentChannel data) => json.encode(data.toJson());

class PaymentChannel {
    List<VirtualAccount> virtualAccounts;
    List<BankTransfer> bankTransfer;

    PaymentChannel({
        this.virtualAccounts,
        this.bankTransfer,
    });

    factory PaymentChannel.fromJson(Map<String, dynamic> json) => PaymentChannel(
        virtualAccounts: List<VirtualAccount>.from(json["virtual_accounts"].map((x) => VirtualAccount.fromJson(x))),
        bankTransfer: List<BankTransfer>.from(json["bank_transfer"].map((x) => BankTransfer.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "virtual_accounts": List<dynamic>.from(virtualAccounts.map((x) => x.toJson())),
        "bank_transfer": List<dynamic>.from(bankTransfer.map((x) => x.toJson())),
    };
}

class BankTransfer {
    String provider;
    String accountNumber;
    String accountName;

    BankTransfer({
        this.provider,
        this.accountNumber,
        this.accountName,
    });

    factory BankTransfer.fromJson(Map<String, dynamic> json) => BankTransfer(
        provider: json["provider"],
        accountNumber: json["account_number"],
        accountName: json["account_name"],
    );

    Map<String, dynamic> toJson() => {
        "provider": provider,
        "account_number": accountNumber,
        "account_name": accountName,
    };
}

class VirtualAccount {
    String channel;
    String bank;

    VirtualAccount({
        this.channel,
        this.bank,
    });

    factory VirtualAccount.fromJson(Map<String, dynamic> json) => VirtualAccount(
        channel: json["channel"],
        bank: json["bank"],
    );

    Map<String, dynamic> toJson() => {
        "channel": channel,
        "bank": bank,
    };
}
