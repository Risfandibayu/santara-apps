class ValidationResultModel {
  String message;
  String field;
  String validation;

  ValidationResultModel({this.message, this.field, this.validation});

  ValidationResultModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    field = json['field'];
    validation = json['validation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['field'] = this.field;
    data['validation'] = this.validation;
    return data;
  }
}
