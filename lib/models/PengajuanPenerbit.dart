import 'dart:convert';

List<PengajuanPenerbit> pengajuanPenerbitFromJson(String str) => List<PengajuanPenerbit>.from(json.decode(str).map((x) => PengajuanPenerbit.fromJson(x)));

String pengajuanPenerbitToJson(List<PengajuanPenerbit> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PengajuanPenerbit {
    String numberSubmission;
    String trademark;
    String status;
    String owner;
    String createdAt;
    String note;

    PengajuanPenerbit({
        this.numberSubmission,
        this.trademark,
        this.status,
        this.owner,
        this.createdAt,
        this.note,
    });

    factory PengajuanPenerbit.fromJson(Map<String, dynamic> json) => PengajuanPenerbit(
        numberSubmission: json["number_submission"],
        trademark: json["trademark"],
        status: json["status"],
        owner: json["owner"],
        createdAt: json["created_at"],
        note: json["note"],
    );

    Map<String, dynamic> toJson() => {
        "number_submission": numberSubmission,
        "trademark": trademark,
        "status": status,
        "owner": owner,
        "created_at": createdAt,
        "note": note,
    };
}
