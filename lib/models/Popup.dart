// To parse this JSON data, do
//
//     final popup = popupFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/utils/api.dart';

List<Popup> popupFromJson(String str) =>
    List<Popup>.from(json.decode(str).map((x) => Popup.fromJson(x)));

String popupToJson(List<Popup> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Popup {
  String uuid;
  String title;
  String type;
  String actionText;
  String mobilePict;
  String mobileUrl;
  String websitePict;
  String websiteUrl;
  String emitenUuid;
  String startDate;
  String finishDate;

  Popup({
    this.uuid,
    this.title,
    this.type,
    this.actionText,
    this.mobilePict,
    this.mobileUrl,
    this.websitePict,
    this.websiteUrl,
    this.emitenUuid,
    this.startDate,
    this.finishDate,
  });

  factory Popup.fromJson(Map<String, dynamic> json) => Popup(
        uuid: json["uuid"],
        title: json["title"],
        type: json["type"],
        actionText: json["action_text"],
        mobilePict: json["mobile_pict"].contains("http")
            ? json["mobile_pict"]
            : apiLocalImage + "/uploads/popup/" + json["mobile_pict"],
        mobileUrl: json["mobile_url"],
        websitePict: json["website_pict"].contains("http")
            ? json["website_pict"]
            : apiLocalImage + "/uploads/popup/" + json["website_pict"],
        websiteUrl: json["website_url"],
        emitenUuid: json["emiten_uuid"],
        startDate: json["start_date"],
        finishDate: json["finish_date"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "title": title,
        "type": type,
        "action_text": actionText,
        "mobile_pict": mobilePict,
        "mobile_url": mobileUrl,
        "website_pict": websitePict,
        "website_url": websiteUrl,
        "emiten_uuid": emitenUuid,
        "start_date": startDate,
        "finish_date": finishDate,
      };
}
