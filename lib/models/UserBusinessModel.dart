class UserBusinessModel {
  String uuid;
  String url;
  String companyName;
  String trademark;
  String category;
  String picture;
  int dayRemaining;
  int totalInvestor;
  LastReport lastReport;
  double percent;
  String reportDeadline;
  String status;

  UserBusinessModel({
    this.uuid,
    this.url,
    this.companyName,
    this.trademark,
    this.category,
    this.picture,
    this.dayRemaining,
    this.totalInvestor,
    this.lastReport,
    this.percent,
    this.reportDeadline,
    this.status,
  });

  UserBusinessModel.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    url = json['url'];
    companyName = json['company_name'];
    trademark = json['trademark'];
    category = json['category'];
    picture = json['picture'];
    dayRemaining = json["day_remaining"];
    totalInvestor = json["total_investor"];
    lastReport = json['last_report'] != null
        ? LastReport.fromJson(json['last_report'])
        : null;
    percent = double.parse(json['percent'] == null ? "0.0" : json['percent']);
    reportDeadline = json['report_deadline'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this.uuid;
    data['url'] = this.url;
    data['company_name'] = this.companyName;
    data['trademark'] = this.trademark;
    data['category'] = this.category;
    data['day_remaining'] = this.dayRemaining;
    data['total_investor'] = this.totalInvestor;
    data['picture'] = this.picture;
    if (this.lastReport != null) {
      data['last_report'] = this.lastReport.toJson();
    }
    data['percent'] = this.percent;
    data['report_deadline'] = this.reportDeadline;
    data['status'] = this.status;
    return data;
  }
}

class LastReport {
  String date;
  int salesRevenue;
  int netProfit;

  LastReport({this.date, this.salesRevenue, this.netProfit});

  LastReport.fromJson(Map<String, dynamic> json) {
    date = json['updated_at'];
    salesRevenue = json['sales_revenue'];
    netProfit = json['net_profit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updated_at'] = this.date;
    data['sales_revenue'] = this.salesRevenue;
    data['net_profit'] = this.netProfit;
    return data;
  }
}
