class FieldErrorStatus {
  int id;
  String fieldId;
  String stepId;
  int status;
  int isEdited;
  String error;
  int kycSubmissionId;
  String createdAt;
  String updatedAt;

  FieldErrorStatus(
      {this.id,
      this.fieldId,
      this.stepId,
      this.status,
      this.isEdited,
      this.error,
      this.kycSubmissionId,
      this.createdAt,
      this.updatedAt});

  FieldErrorStatus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fieldId = json['field_id'];
    stepId = json['step_id'];
    status = json['status'];
    isEdited = json['is_edited'];
    error = json['error'];
    kycSubmissionId = json['kyc_submission_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['field_id'] = this.fieldId;
    data['step_id'] = this.stepId;
    data['status'] = this.status;
    data['is_edited'] = this.isEdited;
    data['error'] = this.error;
    data['kyc_submission_id'] = this.kycSubmissionId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
