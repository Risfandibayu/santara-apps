// To parse this JSON data, do
import 'dart:convert';

List<CompanyType> companyTypeFromJson(String str) => List<CompanyType>.from(json.decode(str).map((x) => CompanyType.fromJson(x)));

String companyTypeToJson(List<CompanyType> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CompanyType {
    int id;
    String type;
    String code;

    CompanyType({
        this.id,
        this.type,
        this.code,
    });

    factory CompanyType.fromJson(Map<String, dynamic> json) => CompanyType(
        id: json["id"],
        type: json["type"],
        code: json["code"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "code": code,
    };
}
