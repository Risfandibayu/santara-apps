import 'dart:convert';

List<Testimonial> testimonialFromJson(String str) => List<Testimonial>.from(json.decode(str).map((x) => Testimonial.fromJson(x)));

String testimonialToJson(List<Testimonial> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Testimonial {
    String title;
    String subtitle;
    String description;
    String image;

    Testimonial({
        this.title,
        this.subtitle,
        this.description,
        this.image,
    });

    factory Testimonial.fromJson(Map<String, dynamic> json) => Testimonial(
        title: json["title"],
        subtitle: json["subtitle"],
        description: json["description"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "subtitle": subtitle,
        "description": description,
        "image": image,
    };
}
