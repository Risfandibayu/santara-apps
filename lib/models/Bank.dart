// To parse this JSON data, do
//
//     final bank = bankFromJson(jsonString);

import 'dart:convert';

List<Bank> bankFromJson(String str) => List<Bank>.from(json.decode(str).map((x) => Bank.fromJson(x)));

String bankToJson(List<Bank> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Bank {
    int id;
    String uuid;
    String provider;
    String accountNumber;
    String accountName;
    dynamic regency;
    dynamic branch;
    String createdAt;
    String updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;

    Bank({
        this.id,
        this.uuid,
        this.provider,
        this.accountNumber,
        this.accountName,
        this.regency,
        this.branch,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        uuid: json["uuid"],
        provider: json["provider"],
        accountNumber: json["account_number"],
        accountName: json["account_name"],
        regency: json["regency"],
        branch: json["branch"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "provider": provider,
        "account_number": accountNumber,
        "account_name": accountName,
        "regency": regency,
        "branch": branch,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
