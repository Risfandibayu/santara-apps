import 'dart:convert';

Deviden devidenFromJson(String str) => Deviden.fromJson(json.decode(str));

String devidenToJson(Deviden data) => json.encode(data.toJson());

class Deviden {
  int status;
  String message;
  dynamic meta;
  List<Datum> data;

  Deviden({
    this.status,
    this.message,
    this.meta,
    this.data,
  });

  factory Deviden.fromJson(Map<String, dynamic> json) => Deviden(
        status: json["status"],
        message: json["message"],
        meta: json["meta"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "meta": meta,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  int id;
  int traderId;
  int emitenId;
  double devidend;
  int fee;
  int status;
  String bank;
  String accountNumber;
  String accountName;
  String bankKota;
  String bankCabang;
  int isDeleted;
  DateTime createdAt;
  DateTime updatedAt;
  String codeEmiten;
  String name;

  Datum({
    this.id,
    this.traderId,
    this.emitenId,
    this.devidend,
    this.fee,
    this.status,
    this.bank,
    this.accountNumber,
    this.accountName,
    this.bankKota,
    this.bankCabang,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.codeEmiten,
    this.name,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        traderId: json["trader_id"],
        emitenId: json["emiten_id"],
        devidend: json["devidend"] != null ? json["devidend"].toDouble() : 0.0,
        fee: json["fee"],
        status: json["status"],
        bank: json["bank"],
        accountNumber: json["account_number"],
        accountName: json["account_name"],
        bankKota: json["bank_kota"],
        bankCabang: json["bank_cabang"],
        isDeleted: json["is_deleted"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        codeEmiten: json["code_emiten"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "trader_id": traderId,
        "emiten_id": emitenId,
        "devidend": devidend,
        "fee": fee,
        "status": status,
        "bank": bank,
        "account_number": accountNumber,
        "account_name": accountName,
        "bank_kota": bankKota,
        "bank_cabang": bankCabang,
        "is_deleted": isDeleted,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "code_emiten": codeEmiten,
        "name": name,
      };
}
