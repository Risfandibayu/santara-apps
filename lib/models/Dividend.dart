import 'dart:convert';

Dividend dividendFromJson(String str) => Dividend.fromJson(json.decode(str));

String dividendToJson(Dividend data) => json.encode(data.toJson());

class Dividend {
    double total;
    List<Datum> data;

    Dividend({
        this.total,
        this.data,
    });

    factory Dividend.fromJson(Map<String, dynamic> json) => Dividend(
        total: json["total"]/1,
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    String companyName;
    String codeEmiten;
    double devidend;
    int status;
    String createdAt;
    String updatedAt;
    String disbursement;

    Datum({
        this.companyName,
        this.codeEmiten,
        this.devidend,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.disbursement,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        companyName: json["company_name"],
        codeEmiten: json["code_emiten"],
        devidend: json["devidend"]/1,
        status: json["status"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        disbursement: json["disbursement"],
    );

    Map<String, dynamic> toJson() => {
        "company_name": companyName,
        "code_emiten": codeEmiten,
        "devidend": devidend,
        "status": status,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "disbursement": disbursement,
    };
}
