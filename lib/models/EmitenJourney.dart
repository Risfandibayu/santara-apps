import 'dart:convert';

List<EmitenJourney> emitenJourneyFromJson(String str) => List<EmitenJourney>.from(json.decode(str).map((x) => EmitenJourney.fromJson(x)));

String emitenJourneyToJson(List<EmitenJourney> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmitenJourney {
    String title;
    String date;

    EmitenJourney({
        this.title,
        this.date,
    });

    factory EmitenJourney.fromJson(Map<String, dynamic> json) => EmitenJourney(
        title: json["title"],
        date: json["date"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "date": date,
    };
}
