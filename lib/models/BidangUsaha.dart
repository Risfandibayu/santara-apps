// To parse this JSON data, do
//
//     final bidangUsaha = bidangUsahaFromJson(jsonString);

import 'dart:convert';

List<BidangUsaha> bidangUsahaFromJson(String str) => new List<BidangUsaha>.from(json.decode(str).map((x) => BidangUsaha.fromJson(x)));

String bidangUsahaToJson(List<BidangUsaha> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class BidangUsaha {
    int id;
    String uuid;
    String category;
    String createdAt;
    dynamic updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;

    BidangUsaha({
        this.id,
        this.uuid,
        this.category,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory BidangUsaha.fromJson(Map<String, dynamic> json) => new BidangUsaha(
        id: json["id"],
        uuid: json["uuid"],
        category: json["category"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "category": category,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
