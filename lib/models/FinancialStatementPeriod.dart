class FinancialStatementPeriod {
  int id;
  String periode;
  double salesRevenue;
  double netIncome;
  int editable;
  String status;
  String financeReport;

  FinancialStatementPeriod(
      {this.id,
      this.periode,
      this.salesRevenue,
      this.netIncome,
      this.editable,
      this.status,
      this.financeReport});

  FinancialStatementPeriod.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    periode = json['periode'];
    salesRevenue =
        json['sales_revenue'] == null ? null : json['sales_revenue'] / 1;
    netIncome = json['net_income'] == null ? null : json['net_income'] / 1;
    editable = json['editable'];
    status = json['status'];
    financeReport = json['finance_report'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['periode'] = this.periode;
    data['sales_revenue'] = this.salesRevenue / 1;
    data['net_income'] = this.netIncome / 1;
    data['editable'] = this.editable;
    data['status'] = this.status;
    data['finance_report'] = this.financeReport;
    return data;
  }
}
