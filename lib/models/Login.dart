import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
    Token token;
    User user;

    Login({
        this.token,
        this.user,
    });

    factory Login.fromJson(Map<String, dynamic> json) => Login(
        token: Token.fromJson(json["token"]),
        user: User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "token": token.toJson(),
        "user": user.toJson(),
    };
}

class Token {
    String type;
    String token;
    String refreshToken;

    Token({
        this.type,
        this.token,
        this.refreshToken,
    });

    factory Token.fromJson(Map<String, dynamic> json) => Token(
        type: json["type"],
        token: json["token"],
        refreshToken: json["refreshToken"],
    );

    Map<String, dynamic> toJson() => {
        "type": type,
        "token": token,
        "refreshToken": refreshToken,
    };
}

class User {
    int id;
    String uuid;
    String email;
    int isVerified;
    int isOtp;
    bool isLoggedIn;
    DateTime updatedAt;
    TraderBank traderBank;
    int isResetPassword;
    int isVerifiedKyc;

    User({
        this.id,
        this.uuid,
        this.email,
        this.isVerified,
        this.isOtp,
        this.isLoggedIn,
        this.updatedAt,
        this.traderBank,
        this.isResetPassword,
        this.isVerifiedKyc
    });

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        uuid: json["uuid"],
        email: json["email"],
        isVerified: json["is_verified"],
        isOtp: json["is_otp"],
        isLoggedIn: json["is_logged_in"],
        updatedAt: DateTime.parse(json["updated_at"]),
        traderBank: json["trader_bank"] == null ? null : TraderBank.fromJson(json["trader_bank"]),
        isResetPassword: json["is_reset_password"],
        isVerifiedKyc: json["is_verified_kyc"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "email": email,
        "is_verified": isVerified,
        "is_otp": isOtp,
        "is_logged_in": isLoggedIn,
        "updated_at": updatedAt.toIso8601String(),
        "trader_bank": traderBank.toJson(),
        "is_reset_password": isResetPassword,
        "is_verified_kyc": isVerifiedKyc
    };
}

class TraderBank {
    int hasSubmitBank;

    TraderBank({
        this.hasSubmitBank,
    });

    factory TraderBank.fromJson(Map<String, dynamic> json) => TraderBank(
        hasSubmitBank: json["has_submit_bank"],
    );

    Map<String, dynamic> toJson() => {
        "has_submit_bank": hasSubmitBank,
    };
}
