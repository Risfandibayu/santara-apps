import 'dart:convert';

AraArbModel araArbModelFromJson(String str) =>
    AraArbModel.fromJson(json.decode(str));

String araArbModelToJson(AraArbModel data) => json.encode(data.toJson());

class AraArbModel {
  AraArbModel({
    this.meta,
    this.data,
  });

  Meta meta;
  Data data;

  factory AraArbModel.fromJson(Map<String, dynamic> json) => AraArbModel(
        meta: Meta.fromJson(json["meta"]),
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.ara,
    this.arb,
  });

  double ara;
  double arb;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        ara: json["ARA"] / 1,
        arb: json["ARB"] / 1,
      );

  Map<String, dynamic> toJson() => {
        "ARA": ara,
        "ARB": arb,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
