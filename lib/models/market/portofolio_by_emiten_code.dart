import 'dart:convert';

PortofolioByEmitenCode portofolioByEmitenCodeFromJson(String str) =>
    PortofolioByEmitenCode.fromJson(json.decode(str));

String portofolioByEmitenCodeToJson(PortofolioByEmitenCode data) =>
    json.encode(data.toJson());

class PortofolioByEmitenCode {
  PortofolioByEmitenCode({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Portofolio> data;

  factory PortofolioByEmitenCode.fromJson(Map<String, dynamic> json) =>
      PortofolioByEmitenCode(
        meta: Meta.fromJson(json["meta"]),
        data: List<Portofolio>.from(
            json["data"].map((x) => Portofolio.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Portofolio {
  Portofolio({
    this.emitenId,
    this.emitenUuid,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.total,
    this.pending,
    this.tradeable,
  });

  int emitenId;
  String emitenUuid;
  String companyName;
  String trademark;
  String codeEmiten;
  double total;
  double pending;
  bool tradeable;

  factory Portofolio.fromJson(Map<String, dynamic> json) => Portofolio(
        emitenId: json["emiten_id"],
        emitenUuid: json["emiten_uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
        total: json["total"] / 1,
        pending: json["pending"] / 1,
        tradeable: json["tradeable"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_id": emitenId,
        "emiten_uuid": emitenUuid,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "total": total,
        "pending": pending,
        "tradeable": tradeable,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
