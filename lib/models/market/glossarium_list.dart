import 'dart:convert';

GlossariumList glossariumListFromJson(String str) =>
    GlossariumList.fromJson(json.decode(str));

String glossariumListToJson(GlossariumList data) => json.encode(data.toJson());

class GlossariumList {
  GlossariumList({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Glossarium> data;

  factory GlossariumList.fromJson(Map<String, dynamic> json) => GlossariumList(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? []
            : List<Glossarium>.from(
                json["data"].map((x) => Glossarium.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Glossarium {
  Glossarium({
    this.id,
    this.uuid,
    this.title,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
  });

  int id;
  String uuid;
  String title;
  String description;
  DateTime createdAt;
  DateTime updatedAt;
  int isDeleted;

  factory Glossarium.fromJson(Map<String, dynamic> json) => Glossarium(
        id: json["id"],
        uuid: json["uuid"],
        title: json["title"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isDeleted: json["is_deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "title": title,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "is_deleted": isDeleted,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
