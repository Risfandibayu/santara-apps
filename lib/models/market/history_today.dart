import 'dart:convert';

import 'package:equatable/equatable.dart';

HistoryToday historyTodayFromJson(String str) =>
    HistoryToday.fromJson(json.decode(str));

String historyTodayToJson(HistoryToday data) => json.encode(data.toJson());

class HistoryToday extends Equatable {
  HistoryToday({
    this.meta,
    this.data,
  });

  final Meta meta;
  final List<History> data;

  factory HistoryToday.fromJson(Map<String, dynamic> json) => HistoryToday(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? []
            : List<History>.from(json["data"].map((x) => History.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };

  @override
  List<Object> get props => [meta, data];
}

class History extends Equatable {
  History({
    this.orderId,
    this.emitenUuid,
    this.docId,
    this.codeEmiten,
    this.companyName,
    this.traderId,
    this.startAmount,
    this.amount,
    this.createdAt,
    this.price,
    this.totalPrice,
    this.status,
    this.statusOrder,
    this.type,
  });

  final String orderId;
  final String emitenUuid;
  final String docId;
  final String codeEmiten;
  final String companyName;
  final int traderId;
  final int startAmount;
  final int amount;
  final DateTime createdAt;
  final int price;
  final int totalPrice;
  final bool status;
  final String statusOrder;
  final String type;

  factory History.fromJson(Map<String, dynamic> json) => History(
        orderId: json["order_id"],
        emitenUuid: json["emiten_uuid"],
        docId: json["doc_id"],
        codeEmiten: json["code_emiten"],
        companyName: json["company_name"],
        traderId: json["trader_id"],
        startAmount: json["start_amount"],
        amount: json["amount"],
        createdAt: DateTime.parse(json["created_at"]),
        price: json["price"],
        totalPrice: json["total_price"],
        status: json["status"],
        statusOrder: json["status_order"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "order_id": orderId,
        "emiten_uuid": emitenUuid,
        "doc_id": docId,
        "code_emiten": codeEmiten,
        "company_name": companyName,
        "trader_id": traderId,
        "start_amount": startAmount,
        "amount": amount,
        "created_at": createdAt.toIso8601String(),
        "price": price,
        "status": status,
        "status_order": statusOrder,
        "type": type,
      };

  @override
  List<Object> get props => [
        orderId,
        emitenUuid,
        docId,
        codeEmiten,
        traderId,
        startAmount,
        amount,
        createdAt,
        price,
        status,
        statusOrder,
        type,
      ];
}

class Meta extends Equatable {
  Meta({
    this.message,
  });

  final String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };

  @override
  List<Object> get props => [message];
}
