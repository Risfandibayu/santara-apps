import 'dart:convert';

MarketFraction marketFractionFromJson(String str) =>
    MarketFraction.fromJson(json.decode(str));

String marketFractionToJson(MarketFraction data) => json.encode(data.toJson());

class MarketFraction {
  MarketFraction({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Fraction> data;

  factory MarketFraction.fromJson(Map<String, dynamic> json) => MarketFraction(
        meta: Meta.fromJson(json["meta"]),
        data:
            List<Fraction>.from(json["data"].map((x) => Fraction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Fraction {
  Fraction({
    this.id,
    this.priceLower,
    this.priceCompareType,
    this.priceUpper,
    this.fraction,
    this.maxChange,
    this.isEnable,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int priceLower;
  String priceCompareType;
  int priceUpper;
  int fraction;
  int maxChange;
  bool isEnable;
  bool isDeleted;
  DateTime createdAt;
  DateTime updatedAt;

  factory Fraction.fromJson(Map<String, dynamic> json) => Fraction(
        id: json["id"],
        priceLower: json["price_lower"],
        priceCompareType: json["price_compare_type"],
        priceUpper: json["price_upper"],
        fraction: json["fraction"],
        maxChange: json["max_change"],
        isEnable: json["is_enable"],
        isDeleted: json["is_deleted"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "price_lower": priceLower,
        "price_compare_type": priceCompareType,
        "price_upper": priceUpper,
        "fraction": fraction,
        "max_change": maxChange,
        "is_enable": isEnable,
        "is_deleted": isDeleted,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  int currentPage;
  int lastPage;
  int count;
  int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        count: json["count"],
        recordPerPage: json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "last_page": lastPage,
        "count": count,
        "record_per_page": recordPerPage,
      };
}
