import 'dart:convert';

FinancialChart financialChartFromJson(String str) =>
    FinancialChart.fromJson(json.decode(str));

String financialChartToJson(FinancialChart data) => json.encode(data.toJson());

class FinancialChart {
  FinancialChart({
    this.meta,
    this.data,
  });

  Meta meta;
  List<DataChart> data;

  factory FinancialChart.fromJson(Map<String, dynamic> json) => FinancialChart(
        meta: Meta.fromJson(json["meta"]),
        data: List<DataChart>.from(
            json["data"].map((x) => DataChart.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataChart {
  DataChart({
    this.codeEmiten,
    this.turnover,
    this.ebitda,
    this.profit,
    this.date,
    this.year,
  });

  String codeEmiten;
  double turnover;
  double ebitda;
  double profit;
  DateTime date;
  int year;

  factory DataChart.fromJson(Map<String, dynamic> json) {
    List<String> data = json["date"].split("-");
    int year = num.parse(data[0]);
    int month = num.parse(
        data[1].toString()[0] == "0" ? data[1].toString()[1] : data[1]);
    int day = num.parse(
        data[2].toString()[0] == "0" ? data[2].toString()[1] : data[2]);
    return DataChart(
      codeEmiten: json["code_emiten"],
      turnover: json["turnover"],
      ebitda: json["ebitda"],
      profit: json["profit"],
      date: DateTime(year, month, day),
      year: json["year"],
    );
  }

  Map<String, dynamic> toJson() => {
        "code_emiten": codeEmiten,
        "turnover": turnover,
        "ebitda": ebitda,
        "profit": profit,
        "date": date,
        "year": year,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
