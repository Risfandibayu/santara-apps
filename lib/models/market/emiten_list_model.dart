import 'dart:convert';

import 'package:equatable/equatable.dart';

EmitenList emitenListFromJson(String str) =>
    EmitenList.fromJson(json.decode(str));

String emitenListToJson(EmitenList data) => json.encode(data.toJson());

class EmitenList extends Equatable {
  EmitenList({
    this.code,
    this.message,
    this.data,
    this.meta,
  });

  final int code;
  final String message;
  final List<Emiten> data;
  final Meta meta;

  factory EmitenList.fromJson(Map<String, dynamic> json) => EmitenList(
        code: json["code"],
        message: json["message"],
        data: json["data"] == null
            ? []
            : List<Emiten>.from(json["data"].map((x) => Emiten.fromJson(x))),
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "meta": meta.toJson(),
      };

  @override
  List<Object> get props => [code, message, data, meta];
}

class Emiten extends Equatable {
  Emiten({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.currentPrice,
    this.prevPrice,
    this.currentPriceData,
    this.isEmitenOwner,
  });

  final int id;
  final String uuid;
  final String companyName;
  final String trademark;
  final String codeEmiten;
  final num currentPrice;
  final num prevPrice;
  final bool isEmitenOwner;
  CurrentPriceData currentPriceData;

  factory Emiten.fromJson(Map<String, dynamic> json) {
    return Emiten(
      id: json["id"],
      uuid: json["uuid"],
      companyName: json["company_name"],
      trademark: json["trademark"],
      codeEmiten: json["code_emiten"],
      currentPrice: json["current_price"] == null ? 0 : json["current_price"],
      prevPrice: json["prev_price"] == null ? 0 : json["prev_price"],
      isEmitenOwner: json["is_emiten_owner"],
      currentPriceData: CurrentPriceData(),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "currentPriceData": currentPriceData.toJson()
      };

  @override
  List<Object> get props => [id, uuid, companyName, trademark, codeEmiten];
}

class Meta extends Equatable {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  final int currentPage;
  final int lastPage;
  final int count;
  final int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        count: json["count"],
        recordPerPage: json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "last_page": lastPage,
        "count": count,
        "record_per_page": recordPerPage,
      };

  @override
  List<Object> get props => [currentPage, lastPage, count, recordPerPage];
}

class CurrentPriceData {
  num currentPrice;
  num prev;
  num changePrice;
  num changeInPercent;
  int status;

  CurrentPriceData(
      {this.currentPrice,
      this.prev,
      this.changePrice,
      this.changeInPercent,
      this.status});

  Map<String, dynamic> toJson() => {
        "currentPrice": currentPrice,
        "prev": prev,
        "changePrice": changePrice,
        "changeInPercent": changeInPercent,
        "status": status,
      };
}
