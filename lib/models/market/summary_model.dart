import 'dart:convert';

import 'package:equatable/equatable.dart';

Summary summaryFromJson(String str) => Summary.fromJson(json.decode(str));

String summaryToJson(Summary data) => json.encode(data.toJson());

class Summary extends Equatable {
  Summary({
    this.meta,
    this.data,
  });

  final Meta meta;
  final Data data;

  factory Summary.fromJson(Map<String, dynamic> json) => Summary(
        meta: Meta.fromJson(json["meta"]),
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": data.toJson(),
      };

  @override
  List<Object> get props => [meta, data];
}

class Data extends Equatable {
  Data({
    this.openingBalance,
    this.withdraw,
    this.deposit,
    this.trxMarket,
    this.balance,
  });

  final double openingBalance;
  final double withdraw;
  final double deposit;
  final double trxMarket;
  final double balance;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        openingBalance: json["opening_balance"] / 1,
        withdraw: json["withdraw"] / 1,
        deposit: json["deposit"] / 1,
        trxMarket: json["trx_market"] / 1,
        balance: json["balance"] / 1,
      );

  Map<String, dynamic> toJson() => {
        "opening_balance": openingBalance,
        "withdraw": withdraw,
        "deposit": deposit,
        "trx_market": trxMarket,
        "balance": balance,
      };

  @override
  List<Object> get props =>
      [openingBalance, withdraw, deposit, trxMarket, balance];
}

class Meta extends Equatable {
  Meta({
    this.message,
  });

  final String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };

  @override
  List<Object> get props => [message];
}
