import 'dart:convert';

import 'package:equatable/equatable.dart';

HistoryMarket historyMarketFromJson(String str) =>
    HistoryMarket.fromJson(json.decode(str));

String historyMarketToJson(HistoryMarket data) => json.encode(data.toJson());

class HistoryMarket extends Equatable {
  HistoryMarket({
    this.meta,
    this.data,
  });

  final Meta meta;
  final List<History> data;

  factory HistoryMarket.fromJson(Map<String, dynamic> json) => HistoryMarket(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? []
            : List<History>.from(json["data"].map((x) => History.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };

  @override
  List<Object> get props => [meta, data];
}

class History extends Equatable {
  History({
    this.id,
    this.emitenUuid,
    this.codeEmiten,
    this.startAmount,
    this.amount,
    this.createdAt,
    this.price,
    this.totalPrice,
    this.status,
    this.traderId,
    this.type,
    this.fee,
  });

  final String id;
  final String emitenUuid;
  final String codeEmiten;
  final int startAmount;
  final int amount;
  final DateTime createdAt;
  final int price;
  final int totalPrice;
  final bool status;
  final int traderId;
  final String type;
  final num fee;

  factory History.fromJson(Map<String, dynamic> json) => History(
        id: json["id"],
        emitenUuid: json["emiten_uuid"],
        codeEmiten: json["code_emiten"],
        startAmount: json["start_amount"],
        amount: json["amount"],
        createdAt: DateTime.parse(json["created_at"]),
        price: json["price"],
        totalPrice: json["total_price"],
        status: json["status"],
        traderId: json["trader_id"],
        type: json["type"],
        fee: json["fee"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "emiten_uuid": emitenUuid,
        "code_emiten": codeEmiten,
        "start_amount": startAmount,
        "amount": amount,
        "created_at": createdAt.toIso8601String(),
        "price": price,
        "status": status,
        "trader_id": traderId,
        "type": type,
        "fee": fee,
      };

  @override
  List<Object> get props => [
        id,
        emitenUuid,
        codeEmiten,
        startAmount,
        amount,
        createdAt,
        price,
        status,
        traderId,
        type
      ];
}

class Meta extends Equatable {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  final int currentPage;
  final int lastPage;
  final int count;
  final int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        count: json["count"],
        recordPerPage: json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "last_page": lastPage,
        "count": count,
        "record_per_page": recordPerPage,
      };

  @override
  List<Object> get props => [currentPage, lastPage, count, recordPerPage];
}
