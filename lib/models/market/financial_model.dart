import 'dart:convert';

Financial financialFromJson(String str) => Financial.fromJson(json.decode(str));

String financialToJson(Financial data) => json.encode(data.toJson());

class Financial {
  Financial({
    this.message,
    this.data,
  });

  String message;
  FinancialData data;

  factory Financial.fromJson(Map<String, dynamic> json) => Financial(
        message: json["message"],
        data: FinancialData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "FinancialData": data.toJson(),
      };
}

class FinancialData {
  FinancialData({
    this.uuid,
    this.id,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.supply,
    this.price,
    this.emitenPrice,
    this.emitenPercentage,
    this.emitenShareAmount,
    this.tradeable,
    this.netIncome,
    this.salesRevenue,
    this.npm,
    this.eps,
    this.dividendYeild,
    this.dps,
    this.per,
    this.proyeksiDividendYeild,
  });

  String uuid;
  int id;
  String companyName;
  String trademark;
  String codeEmiten;
  double supply;
  double price;
  double emitenPrice;
  double emitenPercentage;
  double emitenShareAmount;
  int tradeable;
  double netIncome;
  double salesRevenue;
  double npm;
  double eps;
  double dividendYeild;
  double dps;
  double per;
  dynamic proyeksiDividendYeild;

  factory FinancialData.fromJson(Map<String, dynamic> json) => FinancialData(
        uuid: json["uuid"],
        id: json["id"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
        supply: num.parse(json["supply"].toString()) / 1,
        price: num.parse(json["price"].toString()) / 1,
        emitenPrice: num.parse(json["emiten_price"].toString()) / 1,
        emitenPercentage: num.parse(json["emiten_percentage"].toString()) / 1,
        emitenShareAmount:
            num.parse(json["emiten_share_amount"].toString()) / 1,
        tradeable: json["tradeable"],
        netIncome: num.parse(json["net_income"].toString()) / 1,
        salesRevenue: num.parse(json["sales_revenue"].toString()) / 1,
        npm: num.parse(json["npm"].toString()) / 1,
        eps: num.parse(json["eps"].toString()) / 1,
        dividendYeild: num.parse(json["dividend_yeild"].toString()) / 1,
        dps: num.parse(json["dps"].toString()) / 1,
        per: num.parse(json["per"].toString()) / 1,
        proyeksiDividendYeild: json["proyeksi_dividend_yeild"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "id": id,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "supply": supply,
        "price": price,
        "emiten_price": emitenPrice,
        "emiten_percentage": emitenPercentage,
        "emiten_share_amount": emitenShareAmount,
        "tradeable": tradeable,
        "net_income": netIncome,
        "sales_revenue": salesRevenue,
        "npm": npm,
        "eps": eps,
        "dividend_yeild": dividendYeild,
        "dps": dps,
        "per": per,
        "proyeksi_dividend_yeild": proyeksiDividendYeild,
      };
}
