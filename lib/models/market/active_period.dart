import 'dart:convert';

ActivePeriod activePeriodFromJson(String str) =>
    ActivePeriod.fromJson(json.decode(str));

String activePeriodToJson(ActivePeriod data) => json.encode(data.toJson());

class ActivePeriod {
  ActivePeriod({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Period> data;

  factory ActivePeriod.fromJson(Map<String, dynamic> json) => ActivePeriod(
        meta: Meta.fromJson(json["meta"]),
        data: List<Period>.from(json["data"].map((x) => Period.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Period {
  Period({
    this.id,
    this.marketPeriodId,
    this.day,
    this.isActive,
    this.hour,
  });

  int id;
  int marketPeriodId;
  int day;
  bool isActive;
  List<Hour> hour;

  factory Period.fromJson(Map<String, dynamic> json) => Period(
        id: json["id"],
        marketPeriodId: json["market_period_id"],
        day: json["day"],
        isActive: json["is_active"],
        hour: json["hour"] == null
            ? null
            : List<Hour>.from(json["hour"].map((x) => Hour.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "market_period_id": marketPeriodId,
        "day": day,
        "is_active": isActive,
        "hour": hour == null
            ? null
            : List<dynamic>.from(hour.map((x) => x.toJson())),
      };
}

class Hour {
  Hour({
    this.id,
    this.marketPeriodDayId,
    this.startHour,
    this.endHour,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
  });

  int id;
  int marketPeriodDayId;
  String startHour;
  String endHour;
  DateTime createdAt;
  DateTime updatedAt;
  bool isDeleted;

  factory Hour.fromJson(Map<String, dynamic> json) => Hour(
        id: json["id"],
        marketPeriodDayId: json["market_period_day_id"],
        startHour: json["start_hour"],
        endHour: json["end_hour"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isDeleted: json["is_deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "market_period_day_id": marketPeriodDayId,
        "start_hour": startHour,
        "end_hour": endHour,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "is_deleted": isDeleted,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
