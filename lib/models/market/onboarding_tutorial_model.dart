class OnboardingTutorialModel {
  final String image;
  final String title;
  final String subtitle;
  final String desc;

  OnboardingTutorialModel({this.image, this.title, this.subtitle, this.desc});
}

var tutorialBeli = [
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli1.png",
      title: "Langkah 1",
      subtitle: "Buka Halaman Pembelian",
      desc: "Tap “Beli Saham” di halaman utama pasar sekunder"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli2.png",
      title: "Langkah 2",
      subtitle: "Pilih Saham",
      desc: "Pilih saham yang Anda inginkan"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli3.png",
      title: "Langkah 3",
      subtitle: "Beli Saham",
      desc:
          "Lihat data dan informasi berkaitan dengan perusahaan lalu tap tombol “Beli Saham”"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli4.png",
      title: "Langkah 4",
      subtitle: "Masukan Nilai Pembelian",
      desc:
          "Masukan nominal harga beli dan jumlah lembar saham yang Anda inginkan"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli5.png",
      title: "Langkah 5",
      subtitle: "Cek Saldo Wallet",
      desc:
          "Pastikan saldo di wallet Anda mencukupi untuk melakukan pembelian. Kemudian tap “Beli Saham”"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli6.png",
      title: "Langkah 6",
      subtitle: "Antrian Order",
      desc:
          "Pembelian Anda akan masuk ke antrian order jika pembelian match dengan order jual. Pembelian yang sudah match akan otomatis diproses"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/beli7.png",
      title: "Langkah 7",
      subtitle: "Pembelian Berhasil",
      desc:
          "Selamat, Anda berhasil melakukan pembelian saham ! Cek halaman portofolio untuk melihat saham yang Anda miliki"),
];

var tutorialJual = [
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual1.png",
      title: "Langkah 1",
      subtitle: "Buka Halaman Penjualan",
      desc: "Tap “Jual Saham” di halaman utama pasar sekunder"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual2.png",
      title: "Langkah 2",
      subtitle: "Pilih Saham",
      desc: "Pilih saham di portofolio yang ingin Anda jual. "),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual3.png",
      title: "Langkah 3",
      subtitle: "Jual Saham",
      desc:
          "Lihat data dan informasi berkaitan dengan perusahaan lalu tap tombol “Jual Saham”"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual4.png",
      title: "Langkah 4",
      subtitle: "Masukan Nilai Penjualan",
      desc:
          "Masukan nominal harga jual dan jumlah lembar saham yang ingin Anda jual"),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual5.png",
      title: "Langkah 5",
      subtitle: "Antrian Order",
      desc:
          "Saham yang Anda jual akan masuk ke antrian order. Jika penjualan Anda match dengan order jual maka transaksi akan otomatis diproses."),
  OnboardingTutorialModel(
      image: "assets/image_tutorial_market/jual6.png",
      title: "Langkah 6",
      subtitle: "Penjualan Berhasil",
      desc:
          "Selamat, Anda berhasil melakukan penjualan saham !Cek riwayat transaksi pada halaman transaksi untuk melihat riwayat penjualan Anda"),
];
