// To parse this JSON data, do
//
//     final emitenByUuid = emitenByUuidFromJson(jsonString);

import 'dart:convert';

EmitenByUuid emitenByUuidFromJson(String str) =>
    EmitenByUuid.fromJson(json.decode(str));

String emitenByUuidToJson(EmitenByUuid data) => json.encode(data.toJson());

class EmitenByUuid {
  EmitenByUuid({
    this.meta,
    this.data,
  });

  Meta meta;
  Data data;

  factory EmitenByUuid.fromJson(Map<String, dynamic> json) => EmitenByUuid(
        meta: Meta.fromJson(json["meta"]),
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.emiten,
    this.emitenTeam,
  });

  Emiten emiten;
  List<EmitenTeam> emitenTeam;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        emiten: Emiten.fromJson(json["emiten"]),
        emitenTeam: json["emiten_team"] == null
            ? []
            : List<EmitenTeam>.from(
                json["emiten_team"].map((x) => EmitenTeam.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "emiten": emiten.toJson(),
        "emiten_team": List<dynamic>.from(emitenTeam.map((x) => x.toJson())),
      };
}

class Emiten {
  Emiten({
    this.id,
    this.uuid,
    this.traderId,
    this.companyName,
    this.businessEntity,
    this.trademark,
    this.address,
    this.regencyId,
    this.postalCode,
    this.businessDescription,
    this.businessLifespan,
    this.categoryId,
    this.subCategoryId,
    this.documentBusinessEntity,
    this.employee,
    this.branchCompany,
    this.monthlyTurnover,
    this.annualTurnover,
    this.monthlyProfit,
    this.annualProfit,
    this.monthlyTurnoverPreviousYear,
    this.monthlyProfitPreviousYear,
    this.assetValue,
    this.marketingArea,
    this.marketingPotential,
    this.capitalNeeds,
    this.expansionPlan,
    this.newPotentialMarket,
    this.expectedMonthlyProfit,
    this.experience,
    this.employeeContract,
    this.advantages,
    this.disadvantages,
    this.isVerified,
    this.bankId,
    this.price,
    this.packageId,
    this.supply,
    this.beginPeriod,
    this.endPeriod,
    this.codeEmiten,
    this.status,
    this.pictures,
    this.period,
    this.profit,
    this.latitude,
    this.longitude,
    this.risk,
    this.prospektus,
    this.videoUrl,
    this.updatedBy,
    this.createdBy,
    this.tradeable,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.sold,
    this.minimumInvest,
    this.licenseDocument,
    this.license,
    this.expectedAnnualProfit,
    this.profitMin,
    this.investor,
    this.maxInvestor,
    this.note,
    this.slug,
    this.totalBankDebt,
    this.totalPaidCapital,
    this.bankNameFinancing,
    this.financialRecordingSystem,
    this.bankLoanReputation,
    this.marketPositionForTheProduct,
    this.strategyEmiten,
    this.officeStatus,
    this.levelOfBusinessCompetition,
    this.managerialAbility,
    this.technicalAbility,
    this.tnc,
    this.isPralisting,
    this.email,
    this.telephone,
    this.fax,
    this.youtube,
    this.instagram,
    this.facebook,
    this.twitter,
    this.isKsei,
  });

  int id;
  String uuid;
  int traderId;
  String companyName;
  String businessEntity;
  String trademark;
  String address;
  BankId regencyId;
  Advantages postalCode;
  String businessDescription;
  int businessLifespan;
  int categoryId;
  BankId subCategoryId;
  Advantages documentBusinessEntity;
  int employee;
  String branchCompany;
  AnnualProfit monthlyTurnover;
  AnnualProfit annualTurnover;
  AnnualProfit monthlyProfit;
  AnnualProfit annualProfit;
  AnnualProfit monthlyTurnoverPreviousYear;
  AnnualProfit monthlyProfitPreviousYear;
  int assetValue;
  Advantages marketingArea;
  Advantages marketingPotential;
  int capitalNeeds;
  Advantages expansionPlan;
  Advantages newPotentialMarket;
  AnnualProfit expectedMonthlyProfit;
  Advantages experience;
  Advantages employeeContract;
  Advantages advantages;
  Advantages disadvantages;
  int isVerified;
  BankId bankId;
  int price;
  BankId packageId;
  AnnualProfit supply;
  Advantages beginPeriod;
  Advantages endPeriod;
  String codeEmiten;
  Advantages status;
  String pictures;
  Advantages period;
  AnnualProfit profit;
  double latitude;
  double longitude;
  Advantages risk;
  Advantages prospektus;
  Advantages videoUrl;
  Advantages updatedBy;
  Advantages createdBy;
  bool tradeable;
  bool isActive;
  bool isDeleted;
  DateTime createdAt;
  DateTime updatedAt;
  AnnualProfit sold;
  double minimumInvest;
  Advantages licenseDocument;
  Advantages license;
  int expectedAnnualProfit;
  AnnualProfit profitMin;
  int investor;
  int maxInvestor;
  Advantages note;
  String slug;
  AnnualProfit totalBankDebt;
  AnnualProfit totalPaidCapital;
  Advantages bankNameFinancing;
  Advantages financialRecordingSystem;
  Advantages bankLoanReputation;
  Advantages marketPositionForTheProduct;
  Advantages strategyEmiten;
  Advantages officeStatus;
  Advantages levelOfBusinessCompetition;
  Advantages managerialAbility;
  Advantages technicalAbility;
  int tnc;
  int isPralisting;
  String email;
  int telephone;
  BankId fax;
  Advantages youtube;
  Advantages instagram;
  Advantages facebook;
  Advantages twitter;
  bool isKsei;

  factory Emiten.fromJson(Map<String, dynamic> json) => Emiten(
        id: json["id"],
        uuid: json["uuid"],
        traderId: json["trader_id"],
        companyName: json["company_name"],
        businessEntity: json["business_entity"],
        trademark: json["trademark"],
        address: json["address"],
        regencyId: BankId.fromJson(json["regency_id"]),
        postalCode: Advantages.fromJson(json["postal_code"]),
        businessDescription: json["business_description"],
        businessLifespan: json["business_lifespan"],
        categoryId: json["category_id"],
        subCategoryId: BankId.fromJson(json["sub_category_id"]),
        documentBusinessEntity:
            Advantages.fromJson(json["document_business_entity"]),
        employee: json["employee"],
        branchCompany: json["branch_company"],
        monthlyTurnover: AnnualProfit.fromJson(json["monthly_turnover"]),
        annualTurnover: AnnualProfit.fromJson(json["annual_turnover"]),
        monthlyProfit: AnnualProfit.fromJson(json["monthly_profit"]),
        annualProfit: AnnualProfit.fromJson(json["annual_profit"]),
        monthlyTurnoverPreviousYear:
            AnnualProfit.fromJson(json["monthly_turnover_previous_year"]),
        monthlyProfitPreviousYear:
            AnnualProfit.fromJson(json["monthly_profit_previous_year"]),
        assetValue: json["asset_value"],
        marketingArea: Advantages.fromJson(json["marketing_area"]),
        marketingPotential: Advantages.fromJson(json["marketing_potential"]),
        capitalNeeds: json["capital_needs"],
        expansionPlan: Advantages.fromJson(json["expansion_plan"]),
        newPotentialMarket: Advantages.fromJson(json["new_potential_market"]),
        expectedMonthlyProfit:
            AnnualProfit.fromJson(json["expected_monthly_profit"]),
        experience: Advantages.fromJson(json["experience"]),
        employeeContract: Advantages.fromJson(json["employee_contract"]),
        advantages: Advantages.fromJson(json["advantages"]),
        disadvantages: Advantages.fromJson(json["disadvantages"]),
        isVerified: json["is_verified"],
        bankId: BankId.fromJson(json["bank_id"]),
        price: json["price"],
        packageId: BankId.fromJson(json["package_id"]),
        supply: AnnualProfit.fromJson(json["supply"]),
        beginPeriod: Advantages.fromJson(json["begin_period"]),
        endPeriod: Advantages.fromJson(json["end_period"]),
        codeEmiten: json["code_emiten"],
        status: Advantages.fromJson(json["status"]),
        pictures: json["pictures"],
        period: Advantages.fromJson(json["period"]),
        profit: AnnualProfit.fromJson(json["profit"]),
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
        risk: Advantages.fromJson(json["risk"]),
        prospektus: Advantages.fromJson(json["prospektus"]),
        videoUrl: Advantages.fromJson(json["video_url"]),
        updatedBy: Advantages.fromJson(json["updated_by"]),
        createdBy: Advantages.fromJson(json["created_by"]),
        tradeable: json["tradeable"],
        isActive: json["is_active"],
        isDeleted: json["is_deleted"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        sold: AnnualProfit.fromJson(json["sold"]),
        minimumInvest: json["minimum_invest"] / 1,
        licenseDocument: Advantages.fromJson(json["license_document"]),
        license: Advantages.fromJson(json["license"]),
        expectedAnnualProfit: json["expected_annual_profit"],
        profitMin: AnnualProfit.fromJson(json["profit_min"]),
        investor: json["investor"],
        maxInvestor: json["max_investor"],
        note: Advantages.fromJson(json["note"]),
        slug: json["slug"],
        totalBankDebt: AnnualProfit.fromJson(json["total_bank_debt"]),
        totalPaidCapital: AnnualProfit.fromJson(json["total_paid_capital"]),
        bankNameFinancing: Advantages.fromJson(json["bank_name_financing"]),
        financialRecordingSystem:
            Advantages.fromJson(json["financial_recording_system"]),
        bankLoanReputation: Advantages.fromJson(json["bank_loan_reputation"]),
        marketPositionForTheProduct:
            Advantages.fromJson(json["market_position_for_the_product"]),
        strategyEmiten: Advantages.fromJson(json["strategy_emiten"]),
        officeStatus: Advantages.fromJson(json["office_status"]),
        levelOfBusinessCompetition:
            Advantages.fromJson(json["level_of_business_competition"]),
        managerialAbility: Advantages.fromJson(json["managerial_ability"]),
        technicalAbility: Advantages.fromJson(json["technical_ability"]),
        tnc: json["tnc"],
        isPralisting: json["is_pralisting"],
        email: json["email"],
        telephone: json["telephone"],
        fax: BankId.fromJson(json["fax"]),
        youtube: Advantages.fromJson(json["youtube"]),
        instagram: Advantages.fromJson(json["instagram"]),
        facebook: Advantages.fromJson(json["facebook"]),
        twitter: Advantages.fromJson(json["twitter"]),
        isKsei: json["is_ksei"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "trader_id": traderId,
        "company_name": companyName,
        "business_entity": businessEntity,
        "trademark": trademark,
        "address": address,
        "regency_id": regencyId.toJson(),
        "postal_code": postalCode.toJson(),
        "business_description": businessDescription,
        "business_lifespan": businessLifespan,
        "category_id": categoryId,
        "sub_category_id": subCategoryId.toJson(),
        "document_business_entity": documentBusinessEntity.toJson(),
        "employee": employee,
        "branch_company": branchCompany,
        "monthly_turnover": monthlyTurnover.toJson(),
        "annual_turnover": annualTurnover.toJson(),
        "monthly_profit": monthlyProfit.toJson(),
        "annual_profit": annualProfit.toJson(),
        "monthly_turnover_previous_year": monthlyTurnoverPreviousYear.toJson(),
        "monthly_profit_previous_year": monthlyProfitPreviousYear.toJson(),
        "asset_value": assetValue,
        "marketing_area": marketingArea.toJson(),
        "marketing_potential": marketingPotential.toJson(),
        "capital_needs": capitalNeeds,
        "expansion_plan": expansionPlan.toJson(),
        "new_potential_market": newPotentialMarket.toJson(),
        "expected_monthly_profit": expectedMonthlyProfit.toJson(),
        "experience": experience.toJson(),
        "employee_contract": employeeContract.toJson(),
        "advantages": advantages.toJson(),
        "disadvantages": disadvantages.toJson(),
        "is_verified": isVerified,
        "bank_id": bankId.toJson(),
        "price": price,
        "package_id": packageId.toJson(),
        "supply": supply.toJson(),
        "begin_period": beginPeriod.toJson(),
        "end_period": endPeriod.toJson(),
        "code_emiten": codeEmiten,
        "status": status.toJson(),
        "pictures": pictures,
        "period": period.toJson(),
        "profit": profit.toJson(),
        "latitude": latitude,
        "longitude": longitude,
        "risk": risk.toJson(),
        "prospektus": prospektus.toJson(),
        "video_url": videoUrl.toJson(),
        "updated_by": updatedBy.toJson(),
        "created_by": createdBy.toJson(),
        "tradeable": tradeable,
        "is_active": isActive,
        "is_deleted": isDeleted,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "sold": sold.toJson(),
        "minimum_invest": minimumInvest,
        "license_document": licenseDocument.toJson(),
        "license": license.toJson(),
        "expected_annual_profit": expectedAnnualProfit,
        "profit_min": profitMin.toJson(),
        "investor": investor,
        "max_investor": maxInvestor,
        "note": note.toJson(),
        "slug": slug,
        "total_bank_debt": totalBankDebt.toJson(),
        "total_paid_capital": totalPaidCapital.toJson(),
        "bank_name_financing": bankNameFinancing.toJson(),
        "financial_recording_system": financialRecordingSystem.toJson(),
        "bank_loan_reputation": bankLoanReputation.toJson(),
        "market_position_for_the_product": marketPositionForTheProduct.toJson(),
        "strategy_emiten": strategyEmiten.toJson(),
        "office_status": officeStatus.toJson(),
        "level_of_business_competition": levelOfBusinessCompetition.toJson(),
        "managerial_ability": managerialAbility.toJson(),
        "technical_ability": technicalAbility.toJson(),
        "tnc": tnc,
        "is_pralisting": isPralisting,
        "email": email,
        "telephone": telephone,
        "fax": fax.toJson(),
        "youtube": youtube.toJson(),
        "instagram": instagram.toJson(),
        "facebook": facebook.toJson(),
        "twitter": twitter.toJson(),
        "is_ksei": isKsei,
      };
}

class Advantages {
  Advantages({
    this.string,
    this.valid,
  });

  String string;
  bool valid;

  factory Advantages.fromJson(Map<String, dynamic> json) => Advantages(
        string: json["String"],
        valid: json["Valid"],
      );

  Map<String, dynamic> toJson() => {
        "String": string,
        "Valid": valid,
      };
}

class AnnualProfit {
  AnnualProfit({
    this.float64,
    this.valid,
  });

  double float64;
  bool valid;

  factory AnnualProfit.fromJson(Map<String, dynamic> json) => AnnualProfit(
        float64: json["Float64"].toDouble(),
        valid: json["Valid"],
      );

  Map<String, dynamic> toJson() => {
        "Float64": float64,
        "Valid": valid,
      };
}

class BankId {
  BankId({
    this.int64,
    this.valid,
  });

  int int64;
  bool valid;

  factory BankId.fromJson(Map<String, dynamic> json) => BankId(
        int64: json["Int64"],
        valid: json["Valid"],
      );

  Map<String, dynamic> toJson() => {
        "Int64": int64,
        "Valid": valid,
      };
}

class EmitenTeam {
  EmitenTeam({
    this.id,
    this.position,
    this.name,
    this.displayOrder,
    this.emitenId,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String position;
  String name;
  int displayOrder;
  int emitenId;
  DateTime createdAt;
  DateTime updatedAt;

  factory EmitenTeam.fromJson(Map<String, dynamic> json) => EmitenTeam(
        id: json["id"],
        position: json["position"],
        name: json["name"],
        displayOrder: json["display_order"],
        emitenId: json["emiten_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "position": position,
        "name": name,
        "display_order": displayOrder,
        "emiten_id": emitenId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
