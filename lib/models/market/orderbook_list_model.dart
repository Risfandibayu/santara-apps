import 'dart:convert';

OrderbookListModel orderbookListModelFromJson(String str) =>
    OrderbookListModel.fromJson(json.decode(str));

String orderbookListModelToJson(OrderbookListModel data) =>
    json.encode(data.toJson());

class OrderbookListModel {
  OrderbookListModel({
    this.code,
    this.message,
    this.data,
  });

  int code;
  String message;
  List<Orderbook> data;

  factory OrderbookListModel.fromJson(Map<String, dynamic> json) =>
      OrderbookListModel(
        code: json["code"],
        message: json["message"],
        data: json["data"] == null
            ? []
            : List<Orderbook>.from(
                json["data"].map((x) => Orderbook.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Orderbook {
  Orderbook({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.codeEmiten,
  });

  int id;
  String uuid;
  String companyName;
  String trademark;
  String codeEmiten;

  factory Orderbook.fromJson(Map<String, dynamic> json) => Orderbook(
        id: json["id"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
      };
}
