import 'dart:convert';

import 'package:equatable/equatable.dart';

Balance balanceFromJson(String str) => Balance.fromJson(json.decode(str));

String balanceToJson(Balance data) => json.encode(data.toJson());

class Balance extends Equatable {
  Balance({
    this.code,
    this.message,
    this.data,
  });

  final int code;
  final String message;
  final BalanceData data;

  factory Balance.fromJson(Map<String, dynamic> json) => Balance(
        code: json["code"],
        message: json["message"],
        data: BalanceData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": data.toJson(),
      };

  @override
  List<Object> get props => [code, message, data];
}

class BalanceData extends Equatable {
  BalanceData({
    this.asset,
    this.availableCost,
    this.stockValue,
    this.pendingBuy,
    this.pendingSell,
  });

  final int asset;
  final int availableCost;
  final int stockValue;
  final int pendingBuy;
  final int pendingSell;

  factory BalanceData.fromJson(Map<String, dynamic> json) => BalanceData(
        asset: json["asset"],
        availableCost: json["available_cost"],
        stockValue: json["stock_value"],
        pendingBuy: json["pending_buy"],
        pendingSell: json["pending_sell"],
      );

  Map<String, dynamic> toJson() => {
        "asset": asset,
        "available_cost": availableCost,
        "stock_value": stockValue,
        "pending_buy": pendingBuy,
        "pending_sell": pendingSell,
      };

  @override
  List<Object> get props =>
      [asset, availableCost, stockValue, pendingBuy, pendingSell];
}
