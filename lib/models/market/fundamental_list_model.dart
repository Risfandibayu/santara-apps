import 'dart:convert';

FundamentalList fundamentalListFromJson(String str) =>
    FundamentalList.fromJson(json.decode(str));

String fundamentalListToJson(FundamentalList data) =>
    json.encode(data.toJson());

class FundamentalList {
  FundamentalList({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Fundamental> data;

  factory FundamentalList.fromJson(Map<String, dynamic> json) =>
      FundamentalList(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? null
            : List<Fundamental>.from(
                json["data"].map((x) => Fundamental.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Fundamental {
  Fundamental({
    this.emitenUuid,
    this.companyName,
    this.codeEmiten,
    this.sales,
    this.gps,
    this.cogs,
    this.opm,
    this.npm,
    this.marketCap,
    this.per,
    this.bpvs,
    this.pbv,
    this.roa,
    this.roe,
    this.ev,
    this.der,
    this.year,
    this.price,
    this.period,
  });

  String emitenUuid;
  String companyName;
  String codeEmiten;
  double sales;
  double gps;
  double cogs;
  double opm;
  double npm;
  double marketCap;
  double per;
  double bpvs;
  double pbv;
  double roa;
  double roe;
  double ev;
  double der;
  int year;
  num price;
  int period;

  factory Fundamental.fromJson(Map<String, dynamic> json) => Fundamental(
        emitenUuid: json["emiten_uuid"],
        companyName: json["company_name"],
        codeEmiten: json["code_emiten"],
        sales: json["sales"].toDouble(),
        gps: json["gps"].toDouble(),
        cogs: json["cogs"].toDouble(),
        opm: json["opm"].toDouble(),
        npm: json["npm"].toDouble(),
        marketCap: json["market_cap"].toDouble(),
        per: json["per"].toDouble(),
        bpvs: json["bpvs"].toDouble(),
        pbv: json["pbv"].toDouble(),
        roa: json["roa"] == null ? 0.0 : json["roa"].toDouble(),
        roe: json["roe"] == null ? 0.0 : json["roe"].toDouble(),
        ev: json["ev"].toDouble(),
        der: json["der"].toDouble(),
        year: json["year"],
        price: json["price"],
        period: json["period"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_uuid": emitenUuid,
        "company_name": companyName,
        "code_emiten": codeEmiten,
        "sales": sales,
        "gps": gps,
        "cogs": cogs,
        "opm": opm,
        "npm": npm,
        "market_cap": marketCap,
        "per": per,
        "bpvs": bpvs,
        "pbv": pbv,
        "roa": roa,
        "roe": roe,
        "ev": ev,
        "der": der,
        "year": year,
        "price": price,
        "period": period,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
