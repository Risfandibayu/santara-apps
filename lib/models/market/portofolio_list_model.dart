import 'dart:convert';

PortofolioList portofolioListFromJson(String str) =>
    PortofolioList.fromJson(json.decode(str));

String portofolioListToJson(PortofolioList data) => json.encode(data.toJson());

class PortofolioList {
  PortofolioList({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Portofolio> data;

  factory PortofolioList.fromJson(Map<String, dynamic> json) => PortofolioList(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? []
            : List<Portofolio>.from(
                json["data"].map((x) => Portofolio.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Portofolio {
  Portofolio({
    this.emitenId,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.emitenUuid,
    this.thumbnail,
    this.marketValue,
    this.potGl,
    this.potGlPercent,
    this.avgPrice,
    this.total,
    this.lastPrice,
    this.avgValue,
    this.tradeable,
    this.status,
  });

  int emitenId;
  String companyName;
  String trademark;
  String codeEmiten;
  String emitenUuid;
  String thumbnail;
  double marketValue;
  double potGl;
  double potGlPercent;
  double avgPrice;
  double total;
  double lastPrice;
  double avgValue;
  bool tradeable;
  String status;

  factory Portofolio.fromJson(Map<String, dynamic> json) => Portofolio(
        emitenId: json["emiten_id"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
        emitenUuid: json["emiten_uuid"],
        thumbnail: json["thumbnail"],
        marketValue: json["market_value"] / 1,
        potGl: json["pot_gl"] / 1,
        potGlPercent: json["pot_gl_percent"] / 1,
        avgPrice: json["avg_price"] / 1,
        total: json["total"] / 1,
        lastPrice: json["last_price"] / 1,
        avgValue: json["avg_value"] / 1,
        tradeable: json["tradeable"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_id": emitenId,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "emiten_uuid": emitenUuid,
        "thumbnail": thumbnail,
        "market_value": marketValue,
        "pot_gl": potGl,
        "pot_gl_percent": potGlPercent,
        "avg_price": avgPrice,
        "total": total,
        "last_price": lastPrice,
        "avg_value": avgValue,
        "tradeable": tradeable,
        "status": status,
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  int currentPage;
  int lastPage;
  int count;
  int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        count: json["count"],
        recordPerPage: json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "last_page": lastPage,
        "count": count,
        "record_per_page": recordPerPage,
      };
}
