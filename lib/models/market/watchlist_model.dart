import 'dart:convert';

WatchlistModel watchlistModelFromJson(String str) =>
    WatchlistModel.fromJson(json.decode(str));

String watchlistModelToJson(WatchlistModel data) => json.encode(data.toJson());

class WatchlistModel {
  WatchlistModel({
    this.meta,
    this.data,
  });

  Meta meta;
  List<Watchlist> data;

  factory WatchlistModel.fromJson(Map<String, dynamic> json) => WatchlistModel(
        meta: Meta.fromJson(json["meta"]),
        data: json["data"] == null
            ? []
            : List<Watchlist>.from(
                json["data"].map((x) => Watchlist.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Watchlist {
  Watchlist({
    this.id,
    this.uuid,
    this.emitenId,
    this.userId,
    this.emiten,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
  });

  int id;
  String uuid;
  int emitenId;
  int userId;
  Emiten emiten;
  DateTime createdAt;
  DateTime updatedAt;
  bool isDeleted;

  factory Watchlist.fromJson(Map<String, dynamic> json) => Watchlist(
        id: json["id"],
        uuid: json["uuid"],
        emitenId: json["emiten_id"],
        userId: json["user_id"],
        emiten: Emiten.fromJson(json["emiten"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isDeleted: json["is_deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "emiten_id": emitenId,
        "user_id": userId,
        "emiten": emiten.toJson(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "is_deleted": isDeleted,
      };
}

class Emiten {
  Emiten({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.prevPrice,
    this.currentPrice,
  });

  int id;
  String uuid;
  String companyName;
  String trademark;
  String codeEmiten;
  int prevPrice;
  int currentPrice;

  factory Emiten.fromJson(Map<String, dynamic> json) => Emiten(
        id: json["id"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
        prevPrice: json["prev_price"],
        currentPrice: json["current_price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "prev_price": prevPrice,
        "current_price": currentPrice,
      };
}

class Meta {
  Meta({
    this.currentPage,
    this.lastPage,
    this.count,
    this.recordPerPage,
  });

  int currentPage;
  int lastPage;
  int count;
  int recordPerPage;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        currentPage: json["current_page"],
        lastPage: json["last_page"],
        count: json["count"],
        recordPerPage: json["record_per_page"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "last_page": lastPage,
        "count": count,
        "record_per_page": recordPerPage,
      };
}
