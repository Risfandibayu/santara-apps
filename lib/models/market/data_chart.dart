import 'dart:convert';

DataChart dataChartFromJson(String str) => DataChart.fromJson(json.decode(str));

String dataChartToJson(DataChart data) => json.encode(data.toJson());

class DataChart {
  DataChart({
    this.meta,
    this.data,
  });

  Meta meta;
  List<DataChartItem> data;

  factory DataChart.fromJson(Map<String, dynamic> json) {
    List<DataChartItem> data;
    if (json["data"] == null) {
      data = [
        DataChartItem(
            close: 0,
            high: 0,
            low: 0,
            open: 0,
            volume: 0,
            closedAt: null,
            createdAt: DateTime.now(),
            openedAt: null)
      ];
    } else {
      data = List<DataChartItem>.from(
          json["data"].map((x) => DataChartItem.fromJson(x)));
      data.sort((a, b) => a.createdAt.compareTo(b.createdAt));
    }
    return DataChart(
      meta: Meta.fromJson(json["meta"]),
      data: data,
    );
  }

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataChartItem {
  DataChartItem({
    this.createdAt,
    this.high,
    this.low,
    this.open,
    this.close,
    this.volume,
    this.openedAt,
    this.closedAt,
  });

  DateTime createdAt;
  double high;
  double low;
  double open;
  double close;
  double volume;
  DateTime openedAt;
  DateTime closedAt;

  factory DataChartItem.fromJson(Map<String, dynamic> json) => DataChartItem(
        createdAt: DateTime.parse(json["created_at"]),
        high: json["high"] / 1,
        low: json["low"] / 1,
        open: json["open"] / 1,
        close: json["close"] / 1,
        volume: json["volume"] == null ? 0.0 : json["volume"] / 1,
      );

  Map<String, dynamic> toJson() => {
        "open": open,
        "high": high,
        "low": low,
        "close": close,
        "volumeto": volume,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
