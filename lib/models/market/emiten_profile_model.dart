// To parse this JSON EmitenProfile, do
//
//     final emitenProfileModel = emitenProfileModelFromJson(jsonString);

import 'dart:convert';

EmitenProfileModel emitenProfileModelFromJson(String str) =>
    EmitenProfileModel.fromJson(json.decode(str));

String emitenProfileModelToJson(EmitenProfileModel data) =>
    json.encode(data.toJson());

class EmitenProfileModel {
  EmitenProfileModel({
    this.meta,
    this.data,
  });

  Meta meta;
  EmitenProfile data;

  factory EmitenProfileModel.fromJson(Map<String, dynamic> json) =>
      EmitenProfileModel(
        meta: Meta.fromJson(json["meta"]),
        data: EmitenProfile.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "emitenProfile": data.toJson(),
      };
}

class EmitenProfile {
  EmitenProfile({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.businessDescription,
    this.email,
    this.telephone,
    this.emitenTeam,
    this.address,
    this.fax,
    this.emitenDividen,
    this.price,
    this.supply,
    this.capitalNeeds,
    this.instagram,
    this.facebook,
    this.twitter,
    this.youtube,
    this.isEmitenOwner,
  });

  int id;
  String uuid;
  String companyName;
  String trademark;
  String codeEmiten;
  String businessDescription;
  Email email;
  Fax telephone;
  List<EmitenTeam> emitenTeam;
  String address;
  Fax fax;
  List<EmitenDividen> emitenDividen;
  int price;
  int supply;
  int capitalNeeds;
  Email instagram;
  Email facebook;
  Email twitter;
  Email youtube;
  bool isEmitenOwner;

  factory EmitenProfile.fromJson(Map<String, dynamic> json) => EmitenProfile(
      id: json["id"],
      uuid: json["uuid"],
      companyName: json["company_name"],
      trademark: json["trademark"],
      codeEmiten: json["code_emiten"],
      businessDescription: json["business_description"],
      email: Email.fromJson(json["email"]),
      telephone: Fax.fromJson(json["telephone"]),
      emitenTeam: json["emiten_team"] == null
          ? null
          : List<EmitenTeam>.from(
              json["emiten_team"].map((x) => EmitenTeam.fromJson(x))),
      address: json["address"],
      fax: Fax.fromJson(json["fax"]),
      emitenDividen: json["emiten_dividen"] == null
          ? null
          : List<EmitenDividen>.from(
              json["emiten_dividen"].map((x) => EmitenDividen.fromJson(x))),
      price: json["price"],
      supply: json["supply"],
      capitalNeeds: json["capital_needs"],
      instagram: Email.fromJson(json["instagram"]),
      facebook: Email.fromJson(json["facebook"]),
      twitter: Email.fromJson(json["twitter"]),
      youtube: Email.fromJson(json["youtube"]),
      isEmitenOwner: json["is_emiten_owner"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "business_description": businessDescription,
        "email": email.toJson(),
        "telephone": telephone.toJson(),
        "emiten_team": List<dynamic>.from(emitenTeam.map((x) => x.toJson())),
        "address": address,
        "fax": fax.toJson(),
        "emiten_dividen":
            List<dynamic>.from(emitenDividen.map((x) => x.toJson())),
        "price": price,
        "supply": supply,
        "capital_needs": capitalNeeds,
        "instagram": instagram.toJson(),
        "facebook": facebook.toJson(),
        "twitter": twitter.toJson(),
        "youtube": youtube.toJson(),
        "is_emiten_owner": isEmitenOwner,
      };
}

class Email {
  Email({
    this.string,
    this.valid,
  });

  String string;
  bool valid;

  factory Email.fromJson(Map<String, dynamic> json) => Email(
        string: json["String"],
        valid: json["Valid"],
      );

  Map<String, dynamic> toJson() => {
        "String": string,
        "Valid": valid,
      };
}

class EmitenDividen {
  EmitenDividen({
    this.id,
    this.uuid,
    this.emitenId,
    this.title,
    this.amount,
    this.dividenDate,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
    this.isDeleted,
  });

  int id;
  String uuid;
  int emitenId;
  String title;
  int amount;
  DateTime dividenDate;
  Fax createdBy;
  Fax updatedBy;
  DateTime createdAt;
  DateTime updatedAt;
  bool isDeleted;

  factory EmitenDividen.fromJson(Map<String, dynamic> json) => EmitenDividen(
        id: json["id"],
        uuid: json["uuid"],
        emitenId: json["emiten_id"],
        title: json["title"],
        amount: json["amount"],
        dividenDate: DateTime.parse(json["dividen_date"]),
        createdBy: Fax.fromJson(json["created_by"]),
        updatedBy: Fax.fromJson(json["updated_by"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isDeleted: json["is_deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "emiten_id": emitenId,
        "title": title,
        "amount": amount,
        "dividen_date": dividenDate.toIso8601String(),
        "created_by": createdBy.toJson(),
        "updated_by": updatedBy.toJson(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "is_deleted": isDeleted,
      };
}

class Fax {
  Fax({
    this.int64,
    this.valid,
  });

  int int64;
  bool valid;

  factory Fax.fromJson(Map<String, dynamic> json) => Fax(
        int64: json["Int64"],
        valid: json["Valid"],
      );

  Map<String, dynamic> toJson() => {
        "Int64": int64,
        "Valid": valid,
      };
}

class EmitenTeam {
  EmitenTeam({
    this.id,
    this.position,
    this.name,
    this.displayOrder,
    this.emitenId,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String position;
  String name;
  int displayOrder;
  int emitenId;
  DateTime createdAt;
  DateTime updatedAt;

  factory EmitenTeam.fromJson(Map<String, dynamic> json) => EmitenTeam(
        id: json["id"],
        position: json["position"],
        name: json["name"],
        displayOrder: json["display_order"],
        emitenId: json["emiten_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "position": position,
        "name": name,
        "display_order": displayOrder,
        "emiten_id": emitenId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
