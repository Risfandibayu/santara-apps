// To parse this JSON data, do
//
//     final pendingTransaction = pendingTransactionFromJson(jsonString);

import 'dart:convert';

PendingTransaction pendingTransactionFromJson(String str) =>
    PendingTransaction.fromJson(json.decode(str));

String pendingTransactionToJson(PendingTransaction data) =>
    json.encode(data.toJson());

class PendingTransaction {
  PendingTransaction({
    this.meta,
    this.data,
  });

  Meta meta;
  Transaction data;

  factory PendingTransaction.fromJson(Map<String, dynamic> json) =>
      PendingTransaction(
        meta: Meta.fromJson(json["meta"]),
        data: Transaction.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": data.toJson(),
      };
}

class Transaction {
  Transaction({
    this.buy,
    this.sell,
  });

  int buy;
  int sell;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        buy: json["buy"],
        sell: json["sell"],
      );

  Map<String, dynamic> toJson() => {
        "buy": buy,
        "sell": sell,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
