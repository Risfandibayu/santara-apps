import 'dart:convert';

ProfileBallance profileBallanceFromJson(String str) =>
    ProfileBallance.fromJson(json.decode(str));

String profileBallanceToJson(ProfileBallance data) =>
    json.encode(data.toJson());

class ProfileBallance {
  ProfileBallance({
    this.meta,
    this.data,
  });

  Meta meta;
  Ballance data;

  factory ProfileBallance.fromJson(Map<String, dynamic> json) =>
      ProfileBallance(
        meta: Meta.fromJson(json["meta"]),
        data: Ballance.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "meta": meta.toJson(),
        "data": data.toJson(),
      };
}

class Ballance {
  Ballance(
      {this.totalSaldo,
      this.totalLembarSaham,
      this.totalSaham,
      this.totalAset,
      this.isUnlimitedInvest});

  double totalSaldo;
  double totalLembarSaham;
  double totalSaham;
  double totalAset;
  int isUnlimitedInvest;

  factory Ballance.fromJson(Map<String, dynamic> json) => Ballance(
        totalSaldo: json["total_saldo"] / 1,
        totalLembarSaham: json["total_lembar_saham"] / 1,
        totalSaham: json["total_saham"] / 1,
        totalAset: json["total_aset"] / 1,
        isUnlimitedInvest: json["is_unlimited_invest"],
      );

  Map<String, dynamic> toJson() => {
        "total_saldo": totalSaldo,
        "total_lembar_saham": totalLembarSaham,
        "total_saham": totalSaham,
        "total_aset": totalAset,
        "is_unlimited_invest": isUnlimitedInvest,
      };
}

class Meta {
  Meta({
    this.message,
  });

  String message;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
