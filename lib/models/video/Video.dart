// To parse this JSON data, do
//
//     final videos = videosFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

List<Videos> videosFromJson(String str) =>
    List<Videos>.from(json.decode(str).map((x) => Videos.fromJson(x)));

String videosToJson(List<Videos> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Videos extends Equatable {
  final String uuid;
  final String title;
  final String category;
  final String description;
  final String link;
  final String publishDate;
  final int likes;
  final int dislikes;
  final int views;
  final int isLike;
  final int isDislike;

  Videos({
    this.uuid,
    this.title,
    this.category,
    this.description,
    this.link,
    this.publishDate,
    this.likes,
    this.dislikes,
    this.views,
    this.isLike,
    this.isDislike,
  });

  factory Videos.fromJson(Map<String, dynamic> json) => Videos(
        uuid: json["uuid"],
        title: json["title"],
        category: json["category"],
        description: json["description"],
        link: json["link"],
        publishDate: json["publish_date"],
        likes: json["likes"],
        dislikes: json["dislikes"],
        views: json["views"],
        isLike: json["is_like"],
        isDislike: json["is_dislike"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "title": title,
        "category": category,
        "description": description,
        "link": link,
        "publish_date": publishDate,
        "likes": likes,
        "dislikes": dislikes,
        "views": views,
        "is_like": isLike,
        "is_dislike": isDislike,
      };

  @override
  List<Object> get props => [
        uuid,
        title,
        category,
        description,
        link,
        publishDate,
        likes,
        dislikes,
        views,
        isLike,
        isDislike
      ];
}
