import 'dart:convert';

import 'package:equatable/equatable.dart';

List<VideoCategory> videoCategoryFromJson(String str) =>
    List<VideoCategory>.from(
        json.decode(str).map((x) => VideoCategory.fromJson(x)));

String videoCategoryToJson(List<VideoCategory> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VideoCategory extends Equatable {
  final String uuid;
  final String category;

  VideoCategory({
    this.uuid,
    this.category,
  });

  factory VideoCategory.fromJson(Map<String, dynamic> json) => VideoCategory(
        uuid: json["uuid"],
        category: json["category"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "category": category,
      };

  @override
  List<Object> get props => [uuid, category];
}
