// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<Holdings> welcomeFromJson(String str) => new List<Holdings>.from(json.decode(str).map((x) => Holdings.fromJson(x)));

String welcomeToJson(List<Holdings> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Holdings {
    String companyName;
    String pictures;
    int amount;
    int token;

    Holdings({
        this.companyName,
        this.pictures,
        this.amount,
        this.token,
    });

    factory Holdings.fromJson(Map<String, dynamic> json) => new Holdings(
        companyName: json["company_name"],
        pictures: json['pictures'],
        amount: json["amount"],
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "company_name": companyName,
        "pictures": pictures,
        "amount": amount,
        "token": token,
    };
}
