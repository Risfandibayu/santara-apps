class CommentModel {
  int id;
  String uuid;
  String companyName;
  String trademark;
  int totalComments;
  List<Comments> comments;

  CommentModel(
      {this.id,
      this.uuid,
      this.companyName,
      this.trademark,
      this.totalComments,
      this.comments});

  CommentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    companyName = json['company_name'];
    trademark = json['trademark'];
    totalComments = json['total_comments'];
    if (json['comments'] != null) {
      comments = new List<Comments>();
      json['comments'].forEach((v) {
        comments.add(new Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['company_name'] = this.companyName;
    data['trademark'] = this.trademark;
    data['total_comments'] = this.totalComments;
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Comments {
  String uuid;
  String name;
  String comment;
  String createdAt;
  int isMine;
  String photo;
  List<CommentHistories> commentHistories;

  Comments(
      {this.uuid,
      this.name,
      this.comment,
      this.createdAt,
      this.commentHistories,
      this.isMine,
      this.photo});

  Comments.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    name = json['name'];
    comment = json['comment'];
    isMine = json['is_mine'];
    photo = json['photo'];
    createdAt = json['created_at'];
    if (json['comment_histories'] != null) {
      commentHistories = new List<CommentHistories>();
      json['comment_histories'].forEach((v) {
        commentHistories.add(new CommentHistories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this.uuid;
    data['name'] = this.name;
    data['comment'] = this.comment;
    data['photo'] = this.photo;
    data['is_mine'] = this.isMine;
    data['created_at'] = this.createdAt;
    if (this.commentHistories != null) {
      data['comment_histories'] =
          this.commentHistories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CommentHistories {
  String uuid;
  String name;
  String comment;
  String createdAt;

  int isMine;
  String photo;
  CommentHistories(
      {this.uuid,
      this.name,
      this.comment,
      this.createdAt,
      this.isMine,
      this.photo});

  CommentHistories.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    name = json['name'];
    comment = json['comment'];
    createdAt = json['created_at'];
    isMine = json['is_mine'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this.uuid;
    data['name'] = this.name;
    data['comment'] = this.comment;
    data['created_at'] = this.createdAt;
    data['photo'] = this.photo;
    data['is_mine'] = this.isMine;
    return data;
  }
}
