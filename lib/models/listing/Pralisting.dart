class PralistingModel {
  int status;
  List<Data> data;

  PralistingModel({this.status, this.data});

  PralistingModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String uuid;
  String thumbnail;
  String image;
  String tagName;
  String businessName;
  String companyName;
  int submissions;
  int likes;
  int comments;
  String createdAt;
  String updatedAt;
  int position;
  bool isLiked;
  bool isCommented;
  bool isVoted;
  String description;
  String isDeleted;
  String createdBy;
  String updatedBy;

  Data(
      {this.id,
      this.uuid,
      this.thumbnail,
      this.image,
      this.tagName,
      this.businessName,
      this.companyName,
      this.submissions,
      this.likes,
      this.comments,
      this.createdAt,
      this.updatedAt,
      this.position,
      this.isLiked,
      this.isCommented,
      this.isVoted,
      this.description,
      this.isDeleted,
      this.createdBy,
      this.updatedBy});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    thumbnail = json['thumbnail'];
    image = json['image'];
    tagName = json['tag_name'];
    businessName = json['business_name'];
    companyName = json['company_name'];
    submissions = json['submissions'];
    likes = json['likes'];
    comments = json['comments'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    position = json['position'];
    isLiked = json['is_liked'];
    isCommented = json['is_commented'];
    isVoted = json['is_voted'];
    description = json['description'];
    isDeleted = json['is_deleted'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['thumbnail'] = this.thumbnail;
    data['image'] = this.image;
    data['tag_name'] = this.tagName;
    data['business_name'] = this.businessName;
    data['company_name'] = this.companyName;
    data['submissions'] = this.submissions;
    data['likes'] = this.likes;
    data['comments'] = this.comments;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['position'] = this.position;
    data['is_liked'] = this.isLiked;
    data['is_commented'] = this.isCommented;
    data['description'] = this.description;
    data['is_deleted'] = this.isDeleted;
    data['is_voted'] = this.isVoted;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}
