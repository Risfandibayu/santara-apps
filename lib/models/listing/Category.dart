class Category {
  int id;
  String uuid;
  String category;
  String createdAt;
  String updatedAt;
  int isDeleted;
  String createdBy;
  String updatedBy;

  Category(
      {this.id,
      this.uuid,
      this.category,
      this.createdAt,
      this.updatedAt,
      this.isDeleted,
      this.createdBy,
      this.updatedBy});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 0;
    uuid = json['uuid'] ?? "";
    category = json['category'] ?? "";
    createdAt = json['created_at'] ?? "";
    updatedAt = json['updated_at'] ?? "";
    isDeleted = json['is_deleted'] ?? "";
    createdBy = json['created_by'] ?? "";
    updatedBy = json['updated_by'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['category'] = this.category;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['is_deleted'] = this.isDeleted;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Subcategory {
  int id;
  String uuid;
  int categoryId;
  String subCategory;
  String createdAt;
  String updatedAt;
  int isDeleted;

  Subcategory(
      {this.id,
      this.uuid,
      this.categoryId,
      this.subCategory,
      this.createdAt,
      this.updatedAt,
      this.isDeleted});

  Subcategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    categoryId = json['category_id'];
    subCategory = json['sub_category'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['category_id'] = this.categoryId;
    data['sub_category'] = this.subCategory;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['is_deleted'] = this.isDeleted;
    return data;
  }
}
