import 'package:santaraapp/utils/api.dart';

class PralistingListModel {
  int total;
  int perPage;
  int page;
  int lastPage;
  List<Data> data;

  PralistingListModel(
      {this.total, this.perPage, this.page, this.lastPage, this.data});

  PralistingListModel.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    perPage = json['perPage'];
    page = json['page'];
    lastPage = json['lastPage'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['perPage'] = this.perPage;
    data['page'] = this.page;
    data['lastPage'] = this.lastPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String uuid;
  String companyName;
  String trademark;
  String category;
  List<Pictures> pictures;
  String businessDescription;
  int likes;
  int votes;
  int comments;
  int isLikes;
  int isVote;
  int isComment;

  Data(
      {this.id,
      this.uuid,
      this.companyName,
      this.trademark,
      this.category,
      this.pictures,
      this.businessDescription,
      this.likes,
      this.votes,
      this.comments,
      this.isLikes,
      this.isVote,
      this.isComment});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    companyName = json['company_name'];
    trademark = json['trademark'];
    category = json['category'];
    if (json['pictures'] != null) {
      pictures = new List<Pictures>();
      json['pictures'].forEach((v) {
        pictures.add(new Pictures.fromJson(v));
      });
    }
    businessDescription = json['business_description'];
    likes = json['likes'];
    votes = json['votes'];
    comments = json['comments'];
    isLikes = json['is_likes'];
    isVote = json['is_vote'];
    isComment = json['is_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['company_name'] = this.companyName;
    data['trademark'] = this.trademark;
    data['category'] = this.category;
    if (this.pictures != null) {
      data['pictures'] = this.pictures.map((v) => v.toJson()).toList();
    }
    data['business_description'] = this.businessDescription;
    data['likes'] = this.likes;
    data['votes'] = this.votes;
    data['comments'] = this.comments;
    data['is_likes'] = this.isLikes;
    data['is_vote'] = this.isVote;
    data['is_comment'] = this.isComment;
    return data;
  }
}

class Pictures {
  String picture;

  Pictures({this.picture});

  Pictures.fromJson(Map<String, dynamic> json) {
    picture = json['picture'].contains("http")
        ? json['picture']
        : apiLocalImage + "/uploads/emiten_picture/" + json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['picture'] = this.picture;
    return data;
  }
}
