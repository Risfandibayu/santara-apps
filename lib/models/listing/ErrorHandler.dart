class ErrorHandler {
  final int status; // status error untuk grouping error type
  final String errorMessage; // hasil dari throw catch error
  final String userError; // error message yang ditampilin ke user

  ErrorHandler({this.status, this.errorMessage, this.userError});
}
