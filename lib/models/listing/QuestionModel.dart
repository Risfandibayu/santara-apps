class AnswerModel {
  int answerId;
  int value;
  String answer;
  AnswerModel({this.answerId, this.value, this.answer});
}

class QuestionModel {
  int questionId;
  String question;
  int groupValue;
  List<AnswerModel> answers;
  QuestionModel(
      {this.questionId, this.question, this.groupValue, this.answers});
}
