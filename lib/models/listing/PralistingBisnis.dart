class PralistingBisnis {
  String idBisnis;
  String idKategori;
  String namaBisnis;
  String youtube;
  String thumbnail;
  String namaPerusahaan;
  String diajukan;
  String likes;
  String deskripsi;
  String badanUsaha;
  Null dokumen;
  String lamaUsaha;
  String daerahPemasaran;
  String jumlahKaryawan;
  String jumlahCabang;
  String ifBesarDana;
  String ifRataOmset;
  String ifRataLaba;
  String ifRataOmsetBefore;
  String ifRataLabaBefore;
  String ifTotalHutang;
  String ifNamaBank;
  String ifTotalDisetor;
  String ifNilaiSaham;
  String infPencatatanKeuangan;
  String infReputasiPinjaman;
  String infPosisiPasar;
  String infStrategiKedepan;
  String infStatusLokasi;
  String infTingkatPersaingan;
  String infKemampuanManajerial;
  String infKemampuanTeknis;
  String fb;
  String twitter;
  String ig;
  List<String> alamatLengkap;
  List<String> alamatUsaha;
  String kategori;
  String foto;
  String comments;
  List<ImagesPralisting> images;

  PralistingBisnis(
      {this.idBisnis,
      this.idKategori,
      this.namaBisnis,
      this.youtube,
      this.thumbnail,
      this.namaPerusahaan,
      this.diajukan,
      this.likes,
      this.deskripsi,
      this.badanUsaha,
      this.dokumen,
      this.lamaUsaha,
      this.daerahPemasaran,
      this.jumlahKaryawan,
      this.jumlahCabang,
      this.ifBesarDana,
      this.ifRataOmset,
      this.ifRataLaba,
      this.ifRataOmsetBefore,
      this.ifRataLabaBefore,
      this.ifTotalHutang,
      this.ifNamaBank,
      this.ifTotalDisetor,
      this.ifNilaiSaham,
      this.infPencatatanKeuangan,
      this.infReputasiPinjaman,
      this.infPosisiPasar,
      this.infStrategiKedepan,
      this.infStatusLokasi,
      this.infTingkatPersaingan,
      this.infKemampuanManajerial,
      this.infKemampuanTeknis,
      this.fb,
      this.twitter,
      this.ig,
      this.alamatLengkap,
      this.alamatUsaha,
      this.kategori,
      this.foto,
      this.comments,
      this.images});

  PralistingBisnis.fromJson(Map<String, dynamic> json) {
    idBisnis = json['id_bisnis'];
    idKategori = json['id_kategori'];
    namaBisnis = json['nama_bisnis'];
    youtube = json['youtube'];
    thumbnail = json['thumbnail'];
    namaPerusahaan = json['nama_perusahaan'];
    diajukan = json['diajukan'];
    likes = json['likes'];
    deskripsi = json['deskripsi'];
    badanUsaha = json['badan_usaha'];
    dokumen = json['dokumen'];
    lamaUsaha = json['lama_usaha'];
    daerahPemasaran = json['daerah_pemasaran'];
    jumlahKaryawan = json['jumlah_karyawan'];
    jumlahCabang = json['jumlah_cabang'];
    ifBesarDana = json['if_besar_dana'];
    ifRataOmset = json['if_rata_omset'];
    ifRataLaba = json['if_rata_laba'];
    ifRataOmsetBefore = json['if_rata_omset_before'];
    ifRataLabaBefore = json['if_rata_laba_before'];
    ifTotalHutang = json['if_total_hutang'];
    ifNamaBank = json['if_nama_bank'];
    ifTotalDisetor = json['if_total_disetor'];
    ifNilaiSaham = json['if_nilai_saham'];
    infPencatatanKeuangan = json['inf_pencatatan_keuangan'];
    infReputasiPinjaman = json['inf_reputasi_pinjaman'];
    infPosisiPasar = json['inf_posisi_pasar'];
    infStrategiKedepan = json['inf_strategi_kedepan'];
    infStatusLokasi = json['inf_status_lokasi'];
    infTingkatPersaingan = json['inf_tingkat_persaingan'];
    infKemampuanManajerial = json['inf_kemampuan_manajerial'];
    infKemampuanTeknis = json['inf_kemampuan_teknis'];
    fb = json['fb'];
    twitter = json['twitter'];
    ig = json['ig'];
    alamatLengkap = json['alamat_lengkap'] != null
        ? json['alamat_lengkap'].cast<String>()
        : null;
    alamatUsaha = json['alamat_usaha'] != null
        ? json['alamat_usaha'].cast<String>()
        : null;
    kategori = json['kategori'];
    foto = json['foto'];
    comments = json['comments'].toString();
    if (json['images'] != null) {
      images = new List<ImagesPralisting>();
      json['images'].forEach((v) {
        images.add(new ImagesPralisting.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_bisnis'] = this.idBisnis;
    data['id_kategori'] = this.idKategori;
    data['nama_bisnis'] = this.namaBisnis;
    data['youtube'] = this.youtube;
    data['thumbnail'] = this.thumbnail;
    data['nama_perusahaan'] = this.namaPerusahaan;
    data['diajukan'] = this.diajukan;
    data['likes'] = this.likes;
    data['deskripsi'] = this.deskripsi;
    data['badan_usaha'] = this.badanUsaha;
    data['dokumen'] = this.dokumen;
    data['lama_usaha'] = this.lamaUsaha;
    data['daerah_pemasaran'] = this.daerahPemasaran;
    data['jumlah_karyawan'] = this.jumlahKaryawan;
    data['jumlah_cabang'] = this.jumlahCabang;
    data['if_besar_dana'] = this.ifBesarDana;
    data['if_rata_omset'] = this.ifRataOmset;
    data['if_rata_laba'] = this.ifRataLaba;
    data['if_rata_omset_before'] = this.ifRataOmsetBefore;
    data['if_rata_laba_before'] = this.ifRataLabaBefore;
    data['if_total_hutang'] = this.ifTotalHutang;
    data['if_nama_bank'] = this.ifNamaBank;
    data['if_total_disetor'] = this.ifTotalDisetor;
    data['if_nilai_saham'] = this.ifNilaiSaham;
    data['inf_pencatatan_keuangan'] = this.infPencatatanKeuangan;
    data['inf_reputasi_pinjaman'] = this.infReputasiPinjaman;
    data['inf_posisi_pasar'] = this.infPosisiPasar;
    data['inf_strategi_kedepan'] = this.infStrategiKedepan;
    data['inf_status_lokasi'] = this.infStatusLokasi;
    data['inf_tingkat_persaingan'] = this.infTingkatPersaingan;
    data['inf_kemampuan_manajerial'] = this.infKemampuanManajerial;
    data['inf_kemampuan_teknis'] = this.infKemampuanTeknis;
    data['fb'] = this.fb;
    data['twitter'] = this.twitter;
    data['ig'] = this.ig;
    data['alamat_lengkap'] = this.alamatLengkap;
    data['alamat_usaha'] = this.alamatUsaha;
    data['kategori'] = this.kategori;
    data['foto'] = this.foto;
    data['comments'] = this.comments;
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ImagesPralisting {
  String idImage;
  String idBisnis;
  String image;

  ImagesPralisting({this.idImage, this.idBisnis, this.image});

  ImagesPralisting.fromJson(Map<String, dynamic> json) {
    idImage = json['id_image'];
    idBisnis = json['id_bisnis'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_image'] = this.idImage;
    data['id_bisnis'] = this.idBisnis;
    data['image'] = this.image;
    return data;
  }
}
