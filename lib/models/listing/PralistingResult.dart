// To parse this JSON data, do
//
//     final pralistingResultModel = pralistingResultModelFromJson(jsonString);

import 'dart:convert';

PralistingResultModel pralistingResultModelFromJson(String str) => PralistingResultModel.fromJson(json.decode(str));

String pralistingResultModelToJson(PralistingResultModel data) => json.encode(data.toJson());

class PralistingResultModel {
    int status;
    List<PralistingDataModel> data;

    PralistingResultModel({
        this.status,
        this.data,
    });

    factory PralistingResultModel.fromJson(Map<String, dynamic> json) => PralistingResultModel(
        status: json["status"],
        data: List<PralistingDataModel>.from(json["data"].map((x) => PralistingDataModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class PralistingDataModel {
    int id;
    String uuid;
    String thumbnail;
    String image;
    String tagName;
    String businessName;
    String companyName;
    String submissions;
    String likes;
    String comments;
    String createdAt;
    String updatedAt;
    int position;
    bool isLiked;
    bool isCommented;
    String description;
    String isDeleted;
    String createdBy;
    String updatedBy;

    PralistingDataModel({
        this.id,
        this.uuid,
        this.thumbnail,
        this.image,
        this.tagName,
        this.businessName,
        this.companyName,
        this.submissions,
        this.likes,
        this.comments,
        this.createdAt,
        this.updatedAt,
        this.position,
        this.isLiked,
        this.isCommented,
        this.description,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory PralistingDataModel.fromJson(Map<String, dynamic> json) => PralistingDataModel(
        id: json["id"],
        uuid: json["uuid"],
        thumbnail: json["thumbnail"],
        image: json["image"],
        tagName: json["tag_name"],
        businessName: json["business_name"],
        companyName: json["company_name"],
        submissions: json["submissions"],
        likes: json["likes"],
        comments: json["comments"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        position: json["position"],
        isLiked: json["is_liked"],
        isCommented: json["is_commented"],
        description: json["description"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "thumbnail": thumbnail,
        "image": image,
        "tag_name": tagName,
        "business_name": businessName,
        "company_name": companyName,
        "submissions": submissions,
        "likes": likes,
        "comments": comments,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "position": position,
        "is_liked": isLiked,
        "is_commented": isCommented,
        "description": description,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
