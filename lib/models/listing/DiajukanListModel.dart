class DiajukanListModel {
  int id;
  String uuid;
  String companyName;
  String trademark;
  String category;
  List<Pictures> pictures;
  int isActive;
  int likes;
  int votes;
  int comments;
  String status;
  DateTime createdAt;
  DateTime updatedAt;

  DiajukanListModel({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.category,
    this.pictures,
    this.isActive,
    this.likes,
    this.votes,
    this.comments,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  DiajukanListModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    companyName = json['company_name'];
    trademark = json['trademark'];
    category = json['category'];
    createdAt = DateTime.parse(json["created_at"] ?? DateTime.now().toString());
    updatedAt = DateTime.parse(json["updated_at"] ?? DateTime.now().toString());
    if (json['pictures'] != null) {
      pictures = new List<Pictures>();
      json['pictures'].forEach((v) {
        pictures.add(new Pictures.fromJson(v));
      });
    }
    isActive = json['is_active'];
    likes = json['likes'];
    votes = json['votes'];
    comments = json['comments'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['company_name'] = this.companyName;
    data['trademark'] = this.trademark;
    data['category'] = this.category;
    if (this.pictures != null) {
      data['pictures'] = this.pictures.map((v) => v.toJson()).toList();
    }
    data['is_active'] = this.isActive;
    data['likes'] = this.likes;
    data['votes'] = this.votes;
    data['comments'] = this.comments;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Pictures {
  String picture;

  Pictures({this.picture});

  Pictures.fromJson(Map<String, dynamic> json) {
    picture = json['picture'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['picture'] = this.picture;
    return data;
  }
}
