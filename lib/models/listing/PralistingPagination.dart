// To parse this JSON data, do
//
//     final pralistingPagination = pralistingPaginationFromJson(jsonString);

import 'dart:convert';

PralistingPagination pralistingPaginationFromJson(String str) =>
    PralistingPagination.fromJson(json.decode(str));

String pralistingPaginationToJson(PralistingPagination data) =>
    json.encode(data.toJson());

class PralistingPagination {
  int total;
  int perPage;
  int page;
  int lastPage;
  List<DataPralisting> data;

  PralistingPagination({
    this.total,
    this.perPage,
    this.page,
    this.lastPage,
    this.data,
  });

  factory PralistingPagination.fromJson(Map<String, dynamic> json) =>
      PralistingPagination(
        total: json["total"],
        perPage: json["perPage"],
        page: json["page"],
        lastPage: json["lastPage"],
        data: List<DataPralisting>.from(
            json["data"].map((x) => DataPralisting.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "perPage": perPage,
        "page": page,
        "lastPage": lastPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataPralisting {
  int id;
  String uuid;
  String companyName;
  String trademark;
  String category;
  String pictures;

  String businessDescription;
  int likes;
  int votes;
  int comments;
  int isLikes;
  int isVote;
  int isComment;

  DataPralisting({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.category,
    this.pictures,
    this.businessDescription,
    this.likes,
    this.votes,
    this.comments,
    this.isLikes,
    this.isVote,
    this.isComment,
  });

  factory DataPralisting.fromJson(Map<String, dynamic> json) => DataPralisting(
        id: json["id"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["business_entity"],
        category: json["category"],
        // pictures: List<Picture>.from(
        //     json["pictures"].map((x) => Picture.fromJson(x))),
        pictures: json["pictures"],
        businessDescription: json["business_description"],
        likes: json["likes"],
        votes: json["vote"],
        comments: json["cmt"],
        isLikes: json["is_likes"],
        isVote: json["is_vote"],
        isComment: json["is_comment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        // "pictures": List<dynamic>.from(pictures.map((x) => x.toJson())),
        "pictures": pictures,
        "business_description": businessDescription,
        "likes": likes,
        "votes": votes,
        "comments": comments,
        "is_likes": isLikes,
        "is_vote": isVote,
        "is_comment": isComment,
      };
}

// class Picture {
//   String picture;

//   Picture({
//     this.picture,
//   });

//   factory Picture.fromJson(Map<String, dynamic> json) => Picture(
//         picture: json["picture"],
//       );

//   Map<String, dynamic> toJson() => {
//         "picture": picture,
//       };
// }
