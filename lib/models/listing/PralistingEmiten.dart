class PralistingEmiten {
  String uuid;
  String companyName;
  String trademark;
  int categoryId;
  String category;
  int subCategoryId;
  String address;
  int regencyId;
  String regency;
  String pictures;
  String prospektus;
  String videoUrl;
  String businessDescription;
  String businessEntity;
  int businessLifespan;
  String branchCompany;
  int employee;
  int capitalNeeds;
  int monthlyTurnover;
  int monthlyProfit;
  int monthlyTurnoverPreviousYear;
  int monthlyProfitPreviousYear;
  int totalBankDebt;
  int price;
  String bankNameFinancing;
  int totalPaidCapital;
  String financialRecordingSystem;
  String bankLoanReputation;
  String marketPositionForTheProduct;
  String strategyEmiten;
  String officeStatus;
  String levelOfBusinessCompetition;
  String managerialAbility;
  String technicalAbility;
  int totalInvestmentPlans;
  int investmentPlan;
  int totalLikes;
  int totalVotes;
  int totalComments;
  int isLikes;
  int isVote;
  int isComment;

  PralistingEmiten(
      {this.uuid,
      this.companyName,
      this.trademark,
      this.categoryId,
      this.category,
      this.subCategoryId,
      this.address,
      this.regencyId,
      this.regency,
      this.pictures,
      this.prospektus,
      this.videoUrl,
      this.businessDescription,
      this.businessEntity,
      this.businessLifespan,
      this.branchCompany,
      this.employee,
      this.capitalNeeds,
      this.monthlyTurnover,
      this.monthlyProfit,
      this.monthlyTurnoverPreviousYear,
      this.monthlyProfitPreviousYear,
      this.totalBankDebt,
      this.price,
      this.bankNameFinancing,
      this.totalPaidCapital,
      this.financialRecordingSystem,
      this.bankLoanReputation,
      this.marketPositionForTheProduct,
      this.strategyEmiten,
      this.officeStatus,
      this.levelOfBusinessCompetition,
      this.managerialAbility,
      this.technicalAbility,
      this.totalInvestmentPlans,
      this.investmentPlan,
      this.totalLikes,
      this.totalVotes,
      this.totalComments,
      this.isLikes,
      this.isVote,
      this.isComment});

  PralistingEmiten.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    companyName = json['company_name'];
    trademark = json['trademark'];
    categoryId = json['category_id'];
    category = json['category'];
    subCategoryId = json['sub_category_id'];
    address = json['address'];
    regencyId = json['regency_id'];
    regency = json['regency'];
    pictures = json['pictures'];
    prospektus = json['prospektus'];
    videoUrl = json['video_url'];
    businessDescription = json['business_description'];
    businessEntity = json['business_entity'];
    businessLifespan = json['business_lifespan'];
    branchCompany = json['branch_company'];
    employee = json['employee'];
    capitalNeeds = json['capital_needs'];
    monthlyTurnover = json['monthly_turnover'];
    monthlyProfit = json['monthly_profit'];
    monthlyTurnoverPreviousYear = json['monthly_turnover_previous_year'];
    monthlyProfitPreviousYear = json['monthly_profit_previous_year'];
    totalBankDebt = json['total_bank_debt'];
    price = json['price'];
    bankNameFinancing = json['bank_name_financing'];
    totalPaidCapital = json['total_paid_capital'];
    financialRecordingSystem = json['financial_recording_system'];
    bankLoanReputation = json['bank_loan_reputation'];
    marketPositionForTheProduct = json['market_position_for_the_product'];
    strategyEmiten = json['strategy_emiten'];
    officeStatus = json['office_status'];
    levelOfBusinessCompetition = json['level_of_business_competition'];
    managerialAbility = json['managerial_ability'];
    technicalAbility = json['technical_ability'];
    totalInvestmentPlans = json['total_investment_plans'];
    investmentPlan = json['investment_plan'];
    totalLikes = json['total_likes'];
    totalVotes = json['total_votes'];
    totalComments = json['total_comments'];
    isLikes = json['is_likes'];
    isVote = json['is_vote'];
    isComment = json['is_comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this.uuid;
    data['company_name'] = this.companyName;
    data['trademark'] = this.trademark;
    data['category_id'] = this.categoryId;
    data['category'] = this.category;
    data['sub_category_id'] = this.subCategoryId;
    data['address'] = this.address;
    data['regency_id'] = this.regencyId;
    data['regency'] = this.regency;
    data['pictures'] = this.pictures;
    data['prospektus'] = this.prospektus;
    data['video_url'] = this.videoUrl;
    data['business_description'] = this.businessDescription;
    data['business_entity'] = this.businessEntity;
    data['business_lifespan'] = this.businessLifespan;
    data['branch_company'] = this.branchCompany;
    data['employee'] = this.employee;
    data['capital_needs'] = this.capitalNeeds;
    data['monthly_turnover'] = this.monthlyTurnover;
    data['monthly_profit'] = this.monthlyProfit;
    data['monthly_turnover_previous_year'] = this.monthlyTurnoverPreviousYear;
    data['monthly_profit_previous_year'] = this.monthlyProfitPreviousYear;
    data['total_bank_debt'] = this.totalBankDebt;
    data['price'] = this.price;
    data['bank_name_financing'] = this.bankNameFinancing;
    data['total_paid_capital'] = this.totalPaidCapital;
    data['financial_recording_system'] = this.financialRecordingSystem;
    data['bank_loan_reputation'] = this.bankLoanReputation;
    data['market_position_for_the_product'] = this.marketPositionForTheProduct;
    data['strategy_emiten'] = this.strategyEmiten;
    data['office_status'] = this.officeStatus;
    data['level_of_business_competition'] = this.levelOfBusinessCompetition;
    data['managerial_ability'] = this.managerialAbility;
    data['technical_ability'] = this.technicalAbility;
    data['total_investment_plans'] = this.totalInvestmentPlans;
    data['investment_plan'] = this.investmentPlan;
    data['total_likes'] = this.totalLikes;
    data['total_votes'] = this.totalVotes;
    data['total_comments'] = this.totalComments;
    data['is_likes'] = this.isLikes;
    data['is_vote'] = this.isVote;
    data['is_comment'] = this.isComment;
    return data;
  }
}
