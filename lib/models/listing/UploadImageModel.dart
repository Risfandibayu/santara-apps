import 'package:multi_image_picker/multi_image_picker.dart';

class UploadDataImage {
  String url;
  String filepath;
  Asset asset;
  bool isUploading;
  bool
      uploadToServer; // kondisi jika user edit media, kemudian memilih gambar, kemudian simpan gambar, gambar yang memiliki value upload to server true maka dia yg diupload ke server
  double progress;
  bool isMain;

  UploadDataImage(
      {this.url,
      this.filepath,
      this.asset,
      this.uploadToServer = false, // defaultnya false
      this.isUploading = false,
      this.progress = 0.0,
      this.isMain});
}
