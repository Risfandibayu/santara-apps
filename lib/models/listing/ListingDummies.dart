import 'package:multi_image_picker/multi_image_picker.dart';

class EmitenAssets {
  int id; // optional id
  String filepath; // filepath
  String type; // video / image
  String url; // remote image
  Asset asset; // local image
  bool isMain; // jika gambar adl gambar utama
  EmitenAssets(
      {this.id, this.filepath, this.type, this.url, this.asset, this.isMain});
}

class EmitenDetail {
  int id;
  bool isTopSubmission; // paling banyak diajukan ?
  // Media
  List<EmitenAssets> assets;
  // Lampiran
  EmitenAttachment lampiran;
  // Identitas Calon Penerbit
  String youtube;
  String companyName; // nama perushaan
  String businessName; // nama bisnis / merek dagang
  String category; // kategori jenis usaha
  int subcategory; // subcategory
  String city; // kota lokasi usaha
  // baru kota lokasi usaha = regency
  int regency;
  String companyAddress; // alamat perusahaan
  String businessEntity; // bentuk badan usaha
  int established; // berdiri selama (bulan)
  int employees; // pegawai total
  int branch; // cabang
  String submits; // total diajukan
  String likes; // suka
  String description; // deksiprsi
  List<EmitenSocialMedia> socialMedia; // sosmed
  // informasi finansial
  int fundingReq; // kebutuhan modal
  int turnoverAvg; // rata omset bulan ini
  int profitAvg; // rata laba bulan ini
  int lastYearTurnoverAvg; // rata omset per bulan th sebelumnya
  int lastYearProfitAvg; // rata laba per bulan th sebelumnya
  int debtTotal; // total utang
  String bankName; // nama lembaga
  int paidUpCapital; // total modal disetor
  int valuePerShare; // per lembar saham
  // informasi non finansial
  String pencatatanKeuangan;
  String reputasiPinjaman;
  String posisiPasar;
  String strategiKedepan;
  String statusTempatUsaha;
  String tingkatPersaingan;
  String kemampuanManajerial;
  String kemampuanTeknis;
  EmitenDetail(
      {this.id,
      this.isTopSubmission,
      this.assets,
      this.lampiran,
      this.youtube,
      this.companyName,
      this.businessName,
      this.category,
      this.subcategory,
      this.city,
      this.regency,
      this.companyAddress,
      this.businessEntity,
      this.established,
      this.employees,
      this.branch,
      this.submits,
      this.likes,
      this.description,
      this.socialMedia,
      this.fundingReq,
      this.turnoverAvg,
      this.profitAvg,
      this.lastYearTurnoverAvg,
      this.lastYearProfitAvg,
      this.debtTotal,
      this.bankName,
      this.paidUpCapital,
      this.valuePerShare,
      this.pencatatanKeuangan,
      this.reputasiPinjaman,
      this.posisiPasar,
      this.strategiKedepan,
      this.statusTempatUsaha,
      this.tingkatPersaingan,
      this.kemampuanManajerial,
      this.kemampuanTeknis});
}

class EmitenSocialMedia {
  int id;
  String logo;
  String url;
  EmitenSocialMedia({this.id, this.logo, this.url});
}

class EmitenAttachment {
  int id;
  String fileName;
  String filePath;
  String type; // document / other
  EmitenAttachment({this.id, this.fileName, this.filePath, this.type});
}

class DataDummy {
  static List<EmitenDetail> dummies = [
    EmitenDetail(
      id: 1,
      isTopSubmission: true,
      assets: [
        EmitenAssets(
            id: 1,
            type: "video",
            url: "https://www.youtube.com/watch?v=sX1RPawidZM",
            isMain: false),
        EmitenAssets(
            id: 2,
            type: "image",
            url:
                "https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/94755292_2994660704093526_3549764544287276334_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=106&_nc_ohc=6h5E-qPtz1EAX-1Viff&oh=2fb2764585168a18d5e51edd5c816b4c&oe=5ED2D8E2",
            isMain: false),
        EmitenAssets(
            id: 3,
            type: "image",
            url:
                "https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/e35/95012396_274483413593983_4430306481924627692_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=107&_nc_ohc=hy0szSlVNJQAX97GNqC&oh=8a49da1efe0529fc98ca1595d6106f23&oe=5ED30AF1",
            isMain: false),
        EmitenAssets(
            id: 4,
            type: "image",
            url:
                "https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/e35/94830166_3302388949793370_9174987463305898339_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=106&_nc_ohc=7gvQuu4gh_IAX-BROY1&oh=9e3deaf109d26a81ebcc490ae18d38be&oe=5ED530A8",
            isMain: true),
      ],
      lampiran: EmitenAttachment(
          id: 1,
          fileName:
              "https://file-examples.com/wp-content/uploads/2017/10/file-example_PDF_500_kB.pdf",
          type: "document"),
      companyName: "PT Samudra Limpah Artha",
      businessName: "The Crabbys",
      category: "0",
      subcategory: 0,
      city: "Yogyakarta",
      regency: 0,
      companyAddress:
          "Jl. Sidomukti, Tiyosan, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
      businessEntity: "PT",
      established: 48,
      employees: 150,
      branch: 5,
      submits: "20",
      likes: "80",
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id sapien dictum, ullamcorper tellus sed, laoreet dolor. Pellentesque efficitur pulvinar mi, ac rutrum velit suscipit sed. Cras mollis commodo imperdiet. Duis lobortis magna quis felis semper, id vehicula justo egestas. Aliquam hendrerit congue nibh, et facilisis odio. Aenean maximus felis ligula, in molestie libero posuere at. Nulla placerat vel elit sit amet ornare. Fusce ultricies malesuada luctus. Ut eu libero sit amet est lobortis luctus. Donec dapibus tristique leo vel laoreet",
      socialMedia: [
        EmitenSocialMedia(
            id: 1,
            logo: "assets/icon_socmed/fb.png",
            url: "https://web.facebook.com/The-crabbys-599703893546277/"),
        EmitenSocialMedia(
            id: 2,
            logo: "assets/icon_socmed/ig.png",
            url: "https://www.instagram.com/thecrabbys/?hl=en")
      ],
      fundingReq: 500000000,
      turnoverAvg: 80000000,
      profitAvg: 50000000,
      lastYearTurnoverAvg: 60000000,
      lastYearProfitAvg: 40000000,
      debtTotal: 0,
      bankName: "",
      paidUpCapital: 1000000000,
      valuePerShare: 150000,
      pencatatanKeuangan: "Terkomputerisasi/Software akuntansi",
      reputasiPinjaman: "Tidak memiliki pinjaman",
      posisiPasar: "Pemimpin pasar lokal/nasional",
      strategiKedepan:
          "Punya milestone jangka panjang owner & infrastruktur siap",
      statusTempatUsaha: "Milik sendiri/sewa > 5 Tahun",
      tingkatPersaingan: "Mampu bersaing namun bukan pemimpin pasar",
      kemampuanManajerial: "Berusaha bersaing namun bukan pemimpin pasar",
      kemampuanTeknis: "Owner/Manajemen ahli di bisnis ini",
    )
  ];
}
