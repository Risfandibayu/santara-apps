class FinanceLastReportModel {
  String updatedAt;
  int salesRevenue;
  int netIncome;
  String financeReport;
  String reportStatus;
  String reportDeadline;

  FinanceLastReportModel({
    this.updatedAt,
    this.salesRevenue,
    this.netIncome,
    this.financeReport,
    this.reportStatus,
    this.reportDeadline,
  });

  FinanceLastReportModel.fromJson(Map<String, dynamic> json) {
    updatedAt = json['updated_at'];
    salesRevenue = json['sales_revenue'];
    netIncome = json['net_income'];
    financeReport = json['finance_report'];
    reportStatus = json['report_status'];
    reportDeadline = json['report_deadline'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updated_at'] = this.updatedAt;
    data['sales_revenue'] = this.salesRevenue;
    data['net_income'] = this.netIncome;
    data['finance_report'] = this.financeReport;
    data['report_status'] = this.reportStatus;
    data['report_deadline'] = this.reportDeadline;
    return data;
  }
}
