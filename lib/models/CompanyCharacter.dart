// To parse this JSON data, do
import 'dart:convert';

List<CompanyCharacter> companyCharacterFromJson(String str) => List<CompanyCharacter>.from(json.decode(str).map((x) => CompanyCharacter.fromJson(x)));

String companyCharacterToJson(List<CompanyCharacter> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CompanyCharacter {
    int id;
    String character;
    String code;

    CompanyCharacter({
        this.id,
        this.character,
        this.code,
    });

    factory CompanyCharacter.fromJson(Map<String, dynamic> json) => CompanyCharacter(
        id: json["id"],
        character: json["character"],
        code: json["code"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "type": character,
        "code": code,
    };
}
