// To parse this JSON data, do
//
//     final register = registerFromJson(jsonString);

import 'dart:convert';

Register registerFromJson(String str) => Register.fromJson(json.decode(str));

String registerToJson(Register data) => json.encode(data.toJson());

class Register {
    Token token;
    User user;
    Trader trader;

    Register({
        this.token,
        this.user,
        this.trader,
    });

    factory Register.fromJson(Map<String, dynamic> json) => new Register(
        token: Token.fromJson(json["token"]),
        user: User.fromJson(json["user"]),
        trader: Trader.fromJson(json["trader"]),
    );

    Map<String, dynamic> toJson() => {
        "token": token.toJson(),
        "user": user.toJson(),
        "trader": trader.toJson(),
    };
}

class Token {
    String type;
    String token;
    dynamic refreshToken;

    Token({
        this.type,
        this.token,
        this.refreshToken,
    });

    factory Token.fromJson(Map<String, dynamic> json) => new Token(
        type: json["type"],
        token: json["token"],
        refreshToken: json["refreshToken"],
    );

    Map<String, dynamic> toJson() => {
        "type": type,
        "token": token,
        "refreshToken": refreshToken,
    };
}

class Trader {
    String phone;
    int userId;
    String name;
    String uuid;
    DateTime createdAt;
    DateTime updatedAt;
    int id;

    Trader({
        this.phone,
        this.userId,
        this.name,
        this.uuid,
        this.createdAt,
        this.updatedAt,
        this.id,
    });

    factory Trader.fromJson(Map<String, dynamic> json) => new Trader(
        phone: json["phone"],
        userId: json["user_id"],
        name: json["name"],
        uuid: json["uuid"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "phone": phone,
        "user_id": userId,
        "name": name,
        "uuid": uuid,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "id": id,
    };
}

class User {
    String email;
    int roleId;
    String uuid;
    DateTime createdAt;
    DateTime updatedAt;
    int id;

    User({
        this.email,
        this.roleId,
        this.uuid,
        this.createdAt,
        this.updatedAt,
        this.id,
    });

    factory User.fromJson(Map<String, dynamic> json) => new User(
        email: json["email"],
        roleId: json["role_id"],
        uuid: json["uuid"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "email": email,
        "role_id": roleId,
        "uuid": uuid,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "id": id,
    };
}
