import 'dart:convert';

import 'package:santaraapp/utils/api.dart';

List<Emiten> emitenFromJson(String str) =>
    List<Emiten>.from(json.decode(str).map((x) => Emiten.fromJson(x)));

String emitenToJson(List<Emiten> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

double convertToDouble(dynamic value) {
  try {
    return value.toDouble();
  } catch (e) {
    // print(value);
    // print(e);
    return 0.0;
  }
}

class Emiten {
  String uuid;
  String companyName;
  String trademark;
  String category;
  int price;
  int supply;
  int booked;
  String beginPeriod;
  String endPeriod;
  String codeEmiten;
  String period;
  String address;
  List<Picture> pictures;
  double profit;
  double profitMin;
  int investors;
  int totalInvestor;
  String videoUrl;
  double latitude;
  double longitude;
  String prospektus;
  String createdAt;
  double terjual;
  String name;
  String bio;
  String photo;
  int minimumInvest;
  int startFrom;
  int maxInvestorToggle;

  Emiten(
      {this.uuid,
      this.companyName,
      this.trademark,
      this.category,
      this.price,
      this.supply,
      this.booked,
      this.beginPeriod,
      this.endPeriod,
      this.codeEmiten,
      this.period,
      this.address,
      this.pictures,
      this.profit,
      this.profitMin,
      this.investors,
      this.totalInvestor,
      this.videoUrl,
      this.latitude,
      this.longitude,
      this.prospektus,
      this.createdAt,
      this.terjual,
      this.name,
      this.bio,
      this.photo,
      this.minimumInvest,
      this.startFrom,
      this.maxInvestorToggle});

  factory Emiten.fromJson(Map<String, dynamic> json) => Emiten(
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        category: json["category"],
        price: json["price"],
        booked: json["booked"],
        supply: json["supply"],
        beginPeriod: json["begin_period"],
        endPeriod: json["end_period"],
        codeEmiten: json["code_emiten"],
        period: json["period"],
        address: json["address"],
        pictures: List<Picture>.from(
            json["pictures"].map((x) => Picture.fromJson(x))),
        profit: json["profit"] == null ? 0.0 : convertToDouble(json["profit"]),
        profitMin: json["profit_min"] == null
            ? 0.0
            : convertToDouble(json["profit_min"]),
        investors: json["investors"],
        totalInvestor: json["total_investor"],
        videoUrl: json["video_url"] == null ? null : json["video_url"],
        latitude:
            json["latitude"] == null ? 0.0 : convertToDouble(json["latitude"]),
        longitude: json["longitude"] == null
            ? 0.0
            : convertToDouble(json["longitude"]),
        prospektus: json["prospektus"] == null ? null : json["prospektus"],
        createdAt: json["created_at"],
        terjual:
            json["terjual"] == null ? 0.0 : convertToDouble(json["terjual"]),
        name: json["name"],
        bio: json["bio"] == null ? null : json["bio"],
        photo: json["photo"] == null ? null : json["photo"],
        minimumInvest:
            json["minimum_invest"] == null ? 0 : json["minimum_invest"],
        startFrom: json["start_from"] == null ? 0 : json["start_from"],
        maxInvestorToggle: json["max_investor_toggle"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        "price": price,
        "booked": booked,
        "supply": supply,
        "begin_period": beginPeriod,
        "end_period": endPeriod,
        "code_emiten": codeEmiten,
        "period": period,
        "address": address,
        "pictures": List<dynamic>.from(pictures.map((x) => x.toJson())),
        "profit": profit,
        "profit_min": profitMin == null ? null : profitMin,
        "investors": investors,
        "total_investor": totalInvestor,
        "video_url": videoUrl == null ? null : videoUrl,
        "latitude": latitude,
        "longitude": longitude,
        "prospektus": prospektus == null ? null : prospektus,
        "created_at": createdAt,
        "terjual": terjual,
        "name": name,
        "bio": bio == null ? null : bio,
        "photo": photo == null ? null : photo,
        "minimum_invest": minimumInvest,
        "start_from": startFrom,
        "max_investor_toggle": maxInvestorToggle,
      };
}

class Picture {
  String picture;

  Picture({
    this.picture,
  });

  factory Picture.fromJson(Map<String, dynamic> json) {
    String pic = json["picture"];
    if (!pic.contains('http')) {
      pic = '$urlAsset/token/${json["picture"]}';
    }
    return Picture(
      picture: pic,
    );
  }

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
