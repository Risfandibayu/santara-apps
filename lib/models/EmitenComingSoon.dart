// To parse this JSON data, do
//
//     final emitenComingSoon = emitenComingSoonFromJson(jsonString);

import 'dart:convert';

List<EmitenComingSoon> emitenComingSoonFromJson(String str) =>
    List<EmitenComingSoon>.from(
        json.decode(str).map((x) => EmitenComingSoon.fromJson(x)));

String emitenComingSoonToJson(List<EmitenComingSoon> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmitenComingSoon {
  EmitenComingSoon({
    this.id,
    this.uuid,
    this.companyName,
    this.trademark,
    this.category,
    this.price,
    this.supply,
    this.beginPeriod,
    this.endPeriod,
    this.codeEmiten,
    this.period,
    this.address,
    this.pictures,
    this.profit,
    this.profitMin,
    this.investors,
    this.maxInvestors,
    this.videoUrl,
    this.latitude,
    this.longitude,
    this.prospektus,
    this.minimumInvest,
    this.slug,
    this.createdAt,
    this.maxInvestorToggle,
    this.likes,
    this.vote,
    this.trdlike,
    this.cmt,
  });

  int id;
  String uuid;
  String companyName;
  String trademark;
  String category;
  int price;
  int supply;
  DateTime beginPeriod;
  DateTime endPeriod;
  String codeEmiten;
  String period;
  String address;
  String pictures;
  double profit;
  double profitMin;
  int investors;
  int maxInvestors;
  String videoUrl;
  double latitude;
  double longitude;
  String prospektus;
  int minimumInvest;
  Slug slug;
  DateTime createdAt;
  int maxInvestorToggle;
  int likes;
  int vote;
  String trdlike;
  int cmt;

  factory EmitenComingSoon.fromJson(Map<String, dynamic> json) =>
      EmitenComingSoon(
        id: json["id"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        category: json["category"],
        price: json["price"] == null ? null : json["price"],
        supply: json["supply"] == null ? null : json["supply"],
        beginPeriod: json["begin_period"] == null
            ? null
            : DateTime.parse(json["begin_period"]),
        endPeriod: json["end_period"] == null
            ? null
            : DateTime.parse(json["end_period"]),
        codeEmiten: json["code_emiten"] == null ? null : json["code_emiten"],
        period: json["period"] == null ? null : json["period"],
        address: json["address"],
        pictures: json["pictures"],
        profit: json["profit"] == null ? null : json["profit"].toDouble(),
        profitMin:
            json["profit_min"] == null ? null : json["profit_min"].toDouble(),
        investors: json["investors"],
        maxInvestors: json["max_investors"],
        videoUrl: json["video_url"] == null ? null : json["video_url"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        prospektus: json["prospektus"] == null ? null : json["prospektus"],
        minimumInvest: json["minimum_invest"],
        slug: json["slug"] == null ? null : slugValues.map[json["slug"]],
        createdAt: DateTime.parse(json["created_at"]),
        maxInvestorToggle: json["max_investor_toggle"],
        likes: json["likes"],
        vote: json["vote"],
        trdlike: json["trdlike"] == null ? null : json["trdlike"],
        cmt: json["cmt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        "price": price == null ? null : price,
        "supply": supply == null ? null : supply,
        "begin_period":
            beginPeriod == null ? null : beginPeriod.toIso8601String(),
        "end_period": endPeriod == null ? null : endPeriod.toIso8601String(),
        "code_emiten": codeEmiten == null ? null : codeEmiten,
        "period": period == null ? null : period,
        "address": address,
        "pictures": pictures,
        "profit": profit == null ? null : profit,
        "profit_min": profitMin == null ? null : profitMin,
        "investors": investors,
        "max_investors": maxInvestors,
        "video_url": videoUrl == null ? null : videoUrl,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "prospektus": prospektus == null ? null : prospektus,
        "minimum_invest": minimumInvest,
        "slug": slug == null ? null : slugValues.reverse[slug],
        "created_at": createdAt.toIso8601String(),
        "max_investor_toggle": maxInvestorToggle,
        "likes": likes,
        "vote": vote,
        "trdlike": trdlike == null ? null : trdlike,
        "cmt": cmt,
      };
}

enum Slug { EMPTY, SLUG, WARIST_BUMI_KARYA }

final slugValues = EnumValues({
  "-": Slug.EMPTY,
  "": Slug.SLUG,
  "warist-bumi-karya": Slug.WARIST_BUMI_KARYA
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
