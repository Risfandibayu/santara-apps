import 'dart:convert';

List<Balance> balanceFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Balance>.from(jsonData.map((x) => Balance.fromJson(x)));
}

String balanceToJson(List<Balance> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Balance {
  String code;
  String name;
  double balance;
  double amount;

  Balance({
    this.code,
    this.name,
    this.balance,
    this.amount,
  });

  factory Balance.fromJson(Map<String, dynamic> json) => new Balance(
        code: json["code"],
        name: json["name"],
        balance: json["balance"] == null ? null : json["balance"].toDouble(),
        amount: json["amount"] != null ? json["amount"].toDouble() : 0.0,
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "balance": balance == null ? null : balance,
        "amount": amount,
      };
}
