import 'dart:convert';

class BuyToken {
  BuyToken({
    this.emitenUuid,
    this.amount,
    this.channel,
    this.bank,
    this.fee,
    this.pin,
    this.finger,
  });

  String emitenUuid;
  String amount;
  String channel;
  String bank;
  String fee;
  String pin;
  bool finger;

  factory BuyToken.fromJson(Map<String, dynamic> json) => BuyToken(
        emitenUuid: json["emiten_uuid"],
        amount: json["amount"],
        channel: json["channel"],
        bank: json["bank"],
        fee: json["fee"],
        pin: json["pin"],
        finger: json["finger"],
      );

  Map<String, dynamic> toJson() => {
        "emiten_uuid": emitenUuid,
        "amount": amount,
        "channel": channel,
        "bank": bank,
        "fee": fee,
        "pin": pin,
        "finger": finger,
      };
}
