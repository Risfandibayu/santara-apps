import 'dart:convert';

FeeModel feeModelFromJson(String str) => FeeModel.fromJson(json.decode(str));

String feeModelToJson(FeeModel data) => json.encode(data.toJson());

class FeeModel {
  Withdraw withdraw;
  Deposit deposit;
  Dividend dividend;
  Deposit transaction;

  FeeModel({this.withdraw, this.deposit, this.dividend, this.transaction});

  FeeModel.fromJson(Map<String, dynamic> json) {
    withdraw = json['withdraw'] != null
        ? new Withdraw.fromJson(json['withdraw'])
        : null;
    deposit =
        json['deposit'] != null ? new Deposit.fromJson(json['deposit']) : null;
    dividend = json['dividend'] != null
        ? new Dividend.fromJson(json['dividend'])
        : null;
    transaction = json['transaction'] != null
        ? new Deposit.fromJson(json['transaction'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.withdraw != null) {
      data['withdraw'] = this.withdraw.toJson();
    }
    if (this.deposit != null) {
      data['deposit'] = this.deposit.toJson();
    }
    if (this.dividend != null) {
      data['dividend'] = this.dividend.toJson();
    }
    if (this.transaction != null) {
      data['transaction'] = this.transaction.toJson();
    }
    return data;
  }
}

class Withdraw {
  int dana;
  int bank;

  Withdraw({this.dana, this.bank});

  Withdraw.fromJson(Map<String, dynamic> json) {
    dana = json['dana'];
    bank = json['bank'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dana'] = this.dana;
    data['bank'] = this.bank;
    return data;
  }
}

class Deposit {
  int onepay;

  Deposit({this.onepay});

  Deposit.fromJson(Map<String, dynamic> json) {
    onepay = json['onepay'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['onepay'] = this.onepay;
    return data;
  }
}

class Dividend {
  int dana;
  int bank;

  Dividend({this.dana, this.bank});

  Dividend.fromJson(Map<String, dynamic> json) {
    dana = json['dana'];
    bank = json['bank'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dana'] = this.dana;
    data['bank'] = this.bank;
    return data;
  }
}
