// To parse this JSON data, do
//
//     final tokenHistory = tokenHistoryFromJson(jsonString);

import 'dart:convert';

List<TokenHistory> tokenHistoryFromJson(String str) =>
    new List<TokenHistory>.from(
        json.decode(str).map((x) => TokenHistory.fromJson(x)));

String tokenHistoryToJson(List<TokenHistory> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class TokenHistory {
  int id;
  String uuid;
  int traderId;
  String chanel;
  int amount;
  int emitenId;
  String companyName;
  String codeEmiten;
  String trademark;
  String category;
  int price;
  int fee;
  dynamic verificationPhoto;
  String merchantCode;
  String accountNumber;
  String bank;
  String bankTo;
  String name;
  String desc;
  String danaCheckoutUrl;
  String createdAt;
  String expiredDate;
  List<Picture> picture;
  String lastStatus;

  TokenHistory(
      {this.id,
      this.uuid,
      this.traderId,
      this.chanel,
      this.amount,
      this.emitenId,
      this.companyName,
      this.codeEmiten,
      this.trademark,
      this.category,
      this.price,
      this.fee,
      this.verificationPhoto,
      this.merchantCode,
      this.accountNumber,
      this.bank,
      this.bankTo,
      this.name,
      this.desc,
      this.danaCheckoutUrl,
      this.createdAt,
      this.picture,
      this.lastStatus,
      this.expiredDate});

  factory TokenHistory.fromJson(Map<String, dynamic> json) => new TokenHistory(
        id: json["id"],
        uuid: json["uuid"],
        traderId: json["trader_id"],
        chanel: json["channel"],
        amount: json["amount"],
        emitenId: json["emiten_id"],
        companyName: json["company_name"],
        codeEmiten: json["code"],
        trademark: json["trademark"],
        category: json["category"],
        price: json["price"],
        fee: json["fee"],
        verificationPhoto: json["verification_photo"],
        merchantCode: json["merchant_code"],
        accountNumber: json["account_number"],
        bank: json["bank"],
        bankTo: json["bank_to"],
        name: json["name"],
        desc: json["description"],
        danaCheckoutUrl: json["dana_checkout_url"],
        createdAt: json["created_at"],
        expiredDate: json["expired_date"],
        picture: new List<Picture>.from(
            json["pictures"].map((x) => Picture.fromJson(x))),
        lastStatus: json["last_status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "trader_id": traderId,
        "channel": chanel,
        "amount": amount,
        "emiten_id": emitenId,
        "company_name": companyName,
        "code": codeEmiten,
        "trademark": trademark,
        "category": category,
        "price": price,
        "fee": fee,
        "verification_photo": verificationPhoto,
        "merchant_code": merchantCode,
        "account_number": accountNumber,
        "bank": bank,
        "bank_to": bankTo,
        "name": name,
        "description": desc,
        "dana_checkout_url": danaCheckoutUrl,
        "created_at": createdAt,
        "expired_date": expiredDate,
        "pictures": new List<dynamic>.from(picture.map((x) => x.toJson())),
        "last_status": lastStatus,
      };
}

class Picture {
  String picture;

  Picture({
    this.picture,
  });

  factory Picture.fromJson(Map<String, dynamic> json) {
    String pic = json["picture"];
    if (!pic.contains('http')) {
      pic = 'https://storage.googleapis.com/${json["picture"]}';
    }
    return Picture(
      picture: pic,
    );
  }

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
