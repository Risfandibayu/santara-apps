// To parse this JSON data, do
//
//     final portfolioListModel = portfolioListModelFromJson(jsonString);

import 'dart:convert';

PortfolioListModel portfolioListModelFromJson(String str) =>
    PortfolioListModel.fromJson(json.decode(str));

String portfolioListModelToJson(PortfolioListModel data) =>
    json.encode(data.toJson());

class PortfolioListModel {
  PortfolioListModel({
    this.message,
    this.total,
    this.data,
  });

  String message;
  int total;
  List<SahamData> data;

  factory PortfolioListModel.fromJson(Map<String, dynamic> json) =>
      PortfolioListModel(
        message: json["message"],
        total: json["total"],
        data: List<SahamData>.from(
            json["data"].map((x) => SahamData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "total": total,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class SahamData {
  SahamData({
    this.uuid,
    this.trxDate,
    this.category,
    this.companyName,
    this.trademark,
    this.codeEmiten,
    this.totalSahamLembar,
    this.totalSahamRupiah,
    this.yieldSaham,
    this.netIncome,
    this.npm,
    this.eps,
    this.dividenYield,
    this.proyeksiDividendYeild,
    this.dps,
    this.per,
    this.price,
    this.totalSaham,
    this.performance,
    this.listData,
    this.listNetIncome,
  });

  String uuid;
  DateTime trxDate;
  String category;
  String companyName;
  String trademark;
  String codeEmiten;
  double totalSahamLembar;
  int totalSahamRupiah;
  int yieldSaham;
  double netIncome;
  double npm;
  double eps;
  double dividenYield;
  double proyeksiDividendYeild;
  double dps;
  double per;
  double price;
  double totalSaham;
  double performance;
  List<ListDatum> listData;
  List<ListNetIncome> listNetIncome;

  factory SahamData.fromJson(Map<String, dynamic> json) => SahamData(
        uuid: json["uuid"],
        trxDate: DateTime.parse(json["trx_date"] ?? DateTime.now().toString()),
        category: json["category"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        codeEmiten: json["code_emiten"],
        totalSahamLembar: json["total_saham_lembar"] != null
            ? double.parse(json["total_saham_lembar"].toString())
            : null,
        totalSahamRupiah: json["total_saham_rupiah"],
        yieldSaham: json["yield"],
        netIncome: json["net_income"] != null
            ? double.parse(json["net_income"].toString())
            : null,
        npm: json["npm"] != null ? double.parse(json["npm"]) : null,
        eps: json["eps"] != null ? double.parse(json["eps"]) : null,
        dividenYield: json["dividend_yeild"] != null
            ? double.parse(json["dividend_yeild"].toString())
            : null, // err,
        proyeksiDividendYeild: json["proyeksi_dividend_yeild"] != null
            ? double.parse(json["proyeksi_dividend_yeild"].toString())
            : null, // err,
        dps: json["dps"] != null ? double.parse(json["dps"].toString()) : null,
        per: json["per"] != null ? double.parse(json["per"].toString()) : null,
        price: json["price"] != null
            ? double.parse(json["price"].toString())
            : null,
        totalSaham: json["total_saham"] != null ? json["total_saham"]/1 : null,
        performance: json["performance"] != null
            ? double.parse(json["performance"].toString())
            : null,
        listData: List<ListDatum>.from(
            json["list_data"].map((x) => ListDatum.fromJson(x))),
        listNetIncome: List<ListNetIncome>.from(
            json["list_net_income"].map((x) => ListNetIncome.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "trx_date": trxDate.toIso8601String(),
        "category": category,
        "company_name": companyName,
        "trademark": trademark,
        "code_emiten": codeEmiten,
        "total_saham_lembar": totalSahamLembar,
        "total_saham_rupiah": totalSahamRupiah,
        "yield": yieldSaham,
        "net_income": netIncome,
        "npm": npm,
        "eps": eps,
        "dividend_yeild": dividenYield,
        "proyeksi_dividend_yeild": proyeksiDividendYeild,
        "dps": dps,
        "per": per,
        "price": price,
        "total_saham": totalSaham,
        "performance": performance,
        "list_data": List<dynamic>.from(listData.map((x) => x.toJson())),
        "list_net_income":
            List<dynamic>.from(listNetIncome.map((x) => x.toJson()))
      };
}

class ListNetIncome {
  ListNetIncome({this.month, this.netIncome});

  String month;
  double netIncome;

  factory ListNetIncome.fromJson(Map<String, dynamic> json) => ListNetIncome(
      month: json["month"],
      netIncome: json["net_income"] != null
          ? double.parse(json["net_income"].toString())
          : 0.0);

  Map<String, dynamic> toJson() => {
        "month": month,
        "net_income": netIncome,
      };
}

class ListDatum {
  ListDatum({
    this.desc,
    this.amount,
    this.percentage,
    this.netIncome,
    this.color,
    this.sales,
  });

  String desc;
  int amount;
  double percentage;
  double netIncome;
  String color;
  List<Sale> sales;

  factory ListDatum.fromJson(Map<String, dynamic> json) => ListDatum(
        desc: json["desc"],
        amount: json["amount"],
        percentage:
            json["percentage"] != null ? double.parse(json["percentage"]) : 0.0,
        netIncome: json["net_income"] != null
            ? double.parse(json["net_income"].toString())
            : 0.0,
        color: json["color"],
        sales: List<Sale>.from(json["sales"].map((x) => Sale.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "desc": desc,
        "amount": amount,
        "percentage": percentage,
        "net_income": netIncome,
        "color": color,
        "sales": List<dynamic>.from(sales.map((x) => x.toJson())),
      };
}

class Sale {
  Sale({
    this.month,
    this.year,
    this.value,
  });

  String month;
  int year;
  double value;

  factory Sale.fromJson(Map<String, dynamic> json) => Sale(
        month: json["month"],
        year: json["year"],
        value: double.parse(json["value"] != null ? json["value"] : "0"),
      );

  Map<String, dynamic> toJson() => {
        "month": month,
        "year": year,
        "value": value,
      };
}
