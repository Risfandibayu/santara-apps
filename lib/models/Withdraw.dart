import 'dart:convert';

List<Withdraw> withdrawFromJson(String str) => new List<Withdraw>.from(json.decode(str).map((x) => Withdraw.fromJson(x)));

String withdrawToJson(List<Withdraw> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Withdraw {
    int id;
    String uuid;
    String createdAt;
    String updatedAt;
    int amount;
    int fee;
    String bankTo;
    int isVerified;
    int traderId;
    int verifiedBy;
    dynamic withdrawAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;
    dynamic accountNumber;
    dynamic accountName;

    Withdraw({
        this.id,
        this.uuid,
        this.createdAt,
        this.updatedAt,
        this.amount,
        this.fee,
        this.bankTo,
        this.isVerified,
        this.traderId,
        this.verifiedBy,
        this.withdrawAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
        this.accountNumber,
        this.accountName,
    });

    factory Withdraw.fromJson(Map<String, dynamic> json) => new Withdraw(
        id: json["id"],
        uuid: json["uuid"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        amount: json["amount"],
        fee: json["fee"],
        bankTo: json["bank_to"],
        isVerified: json["is_verified"],
        traderId: json["trader_id"],
        verifiedBy: json["verified_by"] == null ? null : json["verified_by"],
        withdrawAt: json["withdraw_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        accountNumber: json["account_number"],
        accountName: json["account_name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "amount": amount,
        "fee": fee,
        "bank_to": bankTo,
        "is_verified": isVerified,
        "trader_id": traderId,
        "verified_by": verifiedBy == null ? null : verifiedBy,
        "withdraw_at": withdrawAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "account_number": accountNumber,
        "account_name": accountName,
    };
}
