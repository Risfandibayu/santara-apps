import 'dart:convert';

List<HelpTicket> helpTicketFromJson(String str) => new List<HelpTicket>.from(json.decode(str).map((x) => HelpTicket.fromJson(x)));

String helpTicketToJson(List<HelpTicket> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class HelpTicket {
    String uuid;
    int serialTicket;
    String title;
    String status;
    String priority;
    String comment;
    String createdAt;
    String updatedAt;

    HelpTicket({
        this.uuid,
        this.serialTicket,
        this.title,
        this.status,
        this.priority,
        this.comment,
        this.createdAt,
        this.updatedAt,
    });

    factory HelpTicket.fromJson(Map<String, dynamic> json) => new HelpTicket(
        uuid: json["uuid"],
        serialTicket: json["serial_ticket"],
        title: json["title"],
        status: json["status"],
        priority: json["priority"],
        comment: json["comment"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "serial_ticket": serialTicket,
        "title": title,
        "status": status,
        "priority": priority,
        "comment": comment,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
