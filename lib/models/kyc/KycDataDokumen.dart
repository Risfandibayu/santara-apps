import 'dart:io';

class KycDataDokumen {
  bool isSubmited;
  bool isEdited;
  File avatar;
  String name;
  String bop;
  String dateBorn;
  String monthBorn;
  String yearBorn;
  String gender;
  String education;
  String nationality;
  String email;
  String altEmail;
  String phone;
  String altPhone;
  String countryId;
  String nik;
  String idCardType;
  String dayRegId;
  String monthRegId;
  String yearRegId;
  String expiredId;
  String dayExpId;
  String monthExpId;
  String yearExpId;
  String passport;
  String dayExpPassport;
  String monthExpPassport;
  String yearExpPassport;
  File fotoKtp;
  File fotoSelfie;

  KycDataDokumen({
    this.isSubmited,
    this.isEdited,
    this.avatar,
    this.name,
    this.bop,
    this.dateBorn,
    this.monthBorn,
    this.yearBorn,
    this.gender,
    this.education,
    this.nationality,
    this.email,
    this.altEmail,
    this.phone,
    this.altPhone,
    this.countryId,
    this.nik,
    this.idCardType,
    this.dayRegId,
    this.monthRegId,
    this.yearRegId,
    this.expiredId,
    this.dayExpId,
    this.monthExpId,
    this.yearExpId,
    this.passport,
    this.dayExpPassport,
    this.monthExpPassport,
    this.yearExpPassport,
    this.fotoKtp,
    this.fotoSelfie,
  });
}

class KycDataDokumenHelper {
  static KycDataDokumen dokumen = KycDataDokumen(
    isSubmited: null,
    isEdited: null,
    avatar: null,
    name: null,
    bop: null,
    dateBorn: null,
    monthBorn: null,
    yearBorn: null,
    gender: null,
    education: null,
    nationality: null,
    email: null,
    altEmail: null,
    phone: null,
    altPhone: null,
    nik: null,
    dayRegId: null,
    monthRegId: null,
    yearRegId: null,
    expiredId: null,
    dayExpId: null,
    monthExpId: null,
    yearExpId: null,
    passport: null,
    dayExpPassport: null,
    monthExpPassport: null,
    yearExpPassport: null,
    fotoKtp: null,
    fotoSelfie: null,
  );
}


