class AlamatModel {
  int id;
  int countryId;
  String countryName;
  int provinceId;
  String provinceName;
  int regencyId;
  String regencyName;
  String idcardAddress;
  String idcardPostalCode;
  int addressSameWithIdcard;
  int countryDomicile;
  String countryDomicileName;
  String province;
  String regency;
  String address;
  String postalCode;

  AlamatModel(
      {this.id,
      this.countryId,
      this.countryName,
      this.provinceId,
      this.provinceName,
      this.regencyId,
      this.regencyName,
      this.idcardAddress,
      this.idcardPostalCode,
      this.addressSameWithIdcard,
      this.countryDomicile,
      this.countryDomicileName,
      this.province,
      this.regency,
      this.address,
      this.postalCode});

  AlamatModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    countryId = json['country_id'];
    countryName = json['country_name'];
    provinceId = json['province_id'];
    provinceName = json['province_name'];
    regencyId = json['regency_id'];
    regencyName = json['regency_name'];
    idcardAddress = json['idcard_address'];
    idcardPostalCode = json['idcard_postal_code'];
    addressSameWithIdcard = json['address_same_with_idcard'];
    countryDomicile = json['country_domicile'];
    countryDomicileName = json['country_domicile_name'];
    province = json['province'];
    regency = json['regency'];
    address = json['address'];
    postalCode = json['postal_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['country_id'] = this.countryId;
    data['country_name'] = this.countryName;
    data['province_id'] = this.provinceId;
    data['province_name'] = this.provinceName;
    data['regency_id'] = this.regencyId;
    data['regency_name'] = this.regencyName;
    data['idcard_address'] = this.idcardAddress;
    data['idcard_postal_code'] = this.idcardPostalCode;
    data['address_same_with_idcard'] = this.addressSameWithIdcard;
    data['country_domicile'] = this.countryDomicile;
    data['country_domicile_name'] = this.countryDomicileName;
    data['province'] = this.province;
    data['regency'] = this.regency;
    data['address'] = this.address;
    data['postal_code'] = this.postalCode;
    return data;
  }
}
