class InformasiPajakModel {
  int id;
  String income;
  String taxAccountCode;
  String npwp;
  String dateRegistrationOfNpwp;
  String reasonToJoin;
  String securitiesAccount;
  String securitiesAccountDateRegistration;
  bool haveSecuritiesAccount;
  String sidNumber;

  InformasiPajakModel({
    this.id,
    this.income,
    this.taxAccountCode,
    this.npwp,
    this.dateRegistrationOfNpwp,
    this.reasonToJoin,
    this.securitiesAccount,
    this.securitiesAccountDateRegistration,
    this.haveSecuritiesAccount,
    this.sidNumber,
  });

  InformasiPajakModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    income = json['income'];
    taxAccountCode = json['tax_account_code'];
    npwp = json['npwp'];
    dateRegistrationOfNpwp = json['date_registration_of_npwp'];
    reasonToJoin = json['reason_to_join'];
    securitiesAccount = json['securities_account'];
    securitiesAccountDateRegistration =
        json['securities_account_date_registration'];
    haveSecuritiesAccount = json['have_securities_account'];
    sidNumber = json['sid_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['income'] = this.income;
    data['tax_account_code'] = this.taxAccountCode;
    data['npwp'] = this.npwp;
    data['date_registration_of_npwp'] = this.dateRegistrationOfNpwp;
    data['reason_to_join'] = this.reasonToJoin;
    data['securities_account'] = this.securitiesAccount;
    data['securities_account_date_registration'] =
        this.securitiesAccountDateRegistration;
    data['have_securities_account'] = this.haveSecuritiesAccount;
    data['sid_number'] = this.sidNumber;
    return data;
  }
}
