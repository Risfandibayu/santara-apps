class BiodataKeluargaModel {
  int id;
  String maritalStatus;
  String spouseName;
  String motherMaidenName;
  String heir;
  String heirRelation;
  String heirPhone;

  BiodataKeluargaModel(
      {this.id,
      this.maritalStatus,
      this.spouseName,
      this.motherMaidenName,
      this.heir,
      this.heirRelation,
      this.heirPhone});

  BiodataKeluargaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    maritalStatus = json['marital_status'];
    spouseName = json['spouse_name'];
    motherMaidenName = json['mother_maiden_name'];
    heir = json['heir'];
    heirRelation = json['heir_relation'];
    heirPhone = json['heir_phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['marital_status'] = this.maritalStatus;
    data['spouse_name'] = this.spouseName;
    data['mother_maiden_name'] = this.motherMaidenName;
    data['heir'] = this.heir;
    data['heir_relation'] = this.heirRelation;
    data['heir_phone'] = this.heirPhone;
    return data;
  }
}
