class BiodataPribadiModel {
  int id;
  String photo;
  String name;
  String birthPlace;
  String birthPlaceName;
  String birthDate;
  String gender;
  int educationId;
  String education;
  int countryId;
  String countryName;
  String email;
  String anotherEmail;
  String phone;
  String altPhone;
  String idcardNumber;
  String regisDateIdcard;
  String typeIdcard;
  String expiredDateIdcard;
  String passportNumber;
  String expiredDatePassport;
  String idcardPhoto;
  String verificationPhoto;
  String photoIdcard;
  String photoVerification;

  BiodataPribadiModel(
      {this.id,
      this.photo,
      this.name,
      this.birthPlace,
      this.birthPlaceName,
      this.birthDate,
      this.gender,
      this.educationId,
      this.education,
      this.countryId,
      this.countryName,
      this.email,
      this.anotherEmail,
      this.phone,
      this.altPhone,
      this.idcardNumber,
      this.regisDateIdcard,
      this.typeIdcard,
      this.expiredDateIdcard,
      this.passportNumber,
      this.expiredDatePassport,
      this.idcardPhoto,
      this.verificationPhoto,
      this.photoIdcard,
      this.photoVerification});

  BiodataPribadiModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    name = json['name'];
    birthPlace = json['birth_place'];
    birthPlaceName = json['birth_place_name'];
    birthDate = json['birth_date'];
    gender = json['gender'];
    educationId = json['education_id'];
    education = json['education'];
    countryId = json['country_id'];
    countryName = json['country_name'];
    email = json['email'];
    anotherEmail = json['another_email'];
    phone = json['phone'];
    altPhone = json['alt_phone'];
    idcardNumber = json['idcard_number'];
    regisDateIdcard = json['regis_date_idcard'];
    typeIdcard = json['type_idcard'];
    expiredDateIdcard = json['expired_date_idcard'];
    passportNumber = json['passport_number'];
    expiredDatePassport = json['expired_date_passport'];
    idcardPhoto = json['idcard_photo'];
    verificationPhoto = json['verification_photo'];
    photoIdcard = json['photo_idcard'];
    photoVerification = json['photo_verification'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['name'] = this.name;
    data['birth_place'] = this.birthPlace;
    data['birth_date'] = this.birthDate;
    data['gender'] = this.gender;
    data['education_id'] = this.educationId;
    data['education'] = this.education;
    data['country_id'] = this.countryId;
    data['country_name'] = this.countryName;
    data['email'] = this.email;
    data['another_email'] = this.anotherEmail;
    data['phone'] = this.phone;
    data['alt_phone'] = this.altPhone;
    data['idcard_number'] = this.idcardNumber;
    data['regis_date_idcard'] = this.regisDateIdcard;
    data['type_idcard'] = this.typeIdcard;
    data['expired_date_idcard'] = this.expiredDateIdcard;
    data['passport_number'] = this.passportNumber;
    data['expired_date_passport'] = this.expiredDatePassport;
    data['idcard_photo'] = this.idcardPhoto;
    data['verification_photo'] = this.verificationPhoto;
    data['photo_idcard'] = this.photoIdcard;
    data['photo_verification'] = this.photoVerification;
    return data;
  }
}
