class PekerjaanModel {
  int id;
  String name;
  String descriptionJob;
  String totalAssetsOfInvestors;
  String sourceOfInvestorFunds;
  String reasonToJoin;

  PekerjaanModel({
    this.id,
    this.name,
    this.descriptionJob,
    this.totalAssetsOfInvestors,
    this.sourceOfInvestorFunds,
    this.reasonToJoin,
  });

  PekerjaanModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    descriptionJob = json['description_job'];
    totalAssetsOfInvestors = json['total_assets_of_investors'];
    sourceOfInvestorFunds = json['source_of_investor_funds'];
    reasonToJoin = json['reason_to_join'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description_job'] = this.descriptionJob;
    data['total_assets_of_investors'] = this.totalAssetsOfInvestors;
    data['source_of_investor_funds'] = this.sourceOfInvestorFunds;
    data['reason_to_join'] = this.reasonToJoin;
    return data;
  }
}
