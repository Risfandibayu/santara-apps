class BankModel {
  int id;
  String accountName1;
  String bank1;
  String accountNumber1;
  String accountName2;
  String bank2;
  String accountNumber2;
  String accountName3;
  String bank3;
  String accountNumber3;

  BankModel(
      {this.id,
      this.accountName1,
      this.bank1,
      this.accountNumber1,
      this.accountName2,
      this.bank2,
      this.accountNumber2,
      this.accountName3,
      this.bank3,
      this.accountNumber3});

  BankModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    accountName1 = json['account_name1'];
    bank1 = json['bank1'];
    accountNumber1 = json['account_number1'];
    accountName2 = json['account_name2'];
    bank2 = json['bank2'];
    accountNumber2 = json['account_number2'];
    accountName3 = json['account_name3'];
    bank3 = json['bank3'];
    accountNumber3 = json['account_number3'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['account_name1'] = this.accountName1;
    data['bank1'] = this.bank1;
    data['account_number1'] = this.accountNumber1;
    data['account_name2'] = this.accountName2;
    data['bank2'] = this.bank2;
    data['account_number2'] = this.accountNumber2;
    data['account_name3'] = this.accountName3;
    data['bank3'] = this.bank3;
    data['account_number3'] = this.accountNumber3;
    return data;
  }
}
