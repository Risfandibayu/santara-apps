import 'dart:io';

class DokPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  File dokAktaPendirianUsaha;
  File dokAktaPerubahanTerakhir;
  File dokSkKemenhumkam;
  File dokNpwp;
  File dokSiup;
  File ktpDireksi;

  DokPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.dokAktaPendirianUsaha,
    this.dokAktaPerubahanTerakhir,
    this.dokSkKemenhumkam,
    this.dokNpwp,
    this.dokSiup,
    this.ktpDireksi,
  });
}

class DokPerusahaanHelper {
  static DokPerusahaanData data = DokPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    dokAktaPendirianUsaha: null,
    dokAktaPerubahanTerakhir: null,
    dokSkKemenhumkam: null,
    dokNpwp: null,
    dokSiup: null,
    ktpDireksi: null,
  );
  static DokPerusahaanData dokumen = data;
}
