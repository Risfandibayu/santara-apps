import 'dart:io';

class BiodataPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  File companyPhoto;
  String companyJenis;
  String companyName;
  String companyType;
  String companyCharacter;
  String companySyariah;
  String companyCountryDomicile;
  String companyEstablishmentPlace;
  String companyDateEstablishment;
  String companyAnotherEmail;
  String companyDescription;

  BiodataPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.companyPhoto,
    this.companyName,
    this.companyJenis,
    this.companyType,
    this.companyCharacter,
    this.companySyariah,
    this.companyCountryDomicile,
    this.companyEstablishmentPlace,
    this.companyDateEstablishment,
    this.companyAnotherEmail,
    this.companyDescription,
  });
}

class BiodataPerusahaanHelper {
  static BiodataPerusahaanData data = BiodataPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    companyPhoto: null,
    companyName: null,
    companyJenis: null,
    companyType: null,
    companyCharacter: null,
    companySyariah: null,
    companyCountryDomicile: null,
    companyEstablishmentPlace: null,
    companyDateEstablishment: null,
    companyAnotherEmail: null,
    companyDescription: null,
  );
  static BiodataPerusahaanData dokumen = data;
}
