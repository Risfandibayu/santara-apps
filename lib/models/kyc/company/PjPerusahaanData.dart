class PjPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  String nama1;
  String jabatan1;
  String noKtp1;
  String npwp1;
  String noPassport1;
  DateTime dateExpKtp1;
  DateTime dateExpPassport1;
  String dayKtp1;
  String monthKtp1;
  String yearKtp1;
  String dayPassport1;
  String monthPassport1;
  String yearPassport1;
  String nama2;
  String jabatan2;
  String noKtp2;
  String npwp2;
  String noPassport2;
  DateTime dateExpKtp2;
  DateTime dateExpPassport2;
  String dayKtp2;
  String monthKtp2;
  String yearKtp2;
  String dayPassport2;
  String monthPassport2;
  String yearPassport2;
  String nama3;
  String jabatan3;
  String noKtp3;
  String npwp3;
  String noPassport3;
  DateTime dateExpKtp3;
  DateTime dateExpPassport3;
  String dayKtp3;
  String monthKtp3;
  String yearKtp3;
  String dayPassport3;
  String monthPassport3;
  String yearPassport3;
  String nama4;
  String jabatan4;
  String noKtp4;
  String npwp4;
  String noPassport4;
  DateTime dateExpKtp4;
  DateTime dateExpPassport4;
  String dayKtp4;
  String monthKtp4;
  String yearKtp4;
  String dayPassport4;
  String monthPassport4;
  String yearPassport4;

  PjPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.nama1,
    this.jabatan1,
    this.noKtp1,
    this.npwp1,
    this.noPassport1,
    this.dayKtp1,
    this.monthKtp1,
    this.yearKtp1,
    this.dayPassport1,
    this.monthPassport1,
    this.yearPassport1,
    this.nama2,
    this.jabatan2,
    this.noKtp2,
    this.npwp2,
    this.noPassport2,
    this.dayKtp2,
    this.monthKtp2,
    this.yearKtp2,
    this.dayPassport2,
    this.monthPassport2,
    this.yearPassport2,
    this.nama3,
    this.jabatan3,
    this.noKtp3,
    this.npwp3,
    this.noPassport3,
    this.dayKtp3,
    this.monthKtp3,
    this.yearKtp3,
    this.dayPassport3,
    this.monthPassport3,
    this.yearPassport3,
    this.nama4,
    this.jabatan4,
    this.noKtp4,
    this.npwp4,
    this.noPassport4,
    this.dayKtp4,
    this.monthKtp4,
    this.yearKtp4,
    this.dayPassport4,
    this.monthPassport4,
    this.yearPassport4,
    this.dateExpKtp1,
    this.dateExpPassport1,
    this.dateExpKtp2,
    this.dateExpPassport2,
    this.dateExpKtp3,
    this.dateExpPassport3,
    this.dateExpKtp4,
    this.dateExpPassport4,
  });
}

class PjPerusahaanHelper {
  static PjPerusahaanData data = PjPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    nama1: null,
    jabatan1: null,
    noKtp1: null,
    npwp1: null,
    noPassport1: null,
    dayKtp1: null,
    monthKtp1: null,
    yearKtp1: null,
    dayPassport1: null,
    monthPassport1: null,
    yearPassport1: null,
    nama2: null,
    jabatan2: null,
    noKtp2: null,
    npwp2: null,
    noPassport2: null,
    dayKtp2: null,
    monthKtp2: null,
    yearKtp2: null,
    dayPassport2: null,
    monthPassport2: null,
    yearPassport2: null,
    nama3: null,
    jabatan3: null,
    noKtp3: null,
    npwp3: null,
    noPassport3: null,
    dayKtp3: null,
    monthKtp3: null,
    yearKtp3: null,
    dayPassport3: null,
    monthPassport3: null,
    yearPassport3: null,
    nama4: null,
    jabatan4: null,
    noKtp4: null,
    npwp4: null,
    noPassport4: null,
    dayKtp4: null,
    monthKtp4: null,
    yearKtp4: null,
    dayPassport4: null,
    monthPassport4: null,
    yearPassport4: null,
    dateExpKtp1: null,
    dateExpPassport1: null,
    dateExpKtp2: null,
    dateExpPassport2: null,
    dateExpKtp3: null,
    dateExpPassport3: null,
    dateExpKtp4: null,
    dateExpPassport4: null,
  );
  static PjPerusahaanData dokumen = data;
}
