class BankPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  int bank1;
  String name1;
  String account1;
  String currency1;
  int bank2;
  String name2;
  String account2;
  String currency2;
  int bank3;
  String name3;
  String account3;
  String currency3;

  BankPerusahaanData(
      {this.isSubmitted,
      this.isEdited,
      this.bank1,
      this.bank2,
      this.bank3,
      this.name1,
      this.name2,
      this.name3,
      this.account1,
      this.account2,
      this.account3,
      this.currency1,
      this.currency2,
      this.currency3});
}

class BankPerusahaanHelper {
  static BankPerusahaanData data = BankPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    bank1: null,
    bank2: null,
    bank3: null,
    name1: null,
    name2: null,
    name3: null,
    account1: null,
    account2: null,
    account3: null,
    currency1: null,
    currency2: null,
    currency3: null,
  );
  static BankPerusahaanData dokumen = data;
}
