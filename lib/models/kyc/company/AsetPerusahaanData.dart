class AsetPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  String fundsSource;
  String fundsSourceText;
  String lastYearAsset;
  String twoYearAsset;
  String threeYearAsset;

  AsetPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.fundsSource,
    this.fundsSourceText,
    this.lastYearAsset,
    this.twoYearAsset,
    this.threeYearAsset,
  });
}

class AsetPerusahaanHelper {
  static AsetPerusahaanData data = AsetPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    fundsSource: null,
    fundsSourceText: null,
    lastYearAsset: null,
    twoYearAsset: null,
    threeYearAsset: null,
  );
  static AsetPerusahaanData dokumen = data;
}
