class AlamatPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  String country;
  String province;
  String city;
  String address;
  String postalCode;
  String phone;
  String fax;

  AlamatPerusahaanData(
      {this.isSubmitted,
      this.isEdited,
      this.country,
      this.province,
      this.city,
      this.address,
      this.postalCode,
      this.phone,
      this.fax});
}

class AlamatPerusahaanHelper {
  static AlamatPerusahaanData data = AlamatPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    country: null,
    province: null,
    city: null,
    address: null,
    postalCode: null,
    phone: null,
    fax: null,
  );
  static AlamatPerusahaanData dokumen = data;
}
