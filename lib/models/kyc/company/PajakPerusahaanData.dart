class PajakPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  String taxIdCorp;
  String lkpbuCode;
  String npwp;
  String npwpRegDate;
  String bussinessRegCert;
  String articleOfAssc;
  String investorCompanyBICode;

  PajakPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.taxIdCorp,
    this.lkpbuCode,
    this.npwp,
    this.npwpRegDate,
    this.bussinessRegCert,
    this.articleOfAssc,
    this.investorCompanyBICode,
  });
}

class PajakPerusahaanHelper {
  static PajakPerusahaanData data = PajakPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    taxIdCorp: null,
    lkpbuCode: null,
    npwp: null,
    npwpRegDate: null,
    bussinessRegCert: null,
    articleOfAssc: null,
    investorCompanyBICode: null,
  );
  static PajakPerusahaanData dokumen = data;
}
