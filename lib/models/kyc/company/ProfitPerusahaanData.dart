class ProfitPerusahaanData {
  bool isSubmitted;
  bool isEdited;
  String lastYearProfit;
  String twoYearProfit;
  String threeYearProfit;
  String investmentCorp;

  ProfitPerusahaanData({
    this.isSubmitted,
    this.isEdited,
    this.lastYearProfit,
    this.twoYearProfit,
    this.threeYearProfit,
    this.investmentCorp,
  });
}

class ProfitPerusahaanHelper {
  static ProfitPerusahaanData data = ProfitPerusahaanData(
    isSubmitted: null,
    isEdited: null,
    lastYearProfit: null,
    twoYearProfit: null,
    threeYearProfit: null,
    investmentCorp: null,
  );

  static ProfitPerusahaanData dokumen = data;
}
