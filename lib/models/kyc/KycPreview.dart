class KycPreview {
  final String title;
  final String subtitle;
  final String description;
  final String image;
  final bool isCenter;

  KycPreview({
    this.title,
    this.subtitle,
    this.description,
    this.image,
    this.isCenter = false,
  });
}
