import 'package:flutter/foundation.dart';

// untuk nandain status tiap field ( apakah aktif, loading, atau error )
class FieldStatus {
  bool isActive;
  bool isLoaded;
  bool isError;

  FieldStatus({
    @required this.isActive,
    @required this.isLoaded,
    this.isError = false,
  });
}
