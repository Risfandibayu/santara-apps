// To parse this JSON data, do
//
//     final regency = regencyFromJson(jsonString);

import 'dart:convert';

List<Regency> regencyFromJson(String str) => new List<Regency>.from(json.decode(str).map((x) => Regency.fromJson(x)));

String regencyToJson(List<Regency> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Regency {
    int id;
    String uuid;
    String name;
    int provinceId;
    String createdAt;
    String updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;

    Regency({
        this.id,
        this.uuid,
        this.name,
        this.provinceId,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory Regency.fromJson(Map<String, dynamic> json) => new Regency(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        provinceId: json["province_id"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "province_id": provinceId,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
