import 'dart:convert';

List<Supporter> supporterFromJson(String str) => new List<Supporter>.from(json.decode(str).map((x) => Supporter.fromJson(x)));

String supporterToJson(List<Supporter> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Supporter {
    String name;
    String logo;
    String link;

    Supporter({
        this.name,
        this.logo,
        this.link,
    });

    factory Supporter.fromJson(Map<String, dynamic> json) => new Supporter(
        name: json["name"],
        logo: json["logo"],
        link: json["link"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "logo": logo,
        "link": link,
    };
}
