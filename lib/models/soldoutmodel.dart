// To parse this JSON data, do
//
//     final soldoutModel = soldoutModelFromJson(jsonString);

import 'dart:convert';

List<SoldoutModel> soldoutModelFromJson(String str) => List<SoldoutModel>.from(
    json.decode(str).map((x) => SoldoutModel.fromJson(x)));

String soldoutModelToJson(List<SoldoutModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SoldoutModel {
  SoldoutModel({
    this.id,
    this.type,
    this.uuid,
    this.companyName,
    this.trademark,
    this.category,
    this.price,
    this.supply,
    this.beginPeriod,
    this.endPeriod,
    this.codeEmiten,
    this.period,
    this.address,
    this.pictures,
    this.profit,
    this.profitMin,
    this.investors,
    this.videoUrl,
    this.latitude,
    this.longitude,
    this.prospektus,
    this.minimumInvest,
    this.slug,
    this.dynamicLink,
    this.avgCapitalNeeds,
    this.avgAnnualTurnoverCurrentYear,
    this.avgAnnualTurnoverPreviousYear,
    this.avgGeneralShareAmount,
    this.avgTurnoverAfterBecomingAPublisher,
    this.avgAnnualDividen,
    this.businessDescription,
    this.createdAt,
    this.maxInvestorToggle,
    this.isActive,
    this.ownerName,
    this.ownerPicture,
    this.logo,
    this.gradients,
    this.terjual,
    this.percentageSold,
    this.totalInvestor,
    this.name,
    this.bio,
    this.photo,
    this.phone,
    this.sisa,
    this.startFrom,
  });

  int id;
  String type;
  String uuid;
  String companyName;
  String trademark;
  String category;
  int price;
  int supply;
  DateTime beginPeriod;
  DateTime endPeriod;
  String codeEmiten;
  String period;
  String address;
  List<Picture> pictures;
  double profit;
  double profitMin;
  int investors;
  String videoUrl;
  double latitude;
  double longitude;
  String prospektus;
  double minimumInvest;
  String slug;
  String dynamicLink;
  dynamic avgCapitalNeeds;
  dynamic avgAnnualTurnoverCurrentYear;
  dynamic avgAnnualTurnoverPreviousYear;
  dynamic avgGeneralShareAmount;
  dynamic avgTurnoverAfterBecomingAPublisher;
  dynamic avgAnnualDividen;
  String businessDescription;
  DateTime createdAt;
  int maxInvestorToggle;
  int isActive;
  dynamic ownerName;
  dynamic ownerPicture;
  String logo;
  dynamic gradients;
  int terjual;
  String percentageSold;
  dynamic totalInvestor;
  String name;
  String bio;
  String photo;
  String phone;
  int sisa;
  double startFrom;

  factory SoldoutModel.fromJson(Map<String, dynamic> json) => SoldoutModel(
        id: json["id"],
        type: json["type"],
        uuid: json["uuid"],
        companyName: json["company_name"],
        trademark: json["trademark"],
        category: json["category"],
        price: json["price"],
        supply: json["supply"],
        beginPeriod: DateTime.parse(json["begin_period"]),
        endPeriod: DateTime.parse(json["end_period"]),
        codeEmiten: json["code_emiten"],
        period: json["period"],
        address: json["address"],
        pictures: List<Picture>.from(
            json["pictures"].map((x) => Picture.fromJson(x))),
        profit: json["profit"].toDouble(),
        profitMin: json["profit_min"].toDouble(),
        investors: json["investors"],
        videoUrl: json["video_url"],
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
        prospektus: json["prospektus"],
        minimumInvest: json["minimum_invest"].toDouble(),
        slug: json["slug"],
        dynamicLink: json["dynamic_link"],
        avgCapitalNeeds: json["avg_capital_needs"],
        avgAnnualTurnoverCurrentYear: json["avg_annual_turnover_current_year"],
        avgAnnualTurnoverPreviousYear:
            json["avg_annual_turnover_previous_year"],
        avgGeneralShareAmount: json["avg_general_share_amount"],
        avgTurnoverAfterBecomingAPublisher:
            json["avg_turnover_after_becoming_a_publisher"],
        avgAnnualDividen: json["avg_annual_dividen"],
        businessDescription: json["business_description"],
        createdAt: DateTime.parse(json["created_at"]),
        maxInvestorToggle: json["max_investor_toggle"],
        isActive: json["is_active"],
        ownerName: json["owner_name"],
        ownerPicture: json["owner_picture"],
        logo: json["logo"],
        gradients: json["gradients"],
        terjual: json["terjual"],
        percentageSold: json["percentage_sold"],
        totalInvestor: json["total_investor"],
        name: json["name"],
        bio: json["bio"],
        photo: json["photo"],
        phone: json["phone"],
        sisa: json["sisa"],
        startFrom: json["start_from"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "uuid": uuid,
        "company_name": companyName,
        "trademark": trademark,
        "category": category,
        "price": price,
        "supply": supply,
        "begin_period": beginPeriod.toIso8601String(),
        "end_period": endPeriod.toIso8601String(),
        "code_emiten": codeEmiten,
        "period": period,
        "address": address,
        "pictures": List<dynamic>.from(pictures.map((x) => x.toJson())),
        "profit": profit,
        "profit_min": profitMin,
        "investors": investors,
        "video_url": videoUrl,
        "latitude": latitude,
        "longitude": longitude,
        "prospektus": prospektus,
        "minimum_invest": minimumInvest,
        "slug": slug,
        "dynamic_link": dynamicLink,
        "avg_capital_needs": avgCapitalNeeds,
        "avg_annual_turnover_current_year": avgAnnualTurnoverCurrentYear,
        "avg_annual_turnover_previous_year": avgAnnualTurnoverPreviousYear,
        "avg_general_share_amount": avgGeneralShareAmount,
        "avg_turnover_after_becoming_a_publisher":
            avgTurnoverAfterBecomingAPublisher,
        "avg_annual_dividen": avgAnnualDividen,
        "business_description": businessDescription,
        "created_at": createdAt.toIso8601String(),
        "max_investor_toggle": maxInvestorToggle,
        "is_active": isActive,
        "owner_name": ownerName,
        "owner_picture": ownerPicture,
        "logo": logo,
        "gradients": gradients,
        "terjual": terjual,
        "percentage_sold": percentageSold,
        "total_investor": totalInvestor,
        "name": name,
        "bio": bio,
        "photo": photo,
        "phone": phone,
        "sisa": sisa,
        "start_from": startFrom,
      };
}

class Picture {
  Picture({
    this.picture,
  });

  String picture;

  factory Picture.fromJson(Map<String, dynamic> json) => Picture(
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
