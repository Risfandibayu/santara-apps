// To parse this JSON data, do
//
//     final perdana = perdanaFromJson(jsonString);

import 'dart:convert';

import 'package:santaraapp/utils/api.dart';

List<Perdana> perdanaFromJson(String str) =>
    new List<Perdana>.from(json.decode(str).map((x) => Perdana.fromJson(x)));

String perdanaToJson(List<Perdana> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Perdana {
  String companyName;
  double amount;
  String code;
  dynamic token;
  List<Picture> picture;

  Perdana({
    this.companyName,
    this.amount,
    this.code,
    this.token,
    this.picture,
  });

  factory Perdana.fromJson(Map<String, dynamic> json) => new Perdana(
        companyName: json["company_name"],
        amount: json["amount"] / 1,
        code: json["code"],
        token: json["token"],
        picture: new List<Picture>.from(
            json["picture"].map((x) => Picture.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "company_name": companyName,
        "amount": amount,
        "code": code,
        "token": token,
        "picture": new List<dynamic>.from(picture.map((x) => x.toJson())),
      };
}

class Picture {
  String picture;

  Picture({
    this.picture,
  });

  factory Picture.fromJson(Map<String, dynamic> json) {
    String pic = json["picture"];
    if (!pic.contains('http')) {
      pic = '$urlAsset/token/${json["picture"]}';
    }
    return Picture(
      picture: pic,
    );
  }

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
