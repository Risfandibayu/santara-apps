// To parse this JSON data, do
import 'dart:convert';

List<BankInvestor> bankInvestorFromJson(String str) => List<BankInvestor>.from(json.decode(str).map((x) => BankInvestor.fromJson(x)));

String bankInvestorToJson(List<BankInvestor> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BankInvestor {
    int id;
    String bank;
    String code;

    BankInvestor({
        this.id,
        this.bank,
        this.code,
    });

    factory BankInvestor.fromJson(Map<String, dynamic> json) => BankInvestor(
        id: json["id"],
        bank: json["bank"],
        code: json["code"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "bank": bank,
        "code": code,
    };
}
