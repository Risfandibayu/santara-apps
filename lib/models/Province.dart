// To parse this JSON data, do
//
//     final province = provinceFromJson(jsonString);

import 'dart:convert';

List<Province> provinceFromJson(String str) => new List<Province>.from(json.decode(str).map((x) => Province.fromJson(x)));

String provinceToJson(List<Province> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Province {
    int id;
    String uuid;
    String name;
    String createdAt;
    String updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;
    int countryId;

    Province({
        this.id,
        this.uuid,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
        this.countryId,
    });

    factory Province.fromJson(Map<String, dynamic> json) => new Province(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
        countryId: json["country_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
        "country_id": countryId,
    };
}
