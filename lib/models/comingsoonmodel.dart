// To parse this JSON data, do
//
//     final comingSoonModel = comingSoonModelFromJson(jsonString);

import 'dart:convert';

List<ComingSoonModel> comingSoonModelFromJson(String str) =>
    List<ComingSoonModel>.from(
        json.decode(str).map((x) => ComingSoonModel.fromJson(x)));

String comingSoonModelToJson(List<ComingSoonModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ComingSoonModel {
  ComingSoonModel({
    this.id,
    this.uuid,
    this.traderId,
    this.companyName,
    this.businessEntity,
    this.trademark,
    this.address,
    this.regencyId,
    this.postalCode,
    this.businessDescription,
    this.businessLifespan,
    this.categoryId,
    this.subCategoryId,
    this.documentBusinessEntity,
    this.employee,
    this.branchCompany,
    this.monthlyTurnover,
    this.annualTurnover,
    this.monthlyProfit,
    this.annualProfit,
    this.monthlyTurnoverPreviousYear,
    this.monthlyProfitPreviousYear,
    this.assetValue,
    this.marketingArea,
    this.marketPotential,
    this.capitalNeeds,
    this.expansionPlan,
    this.newPotentialMarket,
    this.expectedMonthlyProfit,
    this.experience,
    this.employmentContract,
    this.advantages,
    this.disadvantages,
    this.isVerified,
    this.bankId,
    this.price,
    this.stockPrice,
    this.packageId,
    this.supply,
    this.beginPeriod,
    this.endPeriod,
    this.codeEmiten,
    this.status,
    this.pictures,
    this.period,
    this.profit,
    this.latitude,
    this.longitude,
    this.risk,
    this.prospektus,
    this.videoUrl,
    this.updatedBy,
    this.createdBy,
    this.tradeable,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.sold,
    this.minimumInvest,
    this.licenseDocument,
    this.license,
    this.expectedAnnualProfit,
    this.profitMin,
    this.investors,
    this.maxInvestors,
    this.slug,
    this.totalBankDebt,
    this.totalPaidCapital,
    this.bankNameFinancing,
    this.financialRecordingSystem,
    this.bankLoanReputation,
    this.marketPositionForTheProduct,
    this.strategyEmiten,
    this.officeStatus,
    this.levelOfBusinessCompetition,
    this.managerialAbility,
    this.technicalAbility,
    this.tnc,
    this.isPralisting,
    this.note,
    this.dynamicLink,
    this.isKsei,
    this.instagram,
    this.youtube,
    this.facebook,
    this.twitter,
    this.maxInvestorToggle,
    this.totalPrice,
    this.dividendPercentage,
    this.shareAmount,
    this.lastEmitenJourney,
    this.jurnalAccountName,
    this.type,
    this.email,
    this.fax,
    this.telephone,
    this.highlightIndustry,
    this.businessHistory,
    this.ownerName,
    this.businessStartDate,
    this.businessLocationCert,
    this.hasBankLoan,
    this.profitLossDesc,
    this.posValidationFile,
    this.profitLossMonthly,
    this.newDesc,
    this.newLocation,
    this.newFund,
    this.newMinFund,
    this.newPeriod,
    this.wacc,
    this.isPandhega,
    this.adminDesc,
    this.adminTrackRecord,
    this.pralistingScore,
    this.businessLocationStatus,
    this.lastPralistingSubmissionId,
    this.website,
    this.review,
    this.skNumberKemenkumham,
    this.ownerStock,
    this.existingStakeholder,
    this.telegram,
    this.financialRatioFile,
    this.scoringFile,
    this.profitLossFile,
    this.balanceSheetFile,
    this.isVerifiedByCeo,
    this.isShow,
    this.ownerPicture,
    this.avgAnnualTurnoverCurrentYear,
    this.avgTurnoverAfterBecomingAPublisher,
    this.avgCapitalNeeds,
    this.avgAnnualDividen,
    this.avgGeneralShareAmount,
    this.avgAnnualTurnoverPreviousYear,
    this.isComingSoon,
    this.isVerifiedBisnis,
    this.likes,
    this.vote,
    this.trdlike,
    this.cmt,
  });

  int id;
  String uuid;
  int traderId;
  String companyName;
  Advantages businessEntity;
  String trademark;
  String address;
  int regencyId;
  Advantages postalCode;
  String businessDescription;
  int businessLifespan;
  int categoryId;
  dynamic subCategoryId;
  Advantages documentBusinessEntity;
  int employee;
  String branchCompany;
  double monthlyTurnover;
  int annualTurnover;
  double monthlyProfit;
  int annualProfit;
  double monthlyTurnoverPreviousYear;
  double monthlyProfitPreviousYear;
  int assetValue;
  Advantages marketingArea;
  Advantages marketPotential;
  double capitalNeeds;
  Advantages expansionPlan;
  Advantages newPotentialMarket;
  int expectedMonthlyProfit;
  Advantages experience;
  Advantages employmentContract;
  Advantages advantages;
  Advantages disadvantages;
  int isVerified;
  dynamic bankId;
  int price;
  dynamic stockPrice;
  dynamic packageId;
  int supply;
  DateTime beginPeriod;
  DateTime endPeriod;
  String codeEmiten;
  dynamic status;
  String pictures;
  String period;
  double profit;
  double latitude;
  double longitude;
  dynamic risk;
  String prospektus;
  String videoUrl;
  dynamic updatedBy;
  dynamic createdBy;
  int tradeable;
  int isActive;
  int isDeleted;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic sold;
  int minimumInvest;
  dynamic licenseDocument;
  dynamic license;
  int expectedAnnualProfit;
  double profitMin;
  int investors;
  int maxInvestors;
  Slug slug;
  double totalBankDebt;
  int totalPaidCapital;
  String bankNameFinancing;
  FinancialRecordingSystem financialRecordingSystem;
  BankLoanReputation bankLoanReputation;
  MarketPositionForTheProduct marketPositionForTheProduct;
  StrategyEmiten strategyEmiten;
  OfficeStatus officeStatus;
  LevelOfBusinessCompetition levelOfBusinessCompetition;
  LevelOfBusinessCompetition managerialAbility;
  String technicalAbility;
  int tnc;
  int isPralisting;
  dynamic note;
  dynamic dynamicLink;
  int isKsei;
  String instagram;
  String youtube;
  String facebook;
  String twitter;
  int maxInvestorToggle;
  dynamic totalPrice;
  double dividendPercentage;
  int shareAmount;
  LastEmitenJourney lastEmitenJourney;
  dynamic jurnalAccountName;
  Type type;
  String email;
  int fax;
  int telephone;
  dynamic highlightIndustry;
  dynamic businessHistory;
  String ownerName;
  dynamic businessStartDate;
  dynamic businessLocationCert;
  dynamic hasBankLoan;
  dynamic profitLossDesc;
  dynamic posValidationFile;
  dynamic profitLossMonthly;
  dynamic newDesc;
  dynamic newLocation;
  dynamic newFund;
  dynamic newMinFund;
  dynamic newPeriod;
  dynamic wacc;
  dynamic isPandhega;
  String adminDesc;
  dynamic adminTrackRecord;
  dynamic pralistingScore;
  BusinessLocationStatus businessLocationStatus;
  int lastPralistingSubmissionId;
  String website;
  dynamic review;
  dynamic skNumberKemenkumham;
  dynamic ownerStock;
  dynamic existingStakeholder;
  dynamic telegram;
  dynamic financialRatioFile;
  dynamic scoringFile;
  dynamic profitLossFile;
  dynamic balanceSheetFile;
  int isVerifiedByCeo;
  int isShow;
  String ownerPicture;
  int avgAnnualTurnoverCurrentYear;
  int avgTurnoverAfterBecomingAPublisher;
  int avgCapitalNeeds;
  int avgAnnualDividen;
  int avgGeneralShareAmount;
  int avgAnnualTurnoverPreviousYear;
  int isComingSoon;
  dynamic isVerifiedBisnis;
  int likes;
  int vote;
  String trdlike;
  int cmt;

  factory ComingSoonModel.fromJson(Map<String, dynamic> json) =>
      ComingSoonModel(
        id: json["id"],
        uuid: json["uuid"],
        traderId: json["trader_id"] == null ? null : json["trader_id"],
        companyName: json["company_name"],
        businessEntity: advantagesValues.map[json["business_entity"]],
        trademark: json["trademark"],
        address: json["address"],
        regencyId: json["regency_id"] == null ? null : json["regency_id"],
        postalCode: json["postal_code"] == null
            ? null
            : advantagesValues.map[json["postal_code"]],
        businessDescription: json["business_description"],
        businessLifespan: json["business_lifespan"],
        categoryId: json["category_id"],
        subCategoryId: json["sub_category_id"],
        documentBusinessEntity: json["document_business_entity"] == null
            ? null
            : advantagesValues.map[json["document_business_entity"]],
        employee: json["employee"],
        branchCompany: json["branch_company"],
        monthlyTurnover: json["monthly_turnover"].toDouble(),
        annualTurnover: json["annual_turnover"],
        monthlyProfit: json["monthly_profit"].toDouble(),
        annualProfit: json["annual_profit"],
        monthlyTurnoverPreviousYear:
            json["monthly_turnover_previous_year"] == null
                ? null
                : json["monthly_turnover_previous_year"].toDouble(),
        monthlyProfitPreviousYear: json["monthly_profit_previous_year"] == null
            ? null
            : json["monthly_profit_previous_year"].toDouble(),
        assetValue: json["asset_value"],
        marketingArea: advantagesValues.map[json["marketing_area"]],
        marketPotential: advantagesValues.map[json["market_potential"]],
        capitalNeeds: json["capital_needs"].toDouble(),
        expansionPlan: advantagesValues.map[json["expansion_plan"]],
        newPotentialMarket: advantagesValues.map[json["new_potential_market"]],
        expectedMonthlyProfit: json["expected_monthly_profit"],
        experience: json["experience"] == null
            ? null
            : advantagesValues.map[json["experience"]],
        employmentContract: json["employment_contract"] == null
            ? null
            : advantagesValues.map[json["employment_contract"]],
        advantages: advantagesValues.map[json["advantages"]],
        disadvantages: advantagesValues.map[json["disadvantages"]],
        isVerified: json["is_verified"],
        bankId: json["bank_id"],
        price: json["price"] == null ? null : json["price"],
        stockPrice: json["stock_price"],
        packageId: json["package_id"],
        supply: json["supply"] == null ? null : json["supply"],
        beginPeriod: json["begin_period"] == null
            ? null
            : DateTime.parse(json["begin_period"]),
        endPeriod: json["end_period"] == null
            ? null
            : DateTime.parse(json["end_period"]),
        codeEmiten: json["code_emiten"] == null ? null : json["code_emiten"],
        status: json["status"],
        pictures: json["pictures"],
        period: json["period"] == null ? null : json["period"],
        profit: json["profit"] == null ? null : json["profit"].toDouble(),
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        risk: json["risk"],
        prospektus: json["prospektus"] == null ? null : json["prospektus"],
        videoUrl: json["video_url"] == null ? null : json["video_url"],
        updatedBy: json["updated_by"],
        createdBy: json["created_by"],
        tradeable: json["tradeable"],
        isActive: json["is_active"],
        isDeleted: json["is_deleted"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        sold: json["sold"],
        minimumInvest: json["minimum_invest"],
        licenseDocument: json["license_document"],
        license: json["license"],
        expectedAnnualProfit: json["expected_annual_profit"],
        profitMin:
            json["profit_min"] == null ? null : json["profit_min"].toDouble(),
        investors: json["investors"],
        maxInvestors: json["max_investors"],
        slug: json["slug"] == null ? null : slugValues.map[json["slug"]],
        totalBankDebt: json["total_bank_debt"] == null
            ? null
            : json["total_bank_debt"].toDouble(),
        totalPaidCapital: json["total_paid_capital"] == null
            ? null
            : json["total_paid_capital"],
        bankNameFinancing: json["bank_name_financing"] == null
            ? null
            : json["bank_name_financing"],
        financialRecordingSystem: json["financial_recording_system"] == null
            ? null
            : financialRecordingSystemValues
                .map[json["financial_recording_system"]],
        bankLoanReputation: json["bank_loan_reputation"] == null
            ? null
            : bankLoanReputationValues.map[json["bank_loan_reputation"]],
        marketPositionForTheProduct:
            json["market_position_for_the_product"] == null
                ? null
                : marketPositionForTheProductValues
                    .map[json["market_position_for_the_product"]],
        strategyEmiten: json["strategy_emiten"] == null
            ? null
            : strategyEmitenValues.map[json["strategy_emiten"]],
        officeStatus: json["office_status"] == null
            ? null
            : officeStatusValues.map[json["office_status"]],
        levelOfBusinessCompetition:
            json["level_of_business_competition"] == null
                ? null
                : levelOfBusinessCompetitionValues
                    .map[json["level_of_business_competition"]],
        managerialAbility: json["managerial_ability"] == null
            ? null
            : levelOfBusinessCompetitionValues.map[json["managerial_ability"]],
        technicalAbility: json["technical_ability"] == null
            ? null
            : json["technical_ability"],
        tnc: json["tnc"],
        isPralisting: json["is_pralisting"],
        note: json["note"],
        dynamicLink: json["dynamic_link"],
        isKsei: json["is_ksei"],
        instagram: json["instagram"] == null ? null : json["instagram"],
        youtube: json["youtube"] == null ? null : json["youtube"],
        facebook: json["facebook"] == null ? null : json["facebook"],
        twitter: json["twitter"] == null ? null : json["twitter"],
        maxInvestorToggle: json["max_investor_toggle"],
        totalPrice: json["total_price"],
        dividendPercentage: json["dividend_percentage"] == null
            ? null
            : json["dividend_percentage"].toDouble(),
        shareAmount: json["share_amount"] == null ? null : json["share_amount"],
        lastEmitenJourney:
            lastEmitenJourneyValues.map[json["last_emiten_journey"]],
        jurnalAccountName: json["jurnal_account_name"],
        type: typeValues.map[json["type"]],
        email: json["email"],
        fax: json["fax"],
        telephone: json["telephone"],
        highlightIndustry: json["highlight_industry"],
        businessHistory: json["business_history"],
        ownerName: json["owner_name"] == null ? null : json["owner_name"],
        businessStartDate: json["business_start_date"],
        businessLocationCert: json["business_location_cert"],
        hasBankLoan: json["has_bank_loan"],
        profitLossDesc: json["profit_loss_desc"],
        posValidationFile: json["pos_validation_file"],
        profitLossMonthly: json["profit_loss_monthly"],
        newDesc: json["new_desc"],
        newLocation: json["new_location"],
        newFund: json["new_fund"],
        newMinFund: json["new_min_fund"],
        newPeriod: json["new_period"],
        wacc: json["wacc"],
        isPandhega: json["is_pandhega"],
        adminDesc: json["admin_desc"] == null ? null : json["admin_desc"],
        adminTrackRecord: json["admin_track_record"],
        pralistingScore: json["pralisting_score"],
        businessLocationStatus:
            businessLocationStatusValues.map[json["business_location_status"]],
        lastPralistingSubmissionId:
            json["last_pralisting_submission_id"] == null
                ? null
                : json["last_pralisting_submission_id"],
        website: json["website"] == null ? null : json["website"],
        review: json["review"],
        skNumberKemenkumham: json["sk_number_kemenkumham"],
        ownerStock: json["owner_stock"],
        existingStakeholder: json["existing_stakeholder"],
        telegram: json["telegram"],
        financialRatioFile: json["financial_ratio_file"],
        scoringFile: json["scoring_file"],
        profitLossFile: json["profit_loss_file"],
        balanceSheetFile: json["balance_sheet_file"],
        isVerifiedByCeo: json["is_verified_by_ceo"] == null
            ? null
            : json["is_verified_by_ceo"],
        isShow: json["is_show"],
        ownerPicture:
            json["owner_picture"] == null ? null : json["owner_picture"],
        avgAnnualTurnoverCurrentYear: json["avg_annual_turnover_current_year"],
        avgTurnoverAfterBecomingAPublisher:
            json["avg_turnover_after_becoming_a_publisher"],
        avgCapitalNeeds: json["avg_capital_needs"],
        avgAnnualDividen: json["avg_annual_dividen"],
        avgGeneralShareAmount: json["avg_general_share_amount"],
        avgAnnualTurnoverPreviousYear:
            json["avg_annual_turnover_previous_year"],
        isComingSoon: json["is_coming_soon"],
        isVerifiedBisnis: json["is_verified_bisnis"],
        likes: json["likes"],
        vote: json["vote"],
        trdlike: json["trdlike"] == null ? null : json["trdlike"],
        cmt: json["cmt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "trader_id": traderId == null ? null : traderId,
        "company_name": companyName,
        "business_entity": advantagesValues.reverse[businessEntity],
        "trademark": trademark,
        "address": address,
        "regency_id": regencyId == null ? null : regencyId,
        "postal_code":
            postalCode == null ? null : advantagesValues.reverse[postalCode],
        "business_description": businessDescription,
        "business_lifespan": businessLifespan,
        "category_id": categoryId,
        "sub_category_id": subCategoryId,
        "document_business_entity": documentBusinessEntity == null
            ? null
            : advantagesValues.reverse[documentBusinessEntity],
        "employee": employee,
        "branch_company": branchCompany,
        "monthly_turnover": monthlyTurnover,
        "annual_turnover": annualTurnover,
        "monthly_profit": monthlyProfit,
        "annual_profit": annualProfit,
        "monthly_turnover_previous_year": monthlyTurnoverPreviousYear == null
            ? null
            : monthlyTurnoverPreviousYear,
        "monthly_profit_previous_year": monthlyProfitPreviousYear == null
            ? null
            : monthlyProfitPreviousYear,
        "asset_value": assetValue,
        "marketing_area": advantagesValues.reverse[marketingArea],
        "market_potential": advantagesValues.reverse[marketPotential],
        "capital_needs": capitalNeeds,
        "expansion_plan": advantagesValues.reverse[expansionPlan],
        "new_potential_market": advantagesValues.reverse[newPotentialMarket],
        "expected_monthly_profit": expectedMonthlyProfit,
        "experience":
            experience == null ? null : advantagesValues.reverse[experience],
        "employment_contract": employmentContract == null
            ? null
            : advantagesValues.reverse[employmentContract],
        "advantages": advantagesValues.reverse[advantages],
        "disadvantages": advantagesValues.reverse[disadvantages],
        "is_verified": isVerified,
        "bank_id": bankId,
        "price": price == null ? null : price,
        "stock_price": stockPrice,
        "package_id": packageId,
        "supply": supply == null ? null : supply,
        "begin_period":
            beginPeriod == null ? null : beginPeriod.toIso8601String(),
        "end_period": endPeriod == null ? null : endPeriod.toIso8601String(),
        "code_emiten": codeEmiten == null ? null : codeEmiten,
        "status": status,
        "pictures": pictures,
        "period": period == null ? null : period,
        "profit": profit == null ? null : profit,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "risk": risk,
        "prospektus": prospektus == null ? null : prospektus,
        "video_url": videoUrl == null ? null : videoUrl,
        "updated_by": updatedBy,
        "created_by": createdBy,
        "tradeable": tradeable,
        "is_active": isActive,
        "is_deleted": isDeleted,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "sold": sold,
        "minimum_invest": minimumInvest,
        "license_document": licenseDocument,
        "license": license,
        "expected_annual_profit": expectedAnnualProfit,
        "profit_min": profitMin == null ? null : profitMin,
        "investors": investors,
        "max_investors": maxInvestors,
        "slug": slug == null ? null : slugValues.reverse[slug],
        "total_bank_debt": totalBankDebt == null ? null : totalBankDebt,
        "total_paid_capital":
            totalPaidCapital == null ? null : totalPaidCapital,
        "bank_name_financing":
            bankNameFinancing == null ? null : bankNameFinancing,
        "financial_recording_system": financialRecordingSystem == null
            ? null
            : financialRecordingSystemValues.reverse[financialRecordingSystem],
        "bank_loan_reputation": bankLoanReputation == null
            ? null
            : bankLoanReputationValues.reverse[bankLoanReputation],
        "market_position_for_the_product": marketPositionForTheProduct == null
            ? null
            : marketPositionForTheProductValues
                .reverse[marketPositionForTheProduct],
        "strategy_emiten": strategyEmiten == null
            ? null
            : strategyEmitenValues.reverse[strategyEmiten],
        "office_status": officeStatus == null
            ? null
            : officeStatusValues.reverse[officeStatus],
        "level_of_business_competition": levelOfBusinessCompetition == null
            ? null
            : levelOfBusinessCompetitionValues
                .reverse[levelOfBusinessCompetition],
        "managerial_ability": managerialAbility == null
            ? null
            : levelOfBusinessCompetitionValues.reverse[managerialAbility],
        "technical_ability": technicalAbility == null ? null : technicalAbility,
        "tnc": tnc,
        "is_pralisting": isPralisting,
        "note": note,
        "dynamic_link": dynamicLink,
        "is_ksei": isKsei,
        "instagram": instagram == null ? null : instagram,
        "youtube": youtube == null ? null : youtube,
        "facebook": facebook == null ? null : facebook,
        "twitter": twitter == null ? null : twitter,
        "max_investor_toggle": maxInvestorToggle,
        "total_price": totalPrice,
        "dividend_percentage":
            dividendPercentage == null ? null : dividendPercentage,
        "share_amount": shareAmount == null ? null : shareAmount,
        "last_emiten_journey":
            lastEmitenJourneyValues.reverse[lastEmitenJourney],
        "jurnal_account_name": jurnalAccountName,
        "type": typeValues.reverse[type],
        "email": email,
        "fax": fax,
        "telephone": telephone,
        "highlight_industry": highlightIndustry,
        "business_history": businessHistory,
        "owner_name": ownerName == null ? null : ownerName,
        "business_start_date": businessStartDate,
        "business_location_cert": businessLocationCert,
        "has_bank_loan": hasBankLoan,
        "profit_loss_desc": profitLossDesc,
        "pos_validation_file": posValidationFile,
        "profit_loss_monthly": profitLossMonthly,
        "new_desc": newDesc,
        "new_location": newLocation,
        "new_fund": newFund,
        "new_min_fund": newMinFund,
        "new_period": newPeriod,
        "wacc": wacc,
        "is_pandhega": isPandhega,
        "admin_desc": adminDesc == null ? null : adminDesc,
        "admin_track_record": adminTrackRecord,
        "pralisting_score": pralistingScore,
        "business_location_status":
            businessLocationStatusValues.reverse[businessLocationStatus],
        "last_pralisting_submission_id": lastPralistingSubmissionId == null
            ? null
            : lastPralistingSubmissionId,
        "website": website == null ? null : website,
        "review": review,
        "sk_number_kemenkumham": skNumberKemenkumham,
        "owner_stock": ownerStock,
        "existing_stakeholder": existingStakeholder,
        "telegram": telegram,
        "financial_ratio_file": financialRatioFile,
        "scoring_file": scoringFile,
        "profit_loss_file": profitLossFile,
        "balance_sheet_file": balanceSheetFile,
        "is_verified_by_ceo": isVerifiedByCeo == null ? null : isVerifiedByCeo,
        "is_show": isShow,
        "owner_picture": ownerPicture == null ? null : ownerPicture,
        "avg_annual_turnover_current_year": avgAnnualTurnoverCurrentYear,
        "avg_turnover_after_becoming_a_publisher":
            avgTurnoverAfterBecomingAPublisher,
        "avg_capital_needs": avgCapitalNeeds,
        "avg_annual_dividen": avgAnnualDividen,
        "avg_general_share_amount": avgGeneralShareAmount,
        "avg_annual_turnover_previous_year": avgAnnualTurnoverPreviousYear,
        "is_coming_soon": isComingSoon,
        "is_verified_bisnis": isVerifiedBisnis,
        "likes": likes,
        "vote": vote,
        "trdlike": trdlike == null ? null : trdlike,
        "cmt": cmt,
      };
}

enum Advantages { EMPTY, ADVANTAGES, PILIH_SALAH_SATU, CV, PT, YANG_LAIN }

final advantagesValues = EnumValues({
  "": Advantages.ADVANTAGES,
  "CV": Advantages.CV,
  "-": Advantages.EMPTY,
  "Pilih Salah Satu": Advantages.PILIH_SALAH_SATU,
  "PT": Advantages.PT,
  "Yang Lain": Advantages.YANG_LAIN
});

enum BankLoanReputation {
  PILIH_SALAH_SATU,
  MEMILIKI_PINJAMAN_LANCAR,
  TIDAK_MEMILIKI_PINJAMAN
}

final bankLoanReputationValues = EnumValues({
  "Memiliki pinjaman lancar": BankLoanReputation.MEMILIKI_PINJAMAN_LANCAR,
  "Pilih Salah Satu": BankLoanReputation.PILIH_SALAH_SATU,
  "Tidak memiliki pinjaman": BankLoanReputation.TIDAK_MEMILIKI_PINJAMAN
});

enum BusinessLocationStatus { SEWA }

final businessLocationStatusValues =
    EnumValues({"sewa": BusinessLocationStatus.SEWA});

enum FinancialRecordingSystem {
  PILIH_SALAH_SATU,
  CATATAN_PEMBUKUAN_SEDERHANA_POS,
  TERKOMPUTERISASI_SOFTWARE_AKUNTANSI,
  HANYA_BERUPA_BUKTI_DOKUMENTASI
}

final financialRecordingSystemValues = EnumValues({
  "Catatan pembukuan sederhana/POS":
      FinancialRecordingSystem.CATATAN_PEMBUKUAN_SEDERHANA_POS,
  "Hanya berupa bukti dokumentasi":
      FinancialRecordingSystem.HANYA_BERUPA_BUKTI_DOKUMENTASI,
  "Pilih Salah Satu": FinancialRecordingSystem.PILIH_SALAH_SATU,
  "Terkomputerisasi/Software akuntansi":
      FinancialRecordingSystem.TERKOMPUTERISASI_SOFTWARE_AKUNTANSI
});

enum LastEmitenJourney { PRA_PENAWARAN_SAHAM }

final lastEmitenJourneyValues =
    EnumValues({"PRA PENAWARAN SAHAM": LastEmitenJourney.PRA_PENAWARAN_SAHAM});

enum LevelOfBusinessCompetition {
  PILIH_SALAH_SATU,
  MAMPU_MEMENANGKAN_PERSAINGAN,
  BERUSAHA_BERSAING_NAMUN_BUKAN_PEMIMPIN_PASAR,
  MAMPU_BERSAING_NAMUN_BUKAN_PEMIMPIN_PASAR
}

final levelOfBusinessCompetitionValues = EnumValues({
  "Berusaha bersaing namun bukan pemimpin pasar":
      LevelOfBusinessCompetition.BERUSAHA_BERSAING_NAMUN_BUKAN_PEMIMPIN_PASAR,
  "Mampu bersaing namun bukan pemimpin pasar":
      LevelOfBusinessCompetition.MAMPU_BERSAING_NAMUN_BUKAN_PEMIMPIN_PASAR,
  "Mampu memenangkan persaingan":
      LevelOfBusinessCompetition.MAMPU_MEMENANGKAN_PERSAINGAN,
  "Pilih Salah Satu": LevelOfBusinessCompetition.PILIH_SALAH_SATU
});

enum MarketPositionForTheProduct {
  PILIH_SALAH_SATU,
  PEMIMPIN_PASAR_LOKAL_NASIONAL,
  MAMPU_BERSAING_DI_PASAR_LOKAL_NASIONAL,
  BERUSAHA_BERSAING_DI_PASAR_LOKAL_NASIONAL
}

final marketPositionForTheProductValues = EnumValues({
  "Berusaha bersaing di pasar lokal/nasional":
      MarketPositionForTheProduct.BERUSAHA_BERSAING_DI_PASAR_LOKAL_NASIONAL,
  "Mampu bersaing di pasar lokal/nasional":
      MarketPositionForTheProduct.MAMPU_BERSAING_DI_PASAR_LOKAL_NASIONAL,
  "Pemimpin pasar lokal/nasional":
      MarketPositionForTheProduct.PEMIMPIN_PASAR_LOKAL_NASIONAL,
  "Pilih Salah Satu": MarketPositionForTheProduct.PILIH_SALAH_SATU
});

enum OfficeStatus {
  PILIH_SALAH_SATU,
  MILIK_SENDIRI_SEWA_5_TAHUN,
  SEWA_2_S_D_5_TAHUN,
  SEWA_BULANAN
}

final officeStatusValues = EnumValues({
  "Milik sendiri/sewa > 5 Tahun": OfficeStatus.MILIK_SENDIRI_SEWA_5_TAHUN,
  "Pilih Salah Satu": OfficeStatus.PILIH_SALAH_SATU,
  "Sewa > 2 s.d 5 Tahun": OfficeStatus.SEWA_2_S_D_5_TAHUN,
  "Sewa Bulanan": OfficeStatus.SEWA_BULANAN
});

enum Slug { EMPTY, SLUG, WARIST_BUMI_KARYA }

final slugValues = EnumValues({
  "-": Slug.EMPTY,
  "": Slug.SLUG,
  "warist-bumi-karya": Slug.WARIST_BUMI_KARYA
});

enum StrategyEmiten {
  PILIH_SALAH_SATU,
  PUNYA_MILESTONE_JANGKA_PANJANG_OWNER_INFRASTRUKTUR_SIAP,
  STRATEGI_CASE_BY_CASE_TENTAVIE_AGAR_EFEKTIF,
  MILESTONE_SEDANG_DISUSUN_OWNER_INFRASTRUKTUR_SEDANG_DIPERKUAT
}

final strategyEmitenValues = EnumValues({
  "Milestone sedang disusun owner & infrastruktur sedang diperkuat":
      StrategyEmiten
          .MILESTONE_SEDANG_DISUSUN_OWNER_INFRASTRUKTUR_SEDANG_DIPERKUAT,
  "Pilih Salah Satu": StrategyEmiten.PILIH_SALAH_SATU,
  "Punya milestone jangka panjang owner & infrastruktur siap":
      StrategyEmiten.PUNYA_MILESTONE_JANGKA_PANJANG_OWNER_INFRASTRUKTUR_SIAP,
  "Strategi case by case/tentavie agar efektif":
      StrategyEmiten.STRATEGI_CASE_BY_CASE_TENTAVIE_AGAR_EFEKTIF
});

enum Type { STOCK }

final typeValues = EnumValues({"stock": Type.STOCK});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
