// To parse this JSON data, do
//
//     final secondary = secondaryFromJson(jsonString);

import 'dart:convert';

List<Secondary> secondaryFromJson(String str) => new List<Secondary>.from(
    json.decode(str).map((x) => Secondary.fromJson(x)));

String secondaryToJson(List<Secondary> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Secondary {
  int id;
  String companyName;
  int price;
  int supply;
  String category;
  String codeEmiten;
  List<Picture> pictures;
  double terjual;

  Secondary({
    this.id,
    this.companyName,
    this.price,
    this.supply,
    this.category,
    this.codeEmiten,
    this.pictures,
    this.terjual,
  });

  factory Secondary.fromJson(Map<String, dynamic> json) => new Secondary(
        id: json["id"],
        companyName: json["company_name"],
        price: json["price"],
        supply: json["supply"],
        category: json["category"],
        codeEmiten: json["code_emiten"],
        pictures: new List<Picture>.from(
            json["pictures"].map((x) => Picture.fromJson(x))),
        terjual: json["terjual"] != null ? json["terjual"].toDouble() : 0.0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "company_name": companyName,
        "price": price,
        "supply": supply,
        "category": category,
        "code_emiten": codeEmiten,
        "pictures": new List<dynamic>.from(pictures.map((x) => x.toJson())),
        "terjual": terjual,
      };
}

class Picture {
  String picture;

  Picture({
    this.picture,
  });

  factory Picture.fromJson(Map<String, dynamic> json) => new Picture(
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture,
      };
}
