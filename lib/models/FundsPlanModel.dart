class FundsPlanModel {
  String planStatus;
  String planDeadline;
  int total;
  List<ListFundPlans> listFundPlans;

  FundsPlanModel(
      {this.planStatus, this.planDeadline, this.total, this.listFundPlans});

  FundsPlanModel.fromJson(Map<String, dynamic> json) {
    planStatus = json['plan_status'];
    planDeadline = json['plan_deadline'];
    total = json['total'];
    if (json['list_fund_plans'] != null) {
      listFundPlans = new List<ListFundPlans>();
      json['list_fund_plans'].forEach((v) {
        listFundPlans.add(new ListFundPlans.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['plan_status'] = this.planStatus;
    data['plan_deadline'] = this.planDeadline;
    data['total'] = this.total;
    if (this.listFundPlans != null) {
      data['list_fund_plans'] =
          this.listFundPlans.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListFundPlans {
  String name;
  int subtotat;
  String desc;
  List<Sublist> sublist;

  ListFundPlans({this.name, this.subtotat, this.desc, this.sublist});

  ListFundPlans.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    subtotat = json['subtotat'];
    desc = json['desc'];
    if (json['sublist'] != null) {
      sublist = new List<Sublist>();
      json['sublist'].forEach((v) {
        sublist.add(new Sublist.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['subtotat'] = this.subtotat;
    data['desc'] = this.desc;
    if (this.sublist != null) {
      data['sublist'] = this.sublist.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Sublist {
  String desc;
  int amount;

  Sublist({this.desc, this.amount});

  Sublist.fromJson(Map<String, dynamic> json) {
    desc = json['desc'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['desc'] = this.desc;
    data['amount'] = this.amount;
    return data;
  }
}

