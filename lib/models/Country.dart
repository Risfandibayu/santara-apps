import 'dart:convert';

List<Country> countryFromJson(String str) => List<Country>.from(json.decode(str).map((x) => Country.fromJson(x)));

String countryToJson(List<Country> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Country {
    int id;
    String uuid;
    String name;
    DateTime createdAt;
    DateTime updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;

    Country({
        this.id,
        this.uuid,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: json["id"],
        uuid: json["uuid"],
        name: json["name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "name": name,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
