// To parse this JSON data, do
//
import 'dart:convert';

List<EmitenDividen> emitenDividenFromJson(String str) => List<EmitenDividen>.from(json.decode(str).map((x) => EmitenDividen.fromJson(x)));

String emitenDividenToJson(List<EmitenDividen> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmitenDividen {
    String title;
    int amount;
    String dividenDate;

    EmitenDividen({
        this.title,
        this.amount,
        this.dividenDate,
    });

    factory EmitenDividen.fromJson(Map<String, dynamic> json) => EmitenDividen(
        title: json["title"],
        amount: json["amount"],
        dividenDate: json["dividen_date"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "amount": amount,
        "dividen_date": dividenDate,
    };
}
