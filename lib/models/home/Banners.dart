import 'dart:convert';

import 'package:equatable/equatable.dart';

List<Banner> bannerFromJson(String str) =>
    new List<Banner>.from(json.decode(str).map((x) => Banner.fromJson(x)));

String bannerToJson(List<Banner> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Banner extends Equatable {
  final String title;
  final String mobile;
  final String link;

  Banner({
    this.title,
    this.mobile,
    this.link,
  });

  factory Banner.fromJson(Map<String, dynamic> json) => new Banner(
      title: json["title"], mobile: json["mobile"], link: json["redirection"]);

  Map<String, dynamic> toJson() => {
        "title": title,
        "mobile": mobile,
        "redirection": link,
      };

  @override
  List<Object> get props => [title, mobile, link];
}
