// To parse this JSON data, do
//
//     final notification = notificationFromJson(jsonString);

import 'dart:convert';

List<Notifications> notificationsFromJson(String str) => new List<Notifications>.from(json.decode(str).map((x) => Notifications.fromJson(x)));

String notificationsToJson(List<Notifications> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Notifications {
    int id;
    String uuid;
    String action;
    int userId;
    String message;
    String title;
    String createdAt;
    String updatedAt;
    int isDeleted;
    dynamic createdBy;
    dynamic updatedBy;

    Notifications({
        this.id,
        this.uuid,
        this.action,
        this.userId,
        this.message,
        this.title,
        this.createdAt,
        this.updatedAt,
        this.isDeleted,
        this.createdBy,
        this.updatedBy,
    });

    factory Notifications.fromJson(Map<String, dynamic> json) => new Notifications(
        id: json["id"],
        uuid: json["uuid"],
        action: json["action"],
        userId: json["user_id"],
        message: json["message"],
        title: json["title"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isDeleted: json["is_deleted"],
        createdBy: json["created_by"],
        updatedBy: json["updated_by"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "uuid": uuid,
        "action": action,
        "user_id": userId,
        "message": message,
        "title": title,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_deleted": isDeleted,
        "created_by": createdBy,
        "updated_by": updatedBy,
    };
}
