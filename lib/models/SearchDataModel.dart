class SearchData {
  final String id;
  final String uuid;
  final String name;

  SearchData({
    this.id,
    this.uuid,
    this.name,
  });

  factory SearchData.fromJson(Map<String, dynamic> json) => SearchData(
        id: json["id"] == null ? null : json["id"].toString(),
        name: json["text"] == null ? null : json["text"].toString(),
        uuid: json["uuid"] == null ? null : json["uuid"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "text": name == null ? null : name,
        "uuid": uuid == null ? null : uuid,
      };
}
