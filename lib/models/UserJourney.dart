import 'dart:convert';

UserJourney userJourneyFromJson(String str) =>
    UserJourney.fromJson(json.decode(str));

String userJourneyToJson(UserJourney data) => json.encode(data.toJson());

class UserJourney {
  int total;
  int perPage;
  int page;
  int lastPage;
  List<Datum> data;

  UserJourney({
    this.total,
    this.perPage,
    this.page,
    this.lastPage,
    this.data,
  });

  factory UserJourney.fromJson(Map<String, dynamic> json) => UserJourney(
        total: json["total"],
        perPage: json["perPage"],
        page: json["page"],
        lastPage: json["lastPage"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "perPage": perPage,
        "page": page,
        "lastPage": lastPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  String activity;
  String information1;
  String information2;
  String information3;
  String information4;
  String information5;
  String status;
  DateTime createdAt;

  Datum({
    this.activity,
    this.information1,
    this.information2,
    this.information3,
    this.information4,
    this.information5,
    this.status,
    this.createdAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        activity: json["activity"],
        information1:
            json["information1"] == null ? null : json["information1"],
        information2:
            json["information2"] == null ? null : json["information2"],
        information3:
            json["information3"] == null ? null : json["information3"],
        information4:
            json["information4"] == null ? null : json["information4"],
        information5:
            json["information5"] == null ? null : json["information5"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "activity": activity,
        "information1": information1 == null ? null : information1,
        "information2": information2 == null ? null : information2,
        "information3": information3 == null ? null : information3,
        "information4": information4 == null ? null : information4,
        "information5": information5 == null ? null : information5,
        "status": status,
        "created_at": createdAt.toIso8601String(),
      };
}
