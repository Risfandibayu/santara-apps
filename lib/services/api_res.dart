// Success = statuscode 200
// serverFailure = 400/500 etc
// localFailure = unexpected error on try catch

import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:dio/dio.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/logger.dart';

enum ApiStatus {
  success,
  notFound,
  serverFailure,
  badRequest,
  localFailure,
  unauthorized
}

class ApiResult {
  final ApiStatus status;
  final String message;
  final dynamic data;

  ApiResult({this.status, this.message, this.data});
}

class NetworkRequest {
  final Dio dio = Dio();
  final storage = secureStorage.FlutterSecureStorage();

  ApiResult parseRequest(Response response) {
    switch (response.statusCode) {
      case 200:
        return ApiResult(
          status: ApiStatus.success,
          message: "Success",
          data: response.data,
        );
        break;
      case 404:
        return ApiResult(
          status: ApiStatus.notFound,
          message: "Data Not Found",
          data: null,
        );
      case 401:
        return ApiResult(
          status: ApiStatus.unauthorized,
          message: "Unauthorized",
          data: null,
        );
      case 400:
        return ApiResult(
          status: ApiStatus.badRequest,
          message: response?.data["message"] != null
              ? response.data["message"]
              : "An 400 error has occured!",
          data: null,
        );
      default:
        return ApiResult(
          status: ApiStatus.serverFailure,
          message: "${response.statusCode}",
          data: response.data,
        );
        break;
    }
  }

  // Api Wrapper
  Future<ApiResult> getData(String url) async {
    try {
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "",
        options: Options(
          headers: headers,
        ),
      );
      logger.i("""
      Status Code : ${response.statusCode} \n
      Url : $url
      """);
      return parseRequest(response);
    } on DioError catch (e) {
      logger.e("""
      >> URL         : ${e.request.uri} \n
      >> Status Code : ${e.response.statusCode}\n
      """);
      return parseRequest(e.response);
      // catchDioError(e);
    } catch (e, stack) {
      santaraLog(e, stack);
      return ApiResult(
        status: ApiStatus.localFailure,
        message: "$e",
        data: null,
      );
    }
  }

  Future<ApiResult> deleteData(String url) async {
    try {
      final headers = await UserAgent.headers();
      Response response = await dio.delete(
        "$url",
        options: Options(
          headers: headers,
        ),
      );
      logger.i("""
      Status Code : ${response.statusCode} \n
      Url : $url
      """);
      return parseRequest(response);
    } on DioError catch (e) {
      logger.e("""
      >> URL         : ${e.request.uri} \n
      >> Status Code : ${e.response.statusCode}\n
      """);
      return parseRequest(e.response);
      // catchDioError(e);
    } catch (e, stack) {
      santaraLog(e, stack);
      return ApiResult(
        status: ApiStatus.localFailure,
        message: "$e",
        data: null,
      );
    }
  }
}
