import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/helper.dart';
import 'package:http/http.dart' as http;

class AuthService {
  final storage = new FlutterSecureStorage();

  Future<bool> logout() async {
    var token, datas, refreshToken;
    try {
      token = await storage.read(key: "token");
      if (token != null) {
        datas = userFromJson(await storage.read(key: 'user'));
        refreshToken = await storage.read(key: "refreshToken");
        final uuid = datas.uuid;
        final http.Response response = await http.get(
            '$apiLocal/users/by-uuid/$uuid',
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        datas = userFromJson(response.body);
      }
      final headers = await UserAgent.headers();
      await http.post('$apiLocal/auth/logout',
          body: {
            "refreshToken": "$refreshToken",
            "user_id": "${datas.id}",
          },
          headers: headers);
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'uuid');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'userId');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.notif = 0;
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> logoutV2() async {
    var token, datas, refreshToken;
    try {
      token = await storage.read(key: "token");
      if (token != null) {
        datas = userFromJson(await storage.read(key: 'user'));
        refreshToken = await storage.read(key: "refreshToken");
        final uuid = datas.uuid;
        final http.Response response = await http.get(
            '$apiLocal/users/by-uuid/$uuid',
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        datas = userFromJson(response.body);
      }
      final headers = await UserAgent.headers();
      await http.post('$apiLocal/auth/logout',
          body: {
            "refreshToken": "$refreshToken",
            "user_id": "${datas.id}",
          },
          headers: headers);
      storage.delete(key: 'token');
      storage.delete(key: 'user');
      storage.delete(key: 'uuid');
      storage.delete(key: 'refreshToken');
      storage.delete(key: 'userId');
      storage.delete(key: 'newNotif');
      storage.delete(key: 'oldNotif');
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.notif = 0;
      return true;
    } catch (e) {
      storage.deleteAll();
      Helper.check = false;
      Helper.username = "Guest";
      Helper.email = "guest@gmail.com";
      Helper.userId = null;
      Helper.refreshToken = null;
      Helper.notif = 0;
      return true;
    }
  }

  Future<Response> checkUpdateKyc() async {
    try {
      // final headers = await UserAgent.headers();
      final token = await storage.read(key: 'token');

      final response = await http.post('$apiLocal/auth/update-verified-kwc',
          headers: {
            'Authorization': 'Bearer $token'
          }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> checkStatusKyc() async {
    try {
      // final headers = await UserAgent.headers();
      final token = await storage.read(key: 'token');

      final response = await http.get('$apiLocal/auth/check-status-newkyc',
          headers: {
            'Authorization': 'Bearer $token'
          }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<String> submitNewImage(File image) async {
    try {
      final token = await storage.read(key: 'token');
      Map<String, String> headers = {'Authorization': 'Bearer $token'};

      final multipartRequest = new http.MultipartRequest(
          'POST', Uri.parse('$apiLocal/auth/verified-newimage-kyc'));
      multipartRequest.headers.addAll(headers);
      multipartRequest.files.add(http.MultipartFile(
          'verified_kyc_image',
          File(image.path).readAsBytes().asStream(),
          File(image.path).lengthSync(),
          filename: image.path.split("/").last));
      var response = await multipartRequest.send();
      final respStr = await response.stream.bytesToString();
      return respStr;
    } catch (err) {
      return null;
    }
  }

  Future<Response> getBank() async {
    try {
      final token = await storage.read(key: 'token');
      final response = await http.get('$apiLocal/withdraw/get-databank-kyc',
          headers: {'Authorization': 'Bearer $token'});
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> checkFlagBank() async {
    try {
      final token = await storage.read(key: 'token');
      final response = await http.get('$apiLocal/withdraw/check-flag-bankwd',
          headers: {'Authorization': 'Bearer $token'});
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> getBankWithdraw() async {
    try {
      final token = await storage.read(key: 'token');
      final response = await http.get('$apiLocal/banks/bank-withdraw',
          headers: {'Authorization': 'Bearer $token'});
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> postBankTrader(String id, String accountNumber) async {
    try {
      final token = await storage.read(key: 'token');
      final response =
          await http.post('$apiLocal/withdraw/insert-new-bankwd', body: {
        'bank_wd_id': id,
        'account_currency_bwd': 'IDR',
        'account_number_bwd': accountNumber
      }, headers: {
        'Authorization': 'Bearer $token'
      });
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> postWithdraw(int amount, String pin, String finger) async {
    try {
      final token = await storage.read(key: 'token');
      final response = await http.post('$apiLocal/withdraw/',
          headers: {'Authorization': 'Bearer $token'},
          body: {'amount': '$amount', 'pin': pin, 'finger': finger});
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<Response> requestNewDividen(String pin, String finger) async {
    try {
      final token = await storage.read(key: 'token');
      final response = http.put('$apiLocal/devidend',
          headers: {'Authorization': 'Bearer $token'},
          body: {'pin': pin, 'finger': finger});
        return response;
    } catch(err) {
      return null;
    }
  }
}
