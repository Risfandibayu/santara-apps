import 'package:dio/dio.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';

class DepositService {
  final Dio dio = Dio();

  Future<Response> submitDeposit(
      int amount,
      String bankFrom,
      String account,
      String chanel,
      String bank,
      int fee,
      String token,
      String pin,
      bool finger) async {
    try {
      // FormData formData = FormData.fromMap();
      var body = {
        "amount": amount,
        "bank_from": bankFrom,
        "account_number": account,
        "channel": chanel,
        "bank": bank,
        // "fee": fee,
        "pin": pin,
        "finger": finger
      };
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            '$apiLocal/deposit/idr',
            data: body,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 20));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }
}
