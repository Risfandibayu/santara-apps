import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/Perdana.dart';
import 'package:santaraapp/utils/api.dart';

class KepemilikanSahamService {
  final storage = FlutterSecureStorage();

  Future<double> getMaxInvest() async {
    double maxInvest = 0;
    try {
      final token = await storage.read(key: 'token');
      final response = await http.get('$apiLocal/traders/max-invest',
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        maxInvest = data["max_invest"] / 1;
      } else {
        maxInvest = 0;
      }
    } on SocketException catch (_) {
      maxInvest = 0;
    }
    return maxInvest;
  }

  Future<double> getSahamDimiliki() async {
    double saldo = 0;
    List<Perdana> perdanaList;
    try {
      final token = await storage.read(key: 'token');
      final response = await http.get('$apiLocal/tokens/',
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      if (response.statusCode == 200) {
        perdanaList = perdanaFromJson(response.body);
        perdanaList.forEach((data) => {saldo += data.amount});
      } else {
        saldo = 0;
      }
    } on SocketException catch (_) {
      saldo = 0;
    }
    return saldo / 1;
  }
}
