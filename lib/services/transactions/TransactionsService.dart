import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/utils/api.dart';

class TransactionsService {
  final Dio dio = Dio();
  final NetworkRequest networkRequest = NetworkRequest();
  final storage = secureStorage.FlutterSecureStorage();

  // menerima data daftar transaksi yang belum dibayar
  Future<ApiResult> getCheckoutTransactions() async {
    return await networkRequest.getData("$apiLocal/transactions/checkout");
  }

  // membatalkan data pembelian
  Future<ApiResult> cancelTransaction(String id) async {
    return await networkRequest
        .deleteData("$apiLocal/transactions/cancel-payment?trx_id=$id");
  }

  // menerima data daftar riwayat transaksi
  Future<ApiResult> getHistoryTransactions() async {
    return await networkRequest.getData("$apiLocal/transactions/");
  }
}
