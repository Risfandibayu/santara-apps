import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/models/listing/Category.dart';
import 'package:santaraapp/utils/api.dart';

class ListingCategoryService {
  final storage = new FlutterSecureStorage();

  Future<List<Category>> fetchCategories() async {
    try {
      final token = await storage.read(key: 'token');
      final http.Response response = await http.get("$apiLocal/categories/",
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      if (response.statusCode == 200) {
        final res = json.decode(response.body);
        List<Category> categories = List<Category>();
        res.forEach((val) {
          categories.add(Category.fromJson(val));
        });
        return categories;
      } else {
        return null;
      }
    } catch (e) {
      // print(">> An error has occured, message : $e");
      return null;
    }
  }

  Future<List<Subcategory>> fetchSubCategories(int id) async {
    try {
      final token = await storage.read(key: 'token');
      final http.Response response = await http.get(
          "$apiLocal/categories/sub-category/$id",
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
      // print(">> Response : ");
      // print("$apiLocal/categories/sub-category/$id");
      // print(response.body);
      if (response.statusCode == 200) {
        final res = json.decode(response.body);
        List<Subcategory> subcategories = List<Subcategory>();
        res.forEach((val) {
          subcategories.add(Subcategory.fromJson(val));
        });
        return subcategories;
      } else {
        return null;
      }
    } catch (e) {
      // print(">> An error has occured, message : $e");
      return null;
    }
  }
}
