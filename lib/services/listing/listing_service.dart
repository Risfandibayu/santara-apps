import 'dart:async';
import 'dart:convert';
import 'package:async/async.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart' as path;
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Regency.dart';
import 'package:santaraapp/models/listing/CommentModel.dart';
import 'package:santaraapp/models/listing/DiajukanListModel.dart';
import 'package:santaraapp/models/listing/ListingDummies.dart';
import 'package:santaraapp/models/listing/Pralisting.dart';
import 'package:santaraapp/models/listing/PralistingEmiten.dart';
import 'package:santaraapp/models/listing/PralistingListModel.dart';
import 'package:santaraapp/models/listing/PralistingPagination.dart';
import 'package:santaraapp/models/listing/UploadImageModel.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';
// import 'package:santaraapp/models/listing/PralistingResult.dart';

class ListingApiService {
  final storage = new secureStorage.FlutterSecureStorage();
  final Dio dio = Dio();

  Future<PralistingModel> getPralisting() async {
    var header = {
      "Content-Type": "application/json",
      // "Authorization": "Bearer $token"
    };
    // print(">> Request data : ");
    var response = await http.get(
        "https://api.jsonbin.io/b/5e8744eadd6c3c63eaed9302/1",
        headers: header);
    if (response.statusCode == 200) {
      // print(">> Success ");
      // var result = pralistingResultModelFromJson(response.body);
      var parsedRes = json.decode(response.body);
      // print(">> First data : ");
      // print(parsedRes["data"][0]);
      // print(">> Parsed data : ");
      // var diti = PralistingDataModel.fromJson(parsedRes["data"][0]);
      // print(diti.toString());
      var result = PralistingModel.fromJson(parsedRes);
      // print(">> result : ");
      // print(result.toJson().toString());
      // print(">> Data : ");
      // print(parsedRes.toString());
      return result;
    } else {
      return null;
    }
  }

  Future<PralistingModel> getMostSubmission() async {
    var header = {
      "Content-Type": "application/json",
      // "Authorization": "Bearer $token"
    };

    var response = await http.get(
        "https://api.jsonbin.io/b/5e8744be93960d63f078346d/1",
        headers: header);
    if (response.statusCode == 200) {
      var parsedRes = json.decode(response.body);
      return PralistingModel.fromJson(parsedRes);
    } else {
      return null;
    }
  }

  Future<http.Response> fetchBisnisDiajukan(String status) async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get('$apiLocal/emitens/pre-listing?status=$status', headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (e) {
      // print(">> Error : $e");
      return null;
    }
  }

  Future<Response> setGambarUtama(String uuid, String gambar) async {
    try {
      final headers = await UserAgent.headers();
      var _imgs = [];
      _imgs.add(gambar);

      var formData = FormData.fromMap({
        "selected_images": json.encode(_imgs),
        "action": "set",
        "pictures": null,
        "new_pictures": null
      });

      Response response = await dio
          .put(
            '$apiLocal/emitens/update-media/$uuid',
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));
      // print(">> DIOK");
      // print(response.data);
      return response;
    } on DioError catch (e) {
      // print(">> DIO : ");
      // print(e.error);
      // print(e.request.uri);
      // print(e.response.data);
      // print(e.request.data.fields);
      // print(e.request.queryParameters);
      return e.response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<Response> hapusEmiten(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      // final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/emitens/pre-listing/delete/$uuid",
            // data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 15));
      return response;
    } on DioError catch (e) {
      // print(">> DIO : ");
      if (e.response.statusCode != 200) {
        return e.response;
      }
      // print(e.error);
      // print(e.request.uri);
      // print(e.response.data);
      // print(e.request.data.fields);
      // print(e.request.queryParameters);
      return e.response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<Response> hapusGambar(String uuid, String gambar) async {
    try {
      final headers = await UserAgent.headers();
      var _imgs = [];
      _imgs.add(gambar);

      var formData = FormData.fromMap({
        "selected_images": json.encode(_imgs),
        "action": "delete",
        "pictures": null,
        "new_pictures": null
      });

      // print(">> Data : ");
      // print(formData.fields);

      Response response = await dio
          .put(
            '$apiLocal/emitens/update-media/$uuid',
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 15));
      // print(">> DIOK");
      // print(response.data);
      return response;
    } on DioError catch (e) {
      // print(">> DIO : ");
      // final headers = await UserAgent.headers();
      // print(token);
      // print(e.error);
      // print(e.request.uri);
      // print(e.response.data);
      // print(e.request.data.fields);
      // print(e.request.queryParameters);
      return e.response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> saveMediaChanges(
      String uuid, String utube, List<UploadDataImage> images) async {
    try {
      // print(">> Images length : ");
      // print(images.length);
      var pictures = [];
      if (images != null && images.length > 0) {
        for (var i = 0; i < images.length; i++) {
          ByteData imgBytes = await images[i].asset.getByteData();
          List<int> imageData = imgBytes.buffer.asUint8List();
          var _picData = http.MultipartFile.fromBytes(
            'pictures',
            imageData,
            contentType: MediaType("image", "jpg"),
            filename: images[i].asset.name,
          );
          pictures.add(_picData);
        }
      }
      // final headers = await UserAgent.headers();
      final token = await storage.read(key: 'token');

      var request = new http.MultipartRequest(
          "PUT", Uri.parse("$apiLocal/emitens/update-media/$uuid"));
      request.headers[HttpHeaders.authorizationHeader] = 'Bearer $token';

      //adding items
      pictures.forEach((element) {
        request.files.add(element);
      });
      request.fields['video_url'] = utube;
      var response = await request.send();
      final resp = await http.Response.fromStream(response);
      return resp;
    } catch (e, stack) {
      // print(e);
      santaraLog(e, stack);
      return null;
    }
  }

  Future<Response> updateGambar(
      String uuid, String oldGbr, EmitenAssets gambar) async {
    try {
      final headers = await UserAgent.headers();
      var _imgs = [];
      _imgs.add(oldGbr);

      var formData = FormData.fromMap({
        "selected_images": json.encode(_imgs),
        "action": "update",
        "pictures": null,
        "new_pictures": await MultipartFile.fromFile(gambar.filepath,
            contentType: MediaType('image', 'png'),
            filename: gambar.asset.name),
      });

      // print(">> Data : ");
      // print(formData.fields);

      Response response = await dio
          .put(
            '$apiLocal/emitens/update-media/$uuid',
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));
      // print(">> DIOK");
      // print(response.data);
      return response;
    } on DioError catch (e) {
      // print(">> DIO : ");
      // final headers = await UserAgent.headers();
      // print(token);
      // print(e.error);
      // print(e.request.uri);
      // print(e.response.data);
      // print(e.request.data.fields);
      // print(e.request.queryParameters);
      return e.response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<CommentModel> fetchComments(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get("$apiLocal/emitens/pre-listing/comments/$uuid", headers: headers)
          .timeout(Duration(seconds: 15));
      if (response.statusCode == 200) {
        final res = json.decode(response.body);
        return CommentModel.fromJson(res);
      } else if (response.statusCode == 404) {
        return CommentModel();
      } else {
        return null;
      }
    } catch (e) {
      // print(">> Something error has occured : $e");
      return null;
    }
  }

  Future<http.Response> fetchCommentsV2(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get("$apiLocal/emitens/pre-listing/comments/$uuid", headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (e) {
      // print(">> Something error has occured : $e");
      return null;
    }
  }

  Future<http.Response> sendComment(String uuid, String comment) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
          '$apiLocal/emitens/pre-listing/comment/$uuid',
          headers: headers,
          body: {
            "comment": comment,
          }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> cancelInvestmentPlan(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http
          .post('$apiLocal/emitens/pre-listing/cancel-investment-plan/$uuid',
              headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> deleteComment(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http
          .post(
            '$apiLocal/emitens/pre-listing/delete-comment/$uuid',
            headers: headers,
          )
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> sendLike(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http
          .post('$apiLocal/emitens/pre-listing/like/$uuid', headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> sendReply(String uuid, String comment) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
          '$apiLocal/emitens/pre-listing/reply-comment/$uuid',
          headers: headers,
          body: {
            "comment": comment,
          }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> repostEmiten(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http
          .post('$apiLocal/emitens/pre-listing/re-submit/$uuid',
              headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> sendVote(String uuid) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http
          .post('$apiLocal/emitens/pre-listing/vote/$uuid', headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> sendInvestment(String uuid, int amount) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
          '$apiLocal/emitens/pre-listing/investment-plan',
          headers: headers,
          body: {
            "emiten_uuid": "$uuid",
            "amount": "$amount"
          }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  Future<http.Response> reportComment(String uuid, String description) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
          '$apiLocal/emitens/pre-listing/report/$uuid',
          headers: headers,
          body: {"description": "$description"}).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  // BARU
  Future<List<Regency>> fetchRegencies() async {
    // print(">> Fetching regency");
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get("$apiLocal/regencies/", headers: headers)
          .timeout(Duration(seconds: 15));
      List<Regency> regencies = List<Regency>();
      if (response.statusCode == 200) {
        // print(">> Fetching regency OK");
        regencies = regencyFromJson(response.body);
        regencies.sort((a, b) => a.name.compareTo(b.name));
        return regencies;
      } else {
        // print(">> Fetching regency NOT OK");
        return null;
      }
    } catch (e) {
      // print(">> Something error has occured : $e");
      return null;
    }
  }

  Future<List> parseGambar(EmitenDetail emitenDetail) async {
    List<MultipartFile> pics = [];
    emitenDetail.assets.forEach((val) async {
      var imgBytes = await val.asset.getByteData();
      List<int> imageData = imgBytes.buffer.asUint8List();
      var _picData = MultipartFile.fromBytes(
        imageData,
        contentType: MediaType('image', 'png'),
        filename: val.asset.name,
      );
      pics.add(_picData);
    });
    // print(pics.length);
    return pics;
  }

  String getfiletype(String base) {
    final String fileName = path.basename(base);
    final delim = fileName.lastIndexOf('.');
    String ext = fileName.substring(delim >= 0 ? delim : 0);
    return ext.replaceFirst('.', '');
  }

  Future<File> writeToFile(ByteData data, String tempfile) async {
    final buffer = data.buffer;
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath =
        tempPath + '/$tempfile'; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future<Response> postRegistration(EmitenDetail emitenDetail) async {
    try {
      var pictures = [];
      for (var i = 0; i < emitenDetail.assets.length; i++) {
        ByteData imgBytes = await emitenDetail.assets[i].asset.getByteData();
        List<int> imageData = imgBytes.buffer.asUint8List();
        var _picData = MultipartFile.fromBytes(
          imageData,
          contentType: MediaType("image", "jpg"),
          // contentType: MediaType(
          //     "image", getfiletype(emitenDetail.assets[i].asset.name)),
          filename: emitenDetail.assets[i].asset.name,
        );
        // var _picData = await MultipartFile.fromFile(
        //     await emitenDetail.assets[i].asset.filePath,
        //     contentType: MediaType('image', 'jpg'),
        //     filename: emitenDetail.assets[i].asset.name);
        // print(">> Picture data : ");
        // print(_picData.filename);
        // print(await emitenDetail.assets[i].asset.filePath);
        // print(_picData.contentType);
        emitenDetail.assets[i].isMain
            ? pictures.insert(0, _picData)
            : pictures.add(_picData);
      }

      // print(">> Subcategory : ");
      // print(emitenDetail.subcategory.toString());

      FormData formData = FormData.fromMap({
        "company_name": "${emitenDetail.companyName}",
        "trademark": "${emitenDetail.businessName}",
        "category": "${emitenDetail.category}",
        "subcategory": emitenDetail.subcategory == 0
            ? null
            : "${emitenDetail.subcategory}",
        "regency": "${emitenDetail.regency}",
        "address": "${emitenDetail.companyAddress}",
        "business_entity": "${emitenDetail.businessEntity}",
        "business_lifespan": "${emitenDetail.established}",
        "business_description": "${emitenDetail.description}",
        "branch_company": "${emitenDetail.branch}",
        "employee": "${emitenDetail.employees}",
        "video_url": "${emitenDetail.youtube}",
        "capital_needs": "${emitenDetail.fundingReq}",
        "monthly_turnover": "${emitenDetail.turnoverAvg}",
        "monthly_profit": "${emitenDetail.profitAvg}",
        "annual_turnover": "${emitenDetail.turnoverAvg * 12}",
        "annual_profit": "${emitenDetail.profitAvg * 12}",
        "prospektus": await MultipartFile.fromFile(
            emitenDetail.lampiran.filePath,
            contentType: MediaType('dokumen', 'pdf'),
            filename: emitenDetail.lampiran.fileName), // file
        "pictures": pictures.toList(), // files
        "monthly_turnover_previous_year": "${emitenDetail.lastYearTurnoverAvg}",
        "monthly_profit_previous_year": "${emitenDetail.lastYearProfitAvg}",
        "total_bank_debt": "${emitenDetail.debtTotal}",
        "bank_name_financing": "${emitenDetail.bankName}",
        "total_paid_capital": "${emitenDetail.paidUpCapital}",
        "price": "${emitenDetail.valuePerShare}",
        "financial_recording_system": "${emitenDetail.pencatatanKeuangan}",
        "bank_loan_reputation": "${emitenDetail.reputasiPinjaman}",
        "market_position_for_the_product": "${emitenDetail.posisiPasar}",
        "strategy_emiten": "${emitenDetail.strategiKedepan}",
        "office_status": "${emitenDetail.statusTempatUsaha}",
        "level_of_business_competition": "${emitenDetail.tingkatPersaingan}",
        "managerial_ability": "${emitenDetail.kemampuanManajerial}",
        "technical_ability": "${emitenDetail.kemampuanTeknis}",
        "tnc": "1"
      });

      // Response response = await dio.post("/info", data: formData);
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/emitens/registration",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));

      return response;
    } on DioError catch (e) {
      // print(">> DIO : ");
      // print(e.error);
      // print(e.request.uri);
      // print(e.response.data);
      // print(e.request.data.fields);
      // print(e.request.queryParameters);
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  Future<http.StreamedResponse> newPostRegistration(
      EmitenDetail emitenDetail) async {
    try {
      final token = await storage.read(key: 'token');

      //sending
      final uri = Uri.parse("$apiLocal/emitens/registration");
      final request = new http.MultipartRequest("POST", uri);
      request.headers[HttpHeaders.authorizationHeader] = 'Bearer $token';

      // image process
      // List pictures;
      for (var i = 0; i < emitenDetail.assets.length; i++) {
        // ByteData imgBytes = await emitenDetail.assets[i].asset.getByteData();
        // List<int> imageData = imgBytes.buffer.asUint8List();
        File file = File(await emitenDetail.assets[i].asset.filePath);
        var stream =
            new http.ByteStream(DelegatingStream.typed(file.openRead()));
        // get file length
        var length = await file.length(); //imageFile is your image file
        // multipart that takes file
        var multipartFileSign = new http.MultipartFile(
            'pictures', stream, length,
            filename: emitenDetail.assets[i].asset.name,
            contentType: new MediaType('image', 'png'));
        request.files.add(multipartFileSign);
      }
      var _pp = File(emitenDetail.lampiran.filePath);
      var _pros = new http.ByteStream(DelegatingStream.typed(_pp.openRead()));
      var length1 = await _pp.length();
      var prospektus = new http.MultipartFile('prospektus', _pros, length1,
          filename: emitenDetail.lampiran.fileName,
          contentType: new MediaType('dokumen', 'pdf'));
      request.files.add(prospektus);
      request.fields["company_name"] = "${emitenDetail.companyName}";
      request.fields["trademark"] = "${emitenDetail.businessName}";
      request.fields["category"] = "${emitenDetail.category}";
      request.fields["subcategory"] = "${emitenDetail.subcategory}";
      request.fields["regency"] = "${emitenDetail.regency}";
      request.fields["address"] = "${emitenDetail.companyAddress}";
      request.fields["business_entity"] = "${emitenDetail.businessEntity}";
      request.fields["business_lifespan"] = "${emitenDetail.established}";
      request.fields["business_description"] = "${emitenDetail.description}";
      request.fields["branch_company"] = "${emitenDetail.branch}";
      request.fields["employee"] = "${emitenDetail.employees}";
      request.fields["video_url"] = "${emitenDetail.youtube}";
      request.fields["capital_needs"] = "${emitenDetail.fundingReq}";
      request.fields["monthly_turnover"] = "${emitenDetail.turnoverAvg}";
      request.fields["monthly_profit"] = "${emitenDetail.profitAvg}";
      request.fields["annual_turnover"] = "${emitenDetail.turnoverAvg * 12}";
      request.fields["annual_profit"] = "${emitenDetail.profitAvg * 12}";
      request.fields["monthly_turnover_previous_year"] =
          "${emitenDetail.lastYearTurnoverAvg}";
      request.fields["monthly_profit_previous_year"] =
          "${emitenDetail.lastYearProfitAvg}";
      request.fields["total_bank_debt"] = "${emitenDetail.debtTotal}";
      request.fields["bank_name_financing"] = "${emitenDetail.bankName}";
      request.fields["total_paid_capital"] = "${emitenDetail.paidUpCapital}";
      request.fields["price"] = "${emitenDetail.valuePerShare}";
      request.fields["financial_recording_system"] =
          "${emitenDetail.pencatatanKeuangan}";
      request.fields["bank_loan_reputation"] =
          "${emitenDetail.reputasiPinjaman}";
      request.fields["market_position_for_the_product"] =
          "${emitenDetail.posisiPasar}";
      request.fields["strategy_emiten"] = "${emitenDetail.strategiKedepan}";
      request.fields["office_status"] = "${emitenDetail.statusTempatUsaha}";
      request.fields["level_of_business_competition"] =
          "${emitenDetail.tingkatPersaingan}";
      request.fields["managerial_ability"] =
          "${emitenDetail.kemampuanManajerial}";
      request.fields["technical_ability"] = "${emitenDetail.kemampuanTeknis}";
      request.fields["tnc"] = "1";

      var response = await request.send().timeout(Duration(seconds: 60));
      return response;
    } catch (e) {
      // print(">> Err : $e");
      return null;
    }
  }

  // update lampiran ( prospektus )
  Future<Response> updateLampiran(String filePath, String uuid) async {
    try {
      FormData formData = FormData.fromMap({
        "prospektus": await MultipartFile.fromFile(filePath,
            contentType: MediaType('dokumen', 'pdf'), filename: filePath)
      });
      // Response response = await dio.post("/info", data: formData);
      final headers = await UserAgent.headers();
      // print(">> TOken : $token");
      Response response = await dio
          .put(
            "$apiLocal/emitens/update-prospektus/$uuid",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));

      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // update identitas penerbit
  Future<Response> updateIdentitasPenerbit(
      EmitenDetail emitenDetail, String uuid) async {
    try {
      FormData formData = FormData.fromMap({
        "company_name": "${emitenDetail.companyName}",
        "trademark": "${emitenDetail.businessName}",
        "category": "${emitenDetail.category}",
        "subcategory": "${emitenDetail.subcategory}",
        "regency": "${emitenDetail.regency}",
        "address": "${emitenDetail.companyAddress}",
        "business_entity": "${emitenDetail.businessEntity}",
        "business_lifespan": "${emitenDetail.established}",
        "business_description": "${emitenDetail.description}",
        "branch_company": "${emitenDetail.branch}",
        "employee": "${emitenDetail.employees}"
      });

      // Response response = await dio.post("/info", data: formData);
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/emitens/update-identity/$uuid",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));

      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // update informasi finansial
  Future<Response> updateInformasiFinansial(
      EmitenDetail emitenDetail, String uuid) async {
    try {
      FormData formData = FormData.fromMap({
        "capital_needs": "${emitenDetail.fundingReq}",
        "monthly_turnover": "${emitenDetail.turnoverAvg}",
        "monthly_profit": "${emitenDetail.profitAvg}",
        "annual_turnover": "${emitenDetail.turnoverAvg * 12}",
        "annual_profit": "${emitenDetail.profitAvg * 12}",
        "monthly_turnover_previous_year": "${emitenDetail.lastYearTurnoverAvg}",
        "monthly_profit_previous_year": "${emitenDetail.lastYearProfitAvg}",
        "total_bank_debt": "${emitenDetail.debtTotal}",
        "bank_name_financing": "${emitenDetail.bankName}",
        "total_paid_capital": "${emitenDetail.paidUpCapital}",
        "price": "${emitenDetail.valuePerShare}",
      });

      // Response response = await dio.post("/info", data: formData);
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/emitens/update-financial/$uuid",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));

      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // update informasi no finansial
  Future<Response> updateInformasiNonFinansial(
      EmitenDetail emitenDetail, String uuid) async {
    try {
      FormData formData = FormData.fromMap({
        "financial_recording_system": "${emitenDetail.pencatatanKeuangan}",
        "bank_loan_reputation": "${emitenDetail.reputasiPinjaman}",
        "market_position_for_the_product": "${emitenDetail.posisiPasar}",
        "strategy_emiten": "${emitenDetail.strategiKedepan}",
        "office_status": "${emitenDetail.statusTempatUsaha}",
        "level_of_business_competition": "${emitenDetail.tingkatPersaingan}",
        "managerial_ability": "${emitenDetail.kemampuanManajerial}",
        "technical_ability": "${emitenDetail.kemampuanTeknis}",
      });

      // Response response = await dio.post("/info", data: formData);
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/emitens/update-non-financial/$uuid",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));

      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // end

  // Pralisting List
  // Model data yang digunakan adalah PralistingEmiten ( live )

  Future<http.Response> fetchPralistingDetail(String uuid) async {
    try {
      // print('$apiLocal/emitens/pre-listing/$uuid');
      final headers = await UserAgent.headers();
      // print(token);
      final http.Response response = await http
          .get('$apiLocal/emitens/pre-listing/$uuid', headers: headers)
          .timeout(Duration(seconds: 20));
      return response;
    } catch (e) {
      // print(">> Error : $e");
      return null;
    }
  }

  Future<PralistingListModel> fetchEmitens(
      {String category = '',
      String sort = 'true',
      String keyword = '',
      int limit = 1,
      int offset = 10}) async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get(
              '$apiLocal/emitens/pre-listing/paginate?category=$category&sort=$sort&search=$keyword&limit=$limit&offset=$offset',
              headers: headers)
          .timeout(Duration(seconds: 30));
      if (response.statusCode == 200) {
        final res = json.decode(response.body);
        return PralistingListModel.fromJson(res);
      } else {
        // print(">> Res");
        // print(response.body);
        return null;
      }
    } catch (e) {
      // print(">> Error : $e");
      return null;
    }
  }

  Future<http.Response> fetchTopTen() async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get(
              '$apiLocal/emitens/pre-listing/paginate?category=&sort=vote&search&limit=1&offset=10',
              headers: headers)
          .timeout(Duration(seconds: 30));
      return response;
    } catch (e) {
      // print(">> Error : $e");
      return null;
    }
  }

  Future<http.Response> fetchRandTen() async {
    try {
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .get(
              '$apiLocal/emitens/pre-listing/paginate?category=&sort=true&search&limit=1&offset=10',
              headers: headers)
          .timeout(Duration(seconds: 30));
      return response;
    } catch (e) {
      // print(">> Error : $e");
      return null;
    }
  }

  // jika user belom kyc
  Future<http.Response> fetchPrelistingNonKyc() async {
    try {
      final response = await http
          .get(
            '$apiLocal/emitens/coming-soon',
            // 'https://fire.santarax.com:3701/v3.7.1/emitens/coming-soon'
            // headers: headers,
          )
          .timeout(Duration(seconds: 15));

      // print(">> Response Status : ${response.statusCode}");
      // print(response.body.toString());
      return response;
    } catch (err) {
      // print(err);
      return null;
    }
  }

  // jika user sudah kyc
  Future<List<DataPralisting>> fetchPralistingKyc() async {
    final headers = await UserAgent.headers();
    final response = await http.get(
        '$apiLocal/emitens/pre-listing/paginate?category=&sort=&search&limit=1&offset=3',
        headers: headers);

    print(response.body);
    if (response.statusCode == 200) {
      var pralisting = pralistingPaginationFromJson(response.body);
      return pralisting.data;
    } else if (response.statusCode == 404) {
      return [];
    } else {
      return null;
    }
  }
}

class ResponseResult {
  int status;
  String message;
  ResponseResult({this.status, this.message});
}
