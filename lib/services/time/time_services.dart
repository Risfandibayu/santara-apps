import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/services/api_service.dart';
import 'package:santaraapp/utils/api.dart';

class TimeService {
  final storage = new FlutterSecureStorage();
  final apiService = ApiService();

  Future<DateTime> getOtpExpiredTime(bool isRegister) async {
    try {
      final url = isRegister ? 'otp/otp-expired' : 'otp/otp-expired?type=reset';
      final token = await storage.read(key: "token");
      final http.Response response = await http.get('$apiLocal/$url',
          headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        String dataTime = data["data"];
        return DateTime.parse(dataTime);
      } else {
        return DateTime.now();
      }
    } catch (e) {
      return DateTime.now();
    }
  }

  Future<int> otpExpiredTimeInSecond(bool isRegister) async {
    final now = await apiService.getCurrentTime();
    final expiredAt = await getOtpExpiredTime(isRegister);
    // print(">> Current Time Local : ${DateTime.now()}");
    // print(">> Current Time API : $now");
    // print(">> Expired Time API : $expiredAt");
    final secs = expiredAt.difference(now).inSeconds;
    return secs;
  }
}
