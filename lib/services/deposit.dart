import 'dart:async';
import 'dart:io';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http_parser/http_parser.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;

//utils
import 'package:santaraapp/utils/api.dart';

Future confirmation(BuildContext context, File photo, String idBank, String uuid, String token) async {
  try {
    // SharedPreferences storage = await SharedPreferences.getInstance();
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  var image = new http.ByteStream(DelegatingStream.typed(photo.openRead()));
  var imageLength = await photo.length();
  
  //sending
  final uri = Uri.parse('$apiLocal/deposit/confirmation');
  final request = new http.MultipartRequest("PUT", uri);
  request.headers[HttpHeaders.authorizationHeader] = 'Bearer $token';

  //convert
  var gambar = new http.MultipartFile('confirmation_photo', image, imageLength,
    filename: path.basename(photo.path),
    contentType: new MediaType('image', 'png')
  );

  //adding items
  request.files.add(gambar);
  request.fields['bank_id'] = idBank;
  request.fields['uuid'] = uuid;  
  var response = await request.send();//.timeout(Duration(seconds: 20));
  if(response.statusCode == 200){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Text('Konfirmasi pembayaran berhasil dilakukan'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).popUntil(ModalRoute.withName('/deposit'));
              },
            )
          ],
        );
      }
    );
  }else if(response.statusCode == 500) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Text('Server bermasalah, silahkan coba beberapa saat lagi'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }else if(response.statusCode == 504) {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Text('Koneksi bermasalah, silahkan coba beberapa saat lagi'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  } else {
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Text('Terjadi kesalahan, silahkan coba beberapa saat lagi'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }
  // } on TimeoutException catch (_) {
  //       showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             content: Text('Proses mengalami kendala, harap coba kembali'),
  //             actions: <Widget>[
  //               FlatButton(
  //                 child: Text('Ok'),
  //                 onPressed: () {
  //                   Navigator.of(context).pop();
  //                 },
  //               )
  //             ],
  //           );
  //         });
  } on SocketException catch (_) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Proses mengalami kendala, harap coba kembali'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
  }
}