import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/ChatHistory.dart';
import 'package:santaraapp/models/HelpTicket.dart';
import 'package:santaraapp/utils/api.dart';

Future<List<HelpTicket>> getUserQuestion(BuildContext context) async {
  try {
    final headers = await UserAgent.headers();
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final response = await http.get('$apiLocal/ticket/',
        headers: headers); //.timeout(Duration(seconds: 20));
    List<HelpTicket> list = helpTicketFromJson(response.body);
    return list;
  } on SocketException catch (_) {
    Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: Text('Proses mengalami kendala, harap coba kembali')));
    return null;
  }
}

Future sendQuestion(
    BuildContext context, String judul, String prioritas, String pesan) async {
  try {
    final headers = await UserAgent.headers();
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final response =
        await http.post('$apiLocal/ticket/', headers: headers, body: {
      "title": judul.toString(),
      "priority": prioritas.toString(),
      "comment": pesan.toString(),
    });
    Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Loading'),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.blue));
    if (response.statusCode == 200) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Pertanyaan Anda berhasil dikirim'),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.blue,
      ));
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Ada kesalahan'),
          duration: Duration(seconds: 1),
          backgroundColor: Colors.red));
    }
  } on SocketException catch (_) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('Proses mengalami kendala, harap coba kembali'),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.red,
    ));
  }
}

Future<List<ChatHistory>> getConvertation(
    BuildContext context, String id) async {
  try {
    final headers = await UserAgent.headers();
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final response = await http.get('$apiLocal/ticket/$id', headers: headers);
    if (response.statusCode == 200) {
      List<ChatHistory> list = chatHistoryFromJson(response.body);
      return list;
    } else {
      return null;
    }
  } on SocketException catch (_) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('Proses mengalami kendala, harap coba kembali'),
      backgroundColor: Colors.red,
    ));
    return null;
  }
}

Future sendChat(BuildContext context, String pesan, String uuid) async {
  try {
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final headers = await UserAgent.headers();
    await http.post('$apiLocal/ticket/reply/$uuid', headers: headers, body: {
      "comment": pesan,
    });
    getConvertation(context, uuid);
  } on SocketException catch (err) {
    throw err;
  } catch (err) {
    throw err;
  }
}

Future setStatusChat(BuildContext context, String uuid) async {
  try {
    // final storage = new FlutterSecureStorage();
    // final token = await storage.read(key: 'token');
    final headers = await UserAgent.headers();
    final response =
        await http.put('$apiLocal/ticket/set-status/$uuid', headers: headers);
    if (response.statusCode == 200) {
      Navigator.of(context).pop();
    }
  } on SocketException catch (_) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('Proses mengalami kendala, harap coba kembali'),
      backgroundColor: Colors.red,
    ));
  }
}
