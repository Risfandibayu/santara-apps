import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:intl/intl.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/BidangUsaha.dart';
import 'package:santaraapp/models/Perdana.dart';
import 'package:santaraapp/utils/api.dart';
import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/logger.dart';

/// untuk semua reusable service, lebih baik jika ditaruh disini
///
class ApiService {
  final storage = secureStorage.FlutterSecureStorage();

  // Get user by uuid
  Future<Response> getUserByUuid() async {
    try {
      var token = await storage.read(key: 'token'); // get user token
      var uuid = await storage.read(key: 'uuid'); // get user id
      Response response = await Dio()
          .get(
            "$apiLocal/users/by-uuid/$uuid",
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e, stack) {
      santaraLog(e, stack);
      return null;
    }
  }

  Future<Response> updateFirebaseToken(String token) async {
    try {
      var bearer = await storage.read(key: 'token'); // get user token
      if (bearer != null) {
        final headers = await UserAgent.headers();
        if (token != null) {
          Response response = await Dio()
              .put(
                "$apiLocal/notification/send-device-token",
                data: {
                  "token": "$token",
                  "type": Platform.isAndroid ? 'A' : 'I'
                },
                options: Options(headers: headers),
              )
              .timeout(Duration(seconds: 30));
          return response;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } on DioError catch (resp) {
      return resp.response;
    } catch (e, stack) {
      santaraLog(e, stack);
      return null;
    }
  }

  // Get balance
  Future<double> getAllBalance() async {
    final headers = await UserAgent.headers();
    double rupiah = await getSaldoRupiah();
    double saldo = 0;
    List<Perdana> perdanaList;

    try {
      // getting tokens
      Response response = await Dio()
          .get(
            "$apiLocal/tokens/",
            options: Options(headers: headers),
          )
          .timeout(Duration(seconds: 60));

      if (response.statusCode == 200) {
        perdanaList = perdanaFromJson(json.encode(response.data));
        perdanaList.forEach((data) => {saldo += data.amount / 1});
      } else {
        saldo = 0;
      }
    } on SocketException catch (_) {
      saldo = 0;
    } on DioError catch (e, stack) {
      // santaraLog(e, stack);
      saldo = 0;
    } catch (e) {
      saldo = 0;
    }

    return saldo + rupiah;
  }

  // Get saldo idr
  Future<double> getSaldoRupiah() async {
    try {
      // getting tokens
      final headers = await UserAgent.headers();
      Response response = await Dio()
          .get(
            "$apiLocal/traders/idr",
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      if (response.statusCode == 200) {
        final data = response.data;
        if (data != null) {
          return double.parse("${data['idr'] / 1}");
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    } on SocketException catch (_) {
      return 0;
    } catch (e) {
      return 0;
    }
  }

  // Get kategori bisnis
  Future<List<BidangUsaha>> getBidangUsaha() async {
    try {
      final headers = await UserAgent.headers();
      Response response = await Dio()
          .get(
            "$apiLocal/categories/",
            options: Options(headers: headers),
          )
          .timeout(Duration(seconds: 60));
      List<BidangUsaha> datas = [];
      if (response.statusCode == 200) {
        // final res = json.decode(response.data);
        response.data.forEach((val) {
          var data = BidangUsaha.fromJson(val);
          datas.add(data);
        });
        return datas;
      } else {
        throw Exception("Connection Failed!");
      }
    } catch (e, stack) {
      santaraLog(e, stack);
      return [];
    }
  }

  // Get current time
  Future<DateTime> getCurrentTime() async {
    try {
      DateTime now;
      final http.Response response = await http.get('$apiLocal/traders/ctime');
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        String dataTime = data["time"];
        now = DateTime.parse(dataTime);
      } else {
        now = DateTime.now();
      }
      return now;
    } catch (e, stack) {
      santaraLog(e, stack);
      return DateTime.now();
    }
  }
}
