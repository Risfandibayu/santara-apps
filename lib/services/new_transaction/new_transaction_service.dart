import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/core/utils/platforms/user_agent.dart';
import 'package:santaraapp/features/payment/data/models/submit_transaction_model.dart';
import 'package:santaraapp/models/BuyToken.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:crypto/crypto.dart';

class NewTransactionService {
  final storage = new secureStorage.FlutterSecureStorage();
  Map request = new Map<String, dynamic>();
  Map header = new Map<String, String>();

  Future<Response> refreshToken() async {
    try {
      var token = await storage.read(key: 'token');

      var bytes = utf8.encode(salt);
      var digest = sha256.convert(bytes);
      var key = utf8.encode('$digest$token');
      var tokenRefresh = sha256.convert(key);
      print(utf8.decode(key));

      final response = await http.post('$apiLocal/refreshtoken',
          headers: {'Authorization': "Bearer $token", 'token': '$tokenRefresh'},
          body: {'token': token});

      print("responnya adalah" + response.headers.toString());
      print("responnya adalah" + response.body.toString());

      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> buyTokenService(BuyToken model) async {
    try {
      var token = await storage.read(key: 'token');

      int amount = int.parse(model.amount);
      assert(amount is int);

      request["emiten_uuid"] = model.emitenUuid;
      request["amount"] = amount;
      request["channel"] = model.channel;
      request["pin"] = model.pin;
      request["finger"] = '${model.finger}';

      final removeSpace = jsonEncode(request).replaceAll(" ", '');
      print(removeSpace);

      var key = utf8.encode(token);
      var bytes = utf8.encode(removeSpace);
      var digest = sha256.convert(bytes);

      var hmacSha256 = new Hmac(sha256, key);
      Digest sha256Result = hmacSha256.convert(utf8.encode(digest.toString()));

      header['Authorization'] = "Bearer $token";
      header['accesskeytoken'] = token;
      header['authkey'] = '$sha256Result';

      print('sha$sha256Result');
      final response = await http.post('$apiLocal/transactions/buy-token',
          body: {
            "emiten_uuid": model.emitenUuid,
            "amount": model.amount,
            "channel": model.channel,
            "pin": model.pin,
            "finger": '${model.finger}'
          },
          headers: header);

      return Future.value(response);
    } catch (err) {
      print('error$err');
    }
  }
}
