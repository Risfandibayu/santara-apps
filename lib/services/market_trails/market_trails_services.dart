import 'package:dio/dio.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:meta/meta.dart';

class MarketTrailsServices {
  final Dio dio = new Dio();

  Future<Response> createTrails(
      {@required String event, @required String note}) async {
    try {
      final response = await dio.post(
          '$apiMarket/cmn/$versionApiMarket/trails/',
          data: {"event": event, "note": note});
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}

Map<String, Map<String, String>> marketTrailsData = {
  'home_view': {
    'event': 'HOME_VIEW',
    'note': 'Someone trying to open secondary market home'
  },
  'home_view_success': {
    'event': 'HOME_VIEW_SUCCESS',
    'note': 'The home page successfully loaded'
  },
  'home_view_failed': {
    'event': 'HOME_VIEW_FAILED',
    'note': 'The home page fails to load'
  },
  'stock_list_view': {
    'event': 'STOCK_LIST_VIEW',
    'note': 'Someone trying to open secondary market stock list'
  },
  'stock_list_view_success': {
    'event': 'STOCK_LIST_VIEW_SUCCESS',
    'note': 'The stock list page successfully loaded'
  },
  'stock_list_view_failed': {
    'event': 'STOCK_LIST_VIEW_FAILED',
    'note': 'The stock list page fails to load'
  },
  'portofolio_view': {
    'event': 'PORTOFOLIO_VIEW',
    'note': 'Someone trying to open secondary market portofolio'
  },
  'portofolio_view_success': {
    'event': 'PORTOFOLIO_VIEW_SUCCESS',
    'note': 'The portfolio page successfully loaded'
  },
  'portofolio_view_failed': {
    'event': 'PORTOFOLIO_VIEW_FAILED',
    'note': 'The portfolio page fails to load'
  },
  'how_to_buy_view': {
    'event': 'HOW_TO_BUY_VIEW',
    'note': 'Someone trying to open how to buy tutorial'
  },
  'how_to_buy_view_success': {
    'event': 'HOW_TO_BUY_VIEW_SUCCESS',
    'note': 'Tutorial page about how to buy successfully loaded'
  },
  'how_to_buy_view_failed': {
    'event': 'HOW_TO_BUY_VIEW_FAILED',
    'note': 'Tutorial page about how to buy fails to load'
  },
  'how_to_sell_view': {
    'event': 'HOW_TO_SELL_VIEW',
    'note': 'Someone trying to open how to sell tutorial'
  },
  'how_to_sell_view_success': {
    'event': 'HOW_TO_SELL_VIEW_SUCCESS',
    'note': 'Tutorial page about how to sell successfully loaded'
  },
  'how_to_sell_view_failed': {
    'event': 'HOW_TO_SELL_VIEW_FAILED',
    'note': 'Tutorial page about how to sell fails to load'
  },
  'how_it_works_view': {
    'event': 'HOW_IT_WORKS_VIEW',
    'note': 'Someone trying to open how it works tutorial'
  },
  'how_it_works_view_success': {
    'event': 'HOW_IT_WORKS_VIEW_SUCCESS',
    'note': 'Tutorial page about how it works successfully loaded'
  },
  'how_it_works_view_failed': {
    'event': 'HOW_IT_WORKS_VIEW_FAILED',
    'note': 'Tutorial page about how it works fails to load'
  },
  'glossary_view': {
    'event': 'GLOSSARY_VIEW',
    'note': 'Someone trying to open glossary'
  },
  'glossary_view_success': {
    'event': 'GLOSSARY_VIEW_SUCCESS',
    'note': 'Glossary page successfully loaded'
  },
  'glossary_view_failed': {
    'event': 'GLOSSARY_VIEW_FAILED',
    'note': 'Glossary page fails to load'
  },
  'stock_detail_view': {
    'event': 'STOCK_DETAIL_VIEW',
    'note': 'Someone trying to open stock detail'
  },
  'stock_detail_view_success': {
    'event': 'STOCK_DETAIL_VIEW_SUCCESS',
    'note': 'The stock detail successfully loaded'
  },
  'stock_detail_view_failed': {
    'event': 'STOCK_DETAIL_VIEW_FAILED',
    'note': 'The stock detail fails to load'
  },
  'financial_detail_view': {
    'event': 'FINANCIAL_DETAIL_VIEW',
    'note': 'Someone trying to open financial detail'
  },
  'financial_detail_view_success': {
    'event': 'FINANCIAL_DETAIL_VIEW_SUCCESS',
    'note': 'The financial detail successfully loaded'
  },
  'financial_detail_view_failed': {
    'event': 'FINANCIAL_DETAIL_VIEW_FAILED',
    'note': 'The financial detail fails to load'
  },
  'fundamental_detail_view': {
    'event': 'FUNDAMENTAL_DETAIL_VIEW',
    'note': 'Someone trying to open fundamental detail'
  },
  'fundamental_detail_view_success': {
    'event': 'FUNDAMENTAL_DETAIL_VIEW_SUCCESS',
    'note': 'The fundamental detail successfully loaded'
  },
  'fundamental_detail_view_failed': {
    'event': 'FUNDAMENTAL_DETAIL_VIEW_FAILED',
    'note': 'The fundamental detail fails to load'
  },
  'candlestick_view': {
    'event': 'CANDLESTICK_VIEW',
    'note': 'Someone trying to open candlestick chart'
  },
  'candlestick_view_success': {
    'event': 'CANDLESTICK_VIEW_SUCCESS',
    'note': 'The candlestick chart successfully loaded'
  },
  'candlestick_view_failed': {
    'event': 'CANDLESTICK_VIEW_FAILED',
    'note': 'The candlestick chart fails to load'
  },
  'ohlc_bar_view': {
    'event': 'OHLC_BAR_VIEW',
    'note': 'Someone trying to open OHLC bar'
  },
  'ohlc_bar_view_success': {
    'event': 'OHLC_BAR_VIEW_SUCCESS',
    'note': 'The OHLC bar successfully loaded'
  },
  'ohlc_bar_view_failed': {
    'event': 'OHLC_BAR_VIEW_FAILED',
    'note': 'The OHLC bar fails to load'
  },
  'line_chart_view': {
    'event': 'LINE_CHART_VIEW',
    'note': 'Someone trying to open line chart'
  },
  'line_chart_view_success': {
    'event': 'LINE_CHART_VIEW_SUCCESS',
    'note': 'The line chart successfully loaded'
  },
  'line_chart_view_failed': {
    'event': 'LINE_CHART_VIEW_FAILED',
    'note': 'The line chart fails to load'
  },
  'wishlist_add': {
    'event': 'WISHLIST_ADD',
    'note': 'Someone trying to insert the stock from the wishlist'
  },
  'wishlist_add_success': {
    'event': 'WISHLIST_ADD_SUCCESS',
    'note': 'Someone successfully inserts the stock from the wishlist'
  },
  'wishlist_add_failed': {
    'event': 'WISHLIST_ADD_FAILED',
    'note': 'Someone failed to insert the stock from the wishlist'
  },
  'wishlist_view': {
    'event': 'WISHLIST_VIEW',
    'note': 'Someone trying to open wishlist'
  },
  'wishlist_view_success': {
    'event': 'WISHLIST_VIEW_SUCCESS',
    'note': 'The wishlist successfully loaded'
  },
  'wishlist_view_failed': {
    'event': 'WISHLIST_VIEW_FAILED',
    'note': 'The wishlist fails to load'
  },
  'wishlist_delete': {
    'event': 'WISHLIST_DELETE',
    'note': 'Someone trying to delete the stock from the wishlist'
  },
  'wishlist_delete_success': {
    'event': 'WISHLIST_DELETE_SUCCESS',
    'note': 'Someone successfully deletes the stock from the wishlist'
  },
  'wishlist_delete_failed': {
    'event': 'WISHLIST_DELETE_FAILED',
    'note': 'Someone failed to delete the stock from the wishlist'
  },
  'buy_stocks_form_view': {
    'event': 'BUY_STOCKS_FORM_VIEW',
    'note': 'Someone trying to open the purchasing form'
  },
  'buy_stocks_form_view_success': {
    'event': 'BUY_STOCKS_FORM_VIEW_SUCCESS',
    'note': 'The purchasing form successfully open'
  },
  'buy_stocks_form_view_failed': {
    'event': 'BUY_STOCKS_FORM_VIEW_FAILED',
    'note': 'The purchasing form fails to open'
  },
  'purchase_confirmation_view': {
    'event': 'PURCHASE_CONFIRMATION_VIEW',
    'note': 'Someone trying to open purchase confirmation'
  },
  'purchase_confirmation_view_success': {
    'event': 'PURCHASE_CONFIRMATION_VIEW_SUCCESS',
    'note': 'The purchase confirmation open successfully'
  },
  'purchase_confirmation_view_failed': {
    'event': 'PURCHASE_CONFIRMATION_VIEW_FAILED',
    'note': 'The purchase confirmation fails to open'
  },
  'buy_transaction_insert': {
    'event': 'BUY_TRANSACTION_INSERT',
    'note': 'Someone trying to insert buy transaction'
  },
  'buy_transaction_insert_success': {
    'event': 'BUY_TRANSACTION_INSERT_SUCCESS',
    'note': 'Someone successfully inserts buy transaction'
  },
  'buy_transaction_insert_failed': {
    'event': 'BUY_TRANSACTION_INSERT_FAILED',
    'note': 'Someone failed to insert buy transaction'
  },
  'orderbook_view': {
    'event': 'ORDERBOOK_VIEW',
    'note': 'Someone trying to open orderbook list'
  },
  'orderbook_view_success': {
    'event': 'ORDERBOOK_VIEW_SUCCESS',
    'note': 'The orderbook page successfully loaded'
  },
  'orderbook_view_failed': {
    'event': 'ORDERBOOK_VIEW_FAILED',
    'note': 'The orderbook page fails to load'
  },
  'orderbook_search_view': {
    'event': 'ORDERBOOK_SEARCH_VIEW',
    'note': 'Someone trying open orderbook by using search'
  },
  'orderbook_search_view_success': {
    'event': 'ORDERBOOK_SEARCH_VIEW_SUCCESS',
    'note': 'The orderbook display data of the stock searched'
  },
  'orderbook_search_view_failed': {
    'event': 'ORDERBOOK_SEARCH_VIEW_FAILED',
    'note': 'The orderbook fails to display data of the stock searched'
  },
  'top_gainers_view': {
    'event': 'TOP_GAINERS_VIEW',
    'note': 'Someone try to view orderbook by top gainer filter'
  },
  'top_gainers_view_success': {
    'event': 'TOP_GAINERS_VIEW_SUCCESS',
    'note': 'The orderbook successfully filter stock by the top gainer'
  },
  'top_gainers_view_failed': {
    'event': 'TOP_GAINERS_VIEW_FAILED',
    'note': 'The orderbook fails to filter stock by the top gainer'
  },
  'top_losers_view': {
    'event': 'TOP_LOSERS_VIEW',
    'note': 'Someone try to view orderbook by top loser filter'
  },
  'top_losers_view_success': {
    'event': 'TOP_LOSERS_VIEW_SUCCESS',
    'note': 'The orderbook successfully filter stock by the top loser'
  },
  'top_losers_view_failed': {
    'event': 'TOP_LOSERS_VIEW_FAILED',
    'note': 'The orderbook fails to filter stock by the top loser'
  },
  'top_pick_view': {
    'event': 'TOP_PICK_VIEW',
    'note': 'Someone try to view orderbook by top pick filter'
  },
  'top_pick_view_success': {
    'event': 'TOP_PICK_VIEW_SUCCESS',
    'note': 'The orderbook successfully filter stock by the top pick'
  },
  'top_pick_view_failed': {
    'event': 'TOP_PICK_VIEW_FAILED',
    'note': 'The orderbook fails to filter stock by the top pick'
  },
  'price_sorting_asc_view': {
    'event': 'PRICE_SORTING_ASC_VIEW',
    'note': 'Someone trying to view orderbook with sorting price asc  '
  },
  'price_sorting_asc_view_success': {
    'event': 'PRICE_SORTING_ASC_VIEW_SUCCESS',
    'note':
        'The orderbook successfully sort by price from the cheapest to the most expensive'
  },
  'price_sorting_asc_view_failed': {
    'event': 'PRICE_SORTING_ASC_VIEW_FAILED',
    'note':
        'The orderbook fails to sort by price from the cheapest to the most expensive'
  },
  'price_sorting_dsc_view': {
    'event': 'PRICE_SORTING_DSC_VIEW',
    'note': 'Someone trying to view orderbook with sorting price dsc'
  },
  'price_sorting_dsc_view_success': {
    'event': 'PRICE_SORTING_DSC_VIEW_SUCCESS',
    'note':
        'The orderbook successfully sort by price from the most expensive to the cheapest'
  },
  'price_sorting_dsc_view_failed': {
    'event': 'PRICE_SORTING_DSC_VIEW_FAILED',
    'note':
        'The orderbook fails to sort by price from the most expensive to the cheapest'
  },
  'compare_view': {
    'event': 'COMPARE_VIEW',
    'note': 'Someone tying to comparing some stocks'
  },
  'compare_view_success': {
    'event': 'COMPARE_VIEW_SUCCESS',
    'note': 'The data of the chosen stock successfully loaded'
  },
  'compare_view_failed': {
    'event': 'COMPARE_VIEW_FAILED',
    'note': 'The data of the chosen stock fails to load'
  },
  'sell_stock_form': {
    'event': 'SELL_STOCK_FORM',
    'note': 'Someone open sell stock form'
  },
  'sales_confirmation_view': {
    'event': 'SALES_CONFIRMATION_VIEW',
    'note': 'Someone trying to open sales confirmation'
  },
  'sales_confirmation_view_success': {
    'event': 'SALES_CONFIRMATION_VIEW_SUCCESS',
    'note': 'The sales confirmation page successfully loaded'
  },
  'sales_confirmation_view_failed': {
    'event': 'SALES_CONFIRMATION_VIEW_FAILED',
    'note': 'The sales confirmation page fails to load'
  },
  'sell_transaction_insert': {
    'event': 'SELL_TRANSACTION_INSERT',
    'note': 'Someone trying to insert sell transaction'
  },
  'sell_transaction_insert_success': {
    'event': 'SELL_TRANSACTION_INSERT_SUCCESS',
    'note': 'Someone successfully inserts sell transaction'
  },
  'sell_transaction_insert_failed': {
    'event': 'SELL_TRANSACTION_INSERT_FAILED',
    'note': 'Someone failed to insert sell transaction'
  },
  'order_list_view': {
    'event': 'ORDER_LIST_VIEW',
    'note': 'Someone trying to open the order list'
  },
  'order_list_view_success': {
    'event': 'ORDER_LIST_VIEW_SUCCESS',
    'note': 'The order list page successfully loaded'
  },
  'order_list_view_failed': {
    'event': 'ORDER_LIST_VIEW_FAILED',
    'note': 'The order list page fails to load'
  },
  'buy_transaction_update': {
    'event': 'BUY_TRANSACTION_UPDATE',
    'note': 'Someone trying to update buy transaction'
  },
  'buy_transaction_update_success': {
    'event': 'BUY_TRANSACTION_UPDATE_SUCCESS',
    'note': 'Someone successfully updates buy transaction'
  },
  'buy_transaction_update_failed': {
    'event': 'BUY_TRANSACTION_UPDATE_FAILED',
    'note': 'Someone failed to update buy transaction'
  },
  'buy_transaction_delete': {
    'event': 'BUY_TRANSACTION_DELETE',
    'note': 'Someone trying to delete buy transaction '
  },
  'buy_transaction_delete_success': {
    'event': 'BUY_TRANSACTION_DELETE_SUCCESS',
    'note': 'Someone successfully deletes buy transaction'
  },
  'buy_transaction_delete_failed': {
    'event': 'BUY_TRANSACTION_DELETE_FAILED',
    'note': 'Someone failed to delete buy transaction '
  },
  'sell_transaction_update': {
    'event': 'SELL_TRANSACTION_UPDATE',
    'note': 'Someone trying to update sell transaction'
  },
  'sell_transaction_update_success': {
    'event': 'SELL_TRANSACTION_UPDATE_SUCCESS',
    'note': 'Someone successfully updates sell transaction'
  },
  'sell_transaction_update_failed': {
    'event': 'SELL_TRANSACTION_UPDATE_FAILED',
    'note': 'Someone failed to update sell transaction'
  },
  'sell_transaction_delete': {
    'event': 'SELL_TRANSACTION_DELETE',
    'note': 'Someone trying to delete sell transaction'
  },
  'sell_transaction_delete_success': {
    'event': 'SELL_TRANSACTION_DELETE_SUCCESS',
    'note': 'Someone successfully deletes sell transaction'
  },
  'sell_transaction_delete_failed': {
    'event': 'SELL_TRANSACTION_DELETE_FAILED',
    'note': 'Someone failed to delete sell transaction'
  },
  'transaction_history_view': {
    'event': 'TRANSACTION_HISTORY_VIEW',
    'note': 'Someone trying to open transaction history'
  },
  'transaction_history_view_success': {
    'event': 'TRANSACTION_HISTORY_VIEW_SUCCESS',
    'note': 'The transaction history page successfully loaded'
  },
  'transaction_history_view_failed': {
    'event': 'TRANSACTION_HISTORY_VIEW_FAILED',
    'note': 'The transaction history page fails to load'
  },
};
