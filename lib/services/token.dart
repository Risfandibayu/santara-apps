import 'dart:io';
import 'dart:async';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:santaraapp/helpers/PopupHelper.dart';
import 'package:santaraapp/helpers/ToastHelper.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Emiten.dart';
import 'package:santaraapp/models/TokenHistory.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';

Future<List<Emiten>> getEmiten() async {
  final http.Response response = await http.get('$apiLocal/emitens/');
  if (response.statusCode == 200) {
    List<Emiten> data = emitenFromJson(response.body);
    return data;
  } else {
    // print('data tidak ada');
    return null;
  }
}

Future getTotalEmiten() async {
  final http.Response response = await http.get('$apiLocal/emitens/all');
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  }
}

Future getStatus() async {
  final http.Response response = await http.get('$apiLocal/information');
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  }
}

Future<List<TokenHistory>> getTransactionsHistory() async {
  final headers = await UserAgent.headers();
  List list;
  final http.Response response =
      await http.get('$apiLocal/transactions/', headers: headers);
  if (response.statusCode == 200) {
    list = tokenHistoryFromJson(response.body);
  }
  return list;
}

Future<int> getStatusToken() async {
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  if (token == null) {
    return 0;
  } else {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/transactions/', headers: headers);
    return response.statusCode;
  }
}

Future buyToken(
    BuildContext context, String total, int id, String token) async {
  try {
    final headers = await UserAgent.headers();
    final http.Response response = await http
        .post('$apiLocal/transactions/buy-token', headers: headers, body: {
      "emiten_id": id.toString(),
      "amount": total,
    }); //.timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text('Pembelian Token Telah Berhasil'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pushNamed('/token');
                  },
                ),
              ],
            );
          });
    } else {
      final msg = jsonDecode(response.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: Text(msg['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    }
  } on SocketException catch (_) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text('Proses mengalami kendala, harap coba kembali'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}

Future cancelPayment(BuildContext context, int id, String token) async {
  PopupHelper.showLoading(context);
  try {
    final headers = await UserAgent.headers();
    final http.Response response = await http
        .delete('$apiLocal/transactions/cancel-payment?trx_id=$id',
            headers: headers)
        .timeout(Duration(seconds: 20));
    Navigator.pop(context);
    if (response.statusCode == 200) {
      ToastHelper.showBasicToast(context, "Transaksi telah dibatalkan!");
    } else {
      final msg = jsonDecode(response.body);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(msg['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  } catch (e, stack) {
    santaraLog(e, stack);
    // Navigator.pop(context);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text('Proses mengalami kendala, harap coba kembali'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}
