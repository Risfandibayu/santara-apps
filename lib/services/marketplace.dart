import 'dart:async';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/models/Secondary.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/utils/api.dart';

Future<List<Secondary>> getListSecondary() async {
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  try {
    final http.Response response = await http.get('$apiLocal/market/',
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $token"
        });//.timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      List<Secondary> data = secondaryFromJson(response.body);
      return data;
    } else {
      // print('data tidak ada');
      return null;
    }
    // } on TimeoutException catch (e) {
    //   print('timeout');
  } on SocketException catch (_) {
    // print('socket exception');
    return null;
  }
}

// Future<List<Markets>> getPembelianMarket(int id) async {
//   final storage = new FlutterSecureStorage();
//   final token = await storage.read(key: 'token');
//   final http.Response response =
//       await http.post('$apiLocal/market/get-markets-emiten/',
//         headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
//         body: {"emiten_id": id.toString()});
//   if (response.statusCode == 200) {
//     List<Markets> data = marketsFromJson(response.body);
//     return data;
//   } else {
//     print('data tidak ada');
//   }
// }

// Future<List<Markets>> getTransaksiPribadi(int id) async {
//   final storage = new FlutterSecureStorage();
//   final token = await storage.read(key: 'token');
//   final http.Response response =
//       await http.post('$apiLocal/market/activity-secondary-market-emiten/',
//         headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
//         body: {"emiten_id": id.toString()});
//   if (response.statusCode == 200) {
//     List<Markets> data = marketsFromJson(response.body);
//     return data;
//   } else {
//     print('data tidak ada');
//   }
// }

// Future<List<Markets>> getTransaksiGlobal() async {
//   final storage = new FlutterSecureStorage();
//   final token = await storage.read(key: 'token');
//   final http.Response response =
//       await http.get('$apiLocal/market/activity-secondary-market/',
//         headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
//   if (response.statusCode == 200) {
//     List<Markets> data = marketsFromJson(response.body);
//     return data;
//   } else {
//     print('data tidak ada');
//   }
// }
