import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/Dividen.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/models/Perdana.dart';
import 'package:santaraapp/utils/logger.dart';

Future<List<Perdana>> getAllToken() async {
  List<Perdana> perdanaList;
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  try {
    final response = await http.get('$apiLocal/tokens/',
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
    if (response.statusCode == 200) {
      perdanaList = perdanaFromJson(response.body);
    }
  } on SocketException catch (_) {
    debugPrint("nothing");
  }
  return perdanaList;
}

Future<int> getInfoToken(String emitenCode) async {
  List<Perdana> perdanaList;
  int saham;
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  try {
    final response = await http.get('$apiLocal/tokens/',
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
    if (response.statusCode == 200) {
      perdanaList = perdanaFromJson(response.body);
      perdanaList.forEach((data) {
        if (data.code == emitenCode) {
          saham = data.token;
        }
      });
    }
  } on SocketException catch (_) {
    debugPrint("nothing");
  }
  return saham;
}

Future<double> getAllBalance() async {
  double saldo = 0;
  double idr = 0;
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  List<Perdana> perdanaList;
  try {
    final response = await http.get('$apiLocal/tokens/',
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"});
    if (response.statusCode == 200) {
      perdanaList = perdanaFromJson(response.body);
      perdanaList.forEach((data) => {saldo += data.amount / 1});
    }
    final headers = await UserAgent.headers();
    final http.Response dataIdr =
        await http.get('$apiLocal/traders/idr', headers: headers);
    if (dataIdr.statusCode == 200) {
      final data = jsonDecode(dataIdr.body);
      idr = data['idr'] / 1;
    }
  } on SocketException catch (_) {
    saldo = 0;
  }
  saldo = saldo + idr;
  return saldo;
}

Future<double> getSahamDimiliki(token) async {
  double saldo = 0;
  List<Perdana> perdanaList;
  try {
    final headers = await UserAgent.headers();
    final response = await http.get('$apiLocal/tokens/', headers: headers);
    if (response.statusCode == 200) {
      perdanaList = perdanaFromJson(response.body);
      perdanaList.forEach((data) => {saldo += data.amount});
    } else {
      saldo = 0;
    }
  } on SocketException catch (_) {
    saldo = 0;
  }
  return saldo / 1;
}

Future<double> getMaksimumInvest(token) async {
  double maxInvest = 0;
  try {
    final headers = await UserAgent.headers();
    final response =
        await http.get('$apiLocal/traders/max-invest', headers: headers);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      maxInvest = data["max_invest"] / 1;
    } else {
      maxInvest = 0;
    }
  } on SocketException catch (_) {
    maxInvest = 0;
  }
  return maxInvest;
}

Future<double> getIdr() async {
  double idr = 0.0;
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  try {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/traders/idr', headers: headers);
    final data = jsonDecode(response.body);
    idr = data['idr'] / 1;
  } on SocketException catch (_) {
    idr = 0.0;
  }
  return idr;
}

Future<double> getIdrDeposit() async {
  try {
    final headers = await UserAgent.headers();
    final storage = new FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    final http.Response response =
        await http.get('$apiLocal/traders/idr', headers: headers);
    final data = jsonDecode(response.body);
    final idr = data['idr'] / 1;
    return idr;
  } catch (e, stack) {
    // print(">> Url : $apiLocal/traders/idr");
    santaraLog(e, stack);
    return null;
  }
}

Future<Deviden> getDividen() async {
  final storage = new FlutterSecureStorage();
  final token = await storage.read(key: 'token');
  Deviden deviden;
  try {
    final headers = await UserAgent.headers();
    final http.Response response =
        await http.get('$apiLocal/devidend', headers: headers);
    if (response.statusCode == 200) {
      deviden = devidenFromJson(response.body);
      return deviden;
    } else {
      // debugPrint(response.statusCode.toString());
      return null;
    }
  } on SocketException catch (e) {
    // debugPrint(e.message);
    return null;
  }
}
