import 'dart:async';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/helpers/kyc/AlamatHelper.dart';
import 'package:santaraapp/helpers/kyc/BankPribadiHelper.dart';
import 'package:santaraapp/helpers/kyc/BiodataKeluargaHelper.dart';
import 'package:santaraapp/helpers/kyc/InformasiPajakHelper.dart';
import 'package:santaraapp/helpers/kyc/KycHelper.dart';
import 'package:santaraapp/helpers/kyc/PekerjaanHelper.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/models/kyc/KycDataDokumen.dart';
import 'package:path/path.dart' as path;
import 'package:santaraapp/utils/logger.dart';

class KycPersonalService {
  final storage = new secureStorage.FlutterSecureStorage();
  final Dio dio = Dio();

  // Biodata Pribadi
  Future<Response> sendBiodataPribadi(KycDataDokumen data) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "name": "${data.name}",
        "birth_place": "${data.bop}",
        "birth_date": "${data.yearBorn}-${data.monthBorn}-${data.dateBorn}",
        "gender": "${data.gender}",
        "education_id": "${data.education}",
        "country_id": 78,
        "idcard_number": "${data.nik}",
        "regis_date_idcard":
            "${data.yearRegId}-${data.monthRegId}-${data.dayRegId}",
        "type_idcard": "${data.idCardType}",
        "expired_date_idcard": data.idCardType == 'KTP'
            ? "${data.yearExpId}-${data.monthExpId}-${data.dayExpId}"
            : "",
        "photo": data.avatar != null
            ? await MultipartFile.fromFile(data.avatar.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.avatar.path))
            : null, // foto profil
        "idcard_photo": await MultipartFile.fromFile(data.fotoKtp.path,
            contentType: MediaType("image", "jpg"),
            filename: path.basename(data.fotoKtp.path)), // foto ktp
        "verification_photo": await MultipartFile.fromFile(data.fotoSelfie.path,
            contentType: MediaType("image", "jpg"),
            filename:
                path.basename(data.fotoSelfie.path)), // foto selfie with ktp
        "passport_number": data.passport != null ? "${data.passport}" : "",
        "expired_date_passport": data.passport != null
            ? "${data.yearExpPassport}-${data.monthExpPassport}-${data.dayExpPassport}"
            : "",
        "another_email": data.altEmail != null ? data.altEmail : "",
        "alt_phone": data.altPhone != null ? data.altPhone : ""
      });

      // print(">> Form Data : ");
      // print(formData.f);

      // Response response = await dio.post("/info", data: formData);
      final token = await storage.read(key: 'token');
      // print(token);
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-1",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      // print(response.data.toString());
      return response;
    } on DioError catch (e) {
      //
      return e.response;
    } catch (e) {
      // print(">> DIOx : ");
      // print(e);
      return null;
    }
  }

  // Update Biodata Pribadi
  Future<Response> updateBiodataPribadi(KycDataDokumen data) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "name": "${data.name}",
        "birth_place": "${data.bop}",
        "birth_date": "${data.yearBorn}-${data.monthBorn}-${data.dateBorn}",
        "gender": "${data.gender}",
        "education_id": "${data.education}",
        "country_id": 78,
        "idcard_number": "${data.nik}",
        "regis_date_idcard":
            "${data.yearRegId}-${data.monthRegId}-${data.dayRegId}",
        "type_idcard": "${data.idCardType}",
        "expired_date_idcard": data.idCardType == 'KTP'
            ? "${data.yearExpId}-${data.monthExpId}-${data.monthExpId}"
            : "",
        "photo": data.avatar != null
            ? await MultipartFile.fromFile(data.avatar.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.avatar.path))
            : null, // foto profil
        "idcard_photo": data.fotoKtp != null
            ? await MultipartFile.fromFile(data.fotoKtp.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.fotoKtp.path))
            : null, // foto ktp
        "verification_photo": data.fotoSelfie != null
            ? await MultipartFile.fromFile(data.fotoSelfie.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.fotoSelfie.path))
            : null, // foto selfie with ktp
        "passport_number": data.passport != null ? "${data.passport}" : "",
        "expired_date_passport": data.passport != null
            ? "${data.yearExpPassport}-${data.monthExpPassport}-${data.dayExpPassport}"
            : "",
        "another_email": data.altEmail != null ? data.altEmail : "",
        "alt_phone": data.altPhone != null ? data.altPhone : ""
      });
      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-1",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      // print(response.data.toString());
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(">> DIOx : ");
      // print(e);
      return null;
    }
  }

  // Get biodata pribadi values
  Future<Response> getBiodataPribadiData() async {
    try {
      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-1",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // Biodata Keluarga
  Future<Response> sendBiodataKeluarga(BiodataKeluarga data) async {
    try {
      FormData formData = FormData.fromMap({
        "marital_status": "${data.maritalStatus}", // single = 1, married = 2
        "spouse_name": "${data.spouseName}",
        "mother_maiden_name": "${data.motherMaidenName}",
        "heir": "${data.heir}",
        "heir_relation": "${data.heriRelation}",
        "heir_phone": "${data.heirPhone}"
      });

      final token = await storage.read(key: 'token');
      // print(token);
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-2",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      // print("STATUS CODE : ${response.statusCode}");
      // print(response.data.toString());
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(">> DIOx : ");
      // print(e);
      return null;
    }
  }

  // Get biodata keluarga values
  Future<Response> getBiodataKeluargaData() async {
    try {
      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-2",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // Update biodata keluarga
  Future<Response> updateBiodataKeluarga(BiodataKeluarga data) async {
    try {
      FormData formData = FormData.fromMap({
        "marital_status": "${data.maritalStatus}", // single = 1, married = 2
        "spouse_name": "${data.spouseName}",
        "mother_maiden_name": "${data.motherMaidenName}",
        "heir": "${data.heir}",
        "heir_relation": "${data.heriRelation}",
        "heir_phone": "${data.heirPhone}"
      });

      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      // print(token);
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-2",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(">> DIOx : ");
      // print(e);
      return null;
    }
  }

  // Alamat
  // Getting country list
  Future<Response> getCountries() async {
    try {
      Response response = await dio.get("$apiLocal/countries/");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // Getting province list
  Future<Response> getProvinces() async {
    try {
      Response response = await dio.get("$apiLocal/province/");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
  }

  // Getting city list
  Future<Response> getCities(int id) async {
    try {
      Response response = await dio.get("$apiLocal/regencies/$id");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(e);
      return null;
    }
    // $apiLocal/regencies/$id
  }

  // Post alamat
  Future<Response> sendAlamat(AlamatUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "idcard_country": data.idCardCountry,
        "idcard_province": data.idCardProvince,
        "idcard_regency": data.idCardRegency,
        "idcard_address": data.idCardAddress,
        "idcard_postal_code": data.idCardPostalCode,
        "address_same_with_idcard": data.sameAddressWithIdCard,
        "country_domicile": data.country,
        "province": data.province,
        "regency": data.regency,
        "address": data.address,
        "postal_code": data.postalCode
      });

      final token = await storage.read(key: 'token');
      // print(token);
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-3",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      // print(">> DIOx : ");
      // print(e);
      return null;
    }
  }

  // Get user data alamat
  Future<Response> getAlamatData() async {
    try {
      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-3",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Update alamat
  Future<Response> updateAlamat(AlamatUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "idcard_country": data.idCardCountry,
        "idcard_province": data.idCardProvince,
        "idcard_regency": data.idCardRegency,
        "idcard_address": data.idCardAddress,
        "idcard_postal_code": data.idCardPostalCode,
        "address_same_with_idcard": data.sameAddressWithIdCard,
        "country_domicile": data.country,
        "province": data.province,
        "regency": data.regency,
        "address": data.address,
        "postal_code": data.postalCode
      });

      final token = await storage.read(key: 'token');
      // print(token);
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-3",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Post Pekerjaan
  Future<Response> sendPekerjaan(PekerjaanUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "name": data.occupation,
        "description_job": data.businessDesc,
        "source_of_investor_funds": data.fundsSource,
        "total_assets": data.assetTotal,
        "reason": data.investmentPurposes,
        "code": data.code,
        "reason_code": data.investmentPurposesId,
      });

      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-4",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get data pekerjaan
  Future<Response> getPekerjaanData() async {
    try {
      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-4",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Update Pekerjaan
  Future<Response> updatePekerjaan(PekerjaanUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "name": data.occupation,
        "description_job": data.businessDesc,
        "source_of_investor_funds": data.fundsSource,
        "total_assets": data.assetTotal,
        "reason": data.investmentPurposes,
        "code": data.code,
        "reason_code": data.investmentPurposesId,
      });

      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-4",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Post Tax
  Future<Response> sendInformasiPajak(InformasiPajakUser data) async {
    try {
      String filePath = "";
      String mediaType = "image";
      String fileType = "";
      if (data.securitiesAccount != null) {
        filePath = data.securitiesAccount.path;
        fileType = KycHelpers.getfiletype(filePath);
        mediaType = fileType == "pdf" ? "document" : "image";
      }
      FormData formData = FormData.fromMap({
        "income": data.income,
        "tax_account_code": data.taxAccountCode,
        "npwp": data.npwp,
        "date_registration_of_npwp": data.dateRegNpwp,
        "have_securities_account": data.haveSecurityAccount,
        "securities_account_date_registration": data.securityAccountRegDate,
        "securities_account": data.securitiesAccount != null
            ? await MultipartFile.fromFile(
                filePath,
                contentType: MediaType("$mediaType", "$fileType"),
                filename: path.basename(filePath),
              )
            : null,
        "sid_number": data.sidNumber,
      });
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-5",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get Tax Information
  Future<Response> getInformasiPajakData() async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-5",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Update Tax
  Future<Response> updateInformasiPajak(InformasiPajakUser data) async {
    String filePath = "";
    String mediaType = "image";
    String fileType = "";
    if (data.securitiesAccount != null) {
      filePath = data.securitiesAccount.path;
      fileType = KycHelpers.getfiletype(filePath);
      mediaType = fileType == "pdf" ? "document" : "image";
    }
    try {
      FormData formData = FormData.fromMap({
        "income": data.income,
        "tax_account_code": data.taxAccountCode,
        "npwp": data.npwp,
        "date_registration_of_npwp": data.dateRegNpwp,
        "have_securities_account": data.haveSecurityAccount,
        "securities_account_date_registration": data.securityAccountRegDate,
        "securities_account": data.securitiesAccount != null
            ? await MultipartFile.fromFile(
                filePath,
                contentType: MediaType("$mediaType", "$fileType"),
                filename: path.basename(filePath),
              )
            : null,
        "sid_number": data.sidNumber,
      });

      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-5",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Getting bank list
  Future<Response> getBanks() async {
    try {
      Response response = await dio.get("$apiLocal/banks/bank-investors");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
    // $apiLocal/regencies/$id
  }

  // Post banks
  Future<Response> sendBanks(BankUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "account_name1": "${data.accountName1}",
        "bank_investor1": data.bankInvestor1,
        "account_number1": "${data.accountNumber1}",
        "account_name2":
            data.accountName2 != null ? "${data.accountName2}" : "",
        "bank_investor2": data.bankInvestor2 != null ? data.bankInvestor2 : "",
        "account_number2":
            data.accountNumber2 != null ? "${data.accountNumber2}" : "",
        "account_name3":
            data.accountName3 != null ? "${data.accountName3}" : "",
        "bank_investor3": data.bankInvestor3 != null ? data.bankInvestor3 : "",
        "account_number3":
            data.accountNumber3 != null ? "${data.accountNumber3}" : "",
      });

      final headers = await UserAgent.headers();
      // print(token);
      Response response = await dio
          .post(
            "$apiLocal/traders/individual-kyc-6",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get Bank Information
  Future<Response> getBankData() async {
    try {
      final headers = await UserAgent.headers();
      Response response = await dio.get(
        "$apiLocal/traders/individual-6",
        options: Options(
          headers: headers,
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Post banks
  Future<Response> updateBanks(BankUser data) async {
    try {
      FormData formData = FormData.fromMap({
        "account_name1": "${data.accountName1}",
        "bank_investor1": data.bankInvestor1,
        "account_number1": "${data.accountNumber1}",
        "account_name2":
            data.accountName2 != null ? "${data.accountName2}" : "",
        "bank_investor2": data.bankInvestor2 != null ? data.bankInvestor2 : "",
        "account_number2":
            data.accountNumber2 != null ? "${data.accountNumber2}" : "",
        "account_name3":
            data.accountName3 != null ? "${data.accountName3}" : "",
        "bank_investor3": data.bankInvestor3 != null ? data.bankInvestor3 : "",
        "account_number3":
            data.accountNumber3 != null ? "${data.accountNumber3}" : "",
      });
      final headers = await UserAgent.headers();
      Response response = await dio
          .put(
            "$apiLocal/traders/individual-6",
            data: formData,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 60));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  Future<Response> updateFieldStatus(
    String submissionId,
    String fieldId,
    String stepId,
    int isEdited,
  ) async {
    try {
      FormData formData = FormData.fromMap({
        "field_id": fieldId,
        "step_id": stepId,
        "is_edited": isEdited,
      });

      final token = await storage.read(key: 'token');
      // print(token);
      Response response = await dio
          .put(
            "$apiLocal/kyc-submission-detail/submission_id/$submissionId",
            data: formData,
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 15));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // get submission id
  Future<Response> getSubmissionId() async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/kyc-submission/id",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Status individual
  Future<Response> statusIndividualKyc() async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/traders/status-kyc-trader",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
    } on DioError catch (e, stack) {
      santaraLog(e, stack);
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get field error status
  Future<Response> getErrorStatus(String submissionId, String stepId) async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/kyc-submission-detail/id/$submissionId?step_id=$stepId",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Accepting Terms & Condition
  Future<Response> acceptTnC() async {
    try {
      FormData formData = FormData.fromMap({"submit": 1});

      final token = await storage.read(key: 'token');
      // print(token);
      Response response = await dio
          .post(
            "$apiLocal/traders/submit",
            data: formData,
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 15));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get Term & Condition Status
  Future<Response> getUserData() async {
    try {
      final uuid = await storage.read(key: 'uuid');
      final token = await storage.read(key: 'token');
      Response response = await dio
          .get(
            '$apiLocal/users/by-uuid/$uuid',
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 15));
      // print(response.statusCode);
      // print(response.data.toString());
      return response;
      // if (response.statusCode == 200) {
      //   final data = User.fromJson(response.data);
      //   return data;
      // } else {
      //   return null;
      // }
    } on DioError catch (e) {
      return e.response;
      // return null;
    } catch (e, stack) {
      // print(e);
      return null;
    }
  }

  // get last edit
  Future<Response> getTimerKyc() async {
    try {
      final token = await storage.read(key: 'token');
      // print(token);
      Response response = await dio
          .get(
            "$apiLocal/traders/expired-kyc",
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 15));
      return response;
    } on DioError catch (e) {
      //
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Get pekerjaan list
  Future<Response> getPekerjaanList() async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/profession",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      // print(response.data);
      return response;
    } on DioError catch (e) {
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<Response> submitNewImage(File image) async {
    try {
      FormData formData = FormData.fromMap({
        "verified_kyc_image": await MultipartFile.fromFile(image.path,
            contentType: MediaType("image", "jpg"),
            filename: path.basename(image.path))
      });

      final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      Response response = await dio
          .post("$apiLocal/auth/verified-newimage-kyc",
              data: formData, options: Options(headers: headers))
          .timeout(Duration(seconds: 60));
      return response;
    } catch (err) {
      return null;
    }
  }
}
