import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/StatusKycTrader.dart';
import 'package:santaraapp/models/kyc/company/AlamatPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/AsetPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/BankPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/BiodataPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/DokPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/PajakPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/PjPerusahaanData.dart';
import 'package:santaraapp/models/kyc/company/ProfitPerusahaanData.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart' as path;
import 'package:santaraapp/utils/logger.dart';

class KycPerusahaanService {
  final storage = new secureStorage.FlutterSecureStorage();
  final Dio dio = Dio();

  Future<Response> handleApi(String uri, String status, dynamic formData,
      {Duration timeout = const Duration(seconds: 60)}) async {
    final headers = await UserAgent.headers();
    final token = await storage.read(key: 'token');
    Response response;
    Options options = Options(headers: headers);
    if (status == "empty") {
      response = await dio
          .post(uri, data: formData, options: options)
          .timeout(timeout);
    } else {
      response =
          await dio.put(uri, data: formData, options: options).timeout(timeout);
      logger.i("""
    >> Uri : $uri \n
    >> Status : $status
    >> Res : ${response.data}
    """);
    }
    return response;
  }

  // Get field error status
  Future<Response> getErrorStatus(String submissionId, String stepId) async {
    logger.i("$apiLocal/kyc-submission-detail/id/$submissionId?step_id=$stepId");
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/kyc-submission-detail/id/$submissionId?step_id=$stepId",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
    } on DioError catch (e) {
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<Response> statusKycTrader() async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/traders/status-kyc-trader",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
      // if (response.statusCode == 200) {
      //   return StatusKycTrader.fromJson(response.data);
      // } else {
      //   return null;
      // }
    } on DioError catch (e) {
      return null;
    } catch (e) {
      return null;
    }
  }

  // get submission id
  Future<Response> getSubmissionId() async {
    try {
      final token = await storage.read(key: 'token');
      Response response = await dio.get(
        "$apiLocal/kyc-submission/id",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  Future<Response> getPerusahaanData() async {
    try {
      final token = await storage.read(key: 'token');
      final uuid = await storage.read(key: 'uuid');
      Response response = await dio
          .get(
            "$apiLocal/users/by-uuid/$uuid",
            options: Options(
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ),
          )
          .timeout(Duration(seconds: 30));
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // BIODATA PERUSAHAAN
  Future<Response> postBiodataPerusahaan(
      BiodataPerusahaanData data, String status) async {
    var datax = {
      "name": "${data.companyName}",
      "company_type": "${data.companyJenis}",
      "company_type2_code": "${data.companyCharacter}",
      // "company_character": "${data.companyCharacter}",
      "company_syariah": "${data.companySyariah}",
      "company_country_domicile": "${data.companyCountryDomicile}",
      "company_establishment_place": "${data.companyEstablishmentPlace}",
      "company_date_establishment": "${data.companyDateEstablishment}",
      "another_email": "${data.companyAnotherEmail}",
      // "company_certificate_number": "${data}",
      "company_photo": data.companyPhoto != null
          ? await MultipartFile.fromFile(data.companyPhoto.path,
              contentType: MediaType("image", "jpg"),
              filename: path.basename(data.companyPhoto.path))
          : null,
      "description": "${data.companyDescription}"
    };
    // print(datax.toString());
    // status
    // jika status == empty, methodnya post
    // data.
    try {
      FormData formData = FormData.fromMap({
        "name": "${data.companyName}",
        "company_type": "${data.companyJenis}",
        // "company_type2_code": "${data.companyCharacter}",
        "company_character": "${data.companyCharacter}",
        "company_syariah": "${data.companySyariah}",
        "company_country_domicile": "${data.companyCountryDomicile}",
        "company_establishment_place": "${data.companyEstablishmentPlace}",
        "company_date_establishment": "${data.companyDateEstablishment}",
        "another_email": "${data.companyAnotherEmail}",
        // "company_certificate_number": "${data}",
        "company_photo": data.companyPhoto != null
            ? await MultipartFile.fromFile(data.companyPhoto.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.companyPhoto.path))
            : null,
        "description": "${data.companyDescription}"
      });
      String uri = "$apiLocal/traders/company-kyc-1";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // PAJAK & PERIZINAN
  Future<Response> postPajakPerizinan(
      PajakPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "tax_account_code": data.taxIdCorp,
        "lkpub_code": data.lkpbuCode,
        "npwp": data.npwp,
        "registration_date_npwp": data.npwpRegDate,
        "siup": data.bussinessRegCert,
        "nib": data.investorCompanyBICode,
        "company_certificate_number": data.articleOfAssc,
      });
      String uri = "$apiLocal/traders/company-kyc-2";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // ALAMAT
  Future<Response> postAlamat(AlamatPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "company_country_address": data.country,
        "company_province_address": data.province,
        "company_regency_address": data.city,
        "company_address": data.address,
        "postal_code": data.postalCode,
        "company_phone_number": data.phone,
        "fax": data.fax,
      });
      String uri = "$apiLocal/traders/company-kyc-3";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // ASET
  Future<Response> postAsetPerusahaan(
      AsetPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "source_of_funds": data.fundsSource,
        "funds_source_text": data.fundsSourceText,
        "company_total_property1": data.lastYearAsset,
        "company_total_property2": data.twoYearAsset,
        "company_total_property3": data.threeYearAsset,
      });
      String uri = "$apiLocal/traders/company-kyc-5";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // PROFIt
  Future<Response> postProfitPerusahaan(
      ProfitPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "company_income1": data.lastYearProfit,
        "company_income2": data.twoYearProfit,
        "company_income3": data.threeYearProfit,
        "reason_to_join": data.investmentCorp,
      });
      String uri = "$apiLocal/traders/company-kyc-6";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // POST BANK PERUSAHAAN
  Future<Response> postBankPerusahaan(
      BankPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "bank_investor1": data.bank1,
        "account_name1": data.name1,
        "account_number1": data.account1,
        "account_currency1": data.currency1,
        "bank_investor2": data.bank2,
        "account_name2": data.name2,
        "account_number2": data.account2,
        "account_currency2": data.currency2,
        "bank_investor3": data.bank3,
        "account_name3": data.name3,
        "account_number3": data.account3,
        "account_currency3": data.currency3,
      });
      String uri = "$apiLocal/traders/company-kyc-8";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // POST BANK PERUSAHAAN
  Future<Response> postDokumenPerusahaan(
      DokPerusahaanData data, String status) async {
    // data.
    try {
      FormData formData = FormData.fromMap({
        "company_document": data.dokAktaPendirianUsaha != null
            ? await MultipartFile.fromFile(data.dokAktaPendirianUsaha.path,
                contentType: MediaType('dokumen', 'pdf'),
                filename: path.basename(data.dokAktaPendirianUsaha.path))
            : null,
        "company_document_change": data.dokAktaPerubahanTerakhir != null
            ? await MultipartFile.fromFile(data.dokAktaPerubahanTerakhir.path,
                contentType: MediaType('dokumen', 'pdf'),
                filename: path.basename(data.dokAktaPerubahanTerakhir.path))
            : null,
        "document_sk_kemenkumham": data.dokSkKemenhumkam != null
            ? await MultipartFile.fromFile(data.dokSkKemenhumkam.path,
                contentType: MediaType('dokumen', 'pdf'),
                filename: path.basename(data.dokSkKemenhumkam.path))
            : null,
        "document_npwp": data.dokNpwp != null
            ? await MultipartFile.fromFile(data.dokNpwp.path,
                contentType: MediaType('dokumen', 'pdf'),
                filename: path.basename(data.dokNpwp.path))
            : null,
        "document_siup": data.dokSiup != null
            ? await MultipartFile.fromFile(data.dokSiup.path,
                contentType: MediaType('dokumen', 'pdf'),
                filename: path.basename(data.dokSiup.path))
            : null,
        "idcard_director": data.ktpDireksi != null
            ? await MultipartFile.fromFile(data.ktpDireksi.path,
                contentType: MediaType("image", "jpg"),
                filename: path.basename(data.ktpDireksi.path))
            : null,
      });
      String uri = "$apiLocal/traders/company-kyc-7";
      Response response = await handleApi(
        uri,
        status,
        formData,
        timeout: Duration(seconds: 120),
      );
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // POST BANK PERUSAHAAN
  Future<Response> pjBankPerusahaan(
      PjPerusahaanData data, String status) async {
    // data.
    try {
      var pjData = {
        "company_responsible_name1": data?.nama1 ?? "",
        "company_responsible_position1": data?.jabatan1 ?? "",
        "company_responsible_idcard_number1": data?.noKtp1 ?? "",
        "company_responsible_expired_date_idcard1": data?.dateExpKtp1 ?? "",
        "company_responsible_npwp1": data?.npwp1 ?? "",
        "company_responsible_passport1": data?.noPassport1 ?? "",
        "company_responsible_expired_date_passport1":
            data?.dateExpPassport1 ?? "",
        "company_responsible_name2": data?.nama2 ?? "",
        "company_responsible_position2": data?.jabatan2 ?? "",
        "company_responsible_idcard_number2": data?.noKtp2 ?? "",
        "company_responsible_expired_date_idcard2": data?.dateExpKtp2 ?? "",
        "company_responsible_npwp2": data?.npwp2 ?? "",
        "company_responsible_passport2": data?.noPassport2 ?? "",
        "company_responsible_expired_date_passport2":
            data?.dateExpPassport2 ?? "",
        "company_responsible_name3": data?.nama3 ?? "",
        "company_responsible_position3": data?.jabatan3 ?? "",
        "company_responsible_idcard_number3": data?.noKtp3 ?? "",
        "company_responsible_expired_date_idcard3": data?.dateExpKtp3 ?? "",
        "company_responsible_npwp3": data?.npwp3 ?? "",
        "company_responsible_passport3": data?.noPassport3 ?? "",
        "company_responsible_expired_date_passport3":
            data?.dateExpPassport3 ?? "",
        "company_responsible_name4": data?.nama4 ?? "",
        "company_responsible_position4": data?.jabatan4 ?? "",
        "company_responsible_idcard_number4": data?.noKtp4 ?? "",
        "company_responsible_expired_date_idcard4": data?.dateExpKtp4 ?? "",
        "company_responsible_npwp4": data?.npwp4 ?? "",
        "company_responsible_passport4": data?.noPassport4 ?? "",
        "company_responsible_expired_date_passport4":
            data?.dateExpPassport4 ?? ""
      };

      // print(">> Data : $pjData");

      FormData formData = FormData.fromMap(pjData);
      String uri = "$apiLocal/traders/company-kyc-4";
      Response response = await handleApi(uri, status, formData);
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }
  // END

  // BANK
  // Getting bank list
  Future<Response> getBanks() async {
    try {
      Response response = await dio.get("$apiLocal/banks/bank-investors");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
    // $apiLocal/regencies/$id
  }

  // Alamat
  // Getting country list
  Future<Response> getCountries() async {
    try {
      Response response = await dio.get("$apiLocal/countries/");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Getting province list
  Future<Response> getProvinces() async {
    try {
      Response response = await dio.get("$apiLocal/province/");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
  }

  // Getting city list
  Future<Response> getCities(int id) async {
    try {
      Response response = await dio.get("$apiLocal/regencies/$id");
      return response;
    } on DioError catch (e) {
      return e.response;
    } catch (e) {
      return null;
    }
    // $apiLocal/regencies/$id
  }
}
