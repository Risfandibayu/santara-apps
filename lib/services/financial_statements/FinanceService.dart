import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/utils/api.dart';

class FinanceService {
  final Dio dio = Dio();
  final NetworkRequest networkRequest = NetworkRequest();
  final storage = secureStorage.FlutterSecureStorage();

  // Preload Data Laporan Keuangan
  // Get status laporan keuangan / rencana penggunaan dana
  Future<ApiResult> getPreloadReport(String uuid) async {
    return await networkRequest.getData("$apiLocal/portofolio/tab/$uuid");
  }

  // Get profile bisnis
  Future<ApiResult> getBusinessProfile(String uuid) async {
    return await networkRequest.getData(
      "$apiLocal/emitens/$uuid",
    );
  }

  // Get chart emiten (Tab Laporan Keuangan)
  Future<ApiResult> getEmitenChart(String uuid) async {
    return await networkRequest.getData(
      "$apiLocal/portofolio/detail/by/$uuid",
    );
  }

  // Get data laporan keuangan (last report / history)
  Future<ApiResult> getLastFinancialStatements(String uuid) async {
    return await networkRequest.getData(
      "$apiLocal/finance-report/last/$uuid",
    );
  }

  // Get data rencana penggunaan dana
  Future<ApiResult> getFundsPlan(String uuid) async {
    return await networkRequest.getData(
      "$apiLocal/finance-report/fund-plans/$uuid",
    );
  }

  // Get data daftar bisnis user
  Future<ApiResult> getUserBusinessList() async {
    return await networkRequest.getData(
      "$apiLocal/finance-report/all-business/",
    );
  }
}
