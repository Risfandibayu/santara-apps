import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:santaraapp/services/api_res.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:santaraapp/utils/logger.dart';

class PortfolioService {
  final Dio dio = Dio();
  final NetworkRequest networkRequest = NetworkRequest();
  final storage = new secureStorage.FlutterSecureStorage();

  // Get portfolio list
  Future<ApiResult> getPortfolios(
    String keyword,
    String category,
    String sort,
  ) async {
    // Jika kategori = 666, maka category = empty
    if (category == "666" || category == "Semua") {
      category = "";
    }
    String url =
        "$apiLocal/portofolio?category=${category ?? ''}&sort=${sort ?? ''}&search=${keyword ?? ''}";
    return await networkRequest.getData(url);
  }

  // Get portfolio detail
  Future<ApiResult> getPortfolio(String uuid) async {
    String url = "$apiLocal/portofolio/detail/$uuid";
    return await networkRequest.getData(url);
  }

  // Get portfolio table (Riwayat Laporan Keuangan)
  Future<ApiResult> getPortfolioTable(
      String uuid, String dateStart, String dateEnd) async {
    String url =
        "$apiLocal/portofolio/list/$uuid?start=$dateStart&end=$dateEnd&type=mobile";
    logger.i(url);
    return await networkRequest.getData(url);
  }

  // Get portfolio table (Riwayat Laporan Keuangan)
  Future<ApiResult> getPortfolioTablePenerbit(
      String uuid, String dateStart, String dateEnd) async {
    String url =
        "$apiLocal/portofolio/list/$uuid?start=$dateStart&end=$dateEnd&type=mobile";
    return await networkRequest.getData(url);
  }
}
