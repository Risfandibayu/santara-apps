import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';

class PhoneApiService {
  final storage = new FlutterSecureStorage();

  Future<http.Response> resetPhone(
      String phone, String pin, bool finger) async {
    try {
      final token = await storage.read(key: 'token');
      // print(token);
      final response = await http.put(
        '$apiLocal/users/update-phone-number',
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        body: {"phone": phone, "pin": pin, "finger": finger.toString()},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> requestResetPhone(String phone) async {
    // User datas;
    // datas = userFromJson(await storage.read(key: 'user'));
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final response = await http.post(
        '$apiLocal/auth/request-reset-phone-number',
        headers: headers,
        body: {"phone": phone},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }
}
