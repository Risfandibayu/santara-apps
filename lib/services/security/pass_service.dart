import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/models/User.dart';
import 'package:santaraapp/utils/api.dart';

class PasswordApiService {
  final storage = new FlutterSecureStorage();

  Future<http.Response> resetPassword(
      String pass1, String pass2, String pin, bool finger) async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      // print(token);
      final response = await http.post(
        '$apiLocal/auth/reset-pass',
        headers: headers,
        body: {
          "newPassword": pass1,
          "newPasswordConfirmation": pass2,
          "pin": pin,
          "finger": finger.toString()
        },
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> requestResetPassword() async {
    User datas;
    datas = userFromJson(await storage.read(key: 'user'));
    // print(">> Start Request Password : ");
    // print(datas.email);
    try {
      final headers = await UserAgent.headers();
      // final token = await storage.read(key: 'token');
      final response = await http.post(
        '$apiLocal/auth/request-reset-pass',
        headers: headers,
        body: {"email": datas.email},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> getCheckReset() async {
    try {
      final token = await storage.read(key: 'token');

      final response = await http
          .get('$apiLocal/auth/check-reset-password', headers: {
        'Authorization': 'Bearer $token'
      }).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }
}
