import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    as secureStorage;
import 'package:http/http.dart' as http;
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';

class PinService {
  final storage = new secureStorage.FlutterSecureStorage();
  final Dio dio = Dio();

  Future<http.Response> registerPIN(String pin) async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final response = await http.post(
        '$apiLocal/auth/register-pin',
        headers: headers,
        body: {"pin": pin},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> updatePIN(
      String oldPin, String newPin, bool finger) async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final response = await http.put(
        '$apiLocal/users/update-pin',
        headers: headers,
        body: {"pin": oldPin, "new_pin": newPin, "finger": finger.toString()},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err, _) {
      return null;
    }
  }

  Future<http.Response> verifyPIN(String pin) async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final response = await http.post(
        '$apiLocal/auth/verify-pin',
        headers: headers,
        body: {"pin": pin},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> requestResetPIN(String password) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
        '$apiLocal/auth/request-reset-pin',
        headers: headers,
        body: {"password": password},
      ).timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> verifyOTP(String otp) async {
    try {
      final headers = await UserAgent.headers();
      final response = await http.post(
        '$apiLocal/otp/verify-otp',
        headers: headers,
        body: {"code": otp},
      ).timeout(Duration(seconds: 15));
      // debugPrint(">> RESPONSE : ");
      // debugPrint("${response.body}");
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<DateTime> getExpiredOtp() async {
    // otp/otp-expired
    try {
      final headers = await UserAgent.headers();
      // final token = await storage.read(key: 'token');
      final http.Response response = await http
          .get("$apiLocal/otp/otp-expired?type=reset", headers: headers);
      if (response.statusCode == 200) {
        var parsed = json.decode(response.body);
        // print(">> RESULT : ${response.body}");
        return DateTime.parse(parsed["data"]);
      } else {
        return null;
      }
    } catch (e) {
      // print(">> An error has occured, message : $e");
      return null;
    }
  }

  Future<http.Response> resendOTP() async {
    // otp/otp-expired
    try {
      final headers = await UserAgent.headers();
      // final token = await storage.read(key: 'token');
      final http.Response response =
          await http.get("$apiLocal/otp/?type=reset", headers: headers);
      return response;
    } catch (e) {
      return null;
    }
  }

  Future<http.Response> getExistPIN() async {
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final http.Response response =
          await http.get("$apiLocal/users/check-exist-pin", headers: headers);
      return response;
    } catch (e) {
      // print(">> An error has occured, message : $e");
      return null;
    }
  }

  Future<http.Response> getExistFP() async {
    try {
      final token = await storage.read(key: 'token');
      final http.Response response =
          await http.get("$apiLocal/users/check-exist-fp", headers: {
        HttpHeaders.authorizationHeader: "Bearer $token",
      }).timeout(Duration(seconds: 20));
      return response;
    } catch (e) {
      // print(">> An error has occured, message : $e");
      return null;
    }
  }

  Future<Response> updateFP(bool value) async {
    var body = {"finger_print": value};
    try {
      // final token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();

      final response = await dio
          .put(
            '$apiLocal/auth/update-fp',
            data: body,
            options: Options(
              headers: headers,
            ),
          )
          .timeout(Duration(seconds: 30));
      return response;
    } catch (err) {
      return null;
    }
  }
}
