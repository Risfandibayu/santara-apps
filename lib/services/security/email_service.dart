import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:santaraapp/helpers/UserAgent.dart';
import 'package:santaraapp/utils/api.dart';
import 'package:http/http.dart' as http;

class EmailApiService {
  final storage = new FlutterSecureStorage();

  Future<http.Response> updateEmail(
      String email, String pin, bool finger) async {
    try {
      // var token = await storage.read(key: 'token');
      final headers = await UserAgent.headers();
      final http.Response response = await http
          .put('$apiLocal/users/update-email',
              body: {"email": email, "pin": pin, "finger": finger.toString()},
              headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }

  Future<http.Response> requestResetEmail() async {
    try {
      final headers = await UserAgent.headers();
      // var token = await storage.read(key: 'token');
      final http.Response response = await http
          .post('$apiLocal/auth/request-reset-email', headers: headers)
          .timeout(Duration(seconds: 15));
      return response;
    } catch (err) {
      return null;
    }
  }
}
