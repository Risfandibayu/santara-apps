import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:santaraapp/widget/pralisting/pre_screening/pre_screening_injector.dart';
import 'package:santaraapp/widget/pralisting/prospectus/prospectus_injector.dart';
import 'package:santaraapp/widget/pralisting/registration/business_dev/business_dev_injector.dart';
import 'package:santaraapp/widget/pralisting/registration/business_info_2/business_info_2_injector.dart';
import 'package:santaraapp/widget/pralisting/registration/business_prospective_info/business_prospective_injector.dart';
import 'package:santaraapp/widget/pralisting/registration/financial_info/financial_info_injector.dart';
import 'package:santaraapp/widget/pralisting/registration/terms_conditions/tnc_injector.dart';
import 'package:local_auth/local_auth.dart';

import 'core/data/datasources/fee_remote_data_source.dart';
import 'core/data/datasources/user_remote_data_source.dart';
import 'core/data/repositories/fee_repository_impl.dart';
import 'core/data/repositories/user_repository_impl.dart';
import 'core/domain/repositories/fee_repository.dart';
import 'core/domain/repositories/user_repository.dart';
import 'core/domain/usecases/fee_usecase.dart';
import 'core/domain/usecases/user_usecase.dart';
import 'core/http/network_info.dart';
import 'core/http/rest_client.dart';
import 'core/http/rest_client_impl.dart';
import 'widget/pralisting/business_filed/business_filed_injector.dart';
import 'widget/pralisting/detail/pralisting_detail_injector.dart';
import 'widget/pralisting/journey/pralisting_journey_injector.dart';
import 'widget/pralisting/registration/business_info_1/business_info_1_injector.dart';
import 'widget/pralisting/registration/financial_projections/financial_projections_injector.dart';
import 'widget/pralisting/registration/management_info/management_info_injector.dart';
import 'widget/pralisting/registration/media/media_injector.dart';
import 'widget/pralisting/registration/share_info/share_info_injector.dart';
import 'core/utils/platforms/biometric.dart';
import 'features/dana/dana_injector.dart';
import 'features/deposit/deposit_injector.dart';
import 'features/disbursement/disbursement_injector.dart';
import 'features/dividen/dividen_injector.dart';
import 'features/payment/payment_injector.dart';
import 'features/portofolio/portofolio_injector.dart';
import 'features/transaction/transaction_injector.dart';
import 'features/withdraw/withdraw_injector.dart';
import 'widget/pralisting/rejected/pralisting_rejected_injector.dart';

final sl = GetIt.instance;

Future<void> init() async {
  initBusinessProspective();
  initBusinessInfoTwoInjector();
  initFinancialInfoInjector();
  initBusinessInfoOneInjector();
  initPreScreeningInjector();
  initManagementInfoInjector();
  initBusinessDevInjector();
  initFinancialProjectionsInjector();
  initShareInfoInjector();
  initMediaInjector();
  initBusinessFiled();
  initPralistingJourneyInjector();
  initPralistingDetail();
  initTncPralistingInjector();
  initProspectusInjector();
  initPralistingRejected();

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<BiometricApi>(
      () => BiometricApiImpl(localAuthentication: sl()));
  sl.registerLazySingleton<RestClient>(
    () => RestClientImpl(
      dioClient: sl(),
    ),
  );

  // Fee
  sl.registerLazySingleton(() => GetUserByUuid(sl()));

  sl.registerLazySingleton<FeeRepository>(
    () => FeeRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<FeeRemoteDataSource>(
    () => FeeRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  // User
  sl.registerLazySingleton(() => GetAllFee(sl()));

  sl.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<UserRemoteDataSource>(
    () => UserRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  // Globals

  // deposit
  initDepositInjector();

  // withdraw
  initWithdrawInjector();

  // payment
  initPaymentInjector();

  // portofolio
  initPortofolioInjector();

  // dana
  initDanaInjector();

  // transaction
  initTransactionInjector();

  // disbursement
  initDisbursementInjector();

  // dividen
  initDividenInjector();

  //! External 3rd Party
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => FlutterSecureStorage());
  sl.registerLazySingleton(() => LocalAuthentication());
}
