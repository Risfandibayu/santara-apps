import 'dart:async';

import 'package:santaraapp/utils/logger.dart';
import 'package:url_launcher/url_launcher.dart';

import 'helpers/Connections.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';

// component
import 'pages/Home.dart';

class Main extends StatefulWidget {
  final uuidPage;
  final spesificPage;
  final String dynamicLink;
  Main({this.spesificPage, this.uuidPage,  this.dynamicLink});
  @override
  _MainState createState() =>
      _MainState(spesificPage: spesificPage, uuidPage: uuidPage);
}

class _MainState extends State<Main> {
  final uuidPage;
  final spesificPage;
  _MainState({this.spesificPage, this.uuidPage});
  String connectionStatus;
  final Connectivity connectivity = new Connectivity();
  StreamSubscription<ConnectivityResult> streamSubscription;

  @override
  void initState() {
    super.initState();
    streamSubscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        connectionStatus = result.toString();
      });
    });
    if (widget.dynamicLink != null && widget.dynamicLink.isNotEmpty) {
      _launchUrl(widget.dynamicLink);
    }
  }

  @override
  void dispose() {
    super.dispose();
    streamSubscription.cancel();
  }

  void _launchUrl(String url) async {
    try {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: false);
      } else {
        throw 'Cannot launch $url';
      }
    } catch (e, stack) {
      santaraLog(e, stack);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget home;
    if (connectionStatus == ConnectivityResult.none.toString()) {
      setState(() {
        home = Connections();
      });
    } else {
      setState(() {
        home = Home(
            isFirstOpen: 0,
            index: spesificPage == 2 ? 3 : spesificPage == 4 ? 4 : spesificPage == 5 ? 5 : 0,
            spesificPage: spesificPage,
            uuidPage: uuidPage);
      });
    }
    return home;
  }
}
