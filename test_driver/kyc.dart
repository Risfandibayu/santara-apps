import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:path_provider/path_provider.dart';
import 'package:santaraapp/main.dart' as app;

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension();

  // Picture Picker
  const MethodChannel channel =
      MethodChannel('plugins.flutter.io/image_picker');
  channel.setMockMethodCallHandler((MethodCall methodCall) async {
    ByteData data = await rootBundle.load('assets/santara/profil.jpg');
    Uint8List bytes = data.buffer.asUint8List();
    Directory tempDir = await getTemporaryDirectory();
    File file = await File(
      '${tempDir.path}/image.jpg',
    ).writeAsBytes(bytes);
    print(file.path);
    return file.path;
  });

  // File Picker (PDF)
  const MethodChannel filePickerChannel =
      MethodChannel('miguelruivo.flutter.plugins.filepicker');
  filePickerChannel.setMockMethodCallHandler((MethodCall methodCall) async {
    ByteData data = await rootBundle.load('assets/santara/santara-docs.pdf');
    Uint8List bytes = data.buffer.asUint8List();
    Directory tempDir = await getTemporaryDirectory();
    File file = await File(
      '${tempDir.path}/santara-pdf-doc.pdf',
    ).writeAsBytes(bytes);
    print(file.path);
    return file.path;
  });

  // miguelruivo.flutter.plugins.filepicker
  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  app.main();
}
