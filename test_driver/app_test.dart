// Imports the Flutter Driver API.
import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'class/Utils.dart';

void main() {
  group('Santara', () {
    FlutterDriver driver;
    StreamSubscription streamSubscription;

    // listview homepage
    final indexListView = find.byValueKey('indexListViewKey');
    final String userEmail = "mariogil@spam4.me";
    final String wrongPassword = "iIksadBuw";
    final String correctPassword = "123123123";
    final String userPin = "121212";
    final String emitenName = "Nakula Astro";
    Utility utils;
    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      utils = Utility(driver);
      streamSubscription = driver.serviceClient.onIsolateRunnable
          .asBroadcastStream()
          .listen((isolateRef) {
        print(
            'Resuming isolate: ${isolateRef.numberAsString}:${isolateRef.name}');
        isolateRef.resume();
      });
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      await Future.delayed(Duration(seconds: 10));
      if (driver != null) {
        driver.close();
      }
      if (streamSubscription != null) streamSubscription.cancel();
    });

    test('Close Popup', () async {
      await utils.findAndTap('closePopup'); // tutup popup
      await utils.findAndTap('hideRating'); // tutup popup rating
    });

    test('Testing Button Daftarkan Bisnis', () async {
      await utils.findAndTap('daftarkanButton'); // tap daftarkan bisnis
      await utils.expectValueInWidget(
          "Maaf, silakan login terlebih dahulu."); // expect apakah teks ada di layar
      // await Future.delayed(Duration(milliseconds: 800)); // delay
      await utils.findAndTap('hideDaftarkanBisnisSheet'); // tap mengerti
    });

    test('Testing Daftar Bisnis', () async {
      // inisialisasi identifier button / widget
      final widgetKey = 'viewAllBusinessBtn';
      final lihatSemuaBisnis = find.byValueKey(widgetKey);
      // listview emiten
      final emitenListView = find.byValueKey('emitenListViewKey');

      // Scroll ke widget
      await driver.scrollUntilVisible(
        indexListView, // identifier listview
        lihatSemuaBisnis, // identifier yang mau dituju saat scrolling
        dyScroll: -260.0,
      );
      // Lakukan tap button
      await utils.findAndTap(widgetKey); // tap daftarkan bisnis
      // Aplikasi akan mengarahkan ke halaman list bisnis
      await Future.delayed(Duration(milliseconds: 600)); // Delay
      // cek apakah sudah ada di halaman list bisnis
      await utils.expectValueInWidget(
        "List Bisnis", // cek appbar title
      ); // expect apakah teks ada di layar
      await Future.delayed(
          Duration(milliseconds: 1500)); // delay agar view tetap di list bisnis
      // Tutup halaman
      await driver.tap(find.pageBack());
      // Tap pada salah satu bisnis berdasarkan trademark
      await utils.findAndTap('Citraksi'); // tap emiten
      // tap pada button view progress emiten
      await Future.delayed(Duration(milliseconds: 500)); // delay
      await utils.findAndTap('viewProgressEmitenBtn'); // tap daftarkan bisnis
      // await driver.runUnsynchronized(() async {
      //   final buttonFinder = find.byValueKey('viewProgressEmitenBtn');
      //   await driver.tap(buttonFinder);
      // });
      await Future.delayed(Duration(milliseconds: 500)); // delay
      await utils.findAndTapByText('Tutup');
      // Scroll ke syarat dan ketentuan
      await driver.scrollUntilVisible(
        emitenListView, // identifier listview
        find.byValueKey(
            'viewTermsConditionBtn'), // identifier yang mau dituju saat scrolling
        dyScroll: -260.0,
      );
      await utils.findAndTap('viewTermsConditionBtn');
      await Future.delayed(Duration(milliseconds: 500)); // delay
      await utils.findAndTapByText('Mengerti');
      await Future.delayed(Duration(milliseconds: 500)); // delay
      await driver.tap(find.pageBack());
    });

    test('Testing Pralisting Belum Login', () async {
      // Scroll ke pralisting list
      await driver.scrollUntilVisible(
        indexListView, // identifier listview
        find.byValueKey(
          'pralistingListKey',
        ), // identifier yang mau dituju saat scrolling
        dyScroll: -100.0,
      );
      // Klik pada salah satu pralisting
      await utils.findAndTap('Testt'); // tap navigator bar akun
      await utils.expectValueInWidget(
        "Maaf, silakan login terlebih dahulu.", // cek muncul bottom sheet anda harus login
      );
      await utils.findAndTapByText('Mengerti');
    });

    group('Authentication', () {
      test('Open Login Page', () async {
        await utils.findAndTap('tabAkun'); // tap navigator bar akun
        await utils.findAndTap('loginButton'); // tap tombol login
      });

      test('Simulate Wrong Password', () async {
        // Input email & password
        await utils.findAndTap('emailField'); // tap pada field email
        await driver.enterText(userEmail); // input email
        await Future.delayed(Duration(milliseconds: 500)); // delay
        await utils.findAndTap('passwordField'); // tap pada field password
        await driver.enterText(wrongPassword); // input password
        await Future.delayed(Duration(milliseconds: 500)); // delay
        // Aksi login
        await utils.findAndTap('loggingIn'); // tap pada field email
        await utils.findAndTapByText('Ok');
      });

      test('Simulate Correct Password', () async {
        // Balik ke field password
        await utils.findAndTap('passwordField'); // tap pada field password
        await driver.enterText(correctPassword); // input password yang benar
        await Future.delayed(Duration(milliseconds: 500)); // delay
        // Aksi login
        await utils.findAndTap('loggingIn'); // tap pada field email
      });
    });

    group('Simulate Beli Saham', () {
      test('View List Saham', () async {
        // Tap tab home
        await utils.findAndTap('tabHome');
        // inisialisasi identifier button / widget
        final widgetKey = 'viewAllBusinessBtn';
        final lihatSemuaBisnis = find.byValueKey(widgetKey);
        // Scroll ke widget
        await driver.scrollUntilVisible(
          indexListView, // identifier listview
          lihatSemuaBisnis, // identifier yang mau dituju saat scrolling
          dyScroll: -400.0,
          timeout: Duration(seconds: 60),
        );
        // Lakukan tap button
        await utils.findAndTap(widgetKey); // tap lihat semua bisnis
        await utils.findAndTap(
          'dropdownSortBtn',
          timeout: Duration(seconds: 60),
        ); // tap urutkan
        await utils.findAndTapByText('Terbaru');
        await utils.findAndTap(
          'dropdownCategoryBtn',
          timeout: Duration(seconds: 60),
        ); // tap urutkan
        await utils.findAndTapByText('Perdagangan Besar dan Eceran');
        // Tap pada salah satu bisnis berdasarkan trademark
        await utils.findAndTapByText(emitenName); // tap emiten
      });

      test('View Detail Emiten & Input PIN', () async {
        // Tap beli saham
        await utils.findAndTapByText('Beli Saham Sekarang');
        // Validate PIN
        utils.validatePIN(userPin);
      });

      test('Payment Progress', () async {
        // input lembar saham
        await utils.findAndTap('inputLembarSaham'); // tap pada field email
        await driver.enterText('30000'); // input jumlah lembar saham
        await utils.findAndTap('beliSahamBtn'); // tap pada button beli saham
        await utils.findAndTap('beliSaham'); // tap pada button beli
        await Future.delayed(Duration(milliseconds: 500));
        await utils.findAndTap('methodWallet'); // tap pada Saldo Wallet
        await utils.findAndTap('pilihPaymentBtn'); // tap pada pilih
        await Future.delayed(Duration(seconds: 5));
        await utils.expectValueInWidget(
          'Checkout Saham Telah Berhasil',
        ); // cek apakah beli berhasil
        await utils.findAndTapByText('Ok'); // tap pada button ok
        await utils.findAndTap('historyTransactionTab');
        await utils
            .expectValueInWidget(emitenName); // cek apakah beli berhasil
      });
    });
  });
}
