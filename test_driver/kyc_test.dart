// Imports the Flutter Driver API.
import 'dart:async';
import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'class/Utils.dart';
import 'package:faker/faker.dart';

void main() {
  group('Santara', () {
    FlutterDriver driver;
    StreamSubscription streamSubscription;

    // listview homepage
    final String userEmail = "user23@mail.com";
    final String wrongPassword = "iIksadBuw";
    final String correctPassword = "123123123";
    final String userPin = "121212";
    Utility utils;
    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      // final envVars = Platform.environment;
      final adbPath =
          '/Users/ikhwansetyo/Library/Android/sdk/platform-tools/adb';
      await Process.run(adbPath, [
        'shell',
        'pm',
        'grant',
        'id.co.santara.app',
        'android.permission.RECORD_AUDIO'
      ]);
      await Process.run(adbPath, [
        'shell',
        'pm',
        'grant',
        'id.co.santara.app',
        'android.permission.CAMERA'
      ]);

      driver = await FlutterDriver.connect();
      utils = Utility(driver);
      streamSubscription = driver.serviceClient.onIsolateRunnable
          .asBroadcastStream()
          .listen((isolateRef) {
        print(
            'Resuming isolate: ${isolateRef.numberAsString}:${isolateRef.name}');
        isolateRef.resume();
      });
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      await Future.delayed(Duration(seconds: 10));
      if (driver != null) {
        driver.close();
      }
      if (streamSubscription != null) streamSubscription.cancel();
    });

    test('Close Popup', () async {
      await utils.findAndTap('closePopup'); // tutup popup
      await utils.findAndTap('hideRating'); // tutup popup rating
    });

    group('Authentication', () {
      test('Open Login Page', () async {
        await utils.findAndTap('tabAkun'); // tap navigator bar akun
        await utils.findAndTap('loginButton'); // tap tombol login
      });

      test('Simulate Wrong Password', () async {
        // Input email & password
        await utils.findAndTap('emailField'); // tap pada field email
        await driver.enterText(userEmail); // input email
        await Future.delayed(Duration(milliseconds: 500)); // delay
        await utils.findAndTap('passwordField'); // tap pada field password
        await driver.enterText(wrongPassword); // input password
        await Future.delayed(Duration(milliseconds: 500)); // delay
        // Aksi login
        await utils.findAndTap('loggingIn'); // tap pada field email
        await utils.findAndTapByText('Ok');
      });

      test('Simulate Correct Password', () async {
        // Balik ke field password
        await utils.findAndTap('passwordField'); // tap pada field password
        await driver.enterText(correctPassword); // input password yang benar
        await Future.delayed(Duration(milliseconds: 500)); // delay
        // Aksi login
        await utils.findAndTap('loggingIn'); // tap pada field email
      });

      // Masuk tab profil
      // Jika belum memilih tipe trader
      // test('Simulate Choose Trader Type', () async {
      //   // Cari widget lengkapi data untuk mendanai
      //   await utils.findAndTapByText(
      //     'Mohon lengkapi data Anda untuk mulai mendanai.',
      //   );
      //   // masuk page pilih jenis akun
      //   await utils.findAndTapByText('Perseorangan');
      //   // Muncul popup konfirmasi jenis akun
      //   await utils.findAndTapByText('Yakin');
      // });

      // Jika sudah memilih tipe trader (contoh pribadi / personal)
      test('To KYC', () async {
        // Cari widget lengkapi data untuk mendanai
        await utils.findAndTapByText(
          'Anda belum menyelesaikan proses KYC',
        );
      });

      // Biodata Pribadi
      test('Simulate KYC Biodata Pribadi', () async {
        await utils.findAndTapByText('Biodata Pribadi');
        // await utils.findAndTapByText('Unggah Foto');
        await Future.delayed(Duration(milliseconds: 1500));
        await utils.findAndTap('full_name');
        await driver.enterText(
          faker.person.firstName() + " " + faker.person.lastName(),
        ); // input nama
        await utils.findAndTapByText('Pilih Tempat Lahir Anda');
        await utils.findAndTapByText('Aceh Barat');
      });
    });
  });
}
