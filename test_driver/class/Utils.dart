import 'dart:async';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

class Utility {
  final FlutterDriver driver;
  Utility(this.driver);

  // Method untuk nyari button & melakukan tap dengan key
  Future findAndTap(String key, {Duration timeout}) async {
    final button = find.byValueKey(key);
    // menunggu button / element ada di user interface
    await driver.waitFor(button, timeout: timeout ?? Duration(seconds: 30));
    // Tap tombol / element
    await driver.tap(button, timeout: timeout ?? Duration(seconds: 30));
  }

  // Method nyari widget & tap berdasarkan teks
  Future findAndTapByText(String text) async {
    final button = find.text(text);
    // menunggu button / element ada di user interface
    await driver.waitFor(button);
    // Tap tombol / element
    await driver.tap(button);
  }

  // Method untuk nyari text yang ada di widget
  Future expectValueInWidget(String text) async {
    final widget = find.text(text); // nyari text yang ada di widget
    expect(await driver.getText(widget), text); // expecting
  }

  // Method untuk validasi pin
  Future validatePIN(String pins) async {
    List<String> p = pins.split('');
    await findAndTap('pin${p[0]}');
    await findAndTap('pin${p[1]}');
    await findAndTap('pin${p[2]}');
    await findAndTap('pin${p[3]}');
    await findAndTap('pin${p[4]}');
    await findAndTap('pin${p[5]}');
  }
}
